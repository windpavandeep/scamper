@extends('frontEnd.layouts.master')
@section('title','Home')
@section('content')
@include('frontEnd.common.header')
<style type="text/css">
	.nw-hold{
		width: 80px;
	}
	.nw-plnlt{
		overflow: auto;
		min-height: 100px;
		/*max-height: 100px;*/
		height: 100px;
	}
	.nw-wrap-mrtbt{
		margin-bottom: 15px;
	}
	.new-cls-rp-txt{
        word-wrap: break-word;
        overflow: auto;
        min-height: auto;
        max-height: 50px;
        height: 50px;
    }
    .swal-modal{
    	width: 463px;
    }
</style>

<div class="pg-wrapper" id="outer_div">
	<div class="pg-container index-pg">
		<section class="slider_home">
			<div class="hero-slider">
					@foreach($banner_images as $banner_image)
				<div class="slider-item">
					<img src="{{'public/images/banner_images/'.$banner_image['image']}}" class="img-responsive hero-slider-img">
				</div>
					@endforeach
			
			</div>
    		<div class="slider-overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="hero-sec-content-wrapper">
	                        <h1 class="hero-title">{{ucfirst($home_pages['title'])}}</h1>
	                        <p class="hero-body">{{ucfirst($home_pages['description'])}}</p>
			        		<div class="slider-btn-div">
			        			<a class="exp btn" id="join">JOIN NOW</a>
			        			<a class="trial btn" href="{{url('/explore/'.'activities')}}">EXPLORE ACTIVITIES</a>
			        		</div>
						</div>
					</div>
				</div>
			</div>				
        </section>
        @if(!empty($top_classes))
        <div class="section-divider-wrapper">
        	<p class="section-divider-text">OneVay is a monthly membership that gives you access to the best fitness studios, services and social activities near you.</p>
        </div>
        <section class="top-studios-section text-center">
        	<div class="container">
        		<div class="section-heading blue-color">
					<h2 style="margin:0;">Top Classes</h2>
					<span class="divider"></span>
				</div>
				<div class="top-studios">
					<div class="top-studios-slider">
						@foreach($top_classes as $key=>$value)
							<div class="col-sm-4">
		                        <div class="slick_item">
		                        	<div class="slick-img">
		                        		<?php
		                        			if (!empty($value['image'])) {
		                        				$detail = strval(str_replace("\0","",$value['image']));
		                        				if (file_exists(FacilityBasePath.'/'.$detail)) {
		                        					$image = FacilityImgPath.'/'.$value['image'];
		                        				}else{
		                        					$image = DefaultUserPath;
		                        				}
		                        			}else{
		                        				$image = DefaultUserPath;
		                        			}
		                        		?>
		                            	<img src="{{ $image }}" class="img-responsive">
		                            </div>
		                            <div class="slide-heading">{{ ucfirst($value['name']) }}</div>
		                            <a href="javascript:void(0);">{{ ucfirst($value['business_name']) }}</a>
		                            <div class="ratings-sec">
			                            <div class="col-sm-6 text-right">
				                            <div class="rating-bar">
		                                        <ul class="list-inline">
		                                        	@for($i = 0; $i < 5; $i++)
		                                        	<li>
	      											<i class="fa fa-star{{ $value['ratings'] <= $i ? '-o' : '' }}" style="color: #3a83ff"></i></li>
	  												@endfor
		                                        </ul>
		                                    </div>
		                                </div>
		                                <div class="col-sm-6 text-left">
		                                	<div class="rev-div">{{$value['user_count']}} Reviews</div>
		                                </div>
		                            </div>
	                                <div class="rev-dec">
	                                	<p><!-- Cras ultricies lacus consectetur, consectetur scelerisque arcu.Curabitur --></p>
	                                </div>
		                            <div class="time-duration">
	                                    <div class="student-duration">
	                                        <!-- <strong>Duration</strong><br>
	                                        6:30 - 8:30am --> 
	                                        <?php 
	                                        	$single_facility_detail=url('/single-facility-detail').'/'.base64_encode($value['id']);
	                                        ?>
	                                        <a href="{{ url($single_facility_detail) }}"><strong>View Sessions</strong></a>
	                                    </div>
	                                </div>
		                        </div>
		                    </div>
	                 	@endforeach
                    </div>
				</div>
			</section>
        @endif

		<section class="htw-section" id="index-how-it-works-section">
			<div class="how-it-wrks">
				<div class="sec-contnt">
					<div class="section-heading black-color text-center">
						<h2>How It Works</h2>
						<span class="divider"></span>
						<p>One account. Unlimited activities. Members-only prices.</p>
					</div>
					<div class="container">
						<div class="row">
							<!-- <div class="col-sm-12">
								<div class="col-sm-6">
									<div class="col-sm-3 text-right">
										<div class="icon-img">
											<img src="img/icons/running-man.png" class="img-responsive" width="60px" style="display:inline-block;padding-top:15px;">
										</div>
									</div>
									<div class="col-sm-9">
										<div class="icon-txt">
											<h3>Free Trial</h3>
											<p>Log in or register your account.</p>
										</div>
									</div>
								</div>
							</div> -->
							<div class="col-sm-12 text-center dcovr-job">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right mg-lt mg-ldft-resp">
										<div class="icon-img">
											<img src="{{ url( systemImgPath.'/icons/location.png') }}" class="img-responsive">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9 text-left mg-nw-resp">
										<div class="icon-txt">
											<h3>Discover</h3>
											<p>Find the best activities near you, read reviews and ratings, or pick a new skill to master.</p>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 text-center dcovr-job">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right mg-lt mg-ldft-resp">
										<div class="icon-img">
											<img src="{{ url( systemImgPath.'/icons/signup.png') }}" class="img-responsive">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9 text-left mg-nw-resp">
										<div class="icon-txt">
											<h3>Book</h3>
											<p>Classes are available at all times, throughout the week – pick a slot and book your spot instantly.</p>
										</div>
									</div>
								</div>
							</div>

							<div class="col-sm-12 text-center dcovr-job">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right mg-lt mg-ldft-resp">
										<div class="icon-img">
											<img src="{{ url( systemImgPath.'/icons/icon-3.png') }}" class="img-responsive">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9 text-left mg-nw-resp">
										<div class="icon-txt">
											<h3>Get moving!</h3>
											<p>That’s it! In just minutes, you can customize your entire week in advance – then get out there and have some fun!</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</section>
		<section class="all-studios-section text-center">
			<div class="section-heading blue-color">
				<h2>YOUR TRIBE IS CALLING</h2>
				<span class="divider"></span>
				<p>From kayaking to hot yoga, in fitness bootcamps or baking classes – you’ll meet fellow fitness lovers and fun seekers. Join your people!</p>
			</div>
			<div class="container-fluid">				
					@foreach($facilities as $facility)
					<div class="col-lg-2 col-md-3  col-sm-4 col-xs-6 plr-0 plr-1">
                    	<?php
                    		$encrypt_facility_id = base64_encode(convert_uuencode($facility['id'])); 
                    	?>
                        <a href="{{ url('/explore/'.$encrypt_facility_id) }}"> 
	                        <div class="count-details">
	                        	<img src="{{ url(FacilityImgPath.'/'.$facility['image']) }}" alt="" class="img-responsive tribe-sec-img">
	                        	<span class="tribe-img-overlay"></span>
	                            <span class="counter-number">{{ucfirst($facility['name'])}}</span>
	                        	
	                        </div>
                        </a>
                    </div>
                    @endforeach	
				</div>
			</div>
		</section>
     
		<section class="membership-plan-section text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<div class="section-heading blue-color">
							<h2>THE MOST FLEXIBLE MEMBERSHIP EVER</h2>
							<span class="divider"></span>
							<p>Pick a plan and run with it – you can upgrade, downgrade, pause, cancel or resume anytime.</p>
							<p>Billing is month-to-month with no contracts, no hidden fees, no fuss.</p>
							<h3 class="choose-plan-heading">Choose your plan</h3>
							<div class="location-selection-container">
								<span class="location-selection-text">I'm in</span>
								<span class="location-selection-btn-container">
									<div class="dropdown">
										<button class="btn location-selection-btn" type="button" data-toggle="dropdown">Copenhagen
										<span class="caret"></span></button>
										<ul class="dropdown-menu location-selection-btn-menu">
											<li>Coming Soon to Other Locations</li>
										</ul>
									</div>
								</span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<div class="membership-plan-table-container" id="plans_details">
							<div class="row">
								@foreach($plans as $plan)
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
									<div class="membership-plan-wrapper nw-wrap-mrtbt">
										<ul class="membership-plan-list membership-plan-list-data">
											<li class="membership-plan-list-item">
												<strong class="membership-plan-price">{{$plan['currency']}} {{ucfirst($plan['amount'])}}</strong>
											</li>
											<li class="membership-plan-list-item nw-plnlt">
												<p class="membership-plan-session-data">Includes {{ucfirst($plan['credits'])}} credits to book {{ucfirst($plan['sessions'])}} sessions</p>
											</li>
											<li class="membership-plan-list-item nw-plnlt">
												<p class="membership-plan-terms">{!!ucfirst( $plan['description'] )!!}</p>
											</li>
											<li class="membership-plan-list-item">
											<?php if(Auth()->check()&&Auth()->User()->user_type=='U'){?>
			                                    @if($plan['id']==@$plan_detail['plan_id'])
		                                            <a href="{{url('/user/active-plan')}}">
		                                                <button class="btn membership-plan-list-btn BuySubmit" rel="{{$plan['id']}}">Current Plan
		                                                </button>
		                                            </a>    
		                                        @else
		                                        	<button class="btn membership-plan-list-btn BuySubmit" rel="{{$plan['id']}}">Select</button>
		                                        @endif 
			                                <?php }else{ 
			                                    $previous_url=array('previous_url'=>url()->current());
			                                    Session::put('PricingUrl',$previous_url); 
			                                   ?>
			                                    <button class="btn membership-plan-list-btn BuySubmitLogin">Select</button>
			                                <?php }?>
										</li>
										</ul>
									</div>									
								</div>
								@endforeach								
							</div>
						</div>
					</div>
				</div>
				<form id="payment_form" action="https://www.sandbox.paypal.com/webscr" method="post">
			        <input type="hidden" id="email" name="business" value="promatics.sourav-facilitator@gmail.com">
			        <input type="hidden" name="cmd" value="_xclick-subscriptions">
			        <input type="hidden" id="package-name" name="item_name" value="Alice's Weekly Digest">
			        <input type="hidden" name="custom_package" value="true">
			        <input type="hidden" name="item_number" value="OneVay">
			        <!-- <input type="hidden" name="image_url" value="https://pro.subodh.live/onevay/public/images/system/logo-main.png" height="100px"> -->
			        <input id="custom" type="hidden" name="custom" value="">
			        <input type="hidden" name="currency_code" value="" id="currency_code">
			        <input type="hidden" id="amount" name="a3" value="">
			        <input type="hidden" id="month" name="p3" value="1">
			        <input type='hidden' name='notify_url' value="{{url('paypalPaymentSuccess')}}">
			        <input type='hidden' name='cancel_return' value="{{url('/')}}">
			        <input type='hidden' name='return' value="{{url('/explore/activities')}}">
			        <input type="hidden" name="t3" value="M">
			        <input type="hidden" name="src" value="1">
			    </form>
			</div>
		</section>
		@include('frontEnd.common.footer')
	</div>
</div>
@section('scripts')
<!-- scripts files -->
<script type="text/javascript">



	$('#current_location').on('click',function(){
		// alert('enter');
		var location = "{{$country}}";
		$.ajax({
		    type   : 'post',
		    url    : '{{url('get-plan')}}',
		    data   :  {location:location,_token:'{{csrf_token()}}'},
		    success:function(resp){
		    	if(resp=='false'){
		    		swal({
                      title: "We’re not there yet",
                      text: "In future we may available to this location.",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                    $('.getloc').remove();
		    	}else{

	                $('#plans_details').html(resp);
					$('#searchMapInput').val('{{$country}}');
					$('.getloc').remove();
		    	}
             },
		})
	});
</script>
<script type="text/javascript">
	$('#searchMapInput').on('input',function(){
		var search_input = $('#searchMapInput').val();
		// alert(serach_input);
		$.ajax({
		    type   : 'post',
		    url    : '{{url('get-search-location')}}',
		    data   :  {search_input:search_input,_token:'{{csrf_token()}}'},
		    success:function(resp){
		    	// alert(resp);
                $('#location_date').html(resp);
             },
		});   

	});
</script>
<script type="text/javascript">
	$(document).on("click",".getloc",function(){
		// var search_input = $('#searchMapInput').val();
		// alert('aas');
		var location = $(this).attr('location_city');
		// alert(location);
		// alert(serach_input);
		$.ajax({
		    type   : 'post',
		    url    : '{{url('get-plan')}}',
		    data   :  {location:location,_token:'{{csrf_token()}}'},
		    success:function(resp){
		    	// alert(resp);
		    	if(resp=='false'){
		    		swal({
                      title: "We’re not there yet",
                      text: "In future we may available to "+location+".",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                    $('.getloc').remove();
                    $('#searchMapInput').val('{{$country}}');
		    	}else{

			    	$('#searchMapInput').val(location.charAt(0).toUpperCase()+ location.slice(1));
	                $('#plans_details').html(resp);
	                $('.getloc').remove();
		    	}
             },
		});   

	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.hero-slider').slick({
			speed: 300,
			infinite: true,
			autoplay : true,
	      	autoplaySpeed :4000,
	      	slidesToShow: 1,
  			dots:false,
  			arrow: false,
  			fade: true,
			cssEase: 'linear'
		});
	});
</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('.img_sldr').slick({
      	autoplay : true,
      	autoplaySpeed :4000,
      	pauseOnHover : true,
      	slidesToShow: 3,
		adaptiveHeight: true,
		dots:true
      });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
      $('.top-studios-slider').slick({
      	autoplay : true,
      	autoplaySpeed :4000,
      	pauseOnHover : true,
      	slidesToShow: 3,
		adaptiveHeight: true,
		arrow: true,
		responsive: [
				    {
		      breakpoint: 991,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		  }
		 ]
      });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
      // Add smooth scrolling to all links
      	$("#howItWorks").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
	        if (this.hash !== "") {
	          // Prevent default anchor click behavior
	          	event.preventDefault();
	          	var hash = this.hash;
	          	var top_val = $(hash).offset().top;
	          	top_val = top_val - 80;
	          	$('html, body').animate({
	            	scrollTop: top_val
	          	}, 1500);
	        } // End if
      	});
    });
</script>
<script type="text/javascript">
    $(document).on('click','.BuySubmit',function(){
        var plan_id=$(this).attr('rel');
        var checkAuth = "{{(Auth::check())?'true':'false'}}";
        // alert(checkAuth);
        if(checkAuth=='true'){
        	var user_id       = "{{@Auth::user()->id}}";
        	var value= $(this).closest('.nw-wrap-mrtbt').find('.membership-plan-price').html();
        	var array = value.split(" ");
        	// alert(array[1]);
        	$('#currency_code').val(array[0]);
        	$('#amount').val(array[1]);
        	// return false;
        	// alert(plan_id);
       		// 			return false;
        }
        $.ajax({
            type   : 'post',
            url    : '{{url('check-user-plan')}}',
            data   :  {plan_id:plan_id,_token:'{{csrf_token()}}'},
            dataType: "json",
            context:$(this),
            success:function(res){
                if(res.status=='replace'){
                    swal({
                        title: "Your current plan is valid till "+res.valid_till+" and your pending credits are "+res.user_credits+'.',
                        text: "Choose 'Continue' to replace your current plan.",
                       // text :"",	
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        buttons: ['Cancel', 'Continue'],
                    })
                    .then((willDelete) => {
                      if (willDelete) {

                        $('#custom').val(user_id+'?'+plan_id);
                      	$('#payment_form').submit();
                      }
                    });
                    //toastr.success("Your Plan is valid,Are you sure replace your plan");
                }
                if(res.status=='notransaction'){
                    $('#custom').val(user_id+'?'+plan_id);
                    $('#payment_form').submit();
                    //toastr.error("Here's the title!");                        
                }
                if(res.status=='expiry'){
                    $('#custom').val(user_id+'?'+plan_id);
                    $('#payment_form').submit();
                    //toastr.error("Here's the title!");                        
                }
                if(res.status=='false'){ 
                    swal("Somthing Wrong");
                }
            },
            error:function(error){
              //alert('error');
            }
        });
        //window.location.href='{{url('plan-buy')}}'+'/'+btoa(plan_id);                   
    });
</script>
<script type="text/javascript">
    $(document).on('click','.BuySubmitLogin',function(){
        var auth_check = "{{@Auth::check()}}";
    	var user_type = "{{@Auth::user()->user_type}}";
    	// retun false;
    	if(!auth_check){
	        window.location.href = "{{url('/login')}}";
    	}
    	if(auth_check && user_type=='S'){
    	
    		swal({
	            title: "Not Authorized",
	        	text: "You are not authorized to purchase the plan.",
	        	icon: "warning",
	        	buttons: true,
	        	dangerMode: true,
	        });
    	
    	}
    });
</script>

<script>
	$(document).ready(function(){
		$("#downshift-0-menu1").hide();
	  $("#downshift-0-menu").click(function(){
	    $("#downshift-0-menu1").hide();
	  });
	  $("#searchMapInput").click(function(){
	    $("#downshift-0-menu1").show();
	  });
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#join").on('click',function(){
			var user_type = "{{@Auth::user()->user_type}}";
			var auth_check = "{{@Auth::check()}}";
			// alert(auth_check);
			if(auth_check != ''){
				
				var user_plan = "{{@$user_plan->plan_id}}";
				// alert(user_plan);
				
				if((user_plan != null) && (user_type == 'U')){
					// e.preventDefault();
					// var link = $(this).attr('href');
					 swal({
				            title: "You are logged in",
				        	text: "You have signed up for a plan! Now go ahead and explore your options!",
				        	icon: "warning",
				        	dangerMode: true,
				        	buttons:{
				        			cancel: "Cancel",
				        		    Click:{
				        		    	text: "Explore activities",
				        		      	value: "explore", 
				        		    },
				        		    plan: {
				        		      	text: "Switch plan",
				        		      	value: "switch_plan",
				        		    },
				        	},   
						}).then(function(isroll){
								if (isroll){
									if(isroll=='explore'){
									 window.location = "{{url('/explore/activities')}}";
									}else{
										window.location = "{{url('/user/active-plan')}}";
									}
								}
							});	
					
				} 
			}else{
				// alert('in else')	;
				window.location.href = "{{url('/login')}}";
			}	
            
        });
	});
</script>
@endsection
@endsection	