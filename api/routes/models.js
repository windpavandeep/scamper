var Sequelize   = require('sequelize');
var config      = require('../config.json');

const sequelize = new Sequelize(
    config.dev.database,
    config.dev.user,
    config.dev.password, {
        logging: console.log,
        host: 'localhost',
        dialect: 'mysql',
        define: {
            timestamps: false
        },
        operatorsAliases: 'false',
    }
);

exports.Admin = sequelize.define('admin',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        first_name:{type:Sequelize.STRING,defaultValue:null,},
        last_name:{type:Sequelize.STRING,defaultValue:null,},
        email:{type:Sequelize.STRING},
        password:{type:Sequelize.STRING},
        image:{type:Sequelize.STRING,defaultValue:null,},
        contact:{type:Sequelize.BIGINT,defaultValue:null,},
        address:{type:Sequelize.TEXT,defaultValue:null,},
        commission:{
            type:Sequelize.DOUBLE,
            defaultValue:null,
        },
        gst:{type:Sequelize.DOUBLE(10,2)},
        security_code:{
            type:Sequelize.STRING,
            defaultValue:null,
        },
        created_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },
        updated_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },
    }, {tableName:'admin'}
);

exports.User = sequelize.define('users',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        first_name:{type:Sequelize.STRING},
        last_name:{type:Sequelize.STRING},
        dob:{type:Sequelize.STRING},
        image:{type:Sequelize.STRING},
        email:{type:Sequelize.STRING},
        fb_unique_id:{
            type:Sequelize.STRING,
            defaultValue:null
        },
        password:{type:Sequelize.STRING},
        gender:{type:Sequelize.ENUM('Male','Female')},
        city_id:{type:Sequelize.BIGINT},
        state_id:{type:Sequelize.BIGINT},
        country_id:{type:Sequelize.BIGINT},
        contact:{type:Sequelize.STRING},
        pincode:{type:Sequelize.BIGINT},
        exam_preparation_id:{
            type:Sequelize.BIGINT,
            defaultValue:null
        },
        status:{type:Sequelize.ENUM('A','I')},
        user_type:{type:Sequelize.ENUM('user','trainer')},
        remember_token:{type:Sequelize.STRING,defaultValue:null},
        security_code:{
            type:Sequelize.STRING,
            defaultValue:null
        },
        added_by:{type:Sequelize.ENUM('self','admin')},
        verified_account:{
            type:Sequelize.ENUM('yes','no'),
            defaultValue:'no'
        },
        district:{
            type:Sequelize.STRING,
            defaultValue:null
        },
        address:{
            type:Sequelize.TEXT,
            defaultValue:null
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },
        updated_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },

    },{tableName:'users'}
);
exports.Trainer = sequelize.define('trainers',{
        id:{
           type:Sequelize.BIGINT,
           primaryKey:true,
           autoIncrement:true
        },
        pincode:{
            type:Sequelize.BIGINT
        },
        employment_status_id :{
            type:Sequelize.BIGINT
        },
        organization_name :{
            type:Sequelize.STRING
        },
        experience:{
            type:Sequelize.BIGINT //in years
        },
        trainer_domain_id :{
            type:Sequelize.BIGINT
        },
        subject_id :{
            type:Sequelize.BIGINT
        },
        admin_approval:{
           type:Sequelize.ENUM('A','P','R'),
           defaultValue:'P'
        },
        signup_rejection_reason:{
            type:Sequelize.TEXT
        },
        ratings:{
            type:Sequelize.DOUBLE
        },
        created_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },
        updated_at:{
            type:Sequelize.DATE,
            // defaultValue:Sequelize.NOW(0)
        },
    },{tableName:'trainers'}
);

exports.Country = sequelize.define('countries',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{type:Sequelize.STRING},
        sortname:{type:Sequelize.STRING},
        isd_code:{type:Sequelize.STRING},
    }, {tableName:'countries'}
);
exports.State = sequelize.define('states',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{type:Sequelize.STRING},
        country_id:{type:Sequelize.BIGINT},
    }, {tableName:'states'}
);
exports.City = sequelize.define('cities',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{type:Sequelize.STRING},
        state_id:{type:Sequelize.BIGINT},
    }, {tableName:'cities'}
);

exports.Domain = sequelize.define('trainer_domains',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{type:Sequelize.STRING},
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'trainer_domains'}
);

exports.EmploymentStatus = sequelize.define('employment_status',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{type:Sequelize.STRING},
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'employment_status'}
);
exports.Content = sequelize.define('content',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        contact_no:{type:Sequelize.STRING},
        email:{type:Sequelize.STRING},
        address:{
            type:Sequelize.TEXT
        },
        description:{
            type:Sequelize.TEXT
        },
        type:{

            type:Sequelize.ENUM('H','F','P','T','D'),
            defaultValue:'H'
  
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'content'}
);

exports.Content = sequelize.define('content',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        contact_no:{type:Sequelize.STRING},
        email:{type:Sequelize.STRING},
        address:{
            type:Sequelize.TEXT
        },
        description:{
            type:Sequelize.TEXT
        },
        type:{

            type:Sequelize.ENUM('H','F','P','T','D'),
            defaultValue:'H'
  
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'content'}
);

exports.SubscriptionPlan = sequelize.define('subscription_plans',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        trainer_id:{
            type:Sequelize.BIGINT
        },
        title:{type:Sequelize.STRING},
        price:Sequelize.DOUBLE(10,2),
        validity:{

            type:Sequelize.BIGINT
  
        },
        description:{
            type:Sequelize.TEXT
        },
        admin_approval:{

            type:Sequelize.ENUM('A','P','R'),
            defaultValue:'P'
  
        },
        rejection_reason:{
            type:Sequelize.TEXT,
            defaultValue:null
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'subscription_plans'}
);


exports.TrainerContent = sequelize.define('trainer_contents',{
        id:{
           type:Sequelize.BIGINT,
           primaryKey:true,
           autoIncrement:true
        },
        trainer_id:{
            type:Sequelize.BIGINT
        },
        category_id :{
            type:Sequelize.BIGINT
        },
        sub_category_id :{
            type:Sequelize.BIGINT
        },
        sub_subcategory_id :{
            type:Sequelize.BIGINT
        },
        subject_id:{
            type:Sequelize.BIGINT 
        },
        unit_id  :{
            type:Sequelize.BIGINT
        },
        title :{
            type:Sequelize.TEXT
        },
        file:{
            type:Sequelize.STRING,
            defaultValue:null,
        },
        upload_type:{
           type:Sequelize.ENUM('pdf','video'),
        },
        content_availability :{
            type:Sequelize.ENUM('paid','free','subscriber'),
        },
        paid_amount  :{
            type:Sequelize.DOUBLE(10,2),
            defaultValue:null,
        },
        gst  :{
            type:Sequelize.DOUBLE(10,2),
            defaultValue:null,
        },
        gst_price  :{
            type:Sequelize.DOUBLE(10,2),
        },
        final_price  :{
            type:Sequelize.DOUBLE(10,2),
            defaultValue:null, 
        },
        language_id  :{
            type:Sequelize.BIGINT
        },
        start_date  :{
            type:Sequelize.STRING,
            defaultValue:null, 
        },
        end_date :{
            type:Sequelize.STRING,
            defaultValue:null, 
            // defaultValue:Sequelize.NOW(0)
        },
        no_of_exams :{
            type:Sequelize.STRING,
            defaultValue:null, 
            // defaultValue:Sequelize.NOW(0)
        },
        free_question :{
            type:Sequelize.STRING,
            defaultValue:null, 
        },
        include_gst :{
            type:Sequelize.STRING,
            defaultValue:null, 
        },
        subscriber_video :{
            type:Sequelize.STRING,
            defaultValue:null, 
        },
        show_answer_after_exam  :{
            type:Sequelize.STRING,
            defaultValue:null, 
        },
        description  :{
            type:Sequelize.TEXT,
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    },{tableName:'trainer_contents'}
);

exports.TrainerContentFile = sequelize.define('trainer_content_files',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        trainer_content_id:{type:Sequelize.BIGINT},
        file:{
            type:Sequelize.STRING
        },
        type:Sequelize.ENUM('video','pdf'),
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'trainer_content_files'}
);


exports.TrainerContentImage = sequelize.define('trainer_content_images',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        trainer_content_id:{type:Sequelize.BIGINT},
        name:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'trainer_content_images'}
);

exports.Category = sequelize.define('categories',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{
            type:Sequelize.STRING
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'categories'}
);
exports.SubCategory = sequelize.define('sub_categories',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        category_id:{
            type:Sequelize.BIGINT,
        },
        name:{
            type:Sequelize.STRING
        },
        place_footer:{
            type:Sequelize.ENUM('Yes','No'),
            defaultValue:null,
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'sub_categories'}
);

exports.SubSubCategory = sequelize.define('sub_sub_categories',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        sub_category_id:{
            type:Sequelize.BIGINT,
        },
        name:{
            type:Sequelize.STRING
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'sub_sub_categories'}
);

exports.ContactUs = sequelize.define('contact_us',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        first_name:{
            type:Sequelize.STRING
        },
        last_name:{
            type:Sequelize.STRING
        },
        email:{
            type:Sequelize.STRING
        },
        contact_no:{
            type:Sequelize.BIGINT,
        },
        enquiry:{
            type:Sequelize.TEXT,
        },
        reply_status:{
            type:Sequelize.ENUM('P','R'),
            defaultValue:'P'
        },
        reply:{
            type:Sequelize.TEXT,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'contact_us'}
);

exports.Faq = sequelize.define('faqs',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        title:{
            type:Sequelize.STRING
        },
        description:{
            type:Sequelize.TEXT,
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'faqs'}
);

exports.FreeCounselling = sequelize.define('free_counsellings',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{
            type:Sequelize.STRING
        },
        email:{
            type:Sequelize.STRING
        },
        contact_no:{
            type:Sequelize.BIGINT
        },
        category_id:{
            type:Sequelize.BIGINT
        },
        sub_category_id:{
            type:Sequelize.BIGINT
        },
        sub_sub_category_id:{
            type:Sequelize.BIGINT,
            defaultValue:null
        },
        state_id:{
            type:Sequelize.BIGINT
        },
        description:{
            type:Sequelize.TEXT,
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'free_counsellings'}
);

exports.Transaction = sequelize.define('transactions',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT
        },
        course_id:{
            type:Sequelize.BIGINT
        },
        subscription_plan_id:{
            type:Sequelize.BIGINT
        },
        razor_pay_id:{
            type:Sequelize.STRING
        },
        discount_coupon:{
            type:Sequelize.STRING,
            defaultValue:null,
        },
        refer_code:{
            type:Sequelize.STRING
        },
        gst:{
            type:Sequelize.DOUBLE(10,2),
        },
        price:{
            type:Sequelize.DOUBLE(10,2),
        },
        discount_amount:{
            type:Sequelize.DOUBLE(10,2),
        },
        purchased_on:{
            type:Sequelize.DATE
        },
        final_total:{
            type:Sequelize.DOUBLE(10,2),
        },
        valid_till:{
            type:Sequelize.STRING
        },
        paid_status :{
            type:Sequelize.ENUM('Y','N'),
            defaultValue:'N'
        },
        paid_on:{
            type:Sequelize.STRING,
            defaultValue:null,
        },
        trainer_price :{
            type:Sequelize.DOUBLE,
        },
        admin_commission:{
            type:Sequelize.DOUBLE,
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'transactions'}
);

exports.DiscountCoupon = sequelize.define('discount_coupons',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        coupon_code:{
            type:Sequelize.STRING
        },
        discount_type:{
            type:Sequelize.ENUM('P','V'),
        },
        discount_amount :{
            type:Sequelize.DOUBLE(10,2),
        },
        uses_per_user_limit:{
            type:Sequelize.BIGINT,
        },
        start_date:{
            type:Sequelize.STRING
        },
        end_date:{
            type:Sequelize.STRING
        },
        status:{
            type:Sequelize.ENUM('A','I'),
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'discount_coupons'}
);

exports.RefferalCode = sequelize.define('referral_codes',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        coordinator_id:{
            type:Sequelize.STRING
        },
        pincode:{
            type:Sequelize.BIGINT,
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'referral_codes'}
);

exports.BannerImage = sequelize.define('banner_images',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        category_id:{
            type:Sequelize.BIGINT,
        },
        sub_category_id:{
            type:Sequelize.BIGINT,
        },
        sub_sub_category_id:{
            type:Sequelize.BIGINT,
        },
        image:{
            type:Sequelize.STRING
        },
        course_id:{
            type:Sequelize.BIGINT,
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'banner_images'}
);

exports.UserDevice = sequelize.define('user_devices',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT
        },
        device_id:{
            type:Sequelize.BIGINT,
        },
        device_token:{
            type:Sequelize.BIGINT,
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'user_devices'}
);

exports.Chat = sequelize.define('chats',
    {
        id:{
            type: Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement: true,
        },
        room_id:{
            type: Sequelize.STRING
        },
        sender_id:{
            type: Sequelize.INTEGER
        },
        message:{
            type: Sequelize.TEXT
        },
        messages_type:{
            type: Sequelize.ENUM('text','image','media','video'),
            defaultValue:'text'
        },
        date:{
            type: Sequelize.STRING
        },
        seen:{
            type: Sequelize.ENUM('yes','no')
        },
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type: Sequelize.DATE,
            defaultValue:Sequelize.NOW
        },
        updated_at:{
            type:  Sequelize.DATE,
            defaultValue:Sequelize.NOW
        }
    },{ tableName:'chats' }
);

exports.ChatRoom = sequelize.define('chat_rooms',
    {
        id:{
            type: Sequelize.INTEGER,
            primaryKey:true
        },
        room_id:{
            type: Sequelize.STRING
        },
        sender_id:{
            type: Sequelize.INTEGER
        },
        receiver_id:{
            type: Sequelize.INTEGER
        },  
        last_message:{
            type: Sequelize.TEXT
        },
        last_message_time:{
            type: Sequelize.STRING
        },
        created_at:{
            type: Sequelize.DATE,
            defaultValue:Sequelize.NOW
        },
        updated_at:{
            type:  Sequelize.DATE,
            defaultValue:Sequelize.NOW
        }
        
    },{tableName:'chat_rooms'}
);

exports.WalkinContent = sequelize.define('walkin_contents',{
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        title:{
            type:Sequelize.STRING
        },
        image:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'walkin_contents'}
);

exports.Subject = sequelize.define('subjects',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        category_id:{
            type:Sequelize.BIGINT,
        },
        sub_category_id:{
            type:Sequelize.BIGINT,
        },
        sub_sub_category_id:{
            type:Sequelize.BIGINT,
        },
        name:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'subjects'}
);

exports.Unit = sequelize.define('units',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        subject_id:{
            type:Sequelize.BIGINT,
        },
        name:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'units'}
);

exports.Language = sequelize.define('languages',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'languages'}
);

exports.RecentSearches = sequelize.define('recent_searches',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT,
        },
        course_id:{
            type:Sequelize.BIGINT
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'recent_searches'}
);
exports.UserCourse = sequelize.define('user_courses',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT,
        },
        category_id:{
            type:Sequelize.BIGINT
        },
        sub_category_id:{
            type:Sequelize.BIGINT
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'user_courses'}
);

exports.Exam = sequelize.define('exam',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        language_id:{
            type:Sequelize.BIGINT,
        },
        categories_id:{
            type:Sequelize.BIGINT
        },
        sub_categories_id:{
            type:Sequelize.BIGINT
        },
        sub_sub_categories_id:{
            type:Sequelize.BIGINT
        },
        name:{
            type:Sequelize.STRING
        },
        code:{
            type:Sequelize.TEXT
        },
        exam_validity:{
            type: Sequelize.ENUM('Y','N')
        },
        start_date:{
            type:Sequelize.STRING
        },
        expiry_date:{
            type:Sequelize.STRING
        },
        start_time:{
            type:Sequelize.BIGINT
        },
        end_time:{
            type:Sequelize.BIGINT
        },
        total_questions:{
            type:Sequelize.BIGINT
        },
        total_marks:{
            type:Sequelize.DOUBLE
        },
        negative_marking:{
            type:Sequelize.DOUBLE
        },
        trams_condition:{
            type:Sequelize.TEXT
        },
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'exam'}
);

exports.Question = sequelize.define('questions',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        exam_id:{
            type:Sequelize.BIGINT,
        },
        subject_id:{
            type:Sequelize.BIGINT
        },
        unit_id:{
            type:Sequelize.BIGINT
        },
        question:{
            type:Sequelize.TEXT
        },
        ques_image:{
            type:Sequelize.STRING
        },
        answer_explanation:{
            type:Sequelize.TEXT
        },
        ans_image:{
            type:Sequelize.STRING
        },
        correct_answer:{
            type:Sequelize.BIGINT
        },
        marks:{
            type:Sequelize.DOUBLE
        },
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'questions'}
);

exports.Option = sequelize.define('options',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        question_id:{
            type:Sequelize.BIGINT,
        },
        options:{
            type:Sequelize.TEXT
        },
        option_image:{
            type:Sequelize.STRING
        },
        position:{
            type:Sequelize.BIGINT
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'options'}
);

exports.Set = sequelize.define('sets',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{
            type:Sequelize.STRING,
        },
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'sets'}
);

exports.QuestionSet = sequelize.define('question_sets',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        exam_id:{
             type:Sequelize.BIGINT,
        },
        question_id:{
             type:Sequelize.BIGINT,
        },
        set_id:{
             type:Sequelize.BIGINT,
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'question_sets'}
);

exports.UserExam = sequelize.define('user_exams',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT,
        },
        exam_id:{
            type:Sequelize.BIGINT
        },
        exam_name:{
            type:Sequelize.TEXT
        },
        start_date_time:{
            type:Sequelize.DATE
        },
        end_date_time:{
            type:Sequelize.STRING
        },
        total_question:{
            type:Sequelize.STRING
        },
        duration:{
            type:Sequelize.STRING
        },
        total_marks:{
            type:Sequelize.DOUBLE
        },
        finish_status:{
            type: Sequelize.ENUM('pending','finish','not_attempt'),
            defaultValue:'pending'
        },
        total_attempt:{
            type:Sequelize.BIGINT
        },
        marks_obtained:{
            type:Sequelize.DOUBLE
        },
        set_name:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'user_exams'}
);

exports.UserExamQuestion = sequelize.define('user_exam_questions',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_exam_id:{
            type:Sequelize.BIGINT,
        },
        question:{
            type:Sequelize.TEXT
        },
        question_image:{
            type:Sequelize.STRING
        },
        answer_explanation:{
            type:Sequelize.TEXT
        },
        answer_explanation_image:{
            type:Sequelize.STRING
        },
        correct_answer:{
            type:Sequelize.BIGINT
        },
        given_answer:{
            type:Sequelize.BIGINT
        },
        marks:{
            type:Sequelize.DOUBLE
        },
        ans_status:{
           type: Sequelize.ENUM('true','false','not_attempt'), 
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'user_exam_questions'}
);

exports.UserExamQuestionOption = sequelize.define('user_exam_question_options',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_exam_question_id:{
            type:Sequelize.BIGINT,
        },
        options:{
            type:Sequelize.TEXT
        },
        option_image:{
            type:Sequelize.STRING
        },
        position:{
            type:Sequelize.BIGINT
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'user_exam_question_options'}
);

exports.TrainerContentSubject = sequelize.define('trainer_content_subjects',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        trainer_content_id:{
            type:Sequelize.BIGINT,  
        },
        subject_id:{
            type:Sequelize.BIGINT,  
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'trainer_content_subjects'}
);

exports.TrainerContentSubjectUnit = sequelize.define('trainer_content_subject_units',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        trainer_content_subject_id:{
            type:Sequelize.BIGINT,
        },
        unit_id:{
            type:Sequelize.BIGINT,  
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'trainer_content_subject_units'}
);

exports.QuestionBank = sequelize.define('question_banks',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        category_id:{
            type:Sequelize.BIGINT,
        },
        sub_category_id:{
            type:Sequelize.BIGINT,
        },
        sub_sub_category_id :{
            type:Sequelize.BIGINT,
        },
        subject_id :{
            type:Sequelize.BIGINT,
        },
        unit_id :{
            type:Sequelize.BIGINT,
        },
        question:{
            type:Sequelize.TEXT
        },
        ques_image:{
            type:Sequelize.STRING
        },
        answer_explanation:{
            type:Sequelize.TEXT
        },
        correct_answer:{
            type:Sequelize.BIGINT
        },
        marks:{
            type:Sequelize.DOUBLE
        },
     
        deleted_at:{
            type: Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'question_banks'}
);

exports.QuestionBankOption = sequelize.define('question_bank_options',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        question_bank_id:{
            type:Sequelize.BIGINT,
        },
        options:{
            type:Sequelize.TEXT
        },
        option_image:{
            type:Sequelize.STRING
        },
        position:{
            type:Sequelize.BIGINT
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'question_bank_options'}
);


exports.Notification = sequelize.define('notifications',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT,
        },
        title:{
            type:Sequelize.TEXT
        },
        type:{
            type:Sequelize.TEXT
        },
        seen:{type:Sequelize.ENUM('yes','no')},
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'notifications'}
);

exports.Download = sequelize.define('downloads',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        name:{
            type:Sequelize.TEXT,
        },
        file:{
            type:Sequelize.STRING
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'downloads'}
);

exports.AdvertisementPopups = sequelize.define('advertisement_popups',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        title:{
            type:Sequelize.STRING,
        },
        description:{
            type:Sequelize.TEXT
        },
        url:{
            type:Sequelize.STRING,
        },
        image:{
            type:Sequelize.STRING,
        },
        status:{
            type:{type:Sequelize.ENUM('A','I')},
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'advertisement_popups'}
);

exports.Rating = sequelize.define('ratings',
    {
        id:{
            type:Sequelize.BIGINT,
            primaryKey:true,
            autoIncrement:true
        },
        user_id:{
            type:Sequelize.BIGINT,
        },
        teacher_id:{
            type:Sequelize.BIGINT
        },
        ratings:{
            type:Sequelize.INTEGER
        },
        message:{
            type:Sequelize.TEXT,
        },
        deleted_at:{
            type:Sequelize.DATE,
            defaultValue:null
        },
        created_at:{
            type:Sequelize.DATE,
        },
        updated_at:{
            type:Sequelize.DATE,
        },
    }, {tableName:'ratings'}
);