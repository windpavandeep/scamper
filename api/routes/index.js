var Sequelize 		= require('sequelize');
var bcrypt 			= require('bcrypt');
const Op 			= Sequelize.Op;
var models 			= require('./models.js');
var format      	= require('date-format');
var nodemailer  	= require('nodemailer');
var fcm 			= require('fcm-notification') ;
var fcm_private_key = require('./fcm_private_key.json');
var FCM         	= new fcm(fcm_private_key);
var fs          	= require("fs");
// CONSTANTS
const REQUIRED_FIELDS_ERROR = 'Please enter all the required fields';
const clientEmail  			= 'scamperskills@gmail.com';
const clientEmailPass 		= "Tripura@2014";
const COMMON_ERROR 			= 'Some error occured. Please try again later after sometime';
const LogoImgPath 			= "http://scamperskills.com/public/images/system/logo.png";
const TriangleImgPath 		= "http://scamperskills.com/public/images/system/ptrn1.jpg";
var studentImagePath 	    = '/var/www/html/public/images/profile/student/';
var teacherImagePath 		= '/var/www/html/public/images/profile/trainer/';
var CourseImagePath         = '/var/www/html/public/images/trainerContents/trainerContentImage/';
var CourFilePath            = '/var/www/html/public/images/trainerContents/';
// --- email
var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: clientEmail,
		pass: clientEmailPass
	}
});

var sendMail = function (toAddress, subject, content, next) {
    var mailOptions = {
        from: "Scamper Skills <" + clientEmail + ">",
        to: toAddress,
        replyTo: clientEmail,
        subject: subject,
        html: content
    };
    console.log('---ok--');
    transporter.sendMail(mailOptions)
};


function push_notification(user_id,notification_type,notification_id,notification_title,notification_body){

    var token = [];
    models.User.findOne({
    	where:{
    		id:user_id
    	},
    	attributes:['notifications']
    }).then(user_data=>{
    	if(user_data.notifications == 'on'){
			models.UserDevice.findAll({
				where:{
					user_id:user_id
				}
			}).then(devices=>{
				// console.log(devices);

				devices.forEach(function(val, ind){
					token.push(val.device_token);
					// var token = val.device_token;
					// console.log('token ======== ', token);

					if(ind == (devices.length-1)){
						var message = {
							notification:{
								title:notification_title,
								body:notification_body
							},
							data: {    //This is only optional, you can send any data
					            type: notification_type,
					            id: notification_id.toString()
					        },
							token:token
						}

						// console.log('message ', message);
						// FCM.send(message, function(err, response){
						FCM.sendToMultipleToken(message, token, function(err, response) {
							if(err){
								console.log('error found ',err);
							} else{
								// console.log('response ', response);
								console.log('response ', 'push notification sent');
							}
						});
					}
				});
			})
    	}
    })
}
// ------------ associations start

models.TrainerContent.hasMany(models.TrainerContentImage,{
	foreignKey:"trainer_content_id",
	as:"TrainerContentImage"
});

models.TrainerContent.hasMany(models.TrainerContentFile,{
	foreignKey:"trainer_content_id",
	as:"TrainerContentFile"
});

/*models.TrainerContent.hasMany(models.TrainerContentFile,{
	foreignKey:"trainer_content_id",
	as:"TrainerContentFile"
});*/

models.ChatRoom.belongsTo(models.User,{
	foreignKey:'sender_id',
	as:'Sender'
});

models.ChatRoom.belongsTo(models.User,{
	foreignKey:'receiver_id',
	as:'Receiver'
});


models.Transaction.belongsTo(models.User,{
	foreignKey:'user_id',
	as:'UserSubscribedTeacher'
});

models.User.belongsTo(models.Trainer,{
	foreignKey:'id',
	as:'teacher_details'
});
/*models.ChatRoom.hasMany(models.Chat,{
	foreignKey:'room_id',
	sourceKey:'room_id',
	as:'Chat'
});*/

models.ChatRoom.hasOne(models.Chat,{
	foreignKey:'room_id',
	sourceKey:'room_id',
	as:'Chat'
});

models.ChatRoom.hasMany(models.Chat,{
	foreignKey:'room_id',
	sourceKey:'room_id',
	as:'UnseenChat'
});

models.Chat.belongsTo(models.User,{
	foreignKey:'sender_id',
});

models.Question.hasMany(models.Option,{
	foreignKey:'question_id',
});

models.Question.hasMany(models.QuestionSet,{
	foreignKey:'question_id',
});

models.QuestionSet.belongsTo(models.Set,{
	foreignKey:'set_id',
});

models.User.hasOne(models.UserCourse,{
	foreignKey:'user_id',
	as:'user_category'
});

models.UserCourse.belongsTo(models.Category,{
	foreignKey:'category_id'
});
models.User.belongsTo(models.Country,{
	foreignKey:'country_id'
});
models.User.belongsTo(models.State,{
	foreignKey:'state_id'
});

models.User.belongsTo(models.City,{
	foreignKey:'city_id'
});

models.Exam.hasMany(models.Question,{
	foreignKey:'exam_id',
});

models.UserExam.hasMany(models.UserExamQuestion,{
	foreignKey:'user_exam_id',
	as:'exam_detail'
});

models.UserExam.belongsTo(models.User,{
	foreignKey:'user_id',
	as:'user_details'
});

models.Transaction.belongsTo(models.TrainerContent,{
	foreignKey:'course_id',
	targetKey:'id',
	as:'purchased_content'
});
/*models.UserExamQuestion.belongsTo(models.UserExamQuestionOption,{
	foreignKey:'id',
});*/
// models.UserExam.hasMany(models.UserExamQuestion,{
// 	foreignKey:'user_exam_id',
// 	as:'exam_detail'
// });
models.UserExamQuestion.hasMany(models.UserExamQuestionOption,{
	foreignKey:'user_exam_question_id',
});


models.Transaction.belongsTo(models.SubscriptionPlan,{
	foreignKey:'subscription_plan_id',
	as:"subscribed_plan",
	sourceKey:'subscription_plan_id',
});
//End
models.Trainer.hasMany(models.SubscriptionPlan,{
	foreignKey:'trainer_id'
}); 


models.SubscriptionPlan.hasMany(models.Transaction,{
	foreignKey:'subscription_plan_id',
	sourceKey:'id',
}); 
models.Trainer.hasMany(models.TrainerContent,{
	foreignKey:'trainer_id',
	sourceKey:'id',
}); 

models.TrainerContent.hasMany(models.Transaction,{
	foreignKey:'course_id',
	sourceKey:'id',
}); 

models.BannerImage.belongsTo(models.TrainerContent,{
	foreignKey:'course_id',
	sourceKey:'id',
}); 

models.BannerImage.belongsTo(models.TrainerContent,{
	foreignKey:'course_id',
	sourceKey:'id',
});

models.TrainerContent.hasMany(models.TrainerContentSubject,{
	foreignKey:'trainer_content_id',
	sourceKey:'id',
});

models.TrainerContentSubject.hasMany(models.TrainerContentSubjectUnit,{
	foreignKey:'trainer_content_subject_id',
	sourceKey:'id',
});

models.TrainerContentSubjectUnit.belongsTo(models.Unit,{
	foreignKey:'unit_id',
	sourceKey:'id',
});

models.TrainerContentSubject.belongsTo(models.Subject,{
	foreignKey:'subject_id',
	sourceKey:'id',
});


models.TrainerContent.belongsTo(models.Subject,{
	foreignKey:'subject_id',
	sourceKey:'id',
});

models.TrainerContent.belongsTo(models.Unit,{
	foreignKey:'unit_id',
	sourceKey:'id',
});

models.QuestionBank.hasMany(models.QuestionBankOption,{
	foreignKey:'question_bank_id',
	// sourceKey:'id',
});
// models..belongsTo(models.TrainerContent,{
// 	foreignKey:'course_id',
// 	sourceKey:'id',
// }); 

models.UserExam.belongsTo(models.Exam,{
	foreignKey:'exam_id',
});
models.Exam.hasMany(models.QuestionSet,{
	foreignKey:'exam_id',
	as:'exam_sets'
});

models.Rating.belongsTo(models.User,{
	foreignKey:'user_id',
	// as:'exam_sets'
});

models.Transaction.belongsTo(models.User,{
	foreignKey:'user_id',
	// sourceKey:'id',
}); 

models.Exam.belongsTo(models.Category,{
	foreignKey:'categories_id',
});
exports.index = function(req,res){
	console.log('in function');
}
exports.signup = function(req,res){
	if(req.body.first_name == '' || req.body.contact == '' || req.body.email == '' 
		|| req.body.password == '' || req.body.user_type == ''|| req.body.gender == ''

		|| req.body.state_id=='' || req.body.city_id==''	|| req.body.country_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		var email     = req.body.email;
		var contact   = req.body.contact;
		var user_type = req.body.user_type;
		console.log(email);
		var fb_unique_id = '';
		if(req.body.fb_unique_id){
			fb_unique_id = req.body.fb_unique_id;
		}
		models.User.count({
			where:{
				email:email,
				deleted_at:null
			}
		}).then(check_email=>{
			if(fb_unique_id!=''){
				models.User.count({
					where:{
						fb_unique_id:fb_unique_id,
						email:email,
						deleted_at:null
					}
				}).then(check_fd_id=>{
					if(check_fd_id >'0'){
						res.send({
							success:0,
							message:"This fb id is already registered"
						})
					}
				})
			}
			if(check_email >'0'){
				res.send({
					success:0,
					message:"This email-id is already registered"
				})
			}else{

				models.User.count({
					where:{
						contact:contact,
						deleted_at:null
					}
				}).then(check_contact=>{
					if(check_contact > '0'){
						res.send({
							success:0,
							message:"This mobile number is already registered"
						})
					} else{
						var password = req.body.password;
						var hashPassword = '';
						var salt = bcrypt.genSaltSync(10);
						hashPassword = bcrypt.hashSync(password,salt);
						// console.log(salt);
						// console.log(hashPassword);

						req.body.password   = hashPassword;
						req.body.status 	= 'A';
						req.body.added_by   = 'self';
						req.body.verified_account   = 'yes';
						req.body.created_at = new Date();
						req.body.updated_at = new Date();
						if(user_type=='user'){
							req.body.category_id 	 = req.body.category_id;
							req.body.sub_category_id = req.body.sub_category_id;
							

								models.User.create(req.body).then(data=>{
									req.body.user_id = data.id;
									models.UserCourse.create(req.body).then(user_category=>{

										res.send({
											success:1,
											result:[data,user_category],
											message:"Signup done successfully"
										})
									});
								})
							
						}else if(user_type=='trainer'){
							models.User.create({

						        first_name:req.body.first_name,
						        last_name:req.body.last_name,
						        email:email,
						        fb_unique_id:fb_unique_id,
						        contact:req.body.contact,
						        dob:req.body.dob,
						        password:req.body.password,
						        gender:req.body.gender,
						        user_type:req.body.user_type,
						        country_id:req.body.country_id,
						        state_id:req.body.state_id,
						        city_id:req.body.city_id,
						        address:address,
						        district:district,
						        added_by:'self',
						        verified_account:'no',
						        status:'A',
						        created_at:req.body.created_at,
						        updated_at:req.body.updated_at
							}).then(user=>{
							    models.Trainer.create({
							        id:user.id,
							        pincode:req.body.pincode,
							        employment_status_id:req.body.employment_status_id,
							        experience:req.body.experience,
							        organization_name:req.body.organization_name,
							        trainer_domain_id:req.body.trainer_domain_id,
							        subject_id:req.body.subject_id,
							        admin_approval:'P',
							        created_at:req.body.created_at,
							        updated_at:req.body.updated_at
							    }).then(data=>{
									res.send({
										success:1,
										data:{
                                        	user_detail:user,
                                        	trainer_detail:data,
                                    	},
										message:"Signup done successfully"
									})
								})

							})
						}else{
							res.send({
								success:0,
								message:"Invalid user"
							})
						}
					}
				})
			}
		})
	}
}

exports.forgot_password = function(req,res){
	if(req.body.email == '' || req.body.email == null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		var email = req.body.email;
		models.User.findOne({
			where:{
				email:email
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"This email-id is not registered"
				})
			} else{
				var hashPassword = '';
				var rand = Math.floor(Math.random() * 899999 + 100000);
				rand = rand.toString();
				// console.log('rand---------');
				// console.log(rand);
				
				var salt = bcrypt.genSaltSync(10);
				hashPassword = bcrypt.hashSync(rand,salt);

				data.update({
					password:hashPassword
				}).then(updated=>{
					// var email_html = 'Your new password for your PRY account is '+rand;
					var email_html = `<!DOCTYPE html>
										<html>
										    <head>
										        <title>Scamper Skills Forgot Password</title>
										        <style type="text/css">
										            body{font-family: 'open-sans', sans-serif;color: #77798c;font-size: 14px;}
										        </style>
										    </head>
										    <body>        
										        <table cellspacing="0" cellpadding="0" width="100%" style="padding: 0;">
										            <tbody>
										                <tr>
										                    <td>
										                        <table cellpadding="0" cellspacing="0" width="500px" style="margin: 0px auto 0px; text-align: center; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); border-radius: 4px; background-image: url('`+TriangleImgPath+`');     background-size: cover; background-position: center center; padding: 40px;" >
										                            <tbody>
										                                <tr>
										                                    <td>
										                                        <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; text-align: center; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); border-radius: 4px;" bgcolor="#fff" >
										                                            <tbody>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="background-color: #eee; padding: 40px;border-radius: 4px 4px 0 0;">
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td style="text-align:center;">
										                                                                        <img src="`+LogoImgPath+`" alt="HiFi" width="250px" />
										                                                                        <h1 style="font-weight:normal;margin:20px 0 0;color: #5693d4;"> Forgot Password </h1>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="margin:0 auto;padding:10px;"> 
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td>
										                                                                       <h4 style="text-align:left;"> Hello `+data.first_name+` </h4>
										                                                                       <p style="/*letter-spacing:1px;*/line-height:30px;text-align:left;">
										                                                                        We got a request to reset your password. Your new account password is: `+rand+`
										                                                                        </p>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="border-top:1px solid #ddd; margin-top:10px;border-radius:0 0 4px 4px;"> 
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td>
										                                                                        <p style="line-height:30px;">Copyright &copy; 2019 PRY. All rights reserved. </p>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                            </tbody>
										                                        </table>
										                                    </td>
										                                </tr>                                
										                            </tbody>
										                        </table>
										                    </td>
										                </tr>
										            </tbody>
										        </table>
										    </body>
										</html>`;
					sendMail(email, 'Scamper Skills Forgot Password', email_html, function (err, res) {
				        if (err) {
				            console.log('ERROR!');
				        } else{
				        	console.log("Email sent!");
				        }
				    });
				    res.send({
				    	success:1,
				    	message:"Password reset successfully. We have e-mailed you."
				    })
				})
			}
		})
	}
}

exports.login = function(req,res){
	if(req.body.email_contact == '' || req.body.password == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				[Op.or]:[
					{
						email:req.body.email_contact
					},
					{
						contact:req.body.email_contact
					}
				],
				deleted_at:null,
				
			},
			include:[{
				model:models.UserCourse,
				as:'user_category'
			}],
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Account doesn't exists"
				})
			} else{
				if(data.verified_account=='yes')
					if(data.status == 'A'){
						if(bcrypt.compareSync(req.body.password,data.password) ){
							var full_name = data.first_name+' '+data.last_name;
							res.send({
								success:1,
								result:data,
								message:"Welcome "+full_name
							})
						} else{
							res.send({
								success:0,
								message:"Incorrect email/contact and password combination"
							})
						}
					} else{
						res.send({
							success:0,
							message:"Your account is not active. Please contact to admin for more information"
						})
					}
				else{
					res.send({
						success:0,
						message:"Your account is not approved by admin. Please contact to admin for more information"
					})
					
				}
			}
		})
	}
}

exports.check_email_exist = function(req, res){
	if(req.body.email == '' ){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				[Op.or]:[
					{
						email:req.body.email
					}
				],
				deleted_at:null
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Email doesn't exists"
				})
			}else{
				res.send({
					success:1,
					// result:data,
					message:"Email exists"
				})
			}
		});
	}
}

exports.check_contact_exist = function(req, res){
	if(req.body.contact == '' ){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				[Op.or]:[
					{
						contact:req.body.contact
					}
				],
				deleted_at:null
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Contact number doesn't exists"
				})
			}else{
				res.send({
					success:1,
					// result:data,
					message:"Contact number exists"
				})
			}
		});
	}
}

exports.countries = function(req,res){
	models.Country.findAll({
		attributes:['id','name']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.states = function(req,res){
	var country_id = req.params.country_id;
	models.State.findAll({
		where:{
			country_id:country_id
		},
		attributes:['id','name','country_id']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}
exports.cities = function(req,res){
	var state_id = req.params.state_id;
	models.City.findAll({
		where:{
			state_id:state_id
		},
		attributes:['id','name','state_id']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.domains = function(req,res){
	models.Domain.findAll({
		where:{
			deleted_at:null,
		},
		attributes:['id','name']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.employment_status = function(req,res){
	models.EmploymentStatus.findAll({
		where:{
			deleted_at:null,
		},
		attributes:['id','name']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.user_profile = function(req,res) {
    var user_id = req.params.user_id;
    models.User.findOne({
        where:{
            id:user_id,
            deleted_at:null,
        },
        include:[
        	{
        		model:models.UserCourse,
        		as:'user_category'
        	},
        	{
        		model:models.Trainer,
        		as:'teacher_details'
        	},
        	
        ],
    }).then((user_detail)=>{
        if (user_detail == '' || user_detail == null) {
            res.send({
                success:0,
                message:'This user does not exists'
            });
        }else{
        	if(user_detail.verified_account=='yes'){


	            if (user_detail.status == 'A') {
	                res.send({
	                    success:1,
	                    data:user_detail
	                });
	            }else{
	                res.send({
	                    success:0,
	                    message:"Your account is not active. Please contact to admin for more information",
	                });                
	            }
	        }else{
	        	res.send({
	        		success:0,
	        		message:"Your account is not approved by admin. Please contact to admin for more information"
	        	})
	        }
        }
    },error=>{
        res.send({
            success:0,
            message:error
        })
    })
}

exports.change_password = function(req, res){
	if(req.body.old_password == '' || req.body.new_password == ''|| req.body.user_id == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				id:req.body.user_id,
				deleted_at:null
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Account doesn't exists"
				})
			} else{
				if(data.verified_account=='yes')
					if(data.status == 'A'){
						if(bcrypt.compareSync(req.body.old_password,data.password) ){
							var salt     = bcrypt.genSaltSync(10);
							new_password = bcrypt.hashSync(req.body.new_password,salt);
							data.update({
								password:new_password,
								updated_at:new Date()
							}).then(updated=>{
								res.send({
									success:1,
									message:"Password change successfully",
								})
							}, error=>{
								res.send({
									success:0,
									message:COMMON_ERROR
								})
							})

						} else{
							res.send({
								success:0,
								message:"Incorrect old password."
							})
						}
					} else{
						res.send({
							success:0,
							message:"Your account is not active. Please contact to admin for more information"
						})
					}
				else{
					res.send({
						success:0,
						message:"Your account is not approved by admin. Please contact to admin for more information"
					})
					
				}
			}
		})
	}
}

exports.terms_conditions = function(req,res){
	models.Content.findOne({
		where:{
			type:'T'
		},
		attributes:['description']
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.add_subscription_plan = function(req,res){
	if(req.body.trainer_id == '' || req.body.title == '' || req.body.price == '' 
		|| req.body.validity == ''|| req.body.description == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				id:req.body.trainer_id,
				deleted_at:null
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Teacher id does not exists."
				})
			}else{
				req.body.created_at = new Date();
				req.body.updated_at = new Date();
				models.SubscriptionPlan.create(req.body).then(data=>{
					res.send({
						success:1,
						message:"Subscription plan added successfully."
					})
				}, error=>{
					res.send({
						success:0,
						message:COMMON_ERROR
					})
				})
			}
		});
	}
}
exports.subscription_plans = function(req,res){
	var teacher_id = req.params.teacher_id;
	// console.log(teacher_id);
	models.SubscriptionPlan.findAll({
		where:{
			trainer_id:teacher_id,
			deleted_at:null
		},
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No subscription plan found"
			})
		}else{
			res.send({
				success:1,
				result:data,
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.edit_subscription_plan = function(req,res){
	
	if(req.body.subscription_plan_id == '' || req.body.title == '' || req.body.price == '' 
		|| req.body.validity == '' || req.body.description == ''){
	    res.send({
	        success:0,
	        message:REQUIRED_FIELDS_ERROR
	    })
	}else if(req.body.title.length>='35'){
		res.send({
		    success:0,
		    message:"title must be less then 35 characters",
		})
	}else if(req.body.description.length>'500'){
		res.send({
		    success:0,
		    message:"Description must be less then equal 500 characters",
		})
	}else{ 
		models.SubscriptionPlan.findOne({
			where:{
				id:req.body.subscription_plan_id,
				deleted_at:null
			},
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No subscription plan found"
				})
			}else{
				data.update({
	                title:req.body.title,
	                price:req.body.price,
	                validity:req.body.validity,
	                description:req.body.description,
	            }).then(address=>{
	                res.send({
	                    success:1,
	                    result:data,
	                    message:"Subscription plan edited successfully"
	                })
	            },error=>{
	                res.send({
	                    success:0,
	                    result:data,
	                    message:COMMON_ERROR
	                })
	            });
			}
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.delete_subscription_plan = function(req,res){
    var subscription_plan_id = req.body.subscription_plan_id;
    // console.log(subscription_plan_id);
    models.SubscriptionPlan.update({
        deleted_at:new Date()
    },{
        where:{
            id:subscription_plan_id
        }
    }).then(delete_plan=>{
        res.send({
            success:1,
            message:'Subscription plan deleted successfully.'
        });
    },error=>{
        res.send({
            success:0,
            message:COMMON_ERROR
        });
    });

}

exports.edit_profile = function(req,res){
	
	// console.log(req.files);
	if(req.body.user_id == '' || req.body.first_name == '' || req.body.last_name == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		var user_id = req.body.user_id;
		models.User.findOne({
			where:{
				id:user_id,
				deleted_at:null,
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"No record found"
				})
			} else{
				if(data.status=='A'){

					var image_name = data.image;

					if(data.user_type=='trainer'){
						pincode = null;
						exam_preparation_id = null;
						userImagePath = teacherImagePath;
					}else{
						userImagePath = studentImagePath;
						pincode = req.body.pincode;
						exam_preparation_id = req.body.exam_preparation_id;
					}

					if(req.files){


						var old_image = data.image;
						if(old_image != '' && old_image != null){
							if(fs.existsSync(userImagePath+old_image)){
								fs.unlink(userImagePath+old_image, (err) => {
								  	if (err) throw err;
								  	console.log('file was deleted');
								});
							}
						}

						var new_image = req.files.image;
						if(new_image != '' && new_image != null && new_image != undefined){
							image_name = Math.random().toString(36).slice(-8) + new_image.name;
							new_image.mv(userImagePath+image_name, function(err){
								if(err){
									res.send({
										success:0,
										message:"Image not uploaded"
									})
								}
							})
						}
					}
					
					data.update({
						first_name:req.body.first_name,
						last_name:req.body.last_name,
						dob:req.body.dob,
						gender:req.body.gender,
						country_id:req.body.country_id,
						state_id:req.body.state_id,
						city_id:req.body.city_id,
						exam_preparation_id:exam_preparation_id,
						pincode:pincode,
						image:image_name,
						address:req.body.address,
						district:req.body.district,
						updated_at:new Date()
					}).then(updated=>{
						if (data.user_type=='trainer') {

							models.Trainer.findOne({
								where:{
									id:user_id
								}
							}).then(teacher_detail=>{
								if(teacher_detail == '' || teacher_detail == null){
									res.send({
										success:0,
										message:"No record found"
									})
								} else{
									teacher_detail.update({
										pincode:req.body.pincode,
										employment_status_id:req.body.employment_status_id,
										organization_name:req.body.organization_name,
										experience:req.body.experience,
										trainer_domain_id:req.body.trainer_domain_id,
										subject_id:req.body.subject_id,
									}).then(updated=>{
										res.send({
											success:1,
											message:"Profile updated successfully",
											user_detail:data,
											teacher_detail:teacher_detail
										})
									},error=>{
										console.log('enterlast0');
										res.send({
											success:0,
											message:COMMON_ERROR
										})
									})

								}
							})
						}else{

							res.send({
								success:1,
								message:"Profile updated successfully",
								result:data
							})
						}
					}, error=>{
						console.log('enterlast1');
						res.send({

							success:0,
							message:COMMON_ERROR
						})
					})
				}else{
					res.send({
						success:0,
						message:"Your account is not active. Please contact to admin for more information"
					})
				}
			
			}
		})
	}
}

exports.admin_contents = function(req,res){
	
	models.TrainerContent.findAll({
		where:{
			deleted_at:null,
			trainer_id:'0',
		},
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			}
		],
		attributes:['id','title','description','final_price','gst'] 
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.categories = function(req,res){
	console.log('enter');
	models.Category.findAll({
		where:{
			deleted_at:null,
		},
		attributes:['id','name'],
	}).then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		console.log(error);
		res.send({
			success:0,
			message:error
		})
	})
}

exports.user_profile_image_path = function(req,res){
	var student_image_path = studentImagePath;
	var teacher_image_path = teacherImagePath;
	res.send({
			success:1,
			result:{
				student_profile_image_path:studentImagePath,
				teacher_profile_image_path:teacherImagePath
			},
		})
	
}

exports.courses = function(req,res){
	
	models.TrainerContent.findAll({
		where:{
			deleted_at:null,
		},
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			}
		],
		attributes:['id','trainer_id','title','description','content_availability','paid_amount','final_price','gst'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No course found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}
exports.course_detail = function(req,res){
	var course_id = req.params.course_id;
	models.TrainerContent.findAll({
		where:{
			id:course_id,
			deleted_at:null,
		},
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			}
		],
		attributes:['id','trainer_id','title','paid_amount','content_availability','category_id','sub_category_id','sub_subcategory_id','description','final_price','gst'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No course detail found"
			})
		}else{

			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.contact_us = function(req,res){
		// console.log(req.body);
	if(req.body.first_name == '' || req.body.email == '' || req.body.last_name == ''|| req.body.contact_no == ''
		|| req.body.enquiry == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		req.body.created_at = new Date();
		req.body.updated_at = new Date();

		models.ContactUs.create(req.body).then(data=>{
			res.send({
				success:1,
				message:"Your query has submitted successfully"
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.course_faqs = function(req, res){
	models.Faq.findAll({
		limit:3,
		where:{
			deleted_at:null,
		},

	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No faq found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.related_courses= function(req, res){
	sub_category_id = req.params.sub_category_id,
	models.TrainerContent.findAll({
		
		where:{
			sub_category_id:sub_category_id,
			trainer_id: {[Op.notIn]:[0]},
			deleted_at:null,
		},
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			}
		],
		attributes:['id','trainer_id','title','description','final_price','gst'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No course found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.counsellings = function(req,res){
		// console.log(req.body);
	if(req.body.name == '' || req.body.email == '' || req.body.category_id == ''|| req.body.contact_no == ''
		|| req.body.sub_category_id == '' || req.body.state_id == '' || req.body.description == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		req.body.created_at = new Date();
		req.body.updated_at = new Date();
		console.log(req.body);
		models.FreeCounselling.create(req.body).then(data=>{
			res.send({
				success:1,
				message:"Your Counselling request submitted successfully"
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.buy_course = function(req,res){
	console.log(req.body);
	if(req.body.user_id == '' || req.body.course_id == '' || req.body.razor_pay_id == ''
		|| req.body.price == '' || req.body.subscription_plan_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		console.log('enter');
		user_id   = req.body.user_id;
		course_id = req.body.course_id;
		models.User.findOne({
			where:{
				id:user_id,
				status:'A',
				user_type:'user',
				deleted_at:null,
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Invalid user"
				})
			}else{
				if(req.body.course_id=='0'){
					// console.log('enter23');
					models.SubscriptionPlan.findOne({
						where:{
							id:req.body.subscription_plan_id,
							deleted_at:null
						}
					}).then(subscription=>{
						// console.log('subscription_plan_id='+req.body.subscription_plan_id);
						// console.log(subscription);
						if(subscription == '' || subscription == null){
							res.send({
								success:0,
								message:"Subscription plan does not exists"
							})
						}else{

							req.body.purchased_on = new Date();
							req.body.paid_status  = 'N';
							req.body.paid_on      = null;
							var commission        = '';
							if(req.body.discount_amount!='' && req.body.discount_coupon!=''){
								req.body.discount_coupon= req.body.discount_coupon;
								req.body.price          = req.body.price+req.body.discount_amount;
							}else{
								req.body.discount_coupon  = null;
								req.body.discount_amount  = null;
								req.body.price            = req.body.price;
							}
							req.body.final_total = req.body.price;  
							models.Admin.findOne({
								limit:1,
							}).then(admin=>{
								get_admin_commission = admin.commission;
								
								if(get_admin_commission!=''&&get_admin_commission>0){
									admin_commission = (req.body.price*get_admin_commission)/100;
								
									req.body.admin_commission = admin_commission
									req.body.trainer_price    = req.body.price-admin_commission;
								}else{
									req.body.admin_commission = 0 ;
									req.body.trainer_price    = req.body.price;
									req.body.paid_status      = 'Y'
								}
								
								req.body.created_at   = new Date();
								req.body.updated_at   = new Date();

								// var date2 = new Date(req.body.created_at + 60*60*24*7*subscription.validity);
											// = new Date()
								// console.log("valid_till=>>>>>>>>>>>"+date2);
								req.body.valid_till   = req.body.validity;
								models.Transaction.create(req.body).then(transaction=>{
									console.log('done');
									var date   = new Date();
									var values = { deleted_at: date};
									var selector = { 
									  where: { teacher_id: subscription.trainer_id,user_id:req.body.user_id }
									};
									models.Rating.update(values, selector);
									res.send({
										success:1,
										result:transaction,
										message:"Subscription plan purchased successfully"
									})
								}, error=>{
									console.log(req.body);
									res.send({
										success:0,
										message:COMMON_ERROR
									})
							
								})
							}, error=>{
								res.send({
									success:0,
									message:COMMON_ERROR
								})
							
							})
						}
					},error=>{
						res.send({
							success:0,
							message:error
						})
					
					})
				}else{
					console.log('enter0');
					models.TrainerContent.findOne({
						where:{
							id:course_id,
							deleted_at:null,
						}
					}).then(course=>{
						if(course == '' || course == null){
							res.send({
								success:0,
								message:"No course found"
							})
						}else{
							
							req.body.purchased_on = new Date();
							req.body.valid_till   = course.end_date;
							req.body.paid_status  = 'N';
							req.body.paid_on      = null;
							var commission        = '';
							if(req.body.discount_amount!='' && req.body.discount_coupon!=''){
								req.body.discount_coupon  = req.body.discount_coupon;
								req.body.price            = req.body.price+req.body.discount_amount;
							}else{
								req.body.discount_coupon  = null;
								req.body.discount_amount  = null;
								req.body.price            = req.body.price;
							}
							req.body.final_total = req.body.price;  
							models.Admin.findOne({
								limit:1,
							}).then(admin=>{
								get_admin_commission = admin.commission;
								if(course.trainer_id!=0){
									if(get_admin_commission!=''&&get_admin_commission>0){
										admin_commission = (req.body.price*get_admin_commission)/100;
									
										req.body.admin_commission = admin_commission
										req.body.trainer_price    = req.body.price-admin_commission;
									}else{
										req.body.admin_commission = 0 ;
										req.body.trainer_price    = req.body.price;
									}
								}else{
									req.body.admin_commission = req.body.final_total;
									req.body.trainer_price    = 0;
								}
								req.body.created_at   = new Date();
								req.body.updated_at   = new Date();
								// console.log(req.body);
							
								models.Transaction.create(req.body).then(transaction=>{
									res.send({
										success:1,
										message:"Course purchased successfully"
									})
								}, error=>{
									console.log(req.body);
									res.send({
										success:0,
										message:COMMON_ERROR
									})
							
								})

							}, error=>{
								res.send({
									success:0,
									message:COMMON_ERROR
								})
							
							})
						}
					},error=>{
						res.send({
							success:0,
							message:COMMON_ERROR
						})
					
					})
				}
				
				
			} 
		},error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		
		})
	}
}

exports.verify_coupon_code =function(req,res){
    var coupon_code = req.body.coupon_code;

    models.DiscountCoupon.findOne({
        where:{
            coupon_code:coupon_code,
            status:'A',
            deleted_at:null
        }
    }).then(discount_coupon=>{
        if (discount_coupon == '' || discount_coupon == null) {
            res.send({
                success:0,
                message:'Invalid promo code.'
            });
        }else{

            var start_date      = discount_coupon.start_date;
            var end_date        = discount_coupon.end_date;
            var current_date    = format.asString('yyyy-MM-dd',new Date());
            var coupon_code     = discount_coupon.coupon_code;
            var discount_val    = discount_coupon.discount_amount;
            var discount_type   = discount_coupon.discount_type;
            var uses_limit      = discount_coupon.use_limit_per_user;

            if(current_date < start_date || current_date > end_date) {
                res.send({
                    success:0,
                    message:'Coupon code expired',
                });
            }else{
                models.Transaction.count({
                    where:{
                        discount_coupon:coupon_code
                    }
                }).then(used_coupon_count=>{
                    if (used_coupon_count >= uses_limit ) {
                        res.send({
                            success:0,
                            message:'Coupon code exceeds maximum usage limit',
                        });
                    }else{
                        res.send({
                            success:1,
                            coupon_type:discount_coupon.discount_type,
                            coupon_value:discount_coupon.discount_amount,
                        });
                    }
                });
            }
        }
    })
    
}

exports.get_gst = function(req,res){
	models.Admin.findOne({
		limit:1,
		attributes:['id','gst']
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No gst found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	},error=>{
		res.send({
			success:0,
			message:error
		})
	});
} 

exports.course_bought = function(req,res){
	var user_id = req.params.user_id;
	models.Transaction.findAll({
		where:{
			user_id:user_id,
			course_id:{
				[Op.gt]:0
			}
		},
		include:{
			model:models.TrainerContent,
			as:'purchased_content',
			include:{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			},
		},

		attributes:['id','course_id']
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No purchased course found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
} 

exports.verify_refferal_code =function(req,res){
	if(req.body.referral_code == '' || req.body.user_id == '' ){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		referral_code = req.body.referral_code;
		user_id       = req.body.user_id;
		models.RefferalCode.findOne({
			where:{
				[Op.or]:[
						{
							coordinator_id:referral_code
						},
						{
							pincode:referral_code
						}
					],
				deleted_at:null
			},
		}).then(data=>{
			models.User.findOne({
				where:{
					id:user_id,
					status:'A',
					user_type:'user',
					deleted_at:null,
				}
			}).then(user_data=>{
				if(user_data == '' || user_data == null){
					res.send({
						success:0,
						message:"Invalid user"
					})
				}else{
					if(user_data.pincode!=''&& user_data.pincode==referral_code){
						res.send({
							success:1,
							message:"valid referral code"
						})
					}else{

						if(data == '' || data == null){
							res.send({
								success:0,
								result:data,
								message:"Invalid referral code"
							})
						}else{
							res.send({
								success:1,
								message:"valid referral code"
							})
						}
					}
				}
			},error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})

			
		},error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.banner_images = function(req, res){
	/*if(req.body.catgeory_id==''||req.body.sub_catgeory_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{*/

		models.BannerImage.findAll({
			include:{
				model:models.TrainerContent,
				where:{
					deleted_at:null
				},
				attributes:['id'],
				required:true
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No banner image found"
				})
			}else{
				res.send({
					success:1,
					result:data
				})
			}
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	// }
}

/*exports.subscription_plans = function(req, res){
	models.BannerImage.findAll().then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No banner image found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}*/

exports.add_device = function(req, res){

	if(req.body.user_id == '' || req.body.device_id == '' || req.body.device_token == '' ){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.UserDevice.count({
			where:{
				user_id:req.body.user_id,
				device_id:req.body.device_id,
				device_token:req.body.device_token,
			}
		}).then(device_count=>{
			if(device_count == 0){
				models.UserDevice.create(req.body).then(device_created=>{
					res.send({
						success:1,
						message:'Device added successfully'
					})
				}, error=>{
					res.send({
						success:0,
						message:COMMON_ERROR
					})
				})
			} else{
				res.send({
					success:1,
					message:'Device added successfully'
				})
			}
		})
	}
}

exports.remove_device = function(req, res){

	if(req.body.user_id == '' || req.body.device_id == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.UserDevice.destroy({
			where:{
				user_id:req.body.user_id,
				device_id:req.body.device_id
			}
		}).then(device_removed=>{
			res.send({
				success:1,
				message:"Device removed successfully"
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.chat_room = function(req,res){
   	
   	var random_no = Math.floor(Math.random()*90000) + 10000;
	var chat_all_data = {};
	if(req.body.sender_id != null && req.body.receiver_id != null){
		
		models.ChatRoom.findOne({
	  		where:{
	  			[Op.or]:[
	  				{
	  					sender_id: req.body.sender_id,
	  					receiver_id : req.body.receiver_id,
	  				},
	  				{
	  					sender_id: req.body.receiver_id,
	  					receiver_id: req.body.sender_id,
	  				}
	  			]
	  		}
	  	}).then(data=>{
	  		if(data == null){
	  			console.log(data);
	  			var random_no = Math.floor(Math.random()*90000) + 10000;

		      	models.ChatRoom.create({
		      		sender_id: req.body.sender_id,
		      		receiver_id: req.body.receiver_id,
		      		room_id: random_no,
		      	}).then(chat_room_data=>{

		      		models.Chat.update(
					{
						seen:'yes'
					},{
						where:{
		  					// room_id : data.room_id,
		  					sender_id: req.body.sender_id
		  				},
					})
		      		models.Chat.findAll({
		  				where:{
		  					room_id : chat_room_data.room_id
		  				},
		  				include:[
		  					{
		  						model: models.User,
		  						required: false,
		  					}
		  				]
		  			}).then(chat_data=>{
		  				console.log(chat_data);
		  				chat_all_data.room_id = chat_room_data.room_id,
		  				chat_all_data.chat_data = chat_data,
		  				res.send({
				      		success:1,
				      		result: chat_all_data
				      	})

		  			})
				})
			}else{

				models.Chat.update(
				{
					seen:'yes'
				},{
					where:{
	  					// room_id : data.room_id,
	  					sender_id: req.body.sender_id
	  				},
				})
				
				models.Chat.findAll({
	  				where:{
	  					room_id : data.room_id
	  				},
	  				include:[
	  					{
	  						model: models.User,
	
	  						// where:{
	  						// 	id:req.body.sender_id
	  						// },
	  						required: false,
	  						attributes:['id','image'],
	  					}
	  				],
	  		
	  			}).then(chat_data=>{ 
	  				console.log(chat_data);
	  				chat_all_data.room_id = data.room_id,
	  				chat_all_data.chat_data = chat_data,
	  				res.send({
						success:1,
						result: chat_all_data,
					})
	  			})
			}
		})
	}else{
		res.send({
			success: 0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
}

exports.chat_history = function(req,res){
	
	models.ChatRoom.findAll({
		where:{
			[Op.or]:[
				{
					sender_id: req.body.user_id
				},
				{
					receiver_id: req.body.user_id
				}
			]
		},
		include:[
			{
				model:models.User,
				as:'Sender',
				where:{
					id:{
						[Op.ne]:req.body.user_id
					}
				},
				attributes:['id','first_name','last_name','image'],
				required:false
			},
			{
				model:models.User,
				as:'Receiver',
				where:{
					id:{
						[Op.ne]:req.body.user_id
					}
				},
				attributes:['id','first_name','last_name','image'],
				required:false
			},
			{
				model:models.Chat,
				as:'Chat',
				where:{
					deleted_at:null,
					// seen:'no'
				},
				required:true,
				attributes:['id','room_id']
				/*attributes: [ 
			        [Sequelize.literal('(SELECT COUNT(*) FROM chats WHERE chats.seen = "no")'), 'MessageCount'] 
			    ],
			    limit:1*/
			},
			{
				model:models.Chat,
				as:'UnseenChat',
				where:{
					deleted_at:null,
					seen:'no'
				},
				required:false,
				attributes:['id','room_id']
				/*attributes: [ 
			        [Sequelize.literal('(SELECT COUNT(*) FROM chats WHERE chats.seen = "no")'), 'MessageCount'] 
			    ],
			    limit:1*/
			}

		]
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				message:'No record found'
			})
		} else {
			res.send({
				success:1,
				result:data
			})
		}
	})
}

exports.sub_categories = function(req,res){

	if( req.body.category_id != null &&  req.body.category_id != ''){
		models.SubCategory.findAll({
			where:{
				category_id: req.body.category_id,
				deleted_at:null,
			},
			
			attributes:['id','category_id','name'],
		}).then(data=>{
			res.send({
				success:1,
				result:data
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}else{
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
}

exports.sub_sub_categories = function(req,res){

	if( req.body.sub_category_id != null &&  req.body.sub_category_id != ''){

		models.SubSubCategory.findAll({
			where:{
				sub_category_id: req.body.sub_category_id,
				deleted_at:null,
			},
			
			attributes:['id','sub_category_id','name'],
		}).then(data=>{
			res.send({
				success:1,
				result:data
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}else{
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
}

exports.counselling_states = function(req,res){

	models.State.findAll().then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.walkin_contents = function(req,res){

	models.WalkinContent.findAll().then(data=>{
		res.send({
			success:1,
			result:data
		})
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.chat_room = function(req,res){
   	
   	var random_no = Math.floor(Math.random()*90000) + 10000;
	var chat_all_data = {};
	if(req.body.sender_id != null && req.body.receiver_id != null){
		
		models.ChatRoom.findOne({
	  		where:{
	  			[Op.or]:[
	  				{
	  					sender_id: req.body.sender_id,
	  					receiver_id : req.body.receiver_id,
	  				},
	  				{
	  					sender_id: req.body.receiver_id,
	  					receiver_id: req.body.sender_id,
	  				}
	  			]
	  		}
	  	}).then(data=>{
	  		if(data == null){

	  			var random_no = Math.floor(Math.random()*90000) + 10000;

		      	models.ChatRoom.create({
		      		sender_id: req.body.sender_id,
		      		receiver_id: req.body.receiver_id,
		      		room_id: random_no,
		      	}).then(chat_room_data=>{

		      		models.Chat.update(
					{
						seen:'yes'
					},{
						where:{
		  					// room_id : data.room_id,
		  					sender_id: req.body.sender_id
		  				},
					})
		      		models.Chat.findAll({
		  				where:{
		  					room_id : chat_room_data.room_id
		  				},
		  				include:[
		  					{
		  						model: models.User,
		  						// as: 'usr_dtl',
		  						required: false,
		  					}
		  				]
		  			}).then(chat_data=>{

		  				chat_all_data.room_id = chat_room_data.room_id,
		  				chat_all_data.chat_data = chat_data,
		  				res.send({
				      		success:1,
				      		result: chat_all_data
				      	})

		  			})
				})
			}else{

				models.Chat.update(
				{
					seen:'yes'
				},{
					where:{
	  					// room_id : data.room_id,
	  					sender_id: req.body.sender_id
	  				},
				})
				
				models.Chat.findAll({
	  				where:{
	  					room_id : data.room_id
	  				},
	  				include:[
	  					{
	  						model: models.User,
	  						// as: 'usr_dtl',
	  						required: false,
	  						attributes:['id','image'],

	  					}
	  				]
	  			}).then(chat_data=>{ 
	  				chat_all_data.room_id = data.room_id,
	  				chat_all_data.chat_data = chat_data,
	  				res.send({
						success:1,
						result: chat_all_data,
					})
	  			})
			}
		})
	}else{
		res.send({
			success: 0,
			message: messages.REQUIRED_FIELDS_ERROR
		})
	}
}

exports.related_notes= function(req, res){
/*	category_id = req.params.category_id,
	models.TrainerContent.findAll({
		
		where:{
			category_id:category_id,
			upload_type:'pdf',
			deleted_at:null,
		},required:false,
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				attributes:['id','name']
			},
			
		],
		attributes:['id','trainer_id','paid_amount','title','file','description','final_price','gst'] 
	}).then(teacher_pdf=>{
		
		models.TrainerContent.findAll({
			
			where:{
				category_id:category_id,
				deleted_at:null,
			},required:false,
			include:[
				{
					model:models.TrainerContentImage,
					as:'TrainerContentImage',
					attributes:['id','name']
				},
				{
					model:models.TrainerContentFile,
					as:'TrainerContentFile',
					attributes:['id','file'],
					where:{
						type:'pdf',
					},required:true
					
				},
				
			],
		}).then(admin_content_pdf=>{
			res.send({
				success:1,
				result:{
					teacher_pdf:teacher_pdf,
					admin_content_pdf:admin_content_pdf
				}
			})
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
			
		
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})*/
	var sub_category_id = req.body.sub_category_id;
	var user_id         = req.body.user_id;
	var sub_sub_category_ids= [];
	if(sub_category_id!=''&&sub_category_id!=null&&user_id!=''&&user_id!=null){


		models.Transaction.findAll({
			where:{
				user_id:user_id,
				course_id:{
					[Op.gt]:0
				}
			},
			include:{
				model:models.TrainerContent,
				as:'purchased_content',
				// where:{

				// },
				include:{
					model:models.TrainerContentImage,
					as:'TrainerContentImage',
					attributes:['id','name']
				},
				attributes:['sub_category_id','sub_subcategory_id'],
			},

			attributes:['id','course_id']
		}).then(async result_data=>{
			var result_content       = result_data;
			var purchased_course_ids = [];
			
			if(result_data != "" && result_data != null){
				// var result_content = result_data.subscriber_videos;
				sub_sub_category_ids = result_data.map(function(item, ind){
					// sub_category_ids.push(item.purchased_content.sub_category_id);
					// purchased_course_ids.push(item.course_id);
					return item.purchased_content.sub_subcategory_id;
					

				});
				sub_sub_category_ids = Array.from(new Set(sub_sub_category_ids));
				purchased_course_ids = result_data.map(function(item, ind){
					// sub_category_ids.push(item.purchased_content.sub_category_id);
					// purchased_course_ids.push(item.course_id);
					return item.course_id;
					

				});
				purchased_course_ids = Array.from(new Set(purchased_course_ids));

			}
			

			await models.TrainerContent.findAll({
				
				where:{
					sub_subcategory_id:{
						[Op.in]:sub_sub_category_ids
					},
					content_availability:{
						[Op.in]:['subscriber','free']
					},
					deleted_at:null,
				},required:false,
				include:[
					{
						model:models.TrainerContentImage,
						as:'TrainerContentImage',
						attributes:['id','name']
					},
					{
						model:models.TrainerContentFile,
						as:'TrainerContentFile',
						attributes:['id','file'],
						where:{
							type:'pdf',
						},required:true
						
					},
					
				],
			}).then(async subscriber_content_pdf=>{
				// console.log(sub_category_ids);
				// console.log(purchased_course_ids);
				await models.TrainerContent.findAll({
					
					where:{
						/*content_availability:'paid',
						id:{
							[Op.in]:purchased_course_ids
						},*/
						[Op.or]: [{
							content_availability:'paid',
							id:{
								[Op.in]:purchased_course_ids
							}
						},{
							
							content_availability:'free',
							sub_category_id:sub_category_id,
						}],
						trainer_id:0,
						deleted_at:null,
					},required:false,
					include:[
						{
							model:models.TrainerContentImage,
							as:'TrainerContentImage',
							attributes:['id','name']
						},
						{
							model:models.TrainerContentFile,
							as:'TrainerContentFile',
							attributes:['id','file'],
							where:{
								type:'pdf',
							},required:true
							
						},
						
					],
				}).then(async admin_content_pdf=>{

					await models.TrainerContent.findAll({
						
						where:{
							[Op.or]: [{
								content_availability:'paid',
								id:{
									[Op.in]:purchased_course_ids
								}
							},{
								
								content_availability:'free',
								sub_category_id:sub_category_id,
							}],
							upload_type:'pdf',
							deleted_at:null,
							trainer_id:{
								[Op.ne]:0
							},
						},required:false,
						include:[
							{
								model:models.TrainerContentImage,
								as:'TrainerContentImage',
								attributes:['id','name']
							},
							
						],
						attributes:['id','trainer_id','paid_amount','title','content_availability','file','description','final_price','gst'] 
					}).then(teacher_pdf=>{

						res.send({
							success:1,
							result:{
								teacher_pdf:teacher_pdf,
								admin_content_pdf:admin_content_pdf,
								subscriber_content_pdf:subscriber_content_pdf
							}
						})
					}, error=>{
						res.send({
							success:0,
							message:COMMON_ERROR
						})
					})
				}, error=>{
					res.send({
						success:0,
						message:COMMON_ERROR
					})
				})
			}, error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})
			// console.log(sub_category_ids);
			
			/*res.send({
				success:1,
				result:{
					teacher_videos:teacher_videos,
					admin_content_video:admin_content_video,
					subscriber_videos:result_data
				}
			})*/
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
				/*res.send({
					success:1,
					result:{
						teacher_videos:teacher_videos,
						admin_content_video:admin_content_video
					}
				})*/
			/*}, error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})*/
				
			
		/*}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})*/
	}else{
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
}
exports.related_video= function(req, res){
	var sub_category_id = req.body.sub_category_id;
	var user_id     = req.body.user_id;
	var sub_sub_category_ids = [];
	if(sub_category_id!=''&&sub_category_id!=null&&user_id!=''&&user_id!=null){


		models.Transaction.findAll({
			where:{
				user_id:user_id,
				course_id:{
					[Op.gt]:0
				}
			},
			include:{
				model:models.TrainerContent,
				as:'purchased_content',
				// where:{

				// },
				include:{
					model:models.TrainerContentImage,
					as:'TrainerContentImage',
					attributes:['id','name']
				},
				attributes:['sub_category_id','sub_subcategory_id'],
			},

			attributes:['id','course_id']
		}).then(async result_data=>{
			var result_content       = result_data;
			var purchased_course_ids = [];
			
			if(result_data != "" && result_data != null){
				// var result_content = result_data.subscriber_videos;
				sub_sub_category_ids = result_data.map(function(item, ind){
					// sub_category_ids.push(item.purchased_content.sub_category_id);
					// purchased_course_ids.push(item.course_id);
					return item.purchased_content.sub_subcategory_id;
					

				});
				sub_sub_category_ids = Array.from(new Set(sub_sub_category_ids));
				purchased_course_ids = result_data.map(function(item, ind){
					// sub_category_ids.push(item.purchased_content.sub_category_id);
					// purchased_course_ids.push(item.course_id);
					return item.course_id;
					

				});
				purchased_course_ids = Array.from(new Set(purchased_course_ids));

			}
			await models.TrainerContent.findAll({
				
				where:{
					sub_subcategory_id:{
						[Op.in]:[sub_sub_category_ids]
					},

					content_availability:{
						[Op.in]:['subscriber','free']
					},
					deleted_at:null,
				},required:false,
				include:[
					{
						model:models.TrainerContentImage,
						as:'TrainerContentImage',
						attributes:['id','name']
					},
					{
						model:models.TrainerContentFile,
						as:'TrainerContentFile',
						attributes:['id','file'],
						where:{
							type:'video',
						},required:true
						
					},
					
				],
			}).then(async subscriber_content_videos=>{
				console.log(purchased_course_ids);
				await models.TrainerContent.findAll({
					
					where:{
						/*content_availability:'paid',
						id:{
							[Op.in]:purchased_course_ids
						},*/
						[Op.or]: [{
							content_availability:'paid',
							id:{
								[Op.in]:purchased_course_ids
							}
						},{
							
							content_availability:'free',
							sub_category_id:sub_category_id,
						}],
						trainer_id:0,
						deleted_at:null,
					},required:false,
					include:[
						{
							model:models.TrainerContentImage,
							as:'TrainerContentImage',
							attributes:['id','name']
						},
						{
							model:models.TrainerContentFile,
							as:'TrainerContentFile',
							attributes:['id','file'],
							where:{
								type:'video',
							},required:true
							
						},
						
					],
				}).then(async admin_content_video=>{

					await models.TrainerContent.findAll({
						
						where:{
							[Op.or]: [{
								content_availability:'paid',
								id:{
									[Op.in]:purchased_course_ids
								}
							},{
								
								content_availability:'free',
								sub_category_id:sub_category_id,
							}],
							upload_type:'video',
							deleted_at:null,
							trainer_id:{
								[Op.ne]:0
							},
						},required:false,
						include:[
							{
								model:models.TrainerContentImage,
								as:'TrainerContentImage',
								attributes:['id','name']
							},
							
						],
						attributes:['id','trainer_id','paid_amount','title','content_availability','file','description','final_price','gst'] 
					}).then(teacher_videos=>{

						res.send({
							success:1,
							result:{
								teacher_videos:teacher_videos,
								admin_content_video:admin_content_video,
								subscriber_content_videos:subscriber_content_videos
							}
						})
					}, error=>{
						res.send({
							success:0,
							message:COMMON_ERROR
						})
					})
				}, error=>{
					res.send({
						success:0,
						message:COMMON_ERROR
					})
				})
			}, error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})
			// console.log(sub_category_ids);
			
			/*res.send({
				success:1,
				result:{
					teacher_videos:teacher_videos,
					admin_content_video:admin_content_video,
					subscriber_videos:result_data
				}
			})*/
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
				/*res.send({
					success:1,
					result:{
						teacher_videos:teacher_videos,
						admin_content_video:admin_content_video
					}
				})*/
			/*}, error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})*/
				
			
		/*}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})*/
	}else{
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
}

exports.subjects = function(req, res){

	sub_sub_category_id = req.params.sub_sub_category_id;
	models.Subject.findAll({
		where:{
			sub_sub_category_id:sub_sub_category_id,
			deleted_at:null,
		},
		attributes:['id','sub_sub_category_id','name'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No subject found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	}) 
}

exports.units = function(req, res){

	subject_id = req.params.subject_id;
	models.Unit.findAll({
		where:{
			subject_id:subject_id,
			deleted_at:null,
		},
		attributes:['id','subject_id','name'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No unit found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	}) 
}

exports.languages = function(req, res){

	models.Language.findAll({
		where:{
			deleted_at:null,
		},
		attributes:['id','name'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No language found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	}) 
}

exports.subscription_bought = function(req,res){
	var user_id = req.params.user_id;
	models.Transaction.findAll({
		where:{
			user_id:user_id,
			subscription_plan_id:{
				[Op.gt]:0
			}
		},
		include:{
			model:models.SubscriptionPlan,
			as:'subscribed_plan',
			// order:['id','DESC'],
			required:true
		},
		attributes:['id','subscription_plan_id','user_id','created_at','valid_till']
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No subscription found"
			})
		}else{
			res.send({
				success:1,
				result:data,
			})
		}
	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
} 

exports.teachers = function(req,res){

	models.User.findAll({
		
		where:{
			status:'A',
			user_type:'trainer',
			deleted_at:null,
		},

		include:[
			{
				model:models.Trainer,
				as:'teacher_details',
				where:{
					admin_approval:'A'
				}
			},
		],
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No teacher found"
			})
		}else{
			res.send({
				success:1,
				result:data,
			})
		}
	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	});	
}

exports.recent_searches = function(req,res){

	if(req.body.user_id ==''||req.body.course_id == ''){
		models.RecentSearches.create(req.body).then(data=>{
			res.send({
				success:1,
				result:data,
			})
		},error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		});		
	}
}

exports.exams = function(req,res){
	var sub_category_ids = [];
	if(req.body.user_id == '' || req.body.user_id == null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.Transaction.findAll({
			where:{
				user_id:req.body.user_id,
				course_id:{
					[Op.gt]:0
				}
			},
			include:{
				model:models.TrainerContent,
				as:'purchased_content',
				// where:{

				// },
				include:{
					model:models.TrainerContentImage,
					as:'TrainerContentImage',
					attributes:['id','name']
				},
				attributes:['sub_category_id'],
			},

			attributes:['id','course_id']
		}).then(result_data=>{
			var result_content       = result_data;
			
			if(result_data != "" && result_data != null){
				// var result_content = result_data.subscriber_videos;
				sub_category_ids = result_data.map(function(item, ind){
					// sub_category_ids.push(item.purchased_content.sub_category_id);
					// purchased_course_ids.push(item.course_id);
					return item.purchased_content.sub_category_id;
					

				});
				sub_category_ids = Array.from(new Set(sub_category_ids));
			}
		}).then(data=>{

			models.Exam.findAll({
				where:{
					sub_categories_id:sub_category_ids,
					deleted_at:null,
				},
				
			}).then(data=>{
				console.log('enter');
				if(data == '' || data == null){
					res.send({
						success:0,
						result:data,
						message:"No exam found"
					})
				}else{
					res.send({
						success:1,
						result:data
					})
				}
			},error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			}) 
		})
	}
	
}

exports.questions = function(req, res){
	var exam_id = req.params.exam_id;
	if(req.body.exam_id ==''){
		
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{
		/*models.Question.findAll({
			where:{
				exam_id:exam_id,
				deleted_at:null,
			},include:[
				{
					model: models.QuestionSet,

					include:[
						{
							model: models.Set,
							attributes:['name'],
						},
					],
					required:true,
				},
				{
					model: models.Option,
				}
			],required:true,
		}).then(result=>{
			if(result==null||result==''){
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			}else{

				var value_data;
				result.forEach(function (value, index) {
					value_data = value.question_sets;
					
				});

				res.send({
					success:1,
					message:value_data
				})
			}
		})*/
		/*function makeid(length) {
		   var result           = '';
		   var characters       = 'ABCDEFGHIJKL';
		   var charactersLength = characters.length;
		   for ( var i = 0; i < length; i++ ) {
		      result += characters.charAt(Math.floor(Math.random() * charactersLength));
		   }
		   return result;
		}
		console.log(makeid(1));*/
		

	
		models.Question.findAll({
			where:{
				exam_id:exam_id,
				deleted_at:null,
			},
			include:[
				{
					model: models.QuestionSet,

					include:[
						{
							model: models.Set,
							where:{
								name:'C',
							},
						},
					],
					required:true,
				},
				{
					model: models.Option,
				}
			],required:true,

		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No question found"
				})
			}else{
				res.send({
					success:1,
					result:data
				})
			}
		},error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		}) 
	
	
		

	}
}

exports.test_questions = function(req, res){
	var exam_id = req.body.exam_id;
	var user_id = req.body.user_id;


	// models.UserExam.findOne({
 //      	include:{
 //      		model:models.UserExamQuestion,
 //      		as:"exam_detail"
 //      		/*include:[
 //      			{
 //      				model: models.UserExamQuestionOption,
 //      			},

 //      		],required:false,*/
 //      	},required:false,
 //      	where:{
	// 		id:'166'
	// 	}
	// }).then(questions=>{
	// 	console.log(questions)
 //    	res.send({
 //    		success:1,
 //    		result:questions,
 //    	})
	// },error=>{
	// 	console.log(error);
	// 	res.send({
	// 		success:0,
	// 		message:error
	// 	})
	// });


	if(exam_id ==''||exam_id ==null||user_id==''||user_id==null){

		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{

		model:models.UserExam.findOne({
			
			where:{
				user_id:user_id,
				exam_id:exam_id
			},
			include:{
	      		model:models.UserExamQuestion,
	      		as:"exam_detail",
	      		include:[
	      			{
	      				model: models.UserExamQuestionOption,
	      			},

	      		],required:false,
      		},required:false,

		}).then(exam_given=>{
			if(exam_given==''||exam_given==null){
				models.Exam.findOne({
					where:{
						id:exam_id,
						deleted_at:null,
					},
					include:[
						{	
							model: models.Question,
							where:{
								deleted_at:null
							},
							include:[
								
								{
									model: models.QuestionSet,
									include:[
										{
											model: models.Set,
										},

									],required:true,

								},
								{
									model: models.Option,
								},
								
							],required:true,
							attributes:['id','exam_id']
						},
						
					],required:true,
					attributes:['id']
				}).then(sets=>{
					
					if(sets == '' || sets == null){
						res.send({
							success:0,
							result:sets,
							message:"No question found"
						})
					}else{
					/*	res.send({
							success:0,
							result:sets,
						})*/
						var set = [];
						sets.questions.forEach(function(value,key){
							value.question_sets.forEach(function(set_data,key){
								var isInArray = set.includes(set_data.set.name);
								if(isInArray!=true){
									set.push(set_data.set.name);
								
								}
							});
						});
						
						if(set!=''||set!=null){
							var randomIndex = Math.floor(Math.random() * set.length); 
							var random_set = set[randomIndex];	
							/*res.send({
								success:0,
								result:random_set,
							})		
									*/
							models.Exam.findOne({
								where:{
									id:exam_id,
									deleted_at:null,
								},
								include:[
									{	
										model: models.Question,
										where:{
											deleted_at:null
										},
										include:[
											
											{
												model: models.QuestionSet,
												include:[
													{
														model: models.Set,
														where:{
															name:random_set,
														},
													},

												],required:true,
												
											},
											{
												model: models.Option,
											},
											
										],required:true,
									},
									
								],required:true,

							}).then(data=>{

								if(data == '' || data == null){
									res.send({
										success:0,
										result:data,
										message:"No question found"
									})
								}else{
									models.UserExam.create({
										user_id:user_id,
										exam_id:exam_id,
										exam_name:data.name,
										total_question:data.total_questions,
										total_marks:data.total_marks,
										negative_marking:data.negative_marking,
										set_name:data.questions['0'].question_sets['0'].set.name
										
									}).then(exam_data=>{
										var i = 1;
										data.questions.forEach(function(value,key){

											models.UserExamQuestion.create({
												user_exam_id:exam_data.id,
												question:value.question,
												question_image:value.ques_image,
												answer_explanation:value.answer_explanation,
												answer_explanation_image:value.ans_image,
												marks:value.marks,
												correct_answer:value.correct_answer
											}).then(async user_exam_questions=>{

												value.options.forEach(async function(option,key1){
													
													await models.UserExamQuestionOption.create({
														user_exam_question_id:user_exam_questions.id,
														options:option.options,
														option_image:option.option_image,
														position:option.position,
													});
													if((data.questions.length*4)==i){
													
														models.UserExam.findOne({
													      	
													      	include:{
													      		model:models.UserExamQuestion,
													      		as:"exam_detail",
													      		include:[
													      			{
													      				model: models.UserExamQuestionOption,
													      			},

													      		],required:false,
													      	},required:false,
															      	where:{
																		id:exam_data.id
																	},								      	
														}).then(questions=>{
													    	res.send({
													    		success:1,
													    		result:questions,
													    		message:'New Exam'
													    	})
														},error=>{
															console.log(error);
															res.send({
																success:0,
																message:error
															})
														});
													}
													i++;
												})
											},error=>{
												exam_data.destroy();
												res.send({
													success:0,
													message:COMMON_ERROR
												})
											});
											
													
										})
										
										
									},error=>{
										res.send({
											success:0,
											message:COMMON_ERROR
										})
									});
										
								}
							},error=>{
								res.send({
									success:0,
									message:COMMON_ERROR
								})
							}) 


						}else{
							res.send({
								success:0,
								result:sets,
								message:"No question found"
							})
						}

							
					}
				});	
			}else{
				res.send({
					success:1,
					result:exam_given,
					message:"exam_already_give"
				});
			}
		});

		
		
	}
}

exports.save_test = function(req,res){
	
	console.log('Requested content is=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+req.body.exam_id);
	if(req.body.exam_id ==''||req.body.end_date_time == ''||req.body.duration==''
		||req.body.finish_status==''||req.body.marks_obtained==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{
		console.log('Enter in submission=>>>>>>>>>>>>>>>>>>>',req.body);
	/*	res.send({
			success:1,
			message:req.body
		});*/
		console.log("duration is"+req.body.duration);
		models.UserExam.update(
		    { 
		    	total_attempt:req.body.total_attempt,
		    	end_date_time:req.body.end_date_time,
		    	duration:req.body.duration,
		    	finish_status:req.body.finish_status,
		    	marks_obtained:req.body.marks_obtained,

		    },
		    { where:{ id:req.body.exam_id } }
		).then(data=>{
			
			console.log("updated content is =>>>>>>>>>"+data); 
			try{

				var data_new = req.body;
				console.log(data_new);

				data_new.answer_given.forEach(function(value,key){
					models.UserExamQuestion.update(
					    { 
					    	ans_status:value.ans_status,
					    	given_answer:value.given_answer
					    	
					    },
					    { where:{ id:value.user_exam_question_id } }
					)
				});
				res.send({
					
					success:1,
					message:"Exam submitted successfully"
				});
			}catch(error){
				res.send({
					success:0,
					message:COMMON_ERROR
				});
			}	
			
		});
	}
}

exports.teacher_content=function(req,res){

	if(req.body.category_id==''||req.body.trainer_id==''||req.body.sub_category_id==''
		||req.body.sub_subcategory_id==''||req.body.teacher_id==''
		||req.body.subject_id==''||req.body.unit_id==''||req.body.title==''
		||req.body.description==''||req.body.upload_type==''||req.body.image==''
		||req.body.file==''||req.body.content_availability==''||req.body.paid_amount==''){
			

			res.send({
				success:0,
				message:REQUIRED_FIELDS_ERROR
			})
	}else{

		req.body.created_at = new Date();
		req.body.updated_at = new Date();
		req.final_price     = req.body.paid_amount;
		req.gst    		    = 0;
		req.gst_price       = 0;
		var file     = req.files.file;
		console.log(file);
		if(file != '' && file != null && file != undefined){
			file_name = Math.random().toString(36).slice(-8)+file.name;
			req.body.file = file_name;
			file.mv(CourFilePath+file_name, function(err){
				if(err){
					res.send({
						success:0,
						message:"File not uploaded"
					})
				}
			})
		}
	    models.TrainerContent.create(req.body).then(data=>{
	    	if(req.files){
                req.files.image.forEach(function(image){
	                var new_image = image.name;
		    		image_name = Math.random().toString(36).slice(-8)+new_image;
		    		image.mv(CourseImagePath+image_name);
		    		models.TrainerContentImage.create({
		    			trainer_content_id:data.id,
		    			name:image_name
		    		})
	    		});
	    	}
			res.send({
				success:1,
				data:{
                	result:data,
            	},
				message:"Content added successfully",
			})
		}, error=>{
			res.send({
				success:0,
				message:error
			})
		})
	}
}


exports.get_teacher_contents=function(req,res){
	console.log('enter=>>>>>>>>>>>>>>>>>>>>')
	var whereCondition		= {};
	var teacher_id 			= req.body.teacher_id;
	var category_id         = req.body.category_id;
	var sub_category_id     = req.body.sub_category_id;
	var sub_sub_category_id = req.body.sub_sub_category_id;
	var subject_id 			= req.body.subject_id;
	var unit_id 			= req.body.unit_id;
	var upload_type 	    = req.body.upload_type;

	if(upload_type==''||upload_type==null){
		whereCondition.upload_type = 'pdf';
	}else{
		whereCondition.upload_type = upload_type;
	}
	console.log('enter=>>>>>>>>>>>>>>>>>>>>'+upload_type);

	if (category_id != '' && category_id != null) {
	    whereCondition.category_id = category_id;
	}

	if (sub_category_id != '' && sub_category_id != null) {
	    whereCondition.sub_category_id = sub_category_id;
	}

	if (sub_sub_category_id != '' && sub_sub_category_id != null) {
	    whereCondition.sub_subcategory_id = sub_sub_category_id;
	}

	if (subject_id != '' && subject_id != null) {
	    whereCondition.subject_id = {[Op.in]:subject_id};
	}
	if (unit_id != '' && unit_id != null) {
	    whereCondition.unit_id = {[Op.in]:unit_id};
	}
	console.log("nter=>>>>>>>>>>>>>>>>>>>>"+whereCondition);
	whereCondition.trainer_id = teacher_id;
	whereCondition.deleted_at = null;

	models.TrainerContent.findAll({
		where:whereCondition,
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				
			},
		],required:true,
	}).then(data=>{
		res.send({
			success:1,
            result:data,
		})
	},error=>{
		res.send({
			success:0,
			message:error
		})
	});
}

exports.delete_teacher_content = function(req,res){
	var teacher_content_id = req.params.teacher_content_id;
	console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>id is"+teacher_content_id);

	if(teacher_content_id ==''||teacher_content_id ==null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{
		models.TrainerContent.findOne({
		    where:{
		        id:teacher_content_id,
		        deleted_at:null
		    }
		}).then(teacher_content=>{
	        if (teacher_content == '' || teacher_content == null) {
	            res.send({
	                success:0,
	                message:'No content found'
	            });
	        }else{

	        	teacher_content.update({
	        		deleted_at:new Date()
	        	}).then(teacher_content=>{

	        		models.TrainerContentImage.findAll({
			            where:{
			                trainer_content_id:teacher_content.id
			            }
			        }).then(delete_teacher_content_image=>{
			        	console.log(delete_teacher_content_image);
			         	if(delete_teacher_content_image!=''&&delete_teacher_content_image!=null){
				            delete_teacher_content_image.forEach(function(value1,index1){
				                

				                if(fs.existsSync(CourseImagePath+value1.name)){
    								fs.unlink(CourseImagePath+value1.name, (err) => {
    								  	if (err) throw err;
    								  	console.log('file was deleted');
    								});
    							}
    							value1.destroy();
				            });
			        	}
			            res.send({
			            	success:1,
			            	message:"Content deleted successfully"
			            });
			        }, error=>{
						res.send({
							success:0,
							message:error
						})
					})

	        	});
				
					
			}
		})
	}
}

// exports.teacher_dashboard = function(req,res){
// 	teacher_id = req.params.teacher_id;
// 	models.TrainerContent.count({
// 		where:{
// 			trainer_id:teacher_id,
// 			upload_type:'pdf',
// 			deleted_at:null
// 		},
// 		required:true,
// 	}).then(pdf_count=>{
// 		models.TrainerContent.count({
// 			where:{
// 				trainer_id:teacher_id,
// 				upload_type:'video',
// 				deleted_at:null
// 			},
// 			required:true,
// 		}).then(video_count=>{
			
// 			res.send({
// 				success:1,
// 				data:{
//                 	pdf_count:pdf_count,
//                 	video_count:video_count,
//             	},
// 			})		
// 		},error=>{
// 			res.send({
// 				success:0,
// 				message:COMMON_ERROR
// 			})
// 		});
// 	},error=>{
// 		res.send({
// 			success:0,
// 			message:COMMON_ERROR
// 		})
// 	});
// }

exports.uploaded_notes = function(req,res){
	teacher_id = req.params.teacher_id;
	models.TrainerContent.findAll({
		where:{
			trainer_id:teacher_id,
			upload_type:'pdf',
			deleted_at:null
		},
		include:[
			{
				model:models.TrainerContentImage,
				as:'TrainerContentImage',
				
			},
		],required:true,
	}).then(uploaded_notes=>{
		
		res.send({
			success:1,
			result:uploaded_notes,
        	
		})		

	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	});
}

exports.uploaded_videos = function(req,res){
	teacher_id = req.params.teacher_id;
	models.TrainerContent.findAll({
		where:{
			trainer_id:teacher_id,
			upload_type:'video',
			deleted_at:null
		},
		required:true,
	}).then(uploaded_videos=>{
		
		res.send({
			success:1,
			result:uploaded_videos,
        	
		})		

	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	});
}

exports.total_earnings = function(req,res){
	teacher_id = req.params.teacher_id;


/*	models.Transaction.findAll({
		include:[
			{
				model:models.SubscriptionPlan,
				as:'subscribed_plan',
				where:{
					trainer_id:teacher_id,
				},required:true,
			},
		],
		// attributes: [[Sequelize.fn('sum', Sequelize.col('final_total')), 'minPrice']],
		// attributes: [[Sequelize.literal('COALESCE(SUM(purchased_content.final_price))')]],
	}).then(total_earnings=>{
		
		res.send({
			success:1,
			result:total_earnings,
	       	
		})		

	},error=>{
		res.send({
			success:0,
			message:error
		})
	});*/

	console.log('teacher_id===================='+teacher_id);


	models.Trainer.findOne({
		where:{
			id:teacher_id,
		},
		include:[
			{
				model:models.SubscriptionPlan,
				required:true,
				// where:{
				// 	trainer_id:teacher_id,
				// 	// trainer_id:{
				// 	// 	[Op.ne]:'0'
				// 	// },
				// }
				include:[
					{
						model:models.Transaction,
						required:true,
						include:{
							model:models.User,
							required:true
							
						},
					
					},
				],
			},
			{
				model:models.TrainerContent,
				required:true,
				include:[
					{
						model:models.Transaction,
						required:true,
						include:{
							model:models.User,
							required:true
							
						},
						// attributes:['id','first_name','last_name']
					
					},
				],
			}
		],required:true
	}).then(earnings=>{

		
		res.send({
			success:1,
			result:earnings
		})
	},error=>{
		res.send({
			success:0,
			message:error
		})
	});
	
}

exports.subscribed_users = function(req,res){
	teacher_id = req.params.teacher_id;
	models.Transaction.findAll({
		
		include:[

			{
				model:models.SubscriptionPlan,
				as:'subscribed_plan',
				where:{
					trainer_id:teacher_id,
				},required:true,
			},

			{
				model:models.User,	
				as:'UserSubscribedTeacher',
				required:true,
			},
		]
	}).then(subscribed_users=>{
		
		res.send({
			success:1,
			result:subscribed_users,
        	
		})		

	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	});
}

exports.check_subscription_plan_exist = function(req,res){
	if(req.body.teacher_id==''||req.body.user_id==''){

		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		var teacher_id = req.body.teacher_id;
		// console.log(teacher_id);

		models.Transaction.findOne({
			where:{
				user_id:req.body.user_id
			},
			order:[['id','DESC']],
			include:{
				model:models.SubscriptionPlan,
				as:'subscribed_plan',
				where:{
					trainer_id:teacher_id
				},
				// order:['id','DESC'],
				required:true
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"You did not subscribed any plan"
				})
			}else{
				var date1      = new Date;
				date1      	   = format.asString('yyyy-MM-dd hh:mm:ss',new Date(date1));
				var valid_till = format.asString('yyyy-MM-dd hh:mm:ss',new Date(data.valid_till));
				// console.log(valid_till);
				/*res.send({
					success:1,
					result:date1
				})*/
				if(date1<=valid_till){
					res.send({
						success:1,
						result:data
					})
				}else{
					res.send({
						success:0,
						message:'Your plan is expired. Please purchase a new plan to continue',
						// result:data
					})
				}
				    // var delivery_date =  current_date.setDate(current_date.getDate() + 4);
				    // var delivery_date =  format.asString('yyyy-MM-dd hh:mm:ss',new Date(data.created_at));
			}
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})
	}
}

exports.teacher_content_detail = function(req,res){
	teacher_id = req.params.teacher_id;
	models.TrainerContent.count({
		where:{
			trainer_id:teacher_id,
			upload_type:'pdf',
			deleted_at:null
		},
	}).then(pdf_count=>{
		models.TrainerContent.count({
			where:{
				trainer_id:teacher_id,
				upload_type:'video',
				deleted_at:null
			},
		}).then(video_count=>{
			res.send({
				success:1,
				result:{
                	pdf_count:pdf_count,
                	video_count:video_count,
            	},	
			})		
		},error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		})	

	},error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	});
}

exports.course_mock_test = function(req,res){

	if(req.body.teacher_id==''||req.body.category_id==''||req.body.sub_category_id==''
		||req.body.sub_sub_category_id==''||req.body.teacher_content_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		if(req.body.teacher_id<1){
			// console.log('-------------------------------------------enter1');
			
			models.TrainerContent.findOne({
				where:{
					id:req.body.teacher_content_id,
					trainer_id:req.body.teacher_id,
					category_id:req.body.category_id,
					sub_category_id:req.body.sub_category_id,
					sub_subcategory_id:req.body.sub_sub_category_id,
					// deleted_at:null
				},

				include:{
					model:models.TrainerContentSubject,
					required:true,
					
					include:[
						{
							model:models.Subject,
							required:true,
						},
						{
							model:models.TrainerContentSubjectUnit,
							required:true,
							include:{
								model:models.Unit,
								required:true,
							},
						}
					],
					
				},
			}).then(data=>{
				res.send({
					success:1,
					result:data
				})
			},error=>{
				res.send({
					success:0,
					message:COMMON_ERROR
				})
			})
		}else{
			
			models.TrainerContent.findOne({
				where:{
					id:req.body.teacher_content_id,
					trainer_id:req.body.teacher_id,
					category_id:req.body.category_id,
					sub_category_id:req.body.sub_category_id,
					sub_subcategory_id:req.body.sub_sub_category_id,
					// deleted_at:null
				},
				include:[
					{
						model:models.Subject,
						required:true,
					},
					{
						model:models.Unit,
						required:true,
					},
				],
			
			}).then(data=>{
				res.send({
					success:1,
					result:data
				})
			},error=>{
				res.send({
					success:0,
					message:error
				})
			})
		}
	}
}

exports.mock_test = function(req, res){

	if(req.body.sub_sub_category_id==''||req.body.subject_id==''||req.body.unit_id==''
		||req.body.user_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{

		user_id = req.body.user_id;
		models.QuestionBank.findAll({
			where:{
				sub_sub_category_id:req.body.sub_sub_category_id,
				subject_id:req.body.subject_id,
				unit_id:req.body.unit_id,
				deleted_at:null,
			},

			order:[
		        [Sequelize.literal('RAND()')]
			],
			limit: 10,
			include:[
				{	
					model: models.QuestionBankOption,
				},
				
			],required:true,
			// attributes:['id']
		}).then(data=>{
		/*	console.log('>>>>>>>>>>>correct_answer'+data.correct_answer);
			res.send({
				success:0,
				message:data
			})*/
			// res.send({
			// 	success:1,
			// 	result:data,
			// })
	/*		res.send({
				success:1,
				result:data,
			})*/
			if(data==null||data==''){
				res.send({
					success:0,
					result:data
				});
			}else{

				models.UserExam.create({
					user_id:user_id,
					exam_id:0,
					exam_name:'Mock Test',
					total_question:10,
					total_marks:0,
					negative_marking:0,
					// set_name:data.questions['0'].question_sets['0'].set.name
					
				}).then(exam_data=>{
					// dd()

					var i = 1;

					data.forEach(function(value,key){

						models.UserExamQuestion.create({
							user_exam_id:exam_data.id,
							question:value.question,
							question_image:value.ques_image,
							answer_explanation:value.answer_explanation,
							answer_explanation_image:value.ans_image,
							marks:value.marks,
							correct_answer:value.correct_answer
						}).then(async user_exam_questions=>{

							value.question_bank_options.forEach(async function(option,key1){
								
							await models.UserExamQuestionOption.create({
									user_exam_question_id:user_exam_questions.id,
									options:option.options,
									option_image:option.option_image,
									position:option.position,
								});
								if((data.length*4)==i){
								
									models.UserExam.findOne({
								      	
								      	include:{
								      		model:models.UserExamQuestion,
								      		as:"exam_detail",
								      		include:[
								      			{
								      				model: models.UserExamQuestionOption,
								      			},

								      		],required:false,
								      	},required:false,
								      	where:{
											id:exam_data.id
										},								      	
									}).then(questions=>{
								    	res.send({
								    		success:1,
								    		result:questions,
								    	})
									},error=>{
										console.log(error);
										res.send({
											success:0,
											message:error
										})
									});
								}
								i++;
							})
						},error=>{
							exam_data.destroy();
							res.send({
								success:0,
								message:error
							})
						});			
					})
				},error=>{
					res.send({
						success:0,
						message:error
					})
				});
			}
				
		});	
	}
}

exports.review_mock_test = function(req, res){
	user_exam_id = req.params.user_exam_id;
	// console.log(user_exam_id);
	if(user_exam_id==null||user_exam_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		models.UserExam.findOne({
	      	where:{
				id:user_exam_id
			},
	      	include:{
	      		model:models.UserExamQuestion,
	      		as:"exam_detail",
	      		include:[
	      			{
	      				model: models.UserExamQuestionOption,
	      			},

	      		],required:true,
	      	},required:true,
	      									      	
		}).then(questions=>{
	    	res.send({
	    		success:1,
	    		result:questions,
	    	})
		},error=>{
			// console.log(error);
			res.send({
				success:0,
				message:error
			})
		});
	}	
}

exports.teacher_subjects = function(req, res){
	models.Subject.findAll({
		where:{
			deleted_at:null,
		},
		attributes:['id','sub_sub_category_id','name'] 
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No subject found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	}) 
}


exports.get_notification = function(req, res){
	user_id = req.params.user_id;
	// console.log(user_exam_id);
	if(user_id==null||user_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		models.Notification.findAll({
	      	where:{
				user_id:user_id
			},								      	
		}).then(notifications=>{
	    	res.send({
	    		success:1,
	    		result:notifications,
	    	})
		},error=>{
			// console.log(error);
			res.send({
				success:0,
				message:error
			})
		});
	}	
}

// exports.mock
// exports.save_mock_test = function(req,res){
	
// 	console.log('Requested content is=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+req.body.exam_id);
// 	if(req.body.user_exam_id ==''||req.body.end_date_time == ''||req.body.duration==''
// 		||req.body.finish_status==''||req.body.marks_obtained==''){
// 		res.send({
// 			success:0,
// 			message:REQUIRED_FIELDS_ERROR
// 		});
// 	}else{
// 		console.log('Enter in submission=>>>>>>>>>>>>>>>>>>>',req.body);
// 	/*	res.send({
// 			success:1,
// 			message:req.body
// 		});*/
// 		console.log("duration is"+req.body.duration);
// 		models.UserExam.update(
// 		    { 
// 		    	total_attempt:req.body.total_attempt,
// 		    	end_date_time:req.body.end_date_time,
// 		    	duration:req.body.duration,
// 		    	finish_status:req.body.finish_status,
// 		    	marks_obtained:req.body.marks_obtained,

// 		    },
// 		    { where:{ id:req.body.exam_id } }
// 		).then(data=>{
			
// 			console.log("updated content is =>>>>>>>>>"+data); 
// 			try{

// 				var data_new = req.body;
// 				console.log(data_new);

// 				data_new.answer_given.forEach(function(value,key){
// 					models.UserExamQuestion.update(
// 					    { 
// 					    	ans_status:value.ans_status,
// 					    	given_answer:value.given_answer
					    	
// 					    },
// 					    { where:{ id:value.user_exam_question_id } }
// 					)
// 				});
// 				res.send({
					
// 					success:1,
// 					message:"Exam submitted successfully"
// 				});
// 			}catch(error){
// 				res.send({
// 					success:0,
// 					message:COMMON_ERROR
// 				});
// 			}	
			
// 		});
// 	}
// }

exports.check_exam_given = function(req, res){
	var exam_id = req.body.exam_id;
	var user_id = req.body.user_id;
	if(exam_id ==''||exam_id ==null||user_id==''||user_id==null){

		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{

		model:models.UserExam.findOne({
			
			where:{
				user_id:user_id,
				exam_id:exam_id
			},
			include:{
	      		model:models.UserExamQuestion,
	      		as:"exam_detail",
	      		include:[
	      			{
	      				model: models.UserExamQuestionOption,
	      			},

	      		],required:false,
      		},required:false,

		}).then(exam_given=>{
			if(exam_given==''||exam_given==null){
				
				res.send({
					success:1,
					message:"Exam not given"
				})
			}else{
				res.send({
					success:1,
					message:"You already give this exam"
				})
				
			}
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		}) 
	}
}

exports.user_exam_sets = function(req, res){
	// var exam_id = req.body.exam_id;
	var user_id = req.params.student_id;
	if(user_id==''||user_id==null){

		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{
		var today =  format.asString('dd-MM-yyyy',new Date());
		var time =  parseInt(format.asString('hh',new Date())); 
		console.log(today);
		console.log(time);
		
		model:models.UserExam.findAll({
			
			where:{
				user_id:user_id,
				// exam_id:exam_id
			},
			include:{
				model:models.Exam,
				where:{
					deleted_at:null,
					expiry_date:{
						[Op.gt]:today
					},
					// end_time:{
					// 	[Op.gte]:time
					// },
				},
				include:{
					model:models.QuestionSet,
					as:'exam_sets',
					distinct: true,
					include:[
						{
							model: models.Set,
							distinct: true,
							// where:{
							// 	deleted_at:null,
							// },
						},

					],required:true,
				}
			}
			// include:{
	  //     		model:models.UserExamQuestion,
	  //     		as:"exam_detail",
	  //     		include:[
	  //     			{
	  //     				model: models.UserExamQuestionOption,
	  //     			},

	  //     		],required:false,
   //    		},required:false,

		}).then(exam=>{
			if(exam==''||exam==null){
				
				res.send({
					success:1,
					result:exam
					// message:"Exam not given"
				})
			}else{
				res.send({
					success:1,
					result:exam
					// message:"Exam not given"
				})
				
			}
		}, error=>{
			res.send({
				success:0,
				message:COMMON_ERROR
			})
		}) 
	}
}

exports.solved_unsolved_question_set = function(req, res){
	var exam_id   = req.body.exam_id;
	var user_id   = req.body.user_id;
	var set_name  = req.body.set_name
	if(exam_id ==''||exam_id ==null||user_id==''
		||user_id==null||set_name==''||set_name==null){

		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		});
	}else{

		models.Exam.findOne({
			where:{
				id:exam_id,
				deleted_at:null,
			},
			include:[
				{	
					model: models.Question,
					where:{
						deleted_at:null
					},
					include:[
						
						{
							model: models.QuestionSet,
							include:[
								{
									model: models.Set,
									where:{
										name:set_name,
									},
								},

							],required:true,
							
						},
						{
							model: models.Option,
						},
						
					],required:true,
				},
				
			],required:true,

		}).then(data=>{

			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No question found"
				})
			}else{
				res.send({
					success:0,
					result:data,
				})
			}
		});
	}
}

exports.downloads = function(req, res){
	models.Download.findAll().then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No download content found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.social_login_fb = function(req, res){

	if(req.body.first_name == '' || req.body.last_name == '' || req.body.email == ''|| req.body.fb_unique_id == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}
	else{

		models.User.findOne({
			where:{
				// fb_unique_id: req.body.fb_unique_id,
				email:req.body.email,
				deleted_at:null
			},
			include:[{
				model:models.UserCourse,
				as:'user_category'
			}],
		}).then(data=>{

			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"This email is not resgisterd with us."
				})
				
			} else{
				if(data.verified_account=='yes'){
					if(data.status == 'Inactive'){
						res.send({
							success:0,
							message:"Your account is not active. Please contact to admin for more information"
						})
					} else{
						data.update({
							first_name: req.body.first_name,
							last_name: req.body.last_name,
							email: req.body.email,
							fb_unique_id:req.body.fb_unique_id,
							updated_at:new Date(),
						}).then(updated=>{
							var full_name = req.body.first_name+' '+req.body.last_name;
							res.send({
								success:1,
								result:updated,
								message:"Welcome "+full_name
							})
						})
					}
				}else{
					res.send({
						success:0,
						message:"Your account is not approved by admin. Please contact to admin for more information"
					})
					
				}
			}
		})
	}
}

exports.advertisement_popups = function(req, res){
	models.AdvertisementPopups.findAll({
		where:{
			status:'A'
		}
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No data found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.store_rating = function(req,res){
	if(req.body.user_id == '' || req.body.teacher_id == '' || req.body.message == ''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.User.findOne({
			where:{
				id:req.body.teacher_id,
				status:'A',
				deleted_at:null,
				user_type:'trainer'
			}
		}).then(async data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"Teacher id does not exists."
				})
			}else{

			await models.Rating.create(req.body).then(async data=>{

					await models.Rating.findAll({
						where:{
							teacher_id:req.body.teacher_id
						},
						attributes : [[Sequelize.fn('AVG', Sequelize.col('ratings')),'ratings']]

						// attributes: { include: [[sequelize.fn('COUNT', sequelize.col('ratings')), 'ratings']] }
					}).then(async details=>{
						var values = { ratings: details['0'].ratings };
						var selector = { 
						  where: { id: req.body.teacher_id }
						};
						await models.Trainer.update(values, selector).then(async trainer=>{

							res.send({
								success:1,
								result:data,
								message:"Rating given successfully.",
								// overall_ngs:trainer.ratings,
								overall_rating:details['0'].ratings
								// result:trainer
							})
						})

					})
				}, error=>{
					res.send({
						success:0,
						message:error
					})
				})
			}
		});
	}
}

exports.advertisement_popups = function(req, res){
	models.AdvertisementPopups.findAll({
		where:{
			status:'A'
		}
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No data found"
			})
		}else{
			res.send({
				success:1,
				result:data
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:COMMON_ERROR
		})
	})
}

exports.check_ratings_exist = function(req, res){
	models.Rating.findAll({
		where:{
			user_id:req.body.student_id,
			teacher_id:req.body.teacher_id,
			deleted_at:null
		}
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No data found"
			})
		}else{
			res.send({
				success:1,
				result:'Rating already exist'
			})
		}
	}, error=>{
		res.send({
			success:0,
			message:error
		})
	})
}

exports.get_ratings = function(req, res){
	models.Rating.findAll({
		where:{
			teacher_id:req.body.teacher_id
		},
		include:{
			model:models.User
		},
	}).then(data=>{
		if(data == '' || data == null){
			res.send({
				success:0,
				result:data,
				message:"No data found"
			})
		}else{
			res.send({
				success:1,
				result:data,

			})
		}
	}, error=>{
		res.send({
			success:0,
			message:error
		})
	})
}


exports.user_exam_given_list = function(req, res){
	if(req.body.user_id == '' || req.body.user_id == null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.UserExam.findAll({
			where:{
				user_id:req.body.user_id,
				exam_id:{
					[Op.ne]:0
				}
			},
			/*include:{
				model:models.User
			},*/
			// attributes:['id','exam_name','start_date_time'] 
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No data found"
				})
			}else{
				res.send({
					success:1,
					result:data,

				})
			}
		}, error=>{
			res.send({
				success:0,
				message:error
			})
		})
	}
}

exports.user_exam_detail = function(req, res){
	if(req.body.user_id == '' || req.body.user_id == null || req.body.user_exam_id == '' || req.body.user_exam_id == null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		models.UserExam.findOne({
			where:{
				user_id:req.body.user_id,
				id:req.body.user_exam_id
			},
			include:[
				{
					model:models.User,
					as:'user_details',
					include:[
						{
							model:models.Country
						},
						{
							model:models.State
						},
						{
							model:models.City
						},
						{
							model:models.UserCourse,
							as:'user_category',
							include:{
								model:models.Category
							}
						},

					],
				},
				{
					model:models.Exam,
					attributes:['id','categories_id'], 
					include:{
						model:models.Category
					}
				}
			]
			// attributes:['id','exam_name','start_date_time'] 
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					result:data,
					message:"No data found"
				})
			}else{
				res.send({
					success:1,
					result:data,

				})
			}
		}, error=>{
			res.send({
				success:0,
				message:error
			})
		})
	}
}
/*
		model:models.UserExam.findOne({
			
			where:{
				user_id:user_id,
				exam_id:exam_id
			},
			include:{
	      		model:models.UserExamQuestion,
	      		as:"exam_detail",
	      		include:[
	      			{
	      				model: models.UserExamQuestionOption,
	      			},

	      		],required:false,
      		},required:false,

		}).then(exam_given=>{
			if(exam_given==''||exam_given==null){*/
