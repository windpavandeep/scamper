var Sequelize 	= require('sequelize');
var bcrypt 		= require('bcrypt');
const Op 		= Sequelize.Op;
var models 		= require('./models.js');
var nodemailer  = require('nodemailer');

// CONSTANTS
const REQUIRED_FIELDS_ERROR = 'Please enter all the required fields';
const clientEmail  			= 'nodeteamemail@gmail.com';
const clientEmailPass 		= "promatics";
const LogoImgPath 			= "http://scamperskills.com/public/images/system/logo.png";
const TriangleImgPath 		= "http://scamperskills.com/public/images/system/ptrn1.jpg";

// --- email
var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: clientEmail,
		pass: clientEmailPass
	}
});

var sendMail = function (toAddress, subject, content, next) {
    var mailOptions = {
        from: "Scamper Skills <" + clientEmail + ">",
        to: toAddress,
        replyTo: clientEmail,
        subject: subject,
        html: content
    };
    console.log('---ok--');
    transporter.sendMail(mailOptions)
};

exports.index = function(req,res){
	console.log('in function');
}
exports.signup = function(req,res){
	if(req.body.first_name == '' || req.body.contact == '' || req.body.email == '' 
		|| req.body.password == '' || req.body.user_type == ''|| req.body.gender == ''

		|| req.body.state_id=='' || req.body.city_id==''	|| req.body.country_id==''){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	}else{
		var email     = req.body.email;
		var contact   = req.body.contact;
		var user_type = req.body.user_type;
		
		models.User.count({
			where:{
				email:email,
				deleted_at:null
			}
		}).then(check_email=>{
			if(check_email >'0'){
				res.send({
					success:0,
					message:"This email-id is already registered"
				})
			}else{

				models.User.count({
					where:{
						contact:contact,
						deleted_at:null
					}
				}).then(check_contact=>{
					if(check_contact > '0'){
						res.send({
							success:0,
							message:"This mobile number is already registered"
						})
					} else{
						var password = req.body.password;
						var hashPassword = '';
						var salt = bcrypt.genSaltSync(10);
						hashPassword = bcrypt.hashSync(password,salt);
						// console.log(salt);
						// console.log(hashPassword);

						req.body.password   = hashPassword;
						req.body.status 	= 'A';
						req.body.created_at = new Date();
						req.body.updated_at = new Date();
						if(user_type=='user'){

							models.User.create(req.body).then(data=>{
								res.send({
									success:1,
									result:data,
									message:"Signup done successfully"
								})
							})
						}else if(user_type=='trainer'){
							models.User.create({

						        first_name:req.body.first_name,
						        last_name:req.body.last_name,
						        email:req.body.email,
						        contact:req.body.contact,
						        dob:req.body.dob,
						        password:req.body.password,
						        gender:req.body.gender,
						        user_type:req.body.user_type,
						        country_id:req.body.country_id,
						        state_id:req.body.state_id,
						        city_id:req.body.city_id,
						        added_by:'self',
						        verified_account:'no',
						        status:'I',
						        created_at:req.body.created_at,
						        updated_at:req.body.updated_at
							}).then(user=>{
							    models.Trainer.create({
							        id:user.id,
							        pincode:req.body.pincode,
							        employment_status_id:req.body.employment_status_id,
							        experience:req.body.experience,
							        organization_name:req.body.organization_name,
							        trainer_domain_id:req.body.trainer_domain_id,
							        subject_id:req.body.subject_id,
							        admin_approval:'P',
							        created_at:req.body.created_at,
							        updated_at:req.body.updated_at
							    }).then(data=>{
									res.send({
										success:1,
										data:{
                                        	user_detail:user,
                                        	trainer_detail:data,
                                    	},
										message:"Signup done successfully"
									})
								})

							})
						}else{
							res.send({
								success:0,
								message:"Invalid user"
							})
						}
					}
				})
			}
		})
	}
}

exports.forgot_password = function(req,res){
	if(req.body.email == '' || req.body.email == null){
		res.send({
			success:0,
			message:REQUIRED_FIELDS_ERROR
		})
	} else{
		var email = req.body.email;
		models.User.findOne({
			where:{
				email:email
			}
		}).then(data=>{
			if(data == '' || data == null){
				res.send({
					success:0,
					message:"This email-id is not registered"
				})
			} else{
				var hashPassword = '';
				var rand = Math.floor(Math.random() * 899999 + 100000);
				rand = rand.toString();
				
				var salt = bcrypt.genSaltSync(10);
				hashPassword = bcrypt.hashSync(rand,salt);

				data.update({
					password:hashPassword
				}).then(updated=>{
					// var email_html = 'Your new password for your PRY account is '+rand;
					var email_html = `<!DOCTYPE html>
										<html>
										    <head>
										        <title>Scamper Skills Forgot Password</title>
										        <style type="text/css">
										            body{font-family: 'open-sans', sans-serif;color: #77798c;font-size: 14px;}
										        </style>
										    </head>
										    <body>        
										        <table cellspacing="0" cellpadding="0" width="100%" style="padding: 0;">
										            <tbody>
										                <tr>
										                    <td>
										                        <table cellpadding="0" cellspacing="0" width="500px" style="margin: 0px auto 0px; text-align: center; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); border-radius: 4px; background-image: url('`+TriangleImgPath+`');     background-size: cover; background-position: center center; padding: 40px;" >
										                            <tbody>
										                                <tr>
										                                    <td>
										                                        <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto; text-align: center; box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1); border-radius: 4px;" bgcolor="#fff" >
										                                            <tbody>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="background-color: #eee; padding: 40px;border-radius: 4px 4px 0 0;">
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td style="text-align:center;">
										                                                                        <img src="`+LogoImgPath+`" alt="HiFi" width="250px" />
										                                                                        <h1 style="font-weight:normal;margin:20px 0 0;color: #5693d4;"> Forgot Password </h1>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="margin:0 auto;padding:10px;"> 
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td>
										                                                                       <h4 style="text-align:left;"> Hello `+data.first_name+` </h4>
										                                                                       <p style="/*letter-spacing:1px;*/line-height:30px;text-align:left;">
										                                                                        We got a request to reset your password. Your new account password is: `+rand+`
										                                                                        </p>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                                <tr>
										                                                    <td>
										                                                        <table width="100%" style="border-top:1px solid #ddd; margin-top:10px;border-radius:0 0 4px 4px;"> 
										                                                            <tbody>
										                                                                <tr>
										                                                                    <td>
										                                                                        <p style="line-height:30px;">Copyright &copy; 2019 PRY. All rights reserved. </p>
										                                                                    </td>
										                                                                </tr>
										                                                            </tbody>
										                                                        </table>
										                                                    </td>
										                                                </tr>
										                                            </tbody>
										                                        </table>
										                                    </td>
										                                </tr>                                
										                            </tbody>
										                        </table>
										                    </td>
										                </tr>
										            </tbody>
										        </table>
										    </body>
										</html>`;
					sendMail(email, 'Scamper Skills Forgot Password', email_html, function (err, res) {
				        if (err) {
				            console.log('ERROR!');
				        } else{
				        	console.log("Email sent!");
				        }
				    });
				    res.send({
				    	success:1,
				    	message:"Password reset successfully. We have e-mailed you."
				    })
				})
			}
		})
	}
}