var app 	= require('express')();
var http 	= require('http').createServer(app);
var io 		= require('socket.io')(http);
var models 	= require('./routes/models.js');
var port 	= 3002;
var fcm 			= require('fcm-notification') ;
var fcm_private_key = require('./routes/fcm_private_key.json');
var FCM         	= new fcm(fcm_private_key);
var Sequelize 	= require('sequelize');
const Op 		= Sequelize.Op;
var dateFormat	= require('dateformat');
var fs	= require('fs');
var Files = {};
app.get('/', function(req, res){
  res.sendFile(__dirname + '/chat_form.html');
});

function push_notification(receiver_id,sender_id,notification_type,notification_body,room_id){

    var token = [];
    models.User.findOne({
    	where:{
    		id:receiver_id
    	},
    	// attributes:['notifications']
    }).then(user_data=>{
    	// if(user_data.notifications == 'on'){
			models.UserDevice.findAll({
				where:{
					user_id:receiver_id
				}
			}).then(devices=>{
				var sender_name = '';
				models.User.findOne({
					where:{
						id:receiver_id
					},
				}).then(sender_data=>{
					sender_name = sender_data.first_name+' '+sender_data.last_name;
				});
				// console.log(devices);

				devices.forEach(function(val, ind){
					token.push(val.device_token);
					// var token = val.device_token;
					// console.log('token ======== ', token);

					if(ind == (devices.length-1)){
						var message = {
							notification:{
								title:sender_name,
								body:notification_body
							},
							data: {    //This is only optional, you can send any data
					            type: notification_type,
					            sender_id:sender_id,
					            room_id:room_id
					        },
							token:token
						}

						// console.log('message ', message);
						// FCM.send(message, function(err, response){
						FCM.sendToMultipleToken(message, token, function(err, response) {
							if(err){
								console.log('error found ',err);
							} else{
								// console.log('response ', response);
								console.log('response ', 'push notification sent');
							}
						});
					}
				});
			})
    	// }
    })
}

io.on('connection', function(socket){
	console.log('a user connected');

	socket.on('disconnect', function(){
		console.log('user disconnected');
	});

	socket.on('room join', function (room) {
	    console.log('room = ' + JSON.stringify(room))
	    if(socket.rooms[room.room_id]){
	      console.log('yes')
	    }else{
	      console.log('no')
	      socket.join(room.room_id);  
	    }
	    io.emit('room join', 'You successfully connected to room ' + room.room_id);
	    socket.broadcast.in(room.room_id).emit('user connected', 'A new user connected id ');
	    console.log('i passed the check')
  	})

  	socket.on('room leave', (msg) => {
	    console.log('room leave', msg.room_id);
	    socket.leave(msg.room_id, () => {
	      io.emit('room leave', {
	        status: true,
	        room_id: msg.room_id,
	        sender_id: msg.sender_id
	      });
	    });
  	})

  	socket.on('typeIn', (msg) => {
	    console.log('typeIn*****', msg);
	    io.emit('typeIn', {
	      status: true,
	      room_id: msg.room_id,
	      sender_id: msg.sender_id
	    });
  	})

  	socket.on('typeOut', (msg) => {
	    console.log('typeOut*****', msg);
	    io.emit('typeOut', {
	      status: true,
	      room_id: msg.room_id,
	      sender_id: msg.sender_id
	    });
  	})

  	socket.on('message', function(params, callback){
	    console.log('params====',params);
	    // console.log(params.receiver_id);
	    
	    // var admin_id = 0;
		models.ChatRoom.findOne({
	      	where: {
		        [Op.or]: [{
		            sender_id: params.sender_id,
		            receiver_id: params.receiver_id
		          },
		          {
		            sender_id: params.receiver_id,
		            receiver_id: params.sender_id
		          }
		        ]
	      	}
	    }).then(function(check){
	    	// console.log(check);
	    	if (params.message) {
    		 	// var date = Date.now();
    		 	// var date = new Date();
    		 	var indiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
    		 	var date = indiaTime;
    		 	// console.log(date);
    		 	// indiaTime = new Date(indiaTime);
    		 	// console.log(date);
		        models.Chat.create({
		          sender_id: params.sender_id,
		          receiver_id: params.receiver_id,
		          message: params.message,
		          room_id: check.room_id,
		          date:date

		        }).then(function(result){
		        	models.ChatRoom.update({
		            	last_message: params.message,
		            	last_message_time : date,
		            	deleted_at:null
		          	}, 
		          	{
			            where: {
			              room_id: check.room_id
			            }
		          	}).then(function (update) {
		          		models.Chat.findOne({
			        		where:{
			        			id: result.id
			        		}
			        	}).then(function(chat_detail){

			        		push_notification(params.receiver_id,params.sender_id,
			        			notification_type="chat_notification",notification_body=params.message,check.room_id);
			        		

			        		console.log(chat_detail);
			        		//   console.log('test');
			        		 io.emit('receive', chat_detail)
			        		// io.emit('receive', chat_detail)
			        	})
		          	})
		        	// console.log(result);
		        	
		        })
	    	}
	    })
	    // io.emit('chat message', msg);
  	});

  	socket.on('seen', function (msg) {
	    console.log('msg+++++++++++++++++++++==]',msg);
	    models.Chat.update({
	      seen: 'yes'
	    }, {
	      where: {
	        // receiver_id: msg.receiver_id,
	        sender_id:msg.sender_id
	      }
	    }).then(function(seen){
	      console.log('now everything is seen')
	      console.log('sender_id =** '+msg.sender_id)
	    })
  	});

  	socket.on('uploadFileStart', function(data) {
        
        var fileName = data[0].Name;
        var fileSize = data[0].Size;
        var Place    = 0;
        var directory = "/var/www/html/public/chatData";
        
        // if (fs.existsSync(directory)) {} else {
        //     fs.mkdir(directory)
        // }
        var uploadFilePath = directory + '/' + fileName;
       
        Files[fileName] = { //Create a new Entry in The Files Variable
            FileSize: fileSize,
            Data: "",
            Downloaded: 0
        }
        fs.open(uploadFilePath, "a", 0777, function(err, fd) {
            if (err) {
                console.log('err===');
            } else {
                console.log('uploadFileStart # Requesting Place: %d Percent %d', Place, 0);
                Files[fileName]['Handler'] = fd; //We store the file handler so we can write to it later
                socket.emit('uploadFileMoreDataReq', {
                    'Place': Place,
                    'Percent': 0
                });
                // Send webclient upload progress..
            }
        });
    });

    socket.on('uploadFileChuncks', function(data) {
        console.log('*******************************************************************')
        console.log(data)
        console.log('*******************************************************************')
        var Name = data[0].Name;
        var base64Data = data[0].Data;
        console.log('base64Data=========='+base64Data);
        var playload = new Buffer(base64Data, 'base64').toString('binary');
        console.log('uploadFileChuncks # Got name: %s, received chunk size %d.', Name, playload.length);
        // --------------if new remo
        if (data[0].flag == 'new') {
            Files[Name]['Downloaded'] = playload.length;
            Files[Name]['Data'] = playload;
        } else {
            Files[Name]['Downloaded'] += playload.length;
            Files[Name]['Data'] += playload;
        }
        if (Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
        {
            console.log('uploadFileChuncks # File %s receive completed', Name);
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen) {
                // close the file
                fs.close(Files[Name]['Handler'], function() {
                    console.log('file closed');
                });
                if(data[0].room_id!=null && data[0].room_id!='' && data[0].sender_id!=null && data[0].sender_id!='' && data[0].receiver_id!=null && data[0].receiver_id!='' && data[0].message_type!='' && data[0].message_type !=null){

                	models.ChatRoom.findOne({
				      	where: {
					        [Op.or]: [
					        	{
				            		sender_id: data[0].sender_id,
					            	receiver_id: data[0].receiver_id
					          	},
					          	{
					            	sender_id: data[0].receiver_id,
					            	receiver_id: data[0].sender_id
					          	}
					        ]
				      	}
				    }).then(function(check){
				    	
				    	if (Name) {
			    		 	
			    		 	var date = dateFormat( Date(), "mmmm d, yyyy h:MM TT");
			    		 	
					        models.Chat.create({
					           	sender_id: data[0].sender_id,
					          	receiver_id: data[0].receiver_id,
					          	message: Name,
					          	messages_type: data[0].message_type,
					          	room_id: data[0].room_id,
					          	date:date

					        }).then(function(result){
					        	models.ChatRoom.update({
				        			sender_id: data[0].sender_id,
				          			receiver_id: data[0].receiver_id,
					            	last_message: Name,
					            	// messages_type: data[0].message_type,

					          	}, 
					          	{
						            where: {
						              room_id: data[0].room_id
						            }
					          	}).then(function (update) {
					          		socket.emit('uploadFileCompleteRes', { 'IsSuccess' : true, });
					          		models.Chat.findOne({
						        		where:{
						        			id: result.id
						        		}
						        	}).then(function(chat_detail){
						        		  // console.log('test');
						        		 io.emit('receive', chat_detail)
						        		// io.emit('receive', chat_detail)
						        	})
					          		// io.to(data[0].room_id).emit('message', Name);
					          	})
					        })
				    	}
				    })	
                     
                }else{
                  console.log('wrong data')
                }
            });
        } else if (Files[Name]['Data'].length > 10485760) { //If the Data Buffer reaches 10MB
            console.log('uploadFileChuncks # Updating file %s with received data', Name);
            fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen) {
                Files[Name]['Data'] = ""; //Reset The Buffer
                var Place = Files[Name]['Downloaded'];
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('uploadFileMoreDataReq', {
                    'Place': Place,
                    'Percent': Percent
                });
                // Send webclient upload progress..
            });
        } else {
            var Place = Files[Name]['Downloaded'];
            var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
            console.log('uploadFileChuncks # Requesting Place: %d, Percent %s', Place, Percent);
            socket.emit('uploadFileMoreDataReq', {
                'Place': Place,
                'Percent': Percent
            });
            // Send webclient upload progress..
        }
    });
	/*socket.on('chat message', function(msg){
		console.log('message: ' + msg);
		io.emit('chat message', msg);
	});*/
});

http.listen(port, function(){
  console.log('listening on *:'+port);
});