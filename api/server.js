var express 	= require('express');
var app 		= express();
var bodyParser 	= require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));
var fileUpload 	= require('express-fileupload');
var routes 		= require('./routes/index.js');
app.use(fileUpload());

app.all('/*', function(req, res,next) {
   res.header("Access-Control-Allow-Origin", "*");
   res.header("Access-Control-Allow-Credentials", "true");
   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Key, Authorization");
   res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH");
   next()
}); // here we try to enable url can access cross origin

var port = process.env.PORT || 3001;
app.get('/index',routes.index);
app.listen(port, function(){
	console.log('Scamper running on port number: '+port);
})
app.post('/signup',routes.signup);
app.post('/forgot-password', routes.forgot_password);
app.post('/login', routes.login);
app.post('/validate/email', routes.check_email_exist);
app.post('/validate/contact', routes.check_contact_exist);
app.get('/get/countries', routes.countries);
app.get('/get/states/:country_id', routes.states);
app.get('/get/cities/:state_id', routes.cities);
app.get('/user/profile/:user_id',routes.user_profile);
app.get('/user/domains',routes.domains);
app.post('/change/password',routes.change_password);
app.get('/term/conditions',routes.terms_conditions);
app.post('/trainer/subscription/plan/add',routes.add_subscription_plan);
app.get('/subscription/plans/:teacher_id',routes.subscription_plans);
app.post('/subscription/plan/edit',routes.edit_subscription_plan);
app.post('/subscription/plan/delete',routes.delete_subscription_plan);
app.get('/employment/status',routes.employment_status);
app.post('/user/profile/edit',routes.edit_profile);
app.get('/admin/contents',routes.admin_contents);
app.get('/get/categories',routes.categories);
app.get('/user/profile/image/path',routes.user_profile_image_path);
app.get('/courses',routes.courses);
app.get('/course/detail/:course_id',routes.course_detail);
app.post('/contact-us',routes.contact_us);
app.get('/course/faqs',routes.course_faqs);
app.get('/related/courses/:sub_category_id',routes.related_courses);
app.post('/counsellings',routes.counsellings);
app.post('/course/buy',routes.buy_course);
app.get('/purchased/courses/:user_id',routes.course_bought);
app.get('/gst',routes.get_gst);
app.post('/verify/promo/code',routes.verify_coupon_code);
app.post('/verify/refferal/code',routes.verify_refferal_code);
app.get('/banner/images',routes.banner_images);
app.post('/add/device',routes.add_device);
app.post('/remove/device',routes.remove_device);
app.post('/chat-room',routes.chat_room);
app.post('/chat-history',routes.chat_history);
app.post('/sub-categories',routes.sub_categories);
app.post('/sub-sub-categories',routes.sub_sub_categories);
app.get('/counselling/states',routes.counselling_states);
app.get('/walkin/contents',routes.walkin_contents);
app.post('/chat-room',routes.chat_room);
app.post('/related/notes',routes.related_notes);
app.post('/related/videos/',routes.related_video);
app.get('/subjects/:sub_sub_category_id',routes.subjects);
app.get('/units/:subject_id',routes.units);
app.get('/languages',routes.languages);
app.get('/subscription/bought/:user_id',routes.subscription_bought);
app.get('/teachers',routes.teachers);
app.post('/recent/searches',routes.recent_searches);
app.post('/exams',routes.exams);
app.get('/questions/:exam_id',routes.questions);
app.post('/question/tests',routes.test_questions);
app.post('/submit/test',routes.save_test);
app.post('/teacher/content/add',routes.teacher_content);
app.post('/teacher/contents',routes.get_teacher_contents);
app.get('/teacher/content/delete/:teacher_content_id',routes.delete_teacher_content);
app.get('/uploaded/video/:teacher_id',routes.uploaded_videos);
app.get('/uploaded/notes/:teacher_id',routes.uploaded_notes);
app.get('/earnings/:teacher_id',routes.total_earnings);
// app.get('/teacher/dashboard/:teacher_id',routes.teacher_dashboard);
app.get('/subscribed/users/:teacher_id',routes.subscribed_users);
app.post('/check/subscription/exists',routes.check_subscription_plan_exist);
app.get('/teacher/content/details/:teacher_id',routes.teacher_content_detail);
app.post('/course/mock/test/details',routes.course_mock_test);
app.post('/mock/test',routes.mock_test);
app.get('/review/mock/test/:user_exam_id',routes.review_mock_test);
app.get('/teacher/subjects',routes.teacher_subjects);
app.post('/check/exam/given',routes.check_exam_given);
app.get('/notification/:user_id',routes.get_notification);
app.get('/user/exam/set/:student_id',routes.user_exam_sets);
app.post('/solved/unsolved/series',routes.solved_unsolved_question_set);
app.get('/downloads',routes.downloads);
app.post('/social/login',routes.social_login_fb);
app.post('/give/ratings',routes.store_rating);
app.get('/advertisement/popup',routes.advertisement_popups);
app.post('/check/rating/exist',routes.check_ratings_exist);
app.post('/ratings',routes.get_ratings);
app.post('/user/exams',routes.user_exam_given_list);
app.post('/user/exam/details',routes.user_exam_detail);
