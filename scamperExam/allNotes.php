<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> ScamperSkill - Notes </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon app_note_sec"> <!-- change class -->
                            <div class="page_divider"> 
                                <div class="side_wid">
                                    <div class="sidebar_chd">
                                        <!--  -->
                                        <?php include('include/dashboardSidebar.php') ?>
                                        <!--  -->
                                    </div>
                                </div>
                                <div class="main_wid">
                                	<!-- header index -->
						        	<?php include('include/dashboardheader.php') ?>
						        	<!-- header index -->
                                    <div class="mainside_wrap">
                                        <!--  -->
                                        <section class="main_cntnt_dash page_div ">
                                            <div class="catg_div_sec padtb40">
                                            	<!--  -->
                                            	<div id="slider_catg" class="owl-carousel custm_slider">
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-book"></i></span>
							                                <p>Prep Test</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-file-alt"></i></span>
							                                <p>Exams</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-cart-plus"></i></span>
							                                <p>Buy Courses</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-comment-dots"></i></span>
							                                <p>Chat with Teacher</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-file-signature"></i></span>
							                                <p>Notes</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-download"></i></span>
							                                <p>Downloads</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-globe"></i></span>
							                                <p>Free Counselling</p>
							                            </div>
								                    </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>
                                        <section class="main_cntnt_dash page_div card_shd">
                                            <div class="notesg_div_sec padtb40">
                                            	<div class="sec_heading text-center">
													<h2>Related Notes</h2>
													<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
												</div>
                                            	<!--  -->
                                            	<div class="notes_wrap_all">
                                            		<div class="row">
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Math - knowing Our Numbers</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Whole</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Math-Playing with Numbers</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Understandings</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Math-Basic Geomatrical</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Integers</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Fraction Data</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Whole</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Math-Playing with Numbers</a>
								                            </div>
									                    </div>
									                    <div class="col-sm-3">
								                            <div class="notes_sing">
								                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
								                                <a>Mathematics-Understandings</a>
								                            </div>
									                    </div>
									                </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>

        <!-- modal video -->
        <div class="modal" id="modal_vid">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title"></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
	        	<div id="vidBox">
				    <video id="demo" loop="" controls="" width="100%" height="100%">
				      <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
				    </video>
				</div>
		      </div>

		    </div>
		  </div>
		</div>
        <!-- modal video -->

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            $("#slider_catg").slick({
		    	dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 5,
				slidesToScroll: 1,
		    });
        </script>
    </body>
</html>