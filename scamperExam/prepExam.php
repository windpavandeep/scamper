<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> ScamperSkill - Prep Exams </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon prep_exam_inst_sec"> <!-- change class -->
                            <div class="preInstr">
                                <div class="inst_hed_cont text-center text-uppercase">
                                    <h3 class="inst_hea">JEE main mock test - 1</h3>
                                    <h4 class="inst_hea_sub">Test 2, Physics, Unit-3</h4>
                                </div>
                                <div class="main_inst text-center">
                                    <div class="inr_int">
                                        <h3 class="text-uppercase">Instructions</h3>
                                    	<h5>Please read instructions carefully</h5>
                                    </div>
                                	<ol>
                                		<li>Total duration for exam is 60 min.</li>
                                		<li>CLock wil be on server. When clock reach zero, exam will end itself and get submitted.</li>
                                		<li>Below are the color and symbol that will help you in the exam to understand.</li>
                                		<li>No time limit to attemp qusetuion in assignment.</li>
                                        <li>Type of question will be mentioned along with the questions.</li>
                                        <li>One question can be attempted only once.</li>
                                	</ol>
    								<div class="btn_start">
    									<a href="examQues.php" class="btn btn_gradient btn_active"> Start Test</a>
    								</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>


        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            
			 $('.slider-for').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: true,
			  fade: true,
			  loops: false,
			  infinite: false,
			  asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
			  loops: false,
			  slidesToShow: 6,
			  slidesToScroll: 1,
			  asNavFor: '.slider-for',
			  dots: false,
			  infinite: false,
			  // centerMode: true,
			  focusOnSelect: true
			});
        </script>
    </body>
</html>