<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> ScamperSkill - Live Lectures </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon app_vid_sec"> <!-- change class -->
                            <div class="page_divider"> 
                                <div class="side_wid">
                                    <div class="sidebar_chd">
                                        <!--  -->
                                        <?php include('include/dashboardSidebar.php') ?>
                                        <!--  -->
                                    </div>
                                </div>
                                <div class="main_wid">
                                	<!-- header index -->
						        	<?php include('include/dashboardheader.php') ?>
						        	<!-- header index -->
                                    <div class="mainside_wrap">
                                        <!--  -->
                                        <section class="main_cntnt_dash page_div ">
                                            <div class="catg_div_sec padtb40">
                                            	<!--  -->
                                            	<div id="slider_catg" class="owl-carousel custm_slider">
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-book"></i></span>
							                                <p>Prep Test</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-file-alt"></i></span>
							                                <p>Exams</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-cart-plus"></i></span>
							                                <p>Buy Courses</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-comment-dots"></i></span>
							                                <p>Chat with Teacher</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-file-signature"></i></span>
							                                <p>Notes</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-download"></i></span>
							                                <p>Downloads</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-globe"></i></span>
							                                <p>Free Counselling</p>
							                            </div>
								                    </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>

                                        <div class="card_shd">
                                        	<ul class="nav nav-tabs pos_rel" id="myTab" role="tablist">
												<li class="nav-item">
												    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Math</a>
												</li>
												<li class="nav-item">
												    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Science</a>
												</li>
												<li class="nav-item">
												    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Physics</a>
												</li>
												<div class="wrpa_hedr_cont">
													<ul class="d-flex align-items-center" type="none">
														<li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li>
														<li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</a></li>
														<li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
													</ul>
												</div>
											</ul>
											<div class="tab-content" id="myTabContent">
	                                        	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	                                        		<section class="main_cntnt_dash page_div">
			                                            <div class="fltr_secu padtb40">
			                                            	<div class="sec_heading text-left d-flex justify-content-between">
																<h2 class="chg_font">Live Video Lectures <i class="fa fa-question-circle" data-toggle="tooltip" title="Some text here . . ."></i></h2>
															</div>
															<div class="selc_fltr">
	                                            				<form class="form-inline justify-content-between">
	                                            					<div class="wrap_selc">
		                                            					<select name="as" class="custom-select">
																		    <option selected>Select Subject</option>
																		    <option value="Maths">Maths</option>
																		    <option value="English">English</option>
																		    <option value="Chemistry">Chemistry</option>
																		    <option value="Physics">Physics</option>
																		</select>
																		<select name="cs" class="custom-select">
																		    <option selected>Select Units</option>
																		    <option value="Chapter 1">Maths</option>
																		    <option value="Chapter 2">English</option>
																		    <option value="Chapter 3">Chapter 3</option>
																		    <option value="Chapter 4">Chapter 4</option>
																		</select>
																	</div>
																	<div class="text-right">
																		<input type="text" name="" class="fltr_inpt" placeholder="Search . . .">
				                                        				<select name="cs" class="custom-select">
																		    <option selected>Sort By</option>
																		    <option value="Chapter 5">Name</option>
																		    <option value="Chapter 1">Time</option>
																		    <option value="Chapter 2">Length</option>
																		    <option value="Chapter 3">Upcoming</option>
																		    <option value="Chapter 4">Subjects</option>
																		</select>
				                                        			</div>
	                                            				</form>
		                                            		</div>
			                                            </div>
			                                        </section>

			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="vid_div_sec padtb40">
			                                            	<!--  -->
			                                            	<div class="wrap_live_vids">
			                                            		<div class="row">
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            			<div class="col-sm-4">
			                                            				<div class="vid_sing liv_sing">
				                                            				<div class="img_app text-center pos_rel">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
															            	</div>
															            	<div class="meta_teach">
															            		<div class="d-flex">
															            			<span class="tech_img">
															            				<img src="img/profile2.jpg" class="img-fluid">
															            			</span>
															            			<span class="nam_lang">
															            				<a>Anuj gupta</a>
															            				<p>English</p>
															            			</span>
															            		</div>
																            	<a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
																            	<p class="topc_tpe">#Current Affairs</p>
																            	<p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
															            	</div>
															            </div>
			                                            			</div>
			                                            		</div>
			                                            	</div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>
			                                    </div>
			                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			                                    	<!-- content goes here -->
			                                    	<div class="no_cnt text-center">
			                                    		<h2>Science</h2>
			                                    		<p class="">No data Yet</p>
			                                    	</div>
			                                    </div>
												<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
			                                    	<!-- content goes here -->
			                                    	<div class="no_cnt text-center">
			                                    		<h2>Physics</h2>
			                                    		<p class="">No data Yet</p>
			                                    	</div>
												</div>
			                                </div>
			                            </div>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>

        <!-- modal video -->
        <div class="modal" id="modal_vid">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title"></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
	        	<div id="vidBox">
				    <video id="demo" loop="" controls="" width="100%" height="100%">
				      <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
				    </video>
				</div>
		      </div>

		    </div>
		  </div>
		</div>
        <!-- modal video -->

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            $("#slider_catg").slick({
		    	dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 5,
				slidesToScroll: 1,
		    });
        </script>
    </body>
</html>