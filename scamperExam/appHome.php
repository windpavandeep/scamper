<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ScamperSkill - Dashboard Home </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">
        	

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon app_home_sec"> <!-- change class -->
                            <div class="page_divider"> 
                                <div class="side_wid">
                                    <div class="sidebar_chd">
                                        <!--  -->
                                        <?php include('include/dashboardSidebar.php') ?>
                                        <!--  -->
                                    </div>
                                </div>
                                <div class="main_wid">
                                	<!-- header index -->
						        	<?php include('include/dashboardheader.php') ?>
						        	<!-- header index -->
                                    <div class="mainside_wrap">
                                        
                                        <section class="main_cntnt_dash page_div ">
                                            <div class="catg_div_sec padtb40">
                                            	<!--  -->
                                            	<div id="slider_catg" class="owl-carousel custm_slider">
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-book"></i></span>
							                                <p>Prep Test</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-file-alt"></i></span>
							                                <p>Exams</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-cart-plus"></i></span>
							                                <p>Buy Courses</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-comment-dots"></i></span>
							                                <p>Chat with Teacher</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-file-signature"></i></span>
							                                <p>Notes</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-download"></i></span>
							                                <p>Downloads</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-globe"></i></span>
							                                <p>Free Counselling</p>
							                            </div>
								                    </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>
			                                        
                                        <div class="card_shd">
                                        	<ul class="nav nav-tabs  pos_rel" id="myTab" role="tablist">
												<li class="nav-item wid_cuss">
												    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Math</a>
												</li>
												<li class="nav-item dropdown">
												    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">More</a>
												    <div class="dropdown-menu">
												      	<a class="dropdown-item" id="subj1">Subject 1</a>
												      	<a class="dropdown-item" id="subj2">Subject 2</a>
												    </div>
												</li>
												<div class="wrpa_hedr_cont">
													<ul class="d-flex align-items-center" type="none">
														<li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li>
														<li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
														<li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
													</ul>
												</div>
											</ul>
											<div class="tab-content" id="myTabContent">
	                                        	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			                                        
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="live_div_sec padtb40">
			                                            	<div class="sec_heading text-center">
																<h2>Upcoming Live Classes</h2>
																<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
															</div>
			                                            	<!--  -->
			                                            	<div class="wrap_vids">
			                                            		<div class="selc_fltr">
		                                            				<form class="form-inline justify-content-between">
		                                            					<div class="wrap_selc">
			                                            					<select name="as" class="custom-select">
																			    <option selected>Select Subject</option>
																			    <option value="Maths">Maths</option>
																			    <option value="English">English</option>
																			    <option value="Chemistry">Chemistry</option>
																			    <option value="Physics">Physics</option>
																			</select>
																			<select name="cs" class="custom-select">
																			    <option selected>Select Units</option>
																			    <option value="Chapter 1">Maths</option>
																			    <option value="Chapter 2">English</option>
																			    <option value="Chapter 3">Chapter 3</option>
																			    <option value="Chapter 4">Chapter 4</option>
																			</select>
																		</div>
																		<div class="text-right">
					                                        				<a href="liveLectures.php" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All</a>
					                                        			</div>
		                                            				</form>
			                                            		</div>
			                                            		<div id="live_classes" class="owl-carousel custm_slider">
											                    	<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://i.pinimg.com/600x315/34/e2/f9/34e2f97b66eb1de821761a75285d2404.jpg" class="img-fluid">
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            		</div>
			                                            	</div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="vid_div_sec padtb40">
			                                            	<div class="sec_heading text-center">
																<h2>Related Videos</h2>
																<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
															</div>
			                                            	<!--  -->
			                                            	<div class="wrap_vids">
			                                            		<div class="selc_fltr">
		                                            				<form class="form-inline justify-content-between">
		                                            					<div class="wrap_selc">
			                                            					<select name="as" class="custom-select">
																			    <option selected>Select Subject</option>
																			    <option value="Maths">Maths</option>
																			    <option value="English">English</option>
																			    <option value="Chemistry">Chemistry</option>
																			    <option value="Physics">Physics</option>
																			</select>
																			<select name="cs" class="custom-select">
																			    <option selected>Select Units</option>
																			    <option value="Chapter 1">Maths</option>
																			    <option value="Chapter 2">English</option>
																			    <option value="Chapter 3">Chapter 3</option>
																			    <option value="Chapter 4">Chapter 4</option>
																			</select>
																		</div>
																		<div class="text-right">
					                                        				<a href="allVideos.php" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All</a>
					                                        			</div>
		                                            				</form>
			                                            		</div>
			                                            		<div id="slider_rel_vids" class="owl-carousel custm_slider">
											                    	<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">Preparation of JEE/NEET AIEEE</p>
															            </div>
			                                            			</div>
			                                            			<div class="item">
			                                            				<div class="vid_sing">
				                                            				<div class="img_app text-center">
															            		<img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
															            		<div class="video-play-icon " id="video-trigger">
													                                <a class="wpsuper-lightbox-video vbox-item" data-target="#modal_vid" data-toggle="modal">
													                                    <i class="fa fa-play"></i>
													                                </a>
													                            </div>
															            	</div>
															            	<p class="vid_head">New syllabus of Civil Services</p>
															            </div>
			                                            			</div>
			                                            		</div>
			                                            	</div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>                                        
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="notes_div_sec padtb40">
			                                            	<div class="sec_heading text-center">
																<h2>Related Notes</h2>
																<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
															</div>
			                                            	<!--  -->
			                                            	<div class="selc_fltr mb-1">
	                                            				<form class="form-inline justify-content-end">
																	<div class="text-right col-12">
				                                        				<a href="studyNotes.php" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
				                                        			</div>
	                                            				</form>
		                                            		</div>
			                                            	<div id="slider_notes" class="owl-carousel custm_slider">
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math - knowing Our Numbers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Whole</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math-Playing with Numbers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Understandings</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math-Basic Geomatrical</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Integers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Fraction Data</a>
										                            </div>
											                    </div>
											                </div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="teach_talk padtb40">
			                                            	<div class="sec_heading text-center">
																<h2>Chat with Teachers</h2>
																<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
															</div>
			                                            	<!--  -->
			                                            	<div id="slider_teach" class="owl-carousel custm_slider">
											                    <div class="item">
										                            <div class="teach_sing">
										                                <span><img src="img/g1.jpg" class="img-fluid"></span>
										                                <div class="meta_teacher">
										                                	<h3><a>Anurasha Kaushik</a></h3>
										                                	<p><i>Entrance Exam</i></p>
										                                	<p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
										                                </div>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="teach_sing">
										                                <span><img src="img/profile1.jpg" class="img-fluid"></span>
										                                <div class="meta_teacher">
										                                	<h3><a>Anuj Mishra</a></h3>
										                                	<p><i>Entrance Exam</i></p>
										                                	<p class="ratein"><i class="fa fa-star"></i> 4/5</p>
										                                </div>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="teach_sing">
										                                <span><img src="img/profile2.jpg" class="img-fluid"></span>
										                                <div class="meta_teacher">
										                                	<h3><a>KN Mahajan</a></h3>
										                                	<p><i>Entrance Exam</i></p>
										                                	<p class="ratein"><i class="fa fa-star"></i> 5/5</p>
										                                </div>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="teach_sing">
										                                <span><img src="img/g2.jpg" class="img-fluid"></span>
										                                <div class="meta_teacher">
										                                	<h3><a>Shara Leepa</a></h3>
										                                	<p><i>Entrance Exam</i></p>
										                                	<p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
										                                </div>
										                            </div>
											                    </div>
											                </div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="solved_div_sec padtb40">
			                                            	<div class="sec_heading text-center">
																<h2>Previous Solved Exams</h2>
																<p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
															</div>
															<div class="selc_fltr mb-1">
	                                            				<form class="form-inline justify-content-end">
																	<div class="text-right col-12">
				                                        				<a href="solvedExams.php" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
				                                        			</div>
	                                            				</form>
		                                            		</div>
			                                            	<!--  -->
			                                            	<div id="slider_solved" class="owl-carousel custm_slider">
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math - knowing Our Numbers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Whole</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math-Playing with Numbers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Understandings</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Math-Basic Geomatrical</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Integers</a>
										                            </div>
											                    </div>
											                    <div class="item">
										                            <div class="notes_sing">
										                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
										                                <a>Mathematics-Fraction Data</a>
										                            </div>
											                    </div>
											                </div>
			                                            	<!--  -->
			                                            </div>
			                                        </section>
			                                    </div>
			                                    
											</div>
	                                    </div>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            	
            </div>
        </div>

        <!-- modal video -->
        <div class="modal" id="modal_vid">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title"></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
	        	<div id="vidBox">
				    <video id="demo" loop="" controls="" width="100%" height="100%">
				      <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
				    </video>
				</div>
		      </div>

		    </div>
		  </div>
		</div>
        <!-- modal video -->

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script type="text/javascript">
        	$(document).ready(function(){
			  //   $("#app_slider").slick({
			  //   	dots: true,
					// infinite: false,
					// speed: 300,
					// slidesToShow: 1,
					// slidesToScroll: 1,
			  //   });
			    $("#slider_catg").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 5,
					slidesToScroll: 1,
			    });
			    $("#slider_teach").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 3,
					slidesToScroll: 1,
			    });
			    $("#slider_notes").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 4,
					slidesToScroll: 2,
			    });
			    $("#slider_rel_vids").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 3	,
					slidesToScroll: 2,
			    });
			    $("#live_classes").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 3	,
					slidesToScroll: 2,
			    });
			    $("#slider_solved").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 4,
					slidesToScroll: 2,
			    });
			});
        </script>
    </body>
</html>