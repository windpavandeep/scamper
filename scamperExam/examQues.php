<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> ScamperSkill - Prep Exams </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon exam_page_ques_main"> <!-- change class -->
                            <div class="chpr_ttl_tme d-flex justify-content-between">
                                <h5>JEE Main Mock test - 1
                                <p>Test 2, Physics, Unit-3</p></h5>
                                <div>
                                    <span class="tme_crnt">Time Left: 58:29</span>
                                    <a class="instt_hr" data-toggle="modal" data-target="#inst_modal"><i class="fa fa-info"></i> instructions</a>
                                </div>
                            </div>
                            <div class="chpr_subss">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="javascript:;">Physics</a></li>
                                    <li class="list-inline-item"><a  class="active" href="javascript:;">Maths</a></li>
                                    <li class="list-inline-item"><a href="javascript:;">English</a></li>
                                    <li class="list-inline-item"><a href="javascript:;">Reasoning</a></li>
                                </ul>
                            </div>
                            <div class="wrpr_main_exam d-flex ">
                                <div class="main_left d-flex flex-column justify-content-start">
                                    <div class="question_activity">
                                        <div class="sngl_qust">
                                            <div class="quest_top">
                                                <h5>Q1. Which of the following are the skills required to understand philosophical concepts?</h5>
                                            </div>
                                            <div class="opt-mul">
                                                <ul>   
                                                    <li>
                                                        <div class="inpt_cus form-group">
                                                            <input type="" placeholder="Enter Answer here" class="form-control" name="">
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" name="rad" id="switch1">
                                                            <label class="custom-control-label" for="switch1">Memorising skills</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" name="rad" id="switch2">
                                                            <label class="custom-control-label" for="switch2">Highway codes for motor cars became a common sight on the highwayss</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" name="rad" id="switch3">
                                                            <label class="custom-control-label" for="switch3">Skills to differentiate between tone and pitch</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" name="rad" id="switch4">
                                                            <label class="custom-control-label" for="switch4">Handling crowding with the help of town authorities</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footr_exam">
                                        <div class=" d-flex justify-content-start">
                                            <button class="btnus btn_sav_nxt">Save & Next</button>
                                            <button class="btnus btn_outline_rev">Review Later</button>
                                            <button class="btnus btn_outline_clr">Clear Selection</button>
                                        </div>

                                        <div class="d-flex justify-content-between">
                                            <div class=" text-left">
                                                <button class="btnus btn_outline_prev"><i class="fa fa-long-arrow-alt-left"></i> Prev</button>
                                                <button class="btnus btn_outline_nex">Next <i class="fa fa-long-arrow-alt-right"></i></button>
                                            </div>
                                            <button class="btnus btn_end_exm">End Exam</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="main_right">
                                    <div class="nmbr_info">
                                        <ul class="ul_numb" type="none">
                                            <li><span class="comn_reco no_ans">1</span> Not Answered</li>
                                            <li><span class="comn_reco yes_ans">2</span> Answered</li>
                                            <li><span class="comn_reco no_vst">3</span> Not Visited</li>
                                            <li><span class="comn_reco no_vst">4</span> Not Visited</li>
                                            <li><span class="comn_reco mrk_rev">5</span> Marked Review</li>
                                        </ul>
                                    </div>
                                    <div class="question_serl">
                                        <ul class="ul_numb" type="none">
                                            <li><span class="comn_reco no_ans">1</span></li>
                                            <li><span class="comn_reco yes_ans">2</span></li>
                                            <li><span class="comn_reco no_vst">3</span></li>
                                            <li><span class="comn_reco no_vst">4</span></li>
                                            <li><span class="comn_reco no_vst">5</span></li>
                                            <li><span class="comn_reco mrk_rev">6</span></li>
                                            <li><span class="comn_reco no_vst">7</span></li>
                                            <li><span class="comn_reco no_vst">8</span></li>
                                            <li><span class="comn_reco no_vst">9</span></li>
                                            <li><span class="comn_reco no_vst">10</span></li>
                                            <li><span class="comn_reco no_vst">11</span></li>
                                            <li><span class="comn_reco no_vst">12</span></li>
                                            <li><span class="comn_reco no_vst">13</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
            </div>
        </div>

        <div class="loader_ques">
        	<img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
        </div>

        <div class="modal" id="inst_modal">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		      	<h4>JEE Main Mock test - 1
		      		<p>Test 2, Physics, Unit-3</p>
		      	</h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
		       		<div class="app_wrap_comon prep_exam_inst_sec"> <!-- change class -->
                        <div class="preInstr">
                            <div class="main_inst text-center">
                                <div class="inr_int">
                                    <h3 class="text-uppercase">Instructions</h3>
                                	<h5>Please read instructions carefully</h5>
                                </div>
                            	<ol>
                            		<li>Total duration for exam is 60 min.</li>
                            		<li>CLock wil be on server. When clock reach zero, exam will end itself and get submitted.</li>
                            		<li>Below are the color and symbol that will help you in the exam to understand.</li>
                            		<li>No time limit to attemp qusetuion in assignment.</li>
                                    <li>Type of question will be mentioned along with the questions.</li>
                                    <li>One question can be attempted only once.</li>
                            	</ol>
								<div class="btn_start">
									<a href="examQues.php" class="btn btn_gradient btn_active"> Start Test</a>
								</div>
                            </div>
                        </div>
                    </div>
		      </div>

		    </div>
		  </div>
		</div>

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">

        	$('.loader_ques').hide();
        	$('.btn_sav_nxt').click(function(){
        		$('.loader_ques').show();
        	});
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
            
			 $('.slider-for').slick({
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: true,
			  fade: true,
			  loops: false,
			  infinite: false,
			  asNavFor: '.slider-nav'
			});
			$('.slider-nav').slick({
			  loops: false,
			  slidesToShow: 6,
			  slidesToScroll: 1,
			  asNavFor: '.slider-for',
			  dots: false,
			  infinite: false,
			  // centerMode: true,
			  focusOnSelect: true
			});
        </script>
    </body>
</html>