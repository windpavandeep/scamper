<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ScamperSkill - Test Page </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">
        	

            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon app_home_sec"> <!-- change class -->
                            <div class="page_divider"> 
                                <div class="side_wid">
                                    <div class="sidebar_chd">
                                        <!--  -->
                                        <?php include('include/dashboardSidebar.php') ?>
                                        <!--  -->
                                    </div>
                                </div>
                                <div class="main_wid">
                                	<!-- header index -->
						        	<?php include('include/dashboardheader.php') ?>
						        	<!-- header index -->
                                    <div class="mainside_wrap">
                                        
                                        <section class="main_cntnt_dash page_div ">
                                            <div class="catg_div_sec padtb40">
                                            	<!--  -->
                                            	<div id="slider_catg" class="owl-carousel custm_slider">
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-book"></i></span>
							                                <p>Prep Test</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-file-alt"></i></span>
							                                <p>Exams</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-cart-plus"></i></span>
							                                <p>Buy Courses</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fas fa-comment-dots"></i></span>
							                                <p>Chat with Teacher</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-file-signature"></i></span>
							                                <p>Notes</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-download"></i></span>
							                                <p>Downloads</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="catg_wrap">
							                                <span><i class="fa fa-globe"></i></span>
							                                <p>Free Counselling</p>
							                            </div>
								                    </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>
			                                        
                                        <div class="card_shd">
                                        	<ul class="nav nav-tabs pos_rel" id="myTab" role="tablist">
												<li class="nav-item">
												    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Math</a>
												</li>
												<li class="nav-item">
												    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Science</a>
												</li>
												<li class="nav-item">
												    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Physics</a>
												</li>
												<div class="wrpa_hedr_cont">
													<ul class="d-flex align-items-center" type="none">
														<li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li>
														<li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
														<li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
													</ul>
												</div>
											</ul>
											<div class="tab-content" id="myTabContent">
	                                        	<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			                                                                            
			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="prep_strt_sec padtb40">
			                                            	<div class="sec_heading text-left d-flex justify-content-between">
																<h2 class="chg_font">Prep Test</h2>
															</div>
															<div class="selc_fltr">
	                                            				<form class="form-inline justify-content-between">
	                                            					<div class="wrap_selc">
		                                            					<select name="as" class="custom-select">
																		    <option selected>Select Subject</option>
																		    <option value="Maths">Maths</option>
																		    <option value="English">English</option>
																		    <option value="Chemistry">Chemistry</option>
																		    <option value="Physics">Physics</option>
																		</select>
																		<select name="cs" class="custom-select">
																		    <option selected>Select Units</option>
																		    <option value="Chapter 1">Maths</option>
																		    <option value="Chapter 2">English</option>
																		    <option value="Chapter 3">Chapter 3</option>
																		    <option value="Chapter 4">Chapter 4</option>
																		</select>
																	</div>
																	<div class="text-right">
				                                        				<a href="prepExam.php" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> Start Test</a>
				                                        			</div>
	                                            				</form>
		                                            		</div>
			                                            </div>
			                                        </section>

			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="sub_reslt_div_sec padtb40">
			                                            	<div class="sec_heading text-left d-flex justify-content-between">
																<h2 class="chg_font">Analysis Report</h2>
															</div>
			                                            	<!--  -->
			                                            	<div class="chrt_perfor">
			                                            		<div class="row">
			                                            			<div class="col-sm-3">
			                                            				<div class="wrp_chrt_cir">
    																		<h4>All Subjects</h4>
			                                            					<div class="chart chartb" data-percent="82" data-scale-color="#000"><span class="txt_abslt">82%</span></div>
    																		<p class="text-center">Very Good</p>
			                                            				</div>
			                                            			</div>
			                                            			<div class="col-sm-3">
			                                            				<div class="wrp_chrt_cir">
    																		<h4>Physics</h4>
			                                            					<div class="chart chartr" data-percent="46" data-scale-color="#ffb400"><span class="txt_abslt">46%</span></div>
			                                            					<p class="text-center">Poor</p>
			                                            				</div>
			                                            			</div>
			                                            			<div class="col-sm-3">
			                                            				<div class="wrp_chrt_cir">
    																		<h4>Chemistry</h4>
			                                            					<div class="chart chartg" data-percent="84" data-scale-color="#ffb400"><span class="txt_abslt">84%</span></div>
			                                            					<p class="text-center">Good</p>
			                                            				</div>
			                                            			</div>
			                                            			<div class="col-sm-3">
			                                            				<div class="wrp_chrt_cir">
    																		<h4>Biology</h4>
			                                            					<div class="chart charto" data-percent="95" data-scale-color="#ffb400"><span class="txt_abslt">95%</span></div>
			                                            					<p class="text-center">Good</p>
			                                            				</div>
			                                            			</div>
			                                            		</div>
			                                            	</div>
			                                            </div>
			                                        </section>

			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="grph_anal_sec padtb40">
			                                            	<div class="sec_heading text-left d-flex justify-content-between">
																<h2 class="chg_font">Graph Analysis</h2>
																<span class="wrap_selc">
	                                            					<select name="as" class="custom-select">
																	    <option selected>Select Subject</option>
																	    <option value="Maths">Maths</option>
																	    <option value="English">English</option>
																	    <option value="Chemistry">Chemistry</option>
																	    <option value="Physics">Physics</option>
																	</select>
																</span>
															</div>
			                                            	<!--  -->
			                                            	<div class="chrt_grph_anal">
			                                            		<div class="grph_alsis">
			                                            			<div id="chrt_grph"></div>
			                                            		</div>
			                                            	</div>
			                                            </div>
			                                        </section>

			                                        <section class="main_cntnt_dash page_div">
			                                            <div class="grph_score_sec padtb40">
			                                            	<div class="sec_heading text-left d-flex justify-content-between">
																<h2 class="chg_font">Score Analytics</h2>
															</div>
			                                            	<!--  -->
			                                            	<div class="chrt_score_anal">
			                                            		<div class="grph_alsis col-sm-8">
			                                            			<div id="chrt_scor"></div>
			                                            		</div>
			                                            	</div>
			                                            </div>
			                                        </section>

			                                    </div>
			                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
			                                    	<!-- content goes here -->
			                                    	<div class="no_cnt text-center">
			                                    		<h2>Science</h2>
			                                    		<p class="">No data Yet</p>
			                                    	</div>
			                                    </div>
												<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
			                                    	<!-- content goes here -->
			                                    	<div class="no_cnt text-center">
			                                    		<h2>Physics</h2>
			                                    		<p class="">No data Yet</p>
			                                    	</div>
												</div>
											</div>
	                                    </div>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            	
            </div>
        </div>

        <!-- modal video -->
        <div class="modal" id="modal_vid">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title"></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
	        	<div id="vidBox">
				    <video id="demo" loop="" controls="" width="100%" height="100%">
				      <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
				    </video>
				</div>
		      </div>

		    </div>
		  </div>
		</div>
        <!-- modal video -->

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script> <!-- circular chart -->

        <!-- Resources -->
		<script src="https://www.amcharts.com/lib/4/core.js"></script>
		<script src="https://www.amcharts.com/lib/4/charts.js"></script>
		<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

		<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
		<!-- Chart code -->
		<script>
			am4core.ready(function() {

			am4core.useTheme(am4themes_animated);

			var chart = am4core.create('chrt_grph', am4charts.XYChart)
			chart.colors.step = 5;

			chart.legend = new am4charts.Legend()
			chart.legend.position = 'right'
			chart.legend.paddingBottom = 20
			chart.legend.labels.template.maxWidth = undefined

			var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
			xAxis.dataFields.category = 'category'
			xAxis.renderer.cellStartLocation = 0.1
			xAxis.renderer.cellEndLocation = 0.9
			xAxis.renderer.grid.template.location = 0;

			var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
			yAxis.min = 0;

			function createSeries(value, name) {
			    var series = chart.series.push(new am4charts.ColumnSeries())
			    series.dataFields.valueY = value
			    series.dataFields.categoryX = 'category'
			    series.name = name

			    series.events.on("hidden", arrangeColumns);
			    series.events.on("shown", arrangeColumns);

			    var bullet = series.bullets.push(new am4charts.LabelBullet())
			    bullet.interactionsEnabled = false
			    bullet.dy = 3;
			    bullet.label.text = '{valueY}'
			    bullet.label.fill = am4core.color('#ffffff')

			    return series;
			}

			chart.data = [
			    {
			        category: 'Jan 2019',
			        first: 40,
			        second: 55,
			        third: 60
			    },
			    {
			        category: 'Feb 2019',
			        first: 30,
			        second: 78,
			        third: 69
			    },
			    {
			        category: 'Mar 2019',
			        first: 27,
			        second: 40,
			        third: 45
			    },
			    {
			        category: 'Apr 2019',
			        first: 50,
			        second: 33,
			        third: 22
			    }
			]
			createSeries('first', 'Total Attempt');
			createSeries('second', 'Avg Score');
			createSeries('third', 'Remarks');

			function arrangeColumns() {

			    var series = chart.series.getIndex(0);

			    var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
			    if (series.dataItems.length > 1) {
			        var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
			        var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
			        var delta = ((x1 - x0) / chart.series.length) * w;
			        if (am4core.isNumber(delta)) {
			            var middle = chart.series.length / 2;

			            var newIndex = 0;
			            chart.series.each(function(series) {
			                if (!series.isHidden && !series.isHiding) {
			                    series.dummyData = newIndex;
			                    newIndex++;
			                }
			                else {
			                    series.dummyData = chart.series.indexOf(series);
			                }
			            })
			            var visibleCount = newIndex;
			            var newMiddle = visibleCount / 2;

			            chart.series.each(function(series) {
			                var trueIndex = chart.series.indexOf(series);
			                var newIndex = series.dummyData;

			                var dx = (newIndex - trueIndex + middle - newMiddle) * delta

			                series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
			                series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
			            })
			        }
			    }
			}
			}); // end am4core.ready()
		</script>

		<script>
			window.onload = function () {

			//Better to construct options first and then pass it as a parameter
			var options = {
				animationEnabled: true,
				title: {
					// text: "Mobile Phones Used For",                
					// fontColor: "black"
				},	
				axisY: {
					tickThickness: 0,
					lineThickness: 0,
					valueFormatString: " ",
					gridThickness: 0                    
				},
				axisX: {
					tickThickness: 0,
					lineThickness: 0,
					labelFontSize: 18,
					labelFontColor: "black"				
				},
				data: [{
					indexLabelFontSize: 15,
					// toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
					indexLabelPlacement: "inside",
					indexLabelFontColor: "white",
					indexLabelTextAlign: "right",
					indexLabelFontWeight: 400,
					color: "#62C9C3",
					type: "bar",
					dataPoints: [
						{ y: 29, label: "Poor", indexLabel: "43%" },
						{ y: 32, label: "Good", indexLabel: "67%" },
						{ y: 64, label: "Very Good", indexLabel: "78%" },
						{ y: 73, label: "Excellent", indexLabel: "95%" }
					]
				}]
			};

			$("#chrt_scor").CanvasJSChart(options);
			}
		</script>

        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script type="text/javascript">
        	$(document).ready(function(){
			    $("#slider_catg").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 5,
					slidesToScroll: 1,
			    });
			    $("#slider_notes").slick({
			    	dots: false,
					infinite: true,
					speed: 300,
					slidesToShow: 4,
					slidesToScroll: 2,
			    });
			  	$(function() {
				  $('.chartb').easyPieChart({
				    size: 160,
				    barColor: "blue",
				    scaleLength: 0,
				    lineWidth: 10,
				    // trackColor: "#373737",
				    lineCap: "circle",
				    animate: 2000,
				  });
				  $('.chartr').easyPieChart({
				    size: 160,
				    barColor: "red",
				    scaleLength: 0,
				    lineWidth: 10,
				    // trackColor: "#373737",
				    lineCap: "circle",
				    animate: 2000,
				  });
				  $('.chartg').easyPieChart({
				    size: 160,
				    barColor: "green",
				    scaleLength: 0,
				    lineWidth: 10,
				    // trackColor: "#373737",
				    lineCap: "circle",
				    animate: 2000,
				  });
				  $('.charto').easyPieChart({
				    size: 160,
				    barColor: "orange",
				    scaleLength: 0,
				    lineWidth: 10,
				    // trackColor: "#373737",
				    lineCap: "circle",
				    animate: 2000,
				  });
				});
			});
        </script>
    </body>
</html>