<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ScamperSkill - Live Video Lectures </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
       	<link rel="stylesheet" href="https://cdn.plyr.io/3.5.10/plyr.css" />
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">
            <div class="app_dash_wraper">
            	<div class="full_vid_lect pos_rel d-flex">
        			<div class="pos_rel main_vid_lect">
        				<div class="top_headr_pos">
	            			<h2><a href="liveLectures.php"><i class="fa fa-long-arrow-alt-left"></i></a> Mathematics- Knowing your Numbers</h2>
	            			<p class="hdr_pp">Lesson 2 - Acedmeic maths and Resoning ~ Dr. Sidharth Raj	</p>
	            		</div>
						 <video poster="https://i.ytimg.com/vi/CwwrCBJDulY/maxresdefault.jpg" id="player" playsinline controls autoplay="">
						    <source src="https://www.youtube.com/watch?v=_4h0MS_DL90" type="video/mp4" />
						    <source src="https://www.youtube.com/watch?v=_4h0MS_DL90" type="video/webm" />

						    <!-- Captions are optional -->
						    <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default />
						</video>
        			</div>
        			<div class="pos_rel side_vid_lect">
        				<div class="teacher_vid">
							<video poster="https://images.squarespace-cdn.com/content/v1/5af308849d5abbb875f94e0f/1585318431557-5BCA54WRRKG0UHARYAE9/ke17ZwdGBToddI8pDm48kHnB9AHThyFVVWEA0DlUcxJ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QHyNOqBUUEtDDsRWrJLTmW5SZw0zY0wEgct1jJcv4nOnDrf-ERx9EdYQ5RymI9h93BLB1LuY2-tBmF2qKdG7R/Josh+Ayers.jpg" id="player" playsinline controls autoplay="">
							    <source src="https://www.youtube.com/watch?v=_4h0MS_DL90" type="video/mp4" />
							    <source src="https://www.youtube.com/watch?v=_4h0MS_DL90" type="video/webm" />

							    <!-- Captions are optional -->
							    <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default />
							</video>
        				</div>
        				<div class="comnt_sec pos_rel">
        					<form class="form-inline">
        						<input type="" name="" placeholder="Write comment. . ." />
        						<button class="btn btn-primary">Send</button>
        					</form>
        					<div class="user_cmnt">
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/profile2.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">John Doe</h4>
										    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/profile1.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Murari Lal</h4>
										    <p><i class="fa fa-heart"></i> <i class="fa fa-heart"></i> <i class="fa fa-heart"></i> <i class="fa fa-heart"></i></p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/g2.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Ankita</h4>
										    <p>Nice Lesson Sir!</p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/profile1.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Rohit Kumar</h4>
										    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/g1.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Neha Mishra</h4>
										    <p>Gud mrng Sir <i class="fa fa-sun"></i></p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/profile1.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Raj Malhotra</h4>
										    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/g2.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Monika Negi</h4>
										    <p>Sir, can u repeat the question 1 please?</p>
										</div>
									</div>
        						</div>
        						<div class="sing_sur_cmt">
        							<div class="media">
										<div class="media-left media-top">
										    <img src="img/profile2.jpg" class="media-object" style="width:60px">
										  </div>
										  <div class="media-body">
										    <h4 class="media-heading">Nikesh Jha</h4>
										    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
										</div>
									</div>
        						</div>
        					</div>
        				</div>
        			</div>
	            </div>
            </div>
        </div>

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script src="https://cdn.plyr.io/3.5.10/plyr.js"></script>
		<script>
		    const player = new Plyr('#player');
		</script>

        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>