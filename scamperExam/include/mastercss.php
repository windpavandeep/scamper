<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="" />
<link rel="stylesheet" href="css/bootstrap.css"> <!-- Bootstrap-Core-CSS -->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<link href="css/slick.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/slick-theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap-select.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://vjs.zencdn.net/7.7.5/video-js.css" rel="stylesheet" />

<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/responsive-style.css" rel='stylesheet' type='text/css' />