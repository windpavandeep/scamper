<!-- footer page -->
<!-- Footer-section -->
	<footer class="footer-sec" id="footer">
	<div class="col-md-12 col-sm-12 col-xs-12 p-0">
		<div class="footer-bottm">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
						<h3> COURSES </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="#"> CBSE </a></li>
							<li><a href="#"> ICSE </a></li>
							<li><a href="#"> CAT </a></li>
							<li><a href="#"> IAS </a></li>
							<li><a href="#"> JEE </a></li>
							<li><a href="#"> NEET </a></li>
							<li><a href="#"> GRE </a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
							<h3> EXAMS </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="#">CAT Exam</a></li>
							<li><a href="#">IAS Exams</a></li>
							<li><a href="#">Bank Exams</a></li>
							<li><a href="#">UPSC Syllabus</a></li>
							<li><a href="#">UPSC 2019</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
						<h3> EXAM PREPARATION </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="#">CAT Exam Preparation</a></li>
							<li><a href="#">IAS Exams Preparation</a></li>
							<li><a href="#">Bank Exams Preparation</a></li>
							<li><a href="#">UPSC Syllabus Preparation</a></li>
							<li><a href="#">UPSC 2019 Preparation</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
						<h3> Classes </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="#">Class 4th - 5th</a></li>
							<li><a href="#">Class 4th - 5th</a></li>
							<li><a href="#">Class 4th - 5th</a></li>
							<li><a href="#">Class 4th - 5th</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
						<h3> FREE RESOURCES </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="#">NCERT Solutions</a></li>
							<li><a href="#">NCERT Solutions for Class 6</a></li>
							<li><a href="#">NCERT Solutions for Class 7</a></li>
							<li><a href="#">NCERT Solutions for Class 8</a></li>
							<li><a href="#">NCERT Solutions for Class 9</a></li>
							<li><a href="#">NCERT Solutions for Class 10</a></li>
							<li><a href="#">NCERT Solutions for Class 11</a></li>
							<li><a href="#">NCERT Solutions for Class 12</a></li>
							<li><a href="#">RS Aggarwal Solutions</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
					<div class="wrap-div footer-min">
						<h3> Company </h3>
						<ul type="none" class="footer-content-list p-0">
							<li><a href="contact-Us.html"> Contact us </a></li>
							<li><a href="about-us.html"> About us </a></li>
							<li><a href="career.html"> Investors</a></li>
							<li><a href="#"> Brochures</a></li>
							<li><a href="#">Site map</a></li>
							<li><a href="#"> Privacy </a></li>
							<li><a href="#"> Legal </a></li>
							<li><a href="#"> Terms & Condtions </a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
	<!-- footer section end -->
<!-- footer page -->