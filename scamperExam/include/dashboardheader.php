<header class="app_header">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarNavDropdown">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <div class="close_side">
                            <i class="fa fa-times closd"></i>
                            <i class="fa fa-bars opend"></i>
                        </div>
                    </li>
                    <li class="nav-item">
                        <div class="srch_headr pos_rel">
                            <i class="fa fa-search"></i>
                            <input type="text" placeholder="Search" name="">
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav align-items-center">
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:;"><i class="fas fa-bell"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:;"><i class="fas fa-question-circle"></i></a>
                    </li>
                    <li class="name_item nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="drop-prof" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <img src="img/profile2.jpg" class="img-fluid" alt="Trainer Image">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="drop-prof">
                          <a class="dropdown-item" href="javascript:;">Profle</a>
                          <a class="dropdown-item" href="javascript:;">Settings</a>
                          <a class="dropdown-item" href="javascript:;">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>