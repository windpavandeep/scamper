<div class="logo_side">
    <a class="navbar-brand logo_ch" href="appHome.php"><img src="img/logo.png" class="img-fluid" alt="Logo" /></a>
</div>
<div class="side_menu">
    <div class="pic_top pos_rel text-center">
        <div class="pos_rel pic_pos">
            <img src="img/profile2.jpg" class="img-fluid" alt="">
        </div>
        <h4 class="name_db">Chiranjit Debnath</h4>
    </div>
    <ul type="none" class="ul_side">
        <li class="active"><a href="javascript:;"><i class="far fa-chart-bar"></i> <span class="side_span">Home</span></a></li>
        <li><a href="javascript:;"><i class="far fa-file-excel"></i> <span class="side_span">Exam Progress Report</span></a></li>
        <li><a href="javascript:;"><i class="far fa-video"></i> <span class="side_span">Tutorials & Exams</span></a></li>
        <li><a href="allTrainers.php"><i class="far fa-users"></i> <span class="side_span">Trainers</span></a></li>
        <li><a href="liveLectures.php"><i class="far fa-users"></i> <span class="side_span">Live Lectures</span></a></li>
        <li><a href="teachTalk.php"><i class="far fa-comment"></i> <span class="side_span">Chat with Teacher</span></a></li>
        <li><a href="javascript:;"><i class="fa fa-download"></i> <span class="side_span">Downlaods</span></a></li>
        <li><a href="javascript:;"><i class="far fa-file-pdf"></i> <span class="side_span">T & C</span></a></li>
        <li><a href="javascript:;"><i class="far fa-bell"></i> <span class="side_span">Notifications</span></a></li>
        <li><a href="javascript:;"><i class="fas fa-sign-out-alt"></i> <span class="side_span">Logout</span></a></li>
    </ul>
</div>
