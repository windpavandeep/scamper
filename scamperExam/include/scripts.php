<script type="text/javascript" src="js/jquery-2.2.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 

<script src="js/slick.js"></script>
<script src="js/waypoints.js"></script>
<script src="js/counterup.js"></script>
<script src="js/bootstrap-select.js"></script>
<script src="js/moment.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="https://vjs.zencdn.net/7.7.5/video.js"></script>

<script defer src="js/wow.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.niceselc').selectpicker();
        $('.dtpckr').datepicker();
    });
</script>
<!-- <script src="js/jquery.backstretch.js"></script>
<script>
    $.backstretch([
      "img/bg1.jpg",
      "img/bg2.jpg",
      "img/bg3.jpg"
    ], {
        fade: 750,
        duration: 3000
    });
</script> -->

<script type="text/javascript">
	$('.counter').counterUp({
	    delay: 10,
	    time: 1000
	});
</script>

<script type="text/javascript">
	$('#homeSlider').slick({
		  dots: true,
		  infinite: true,
		  speed: 300,
		  autoplay: true,
  			autoplaySpeed: 2000,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 1,
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		      }
		    }
		  ]
		});
</script>
<script>
	wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
        	console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
	wow.init();
</script>

<script>
	// $(window).on("scroll load" , function() {
	// 	if ($(this).scrollTop() > 100){  
	// 		$('.header').addClass("fixed_hedr");
	// 		$('.logo_ch').find("img").attr('src', 'img/logo.png');
	// 	}
	// 	else {
	// 		$('.header').removeClass("fixed_hedr");
	// 		$('.logo_ch').find("img").attr('src', 'img/logow.png');
	// 		$('.innr_hedr .logo_ch').find("img").attr('src', 'img/logo.png');
	// 	}
	// });
	$('.closd').hide();
	$('.page_divider').removeClass('added');
	$('.opend').click(function(){
		$(this).hide();
		$('.closd').show();
		$('.page_divider').addClass('added');
	});
	$('.closd').click(function(){
		$(this).hide();
		$('.opend').show();
		$('.page_divider').removeClass('added');
	});
</script>
<script type="text/javascript">
	$( document ).ready( function () {
	    $( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
	        var $el = $( this );
	        var $parent = $( this ).offsetParent( ".dropdown-menu" );
	        if ( !$( this ).next().hasClass( 'show' ) ) {
	            $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
	        }
	        var $subMenu = $( this ).next( ".dropdown-menu" );
	        $subMenu.toggleClass( 'show' );
	        
	        $( this ).parent( "li" ).toggleClass( 'show' );

	        $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
	            $( '.dropdown-menu .show' ).removeClass( "show" );
	        } );
	        
	         if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
	            $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
	        }

	        return false;
	    } );
	} );
</script>