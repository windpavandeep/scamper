<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> ScamperSkill - Chat with Teacher </title>
        <!-- css here -->
       	<?php include('include/mastercss.php') ?>
        <!-- css here -->
    </head>
    <body>

        <div class="page-wrapper">
            <div class="app_dash_wraper">

                <section class="sec_dashboard db_main">
                    <div class="page_container">
                        <div class="app_wrap_comon app_teaTalk_sec"> <!-- change class -->
                            <div class="page_divider"> 
                                <div class="side_wid">
                                    <div class="sidebar_chd">
                                        <!--  -->
                                        <?php include('include/dashboardSidebar.php') ?>
                                        <!--  -->
                                    </div>
                                </div>
                                <div class="main_wid">
                                	<!-- header index -->
						        	<?php include('include/dashboardheader.php') ?>
						        	<!-- header index -->
                                    <div class="mainside_wrap">
                                        <!--  -->
                                        <section class="main_cntnt_dash page_div card_shd">
                                            <div class="catg_div_sec padtb40">
                                            	<!--  -->
                                            	<div id="teachers_slider" class="owl-carousel custm_slider">
								                    <div class="item">
							                            <div class="teach_face chatting">
							                                <span class="active">
							                                	<img src="img/profile1.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Kuldeep Singh</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span class="active">
							                                	<img src="img/g1.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Neha Sharma</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span class="active">
							                                	<img src="img/profile2.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Anuj mishra</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span class="active">
							                                	<img src="img/profile3.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>KL Choudry</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/g2.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Shera Leepa</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/profile1.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Roy Jindal</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/profile2.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Anuj mishra</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span class="active">
							                                	<img src="img/profile3.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>KL Choudry</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/g2.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Shera Leepa</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/profile1.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Roy Jindal</p>
							                            </div>
								                    </div>
								                    <div class="item">
							                            <div class="teach_face">
							                                <span>
							                                	<img src="img/profile2.jpg" class="img-fluid">
							                                	<i class="fa fa-circle"></i>
							                                </span>
							                                <p>Anuj mishra</p>
							                            </div>
								                    </div>
								                </div>
                                            	<!--  -->
                                            </div>
                                        </section>
                                        <section class="main_cntnt_dash page_div card_shd">
                                            <div class="talk_box padtb40">
                                            	<!--  -->
			                                        <div class="mesagees_wrap">
			                                            <div class="chat-body"> 
			                                                <div class="top-head_in">
			                                                    <img class="img-responsive" src="img/profile2.jpg" />
			                                                    <span class="right_inf">
			                                                        <h3 class="nam_ch">Anuj Mishra <sub>(Mathematics)</sub> </h3>
			                                                        <span class="act_stts active"><i class="fa fa-circle"></i> Active Now </span>
			                                                    </span>
			                                                </div>
			                                                <ul class="my-chat-convo clearfix">
			                                                    <li class="msg-box pull-left">
			                                                        <div class="li_msg">
			                                                            <p>Hello</p>
			                                                            <span class="msg-time">07:45 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-right">
			                                                        <div class="li_msg">
			                                                            <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			                                                            <span class="msg-time">07:50 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-left">
			                                                        <div class="li_msg">
			                                                            <p>Hello</p>
			                                                            <span class="msg-time">07:45 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-right">
			                                                        <div class="li_msg">
			                                                            <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			                                                            <span class="msg-time">07:50 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-right">
			                                                        <div class="li_msg">
			                                                            <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			                                                            <span class="msg-time">07:50 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-right">
			                                                        <div class="li_msg">
			                                                            <p>consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			                                                            <span class="msg-time">07:51 PM</span>
			                                                        </div>
			                                                    </li>
			                                                    <li class="msg-box pull-left">
			                                                        <div class="li_msg">
			                                                            <p>Typing . . .</p>
			                                                            <!-- <span class="msg-time">07:45 PM</span> -->
			                                                        </div>
			                                                    </li>
			                                                </ul>
			                                                <div class="write-msg-content">
			                                                    <!-- <form> -->
			                                                    <span class="fa fa-paperclip pos_rel">
			                                                    	<input type="file" class="atch_in" name="">
			                                                    </span>
		                                                        <input type="text" class="form-control input_fit" name="msg" placeholder="Write a Message" value="" />
		                                                        <button class="btn btn-green msg" type="button"><i class="fa fa-paper-plane"></i></button>
			                                                    <!-- </form> -->
			                                                </div>
			                                            </div>
			                                        </div>
			                                    <!--  -->
                                            </div>
                                        </section>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>

        <!-- modal video -->
        <div class="modal" id="modal_vid">
		  <div class="modal-dialog modal-lg">
		    <div class="modal-content">

		      <!-- Modal Header -->
		      <div class="modal-header">
		        <h4 class="modal-title"></h4>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>

		      <!-- Modal body -->
		      <div class="modal-body">
	        	<div id="vidBox">
				    <video id="demo" loop="" controls="" width="100%" height="100%">
				      <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
				    </video>
				</div>
		      </div>

		    </div>
		  </div>
		</div>
        <!-- modal video -->

        <!-- script files -->
        <?php include('include/scripts.php') ?>
        <!-- script files -->
        <script type="text/javascript">
            $(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <script type="text/javascript">
        	$(document).ready(function(){
			    $("#teachers_slider").slick({
			    	dots: false,
			    	prevArrow: false,
    				nextArrow: false,
					infinite: false,
					speed: 300,
					slidesToShow: 7,
					slidesToScroll: 4,
			    });
			});
        </script>
        <script type="text/javascript">
            var get_height = $('.my-chat-convo').prop('scrollHeight');
            $('.my-chat-convo').animate({scrollTop: get_height}, 1200);



            $('.msg').on('click',function(){
                var cur_msg = $(this).closest('.write-msg-content').find('input').val();
                var cur_date = new Date();
                var h =  cur_date.getHours(), m = cur_date.getMinutes();
                var time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
                var da_tm = time;

                $('.my-chat-convo').append('<li class="msg-box pull-right"><div class="li_msg"><p> '+ cur_msg +' </p><span class="msg-time"> '+ da_tm +' </span></div></li>');

                var get_height = $('.my-chat-convo').prop('scrollHeight');      
                $('.my-chat-convo').animate({scrollTop: get_height}, 1200);
            });
        </script>
    </body>
</html>