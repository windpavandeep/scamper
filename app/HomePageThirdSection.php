<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePageThirdSection extends Model
{
    protected $table = 'home_page_third_section';
}
