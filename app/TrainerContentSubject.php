<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerContentSubject extends Model
{
    
    public function content_unit(){
        return $this->hasMany('App\TrainerContentSubjectUnit','trainer_content_subject_id','id');
    }
}
