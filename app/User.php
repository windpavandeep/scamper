<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth,Mail;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function country(){
        return $this->hasOne('App\Country','id','country_id')->select('id','name');
    }
    public function state(){
        return $this->hasOne('App\State','id','state_id')->select('id','name');
    }
    public function city(){
        return $this->hasOne('App\City','id','city_id')->select('id','name');
    }

    public static function change_password($data){
        
        $email = User::where('id',Auth::User()->id)->value('email');

        if($data['new'] != $data['confirm']){
            return 'pass_not_match';
        }

        if(Auth::attempt(['email'=>$email, 'password'=>$data['current']])){
            $hashPassword       = bcrypt($data['new']);
            $nodeHash           = str_replace('$2y$','$2a$',$hashPassword);

            $pw = User::where('id',Auth::User()->id)->update(['password'=>$nodeHash]);
            if($pw){
                return 'true'; 
            }else{
                return 'false';  
            }
        } else{
            return 'invalid_curr_pass';
        }
    }

    public static function sendCredentialMail($user_id = null,$type = null ){ 

        $details = User::where('id',$user_id)
                        ->first();
                        // dd($user_id);
        $security_code          = uniqid();
        $user_id                = base64_encode(convert_uuencode($user_id));
        $company_name           = PROJECT_NAME;
        $email                  = $details->email;
        $name                   = ucfirst($details->first_name).' '.ucfirst($details->last_name);
        $details->security_code = $security_code;
        if($details->save())
        {   
            $security_code    = base64_encode(convert_uuencode($security_code));
            $set_password_url = url('/set-password'.'/'.$user_id.'/'.$security_code);
            if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
            {   

                if($type == 'forgot'){
                    Mail::send('emails.admin_forgot_password', ['name'=>$name,'set_password_url'=>$set_password_url],function($message) use ($email,$company_name){
                        $message->to($email,$company_name)->subject('Forgot your password?');
                    });
                    // return true;
                }else{

                    Mail::send('emails.set_password', ['name'=>$name,'set_password_url'=>$set_password_url], function($message) use ($email,$company_name)
                    {   
                        $message->to($email,$company_name)->subject('Scamper Skills Set Password Mail');
                    });
                }
                return true;
            }         
        }
        return false;
        
    } 

    public static function sendDiscountCouponMail($data= null){
        
                                          // ->toArray();              
        if(!empty($data['email'])){
            foreach($data['email'] as $email){ 

                $user         = User::where('email',$email)->first();
                $user_name    = $user->first_name.' '.$user->last_name;
           
                $company_name = PROJECT_NAME;
                
                $desc = $data['description'];                     
                if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){

                    Mail::send('emails.discount_coupon_mail',['user_name'=>$user_name, 'email'=>$email, 'desc'=>$desc],function($message) use ($email,$company_name){
                                $message->to($email,$company_name)->subject('Scamper Skills Discount Coupon');
                    });
                }
            }
            return true;
        }else{
            return false;
        }                                  
    }

    public static function desktop_notification($banner_id=null){

        $users = User::select('users.*','ud.device_token','ud.user_id','uc.sub_category_id')
                     ->join('user_devices as ud','ud.user_id','users.id')
                     ->join('user_courses as uc','uc.user_id','users.id')
                     ->where('users.user_type','user')
                     ->whereNull('users.deleted_at')
                     ->get()
                     ->toArray();
                     // dd($users);
        if(!empty($banner_id)){
            $get_banner = BannerImage::where('id',$banner_id)->first();

                     // dd($banner_id);
            foreach ($users as $key => $user) {
                if($user['sub_category_id']==$get_banner->sub_category_id){

                    $user_name = ucfirst($user['first_name']).' '.ucfirst($user['last_name']);
                    $message   = "A new banner added.";
                    if(file_exists(HomepageBannerBasePath.'/'.$get_banner->image)){
                        $image = HomepageBannerImgPath.'/'.$get_banner->image;
                    }else{
                        $image = DefaultImgPath;
                    }
                    $url       = url($image);
                    // dd($url);
                    $msg = array(
                        "image"=> $url,
                        'body'  => $message,
                        'title' =>'New Notification from Scamper Skills',
                        'receiver'=>$user_name,
                        'icon'  =>url("http://scamperskills.com/public/images/system/logo.png"),                    /*Default Icon*/
                        'sound' => 'default',/*Default sound*/
                        'type'=>'banner',
                        // 'click_action' => $url,
                    );
                    
                    $token = "AAAAuaa7yIo:APA91bHEXFNby3VNdumSba9lGvaaNro0pyXepJ_s94Zp053jZWlhFlXT9gAoQbh2pYg8cI0GMXeQu3epk9eUPq1mbZVOOmSi9H7vXyQ7s0M6DVeyAphNSFW5u7ac11nnrQhl7nVChdV5";
                    $token2=$user['device_token'];
                    $headers = array(
                                'Authorization: key=' . $token,
                                'Content-Type: application/json'
                            );
                    $fields = array(
                        'to'          => $token2,
                        'notification'=> $msg,
                        'image_url'   =>'http://scamperskills.com/public/images/system/logo.png', 
                        'data' => array("type" => 'banner',"course_id"=>$get_banner->course_id)
                    );
                    $ch = curl_init();
                    curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
                    curl_setopt( $ch,CURLOPT_POST, true );
                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                    $result = curl_exec($ch );
                    // dd($result);
                    curl_close($ch);
                }else{
                    continue;
                }
                
            }             
        }
    }

    public static function send_notification($exam_id=null,$product_id=null){

        $users = User::select('users.*','uc.sub_category_id')
                     ->join('user_courses as uc','uc.user_id','users.id')
                     ->get()
                     ->toArray();

        if(!empty($exam_id)){
            $exam = Exam::where([
                            'id'=>$exam_id,
                            'deleted_at'=>null
                        ])
                         ->first();
            foreach ($users as $key => $user) {
                if($user['sub_category_id']==$exam->sub_categories_id){
                    $notification          = new Notification;
                    $notification->user_id = $user['id'];
                    $notification->title = 'A new exam is added.';
                    $notification->type    = 'new_exam';  
                    $notification->save();
                }else{
                    continue;
                }
            }
        }else{
            $course = TrainerContent::where([
                                        'id'=>$product_id,
                                        'deleted_at'=>null
                                  ])
                                   ->first();
            foreach ($users as $key => $user) {
                if($user['sub_category_id']==$course->sub_category_id){
                    $notification          = new Notification;
                    $notification->user_id = $user['id'];
                    $notification->title = 'A new course is added.';
                    $notification->type    = 'new_exam';  
                    $notification->save();
                    // dd($notification);
                }else{
                    continue;
                }
            }
        }

    }
    public function device_tokken(){

        return $this->hasMany('App\UserDevice','user_id','id');
    }

    public function user_courses(){

        return $this->hasOne('App\UserCourse','user_id','id');
    }
}
