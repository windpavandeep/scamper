<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuccessStory extends Model
{
    public static function get_success_stories(){

    	$success_stories = SuccessStory::where('type','S')->get()->toArray();
    	return $success_stories;
    }

    public static function get_our_faculties(){

    	$our_faculties = SuccessStory::where('type','O')->get()->toArray();
    	return $our_faculties;
    }
}
