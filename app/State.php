<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public static function get_states(){

    	$states  = State::select('*')->get()->toArray();
    	return $states;
    }
}
