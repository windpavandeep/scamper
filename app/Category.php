<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    
    public static function get_category_details(){

    	$categories_details = Category::with(['sub_categories'=>function($query){
                                            $query->whereNull('deleted_at'); 

                                      
                                       },'sub_categories.sub_sub_categories'=>function($query){
                                            $query->whereNull('deleted_at');
                                        }
                                       ])
                                      ->whereNull('deleted_at')
                                      ->get()
                                      ->toArray();
		    						   // dd($categories_details);

   		return $categories_details;
    }

    public function sub_categories(){

    	return $this->hasMany('App\SubCategory','category_id','id')->whereNull('deleted_at');
    }

    public static function get_categories(){

        $categories = Category::select('*')
                               ->whereNull('deleted_at')
                               ->get()
                               ->toArray();
        return $categories; 
    }

}
