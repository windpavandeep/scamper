<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePageFirstSection extends Model
{
    protected $table = 'home_page_first_section';
}
