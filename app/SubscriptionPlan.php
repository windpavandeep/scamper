<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
	public function trainer_detail(){
		return $this->hasOne('App\User','id','trainer_id');	
	}
}
