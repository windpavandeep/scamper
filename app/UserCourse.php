<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{
	public function user(){
		return $this->hasOne('App\User','id','user_id')->whereNull('deleted_at')->where('status','A')->where('user_type','user');
	}
}
