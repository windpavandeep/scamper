<?php

namespace App\Http\Controllers\frontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam,App\User,App\Notification;
use App\UserExam,App\UserCourse,Mail;
class CronJobController extends Controller
{

	public function exam_result(){
		$exams = UserExam::select('user_exams.id','user_id','e.deleted_at','result_notification_sent','start_date','expiry_date','start_time','end_time','e.name','u.first_name','u.last_name','e.id as user_exam_id')
						 ->join('exam as e','e.id','user_exams.exam_id')
					     ->join('users as u','u.id','user_exams.user_id')
						 ->whereNull('e.deleted_at')
						 ->whereNull('u.deleted_at')
						 ->where('status','A')
						 ->where('user_exams.result_notification_sent','no')
						 ->get()
						 ->toArray();
		// dd($exams);
		// echo'<pre>'; print_r($exams);die;
		foreach ($exams as $key => $exam) {

			if(!empty($exam['start_date'])||!empty($exam['expiry_date'])){
				// $start_date = $exam['start_date'].' '.$exam['start_time'].':00:00';
				$end_date = $exam['expiry_date'].' '.$exam['end_time'].':00:00';
				$end_date = strtotime(date('Y-m-d H:i:s',strtotime($end_date)));
				$today    = strtotime(date('Y-m-d H:i:s'));
				// dd($today);
				/*if($today>=$end_date){

				dd('enter');
				}*/
				if($today>=$end_date){
					$notification = new Notification;
					$notification->user_id = $exam['user_id'];
					$notification->title = 'Hello'.' '.$exam['first_name'].' '.$exam['name'].' exam result is declared.' ;
					$notification->type = 'exam_result';
					if($notification->save()){
						$update 	= UserExam::where('id',$exam['id'])->first();
						$update->result_notification_sent = 'yes';
						$update->save();
					}else{
						return redirect()->back()->with('error',COMMON_ERROR);
					}
				}
			}
		}
	}

	public function exam_notification(){
		$exams = Exam::with('user_courses.user')
					  ->whereHas('user_courses.user')
					  ->whereNull('deleted_at')
		        	  ->where('exam_notification_sent','no')
					  ->get()
					  ->toArray();
					  
		// dd($exams);
		// echo'<pre>'; print_r($exams);die;
		foreach ($exams as $key => $exam) {

			if(!empty($exam['start_date'])||!empty($exam['start_time'])){

				$start_date = $exam['start_date'].' '.$exam['start_time'].':00:00';
				$exam_date  = date('d-m-Y H:i:sa',strtotime($start_date));
				// dd($exam_date);
				$start_date = strtotime(date('Y-m-d H:i:s',strtotime($start_date)));
				$today      = strtotime(date('Y-m-d H:i:s'));
				$diff = $start_date - $today;
				$hours = $diff / ( 60 * 60 );
				// dd(abs($hours));
				if(abs($hours)<=24){
					foreach ($exam['user_courses'] as $key => $value) {
						if(!empty($value['user'])){
							$notification = new Notification;
							$notification->user_id = $value['user']['id'];
							$notification->title = 'Dear candidate your'.' '.$exam['name'].' exam is scheduled on '.$exam_date.'. Please be ready for you exam.' ;
							$mail_desc          = 'Your'.' '.$exam['name'].' exam is scheduled on '.$exam_date.'. Please be ready for you exam.' ;
							$notification->type = 'exam';
							if($notification->save()){
								$this->desktop_notification($value['user']['id'],$notification->title);
								$this->send_sms($value['user']['contact'],$notification->title);
								$user_name = ucfirst($value['user']['first_name']).' '.ucfirst($value['user']['last_name']);
								$this->send_notification_email($user_name,$value['user']['email'],$mail_desc);
							}else{
								continue;
							}
						}else{
							continue;
						}
					}
					$exam = Exam::where('id',$exam['id'])->update(['exam_notification_sent'=>'yes']); 
				}
			}
		}
	}

	public function send_sms($contact,$get_content){

		$contact     = $contact;
		$get_content = $get_content;
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://control.msg91.com/api/balance.php?authkey=280941Am4jdOUBM5d02a265&type=default",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		 	return 'false';
		} else {
			if($response>0){
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "{ \"sender\": \"SOCKET\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"$get_content\", \"to\": [ \"$contact\" ] } ] }",
				  CURLOPT_SSL_VERIFYHOST => 0,
				  CURLOPT_SSL_VERIFYPEER => 0,
				  CURLOPT_HTTPHEADER => array(
				    "authkey:280941Am4jdOUBM5d02a265",
				    "content-type: application/json"
				  ),
				));
				$response = curl_exec($curl);
				$err = curl_error($curl);
				curl_close($curl);
				if ($err) {
				  	return 'false';
				}else {
					return 'true';
				}
			}else{
				return 'false';
			}
		}
	}

	public function desktop_notification($user_id,$title){

	    $users = User::with('device_tokken')
		               ->whereNull('users.deleted_at')
		               ->where('status','A')
		               ->where('id',$user_id)
		               ->first();
		               // dd($users);
		               // echo'<pre>'; print_r($users); die;
	    if(!empty($users)){
	        foreach ($users['device_tokken'] as $key => $user) {
	        	if(!empty($user)){

	                $user_name = ucfirst($user['first_name']).' '.ucfirst($user['last_name']);
	                $message   = $title;
	                
	                // dd($url);
	                $msg = array(
	                    'body'  => $title,
	                    'title' =>'New Notification from Scamper Skills',
	                    'receiver'=>$user_name,
	                    'icon'  =>url("http://scamperskills.com/public/images/system/logo.png"),                    
	                    'sound' => 'default'
	                );
	                
	                $token = "AAAAuaa7yIo:APA91bHEXFNby3VNdumSba9lGvaaNro0pyXepJ_s94Zp053jZWlhFlXT9gAoQbh2pYg8cI0GMXeQu3epk9eUPq1mbZVOOmSi9H7vXyQ7s0M6DVeyAphNSFW5u7ac11nnrQhl7nVChdV5";
	                $token2=$user['device_token'];
	                $headers = array(
	                            'Authorization: key=' . $token,
	                            'Content-Type: application/json'
	                        );
	                $fields = array(
	                    'to'          => $token2,
	                    'notification'=> $msg,
	                    'image_url'   =>'http://scamperskills.com/public/images/system/logo.png', 
	                );
	                $ch = curl_init();
	                curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
	                curl_setopt( $ch,CURLOPT_POST, true );
	                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	                $result = curl_exec($ch );
	                // dd($result);
	                curl_close($ch);
	        	}else{
	                continue;
	            }         
	    	}
	    }else{
	    	return 'true';
	    }
	   
	}

	public function send_notification_email($user_name,$email,$description){
	    
	    try{

		    if(!empty($email)){
		 
	            $company_name = PROJECT_NAME;               
	            if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){

	                Mail::send('emails.request_response',['user_name'=>$user_name, 'email'=>$email, 'desc'=>$description],function($message) use ($email,$company_name){
	                            $message->to($email,$company_name)->subject('New Notification From Scamper Skills ');
	                });
	            }
		    }                              
	    }
	    catch (Exception $e) {
	    	return 'true';
	    }
	}
}
