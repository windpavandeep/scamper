<?php

namespace App\Http\Controllers\frontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session, Auth,App\Subject,App\Trainer;
use App\Country, App\State, App\City, App\TrainerDomain, App\User,App\EmploymentStatus,App\VerifyContact;
use App\Language,App\WelcomeSms,App\Category,App\UserCourse;
class AuthController extends Controller
{

	public function trainer_signup(Request $request){

		$countries 		 = Country::select('*')->get()->toArray();
		$domains         = TrainerDomain::select('*')->whereNull('deleted_at')->orderBy('name','asc')->get()->toArray();
		$employee_status = EmploymentStatus::select('*')->whereNull('deleted_at')->get()->toArray();
		$subject         = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
		$languages       = Language::whereNull('deleted_at')->get()->toArray();
				// echo '<pre>'; print_r($data); die;
		if(Session::has('SignupContact')){
			if($request->isMethod('post')){
				$data = $request->all();

				if(!empty($data['first_name']) && !empty($data['last_name']) ){

					if($data['password'] == $data['confirm_password']){
						$phpGeneratedHash       = bcrypt(@$data['password']);
            			$finalNodeGeneratedHash = str_replace("$2y$","$2a$",$phpGeneratedHash);

						$user = new User;
						$user->first_name 		= $data['first_name'];
						$user->last_name 		= $data['last_name'];
						$user->email 			= $data['email'];
						$user->contact 			= Session::get('SignupContact');
						$user->dob 				= date('Y-m-d',strtotime($data['dob']));
						$user->gender 			= $data['gender'];
						$user->password			= $finalNodeGeneratedHash;
						$user->country_id 		= $data['country_id'];
						$user->state_id 		= $data['state_id'];
						$user->city_id 			= $data['city_id'];
						$user->district 	    = $data['district'];
						$user->address 			= $data['address'];
						$user->added_by  		= 'self';
						$user->user_type 		= 'trainer';
						$user->verified_account = 'No';
						$user->language_id  	= $data['language_id'];

						if($user->save()){

							$trainer 					 	= new Trainer;
							$trainer->id           	     	= $user->id;
							$trainer->pincode     		    = $data['pincode'];
							$trainer->employment_status_id  = $data['employment_status_id'];
							$trainer->organization_name  	= $data['organization_name'];
							$trainer->experience         	= $data['total_experiance'];
							$trainer->trainer_domain_id     = $data['domain_id'];
							$trainer->subject_id     	 	= $data['subject_id'];
							if($trainer->save()){
								$get_content = WelcomeSms::value('description');
								$contact     = $user->contact;
								$this->send_welcome_message($contact,$get_content);
								Trainer::request_response_email($trainer->id,'signup'); 

								return redirect('/login')->with('success','Your signup request has been successfully forward to admin');
							}else{
								return redirect()->back()->with('error',COMMON_ERROR);	
							}
						} else{
							return redirect()->back()->with('error',COMMON_ERROR);
						}

					} else{
						return redirect()->back()->with('error',"Password and confirm password doesn't matched");	
					}
				} else{
					return redirect()->back()->with('error','Please fill all the required fields');
				}
				
			}
			return view('frontEnd.trainer.signup',compact('countries','domains','employee_status','subject','languages'));
		} else{
			return redirect('/register/trainer');
		}
		

		return view('frontEnd.trainer.signup',compact('countries','domains','employee_status','subject','languages'));
	}

	public function user_signup(Request $request){

		if(Session::has('SignupContact')){
			if($request->isMethod('post')){
				$data = $request->all();
				// echo '<pre>'; print_r($data); die;

				if(!empty($data['first_name']) && !empty($data['last_name']) ){

					if($data['password'] == $data['confirm_password']){
						$phpGeneratedHash       = bcrypt(@$data['password']);
            			$finalNodeGeneratedHash = str_replace("$2y$","$2a$",$phpGeneratedHash);

						$user = new User;

						$user->first_name 	= $data['first_name'];
						$user->last_name 	= $data['last_name'];
						$user->email 		= $data['email'];
						$user->contact 		= Session::get('SignupContact');
						$user->dob 			= date('Y-m-d',strtotime($data['dob']));
						$user->gender 		= $data['gender'];
						$user->password		= $finalNodeGeneratedHash;
						$user->country_id 	= $data['country_id'];
						$user->state_id 	= $data['state_id'];
						$user->city_id 		= $data['city_id'];
						$user->pincode 		= $data['pincode'];
						$user->exam_preparation_id  = $data['sub_category_id'];
						$user->added_by  	= 'self';
						$user->district     = $data['district'];
						$user->address 		= $data['address'];
						$user->user_type 	= 'user';
						$user->verified_account = 'yes';

						if($user->save()){
							$user_course 		  		  = new UserCourse;
							$user_course->user_id 		  = $user->id;
							$user_course->category_id     = $data['category_id'];
							$user_course->sub_category_id = $data['sub_category_id'];
							if($user_course->save()){

								$get_content = WelcomeSms::value('description');
								$contact     = $user->contact;
								$this->send_welcome_message($contact,$get_content);
								return redirect('/login')->with('success','Signup done successfully');
							} else{
								return redirect()->back()->with('error',COMMON_ERROR);
							}
						} else{
							return redirect()->back()->with('error',COMMON_ERROR);
						}

					} else{
						return redirect()->back()->with('error',"Password and confirm password doesn't matched");	
					}
				} else{
					return redirect()->back()->with('error','Please fill all the required fields');
				}
				
			}

			$countries = Country::get()->toArray();
			$domains   = Category::whereNull('deleted_at')->get()->toArray();
			$languages = Language::whereNull('deleted_at')->get()->toArray();
			return view('frontEnd.user.signup', compact('countries','domains','languages'));
		} else{
			return redirect('register/user');
		}
	}

	public function login(Request $request){

		if (Auth::check()) {
            // $user_type = Auth::User()->user_type;
            $user_type = Auth::User()->user_type;
            if ($user_type =='user') {
                // return redirect('/student/profile');
                // return redirect('user/profile');
                 //return redirect('/student/dashboard');
            	return redirect('/student/prep-exam');
            }elseif($user_type=='trainer') {

                return redirect('/trainer/dashboard');
            }
        }
		if($request->isMethod('post')){
			$data = $request->all();
			// echo '<pre>'; print_r($data); die;
            if(!empty($data['email']) && !empty($data['password'])){
	            if (Auth::attempt(['email'=>$data['email'], 'password'=>$data['password'], 'deleted_at'=>null])) {
	                return $this->_login_conditions();
	                // echo '1'; die;
	            }else{
	                if (Auth::attempt(['contact'=>$data['email'], 'password'=>$data['password'], 'deleted_at'=>null])) {
	                    return $this->_login_conditions();

	                } else{
	                    $course_url = '';
	                	if(Session::has('course_url')){
	                		$course_url = Session::get('course_url');
	                	}
	                    Auth::logout();
	                    Session::flush();
	                    if(!empty($course_url)){
	                    	Session::put('course_url',$course_url);
	                    }
	                    Session::put(['email'=>$data['email'], 'password'=>$data['password']]);
	                    return redirect()->back()->with('error',"Incorrect email/contact and password combination.");
	                }
	            }
            	
            } else{
            	return redirect()->back()->with('error','Please fill the required fields');
            }
		}
		return view('frontEnd.login');
	}

	public function logout(){
		Auth::logout();
		Session::flush();
		return redirect('/login')->with('success','Profile logged out successfully');
	}

	public function verify_contact(Request $request,$user_type){
		// dd($user_type);
		if($request->isMethod('post')){
			$data = $request->all();
			// echo '<pre>'; print_r($data); die;
			Session::put('SignupContact',$data['contact']);
			if($user_type=='user'){
				return redirect('user/complete-signup');

			}else{
				return redirect('trainer/complete-signup');				
			}
		}

		return view('frontEnd.verify_contact',compact('user_type'));		
	}

	public function send_otp(Request $request){

		if($request->isMethod('post')){

			$response_messages=[];
			$data = $request->input();
			// die;
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://control.msg91.com/api/balance.php?authkey=280941Ai3t4b6Kk5d11e35c&type=4",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_SSL_VERIFYPEER => 0,
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			if ($err) {
			 	$response_messages['status'] = 'false';
			} else {

				if($response>0){

					$otp          = rand(1000,9999);
					/*$contact_no   = $data['contact']; 
					$postData = array(
					    'authkey' => '280941Am4jdOUBM5d02a265',
					    'mobile'  => '+91'.$contact_no,
					    'message' => 'Your verification code for Scamper Skills is:'.$otp,
					    'sender'  => 'Hp2312',
					    'route'   => 'default',
					    'otp'     => $otp, 
					);*/
					/*$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://control.msg91.com/api/sendotp.php",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => $postData,
						CURLOPT_SSL_VERIFYHOST => 0,
						CURLOPT_SSL_VERIFYPEER => 0,
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);
					curl_close($curl);
*/					
					$authKey = "280941Ai3t4b6Kk5d11e35c";

					//Multiple mobiles numbers separated by comma
					$contact_no = $data['contact'];
					//Sender ID,While using route4 sender id should be 6 characters long.
					$senderId = "SCAMPR";
					//Your message to send, Add URL encoding here.
					$message = urlencode('Your verification code for Scamper Skills is:'.$otp);
					//Define route 
					$route = "4";
					//Prepare you post parameters
					$postData = array(
					    'authkey' => $authKey,
					    'mobiles' => $contact_no,
					    'message' => $message,
					    'sender' => $senderId,
					    'route' => $route
					);
					$url="http://api.msg91.com/api/sendhttp.php";
					// init the resource
					$ch = curl_init();
					curl_setopt_array($ch, array(
					    CURLOPT_URL => $url,
					    CURLOPT_RETURNTRANSFER => true,
					    CURLOPT_POST => true,
					    CURLOPT_POSTFIELDS => $postData
					    //,CURLOPT_FOLLOWLOCATION => true
					));
					//Ignore SSL certificate verification
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					//get response
					$output = curl_exec($ch);
					//Print error if any
					$err = '';
					if(curl_errno($ch))
					{
					    $err = curl_error($ch);
					}
					curl_close($ch);
					if ($err) {
					  	$response_messages['status'] = 'false';
					}else {
						$prev = VerifyContact::where('contact',$contact_no)->first();
					  	if(empty(@$prev->contact)){

	                        $verify           = new VerifyContact;
	                        $verify->contact  = $contact_no;
	                        $verify->otp      = $otp;
	                        $verify->save();
	                    } else{
	                        $prev->otp = $otp;
	                        $prev->save();
	                    }
	                    $response_messages['contact'] = $contact_no;
	                    $response_messages['status']  = 'true';
					}
				}else{
					$response_messages['status'] = 'false';
				}
			}
			return $response_messages;	
		}
	}

	/*public function send_otp(Request $request){

		if($request->isMethod('post')){
			$data = $request->all();
			// echo '<pre>'; print_r($data); die;
			if(empty($data['contact'])){
				$response['status'] = 'false';
			} else{
				$response['status'] = 'true';
				$response['contact'] = $data['contact'];
			}

			return $response;
		}
	}*/

	public function verify_otp(Request $request){
		if($request->isMethod('post')){
			$data = $request->all();
			// echo '<pre>'; print_r($data); die;
			$otp1 = $data['otp1'];
			$otp2 = $data['otp2'];
			$otp3 = $data['otp3'];
			$otp4 = $data['otp4'];

			$otp = $otp1.$otp2.$otp3.$otp4;
			// echo $otp; die;
			$db_otp = VerifyContact::where('contact',$data['contact'])->value('otp');
			if($otp == $db_otp){
				$response['status'] = 'true';
				$response['contact'] = $data['contact'];
			} else{
				$response['status'] = 'false';
			}

			return $response;
		}
	}

	public function destroy_otp(Request $request){
	    if($request->isMethod('post')){
	        $data = $request->all();
	        // echo '<pre>'; print_r($data); die;

	        $db_otp = VerifyContact::where('contact',$data['contact'])->update(['otp'=>null]);

	        if($db_otp){
	            return 'true';
	        } else{
	            return 'false';
	        }
	    }
	}
	
	public function get_states($country_id){

        $states = State::select('id','name')->where('country_id',$country_id)->orderBy('name')->get()->toArray();
        $options = "<option value=''>Select State</option>";
        
        if(!empty($states)){
            foreach($states as $state){
                if(!empty($state['name'])){
                    $options .= "<option value='".$state['id']."'>".ucfirst($state['name'])."</option>";
                }
            }
        } else{
            $options = "<option value=''>No State Found.</option>";
        }

        return $options;
    }
    public function get_cities($state_id){
    	// dd($state_id);
        $countries = City::select('id','name')->where('state_id',$state_id)
        				  ->orderBy('name')
        				  ->get()
        				  ->toArray();
        $options = "<option value=''>Select City</option>";
        
        if(!empty($countries)){
            foreach($countries as $country){
                if(!empty($country['name'])){
                    $options .= "<option value='".$country['id']."'>".ucfirst($country['name'])."</option>";
                }
            }
        } else{
            $options = "<option value=''>No City Found.</option>";
        }

        return $options;
    }

    function _login_conditions(){
    	if(Auth::check()){
    		$user 	   = User::where('id',Auth::User()->id)->whereNull('deleted_at')->first();
    		$user_type = $user->user_type;
    		$status    = $user->status;
    		// echo $status; die;
    		$name      = $user->first_name.' '.$user->last_name;
    		if($user_type == 'user'){
    			if($status=='A'){
    				if(Session::has('course_url')){
    					$course_url = Session::get('course_url');
    					Session::forget('course_url');
    					return redirect($course_url);
    				}else{
    					// return redirect('/student/profile')->with('success','Welcome '.$name);
    					// return redirect('/student/dashboard')->with('success','Welcome '.$name);
    					return redirect('/student/prep-exam')->with('success','Welcome '.$name);
    				}
    			}else{
    				
    				Auth::logout();
	                Session::flush();
    				return redirect()->back()->with('error','This account is not active. Please contact to admin for more information.');
    			}
    		}elseif($user_type=='trainer'){
    			$admin_approval = Trainer::where('id',Auth::User()->id)->value('admin_approval');
    			if($status=='A'){
    			// echo '1'; die;
	    			if($admin_approval == 'A'){
	    				return redirect('/trainer/dashboard')->with(['success'=>'Welcome '.$name]);
	    			}else{
	            		Auth::logout();
	                    Session::flush();
	                    return redirect('/login')->with('error','Your account is not approved by admin');  
	                }
            	}else{
    				Auth::logout();
    		        Session::flush();
            		return redirect()->back()->with('error','This account is not active. Please contact to admin for more information.');
            	}
    		}else{
    			Auth::logout();
    			Session::flush();
    			return redirect('login');
    		}
    	} else{
    		// echo '3'; die;
    		return redirect('login');
    	}
    }

    public function validate_contact($contact){
    	$number = User::where('contact',$contact)->whereNull('deleted_at')->value('contact');
    	if(empty($number)){
    		return 'true';
    	} else{
    		return 'false';
    	}
    }

    public function validate_email(){
    	$email = $_GET['email'];
    	$number = User::where('email',$email)->whereNull('deleted_at')->value('email');
    	if(empty($number)){
    		return 'true';
    	} else{
    		return 'false';
    	}
    }

    public function view(Request $request, $user_id = null, $security_code = null){

        $decoded_user_id         = convert_uudecode(base64_decode($user_id));
        $decoded_security_code   = convert_uudecode(base64_decode($security_code));
       // dd($decoded_user_id);
        $user = User::where([
                        'id'            =>$decoded_user_id,
                        'security_code' =>$decoded_security_code
                    ])
                     ->first();
                          // echo "<pre>"; print_r($user);die;
        if(!empty($user)){ 
            $user_email = $user->email;
            return view('frontEnd.set_password',compact('user_id','security_code','user_email'));
        } else{ 
            return redirect('/')->with('error','This link has been already used');
        }
    } 

    public function set_Password(Request $request){
        
        if($request->isMethod('post')){
            $data = $request->input();

            $decoded_user_id       = convert_uudecode(base64_decode($data['user_id']));
            $decoded_security_code = convert_uudecode(base64_decode($data['security_code']));
            if(empty($data['password']))
            {
                return redirect()->back()->with('error','Please Enter Password');
            }
            else if($data['password'] != $data['confirm_password'])
            {
                return redirect()->back()->with('error','Password & confirm password does not matched');
            }
            $user = User::where('id',$decoded_user_id)
                            ->where('security_code',$decoded_security_code)
                            ->first();
                    
            $phpGeneratedHash       = bcrypt($data['password']);
            $finalNodeGeneratedHash = str_replace("$2y$", "$2a$", $phpGeneratedHash);
            $user->password         = $finalNodeGeneratedHash;
            $user->security_code    = '';
            if($user->save())  {   
                return redirect('login')->with('success', 'Password set successfully');
            }else{ 
                return redirect('login')->with('error',COMMON_ERROR);          
            }
        }else{
            return redirect('login');
        }
    }

    public function forgot_password(Request $request){

    	$type= '';
    	$data = $request->input();
    	if($request->isMethod('post')){
    	    $user_id = User::select('id')
    	                    ->where('email',$data['email'])
    	                    ->value('id');  
    	    $type = 'forgot';                       
    	    $mail= User::sendCredentialMail($user_id,$type);
    	    if($mail==true){
    	        return redirect()->back()->with('success','Forgot password link sent successfully');
    	    }else{

    	        return redirect()->back()->with('error',COMMON_ERROR);
    	    }
    	}
    	return view('frontEnd.forgot_password');
    }

    public function send_welcome_message($contact,$get_content){
    	$contact     = $contact;
    	$get_content = $get_content;
    	$curl = curl_init();

    	curl_setopt_array($curl, array(
    		CURLOPT_URL => "https://control.msg91.com/api/balance.php?authkey=280941Am4jdOUBM5d02a265&type=default",
    		CURLOPT_RETURNTRANSFER => true,
    		CURLOPT_ENCODING => "",
    		CURLOPT_MAXREDIRS => 10,
    		CURLOPT_TIMEOUT => 30,
    		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    		CURLOPT_CUSTOMREQUEST => "GET",
    		CURLOPT_SSL_VERIFYHOST => 0,
    		CURLOPT_SSL_VERIFYPEER => 0,
    	));
    	$response = curl_exec($curl);
    	$err = curl_error($curl);
    	curl_close($curl);
    	if ($err) {
    	 	return 'false';
    	} else {
    		if($response>0){
    			$curl = curl_init();
    			curl_setopt_array($curl, array(
    			  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
    			  CURLOPT_RETURNTRANSFER => true,
    			  CURLOPT_ENCODING => "",
    			  CURLOPT_MAXREDIRS => 10,
    			  CURLOPT_TIMEOUT => 30,
    			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    			  CURLOPT_CUSTOMREQUEST => "POST",
    			  CURLOPT_POSTFIELDS => "{ \"sender\": \"SOCKET\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"$get_content\", \"to\": [ \"$contact\" ] } ] }",
    			  CURLOPT_SSL_VERIFYHOST => 0,
    			  CURLOPT_SSL_VERIFYPEER => 0,
    			  CURLOPT_HTTPHEADER => array(
    			    "authkey:280941Am4jdOUBM5d02a265",
    			    "content-type: application/json"
    			  ),
    			));
    			$response = curl_exec($curl);
    			$err = curl_error($curl);
    			curl_close($curl);
    			if ($err) {
    			  	return 'false';
    			}else {
    				return 'true';
    			}
    		}else{
    			return 'false';
    		}
    	}
    	
    }
}
