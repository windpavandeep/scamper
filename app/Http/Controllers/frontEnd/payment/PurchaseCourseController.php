<?php

namespace App\Http\Controllers\frontEnd\payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller,App\DiscountCoupon,App\User,Session;
use App\Transaction,Auth,App\TrainerContent,App\Admin,App\TrainerContentFile;
class PurchaseCourseController extends Controller
{
	public function purchase_course(Request $request){

		if($request->isMethod('post')){

			$data                      = $request->all();
			// echo'<pre>'; print_r($data);die;
			$price                     = $data['price']/100;
			$user_id                   = Auth::User()->id;
			$transaction               = new Transaction;
			$transaction->user_id      = $user_id;
			$transaction->course_id    = $data['course_id'];
			$transaction->price        = $price;
			$transaction->razor_pay_id = $data['razorpay_payment_id'];
			$transaction->purchased_on = date('Y-m-d');
			$course_details            = TrainerContent::select('file','trainer_id','end_date','trainer_id','id')->where('id',$data['course_id'])->first();
			$transaction->gst          = $data['gst'];
			if(Session::has('postal_code')){
				$pincode = Session::get('postal_code');
				
				if(!empty($pincode)){
					$transaction->pincode = $pincode;
				}
			}
			if(!empty($data['discount_coupon'])){
				$transaction->discount_coupon = $data['discount_coupon'];
				$transaction->discount_amount = $data['discount_amount'];
				$transaction->price        = $price+$data['discount_amount'];
			}else{
				$transaction->price        = $price;
			}
			if(!empty($data['refer_code'])){
				$transaction->refer_code = $data['refer_code'];
			}
			$transaction->final_total  = $price;    
			$get_admin_commission      = Admin::select('*')->value('commission');
			$transaction->valid_till   = @date('Y-m-d',strtotime($course_details->end_date));
			// echo'<pre>'; print_r($course_details); die;
			if($course_details->trainer_id!=0){
				if($get_admin_commission>0){
					$admin_commission = ($transaction->price*$get_admin_commission)/100;
					$transaction->admin_commission = $admin_commission;
					$transaction->trainer_price    = $transaction->price-$admin_commission;
				}else{
					$transaction->admin_commission = 0 ;
					$transaction->trainer_price    = $transaction->price;
				}
			}else{

				$transaction->admin_commission = $transaction->final_total;
				$transaction->paid_status      = 'Y';
				$transaction->trainer_price    = 0;
			}
			if($transaction->save()){
				$file     = '';
				$purchased= '';
				if($course_details->trainer_id=='0'){

					$content = TrainerContentFile::where('trainer_content_id',$course_details->id)
												  ->get()
												  ->toArray();
					$purchased.='<div style="margin-bottom:15px;margin-top:15px">
									<div class="col-md-4">Download Course
										<p>';
					foreach ($content as $key => $value) {

						$file_url = 'javascript:;';
		   				$file_type= ''; 
		   				if(!empty($value['file'])) {

		   				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
		   				        $file_url = TrainerContentImgPath.'/'.$value['file'];
		   				        $file_name= pathinfo($value['file']);
                                $ext      = $file_name['extension'];
                                if($ext=='pdf'){
                                	$file_type = 'pdf';
                                }else{
                                	$file_type = 'video';
                                }
		   				    }
		   				}
		   				$class = ''; 
		   				if($file_type=='pdf'){
		   					$class = 'fas fa-file-pdf';
		   				}else{
		   					$class = 'fas fa-video';
		   				}
		   				$purchased.= "(";
		   				$purchased.= $key+1;  
	   					$purchased.= ")";
	   					$purchased.= "  ";
	   					$purchased.= ucfirst($file_type);		
	   					$purchased.= '<a href="'.$file_url.'"  target="_blank">
										 <i class="'.$class.'"></i>
					   				  </a>';

					}
					$purchased.= '</div></div></p><a href="javascript:;" class="btn btn_gradient btn_active" disabled><i class="fa fa-cart"></i> Purchased</a>';
					return $purchased;
				}else{

				    if($course_details->upload_type=='pdf'){
				        $title = 'Download PDF';
				    }else{
				        $title ='Download Video';
				    }
				    // $image = DefaultImgPath; 
				    
				    $file_url = 'javascript:;';
				    $file_type= '';
				    if(!empty($course_details->file)) {
				        if(file_exists(TrainerContentBasePath.'/'.$course_details->file)) {
				            $file_url = TrainerContentImgPath.'/'.$course_details->file;
				            $file_name= pathinfo($course_details['file']);
                            $ext      = $file_name['extension'];
                            if($ext=='pdf'){
                            	$file_type = 'pdf';
                            }else{
                            	$file_type = 'video';
                            }
				        }
				    }

					    // dd($file_url);
					$class = ''; 
					if($file_type=='pdf'){
						$class = 'fas fa-file-pdf';
					}else{
						$class = 'fas fa-video';
					}
					$purchased.='<div style="margin-bottom:15px;margin-top:15px">
									<div class="col-md-4">Download Course
										<p>';
					
					$purchased.= "(";
	   				$purchased.= 1;  
   					$purchased.= ")";
   					$purchased.= "  ";				
					$purchased.= ucfirst($file_type);
					$purchased.='<a href="'.$file_url.'"  target="_blank">
											<i data-toggle="tooltip" title="'.$title.'" class="'.$class.'" data-original-title="'.$title.'"></i>
										</a>
									</p>
									</div>
								</div>
									<a href="javascript:;" class="btn btn_gradient btn_active" disabled><i class="fa fa-cart"></i> Purchased</a>';
					return $purchased;
				}
			}else{
				return 'false';
			}	
		}
	}

	public function verify_coupon_code(Request $request){

		$data          = $request->input();
		$coupon_code   = $data['coupon_code'];
		$course_id     = $data['course_id'];
		if(!empty($data['course_id'])&&!empty($data['coupon_code'])){


			$coupon_detail = DiscountCoupon::select()->whereNull('deleted_at')
											->where('coupon_code',$coupon_code)
											->where('status','A')
											->first();
			if (!empty($coupon_detail)) {
			    $start_date = strtotime($coupon_detail->start_date);
			    $end_date   = strtotime($coupon_detail->end_date);
			    $current_date = strtotime(date('Y-m-d'));
			    $coupon_code =  $coupon_code;
			    $discount_val = $coupon_detail->discount_amount;
			    $discount_type = $coupon_detail->discount_type;
			    if($current_date < $start_date || $current_date > $end_date) {
			        
			  		return 'false';
			    }
			 
			    $used_coupon_count = Transaction::where('discount_coupon',$coupon_code)->count();
			    if ($used_coupon_count < $coupon_detail->uses_per_user_limit){
			      	
			      	$course = TrainerContent::where('id',$course_id)
	      									->whereNull('deleted_at')
	      									->first();
			      	$discount_amount = 0;
			      	$gst_amount = 0;
			      	if($coupon_detail->discount_type=='P'){

			      		$discount_amount = ($coupon_detail->discount_amount*$course->paid_amount)/100;
			      		if($course->gst>0){
			      			$gst_precent = Admin::value('gst');
			      			$amount      = (($course->paid_amoun-$discount_amount)*$gst_precent)/100;
			      			$gst_amount  = $amount;
			      		}

			      	}else{
			      		$discount_amount = $coupon_detail->discount_amount;
			      		if($course->gst>0){
			      			$gst_precent = Admin::value('gst');
			      			$amount      = (($course->paid_amoun-$discount_amount)*$gst_precent)/100;
			      			$gst_amount  = $amount;
			      		}
			      	}
			      	// dd($gst_amount);
			        $price       = $course->paid_amount;
			        $final_total = $course->final_price - $discount_amount;
			        if ($final_total <= 0) {
			            return 'false';
			        }else{
			            $response = ['price'=>$price,'final_total'=>$final_total,'discount_amount'=>$discount_amount,'coupon_code'=>$coupon_code,'gst_amount'=>$gst_amount];
						return $response;
			   		}
			    }else{
			    	return 'false';
			    }
			}else{
				return 'false';
			}
		}
	}

	public function set_pincodee_session($postal_code){

		if(!empty($postal_code)){
			Session::put('postal_code',$postal_code);
		}

	}
}
