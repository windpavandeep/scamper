<?php

namespace App\Http\Controllers\frontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainerContent, App\Country, App\Franchise, App\ContactUs;
use App\Transaction,Auth, App\HomePageFirstSection, App\HomePageSecondSection, App\HomePageThirdSection;
use App\SuccessStory,App\Category,App\ContactUsContent,App\SubCategory,App\SubSubCategory;
use App\Faq,App\BecomeTrainer,App\RetailerImage,App\FreeCounselling,App\Admin;
use Session,App\Download;
class HomeController extends Controller
{
    public function index(){
        //dd('hi');

    	$our_courses = TrainerContent::with('course_image')
                                      ->where('trainer_id',0)
    								  ->where('deleted_at',NULL)
                                      ->where('content_availability','paid')
    								  ->with('sub_category')
                                      ->take(12)
                                      ->orderBy('id','desc')
    								  ->get()
    								  ->toArray();

        $first_section = HomePageFirstSection::first();
        $second_section = HomePageSecondSection::where('id','<>',5)->get()->toArray();
        $third_section = HomePageThirdSection::get()->toArray();
        $why_choose_us_image = HomePageSecondSection::select('id','image')
                                                     ->where('id',5)
                                                     ->first();
        $third_section_video = HomePageThirdSection::where('id','2')->value('video');
        $our_faculty_banner_image = SuccessStory::where('type','OB')->value('image');

        $footer_categories_details = Category::with(['sub_categories'=>function($query){

                                                    $query->where('place_footer','Yes')
                                                          ->whereNull('deleted_at'); 

                                              
                                               },'sub_categories.sub_sub_categories'=>function($query){
                                                                                   
                                                                                    $query->whereNull('deleted_at');
                                                                                    }
                                                ])
                                              ->whereNull('deleted_at')
                                              ->get()
                                              ->toArray();
    	// echo "<pre>"; print_r($our_courses); die;
        $gst_precent = Admin::value('gst');
        return view('frontEnd.index',compact('our_courses','first_section','second_section','third_section','why_choose_us_image','third_section_video','our_faculty_banner_image','footer_categories_details','gst_precent'));
    }

    public function courses(Request $request){

        $data        = $request->input();
        $our_courses = TrainerContent::with('course_image')
                                      ->where('deleted_at',NULL)
                                      ->where('content_availability','paid')
                                      ->with('sub_category');
        $category       = '';
        $sub_category   = '';
        $sub_subcategory= '';
        if(!empty(@$data['category_id'])) {

            $our_courses = $our_courses->where('category_id',$data['category_id']);
            $category    = Category::where('id',$data['category_id'])->first(); 
        }
        if(!empty(@$data['sub_category_id'])) {
            $our_courses  = $our_courses->where('sub_category_id',$data['sub_category_id']);
            $sub_category = SubCategory::where('id',$data['sub_category_id'])
                                        ->first();   
            $category     = Category::where('id',$sub_category->category_id)
                                     ->first();
        }
        if(!empty(@$data['sub_subcategory_id'])) {
            $our_courses     = $our_courses->where('sub_subcategory_id',$data['sub_subcategory_id']); 
          
            $sub_subcategory = SubSubCategory::where('id',$data['sub_subcategory_id'])
                                              ->first();
            $sub_category    = SubCategory::where('id',$sub_subcategory->sub_category_id)
                                          ->first();
            $category        = Category::where('id',$sub_category->category_id)
                                    ->first();

        }
      
        $our_courses = $our_courses->get()->toArray();       

        $gst_precent = Admin::value('gst');                
        return view('frontEnd.courses',compact('our_courses','category','sub_category','sub_subcategory','gst_precent'));
    }
    public function course_detail($course_id = null){

        $decry_course_id = base64_decode($course_id);
                            // echo'<pre>'; print_r($decry_course_id); die;
        $course_id       = explode('-',$decry_course_id);
        $trainer_id      = 0;
        if(!empty(@$course_id['1'])){
            $trainer_id = $course_id['1'];
        }
        $trainer_content_page = '';
        if(!empty(@$course_id['2'])){
            $trainer_content_page = $course_id['2'];
        }
        $course_id = $course_id['0'];
       
        $course_details = TrainerContent::with('course_images','course_files')
                                        ->where('trainer_id',$trainer_id)
                                        ->with('sub_category')
                                        ->where('id',$course_id)
                                        ->where('deleted_at',NULL)
                                        ->first();
                          // echo'<pre>'; print_r($decry_course_id); die;              
        if(empty($course_details)){
            return redirect()->back()->with('error','No course found.');
        }
        $user_id = @Auth::User()->id;
        $check_user_course ='';

        $suggested_trainers= '';
        if(!empty($course_details->sub_category_id)){
            $suggested_trainers = TrainerContent::select('trainer_contents.*','s.name')
                                                 ->with('course_image')
                                                 ->join('sub_categories as s','s.id','trainer_contents.sub_category_id')
                                                 ->join('users as u','trainer_contents.trainer_id','u.id')
                                                 ->where('sub_category_id',$course_details->sub_category_id)
                                                 ->where('trainer_id','<>','0')
                                                 ->where('content_availability','paid')
                                                 ->where('user_type','trainer')
                                                 ->whereNull('u.deleted_at')
                                                 ->whereNull('trainer_contents.deleted_at')
                                                 ->get()
                                                 ->toArray();
        }
        // echo'<pre>'; print_r($suggested_trainers); die; 
        if(!empty($user_id)){
            
            $check_user_course_exist = Transaction::where([
                                                        'user_id'  =>$user_id,
                                                        'course_id'=>$course_id
                                                     ])
                                                     ->orderBy('id','desc')
                                                     ->first();
        }
        $faqs = Faq::select('id','title','description')
                    ->whereNull('deleted_at')
                    ->get()
                    ->toArray(); 
        if(Session::has('checkout')){
            Session::forget('checkout');
        }
        // echo'<pre>'; print_r($course_details->toArray()); die;           
                          // dd($trainer_id);
        $gst_precent = Admin::value('gst');
        if(!empty($course_details)){
            if(!empty($course_details->gst)&& $course_details->gst>'0'){

                $final_price = ($course_details->paid_amount*$gst_precent)/100;
                $final_price = $course_details->paid_amount+$final_price;
            }else{
                $final_price = $course_details->final_price;
            }
            if($final_price<'1'){
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
        //dd($course_details);
    	return view('frontEnd.course_detail',compact('course_details','course_id','check_user_course_exist','trainer_id','suggested_trainers','faqs','trainer_content_page','final_price'));
    }

    public function become_retailer(Request $request){

        $countries = Country::get()->toArray();
        if($request->isMethod('post')){
            // echo "<pre>"; print_r($request->input()); die;

            $franchise                  = new Franchise;
            $franchise->name            = $request->name;
            $franchise->email           = $request->email;
            $franchise->contact_no      = $request->contact_no;
            $franchise->address         = $request->address;
            $franchise->country_id      = $request->country;
            $franchise->state_id        = $request->state;
            $franchise->city_id         = $request->city;
            $franchise->pin_code        = $request->pin_code;
            $franchise->experience_edu  = $request->radio;
            $franchise->edu_detail      = $request->description;
            $franchise->query_desc           = $request->query_desc;
            if($franchise->save()){
            // echo "222"; die;
                return redirect('/become-retailer')->with('success','Reseller request submitted successfully.');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }

        }
        $retailer_image = RetailerImage::select('*')->first();
        return view('frontEnd.become_retailer',compact('countries','retailer_image'));
    }


    public function contact_us(Request $request){
        if($request->isMethod('post')){
            // echo "<pre>"; print_r($request->input()); die;

            $contact_us             = new ContactUs;
            $contact_us->first_name = $request->first_name;
            $contact_us->last_name  = $request->last_name;
            $contact_us->email      = $request->email;
            $contact_us->contact_no = $request->contact_no;
            $contact_us->enquiry    = $request->enquiry;
            if($contact_us->save()){
                return redirect('/contact-us')->with('success','Your query has submitted successfully.');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }
        }
        $contact_us_contents = ContactUsContent::select('*')->first();
        return view('frontEnd.contact_us',compact('countries','contact_us_contents'));
    }

    public function site_map(){

        return view('frontEnd.sitemap');
    }

    public function downloads(){

        $downloads = Download::get()->toArray();
        return view('frontEnd.download',compact('downloads'));
    }

    public function become_trainer(){

        $section1_content = BecomeTrainer::where('type','section1')->first();
        $section2_content = BecomeTrainer::where('type','section2')
                                          ->orderBy('id','Desc')
                                          ->get()
                                          ->toArray();
        return view('frontEnd.trainer.become_trainer',compact('section1_content','section2_content'));
    }

    public function free_counselling_resquest(Request $request){

        if($request->isMethod('post')){
            $data                               = $request->input();
            $counselling                        = new FreeCounselling;
            $counselling->name                  = $data['name'];
            $counselling->email                 = $data['email'];
            $counselling->contact_no            = $data['contact_no'];
            $counselling->category_id           = $data['category_id'];
            $counselling->sub_category_id       = $data['sub_category_id'];
            $counselling->state_id              = $data['state_id'];
            $counselling->description           = $data['desc']; 
            if(!empty($data['sub_subcategory_id'])){

                $counselling->sub_sub_category_id   = $data['sub_subcategory_id'];
            }
            if($counselling->save()){
                return redirect()->back()->with('success','Counselling request submitted successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }

    }
}
