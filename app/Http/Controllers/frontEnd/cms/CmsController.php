<?php

namespace App\Http\Controllers\frontEnd\cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AboutUs,App\Content,App\NewsEvent,App\Faq;
class CmsController extends Controller
{
    public function about_us(){

    	$first_section  = AboutUs::where('type','section1')->first();
    	$second_section = AboutUs::where('type','section2')->get()->toArray();
    	$our_partners   = AboutUs::where('type','partner')->get()->toArray();
    	return view('frontEnd.cms.aboutUs',compact('first_section','second_section','our_partners'));
    }

    public function privacy_policy(){
    	$privacy_terms = Content::where('type','P')->first();
    	return view('frontEnd.cms.privacy_terms',compact('privacy_terms'));	
    }

    public function terms_conditions(){
        $terms_conditions = Content::where('type','T')->first();
        return view('frontEnd.cms.terms_condition',compact('terms_conditions')); 
    }

    public function news_events(){

        $paginate    = NewsEvent::select('*')->paginate(7);
        $news_events = NewsEvent::select('*')->get()->toArray();
        // $news_events = $news_events->toArray();
        // echo'<pre>'; print_r($news_events); die;
        return view('frontEnd.cms.news_event',compact('news_events','paginate')); 
    }

    public function disclaimer(){
        $disclaimer = Content::where('type','D')->first();
        // echo'<pre>'; print_r($disclaimer->toArray());
        return view('frontEnd.cms.disclaimer',compact('disclaimer'));
    }

    public function faqs(){
        $faqs = Faq::select('*')
                    ->get()
                    ->toArray();
        return view('frontEnd.cms.faqs',compact('faqs'));
    }
}
