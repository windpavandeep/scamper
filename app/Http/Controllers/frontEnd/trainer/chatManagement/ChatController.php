<?php

namespace App\Http\Controllers\frontEnd\trainer\chatManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
	public function index(){

		$page = 'chats';
		return view('frontEnd.trainer.chatManagement.index',compact('page'));
	}
}
