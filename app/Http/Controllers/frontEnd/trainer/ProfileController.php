<?php

namespace App\Http\Controllers\frontEnd\trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User,App\Trainer,Auth,App\Country,App\Subject,App\TrainerDomain,App\EmploymentStatus,App\State,App\City;
class ProfileController extends Controller
{
    public function index(){

        $trainer_id      = Auth::User()->id;
        $trainer_details = Trainer::select('*')
                                    ->with(['user_details.country','user_details.state','user_details.city','employment_status'=>function($query){
                                            $query->whereNull('deleted_at');},
                                        'subject'=>function($query){$query->whereNull('deleted_at');},
                                        'domain'=>function($query){$query->whereNull('deleted_at');}
                                    ])
                                    // ->whereHas()
                                    ->where('id',$trainer_id)
                                    ->first();
    	// echo'<pre>'; print_r($trainer_details->toArray());die;
        $page = 'profile';
    	return view('frontEnd.trainer.profile.index',compact('page','trainer_details'));
    }

    public function edit(Request $request){

        $trainer_id      = Auth::User()->id;
        $trainer_details = Trainer::select('*')
                                    ->with(['user_details.country','user_details.state','user_details.city','employment_status'=>function($query){
                                            $query->whereNull('deleted_at');},
                                        'subject'=>function($query){$query->whereNull('deleted_at');},
                                        'domain'=>function($query){$query->whereNull('deleted_at');}
                                    ])
                                    // ->whereHas()
                                    ->where('id',$trainer_id)
                                    ->first();
            // echo'<pre>'; print_r($trainer_details); die;
        if($request->isMethod('post')){
            // dd($_FILES);
            $data                                  = $request->input();
            $trainer_details->pincode              = $data['pincode'];
            $trainer_details->employment_status_id = $data['employment_status_id'];
            $trainer_details->organization_name    = $data['organization_name'];
            $trainer_details->experience           = $data['total_experience'];
            $trainer_details->trainer_domain_id    = $data['domain_id'];
            $trainer_details->subject_id           = $data['subject_id'];
            if($trainer_details->save()){

                $user             = User::where('id',$trainer_id)->first();
                $user->first_name = $data['first_name'];
                $user->last_name  = $data['last_name'];
                $user->dob        = date('Y-m-d',strtotime($data['dob']));
                $user->gender     = $data['gender'];
                $user->country_id = $data['country_id'];
                $user->state_id   = $data['state_id'];
                $user->city_id    = $data['city_id'];
                $user->address    = $data['address'];
                $user->district   = $data['district'];

                if(!empty($_FILES['image']['name'])){
                    $image      = pathinfo($_FILES['image']['name']);
                    $ext        = $image['extension'];
                    $tmp_name   = $_FILES['image']['tmp_name'];
                    $random_no  = uniqid();
                    $new_name   = $random_no.'.'.$ext;
                    $destination= TrainerProfileBasePath.'/'.$new_name;

                    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                        if (file_exists($destination.'/'.$user->image)){
                            unlink($destination.'/'.$user->image);
                        }
                        move_uploaded_file($tmp_name, $destination);
                        $user->image = $new_name;
                    }else{
                        return redirect()->back()->with('error','Invalid image extension');
                    }
                }
                if($user->save()){
                    return redirect()->back()->with('success','Profile edited successfully');
                }else{
                    return redirect()->back()->with('error',COMMON_ERROR);
                }

            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }                            

        $states = [];
        $cities = [];
        $countries       =Country::select('*')->get()->toArray();  
        $domains         = TrainerDomain::select('*')->whereNull('deleted_at')->orderBy('name','asc')->get()->toArray();
        $employee_status = EmploymentStatus::select('*')->whereNull('deleted_at')->get()->toArray();
        $subject         = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
        if(!empty($trainer_details->user_details->country_id)){

            $states = State::where('country_id',$trainer_details->user_details->country_id)->get()->toArray();
            $cities = City::where('state_id',$trainer_details->user_details->state_id)->get()->toArray();
        }
        // dd($states);
    	$page = 'profile';
    	return view('frontEnd.trainer.profile.form',compact('page','countries','domains','employee_status','subject','trainer_details','states','cities'));
    }

    public function change_password(Request $request){

        if($request->isMethod('post')){
            
            $data = $request->all();
            
            $pw = User::change_password($data);

            if($pw=='pass_not_match'){
                return redirect()->back()->with('error',"New password and confirm password doesn't matched");
            }else{
                if($pw=='true'){
                    return redirect()->back()->with('success','Password changed successfully'); 
                }elseif($pw=='false'){
                    return redirect()->back()->with('error',COMMON_ERROR);   
                }else{
                    return redirect()->back()->with('error','Invalid current password');
                }
            }
        }
        
    	$page = 'change_password';
    	return view('frontEnd.trainer.profile.change_password',compact('page'));	
    }
  
}
