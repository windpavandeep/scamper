<?php

namespace App\Http\Controllers\frontEnd\trainer\earnings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction,App\TrainerContent;
use Auth;
class EarningController extends Controller
{
    public function index(){

    	$trainer_id = Auth::User()->id;

    	$earnings  = Transaction::select('transactions.*','tc.title','tc.trainer_id','tc.upload_type','u.first_name','u.last_name')
								->join('trainer_contents as tc','tc.id','transactions.course_id')
								->join('users as u','u.id','transactions.user_id')
								->where('paid_status','Y')
								->where('tc.trainer_id',$trainer_id);
								// ->orderBy('transactions.id','Desc');
		
		$total_earnings  = $earnings->sum('trainer_price');
		$earnings        = $earnings->get()->toArray();
		// echo'<pre>'; print_r($earnings); die;
		$current_month_earnings = Transaction::join('trainer_contents as tc','tc.id','transactions.course_id')
											->where('paid_status','N')
											->where('tc.trainer_id',$trainer_id)
							  				->sum('trainer_price');
		// echo'<pre>'; print_r($current_month_earnings); die;

		$page ='earnings';
		return view('frontEnd.trainer.earnings.index',compact('page','total_earnings','current_month_earnings','earnings'));
	}
}
