<?php

namespace App\Http\Controllers\frontEnd\trainer\subscriptions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubscriptionPlan,Auth,App\Trainer;
class SubscriptionController extends Controller
{
	public function index(){

		$trainer_id = Auth::User()->id;
		$details	= SubscriptionPlan::where(['deleted_at'=>null,'trainer_id'=>$trainer_id])
									  ->get()->toArray();
		$page ='subscriptions';
		return view('frontEnd.trainer.subscriptions.index',compact('page','details'));
	}

	public function add(Request $request){

		$trainer_id = Auth::User()->id;
		if($request->isMethod('post')){

			$data = $request->input();
			$subscriptions 				= new SubscriptionPlan;
			$subscriptions->trainer_id 	= $trainer_id;
			$subscriptions->title 		= $data['title'];
			$subscriptions->price 		= $data['price'];
			$subscriptions->validity 	= $data['plan_validity'];
			$subscriptions->description = $data['description'];
			if($subscriptions->save()){
				Trainer::subscription_plan_response_email($subscriptions->id); 
				return redirect('/trainer/subscriptions')->with('success','Subscription plan request successfully forwarded to admin');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		
		$page ='subscription_plan';
		return view('frontEnd.trainer.subscriptions.form',compact('page'));
	}

	public function edit(Request $request,$subscription_plan_id){

		$trainer_id = Auth::User()->id;
		$details	= SubscriptionPlan::where(['id'=>$subscription_plan_id,'trainer_id'=>$trainer_id])
									  ->first();
									  // echo "<pre>";print_r($details);die;
		if($request->isMethod('post')){

			$data = $request->input();

			$details->trainer_id 	= $trainer_id;
			$details->title 		= $data['title'];
			$details->price 		= $data['price'];
			$details->validity 		= $data['plan_validity'];
			$details->description 	= $data['description'];
			if($details->save()){
				return redirect()->back()->with('success','Subscription plan edited successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		
		$page ='subscriptions';
		return view('frontEnd.trainer.subscriptions.form',compact('page','trainer_id','details','subscription_plan_id'));
	}

	public function delete($subscription_plan_id){

	    $del = SubscriptionPlan::where('id',$subscription_plan_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
	    if ($del) {
	        return redirect()->back()->with('success','Subscription plan deleted successfully');
	    }else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}
}
