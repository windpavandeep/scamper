<?php

namespace App\Http\Controllers\frontEnd\trainer\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category, App\SubCategory, App\SubSubCategory, App\Subject, App\Unit,App\TrainerContent,Auth;
use App\Language,App\TrainerContentImage;
class ContentController extends Controller
{
	public function index(){

		$trainer_id = Auth::User()->id;
		$details   	= TrainerContent::select('trainer_contents.*','c.name as category_name')
									->join('categories as c','trainer_contents.category_id','c.id')
									->where('trainer_id',$trainer_id)
									->where('upload_type','pdf')
									->whereNull('trainer_contents.deleted_at')
									->whereNull('c.deleted_at')
									->orderBy('trainer_contents.id','Desc')
									->get()
									->toArray();

 									 	// echo '<pre>'; print_r($details); die;
		$categories = Category::whereNull('deleted_at')->get()->toArray();
		$subjects   = Subject::whereNull('deleted_at')->get()->toArray();

		$page = 'contents';
		return view('frontEnd.trainer.contentManagement.index',compact('page','details','categories','subjects'));
	}
	public function add(Request $request){

		if($request->isMethod('post')){

			$data                   	= $request->all();

			$trainer_id					= Auth::User()->id;
			$content    				= new TrainerContent;
			$content->trainer_id   		= $trainer_id;
			$content->category_id   	= $data['category_id'];
			$content->sub_category_id   = $data['sub_category_id'];
			$content->sub_subcategory_id= $data['sub_subcategory_id'];
			$content->subject_id		= $data['subject_id'];
			$content->unit_id   		= $data['unit_id'];
			$content->title				= $data['title'];
			$content->upload_type   	= $data['upload_optn'];
			$content->language_id       = $data['language_id'];
			// dd($content);
			// echo'<pre>'; print_r($content); die;
			if(!empty($data['upload_optn'])){
		
				if($data['upload_optn']=='pdf'){
					
					if(!empty($_FILES['pdf']['name'])){
					    $image      = pathinfo($_FILES['pdf']['name']);
					    $ext        = $image['extension'];
					    $tmp_name   = $_FILES['pdf']['tmp_name'];
					    $random_no  = uniqid();
					    $new_name   = $random_no.'.'.$ext;
					    $destination= TrainerContentBasePath.'/'.$new_name;

					    if($ext == 'pdf'){
					        move_uploaded_file($tmp_name, $destination);
					        $content->file = $new_name;
					    }else{
					        return redirect()->back()->with('error','Invalid file extension');
					    }
					}

				}else{

					if(!empty($_FILES['video']['name'])){
					    $image      = pathinfo($_FILES['video']['name']);
					    $ext        = $image['extension'];
					    $tmp_name   = $_FILES['video']['tmp_name'];
					    $random_no  = uniqid();
					    $new_name   = $random_no.'.'.$ext;
					    $destination= TrainerContentBasePath.'/'.$new_name;

					    if($ext == 'mp4' || $ext == '3gp'|| $ext == 'webm'|| $ext == 'flv'|| $ext == 'avi'){
					        move_uploaded_file($tmp_name, $destination);
					        $content->file = $new_name;
					    }else{
					        return redirect()->back()->with('error','Invalid file extension');
					    }
					}
				}
			}
			$content->content_availability  = $data['content_type'];
			if(!empty($data['paid_amount'])){

				$content->paid_amount = $data['paid_amount'];
				$content->final_price = $data['paid_amount'];
			}
			$content->gst         = '0';
			$content->gst_price   = '0';
			$content->description = $data['description'];
			
			if($content->save()){

				if(!empty($_FILES)){

				    foreach($_FILES['images']['name'] as $key => $img) {
	                    if(!empty($_FILES['images']['name'][$key])){
	                        $image      = pathinfo($_FILES['images']['name'][$key]);
	                        $ext        = strtolower($image['extension']);
	                        $temp_name  = $_FILES['images']['tmp_name'][$key];
	                        $random_no  = uniqid();
	                        $new_name   = $random_no.'.'.$ext;
	                        if($ext    == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
	                            $destination = base_path().'/'.TrainerContentImageBasePath;
	                            move_uploaded_file($temp_name, $destination.'/'.$new_name);
	                            $trainer_content_image 	           	 		= new TrainerContentImage;
	                            $trainer_content_image->trainer_content_id  = $content->id;
	                            $trainer_content_image->name      			= $new_name;
	                            $trainer_content_image->save();
	                        }else{
	                            return false;
	                        }
	                    }
	                }	
				}
				return redirect('/trainer/contents')->with('success','Content added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}

		$categories = Category::whereNull('deleted_at')->get()->toArray();
		$subjects   = Subject::whereNull('deleted_at')->get()->toArray();
		$languages  = Language::whereNull('deleted_at')->get()->toArray();
		// echo '<pre>'; print_r($subjects); die;

		$page = 'contents';
		return view('frontEnd.trainer.contentManagement.form',compact('page','categories','subjects','languages'));
	}

	public function get_subcategory($category_id){
		$subs = SubCategory::where('category_id',$category_id)
							->whereNull('deleted_at')
							->get()
							->toArray();

		/*$units = Unit::whereNull('deleted_at')
						->where('category_id',$category_id)
						->get()
						->toArray();
*/
		$response = [];
		$options =  '<option data-display="Choose Sub-Category" value="">Choose Sub-Category</option>';
		if(!empty($subs)){
			foreach ($subs as $key => $value) {
				$options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
			}
		}

		/*$options1 =  '<option data-display="Choose Unit" value="">Choose Unit</option>';
		if(!empty($units)){
			foreach ($units as $key => $value1) {
				$options1 .=  "<option value=".$value1['id'].">".ucfirst($value1['name'])." </option> ";
			}
		}*/

		$response['sub_category'] 	= $options;

		return $response;
	}

	public function get_subsubcategory($sub_category_id){
		$subs = SubSubCategory::where('sub_category_id',$sub_category_id)
							->whereNull('deleted_at')
							->get()
							->toArray();
		// echo '<pre>'; print_r($subs); die;

		$options =  '<option data-display="Choose Sub-Category 2" value="">Choose Sub-Category 2</option>';
		if(!empty($subs)){
			foreach ($subs as $key => $value) {
				$options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
			}
		}

		return $options;
	}

	public function delete($content_id){

		$trainer_id = Auth::User()->id;
		
		$del  = TrainerContent::where([
										'trainer_id'=>$trainer_id,
										'id'        =>$content_id
									])
							  ->update(['deleted_at'=>date('Y-m-d h:i:s')]);      

		if ($del) {
		    return redirect()->back()->with('success','Content deleted successfully');
		}else{
		    return redirect()->back()->with('error',COMMON_ERROR);
		}
	}

	public function render_contents(Request $request){

		if($request->isMethod('post')){
			$data = $request->all();
			// dd($data);
			$trainer_id = Auth::User()->id;
			$trainer_content =  TrainerContent::select('trainer_contents.*','c.name as category_name')
												->join('categories as c','trainer_contents.category_id','c.id')
												->where('trainer_id',$trainer_id)
												->where('upload_type',$data['content_type'])
												->whereNull('trainer_contents.deleted_at')
												->whereNull('c.deleted_at');
			
			if(!empty($data['category_id'])&&!empty($data['subject_id'])){
				
				$trainer_content = $trainer_content->where([
														'category_id'=>$data['category_id'],
														'subject_id' =>$data['subject_id'],
													]);

			}

			if(!empty(@$data['subject_id']) &&empty(@$data['category_id'])){

				$trainer_content = $trainer_content->where('subject_id',$data['subject_id']);
			}

			if(empty(@$data['subject_id']) &&!empty(@$data['category_id'])){


				$trainer_content = $trainer_content->where('category_id',$data['category_id']);

			}
			$trainer_content = $trainer_content->get()->toArray();
			$contents = '';
			// dd($trainer_content);
			if(!empty($trainer_content)){

			
			foreach ($trainer_content as $key => $value) {
				$file_url   = 'javascript:;';
				$delete_url = url('/trainer/content/delete/'.$value['id']); 
				if(!empty($value['file'])) {
				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
				        $file_url = TrainerContentImgPath.'/'.$value['file'];
				    }
				}   
				$contents.='<tr>
							    <td>'.++$key.'</td>
							    <td>'.ucfirst($value["title"]).'</td>
							    <td>'.ucfirst($value["category_name"]).'</td>
							    <td>'.date('d/m/y',strtotime($value['created_at'])).'</td>
							    <td>'.ucfirst($value["content_availability"]).'</td>
							    <td class="icoss">
							    	<a href='.$file_url.'>
							        <i data-toggle="tooltip" title="" class="fas fa-download cp" data-original-title="Download PDF"></i></a>
							        <a href="'.$delete_url.'"  target="_blank">
							        <i data-toggle="tooltip" title="Remove" class="fas fa-trash cp rmv_tr" data-original-title="Download PDF"></i></a>
							    </td>
							</tr>';
			}
			return $contents;
			}else{
				$contents .='<tr >
                                <td colspan="6" style="text-align: center;">No Record Found</td>
                            </tr>';
				return $contents;
			}
		}
	}


    public function get_units($subject_id){

    	$units = Unit::where('subject_id',$subject_id)
    				  ->whereNull('deleted_at')
    				  ->get()
    				  ->toArray();
    	$unit_options .= '<option data-display="Choose Unit>Select Unit</option>';
    	if(!empty($units)){

			foreach ($units as $key => $value) {
				$unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
			}
		
    	}
    	return $unit_options;
	}

}
