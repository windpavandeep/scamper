<?php

namespace App\Http\Controllers\frontEnd\trainer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainerContent,Auth,App\Transaction,App\User,App\SubscriptionPlan;
class DashboardController extends Controller
{
	public function index(){

		$trainer_id  = Auth::User()->id;
		$video_count = TrainerContent::where('trainer_id',$trainer_id)
									  ->where('upload_type','video')
									  ->whereNull('deleted_at')
									  ->count();
		$pdf_count = TrainerContent::where('trainer_id',$trainer_id)
									  ->where('upload_type','pdf')
									  ->whereNull('deleted_at')
									  ->count();
		$pending_earning_count  = Transaction::join('trainer_contents as tc','tc.id','transactions.course_id')
											  ->where('paid_status','N')
											  ->where('tc.trainer_id',$trainer_id)
											  ->sum('trainer_price');
		/*$pending_earning_count1  = Transaction::join('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
											  ->where('paid_status','N')
											  ->where('sp.trainer_id',$trainer_id)
											  ->sum('trainer_price');
		$pending_earning_count = $pending_earning_count+$pending_earning_count1;*/
		$total_earning_count  = Transaction::join('trainer_contents as tc','tc.id','transactions.course_id')
											  ->where('paid_status','Y')
											  ->where('tc.trainer_id',$trainer_id)
											  ->sum('trainer_price');
		
		/*$total_earning_count1  = Transaction::join('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
											  ->where('paid_status','Y')
											  ->where('sp.trainer_id',$trainer_id)
											  ->sum('trainer_price');				
		$total_earning_count = $total_earning_count+$total_earning_count1;*/				  
		$admin_content_ids = TrainerContent::where('trainer_id',$trainer_id)
											->pluck('id')
											->toArray();
		$subscription_plan_ids = SubscriptionPlan::where('trainer_id',$trainer_id)
												->pluck('id')
												->toArray();								
	/*	$pending_earning_count = Transaction::leftJoin('trainer_contents as tc','tc.id','transactions.course_id')
								->leftJoin('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
								->whereIn('course_id',$admin_content_ids)
								->whereIn('subscription_plan_id',$subscription_plan_ids)
								->where('paid_status','N')
								->sum('trainer_price');*/									  
		$page = 'dashboard';
	    return view('frontEnd.trainer.dashboard',compact('page','video_count','pdf_count','pending_earning_count','total_earning_count'));    
	}
}
