<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction,App\User,Auth;
class ContentController extends Controller
{
	public function index(){
		$student_id = Auth::User()->id; 
		$details    = Transaction::select('transactions.*','tc.title','tc.upload_type','tc.file')
									->join('trainer_contents as tc','transactions.course_id','tc.id')
								  	->where('user_id',$student_id)
								  	->get()
								  	->toArray();
		// echo'<pre>'; print_r($details);die;
		$page = 'purchased_contents';
		return view('frontEnd.user.purchasedContents.index',compact('page','details'));
	}

}
