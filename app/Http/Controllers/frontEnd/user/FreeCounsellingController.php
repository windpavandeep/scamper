<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class FreeCounsellingController extends Controller
{
    public function index() {
    	$page = 'Free Counselling';

    	return view('frontEnd.user.freeCounselling.index',compact('page'));
    }

}
