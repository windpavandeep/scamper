<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionHistoryController extends Controller
{
    public function index(){

    	$page ='subscription_history';
    	return view('frontEnd.user.subscriptionHistory.index',compact('page'));
    }
}
