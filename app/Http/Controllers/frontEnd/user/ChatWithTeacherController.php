<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class ChatWithTeacherController extends Controller
{
    public function index() {
    	$page = 'Chat With Teacher';
    	return view('frontEnd.user.chatTeacher.index',compact('page'));
    }

}
