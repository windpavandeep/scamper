<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DateTime;
use Auth,App\User,App\Subject,App\Unit,App\Question,App\UserCourse,App\TrainerContent,App\Transaction,App\UserExam,App\UserExamQuestions,App\UserExamQuestionOptions,App\Option,App\SubSubCategory,App\QuestionBank,App\QuestionBankOption;
class PrepExamController extends Controller
{
    public function index() {
    	$page = 'Prep Exams';
        $userId = Auth::id();
        $transaction =  Transaction::where(['user_id'=>$userId])->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }
        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();
        if(count($maincourse) <=0 ) {
            return redirect('/student/dashboard')->with('success','You do not have any subscription');
        }
        $activeMainCourse  = @$maincourse[0]; 
    	$subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();
        $sub_subcategory_id  =  $activeMainCourse['sub_subcategory_id'];
        $subject_ids  = array();
        if(!empty($subject)) {
            foreach($subject as $subject_id_value){
                $subject_ids[]  = $subject_id_value['id'];
            }
        }
        $all_subject_analysis  = UserExam::where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->get()->toArray();
        $total_marks  = 0;
        $total_marks_obtained  = 0;
        $no_test_given  = 0;
        if(!empty($all_subject_analysis)){
            foreach($all_subject_analysis as $all_subject_analysis_value){
                $total_marks  =   $total_marks+$all_subject_analysis_value['total_marks'];
                $total_marks_obtained  = $total_marks_obtained+$all_subject_analysis_value['marks_obtained'];
            }
        } else{
            $no_test_given  = 1;
        }
        if($total_marks > 0){    
            $all_subject_score  =  round($total_marks_obtained*100/$total_marks);
            $all_subject_score   = intval($all_subject_score);
        } else{
            $all_subject_score  = 0;
        }
        $graph_data  =  array();
        $all_subject_graph_data  = UserExam::select('created_at','total_attempt','total_marks','marks_obtained','user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->get();

        if(!empty($all_subject_graph_data)){
            foreach($all_subject_graph_data as $all_subject_graph_value){
                $total_marks  = $all_subject_graph_value['total_marks'];
                $total_marks_obtained =  $all_subject_graph_value['marks_obtained'];
                $created_date  = date('Y-m-d',strtotime($all_subject_graph_value['created_at']));
                $graph_datafinal['label'] = $created_date;
                $graph_datafinal['y']  = $total_marks_obtained; 
                //$graph_datafinal['indexLabel']  = "Test Date:".$created_date."Total scored:".$total_marks_obtained;
                $graph_data[]  =   $graph_datafinal;
             }
        }

       $json_graph_data  = json_decode(json_encode($graph_data));

       $all_subject_graph_data_score  = UserExam::select(DB::raw('count(id) as `total_attempt`'),DB::raw('sum(total_marks) as `total_marks`'),DB::raw('sum(marks_obtained) as `marks_obtained`'),'user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->first();
        $total_attempt  = @$all_subject_graph_data_score['total_attempt'];
        $average_score  = 0;
        if(!empty($all_subject_graph_data_score)){
            if($all_subject_graph_data_score['total_marks'] > 0){
                $average_score   = round($all_subject_graph_data_score['marks_obtained']/$all_subject_graph_data_score['total_attempt'],2);
            }
        }

        $graph_trade  = "Poor";
        $trade_color_class  = "poor_class";
        if($average_score >0 && $average_score <= 50){
           $graph_trade  = "Poor";
           $trade_color_class  = "poor_class";
        } 
        if($average_score >50 && $average_score <= 70){
            $graph_trade  = "Good";
            $trade_color_class  = "good_class";
        } 
        if($average_score >70 && $average_score <= 85){
            $graph_trade  = "Very Good";
            $trade_color_class  = "very_good_class";
        } 
        if($average_score >85 && $average_score <= 100){
            $graph_trade  = "Excellent";
            $trade_color_class  = "excellent_class";
        }

        $active_course_id  =  $activeMainCourse['id'];
        //dd($graph_data);
    	return view('frontEnd.user.prepexam.index',compact('page','subject','maincourse','sub_subcategory_id','all_subject_score','userId','graph_data','active_course_id','json_graph_data','total_attempt','average_score','graph_trade','no_test_given','trade_color_class'));
    }

    public function prepExamInstructions(Request $request) {
    	if($request->isMethod('post')){
    		$post_data  =  $request->all();
            $subject_id  =  $post_data['subject_id'];
            $unit_id  =  $post_data['unit_id'];
    		unset($post_data['_token']);
            $question =  QuestionBank::where(['subject_id'=>$subject_id,'unit_id'=>$unit_id,'deleted_at'=>null])->with(['question_bank_options'])->inRandomOrder()->limit(10)->get();
            if(count($question) <= 0){
                return redirect('/student/prep-exam')->with('success','This subject and unit do not have any question');
            }
            $subject = Subject::where(['id'=> $subject_id])->first();
            $unit  =  Unit::where(['id'=>$unit_id])->first(); 
            $totalQuestion  =  count($question);
            $totaltime  =  $totalQuestion*2;
            $sub_subcategory = SubSubCategory::where('id',$post_data['sub_subcategory_id'])
                                              ->first();
            $sub_subcategory_id  = $post_data['sub_subcategory_id'];
    		return view('frontEnd.user.prepexam.prepexaminstructions',compact('page','post_data','totalQuestion','totaltime','subject_id','unit_id','subject','unit','sub_subcategory','sub_subcategory_id'));
    	}
    }

    public function examQues($subject_id,$unit_id,$totaltime,$sub_subcategory_id) {
        $page  =  "Exam Ques";
        $subject_id  =  base64_decode($subject_id);
        $unit_id  = base64_decode($unit_id);
        $totaltime  = base64_decode($totaltime);
        $sub_subcategory_id  = base64_decode($sub_subcategory_id);
        if(empty($totaltime)){
            $totaltime  = 20;
        }
        $totaltimesecond  =  $totaltime*60;
        $subject = Subject::where(['id'=>$subject_id])->first();
        $unit = Unit::where(['id'=>$unit_id])->first();
        $question =  QuestionBank::where(['subject_id'=>$subject_id,'unit_id'=>$unit_id,'deleted_at'=>null])->with(['question_bank_options'])->inRandomOrder()->limit(10)->get();
        if(count($question) < 1){
            return redirect('/student/prep-exam')->with('success','This subject and unit do not have any question');
        }
        $total_marks  = 0;
        foreach($question as $questionval){
            $total_marks  = $total_marks+$questionval['marks'];
        } 
        $total_question_count  =  count($question);
        $first_question  =  @$question[0];
        $sub_subcategory = SubSubCategory::where('id',$sub_subcategory_id)->first();
        $start_date_time  =  date('Y-m-d H:i:s');
        $first_question_id  = $question[0]['id'];
        return view('frontEnd.user.prepexam.examQues',compact('page','subject_id','unit_id','subject','unit','question','totaltime','totaltimesecond','total_question_count','first_question','sub_subcategory','total_marks','start_date_time','first_question_id'));
    }

    public function prepTestSubmit(Request $request) {
        if($request->ajax()){
        	$userId = Auth::id();
            $post_data  =  $request->all();
            $question_ids  = json_decode($post_data['question_ids']);
            $answeroption  = json_decode($post_data['answer_option']);
            if(is_array($question_ids) && count($question_ids) > 0){
            	$user_exam  = new UserExam;
            	$user_exam->exam_id  =  0;
            	$user_exam->user_id  =  $userId;
            	$user_exam->exam_name = "Mock Test"; 
            	$user_exam->total_question  = 10;
                $user_exam->total_marks  = 40;
                $user_exam->subject_id  = $post_data['subject_id'];
                $user_exam->start_date_time  =  $post_data['start_date_time'];
                $user_exam->end_date_time = date('Y-m-d H:i:s');
                $user_exam->finish_status = "finish";
                $user_exam->total_attempt  = count($answeroption);
            	$user_exam->save();
            	$user_exam_id  =   $user_exam->id;
                $total_mark_obtain  = 0;
            		foreach($question_ids as $question_val){
            			$question_data  = QuestionBank::where(['id'=>$question_val])->first();
        				$user_exam_question  = new UserExamQuestions;
        				$user_exam_question->user_exam_id   = $user_exam_id;
        				$user_exam_question->question  = $question_data['question'];
                        $user_exam_question->question_id  = $question_val;
        				$user_exam_question->question_image = $question_data['ques_image'];
        			    $user_exam_question->answer_explanation  = $question_data['answer_explanation'];
        			    $user_exam_question->answer_explanation_image  = $question_data['ans_image'];
        			    $user_exam_question->marks  = 4;
        			    $user_exam_question->correct_answer  = $question_data['correct_answer'];
                        $user_exam_question->given_answer  = 0;
                        foreach($answeroption as $answeroptionval) { 
                            if($answeroptionval[0] == $question_val){
        			            $user_exam_question->given_answer  =  @$answeroptionval[1];
                                break;
                            }   
                        }
                        if($user_exam_question->correct_answer  == $user_exam_question->given_answer){
                            $user_exam_question->ans_status = "true";
                            $total_mark_obtain  =  $total_mark_obtain+4;
                        } elseif($user_exam_question->given_answer == NULL || $user_exam_question->given_answer == null){
                            $user_exam_question->ans_status = "not_attempt";
                        } else{
                            $user_exam_question->ans_status = "false";
                        }
                        DB::table('user_exams')->where(['id'=>$user_exam_question->user_exam_id])->update(['marks_obtained'=>$total_mark_obtain]);
            			$user_exam_question->save();
            			$user_exam_question_id  =   $user_exam_question->id;
            			$option_data  = QuestionBankOption::where(['question_bank_id'=>$question_data['id'],'position'=>@$user_exam_question->given_answer])->first();
                        if($option_data){
                			$option_data_obj  = new UserExamQuestionOptions;
                			$option_data_obj->user_exam_question_id  = $user_exam_question_id;
                			$option_data_obj->options  = $option_data['options'];
                			$option_data_obj->option_image = $option_data['option_image'];
                			$option_data_obj->position  = $option_data['position'];
                			$option_data_obj->save();
                        }
            	    }
            }

            $subcategory_name = $post_data['subcategory_name'];
            $subject_name  = $post_data['subject_name'];
            $unit_name  = $post_data['unit_name'];
            $question_ids  = $post_data['question_ids'];
            $answer_option = $post_data['answer_option'];
            $start_date_time  = $post_data['start_date_time'];
            $end_date_time = date('Y-m-d H:i:s');
            $first_question_id = $post_data['first_question_id'];
            $reviewtestrecord  =  $this->prepReviewTest($question_ids,$answer_option,$start_date_time,$end_date_time);
            $encoded_post_data  =  json_encode($post_data);
            $question   = $post_data['question'];
            $question_array  =  array();
            foreach($question as $questionval){
                $question_array[]  =  $questionval['id'];
            }

            //dd($post_data);
            $render_view =   view('frontEnd.user.prepexam.submitlastRender',compact('subject_name','unit_name','reviewtestrecord','subcategory_name','first_question_id','question_ids','post_data','user_exam_id','question','question_array'))->render();
            return response()->json(['status'=>'false','render_view'=>$render_view]); 
        }
    }

    public function saveNextPreptest(Request $request) {
        if($request->ajax()){
            $post_data  =  $request->all();
            $question_id  =  $post_data['question_id'];
            $answerelems  =  json_decode($post_data['answerelems']);
            $subcategory_name = $post_data['subcategory_name'];
            $total_question_count =  $post_data['total_question_count'];
            unset($post_data['_token']);
            $nextccount  =  $post_data['nextccount'];
            $nextccountplus  = $nextccount+1;
            if( $nextccount == $total_question_count){
                $render_view =   view('frontEnd.user.prepexam.lastRender')->render();
                return response()->json(['status'=>'false','nextccountplus'=>$nextccountplus,'render_view'=>$render_view]);
            }
            $question  =  $post_data['question'];
            $nextquestion  =  $question[$post_data['nextccount']];
            $new_question_id = $nextquestion['id'];
            $render_view =   view('frontEnd.user.prepexam.nextQuestion',compact('nextquestion','nextccount'))->render();
            $next_question_id  =   $question[$nextccount-1]['id'];
            $option_value  = 0;
            if(is_array($answerelems)){
                foreach($answerelems as $answeroptionval){
                    if($answeroptionval[0] == $new_question_id){
                        $option_value  =    @$answeroptionval[1];
                    }
                }
            }
            return response()->json(['status'=>'true','render_view'=>$render_view,'nextccountplus'=>$nextccountplus,'next_question_id'=>$next_question_id,'option_value'=>$option_value,'question_id'=>$question_id,'new_question_id'=>$new_question_id]);
        }
    }


    public function markForreviewPreptest(Request $request) {
        if($request->ajax()){
            $post_data  =  $request->all();
            $answerelems  =  json_decode($post_data['answerelems']);
            $total_question_count =  $post_data['total_question_count'];
            unset($post_data['_token']);
            $nextccount  =  $post_data['nextccount'];
            $nextccountplus  = $nextccount+1;
            if( $nextccount == $total_question_count){
                $render_view =   view('frontEnd.user.prepexam.lastRender')->render();
                return response()->json(['status'=>'false','nextccountplus'=>$nextccountplus,'render_view'=>$render_view]);
            }
            $question  =  $post_data['question'];
            $nextquestion  =  $question[$post_data['nextccount']];
            $render_view =   view('frontEnd.user.prepexam.nextQuestion',compact('nextquestion','nextccount'))->render();
            $next_question_id  =  $nextquestion['id'];
            $option_value  = 0;
            if(is_array($answerelems)){
                foreach($answerelems as $answeroptionval){
                    if($answeroptionval[0] == $next_question_id){
                      $option_value  =    @$answeroptionval[1];
                    }
                }
            }
            return response()->json(['status'=>'true','render_view'=>$render_view,'nextccountplus'=>$nextccountplus,'next_question_id'=>$next_question_id,'option_value'=>$option_value]);
        }
    }

    public function backToQuestion(Request $request) {
        if($request->ajax()){
            $post_data  =  $request->all();
            unset($post_data['_token']);
            $question  =  $post_data['question'];
            $nextquestion = "";
            foreach($question as $questionval){
                if($questionval['id'] == $post_data['question_id']){
                    $nextquestion  = $questionval;
                    break;
                }
            }
            $question_count  =  $post_data['question_count'];
            $render_view =   view('frontEnd.user.prepexam.backQuestion',compact('nextquestion','nextccount','question_count'))->render();
            return response()->json(['status'=>'true','render_view'=>$render_view]);
        }
    }


    public function getSubjectByCondtion(Request $request){
        $data  =  $request->all();
        $main_course  =  $data['main_course'];
        $userId = Auth::id();
        $course_id  =  $data['course_id'];
        $activeMainCourse =  TrainerContent::where('id',$course_id)->first();
        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();
        $subject_options = '<option value="">Select Subject</option>';
        $subject_options_graph = '<option value="all_subject">All Subject</option>';
        $subject_ids  = array();
        if(!empty($subject)){
            foreach ($subject as $key => $value) {
                $subject_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
                $subject_options_graph .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
                $subject_ids[]  = $value['id'];
            }
        }
        $sub_subcategory_id  =  $activeMainCourse['sub_subcategory_id'];
        $activeMainCourseNew =  TrainerContent::where('id',$course_id)->get()->toArray();
        $finalarray  = array_merge($activeMainCourseNew,$main_course);
        $maincourse  =  array_unique($finalarray,SORT_REGULAR);
        $active_course_id  =  $activeMainCourse['id'];
        $render_view  =   view('frontEnd.user.prepexam.courseRender',compact('maincourse','course_id','active_course_id'))->render();
        $all_subject_analysis  = UserExam::where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->get()->toArray();
        $total_marks  = 0;
        $total_marks_obtained  = 0;
        $no_test_given  = 0;
        if(!empty($all_subject_analysis)){
            foreach($all_subject_analysis as $all_subject_analysis_value){
                $total_marks  =   $total_marks+$all_subject_analysis_value['total_marks'];
                $total_marks_obtained  = $total_marks_obtained+$all_subject_analysis_value['marks_obtained'];
            }
        } else{
            $no_test_given  = 1;
        }
        if($total_marks > 0){    
            $all_subject_score  =  round($total_marks_obtained*100/$total_marks);
            $all_subject_score  =  intval($all_subject_score);
        } else{
            $all_subject_score  = 0;
        }
        $analysis_report  =  view('frontEnd.user.prepexam.changeAnalysisReportSubject',compact('subject','all_subject_score','userId','no_test_given'))->render();
        $graph_data  =  array();
        $all_subject_graph_data  = UserExam::select('created_at','total_attempt','total_marks','marks_obtained','user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->get();
        if(!empty($all_subject_graph_data)){
            foreach($all_subject_graph_data as $all_subject_graph_value){
                $total_marks  = $all_subject_graph_value['total_marks'];
                $total_marks_obtained =  $all_subject_graph_value['marks_obtained'];
                $created_date  = date('Y-m-d',strtotime($all_subject_graph_value['created_at']));
                $graph_datafinal['label'] = $created_date;
                $graph_datafinal['y']  = $total_marks_obtained;  
                $graph_data[]  =   $graph_datafinal;
             }
        }

        $json_graph_data  = json_decode(json_encode($graph_data));
        $all_subject_graph_data_score  = UserExam::select(DB::raw('count(id) as `total_attempt`'),DB::raw('sum(total_marks) as `total_marks`'),DB::raw('sum(marks_obtained) as `marks_obtained`'),'user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->first();
        $total_attempt  = @$all_subject_graph_data_score['total_attempt'];
        $average_score  = 0;
        if(!empty($all_subject_graph_data_score)){
            if($all_subject_graph_data_score['total_marks'] > 0){
                $average_score   = round($all_subject_graph_data_score['marks_obtained']/$all_subject_graph_data_score['total_attempt'],2);
            }
        }
        $graph_trade  = "Poor";
        $trade_color_class  = "poor_class";
        if($average_score >0 && $average_score <= 50){
           $graph_trade  = "Poor";
           $trade_color_class  = "poor_class";
        } 
        if($average_score >50 && $average_score <= 70){
            $graph_trade  = "Good";
            $trade_color_class  = "good_class";
        } 
        if($average_score >70 && $average_score <= 85){
            $graph_trade  = "Very Good";
            $trade_color_class  = "very_good_class";
        } 
        if($average_score >85 && $average_score <= 100){
            $graph_trade  = "Excellent";
             $trade_color_class  = "excellent_class";
        }
        $graph_report = view('frontEnd.user.prepexam.changeGraphAnalysisReportSubject',compact('graph_data','json_graph_data','total_attempt','average_score','graph_trade','trade_color_class'))->render();
        return response()->json(['status'=>'true','subject_options'=>$subject_options,'sub_subcategory_id'=>$sub_subcategory_id,'render_view'=>$render_view,'analysis_report'=>$analysis_report,'graph_report'=>$graph_report,'subject_options_graph'=>$subject_options_graph,'active_course_id'=>$active_course_id]);
    }

    

    public function goToNextQuestion(Request $request) {
        if($request->ajax()){
            $post_data  =  $request->all();
            $answerelems  =  json_decode($post_data['answerelems']);
            unset($post_data['_token']);
            $question  =  $post_data['question'];
            $key =  $post_data['next_question_total_count'];
            if($key >= count($question)){
                $render_view =   view('frontEnd.user.prepexam.lastRender')->render();
                return response()->json(['status'=>'false','key'=>$key]);
            }
            $nextccount  =  $key+1;
            $nextquestion  =  @$question[$key];
            $next_question_id  =  @$nextquestion['id']; 
            $option_value  = 0;
                if(is_array($answerelems)){
                    foreach($answerelems as $answeroptionval){
                        if($answeroptionval[0] == $next_question_id){
                            $option_value  =    @$answeroptionval[1];
                        }
                    }
                }
            $render_view =   view('frontEnd.user.prepexam.gotoNextquestion',compact('nextquestion','nextccount'))->render();
            return response()->json(['status'=>'true','render_view'=>$render_view,'next_question_id'=>$next_question_id,'option_value'=>$option_value]);
        }
    }

    public function goToPrevoiusQuestion(Request $request) {
        if($request->ajax()){
            $post_data  =  $request->all();
            $question_id  = $post_data['question_id']; 
            $answerelems  =  json_decode($post_data['answerelems']);
            unset($post_data['_token']);
            $question  =  $post_data['question'];
            $key = 0;
            for($i = 0; $i < count($question); $i++) {
                if($question[$i]['id'] == $post_data['question_id']){
                    $key  =  $i;
                }
            }
            if($post_data['next_question_total_count'] <= 2){
                $key  =  $post_data['next_question_total_count']-1;
            } else{
                $key  =  $post_data['next_question_total_count'];
            }
            if($key == 0){
                $nextccount  =  $key;
                $nextquestion  =  $question[$nextccount];
                $next_question_id  =  @$nextquestion['id']; 
                $render_view =   view('frontEnd.user.prepexam.goToPrevoiusQuestion',compact('nextquestion','nextccount'))->render();
                $option_value  = 0;
                if(is_array($answerelems)){
                    foreach($answerelems as $answeroptionval){
                        if($answeroptionval[0] == $next_question_id){
                            $option_value  =    @$answeroptionval[1];
                        }
                    }
                }
                return response()->json(['status'=>'false','key'=>$key,'render_view'=>$render_view,'answerelems'=>$answerelems,'option_value'=>$option_value,'next_question_id'=>$next_question_id]);
            }
            $nextccount  =  $key-1;
            $nextquestion  =  $question[$nextccount];
            $next_question_id  =  @$nextquestion['id']; 
            if($post_data['next_question_total_count'] > 2){
                $nextccount  =  $nextccount-1;
            }
            $render_view =   view('frontEnd.user.prepexam.goToPrevoiusQuestion',compact('nextquestion','nextccount','next_question_id'))->render();
            $option_value  = 0;
                if(is_array($answerelems)){
                    foreach($answerelems as $answeroptionval){
                        if($answeroptionval[0] == $next_question_id){
                            $option_value  =    @$answeroptionval[1];
                        }
                    }
                }
            return response()->json(['status'=>'true','render_view'=>$render_view,'next_question_id'=>$next_question_id,'key'=>$key,'answerelems'=>$answerelems,'option_value'=>$option_value]);
        }
    }


    public function prepExamResult() {
        $page = 'Prep Exams';
        return view('frontEnd.user.prepexam.result',compact('page'));
    }

    public function graphAnalysis($subject_id,$active_course_id){
         $userId = Auth::id();
         if($subject_id  == 'all_subject'){
            $activeMainCourse  = TrainerContent::where(['id'=>$active_course_id])->first();
            $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();
            $subject_ids  = array();
            if(!empty($subject)) {
                foreach($subject as $subject_id_value){
                    $subject_ids[]  = $subject_id_value['id'];
                }
            }
            $all_subject_graph_data  = UserExam::select('created_at','total_attempt','total_marks','marks_obtained','user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->get();

            $all_subject_graph_data_score  = UserExam::select(DB::raw('count(id) as `total_attempt`'),DB::raw('sum(total_marks) as `total_marks`'),DB::raw('sum(marks_obtained) as `marks_obtained`'),'user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->whereIn('subject_id',$subject_ids)->first();
         } else{
             $all_subject_graph_data  = UserExam::select('created_at','total_attempt','total_marks','marks_obtained','user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->where('subject_id',$subject_id)->get();

            $all_subject_graph_data_score  = UserExam::select(DB::raw('count(id) as `total_attempt`'),DB::raw('sum(total_marks) as `total_marks`'),DB::raw('sum(marks_obtained) as `marks_obtained`'),'user_id','subject_id')->where(['user_id'=>$userId,'exam_id'=>0])->where('subject_id',$subject_id)->first();

         }
         $graph_data =  array();
         if(!empty($all_subject_graph_data)){
            foreach($all_subject_graph_data as $all_subject_graph_value){
                $total_marks  = $all_subject_graph_value['total_marks'];
                $total_marks_obtained =  $all_subject_graph_value['marks_obtained'];
                $created_date  = date('Y-m-d',strtotime($all_subject_graph_value['created_at']));
                $graph_datafinal['label'] = $created_date;
                $graph_datafinal['y']  = $total_marks_obtained;
                $graph_data[]  =   $graph_datafinal;
             }
        }

       $json_graph_data  = json_decode(json_encode($graph_data));
        $total_attempt  = @$all_subject_graph_data_score['total_attempt'];
        $average_score  = 0;
        if(!empty($all_subject_graph_data_score)){
            if($all_subject_graph_data_score['total_marks'] > 0){
                $average_score   = round($all_subject_graph_data_score['marks_obtained']/$all_subject_graph_data_score['total_attempt'],2);
            }
        }
        $graph_trade  = "Poor";
        $trade_color_class  = "poor_class";
        if($average_score >0 && $average_score <= 50){
           $graph_trade  = "Poor";
           $trade_color_class  = "poor_class";
        } 
        if($average_score >50 && $average_score <= 70){
            $graph_trade  = "Good";
            $trade_color_class  = "good_class";
        } 
        if($average_score >70 && $average_score <= 85){
            $graph_trade  = "Very Good";
            $trade_color_class  = "very_good_class";
        } 
        if($average_score >85 && $average_score <= 100){
            $graph_trade  = "Excellent";
            $trade_color_class  = "excellent_class";
        }
        $graph_data  =   view('frontEnd.user.prepexam.graph_data_not_found',compact('json_graph_data','graph_data','total_attempt','average_score','graph_trade','trade_color_class'))->render();
        return response()->json(['status'=>false,'graph_data'=>$graph_data]); 
    }


    function prepReviewTest($question_ids,$answer_option,$start_date_time,$end_date_time){
        $question_ids  = json_decode($question_ids);
        $answeroption  = json_decode($answer_option);
        $question_count =  count($question_ids);
        $total_mark_obtain  = 0;
        $correct_answer = 0;
        $incorrect_answer = 0;
        if($question_ids){
            foreach($question_ids as $question_val){
                $question_data  = QuestionBank::where(['id'=>$question_val])->first();
                foreach($answeroption as $answeroptionval) { 
                    if($answeroptionval[0] == $question_val){
                        $question_data['given_answer']  =  @$answeroptionval[1];
                        break;
                    }   
                }
                if($question_data['correct_answer']  == $question_data['given_answer']){
                    $total_mark_obtain  =  $total_mark_obtain+4;
                    $correct_answer++;
                }  elseif(!empty($question_data['given_answer'])){
                    $incorrect_answer++;
                }
            }
        }
        $total_attempet =  $correct_answer+ $incorrect_answer;
        $total_marks  =  40;
        $percentage_score  = round($total_mark_obtain*100/$total_marks); 
        $return_data['total_mark_obtain'] = $total_mark_obtain;
        $return_data['correct_answer']  = $correct_answer;
        $return_data['incorrect_answer']  = $incorrect_answer;
        $return_data['total_marks']   = $total_marks;
        $return_data['percentage_score'] = $percentage_score;
        $datetime1 = new DateTime($start_date_time);  
        $datetime2= new DateTime($end_date_time);
        $interval = $datetime1->diff($datetime2); 
        $total_min  = $interval->format('%i');
        $total_sec  = $interval->format('%s');
        $final_total_sec  = $total_min*60+$total_sec;
        $per_question_sec  = $final_total_sec/10; 
        $per_minutes = floor(($per_question_sec / 60) % 60);
        $per_seconds = $per_question_sec % 60;
        $time_taken  = $total_min."min ".$total_sec."sec";
        $per_question_time = $per_minutes."min ".$per_seconds."sec";;
        $return_data['time_taken']   = $time_taken;
        $return_data['per_question_time']   = $per_question_time;
        $return_data['left'] =  10-$total_attempet;
        return $return_data;
    }


    public function prepTestReview(Request $request){
         $page  =  "Review Page"; 
        if($request->isMethod('post')){
            $data  =  $request->all();
          //  dd($data);
            $user_exam_id  = $data['user_exam_id'];
            $question = array();
            if(is_array($data['question'])){
                foreach($data['question'] as $questionvalue){
                    $question_data =  QuestionBank::where('id',$questionvalue)->with(['question_bank_options'])->first();
                    $question[]  =   $question_data; 
                }
            }

            $user_exam_details  = UserExam::where(['id'=>$user_exam_id])->first();
            $subject_name  =  $data['subject_name'];
            $unit_name  = $data['unit_name'];
            $subcategory_name = $data['subcategory_name'];
            $totaltime  = '20';
            return view('frontEnd.user.prepexam.prep-test-review',compact('page','question','user_exam_details','subject_name','unit_name','subcategory_name','totaltime','user_exam_id'));
        } else{
            return redirect('/student/prep-exam');
        }
    }

    public function goToNextPrepReviewQuestion(Request $request){
        if($request->ajax()){
            $post_data  =  $request->all();
            $question  =  $post_data['question'];
            $index = 0;
            if(is_array($question) && count($question) > 0){
                foreach($question as $key => $questionval){
                    if($questionval['id'] == $post_data['question_id']){
                        $index  =  $key+1;
                        break;
                    }
                }
            }

            $nextquestion =  @$question[$index];
            $user_exam_id  =  $post_data['user_exam_id'];
            $render_view =   view('frontEnd.user.prepexam.gotoNextPrepReviewQuestion',compact('nextquestion','index','question','user_exam_id'))->render();
            return response()->json(['status'=>'true','render_view'=>$render_view,'index'=>$index]);
        }
    }

    public function goToPrepReviewQuestion(Request $request){
        if($request->ajax()){
            $post_data  =  $request->all();
            $question  =  $post_data['question'];
            $index = 0;
            if(is_array($question) && count($question) > 0){
                foreach($question as $key => $questionval){
                    if($questionval['id'] == $post_data['question_id']){
                        $index  =  $key-1;
                        break;
                    }
                }
            }

            $nextquestion =  @$question[$index];
            $user_exam_id  =  $post_data['user_exam_id'];
            $render_view =   view('frontEnd.user.prepexam.gotoNextPrepReviewQuestion',compact('nextquestion','index','question','user_exam_id'))->render();
            return response()->json(['status'=>'true','render_view'=>$render_view,'index'=>$index]);
        }
    }

    public function goToRigthSideReviewQuestion(Request $request){
        if($request->ajax()){
            $post_data  =  $request->all();
            $question  =  $post_data['question'];
            $index = 0;
            if(is_array($question) && count($question) > 0){
                foreach($question as $key => $questionval){
                    if($questionval['id'] == $post_data['question_id']){
                        $index  =  $key;
                        break;
                    }
                }
            }
            $nextquestion =  @$question[$index];
            $user_exam_id  =  $post_data['user_exam_id'];
            $render_view =   view('frontEnd.user.prepexam.gotoNextPrepReviewQuestion',compact('nextquestion','index','question','user_exam_id'))->render();
            return response()->json(['status'=>'true','render_view'=>$render_view,'index'=>$index]);
        }
    }

}
