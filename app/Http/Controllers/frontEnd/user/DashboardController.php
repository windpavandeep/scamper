<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User,App\TrainerDomain,App\Country,App\City,App\State,App\Category,App\SubCategory,App\UserCourse,App\TrainerContent,App\Transaction,App\Subject,App\Unit,App\TrainerContentSubject,App\TrainerContentSubjectUnit,App\TrainerContentViews;
class DashboardController extends Controller
{
    public function dashboard(){

        //dd('hi');
    	$student_id      = Auth::User()->id;
    	$student_details = User::select('users.*','c.name as country_name','s.name as state_name','city.name as city_name')
    							->join('countries as c','c.id','users.country_id')
    							->join('states as s','s.id','users.state_id')
    							->join('cities as city','city.id','users.city_id')
                                ->with('user_courses')
    							->where([
    						 		'user_type'        =>'user',
    						 		'users.id'         =>$student_id,
                                    'users.deleted_at' =>null
    						 	])    	  
    	                        ->first();

        $domains   = Category::select('*')
                               ->whereNull('deleted_at')
                               ->orderBy('name','asc')
                               ->get()
                               ->toArray();   

        $sub_categories = '';
        if(!empty(@$student_details['user_courses'])){
            $sub_categories = SubCategory::where('category_id',$student_details['user_courses']['category_id'])
                                          ->whereNull('deleted_at')
                                          ->orderBy('name','asc')
                                          ->get()
                                          ->toArray();   
        }  
    	                       // echo'<pre>'; print_r($student_details->toArray()); 
        $userId = Auth::id();
        //echo  $userId; die;
        $transaction =  Transaction::where(['user_id'=>$userId])->where('course_id', '!=' , 0)->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }

        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();

        //dd($maincourse);

        $activeMainCourse  = @$maincourse[0]; 
         // dd($activeMainCourse);
        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();


    	$page = 'Dashboard';

        /**** realted Viedo code start here  *****/

        $content_availability  = ['subscriber','free'];
       

        $activecoursevideo   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();


        $activecoursepdf   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get()->toArray();


        //dd($activecoursepdf);
        

    	return view('frontEnd.user.dashboard.index',compact('page','student_details','sub_categories','domains','maincourse','subject','activecoursevideo','activeMainCourse','activecoursepdf'));
    }

    public function liveLectures() {
        $page  =  "Live Lectures";
        return view('frontEnd.user.dashboard.live_lectures',compact('page'));
    }

    public function allVideos($active_id) {
        $page  =  "All Videos";
      

        $activeMainCourse  = TrainerContent::where(['id'=>$active_id])->first();

        $content_availability  = ['subscriber','free'];

         $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();


        $activecoursevideo   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();

      //dd($activecoursevideo);

        return view('frontEnd.user.dashboard.live_videos',compact('page','activecoursevideo','subject'));   
    }

    public function  relatedNotes($active_id) {
        $page  =  "Notes";

        $activeMainCourse  = TrainerContent::where(['id'=>$active_id])->first();

        $content_availability  = ['subscriber','free'];

        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();


        $activecoursepdf  =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();

        return view('frontEnd.user.dashboard.related_notes',compact('page','activecoursepdf','subject','active_id'));
    }

    public function  solvedExams() {
        $page  =  "Solved Exams";
        return view('frontEnd.user.dashboard.solved_exams',compact('page'));
    }

    public function getSubjectByCourse(Request $request){
        $data  =  $request->all();
        $main_course  =  $data['main_course'];
        //dd($main_course);
        $userId = Auth::id();

        $course_id  =  $data['course_id'];
        $activeMainCourse =  TrainerContent::where('id',$course_id)->first();
    
        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();

        $subject_options = '<option value="">Select Subject</option>';

        $subject_ids  = array();
        if(!empty($subject)){

            foreach ($subject as $key => $value) {
                $subject_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
                 $subject_ids[]  = $value['id'];
            }
        
        }
        $sub_subcategory_id  =  $activeMainCourse['sub_subcategory_id'];
        $activeMainCourseNew =  TrainerContent::where('id',$course_id)->get()->toArray();
        $finalarray  = array_merge($activeMainCourseNew,$main_course);
        
        $maincourse  =  array_unique($finalarray,SORT_REGULAR);

        $render_view  =   view('frontEnd.user.dashboard.courseRenderVideo',compact('maincourse','course_id'))->render();


        /**** realted Viedo code start here  *****/

        $content_availability  = ['subscriber','free'];
       
        $activecoursevideo   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();

      
        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video_render',compact('activecoursevideo'))->render();

        $video_serach_replace  = view('frontEnd.user.dashboard.video_serach_replace',compact('activeMainCourse','subject'))->render();


        /**** realted Notes code start here  *****/

        $activecoursepdf   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();

      
        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf_render',compact('activecoursepdf'))->render();

        $pdf_serach_replace  = view('frontEnd.user.dashboard.pdf_serach_replace',compact('activeMainCourse','subject'))->render();

       //dd($pdf_serach_replace );
        return response()->json(['status'=>'true','subject_options'=>$subject_options,'sub_subcategory_id'=>$sub_subcategory_id,'render_view'=>$render_view,'related_video_render_view'=>$related_video_render_view,'video_serach_replace'=>$video_serach_replace,'related_pdf_render_view'=>$related_pdf_render_view,'pdf_serach_replace'=>$pdf_serach_replace]);
    }


    public function unitVideo($subject_id){

        $units = Unit::where('subject_id',$subject_id)
                      ->whereNull('deleted_at')
                      ->get()
                      ->toArray();
        $unit_options = '<option selected>Select Units</option>';
        if(!empty($units)){

            foreach ($units as $key => $value) {
                $unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
            }
        
        }


        $TrainerContentSubject  = TrainerContentSubject::where(['subject_id'=>$subject_id])->get();
       
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }

        $content_availability  = ['subscriber','free'];


        $activecoursevideo   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();
        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video_render',compact('activecoursevideo'))->render();
        return response()->json(['status'=>'true','unit_options'=>$unit_options,'related_video_render_view'=>$related_video_render_view]);
    }



    public function subjectunitVideo($unit_id){

        $TrainerContentSubjectunit  = TrainerContentSubjectUnit::where(['unit_id'=>$unit_id])->get();
        $TrainerContentsubjectids  =  array();
        if(!empty($TrainerContentSubjectunit)){
            foreach($TrainerContentSubjectunit as $uvalue){
                $TrainerContentsubjectids[]  =  $uvalue['trainer_content_subject_id'];
            }
        }

        //dd($TrainerContentsubjectids);

        $TrainerContentSubject  = TrainerContentSubject::whereIn('id',$TrainerContentsubjectids)->get();

//        dd($TrainerContentSubject);
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }

       // dd($TrainerContentSubject);
        
        $content_availability  = ['subscriber','free'];


        $activecoursevideo   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();
        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','related_video_render_view'=>$related_video_render_view]);
    }


     public function viewunitVideo($subject_id){

        $units = Unit::where('subject_id',$subject_id)
                      ->whereNull('deleted_at')
                      ->get()
                      ->toArray();
        $unit_options = '<option selected>Select Units</option>';
        if(!empty($units)){

            foreach ($units as $key => $value) {
                $unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
            }
        
        }


        $content_availability  = ['subscriber','free'];


        $TrainerContentSubject  = TrainerContentSubject::where(['subject_id'=>$subject_id])->get();
       
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }


        $activecoursevideo   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();
        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video__view_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','unit_options'=>$unit_options,'related_video_render_view'=>$related_video_render_view]);
    }


    public function viewSubjectUnitVideo($unit_id){
    	
    	$TrainerContentSubjectunit  = TrainerContentSubjectUnit::where(['unit_id'=>$unit_id])->get();
        $TrainerContentsubjectids  =  array();
        if(!empty($TrainerContentSubjectunit)){
            foreach($TrainerContentSubjectunit as $uvalue){
                $TrainerContentsubjectids[]  =  $uvalue['trainer_content_subject_id'];
            }
        }

        //dd($TrainerContentsubjectids);

        $TrainerContentSubject  = TrainerContentSubject::whereIn('id',$TrainerContentsubjectids)->get();

//        dd($TrainerContentSubject);
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }



        $content_availability  = ['subscriber','free'];


        $activecoursevideo   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();
        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video__view_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','related_video_render_view'=>$related_video_render_view]);
    }



    public function viewVideoByTitle($title){
    
        $userId = Auth::id();
        //echo  $userId; die;
        $transaction =  Transaction::where(['user_id'=>$userId])->where('course_id', '!=' , 0)->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }

        $content_availability  = ['subscriber','free'];

    
        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();

        $category_id  =  array();
        $sub_category_id = array();
        $sub_subcategory_id  = array();

        foreach( $maincourse as  $maincoursevalue){
            $category_id[]  = $maincoursevalue['category_id'];
            $sub_category_id[]  = $maincoursevalue['sub_category_id'];
            $sub_subcategory_id[] = $maincoursevalue['sub_subcategory_id'];
        }

        $activecoursevideo   =  TrainerContent::whereIn('category_id',$category_id)->whereIn('sub_category_id',$sub_category_id)->whereIn('sub_subcategory_id',$sub_subcategory_id)->whereIn('content_availability',$content_availability)->where('title', 'LIKE', "%$title%")->with(['course_files'=>function ($query) {
                $query->where('type','video');
        }])->with('course_image')->get();

        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video__view_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','related_video_render_view'=>$related_video_render_view]);
    }



    public function viewAllVideo(){
    
        $userId = Auth::id();
        //echo  $userId; die;
        $transaction =  Transaction::where(['user_id'=>$userId])->where('course_id', '!=' , 0)->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }

        $content_availability  = ['subscriber','free'];

        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();

        $category_id  =  array();
        $sub_category_id = array();
        $sub_subcategory_id  = array();

        foreach( $maincourse as  $maincoursevalue){
            $category_id[]  = $maincoursevalue['category_id'];
            $sub_category_id[]  = $maincoursevalue['sub_category_id'];
            $sub_subcategory_id[] = $maincoursevalue['sub_subcategory_id'];
        }

        $activecoursevideo   =  TrainerContent::whereIn('category_id',$category_id)->whereIn('sub_category_id',$sub_category_id)->whereIn('sub_subcategory_id',$sub_subcategory_id)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                $query->where('type','video');
        }])->with('course_image')->get();

        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video__view_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','related_video_render_view'=>$related_video_render_view]);
    }



    public function viewAllVideoActiveCourse($active_id){
    
        $userId = Auth::id();
        
        $activeMainCourse  = TrainerContent::where(['id'=>$active_id])->first();

         $content_availability  = ['subscriber','free'];

        $activecoursevideo   =  TrainerContent::where(['category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>@$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> @$activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();
        

        $related_video_render_view  =   view('frontEnd.user.dashboard.related_video_render',compact('activecoursevideo'))->render();

        return response()->json(['status'=>'true','related_video_render_view'=>$related_video_render_view]);
    }



    /*******************    Notes serach filter function  **************/

    public function unitNotes($subject_id){

        $units = Unit::where('subject_id',$subject_id)
                      ->whereNull('deleted_at')
                      ->get()
                      ->toArray();
        $unit_options = '<option selected>Select Units</option>';
        if(!empty($units)){

            foreach ($units as $key => $value) {
                $unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
            }
        
        }


        $TrainerContentSubject  = TrainerContentSubject::where(['subject_id'=>$subject_id])->get();
       
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }

        $content_availability  = ['subscriber','free'];


        $activecoursepdf  =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();
        
      
        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf_render',compact('activecoursepdf'))->render();

      //  dd($activecoursevideo);
        return response()->json(['status'=>'true','unit_options'=>$unit_options,'related_pdf_render_view'=>$related_pdf_render_view]);
    }



    public function subjectunitNotes($unit_id){

        $TrainerContentSubjectunit  = TrainerContentSubjectUnit::where(['unit_id'=>$unit_id])->get();
        $TrainerContentsubjectids  =  array();
        if(!empty($TrainerContentSubjectunit)){
            foreach($TrainerContentSubjectunit as $uvalue){
                $TrainerContentsubjectids[]  =  $uvalue['trainer_content_subject_id'];
            }
        }

        //dd($TrainerContentsubjectids);

        $TrainerContentSubject  = TrainerContentSubject::whereIn('id',$TrainerContentsubjectids)->get();

//        dd($TrainerContentSubject);
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }

       // dd($TrainerContentSubject);
        
        $content_availability  = ['subscriber','free'];


        $activecoursepdf  =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();
        

        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf_render',compact('activecoursepdf'))->render();

        return response()->json(['status'=>'true','related_pdf_render_view'=>$related_pdf_render_view]);
    }


     public function viewunitNotes($subject_id){

        $units = Unit::where('subject_id',$subject_id)
                      ->whereNull('deleted_at')
                      ->get()
                      ->toArray();
        $unit_options = '<option selected>Select Units</option>';
        if(!empty($units)){

            foreach ($units as $key => $value) {
                $unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
            }
        
        }


        $content_availability  = ['subscriber','free'];


        $TrainerContentSubject  = TrainerContentSubject::where(['subject_id'=>$subject_id])->get();
       
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }


        $activecoursepdf   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();
        

        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf__view_render',compact('activecoursepdf'))->render();

        return response()->json(['status'=>'true','unit_options'=>$unit_options,'related_pdf_render_view'=>$related_pdf_render_view]);
    }


    public function viewSubjectUnitNotes($unit_id){
        
        $TrainerContentSubjectunit  = TrainerContentSubjectUnit::where(['unit_id'=>$unit_id])->get();
        $TrainerContentsubjectids  =  array();
        if(!empty($TrainerContentSubjectunit)){
            foreach($TrainerContentSubjectunit as $uvalue){
                $TrainerContentsubjectids[]  =  $uvalue['trainer_content_subject_id'];
            }
        }

        //dd($TrainerContentsubjectids);

        $TrainerContentSubject  = TrainerContentSubject::whereIn('id',$TrainerContentsubjectids)->get();

//        dd($TrainerContentSubject);
        $TrainerContentids  =  array();
        if(!empty($TrainerContentSubject)){
            foreach($TrainerContentSubject as $tvalue){
                $TrainerContentids[]  =  $tvalue['trainer_content_id'];
            }
        }



        $content_availability  = ['subscriber','free'];


        $activecoursepdf   =  TrainerContent::whereIn('id',$TrainerContentids)->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();
        

        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf__view_render',compact('activecoursepdf'))->render();

        return response()->json(['status'=>'true','related_pdf_render_view'=>$related_pdf_render_view]);
    }



    public function viewNotesByTitle($title,$active_id){
    
        $userId = Auth::id();

        $activeMainCourse  = TrainerContent::where(['id'=>$active_id])->first();

        $content_availability  = ['subscriber','free'];

        $activecoursepdf  =  TrainerContent::where(['category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>@$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> @$activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->where('title', 'LIKE', "%$title%")->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();

        

        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf__view_render',compact('activecoursepdf'))->render();

        return response()->json(['status'=>'true','related_pdf_render_view'=>$related_pdf_render_view]);
    }


    public function viewAllNotesActiveCourse($active_id){
    
        $userId = Auth::id();
        
        $activeMainCourse  = TrainerContent::where(['id'=>$active_id])->first();

        $content_availability  = ['subscriber','free'];

        $activecoursepdf  =  TrainerContent::where(['category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>@$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> @$activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();
        

        $related_pdf_render_view  =   view('frontEnd.user.dashboard.related_pdf_render',compact('activecoursepdf'))->render();

        //dd($related_pdf_render_view);

        return response()->json(['status'=>'true','related_pdf_render_view'=>$related_pdf_render_view]);
    }

    public function videoLectures(){
        $page = 'Video Lectures';
        $userId = Auth::id();
        $transaction =  Transaction::where(['user_id'=>$userId])->where('course_id', '!=' , 0)->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }
        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();
        if(count($maincourse) <=0 ) {
            return redirect('/student/dashboard')->with('success','You do not have any subscription');
        }
        $activeMainCourse  = @$maincourse[0];
        $content_availability  = ['subscriber','free'];
        $activecoursevideo   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get(); 

        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();

        return view('frontEnd.user.dashboard.video_lectures',compact('page','maincourse','activecoursevideo','activeMainCourse','subject'));

    }

     public function getVideoLecturesByCourse(Request $request){
        $data  =  $request->all();
        $main_course  =  $data['main_course'];
        //dd($main_course);
        $userId = Auth::id();

        $course_id  =  $data['course_id'];
        $activeMainCourse =  TrainerContent::where('id',$course_id)->first();
    
        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();

        $subject_options = '<option value="">Select Subject</option>';

        $subject_ids  = array();
        if(!empty($subject)){

            foreach ($subject as $key => $value) {
                $subject_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
                 $subject_ids[]  = $value['id'];
            }
        
        }
        $sub_subcategory_id  =  $activeMainCourse['sub_subcategory_id'];
        $activeMainCourseNew =  TrainerContent::where('id',$course_id)->get()->toArray();
        $finalarray  = array_merge($activeMainCourseNew,$main_course);
        
        $maincourse  =  array_unique($finalarray,SORT_REGULAR);

        $render_view  =   view('frontEnd.user.dashboard.courseRenderVideo',compact('maincourse','course_id'))->render();


        /**** realted Viedo code start here  *****/

        $content_availability  = ['subscriber','free'];
       
        $activecoursevideo   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','video');
            }])->with('course_image')->get();

      
        $related_video_render_view  =   view('frontEnd.user.dashboard.all_video_render',compact('activecoursevideo'))->render();

        $video_serach_replace  = view('frontEnd.user.dashboard.all_video_serach_replace',compact('activeMainCourse','subject'))->render();


       //dd($pdf_serach_replace );
        return response()->json(['status'=>'true','subject_options'=>$subject_options,'sub_subcategory_id'=>$sub_subcategory_id,'render_view'=>$render_view,'related_video_render_view'=>$related_video_render_view,'video_serach_replace'=>$video_serach_replace]);
    }

    public function studyMaterials(Request $request){
        $page = 'Study Materials';
        $userId = Auth::id();
        $transaction =  Transaction::where(['user_id'=>$userId])->where('course_id', '!=' , 0)->get();
        $course_id  = array();
        foreach($transaction as $transactionval){
            $course_id[]  =  $transactionval['course_id'];
        }
        $maincourse  =  TrainerContent::whereIn('id',$course_id)->get();
        if(count($maincourse) <=0 ) {
            return redirect('/student/dashboard')->with('success','You do not have any subscription');
        }
        $activeMainCourse  = @$maincourse[0];
        $content_availability  = ['subscriber','free'];
        

        $activecoursepdf   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get()->toArray();

        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();
        $active_id  = $activeMainCourse['id'];

        return view('frontEnd.user.dashboard.study_materials',compact('page','maincourse','activecoursepdf','activeMainCourse','subject','active_id'));
    }

    public function getStudyMaterialsByCourse(Request $request){
        $data  =  $request->all();
        $main_course  =  $data['main_course'];
        //dd($main_course);
        $userId = Auth::id();

        $course_id  =  $data['course_id'];
        $activeMainCourse =  TrainerContent::where('id',$course_id)->first();
    
        $subject = Subject::where(['deleted_at'=>null,'category_id'=>@$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_sub_category_id'=> $activeMainCourse['sub_subcategory_id']])->get();

        $subject_options = '<option value="">Select Subject</option>';

        $subject_ids  = array();
        if(!empty($subject)){

            foreach ($subject as $key => $value) {
                $subject_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
                 $subject_ids[]  = $value['id'];
            }
        
        }
        $sub_subcategory_id  =  $activeMainCourse['sub_subcategory_id'];
        $activeMainCourseNew =  TrainerContent::where('id',$course_id)->get()->toArray();
        $finalarray  = array_merge($activeMainCourseNew,$main_course);
        
        $maincourse  =  array_unique($finalarray,SORT_REGULAR);

        $render_view  =   view('frontEnd.user.dashboard.courseRenderVideo',compact('maincourse','course_id'))->render();


        /**** realted Viedo code start here  *****/

        $content_availability  = ['subscriber','free'];
       
        /**** realted Notes code start here  *****/

        $activecoursepdf   =  TrainerContent::where(['category_id'=>$activeMainCourse['category_id'],'sub_category_id'=>$activeMainCourse['sub_category_id'],'sub_subcategory_id'=> $activeMainCourse['sub_subcategory_id']])->whereIn('content_availability',$content_availability)->with(['course_files'=>function ($query) {
                    $query->where('type','pdf');
            }])->with('course_image')->get();

        $active_id  = $activeMainCourse['id']; 
        $related_pdf_render_view  =   view('frontEnd.user.dashboard.all_related_pdf_render',compact('activecoursepdf','active_id'))->render();

        $pdf_serach_replace  = view('frontEnd.user.dashboard.all_pdf_serach_replace',compact('activeMainCourse','subject','active_id'))->render();

       //dd($pdf_serach_replace );
        return response()->json(['status'=>'true','subject_options'=>$subject_options,'sub_subcategory_id'=>$sub_subcategory_id,'render_view'=>$render_view,'related_pdf_render_view'=>$related_pdf_render_view,'pdf_serach_replace'=>$pdf_serach_replace]);
    }


    public function  expertOpinions(){
        $page  =  "Expert Opinions";
        return view('frontEnd.user.dashboard.expert_opinions',compact('page'));
    }

    public function grievanceCell(){
        $page  =  "Grievance Cell";
        return view('frontEnd.user.dashboard.grievance_cell',compact('page'));
    }

    public function mySubscriptions(){
        $page  =  "My Subscriptions";
        return view('frontEnd.user.dashboard.my_subscriptions',compact('page'));
    }

    public function submitFeedback(){
        $page  =  "Student Feedback";
        return view('frontEnd.user.dashboard.student_feedback',compact('page'));
    }

    public function privacyPolicy(){
        $page  =  "Privacy Policy";
        return view('frontEnd.user.dashboard.privacy_policy',compact('page'));
    }

    public function trainerContentViews($trainer_content_file_id){
        $userId = Auth::id();
        $alreadyexist  = TrainerContentViews::where(['user_id'=>$userId,'trainer_content_file_id'=>$trainer_content_file_id])->first();
        if(empty($alreadyexist)){
            $content  = new TrainerContentViews;
            $content->trainer_content_file_id = $trainer_content_file_id;
            $content->user_id = $userId;
            $content->save();
        }   
        $total_view  = TrainerContentViews::where(['trainer_content_file_id'=>$trainer_content_file_id])->count();
        return response()->json(['status'=>'true','total_view'=>$total_view]);
    }

}
