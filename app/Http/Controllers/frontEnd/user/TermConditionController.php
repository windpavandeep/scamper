<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class TermConditionController extends Controller
{
    public function index() {
    	$page = 'Term Condition';

        $userId = Auth::id();
        
    	return view('frontEnd.user.termCondition.index',compact('page'));
    }
}
