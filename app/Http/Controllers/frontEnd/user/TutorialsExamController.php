<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class TutorialsExamController extends Controller
{
    public function index() {
    	$page = 'Tutorials';

        $userId = Auth::id();
        
    	return view('frontEnd.user.tutorialsExam.index',compact('page'));
    }
}
