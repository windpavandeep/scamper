<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User,App\TrainerDomain,App\Country,App\City,App\State,App\Category,App\SubCategory,App\UserCourse;
class ProfileController extends Controller
{
    public function index(){

    	$student_id      = Auth::User()->id;
    	$student_details = User::select('users.*','c.name as country_name','s.name as state_name','city.name as city_name')
    							->join('countries as c','c.id','users.country_id')
    							->join('states as s','s.id','users.state_id')
    							->join('cities as city','city.id','users.city_id')
                                ->with('user_courses')
    							->where([
    						 		'user_type'        =>'user',
    						 		'users.id'         =>$student_id,
                                    'users.deleted_at' =>null
    						 	])    	  
    	                        ->first();

        $domains   = Category::select('*')
                               ->whereNull('deleted_at')
                               ->orderBy('name','asc')
                               ->get()
                               ->toArray();   

        $sub_categories = '';
        if(!empty(@$student_details['user_courses'])){
            $sub_categories = SubCategory::where('category_id',$student_details['user_courses']['category_id'])
                                          ->whereNull('deleted_at')
                                          ->orderBy('name','asc')
                                          ->get()
                                          ->toArray();   
        }  
    	                        // echo'<pre>'; print_r($student_details->toArray()); 
    	$page = 'profile';
    	return view('frontEnd.user.profile.index',compact('page','student_details','sub_categories','domains'));
    }

    public function edit(Request $request){

    	$student_id      = Auth::User()->id;
    	$student_details = User::with('user_courses')
                                ->where([
    						 		'user_type' =>'user',
    						 		'id'        =>$student_id,
    						 		'deleted_at'=>null,
    						 	])        	  
    	                        ->first();

        if($request->isMethod('post')){
    
            $data                                   = $request->input();
            $student_details->first_name            = $data['first_name'];
            $student_details->last_name             = $data['last_name'];
            $student_details->dob                   = date('Y-m-d',strtotime($data['dob']));
            $student_details->gender                = $data['gender'];
            $student_details->pincode               = $data['pincode'];
            $student_details->country_id            = $data['country_id'];
            $student_details->state_id              = $data['state_id'];
            $student_details->city_id               = $data['city_id'];
            $student_details->exam_preparation_id   = $data['sub_category_id'];
            $student_details->district              = $data['district'];
            $student_details->address               = $data['address'];

            if(!empty($_FILES['image']['name'])){

                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= StudentProfileBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    if (file_exists($destination.'/'.$student_details->image)){
                        unlink($destination.'/'.$student_details->image);
                    }
                    move_uploaded_file($tmp_name, $destination);
                    $student_details->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($student_details->save()){
                $update  = UserCourse::where('user_id',$student_details->id)->update(['category_id'=>$data['category_id'],'sub_category_id'=>$data['sub_category_id']]);
                return redirect()->back()->with('success','Profile updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }  
    	$countries = Country::select('*')->get()->toArray();  
    	$domains   = Category::select('*')
							   ->whereNull('deleted_at')
							   ->orderBy('name','asc')
							   ->get()
							   ->toArray();   

        $states = [];
        $cities = [];        
        if(!empty($student_details->country_id)){

            $states = State::where('country_id',$student_details->country_id)
                            ->get()
                            ->toArray();
            $cities = City::where('state_id',$student_details->state_id)
                           ->get()
                           ->toArray();
        }
        $sub_categories = '';
        if(!empty(@$student_details['user_courses'])){
            $sub_categories = SubCategory::where('category_id',$student_details['user_courses']['category_id'])
                                          ->whereNull('deleted_at')
                                          ->orderBy('name','asc')
                                          ->get()
                                          ->toArray();   
        }             
        // dd($student_details['user_courses']['sub_category_id']);
    	                        // echo'<pre>'; print_r($student_details->toArray());die; 
    	$page = 'profile';
    	return view('frontEnd.user.profile.form',compact('page','student_details','states','cities','domains','countries','sub_categories'));
    }

    public function change_password(Request $request){

        if($request->isMethod('post')){
            
            $data = $request->all();
            
            $pw = User::change_password($data);

            if($pw=='pass_not_match'){
                return redirect()->back()->with('error',"New password and confirm password doesn't matched");
            }else{
                if($pw=='true'){
                    return redirect()->back()->with('success','Password changed successfully'); 
                }elseif($pw=='false'){
                    return redirect()->back()->with('error',COMMON_ERROR);   
                }else{
                    return redirect()->back()->with('error','Invalid current password');
                }
            }
        }
        
        $page = 'change_password';
        return view('frontEnd.user.profile.change_password',compact('page'));    
    }
}
