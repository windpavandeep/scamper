<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class DownloadsController extends Controller
{
    public function index() {
    	$page = 'Download';
    	return view('frontEnd.user.download.index',compact('page'));
    }
}
