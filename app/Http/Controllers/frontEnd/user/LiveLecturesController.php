<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class LiveLecturesController extends Controller
{
    public function index() {
    	$page = 'LiveLectures';

        $userId = Auth::id();
        
    	return view('frontEnd.user.liveLectures.index',compact('page'));
    }
}
