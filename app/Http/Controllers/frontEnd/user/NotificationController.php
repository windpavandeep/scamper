<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class NotificationController extends Controller
{
    public function index() {
    	$page = 'Notification';

        $userId = Auth::id();
        
    	return view('frontEnd.user.notification.index',compact('page'));
    }
}
