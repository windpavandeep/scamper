<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class TrainersController extends Controller
{
    public function index() {
    	$page = 'Trainers';

        $userId = Auth::id();
        
    	return view('frontEnd.user.trainers.index',compact('page'));
    }
}
