<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class NotesController extends Controller
{
    public function index() {
    	$page = 'Notes';
    	return view('frontEnd.user.notes.index',compact('page'));
    }
    
}
