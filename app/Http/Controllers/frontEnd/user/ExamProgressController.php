<?php

namespace App\Http\Controllers\frontEnd\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\User;
class ExamProgressController extends Controller
{
    public function index() {
    	$page = 'Exams';

        $userId = Auth::id();
        
    	return view('frontEnd.user.examprogress.index',compact('page'));
    }
}
