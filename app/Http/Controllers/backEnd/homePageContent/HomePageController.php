<?php

namespace App\Http\Controllers\backEnd\homePageContent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\HomePageFirstSection, App\HomePageSecondSection, App\HomePageThirdSection;
class HomePageController extends Controller
{
    public function index(Request $request){

        $content_details = HomePageFirstSection::first();
        if($request->isMethod('post')){
           
            $content_details->first_title   = $request->first_title;
            $content_details->second_title  = $request->second_title;
            $content_details->third_title   = $request->third_title;
            $content_details->contact_no    = $request->contact_no;
            if($content_details->save()){
                return redirect('admin/home/page/first/section')->with('success','Content updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }

        }
    	
    	$page = 'first_section';
    	return view('backEnd.homePageContent.firstSection.form',compact('page','content_details'));
    }

    public function second_section(){

        $second_section_content = HomePageSecondSection::orderBy('id','Desc')->get()->toArray();

        $page = 'second_section';
        return view('backEnd.homePageContent.secondSection.index',compact('page','second_section_content'));
    }
    public function second_section_edit(Request $request,$content_id = null){

        $second_section_content = HomePageSecondSection::where('id',$content_id)->first();
        // echo "<pre>"; print_r($second_section_content); die;

        if($request->isMethod('post')){
            // echo "<pre>"; print_r($request->input()); die;
            $second_section_content->title          = $request->title;
            $second_section_content->description    = $request->description;

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= HomeContentImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    move_uploaded_file($tmp_name, $destination);
                    $second_section_content->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($second_section_content->save()){
                
                return redirect()->back()->with('success','Content updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }

        $page = 'second_section';
        return view('backEnd.homePageContent.secondSection.form',compact('page','second_section_content','content_id'));
    }

     public function third_section(){

        $third_section_content = HomePageThirdSection::get()->toArray();

        $page = 'third_section';
        return view('backEnd.homePageContent.thirdSection.index',compact('page','third_section_content'));
    }
    public function third_section_edit(Request $request,$content_id = null){

        $third_section_content = HomePageThirdSection::where('id',$content_id)->first();
        // echo "<pre>"; print_r($second_section_content); die;

        if($request->isMethod('post')){
            // echo "<pre>"; print_r($request->input()); die;
            $third_section_content->title          = $request->title;
            $third_section_content->description    = $request->description;

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= HomeContentImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'|| $ext == 'svg'){
                    move_uploaded_file($tmp_name, $destination);
                    $third_section_content->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if(!empty($_FILES['video']['name'])){
                $image      = pathinfo($_FILES['video']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['video']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= HomeContentImageBasePath.'/'.$new_name;

                if($ext == 'mp4' || $ext == 'mov' || $ext == 'avi'){
                    move_uploaded_file($tmp_name, $destination);
                    $third_section_content->video = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid video extension');
                }
            }

            if($third_section_content->save()){
                
                return redirect()->back()->with('success','Content updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }

        $page = 'third_section';
        return view('backEnd.homePageContent.thirdSection.form',compact('page','third_section_content','content_id'));
    }
}
