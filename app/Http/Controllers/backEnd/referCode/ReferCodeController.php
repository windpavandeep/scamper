<?php

namespace App\Http\Controllers\backEnd\referCode;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ReferralCode;
class ReferCodeController extends Controller
{
	public function index(){

		$detail = ReferralCode::whereNull('deleted_at')
							   ->get()
							   ->toArray();

		$page = 'referrals';
		return view('backEnd.referCode.index',compact('page','detail'));
	}

	public function add(Request $request){

		if($request->isMethod('post')){

			$data 					     = $request->input();
			$referrals     			     = new ReferralCode;
			$referrals->coordinator_id   = $data['referral_id'];
			$referrals->pincode 	     = $data['pincode'];
			$referrals->coordinator_name = $data['coordinator_name'];
			if($referrals->save()){
				return redirect('admin/referrals')->with('success','Referral added sucessfully');
			}else{
				return redirec()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'referrals';	
		return view('backEnd.referCode.form',compact('page'));
	}

	public function edit(Request $request,$referral_id){

		$detail = ReferralCode::where('id',$referral_id)
							   ->first();
		if($request->isMethod('post')){

			$data 						= $request->input();
			$detail->coordinator_id 	= $data['referral_id'];
			$detail->pincode 	    	= $data['pincode'];
			$detail->coordinator_name   = $data['coordinator_name'];
			if($detail->save()){
				return redirect()->back()->with('success','Referral edited sucessfully');
			}else{
				return redirec()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'referrals';	
		return view('backEnd.referCode.form',compact('page','referral_id','detail'));
	}

	public function delete($referral_id){

	    $del = ReferralCode::where('id',$referral_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
	    if ($del) {
	        return redirect()->back()->with('success','Referral deleted successfully');
	    }else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}
	
	public function validate_refer_id(Request $request){

		$data = $request->all();
    
        if($data['coordinator_id']==null){
            $check_id = ReferralCode::where('coordinator_id',$data['referral_id'])
                            		->whereNull('deleted_at')
                            		->count(); 
        }else{
            $check_id = ReferralCode::where('coordinator_id',$data['referral_id'])
			                        ->where('id','<>',$data['coordinator_id'])
			                        ->whereNull('deleted_at')
			                        ->count();
        }
        if ($check_id > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

	public function validate_pincode(Request $request){

		$data = $request->all();
    
        if($data['coordinator_id']==null){
            $check_id = ReferralCode::where('pincode',$data['pincode'])
                            		->whereNull('deleted_at')
                            		->count(); 
        }else{
            $check_id = ReferralCode::where('pincode',$data['pincode'])
			                        ->where('id','<>',$data['coordinator_id'])
			                        ->whereNull('deleted_at')
			                        ->count();
        }
        if ($check_id > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

	public function import_referrals(Request $request){
		// dd($request->all());
		if($request->isMethod('post')){
			// dd($_FILES);
			// dd($request->file('csv_file')->getRealPath());
			if ($request->hasFile('csv_file')) {
		       
		        $path = $request->file('csv_file')->getRealPath();
		        $data = \Excel::load($path)->get()->toArray();
		        // echo "<pre>"; print_r($data); die;
		       	foreach ($data as $key => $value) {
		       		if((is_string($value['pincode']))){
		       			return redirect()->back()->with('error','Pincode consists of digits only');
		       		}
		       	}
	            foreach ($data as $key => $value) {
		            $referral 						= new ReferralCode;
		            $referral->coordinator_id 	    = $value['coordinator_id'];
		            $referral->pincode 				= $value['pincode'];
		            $referral->coordinator_name     = $value['coordinator_name'];
		            $referral->save();
		        }
		        return redirect('admin/referrals')->with('success','Referrals added successfully.');
		    }else{
		        return redirect()->back()->with('error',COMMON_ERROR);	
			}
			
		}

		$page = 'referrals';	
		return view('backEnd.referCode.import_referrals',compact('page'));
	}
}
