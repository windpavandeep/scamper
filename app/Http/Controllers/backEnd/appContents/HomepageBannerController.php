<?php

namespace App\Http\Controllers\backEnd\appContents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BannerImage,App\Category,App\TrainerContent,App\SubCategory,App\SubSubCategory;
use App\User;
class HomepageBannerController extends Controller
{
	public function index(Request $request){

	    $details = BannerImage::select('banner_images.*','tc.title')
	    					   ->join('trainer_contents as tc','tc.id','banner_images.course_id')
	    					   ->whereNull('deleted_at')
	    					   ->get()
	                           ->toArray();
	    $page = 'homepage_banners';
	    return view('backEnd.appContents.bannerContents.index', compact('page','details'));
	}
	public function add(Request $request) {

	    if($request->isMethod('post')){

	        $data 			     		 = $request->all();
	        $banner 		     		 = new BannerImage;
	        $banner->category_id 		 = $data['category_id'];
	        $banner->sub_category_id     = $data['sub_category_id'];
	        $banner->sub_sub_category_id = $data['sub_sub_category_id'];
	        $banner->course_id 			 = $data['course_id'];

	        if(!empty($_FILES['image']['name'])){
	            // echo '<pre>'; print_r($_FILES);die;

	            $info = pathinfo($_FILES['image']['name']);
	           
	            $extension = $info['extension'];
	            
	            $random = rand(0000000,9999999);
	            // $random = mt_rand(0000000,9999999);

	            // echo "<pre>";print_r($random);die;
	            $new_name = $random.'.'.$extension;

	            if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png'){
	                $file_path = base_path().'/'.HomepageBannerBasePath;
	                
	                move_uploaded_file($_FILES['image']['tmp_name'], $file_path.'/'.$new_name);

	                $banner->image = $new_name;
	                // echo "<pre>";print_r($new_name);die;

	                
	            }else{
	            	return redirect()->back()->with('error','Invalid image extension.');
	            }
	        }
	        // echo "<pre>";print_r($new_name);die;
	        if($banner->save()){
        		User::desktop_notification($banner->id);
                return redirect('admin/homepage-banners')->with('success','Homepage banner added successfully');
            } else{
                return redirect()->back()->with('error',COMMON_ERROR);
	        }

	        
	    }
	    $categories = Category::whereNull('deleted_at')
    					    	->get()
    					    	->toArray();
   		$image = DefaultImgPath;
	    $page = 'banner_images';
	    return view('backEnd.appContents.bannerContents.form',compact('page','categories','image'));
	}

	public function edit(Request $request, $banner_image_id) {

	    $banner_images = BannerImage::where('id',$banner_image_id)
	                                ->first();
	    if($request->isMethod('post')){
	        $data = $request->all();
	        // echo '<pre>'; print_r($banner_image_id); die;

	        $banner_images->category_id 		= $data['category_id'];
	        $banner_images->sub_category_id     = $data['sub_category_id'];
	        $banner_images->sub_sub_category_id = $data['sub_sub_category_id'];
	        $banner_images->course_id 			= $data['course_id'];
	        
	        if(!empty($_FILES['image']['name'])){
	            // echo '<pre>'; print_r($_FILES);

	            $info = pathinfo($_FILES['image']['name']);
	            $extension = $info['extension'];
	            $random = rand(0000000,9999999);
	            $new_name = $random.'.'.$extension;

	            if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png'){
	                $file_path = base_path().'/'.HomepageBannerBasePath;
	                if(!empty($banner_images->image)){
	                    unlink($file_path.'/'.$banner_images->image);
	                }
	                move_uploaded_file($_FILES['image']['tmp_name'], $file_path.'/'.$new_name);

	                $banner_images->image = $new_name;
	            }
	        }

	        if($banner_images->save()){
	            return redirect()->back()->with('success','Homepage banner edited successfully');
	        } else{
	            return redirect()->back()->with('error',COMMON_ERROR);
	        }
	    }
	    
	    if (!empty($banner_images->image)) {

	        $path = base_path().'/'.HomepageBannerBasePath.'/'.$banner_images->image;
	        if (file_exists($path)) {
	            $image = HomepageBannerImgPath.'/'.$banner_images->image;
	            
	        }else{
	            $image = DefaultImgPath;    
	        }
	    }else{
	        $image = DefaultImgPath;
	    }
	    $page = 'banner_images';
	    $sub_categories = '';
	    $sub_sub_categories = '';
	    if(!empty(@$banner_images->category_id)){
	    	$sub_categories = SubCategory::where('category_id',$banner_images->category_id)
	    								  ->whereNull('deleted_at')
	    								  ->get()
	    								  ->toArray();
	    }
	    $products = '';
	    if(!empty(@$banner_images->sub_category_id)){
	    	$sub_sub_categories = SubSubCategory::where('sub_category_id',$banner_images->sub_category_id)
			    								 ->whereNull('deleted_at')
			    								 ->get()
			    								 ->toArray();
			$products = TrainerContent::where('sub_subcategory_id',$banner_images->sub_sub_category_id)
										->whereNull('deleted_at')
										->get()
										->toArray();
	    }
	    $categories = Category::whereNull('deleted_at')
    					    	->get()
    					    	->toArray();
    			// echo '<pre>'; print_r($sub_sub_categories);die;		    	
	    return view('backEnd.appContents.bannerContents.form',compact('banner_images','page','banner_image_id','image','sub_categories','sub_sub_categories','categories','products'));
	}
	public function delete($banner_id) {

	    $banner_image = BannerImage::where('id',$banner_id)
	                                ->first();

	    $file_path = base_path().'/'.HomepageBannerBasePath;
	    // echo "<pre>";print_r($file_path);die;
	    if (!empty($banner_image->image)) {
	        unlink($file_path.'/'.$banner_image->image);
	    }
	    if ($banner_image->delete()) {
	        return redirect()->back()->with('success','Banner image deleted successfully');
	    } else {
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}

	public function course_products(Request $request){

		$data = $request->input();
		$products = TrainerContent::where('sub_subcategory_id',$data['sc'])
									->whereNull('deleted_at')
									->get()
									->toArray();
		$options =  '<option data-display="Choose Product" value="">Choose Product</option>';
		if(!empty($products)){
			foreach ($products as $key => $value) {
				$options .=  "<option value=".$value['id'].">".ucfirst($value['title'])." </option> ";
			}
		}

		return $options;
	}
	
}
