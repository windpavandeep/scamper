<?php

namespace App\Http\Controllers\backEnd\appContents;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WalkinContent;

class WalkthroughController extends Controller
{
    public function index(Request $request){

        $details = WalkinContent::get()
                                 ->toArray();
        $page = 'walkthroughs';
        return view('backEnd.appContents.walkthrough.index', compact('page','details'));
    }
    /*public function add(Request $request) {

        if($request->isMethod('post')){

            $data = $request->all();
            // echo '<pre>'; print_r($data); die;
            $walkthrough = new Walkthrough;
            $walkthrough->title  = $data['title'];

            if(!empty($_FILES['image']['name'])){
                // echo '<pre>'; print_r($_FILES);die;

                $info = pathinfo($_FILES['image']['name']);
               
                $extension = $info['extension'];
                
                $random = rand(0000000,9999999);
                // $random = mt_rand(0000000,9999999);

                // echo "<pre>";print_r($random);die;
                $new_name = $random.'.'.$extension;

                if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png'){
                    $file_path = base_path().'/'.WalkthroughBasePath;
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], $file_path.'/'.$new_name);

                    $walkthrough->image = $new_name;
                    // echo "<pre>";print_r($new_name);die;

                    
                }
            // echo "<pre>";print_r($new_name);die;
            if($walkthrough->save()){
                    return redirect('admin/walkthroughs')->with('success','Walkthrough added successfully');
                } else{
                    return redirect('admin/walkthroughs')->with('error',COMMON_ERROR);
                }
            }
            
        }

        $page = 'walkthroughs';
        return view('backEnd.contentManagement.walkthrough.form',compact('page'));
    }*/

    public function edit(Request $request, $walkthrough_id) {

        $walkthrough = WalkinContent::where('id',$walkthrough_id)
                                ->first();
        if($request->isMethod('post')){
            $data = $request->all();
            // echo '<pre>'; print_r($data); die;

            $walkthrough->title  = $data['title'];
            
            if(!empty($_FILES['image']['name'])){
                // echo '<pre>'; print_r($_FILES);

                $info = pathinfo($_FILES['image']['name']);
                $extension = $info['extension'];
                $random = rand(0000000,9999999);
                $new_name = $random.'.'.$extension;

                if($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png'){
                    $file_path = base_path().'/'.WalkthroughBasePath;
                    if(!empty($walkthrough->image)){
                        unlink($file_path.'/'.$walkthrough->image);
                    }
                    move_uploaded_file($_FILES['image']['tmp_name'], $file_path.'/'.$new_name);

                    $walkthrough->image = $new_name;
                }
            }

            if($walkthrough->save()){
                return redirect()->back()->with('success','Walkthrough edited successfully');
            } else{
                return redirect('admin/walkthroughs')->with('error',COMMON_ERROR);
            }
        }
        
        if (!empty($walkthrough->image)) {

            $path = base_path().'/'.WalkthroughBasePath.'/'.$walkthrough->image;
            if (file_exists($path)) {
                $image = WalkthroughImgPath.'/'.$walkthrough->image;
                
            }else{
                $image = DefaultImgPath;    
            }
        }else{
            $image = DefaultImgPath;
        }
        $page = 'walkthroughs';

        return view('backEnd.appContents.walkthrough.form',compact('walkthrough','page','walkthrough_id','image'));
    }
    /*public function delete($walkthrough_id) {

        $walkthrough = Walkthrough::where('id',$walkthrough_id)
                                ->first();

        $file_path = base_path().'/'.WalkthroughBasePath;
        // echo "<pre>";print_r($file_path);die;
        if (!empty($walkthrough->image)) {
            unlink($file_path.'/'.$walkthrough->image);
        }
        if ($walkthrough->delete()) {
            return redirect()->back()->with('success','Walkthrough deleted successfully');
        } else {
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }*/
	
}