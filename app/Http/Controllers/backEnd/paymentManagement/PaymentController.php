<?php

namespace App\Http\Controllers\backEnd\paymentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction,App\User,App\Trainer,App\State,App\Category,Excel,App\TrainerContent,App\SubscriptionPlan;
use PDF,App,App\ReferralCode;
class PaymentController extends Controller
{
	public function index(){

		// $payments = Transaction::select('transactions.*','tc.title','tc.trainer_id')
		// 						->join('trainer_contents as tc','tc.id','transactions.course_id')
		// 						->where('trainer_id','<>','0')
		// 						->where('paid_status','<>','Y')
		// 						->orderBy('transactions.id','Desc')
		// 						->get()
		// 						->toArray();

		$admin_content_ids = TrainerContent::where('trainer_id','0')->pluck('id')->toArray();

		$payments = Transaction::select('transactions.*','tc.title','sp.title as subscription_plan_tile','tc.trainer_id as trainer_content_id')
								->leftJoin('trainer_contents as tc','tc.id','transactions.course_id')
								->leftJoin('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
								->whereNotIn('course_id',$admin_content_ids)
								->where('paid_status','<>','Y')
								->orderBy('transactions.id','desc')
								->get()
								->toArray();

		// echo'<pre>'; print_r($payments);die;
		$page = 'pending_payment';
		return view('backEnd.paymentManagement.index',compact('page','payments'));
	}

	public function payment_details($transaction_id){

		$payments = Transaction::select('transactions.*','u.*')
								->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','trainer_contents.subject','trainer_contents.trainer_detail','subscription_plan.trainer_detail')
								->join('users as u','u.id','transactions.user_id')
								->where('transactions.id',$transaction_id)
								->first();
		// echo'<pre>'; print_r($payments->toArray());die;
		$page = 'pending_payment';
		return view('backEnd.paymentManagement.form',compact('page','payments'));
	}

	public function pay_pending_payment(Request $request){

		if($request->isMethod('post')){


			$data = $request->input();
			/*if(!empty($data['transaction_id'])){
				echo'enter';
			}else{
				echo'not enter';
			}*/
			// die;
			$date = date('Y-m-d');
			// echo'<pre >'; print_r($data);
			// echo'<pre >'; print_r($data['transaction_id']);
			 // die;
			if(!empty($data['transaction_id'])){

		        foreach ($data['transaction_id'] as $key => $value) {
	
					$provider_payment = Transaction::where('id',$value)
													->where('paid_status','N')
													->update(['paid_status'=>'Y','paid_on'=>$date]);
				}
				return redirect()->back()->with('success','Payment paid successfully');
			}else{
				return redirect()->back()->with('error','Please select any checkbox to pay'); 
			}
		}
	}


	public function paid_payment(){

		// $payments = Transaction::select('transactions.*','tc.title','tc.trainer_id')
		// 						->with('trainer_contents.trainer_detail')
		// 						->join('trainer_contents as tc','tc.id','transactions.course_id')
		// 						->where('trainer_id','<>','0')
		// 						->where('paid_status','Y')
		// 						->orderBy('transactions.id','Desc')
		// 						->get()
		// 						->toArray();
		$admin_content_ids = TrainerContent::where('trainer_id','0')->pluck('id')->toArray();

		$payments = Transaction::select('transactions.*','tc.title','sp.title as subscription_plan_tile','tc.trainer_id as trainer_content_id')
								->leftJoin('trainer_contents as tc','tc.id','transactions.course_id')
								->leftJoin('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
								->with('trainer_contents.trainer_detail','subscription_plan.trainer_detail')
								->whereNotIn('course_id',$admin_content_ids)
								->where('paid_status','Y')
								->orderBy('transactions.id','desc')
								->get()
								->toArray();

		// echo'<pre>'; print_r($payments);die;
		$page = 'paid';
		return view('backEnd.paymentManagement.paid.index',compact('page','payments'));
	}

	public function admin_earnings(){

		// $earnings = Transaction::select('transactions.*','tc.title','tc.trainer_id','u.first_name','u.last_name','tc.title')
		// 						c
		// 						->join('trainer_contents as tc','tc.id','transactions.course_id')
		// 						// ->where('trainer_id','0')
		// 						->orderBy('transactions.id','Desc');
		$earnings = Transaction::select('transactions.*','u.first_name','u.last_name')
								->with('trainer_contents','trainer_contents.trainer_detail','subscription_plan.trainer_detail')
								->join('users as u','u.id','transactions.user_id')
								// ->where('tc.trainer_id','<>','0')
								// ->where('paid_status','Y')
								->orderBy('transactions.id','Desc');
		$total_earning = $earnings->sum('admin_commission');
		$earnings      = $earnings->get()->toArray();
		$states     = State::select('*')->get()->toArray();
		$categories = category::select('*')->get()->toArray();
		$refferals   = ReferralCode::whereNull('deleted_at')->get()->toArray();
				$page = 'earnings';
		return view('backEnd.paymentManagement.earnings.index',compact('page','earnings','total_earning','states','categories','refferals'));	
	}


	/*public function export_earnings(Request $request){

		if($request->isMethod('post')){
			// dd('enter');
			$extension = '';
			$data  = $request->input();
			$earnings ='';
			$date = date('Y-m-d',strtotime($data['date']));
			if(!empty(@$data['category_id'])&&!empty(@$data['state_id'])&&!empty(@$data['date'])){
				$category_id = $data['category_id'];
				$state_id = $data['state_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('trainer_contents.category',function($query)use($category_id){
									    	$query->where('id',$category_id);
									    })
									    ->whereHas('student_details.state',function($query)use($state_id){
									    	$query->where('id',$state_id);
									    })
									    ->whereDate('purchased_on',$date)
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			}
			if(!empty(@$data['category_id'])&&!empty(@$data['state_id'])){
				$category_id = $data['category_id'];
				$state_id = $data['state_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('trainer_contents.category',function($query)use($category_id){
									    	$query->where('id',$category_id);
									    })
									    ->whereHas('student_details.state',function($query)use($state_id){
									    	$query->where('id',$state_id);
									    })
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			
			}
			if(!empty(@$data['category_id'])&&!empty(@$data['date'])){
				$category_id = $data['category_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('trainer_contents.category',function($query)use($category_id){
									    	$query->where('id',$category_id);
									    })
									    ->whereDate('purchased_on',$date)
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			}
			if(!empty(@$data['state_id'])&&!empty(@$data['date'])){
	
				$state_id = $data['state_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('student_details.state',function($query)use($state_id){
									    	$query->where('id',$state_id);
									    })
									    ->whereDate('purchased_on',$date)
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			}
			if(!empty(@$data['category_id'])){
				$category_id = $data['category_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('trainer_contents.category',function($query)use($category_id){
									    	$query->where('id',$category_id);
									    })
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();

										
			}
			if(!empty(@$data['state_id'])){
			
				$state_id = $data['state_id'];
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereHas('student_details.state',function($query)use($state_id){
									    	$query->where('id',$state_id);
									    })
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			}
			
			if(!empty($data['date'])){
			
				$earnings = Transaction::select('transactions.*')
									    ->with('trainer_contents.category','student_details','student_details.state')
									    ->whereDate('purchased_on',$date)
										->orderBy('transactions.id','Desc')
										->get()
										->toArray();
										
			}
			// echo'<pre>'; print_r($earnings); die;
			if(!empty($earnings)){


				$earning_detail = [];
		        $earning_detail[] = ['Transaction Id','Student Name','State','Category Name','Course Name','Recived On','Recieved Amount','Admin Amount'];

		        $detail = [];
		        foreach ($earnings as $key => $value){
		        	$detail['razor_pay_id']    = $value['razor_pay_id'];
		            $detail['student_name']    = ucfirst($value['student_details']['first_name']).' '.ucfirst($value['student_details']['last_name']);

		            $detail['state_name']      = ucfirst($value['student_details']['state']['name']);
		            $detail['category_name']   = ucfirst($value['trainer_contents']['category']['name']);
		            $detail['course_name']     = ucfirst($value['trainer_contents']['title']);
		            $detail['recived_on']      = $value['purchased_on'];
		            $detail['price']           = $value['price'];
		            $detail['admin_commission']= $value['admin_commission'];
		            $earning_detail[] = $detail;
		        }

		        if(!empty(@$data['export_type'])){
					if($data['export_type']=='excel'){
		            	$extension = 'xls';
			        }elseif($data['export_type']=='csv'){
			            
			        	$extension = 'csv';
			        }else{
			        	$extension = 'pdf';
			        }
			    }
			    if($extension=='csv' || $extension=='xls'){

			        Excel::create('Payment-List',function($excel)use ($earning_detail){
			            $excel->setTitle('Payment-List');
			            $excel->setCreator('Laravel')->setCompany('Scamper Skills');
			            $excel->setDescription('Payment File');
			            $excel->sheet('sheet1',function($sheet) use ($earning_detail){
			            $sheet->fromArray($earning_detail, null, 'A1', false, false);
			            });
			        })->download($extension);    
			    }else{
		    	        $html =<<<EOD
		    	                    <html>
		    	                        <head>
		    	                            <meta charset="utf-8">
		    	                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    	                            <meta name="viewport" content="width=device-width, initial-scale=1">
		    	                            <title>Flags</title>
		    	                            <style>
		    	                                .th{
		    	                                    font-weight:bold;
		    	                                    background-color:#dfdfdf;    
		    	                                }
		    	                                img{
		    	                                    width:100px;
		    	                                }
		    	                            </style>
		    	                        </head>
		    	                        <body style="font-family:helvetica; background:#F0F0F0; margin:0; padding:20px;"> 
		    	                            <table style="width:100%; background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
		    	                                    <tr>
		    	                                        <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
		    	                                            <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skills"/>
		    	                                        </th>
		    	                                    </tr>
		    	                            </table>    
		    	                              
		    	                            <table border="1" style="width:100%;font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
		    	                                <thead style="background:#dfdfdf;">
		    	                                    <tr>
		    	                                        <th class="th">Transaction Id</th>
		    	                                        <th class="th">Student Name</th>
		    	                                        <th class="th">State</th>
		    	                                        <th class="th">Category Name</th>
		    	                                        <th class="th">Course Name</th>
		    	                                        <th class="th">Recived On</th>
		    	                                        <th class="th">Recieved Amount</th>
		    	                                        <th class="th">Admin Amount</th>
		    	                                    </tr>
		    	                                </thead>
		    	                                <tbody>
EOD;
												
		    	                                    foreach($earnings as $key=>$earning){
		    	                                        
		    	                                        $earning['student_name']    = ucfirst($earning['student_details']['first_name']).' '.ucfirst($earning['student_details']['last_name']);

		    	                                        $earning['state_name']      = ucfirst($earning['student_details']['state']['name']);

		    	                                        $earning['category_name']   = ucfirst($earning['trainer_contents']['category']['name']);

		    	                                        $earning['course_name']     = ucfirst($earning['trainer_contents']['title']);
		    	                                       
		    	                                        $html .=<<<EOD
		    	                                                    <tr>
		    	                                                        <td>{$earning['razor_pay_id']}</td>
		    	                                                        <td>{$earning['student_name']}</td>
		    	                                                        <td>{$earning['state_name']}</td>
		    	                                                        <td>{$earning['category_name']}</td>
		    	                                                        <td>{$earning['course_name']}</td>
		    	                                                        <td>{$earning['purchased_on']}</td>
		    	                                                        <td>{$earning['price']}</td>
		    	                                                        <td>{$earning['admin_commission']}
		    	                                                        </td>
		    	                                                    </tr>
EOD;
		    	                                    }
		    	                            
			    	    $html .=<<<EOD
			    	                                </tbody>
			    	                            </table>
			    	                        </body>    
			    	                    </html> 
EOD;
			    	            // prx($html);
	    	        $pdf = App::make('dompdf.wrapper');
	    	        $pdf->loadHTML($html);
	    	        return $pdf->stream();
	    	        // return Redirect::back();

			    	    
			    }
		        // echo'<pre>'; print_r($earning_detail);die;
		    }else{
		    	return redirect()->back()->with('error','No data found to export');
		    }                  
		}
    
	}*/
// 	public function export_earnings(Request $request){

// 		if($request->isMethod('post')){
// 			// dd('enter');
// 			$extension = '';
// 			$data  = $request->input();
// 			$earnings ='';
// 			$earnings = Transaction::select('transactions.*','u.first_name','u.last_name')
// 								->with('trainer_contents','trainer_contents.trainer_detail','subscription_plan.trainer_detail')
// 								->join('users as u','u.id','transactions.user_id')
// 								// ->where('tc.trainer_id','<>','0')
// 								// ->where('paid_status','Y')
// 								->orderBy('transactions.id','Desc');

// 			if(!empty($data['date'])){

// 				$date = date('Y-m-d',strtotime($data['date']));
// 				$earnings = $earnings->whereDate('purchased_on',$date);
// 			}
// 			if(!empty($data['state_id'])){
				
// 				$earnings = $earnings->where('state_id',$data['state_id']);
// 			}
// 			if(!empty($data['category_id'])){
				
// 				$earnings = $earnings->where('state_id',$data['state_id']);
// 			}
// 			/*if(!empty())
// 			if(!empty(@$data['category_id'])&&!empty(@$data['state_id'])&&!empty(@$data['date'])){
// 				$category_id = $data['category_id'];
// 				$state_id = $data['state_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('trainer_contents.category',function($query)use($category_id){
// 									    	$query->where('id',$category_id);
// 									    })
// 									    ->whereHas('student_details.state',function($query)use($state_id){
// 									    	$query->where('id',$state_id);
// 									    })
// 									    ->whereDate('purchased_on',$date)
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
// 			}
// 			if(!empty(@$data['category_id'])&&!empty(@$data['state_id'])){
// 				$category_id = $data['category_id'];
// 				$state_id = $data['state_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('trainer_contents.category',function($query)use($category_id){
// 									    	$query->where('id',$category_id);
// 									    })
// 									    ->whereHas('student_details.state',function($query)use($state_id){
// 									    	$query->where('id',$state_id);
// 									    })
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
			
// 			}
// 			if(!empty(@$data['category_id'])&&!empty(@$data['date'])){
// 				$category_id = $data['category_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('trainer_contents.category',function($query)use($category_id){
// 									    	$query->where('id',$category_id);
// 									    })
// 									    ->whereDate('purchased_on',$date)
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
// 			}
// 			if(!empty(@$data['state_id'])&&!empty(@$data['date'])){
	
// 				$state_id = $data['state_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('student_details.state',function($query)use($state_id){
// 									    	$query->where('id',$state_id);
// 									    })
// 									    ->whereDate('purchased_on',$date)
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
// 			}
// 			if(!empty(@$data['category_id'])){
// 				$category_id = $data['category_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('trainer_contents.category',function($query)use($category_id){
// 									    	$query->where('id',$category_id);
// 									    })
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();

										
// 			}
// 			if(!empty(@$data['state_id'])){
			
// 				$state_id = $data['state_id'];
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereHas('student_details.state',function($query)use($state_id){
// 									    	$query->where('id',$state_id);
// 									    })
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
// 			}
			
// 			if(!empty($data['date'])){
			
// 				$earnings = Transaction::select('transactions.*')
// 									    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city')
// 									    ->whereDate('purchased_on',$date)
// 										->orderBy('transactions.id','Desc')
// 										->get()
// 										->toArray();
										
// 			}*/
// 			// echo'<pre>'; print_r($earnings); die;
// 			if(!empty($earnings)){


// 				$earning_detail = [];
// 		        $earning_detail[] = ['Transaction Id','Student Id','Student Name','Student DOB','Student Pincode','Coodinator Id','State','City','Category Name','Sub-Category Name','Sub-Category-2 Name','Course Name','Purchase Date','Discount Amount','Recieved Amount','Admin Amount'];

// 		        $detail = [];
// 		        foreach ($earnings as $key => $value){
// 		        	$detail['razor_pay_id']    = $value['razor_pay_id'];
// 		        	$detail['user_id']         = $value['user_id'];
// 		            $detail['student_name']    = ucfirst($value['student_details']['first_name']).' '.ucfirst($value['student_details']['last_name']);
// 		            $detail['student_dob']     =  date('d-m-Y',strtotime($value['student_details']['dob']));
// 		            $detail['student_pincode'] = $value['student_details']['pincode'];
// 		            $detail['refer_code']      = $value['refer_code'];
// 		            $detail['state_name']      = ucfirst($value['student_details']['state']['name']);
// 		            $detail['city_name']       = ucfirst($value['student_details']['city']['name']);
// 		            $detail['category_name']   = ucfirst($value['trainer_contents']['category']['name']);
// 		            $detail['sub_category']    = ucfirst($value['trainer_contents']['sub_category']['name']);
// 		            $detail['sub_sub_category']= ucfirst($value['trainer_contents']['sub_sub_category']['name']);
// 		            $detail['course_name']     = ucfirst($value['trainer_contents']['title']);
// 		            $detail['recived_on']      = date('d-m-Y',strtotime($value['purchased_on']));
// 		            $detail['discount_amount'] = $value['discount_amount'];
// 		            $detail['final_total']     = $value['final_total'];
// 		            $detail['admin_commission']= $value['admin_commission'];
// 		            $earning_detail[]          = $detail;
// 		        }

// 		        if(!empty(@$data['export_type'])){
// 					if($data['export_type']=='excel'){
// 		            	$extension = 'xls';
// 			        }elseif($data['export_type']=='csv'){
			            
// 			        	$extension = 'csv';
// 			        }else{
// 			        	$extension = 'pdf';
// 			        }
// 			    }
// 			    if($extension=='csv' || $extension=='xls'){

// 			        Excel::create('Payment-List',function($excel)use ($earning_detail){
// 			            $excel->setTitle('Payment-List');
// 			            $excel->setCreator('Laravel')->setCompany('Scamper Skills');
// 			            $excel->setDescription('Payment File');
// 			            $excel->sheet('sheet1',function($sheet) use ($earning_detail){
// 			            $sheet->fromArray($earning_detail, null, 'A1', false, false);
// 			            });
// 			        })->download($extension);    
// 			    }else{
// 		    	        $html =<<<EOD
// 		    	                    <html>
// 		    	                        <head>
// 		    	                            <meta charset="utf-8">
// 		    	                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
// 		    	                            <meta name="viewport" content="width=device-width, initial-scale=1">
// 		    	                            <title>Flags</title>
// 		    	                            <style>
// 		    	                                .th{
// 		    	                                    font-weight:bold;
// 		    	                                    background-color:#dfdfdf;    
// 		    	                                }
// 		    	                                img{
// 		    	                                    width:100px;
// 		    	                                }
// 		    	                            </style>
// 		    	                        </head>
// 		    	                        <body style="font-family:helvetica; background:#F0F0F0; margin:0; padding:20px;"> 
// 		    	                            <table style="width:100%; background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
// 		    	                                    <tr>
// 		    	                                        <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
// 		    	                                            <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skills"/>
// 		    	                                        </th>
// 		    	                                    </tr>
// 		    	                            </table>    
		    	                              
// 		    	                            <table border="1" style="width:100%;font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
// 		    	                                <thead style="background:#dfdfdf;">
// 		    	                                    <tr>
// 		    	                                        <th class="th">Transaction Id</th>
// 		    	                                        <th class="th">Student Name</th>
// 		    	                                        <th class="th">State</th>
// 		    	                                        <th class="th">Category Name</th>
// 		    	                                        <th class="th">Course Name</th>
// 		    	                                        <th class="th">Recived On</th>
// 		    	                                        <th class="th">Recieved Amount</th>
// 		    	                                        <th class="th">Admin Amount</th>
// 		    	                                    </tr>
// 		    	                                </thead>
// 		    	                                <tbody>
// EOD;
												
// 		    	                                    foreach($earnings as $key=>$earning){
		    	                                        
// 		    	                                        $earning['student_name']    = ucfirst($earning['student_details']['first_name']).' '.ucfirst($earning['student_details']['last_name']);

// 		    	                                        $earning['state_name']      = ucfirst($earning['student_details']['state']['name']);

// 		    	                                        $earning['category_name']   = ucfirst($earning['trainer_contents']['category']['name']);

// 		    	                                        $earning['course_name']     = ucfirst($earning['trainer_contents']['title']);
		    	                                       
// 		    	                                        $html .=<<<EOD
// 		    	                                                    <tr>
// 		    	                                                        <td>{$earning['razor_pay_id']}</td>
// 		    	                                                        <td>{$earning['student_name']}</td>
// 		    	                                                        <td>{$earning['state_name']}</td>
// 		    	                                                        <td>{$earning['category_name']}</td>
// 		    	                                                        <td>{$earning['course_name']}</td>
// 		    	                                                        <td>{$earning['purchased_on']}</td>
// 		    	                                                        <td>{$earning['price']}</td>
// 		    	                                                        <td>{$earning['admin_commission']}
// 		    	                                                        </td>
// 		    	                                                    </tr>
// EOD;
// 		    	                                    }
		    	                            
// 			    	    $html .=<<<EOD
// 			    	                                </tbody>
// 			    	                            </table>
// 			    	                        </body>    
// 			    	                    </html> 
// EOD;
// 			    	            // prx($html);
// 	    	        $pdf = App::make('dompdf.wrapper');
// 	    	        $pdf->loadHTML($html);
// 	    	        return $pdf->stream();
// 	    	        // return Redirect::back();

			    	    
// 			    }
// 		        // echo'<pre>'; print_r($earning_detail);die;
// 		    }else{
// 		    	return redirect()->back()->with('error','No data found to export');
// 		    }                  
// 		}
    
// 	}

	public function test(){
		// $subscription_plan_ids = SubscriptionPlan::where('trainer_id','12')->pluck('id')->toArray();
		// $admin_content_ids = TrainerContent::where('trainer_id','12')->pluck('id')->toArray();

		$payments = Transaction::select('transactions.*','tc.trainer_id as trainer_id','sp.trainer_id as trainer_id')
								->leftJoin('trainer_contents as tc','tc.id','transactions.course_id')
								->leftJoin('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
								// ->where('sp.trainer_id','17')
								// ->where('tc.trainer_id','12')
								// ->whereIn('course_id',$admin_content_ids)
								// ->whereIn('subscription_plan_id',$subscription_plan_ids)
								// ->where('sp.trainer_id','12')
								// ->where('tc.trainer_id','12')
								// ->where('paid_status','<>','Y')
								->orderBy('transactions.id','desc')
								->get()
							 	->toArray();	
		// $payments = $payments->where('trainer_id','12')
							 // ->get()
							 // ->toArray();							
		echo'<pre>'; print_r($payments);die;
		/*$payments = Transaction::select('transactions.*','tc.title','tc.trainer_id','sp.*','tc.trainer_id as trainer_content_id')
								->leftJoin('trainer_contents as tc','tc.id','transactions.course_id')
								->leftJoin('subscription_plans as sp','sp.id','transactions.subscription_plan_id')
								// ->where('tc.trainer_id','<>','0')
								->where('paid_status','Y')
								->orderBy('transactions.id','Desc')
								->get()
								->toArray();*/

		// $payments = Transaction::where('paid_status','Y')
		// 						->orderBy('transactions.id','Desc')
		// 						->get()->count();

		// $update = Transaction::whereIn('course_id',$admin_content_ids)->update(['paid_status'=>'Y']);
								// print_r(array_merge($a1,$a2));
	}

	public function export_earnings(Request $request){

		if($request->isMethod('post')){
			// dd('enter');
			$extension = '';
			$data  = $request->input();
			$earnings ='';
			
			$earnings = Transaction::select('transactions.*')
								    ->with('trainer_contents.category','trainer_contents.sub_category','trainer_contents.sub_sub_category','student_details','student_details.state','student_details.city','coordinator','coordinator_details')
									->orderBy('transactions.id','Desc');
								    // ->whereDate('purchased_on',$date)
									/*->get()
									->toArray();*/
			if(!empty(@$data['category_id'])){
				$category_id = $data['category_id'];
				$earnings = $earnings->whereHas('trainer_contents.category',function($query)use($category_id){
									    	$query->where('id',$category_id);
									    });
			}
			if(!empty(@$data['state_id'])){
				$state_id = $data['state_id'];
				$earnings = $earnings->whereHas('student_details.state',function($query)use($state_id){
									    	$query->where('id',$state_id);
									    });
			}
			if(!empty(@$data['pincode'])){
				$pincode = $data['pincode'];
				$earnings = $earnings->where('refer_code',$data['pincode']);
			}
			if(!empty(@$data['date'])){
				$date = date('Y-m-d',strtotime($data['date']));
				$earnings = $earnings->whereDate('purchased_on',$date);
			}
			if(!empty(@$data['district'])){
				$district = $data['district'];
				$earnings = $earnings->whereHas('student_details',function($query)use($district){
									    	$query->where('district', 'like', '%' .$district. '%');
									    });
			}
			$earnings = $earnings->get()
								 ->toArray();
								 
			// echo'<pre>'; print_r($earnings); die;
					if(!empty($earnings)){


				$earning_detail = [];
		        $earning_detail[] = ['Name','Student Unique Id','Address','State','District','Pincode','Email','Contact','Category','Course','Order Id','Purchase Date','Payment Id','Basic Amount','Discount Coupon','Discount Amount','Tax','Total Amount','Co-ordinator Id','Co-ordinator Name'];

		        $detail = [];
		        foreach ($earnings as $key => $value){
		            $detail['student_name']    = ucfirst($value['student_details']['first_name']).' '.ucfirst($value['student_details']['last_name']);
		            $detail['student_id']      = $value['user_id'];
		            $detail['address']         = ucfirst($value['student_details']['address']);
		            $detail['state_name']      = ucfirst($value['student_details']['state']['name']);
		            $detail['district']        = ucfirst($value['student_details']['district']);
		            $detail['pincode']         = ucfirst($value['student_details']['pincode']);
		            $detail['email']           = ucfirst($value['student_details']['email']);
		            $detail['contact']         = ucfirst($value['student_details']['contact']);
		            $detail['category_name']   = ucfirst($value['trainer_contents']['category']['name']);
		            $detail['course_name']     = ucfirst($value['trainer_contents']['title']);
		        	$detail['order_id']        = $value['id'];
		            $detail['recived_on']      = $value['purchased_on'];
		        	$detail['payment_id']      = $value['razor_pay_id'];
		            if($value['gst']>0 && !empty($value['discount_amount'])){
		            	
		            	$amount = $value['final_total']*$value['gst']/100;
		            	$amount = $value['final_total']-$amount;
		            	$detail['amount'] = $amount-$value['discount_amount'];
		            }else if($value['gst']>0 && empty($value['discount_amount'])){
		            	$amount = $value['final_total']*$value['gst']/100;
		            	$detail['amount'] = $value['final_total']-$amount;
		            }else if($value['gst']==0 && !empty($value['discount_amount'])){
		            	$detail['amount'] = $value['final_total']-$value['discount_amount'];
		            }else{
		            	$detail['amount'] = $value['price'];
		            }
		            $detail['discount_coupon'] = $value['discount_coupon'];
		            $detail['discount_amount'] = $value['discount_amount'];
		            $detail['gst']             = $value['gst'];
		            $detail['final_total']     = $value['final_total'];
		            if(!empty($value['coordinator'])){

			            $detail['coordinator_id']     = $value['coordinator']['coordinator_id'];
			            $detail['co_ordinatore_name'] = $value['coordinator']['coordinator_name'];
		            }elseif(!empty($value['coordinator_details'])){
			            $detail['coordinator_id']     = $value['coordinator_details']['coordinator_id'];
		            	$detail['co_ordinatore_name'] = $value['coordinator_details']['coordinator_name'];
		            }
		            // $detail['admin_commission']= $value['admin_commission'];
		            $earning_detail[] = $detail;
		        }

		        if(!empty(@$data['export_type'])){
					if($data['export_type']=='excel'){
		            	$extension = 'xls';
			        }elseif($data['export_type']=='csv'){
			            
			        	$extension = 'csv';
			        }else{
			        	$extension = 'pdf';
			        }
			    }
			    if($extension=='csv' || $extension=='xls'){

			        Excel::create('Payment-List',function($excel)use ($earning_detail){
			            $excel->setTitle('Payment-List');
			            $excel->setCreator('Laravel')->setCompany('Scamper Skills');
			            $excel->setDescription('Payment File');
			            $excel->sheet('sheet1',function($sheet) use ($earning_detail){
			            $sheet->fromArray($earning_detail, null, 'A1', false, false);
			            });
			        })->download($extension);    
			    }else{
		    	        $html =<<<EOD
		    	                    <html>
		    	                        <head>
		    	                            <meta charset="utf-8">
		    	                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    	                            <meta name="viewport" content="width=device-width, initial-scale=1">
		    	                            <title>Flags</title>
		    	                            <style>
		    	                                .th{
		    	                                    font-weight:bold;
		    	                                    background-color:#dfdfdf;    
		    	                                }
		    	                                img{
		    	                                    width:100px;
		    	                                }
		    	                            </style>
		    	                        </head>
		    	                        <body style="font-family:helvetica; background:#F0F0F0; margin:0; padding:20px;"> 
		    	                            <table style="width:100%; background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
		    	                                    <tr>
		    	                                        <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
		    	                                            <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skills"/>
		    	                                        </th>
		    	                                    </tr>
		    	                            </table>    
		    	                              
		    	                            <table border="1" style="width:100%;font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
		    	                                <thead style="background:#dfdfdf;">
		    	                                    <tr>
		    	                                        <th class="th">Name</th>
		    	                                        <th class="th">Student Unique Id</th>
		    	                                        <th class="th">Address</th>
		    	                                        <th class="th">State</th>
		    	                                        <th class="th">District</th>
		    	                                        <th class="th">Pincode</th>
		    	                                        <th class="th">Contact</th>
		    	                                        <th class="th">Category</th>
		    	                                        <th class="th">Course</th>
		    	                                        <th class="th">Order Id</th>
		    	                                        <th class="th">Purchase Date</th>
		    	                                        <th class="th">Payment Id</th>
		    	                                        <th class="th">Basic Amount</th>
		    	                                        <th class="th">Discount Coupon</th>
		    	                                        <th class="th">Discount Amount</th>
		    	                                        <th class="th">Tax</th>
		    	                                        <th class="th">Total Amount</th>
		    	                                        <th class="th">Co-ordinator Id</th>
		    	                                        <th class="th">Co-ordinator Name</th>
		    	                                    </tr>
		    	                                </thead>
		    	                                <tbody>
EOD;

		    	                                    foreach ($earnings as $key => $value){
		    	                                        
		    	                                       $detail['student_name']    = ucfirst($value['student_details']['first_name']).' '.ucfirst($value['student_details']['last_name']);
                                      		            $detail['student_id']      = $value['user_id'];
                                      		            $detail['address']         = ucfirst($value['student_details']['address']);
                                      		            $detail['state_name']      = ucfirst($value['student_details']['state']['name']);
                                      		            $detail['district']        = ucfirst($value['student_details']['district']);
                                      		            $detail['pincode']         = ucfirst($value['student_details']['pincode']);
                                      		            $detail['email']           = ucfirst($value['student_details']['email']);
                                      		            $detail['contact']         = ucfirst($value['student_details']['contact']);
                                      		            $detail['category_name']   = ucfirst($value['trainer_contents']['category']['name']);
                                      		            $detail['course_name']     = ucfirst($value['trainer_contents']['title']);
                                      		        	$detail['order_id']        = $value['id'];
                                      		            $detail['recived_on']      = $value['purchased_on'];
                                      		        	$detail['payment_id']      = $value['razor_pay_id'];
                                      		            if($value['gst']>0 && !empty($value['discount_amount'])){
                                      		            	
                                      		            	$amount = $value['final_total']*$value['gst']/100;
                                      		            	$amount = $value['final_total']-$amount;
                                      		            	$detail['amount'] = $amount-$value['discount_amount'];
                                      		            }else if($value['gst']>0 && empty($value['discount_amount'])){
                                      		            	$amount = $value['final_total']*$value['gst']/100;
                                      		            	$detail['amount'] = $value['final_total']-$amount;
                                      		            }else if($value['gst']==0 && !empty($value['discount_amount'])){
                                      		            	$detail['amount'] = $value['final_total']-$value['discount_amount'];
                                      		            }else{
                                      		            	$detail['amount'] = $value['price'];
                                      		            }
                                      		            $detail['discount_coupon'] = $value['discount_coupon'];
                                      		            $detail['discount_amount'] = $value['discount_amount'];
                                      		            $detail['gst']             = $value['gst'];
                                      		            $detail['final_total']     = $value['final_total'];
                                      		            if(!empty($value['coordinator'])){

                                      			            $detail['coordinator_id']     = $value['coordinator']['coordinator_id'];
                                      			            $detail['co_ordinatore_name'] = $value['coordinator']['coordinator_name'];
                                      		            }elseif(!empty($value['coordinator_details'])){
                                      			            $detail['coordinator_id']     = $value['coordinator_details']['coordinator_id'];
                                      		            	$detail['co_ordinatore_name'] = $value['coordinator_details']['coordinator_name'];
                                      		            }
		    	                                       
		    	                                        $html .=<<<EOD
		    	                                                    <tr>
		    	                                                        <td>{$detail['student_name']}</td>
		    	                                                        <td>{$detail['student_id']}</td>
		    	                                                        <td>{$detail['state_name']}</td>
		    	                                                        <td>{$detail['address']}</td>
		    	                                                        <td>{$detail['state_name']}</td>
		    	                                                        <td>{$detail['district']}</td>
		    	                                                        <td>{$detail['pincode']}</td>
		    	                                                        <td>{$detail['email']}
		    	                                                        </td>
		    	                                                        <td>{$detail['contact']}</td>
		    	                                                        <td>{$detail['category_name']}</td>
		    	                                                        <td>{$detail['course_name']}</td>
		    	                                                        <td>{$detail['order_id']}</td>
		    	                                                        <td>{$detail['recived_on']}</td>

		    	                                                        <td>{$detail['payment_id']}</td>
		    	                                                        <td>{$detail['amount']}</td>
		    	                                                        <td>{$detail['discount_coupon']}</td>
		    	                                                        <td>{$detail['discount_amount']}</td>
		    	                                                        <td>{$detail['gst']}</td>
		    	                                                        <td>{$detail['final_total']}</td>
		    	                                                        <td>{$detail['coordinator_id']}</td>
		    	                                                        <td>{$detail['co_ordinatore_name']}</td>
		    	                                                      
		    	                                                    </tr>
EOD;
		    	                                    }
		    	                            
			    	    $html .=<<<EOD
			    	                                </tbody>
			    	                            </table>
			    	                        </body>    
			    	                    </html> 
EOD;
			    	            // prx($html);
	    	        $pdf = App::make('dompdf.wrapper');
	    	        $pdf->loadHTML($html);
	    	        return $pdf->stream();
	    	        // return Redirect::back();

			    	    
			    }
		        // echo'<pre>'; print_r($earning_detail);die;
		    }else{
		    	return redirect()->back()->with('error','No data found to export');
		    }                  
		}
    
	}

}
