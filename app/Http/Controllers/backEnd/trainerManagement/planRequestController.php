<?php

namespace App\Http\Controllers\backEnd\trainerManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubscriptionPlan,App\Trainer;
class planRequestController extends Controller
{

	public function index(Request $request){

        $subscription_plan = SubscriptionPlan::select('subscription_plans.id','first_name','last_name','contact','admin_approval','title','u.id as trainer_id')
					                            ->join('users as u','u.id','subscription_plans.trainer_id')    
					                            ->where([
					                                'user_type'		            	=>'trainer',
					                                'u.deleted_at'		            =>null,
					                                'subscription_plans.deleted_at' =>null,
                                                ])
					                            ->get()
                            					->toArray();
                        // echo'<pre>'; print_r($subscription_plan);die;
		$page = 'subscription_plan';
		return view('backEnd.trainerManagement.subscriptionPlanRequest.index',compact('page','subscription_plan'));
	}

	public function view_details(Request $request,$subscription_plan_id){
        // dd($subscription_plan_id);
		$subscription_details = SubscriptionPlan::select('subscription_plans.*','u.first_name','u.last_name','contact')
                                                ->join('users as u','u.id','subscription_plans.trainer_id')
                                                ->where('subscription_plans.id',$subscription_plan_id)
                                                ->first();
                           		// echo'<pre>'; print_r($subscription_details->toArray()); die;
        if($request->isMethod("post")){

        	$data                                 = $request->input();
        	$subscription_details->admin_approval = $data['admin_approval'];
            if(!empty($data['rejection_reason'])&& $data['admin_approval']=='R'){
                $subscription_details->rejection_reason = $data['rejection_reason'];
                $subscription_details->admin_approval   = 'R';
                $admin_approval                         = 'Rejected';
            }else{
                $admin_approval   = 'Approved';
                $subscription_details->admin_approval   = 'A';
                $subscription_details->rejection_reason = '';
            }
        	if($subscription_details->save()){

                Trainer::subscription_plan_response_email($subscription_plan_id,'response'); 
        		return redirect()->back()->with('success','Subscription plan request'.' '.$admin_approval.' '.'Successfully');
        		
        	}else{
        		return redirect()->back()->with('error',COMMON_ERROR);
        	}

        }                   		
		$page = 'subscription_plan';
		return view('backEnd.trainerManagement.subscriptionPlanRequest.form',compact('page','subscription_details','subscription_plan_id'));
	}
}
