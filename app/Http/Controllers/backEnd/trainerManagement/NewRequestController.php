<?php

namespace App\Http\Controllers\backEnd\trainerManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User,App\Trainer,App\Country,App\State,App\City,Mail;
class NewRequestController extends Controller
{
	public function index(Request $request){

        $trainers = Trainer::select('trainers.id','first_name','last_name','email','contact','verified_account','admin_approval')
                            ->join('users as u','u.id','trainers.id')    
                            ->where([
                                'verified_account'=>'no',
                                'user_type'       =>'trainer'
                            ])
                            ->where('admin_approval','<>','A')
                            ->where('user_type','trainer')
                            ->whereNull('deleted_at')
                            ->get()
                            ->toArray();
                        // echo'<pre>'; print_r($trainers);die;
		$page = 'new_signup_request';
		return view('backEnd.trainerManagement.newRequest.index',compact('page','trainers'));
	}

	public function view_details(Request $request,$trainer_id){
        // dd($trainer_id);
		$trainer_details = Trainer::with('user_details.country','user_details.state','user_details.city','domain','subject','employment_status') 
                                 // ->join('states as s','s.id','users.state_id')     
                                 // ->join('countries as c','c.id','users.country_id')
                                 // ->join('cities as city','city.id','users.city_id')
                                  ->where('trainers.id',$trainer_id)
                                  ->first();
        if($request->isMethod("post")){
                           		// echo'<pre>'; print_r($trainer_details->toArray()); die;

        	$data                             = $request->input();
        	$trainer_details->admin_approval = $data['admin_approval'];
            if(!empty($data['rejection_reason'])&& $data['admin_approval']=='R'){
                $trainer_details->signup_rejection_reason = $data['rejection_reason'];
            }
        	if($data['admin_approval']=='A'){
        		$admin_approval   = 'Approved';
                $verified_account = 'yes';
        	}else{
        		$admin_approval   = 'Rejected';
                $verified_account = 'no';
        	}
        	if($trainer_details->save()){
        		$user = User::where('id',$trainer_id)->first();
        		$user->verified_account = $verified_account;
        		if($user->save()){
                    Trainer::request_response_email($trainer_id,'response'); 
        			return redirect('/admin/trainer/signup-requests')->with('success','Trainer Request'.' '.$admin_approval.' '.'Successfully');
        		}
        	}else{
        		return redirect()->back()->with('error',COMMON_ERROR);
        	}

        }                   		
		$page = 'new_signup_request';
		return view('backEnd.trainerManagement.newRequest.form',compact('page','trainer_details','trainer_id'));
	}

}
