<?php

namespace App\Http\Controllers\backEnd\categoryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject,App\SubSubCategory,App\SubCategory;
use App\Category;
class SubjectController extends Controller
{

    public function index(Request $request){
   		
        $details = Subject::select('subjects.*','c.name as category_name','sc.name as sub_category_name','ssc.name as sub_sub_category_name')
                            ->join('categories as c','c.id','subjects.category_id')
                            ->join('sub_categories as sc','sc.id','subjects.sub_category_id')
                            ->join('sub_sub_categories as ssc','ssc.id','subjects.sub_sub_category_id')
                            ->whereNull('c.deleted_at')
                            ->whereNull('sc.deleted_at')
                            ->whereNull('ssc.deleted_at')
                            ->whereNull('subjects.deleted_at')
                            ->get()
                            ->toArray(); 
       	$page = 'subjects';
       	return view('backEnd.categoryManagement.subject.index',compact('page','details'));
    }

    public function add(Request $request){

   	  	if($request->isMethod("post")){
    		$data                           = $request->all();
    		$subject                        = new Subject;
            $subject->category_id           = $data['category_id'];
            $subject->sub_category_id       = $data['sub_category_id'];
            $subject->sub_sub_category_id   = $data['sub_subcategory_id'];
            $subject->name                  = $data['name'];
            if($subject->save()){

				return redirect('admin/subjects')->with('success','Subject added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
        $categories = Category::whereNull('deleted_at')
                               ->get()
                               ->toArray();
        $page = 'subjects';
   		return view('backEnd.categoryManagement.subject.form',compact('page','categories'));
    }

    public function edit(Request $request,$subject_id){

        $subject = Subject::where('id',$subject_id)->first();
        if($request->isMethod("post")){

            $data           = $request->all();
            $subject->name  = $data['name'];
            if($subject->save()){

                return redirect()->back()->with('success','Subject edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $sub_category     = '';
        $sub_sub_category = '';
        if(!empty($subject->category_id)){
            $sub_category = SubCategory::where('category_id',$subject->category_id)
                                        ->whereNull('deleted_at')
                                        ->get()
                                        ->toArray();
        }
        if(!empty($subject->sub_category_id)){
            $sub_sub_category = SubSubCategory::where('sub_category_id',$subject->sub_category_id)               ->whereNull('deleted_at')
                                               ->get()
                                               ->toArray();
        }
        $categories = Category::whereNull('deleted_at')
                               ->get()
                               ->toArray();
        $page = 'subjects';
        return view('backEnd.categoryManagement.subject.form',compact('page','subject_id','subject','sub_category','sub_sub_category','categories'));
    }

    public function validate_name(Request $request){

        $data = $request->all();

        if($data['subject_id']==null){
            $check_name = Subject::where('name',$data['name'])
                                  ->where('sub_sub_category_id',$data['sub_sub_category_id'])
                                  ->whereNull('deleted_at')
                                  ->count(); 
        }else{
            $check_name = Subject::where('name',$data['name'])
                                  ->where('sub_sub_category_id',$data['sub_sub_category_id'])
                                  ->where('id','<>',$data['subject_id'])
                                  ->whereNull('deleted_at')
                                  ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

   	
    public function delete($subject_id){

        $del = Subject::where('id',$subject_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Subject deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
