<?php

namespace App\Http\Controllers\backEnd\categoryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubCategory;
class SubCategoryController extends Controller
{
   	public function index(Request $request,$category_id){
        
        $details = SubCategory::where('category_id',$category_id)
                                ->whereNull('deleted_at')
                                ->orderBy('id','Desc')
                                ->get()
                                ->toArray(); 
                                // dd($category_id);
        $page = 'sub_categories';
        return view('backEnd.categoryManagement.subCategory.index',compact('page','details','category_id'));
    }

    public function add(Request $request,$category_id){

        if($request->isMethod("post")){
            $data                       = $request->all();
            $Sub_category               = new SubCategory;
            $Sub_category->category_id  = $category_id;
            $Sub_category->name         = $data['name'];
            $Sub_category->place_footer = $data['add_to_footer'];
            if($Sub_category->save()){

                return redirect('admin/subcategories/'.$category_id)->with('success','Subcategory added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $page = 'sub_categories';
        return view('backEnd.categoryManagement.subCategory.form',compact('page','category_id'));
    }

    public function edit(Request $request,$sub_category_id){

        $sub_category = SubCategory::where('id',$sub_category_id)->first();
        $category_id  = $sub_category->category_id;
        if($request->isMethod("post")){

            $data                       = $request->all();
            // dd($data);   
            $sub_category->name         = $data['name'];
            $sub_category->place_footer = $data['add_to_footer'];
            if($sub_category->save()){

                return redirect()->back()->with('success','Subcategory edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $page = 'sub_categories';
        return view('backEnd.categoryManagement.subCategory.form',compact('page','sub_category_id','sub_category','category_id'));
    }

    public function validate_name(Request $request){

        $data = $request->all();

        if($data['sub_category_id']==null){
            $check_name = SubCategory::where('name',$data['name'])
                                      ->where('category_id',$data['category_id'])
                                      ->whereNull('deleted_at')
                                      ->count();
        }else{
            $check_name = SubCategory::where('name',$data['name'])
                                      ->where('category_id',$data['category_id'])
                                      ->where('id','<>',$data['sub_category_id'])
                                      ->whereNull('deleted_at')
                                      ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

    
    public function delete($sub_category_id){

        $del = SubCategory::where('id',$sub_category_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Subcategory deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
