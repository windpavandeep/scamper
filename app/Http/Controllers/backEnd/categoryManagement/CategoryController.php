<?php

namespace App\Http\Controllers\backEnd\categoryManagement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
class CategoryController extends Controller
{
    
    public function index(Request $request){
   		
        $details = Category::whereNull('deleted_at')
                            ->get()
                            ->toArray(); 
       	$page = 'categories';
       	return view('backEnd.categoryManagement.category.index',compact('page','details'));
    }

    public function add(Request $request){

   	  	if($request->isMethod("post")){
    		$data           = $request->all();
    		$category       = new Category;
            $category->name = $data['name'];
            if($category->save()){

				return redirect('admin/categories')->with('success','Category added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
        $page = 'categories';
   		return view('backEnd.categoryManagement.category.form',compact('page'));
    }

    public function edit(Request $request,$category_id){

        $category = Category::where('id',$category_id)->first();
        if($request->isMethod("post")){

            $data           = $request->all();
            $category->name = $data['name'];
            if($category->save()){

                return redirect()->back()->with('success','Category edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $page = 'categories';
        return view('backEnd.categoryManagement.category.form',compact('page','category_id','category'));
    }

    public function validate_name(Request $request){

        $data = $request->all();
       // dd($data);
        if($data['category_id']==null){
            $check_name = category::where('name',$data['name'])
                                  ->whereNull('deleted_at')
                                  ->count(); 
        }else{
            $check_name = category::where('name',$data['name'])
                                  ->where('id','<>',$data['category_id'])
                                  ->whereNull('deleted_at')
                                  ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

   	
    public function delete($category_id){

        $del = Category::where('id',$category_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Category deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}