<?php

namespace App\Http\Controllers\backEnd\categoryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Unit,App\Category;
class UnitController extends Controller
{
    
    public function index($subject_id){
   		
        $details = Unit::where('subject_id',$subject_id) 
                        ->whereNull('units.deleted_at')	
                        ->get()
                        ->toArray(); 
       	$page = 'subjects';
       	return view('backEnd.categoryManagement.subject.unit.index',compact('page','details','subject_id'));
    }

    public function add(Request $request,$subject_id){

   	  	if($request->isMethod("post")){

    		$data             = $request->all();
            $unit             = new Unit;
            $unit->subject_id = $subject_id;
            $unit->name       = $data['unit_name'];
            if($unit->save()){

				return redirect('admin/subject/unit/'.$subject_id)->with('success','Unit added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
        $page = 'subjects';
   		return view('backEnd.categoryManagement.subject.unit.form',compact('page','subject_id'));
    }

    public function edit(Request $request,$unit_id){

        $unit = Unit::where('id',$unit_id)->first();
        if($request->isMethod("post")){

            $data       = $request->all();
            $unit->name = $data['unit_name'];
            if($unit->save()){

                return redirect()->back()->with('success','Unit edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $subject_id = $unit->subject_id;

        $page = 'subjects';
        return view('backEnd.categoryManagement.subject.unit.form',compact('page','unit_id','unit','subject_id'));
    }

    public function validate_name(Request $request){

        $data = $request->all();

        if($data['unit_id']==null){
            $check_name = Unit::where('name',$data['unit_name'])
                              ->where('subject_id',$data['subject_id'])
                              ->whereNull('deleted_at')
                              ->count();
        }else{
            $check_name = Unit::where('name',$data['unit_name'])
                              ->where('subject_id',$data['subject_id'])
                              ->where('id','<>',$data['unit_id'])
                              ->whereNull('deleted_at')
                              ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

   	
    public function delete($unit_id){

        $del = Unit::where('id',$unit_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Unit deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
