<?php

namespace App\Http\Controllers\backEnd\categoryManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubSubCategory,App\SubCategory;

class SubSubCategoryController extends Controller
{
    public function index(Request $request,$sub_category_id){
        
        $details = SubSubCategory::where('sub_category_id',$sub_category_id)
                                ->whereNull('deleted_at')
                                ->orderBy('id','Desc')
                                ->get()
                                ->toArray(); 
        $category_id = SubCategory::where('id',$sub_category_id)->value('category_id');
                                // dd($category_id);
        $page = 'sub_sub_categories';
        return view('backEnd.categoryManagement.subSubCategory.index',compact('page','details','sub_category_id','category_id'));
    }

    public function add(Request $request,$sub_category_id){


        if($request->isMethod("post")){

            $data                               = $request->all();
            $sub_sub_category                   = new SubSubCategory;
            $sub_sub_category->sub_category_id  = $sub_category_id;
            $sub_sub_category->name             = $data['name'];
            if($sub_sub_category->save()){

                return redirect('admin/sub-subcategories/'.$sub_category_id)->with('success','Sub subcategory added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$category_id = SubCategory::where('id',$sub_category_id)->value('category_id');
        $page = 'sub_sub_categories';
        return view('backEnd.categoryManagement.subSubCategory.form',compact('page','sub_category_id','category_id'));
    }

    public function edit(Request $request,$sub_sub_category_id){

        $sub_sub_category = SubSubCategory::where('id',$sub_sub_category_id)->first();
        if($request->isMethod("post")){

            $data               = $request->all();
            $sub_sub_category->name = $data['name'];
            if($sub_sub_category->save()){

                return redirect()->back()->with('success','sub subcategory edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $sub_category_id  = $sub_sub_category->sub_category_id;
        $category_id      = SubCategory::where('id',$sub_category_id)->value('category_id');
        $page = 'sub_sub_categories';
        return view('backEnd.categoryManagement.subSubCategory.form',compact('page','sub_category_id','sub_sub_category','category_id','sub_sub_category_id'));
    }

    public function validate_name(Request $request){

        $data = $request->all();
       // dd($data);
        if($data['sub_sub_category_id']==null){
            $check_name = SubSubCategory::where('name',$data['name'])
                                         ->where('sub_category_id',$data['sub_category_id'])
                                         ->whereNull('deleted_at')
                                         ->count(); 
        }else{
            $check_name = SubSubCategory::where('name',$data['name'])
                                        ->where('sub_category_id',$data['sub_category_id'])
                                        ->where('id','<>',$data['sub_sub_category_id'])
                                        ->whereNull('deleted_at')
                                        ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

    
    public function delete($sub_sub_category_id){

        $del = SubSubCategory::where('id',$sub_sub_category_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Sub subcategory deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }

}
