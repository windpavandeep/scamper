<?php

namespace App\Http\Controllers\backEnd\franchise;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Franchise, App\State, App\City, App\TrainerDomain,App\Subject,App\User;
class FranchiseController extends Controller
{
    public function index(){

    	$franchise_details = Franchise::orderBy('id','desc')
    							->get()
    							->toArray();
    	// echo'<pre>'; print_r($franchise_details);die;
    	$page = 'franchise';
    	return view('backEnd.retailer.index',compact('page','franchise_details'));
    }

    public function detail($franchise_id = null){

    	$franchise_details = Franchise::where('id',$franchise_id)
    							->with('country')
    							->with('state')
    							->with('city')
    							->first();
    	if(!empty($franchise_details)){
    		$franchise_details = $franchise_details->toArray();
    	}
    	// echo'<pre>'; print_r($franchise_details);die;
    	$page = 'franchise';
    	return view('backEnd.retailer.form',compact('page','franchise_details'));
    }

}
