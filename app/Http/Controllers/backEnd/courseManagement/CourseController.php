<?php

namespace App\Http\Controllers\backEnd\courseManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainerContent,App\TrainerContentImage,App\Category,App\Subject,App\Language;
use App\TrainerContentFile,App\Unit,App\TrainerContentSubject,App\TrainerContentSubjectUnit,App\SubCategory,App\SubSubCategory;
use App\Admin,App\User;
use Illuminate\Support\Facades\Storage;
class CourseController extends Controller
{
	public function index(){

	

		$course_details = TrainerContent::select('trainer_contents.*','c.name')
										 ->where('trainer_id','0')
										 ->join('categories as c','trainer_contents.category_id','c.id')
										 ->whereNull('trainer_contents.deleted_at')
										 ->whereNull('c.deleted_at')
										 ->get()
										 ->toArray();
		// echo'<pre>'; print_r($course_details); die;

     	$page = 'course_management';
    	return view('backEnd.courseManagement.index',compact('page','course_details'));
    }

    public function add(Request $request){

		if($request->isMethod('post')){

			$data                	   	= $request->all();
			// echo'<pre>'; print_r($data); die;
			$trainer_id					= 0;
			$content    				= new TrainerContent;
			$content->trainer_id   		= $trainer_id;
			$content->category_id   	= $data['category_id'];
			$content->sub_category_id   = $data['sub_category_id'];
			$content->sub_subcategory_id= $data['sub_subcategory_id'];
			$content->unit_id   		= @$data['unit_id'];
			$content->title				= $data['course_title'];
			$content->language_id       = $data['language_id'];
			$content->start_date     	= date('Y-m-d',strtotime($data['start_date']));
			$content->end_date      	= date('Y-m-d',strtotime($data['end_date']));
			$content->no_of_exams       = $data['no_of_exams'];
			$content->free_question     = $data['free_question'];
			$content->subscriber_video  = $data['subscriber_video'];

			if($data['content_type']=='paid'){

				if(!empty($data['paid_amount'])){
					
					if(!empty(@$data['include_gst'])){

						$gst                  = Admin::select('gst')->value('gst');	
						$gst_price            = ($data['paid_amount']*2)/100;
						$content->gst         = $gst;
						$content->gst_price   = $gst_price;
						$content->final_price = $gst_price+$data['paid_amount'];
						$content->include_gst = 'Y';

					}else{
						$content->include_gst = 'N';
						$content->final_price = $data['paid_amount'];
					}
					$content->paid_amount 	  = $data['paid_amount'];
				}
			}
			
			// echo'<pre>'; print_r($_FILES['pdf']);die;
			// free_question
			$content->content_availability    = $data['content_type'];
			$content->show_answer_after_exam  = $data['show_answer_after_exam'];
			
			$content->description     = $data['description'];
			
			if($content->save()){
			// echo'<pre>'; print_r($data); die;

				if(!empty($data['content'])){
					foreach ($data['content'] as $key => $value) {
						// dd($content->id);
						$subject              		  = new TrainerContentSubject;
						$subject->trainer_content_id  = $content->id;
						$subject->subject_id 		  = $value['subject'];
						if($subject->save()){

							foreach($value['unit_id'] as $key=> $unit_id){

								$unit            				  = new TrainerContentSubjectUnit;
								$unit->trainer_content_subject_id =  $subject->id;
								$unit->unit_id    				  =  $unit_id;
								$unit->save();
							}
						}
					}
				}
			
				
				foreach($_FILES['file']['name'] as $key => $vid) {
					if(!empty($_FILES['file']['name'][$key])){
						$file = $request->file('file');
					    $video      = pathinfo($_FILES['file']['name'][$key]);
					    $ext        = $video['extension'];
					    $tmp_name   = $_FILES['file']['tmp_name'][$key];
					    $random_no  = uniqid();
					    $new_name   = $random_no.'.'.$ext;
					   // $destination= TrainerContentBasePath.'/'.$new_name;
					    if($ext == 'mp4' || $ext == 'mov'|| $ext == 'avi'|| $ext == 'pdf'){
					    	 $test  =  Storage::disk('s3')->put($new_name,file_get_contents($file[$key]));
					       // move_uploaded_file($tmp_name, $destination);
					        $content->file = $new_name;
					        $trainer_content_video 	           	 		= new TrainerContentFile;
                            // dd($content->id);
                            $trainer_content_video->trainer_content_id  = $content->id;
					        $trainer_content_video->file    			= $new_name;
					        if(strtolower($ext)=='pdf'){
					        	$trainer_content_video->type = 'pdf'; 
					        }else{
					        	$trainer_content_video->type = 'video';
					        }
                            $trainer_content_video->save();
					    }else{
					        return redirect()->back()->with('error','Invalid file extension');
					    }
						// dd($content);
					}
				}
					
				
				if(!empty($_FILES)){

				    foreach($_FILES['images']['name'] as $key => $img) {
	                    if(!empty($_FILES['images']['name'][$key])){
	                        $image      = pathinfo($_FILES['images']['name'][$key]);
	                        $ext        = strtolower($image['extension']);
	                        $temp_name  = $_FILES['images']['tmp_name'][$key];
	                        $random_no  = uniqid();
	                        $new_name   = $random_no.'.'.$ext;
	                        if($ext    == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
	                            $destination = base_path().'/'.TrainerContentImageBasePath;
	                            move_uploaded_file($temp_name, $destination.'/'.$new_name);
	                            $trainer_content_image 	           	 		= new TrainerContentImage;
	                            $trainer_content_image->trainer_content_id  = $content->id;
	                            $trainer_content_image->name      			= $new_name;
	                            $trainer_content_image->save();
	                        }else{
	                            return redirect()->back()->with('error','Invalid file extension');
	                        }
	                    }
	                }	
				}

				$exam = '';
				User::send_notification($exam,$content->id);
				return redirect('/admin/courses')->with('success','Product added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
    	$categories = Category::whereNull('deleted_at')->get()->toArray();
    	// $subjects   = Subject::whereNull('deleted_at')->get()->toArray();
    	$languages  = Language::whereNull('deleted_at')->get()->toArray();
    	// echo '<pre>'; print_r($subjects); die;
    	$course_detail = [];
    	$units         = [];
    	$page = 'course_management';
    	return view('backEnd.courseManagement.form',compact('page','categories','subjects','languages','course_detail','units'));
    }

    public function edit(Request $request,$course_id){

    	$course_detail = TrainerContent::with(['course_images','course_files','content_subjects.content_unit'
    	])
    									->where('trainer_contents.id',$course_id)
    									->first();
    									// echo'<pre>'; print_r($course_detail->toArray(); die;
    	$upload_type = @$course_detail->upload_type;
		if($request->isMethod('post')){
			// dd($course_detail->upload_type);
			$data                   		   	= $request->all();
			// echo'<pre>'; print_r($data); die;
			$course_detail->category_id   	   	= $data['category_id'];
			$course_detail->sub_category_id    	= $data['sub_category_id'];
			$course_detail->sub_subcategory_id 	= $data['sub_subcategory_id'];
			$course_detail->title			   	= $data['course_title'];
			$course_detail->language_id        	= $data['language_id'];
			$course_detail->start_date     	   	= date('Y-m-d',strtotime($data['start_date']));
			$course_detail->end_date      	   	= date('Y-m-d',strtotime($data['end_date']));
			$course_detail->no_of_exams         = $data['no_of_exams'];
			$course_detail->content_availability= $data['content_type'];
			$course_detail->subscriber_video  	= $data['subscriber_video'];
			$course_detail->free_question    	= $data['free_question'];
			if($data['content_type']=='paid'){

				if(!empty($data['paid_amount'])){
					
					if(!empty(@$data['include_gst'])){
						
						$gst                 	    = Admin::select('gst')->value('gst');	
						$gst_price                  = ($data['paid_amount']*2)/100;
						$course_detail->gst         = $gst;
						$course_detail->gst_price   = $gst_price;
						$course_detail->final_price = $gst_price+$data['paid_amount'];
						$course_detail->include_gst = 'Y';
					}else{
						$course_detail->include_gst = 'N';
						$course_detail->gst         = '0';
						$course_detail->gst_price   = '0';
						$course_detail->final_price = '0';
						$course_detail->final_price = $data['paid_amount'];
					}
					$course_detail->paid_amount 	= $data['paid_amount'];
				}
			}else{
				$course_detail->paid_amount = '0';
				$course_detail->gst         = '0';
				$course_detail->gst_price   = '0';
				$course_detail->final_price = '0';
				$course_detail->include_gst = '0';
			}
			$course_detail->description             = $data['description'];
			$course_detail->show_answer_after_exam  = $data['show_answer_after_exam'];
			if($course_detail->save()){

				if(!empty($data['content'])){
					$arr              = [];
					$new_content      = array_map(function($q) use($arr){ 
                                                                      $arr = $q['subject'];
                                                                      return $arr;
                                                                       }, $data['content']);
					$prev_ids         =  array_map(function($q) use($arr){ 
                                                                      $arr = $q['subject_id'];
                                                                      return $arr;
                                                                       }, $course_detail->content_subjects->toArray());
					$prev_subject_ids = array_diff($prev_ids, $new_content);
					// echo'<pre>'; print_r($prev_subject_ids);die;
					if(!empty($prev_subject_ids)){

						foreach ($prev_subject_ids as $key => $subject_id) {
							
							$del 		 = TrainerContentSubjectUnit::where([
																'trainer_content_subject_id'=>$subject_id
															 ])
															 ->delete();  
							$subject_del = TrainerContentSubject::where([
																'trainer_content_id'=>$course_id,
																'subject_id'        =>$subject_id,
															 ])->delete(); 
						}
					}
					foreach ($data['content'] as $value) {
						// dd($content->id);
						$subject_del = TrainerContentSubject::where([
																'trainer_content_id'=>$course_id,
																'subject_id'        =>$value['subject'],
															 ])->first(); 
						// echo'<pre>'; print_r($subject_del); die;
						$previous_subject_id          = $subject_del['id'];
						$subject              		  = new TrainerContentSubject;
						// dd('enter');
						$subject->trainer_content_id  = $course_id;
						$subject->subject_id 		  = $value['subject'];
						if($subject->save()){

							if(!empty($previous_subject_id)){

								$del = TrainerContentSubjectUnit::where([
																	'trainer_content_subject_id'=>$previous_subject_id
																 ])
																 ->delete();  
								$subject_del->delete();
							}

							foreach($value['unit_id'] as $id){
								if(!empty($id)){

									$unit             				 = new TrainerContentSubjectUnit;
									$unit->trainer_content_subject_id=  $subject->id;
									$unit->unit_id     		 		 =  $id;
													
									$unit->save();
								}
							}
						}
					}
				}

				foreach($_FILES['file']['name'] as $key => $vid) {
					//dd($_FILES);
					if(!empty($_FILES['file']['name'][$key])){
					    $video      = pathinfo($_FILES['file']['name'][$key]);
					    $ext        = $video['extension'];
					    $tmp_name   = $_FILES['file']['tmp_name'][$key];
					    $random_no  = uniqid();
					    $new_name   = $random_no.'.'.$ext;
					    $destination= TrainerContentBasePath.'/'.$new_name;
					    if($ext == 'mp4' || $ext == 'mov'|| $ext == 'avi'|| $ext == 'pdf'){
					    	//dd($tmp_name);
					        move_uploaded_file($tmp_name, $destination);
					        $trainer_content_video 	           	 		= new TrainerContentFile;
                            // dd($content->id);
                            $trainer_content_video->trainer_content_id  = $course_detail->id;
					        $trainer_content_video->file    			= $new_name;
					        if(strtolower($ext)=='pdf'){
					        	$trainer_content_video->type = 'pdf'; 
					        }else{
					        	$trainer_content_video->type = 'video';
					        }
                            $trainer_content_video->save();
					    }else{
					        return redirect()->back()->with('error','Invalid file extension');
					    }
						// dd($content);
					}
				}
				if(!empty($_FILES['images'])){

				    foreach($_FILES['images']['name'] as $key => $img) {

	                    if(!empty($_FILES['images']['name'][$key])){
	                        $image      = pathinfo($_FILES['images']['name'][$key]);
	                        $ext        = strtolower($image['extension']);
	                        $temp_name  = $_FILES['images']['tmp_name'][$key];
	                        $random_no  = uniqid();
	                        $new_name   = $random_no.'.'.$ext;
	                        if($ext    == 'jpg' || $ext == 'png' || $ext == 'jpeg'){
	                            $destination = base_path().'/'.TrainerContentImageBasePath;
	                            move_uploaded_file($temp_name, $destination.'/'.$new_name);
	                            $trainer_content_image 	           	 		= new TrainerContentImage;
	                            $trainer_content_image->trainer_content_id  = $course_id;
	                            $trainer_content_image->name      			= $new_name;
	                            $trainer_content_image->save();
	                        }else{
	                            return redirect()->back()->with('error','Invalid file extension');
	                        }
	                    }
	                }	
				}
				return redirect()->back()->with('success','Product edited successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
    	$categories       = Category::whereNull('deleted_at')->get()->toArray();
    	$subjects         = Subject::where('sub_sub_category_id',$course_detail->sub_subcategory_id)
    								->whereNull('deleted_at')->get()->toArray();
    						
    	$languages        = Language::whereNull('deleted_at')->get()->toArray();
    	$sub_category     = '';
    	$sub_sub_category = '';
    	if(!empty($course_detail->category_id)){
    		$sub_category = SubCategory::where('category_id',$course_detail->category_id)
    									->whereNull('deleted_at')
    									->get()
    									->toArray();
    	}
    	if(!empty($course_detail->sub_category_id)){
    		$sub_sub_category = SubSubCategory::where('sub_category_id',$course_detail->sub_category_id)			   ->whereNull('deleted_at')
    									       ->get()
    									       ->toArray();
    	}
    	$units = Unit::whereNull('deleted_at')->get()->toArray();

    
    	$page = 'course_management';
    	return view('backEnd.courseManagement.form',compact('page','categories','subjects','languages','course_detail','course_id','sub_category','sub_sub_category','units','selected_unit'));
    }

    public function delete($course_id){
    	// dd($course_id);
    	$del  = TrainerContent::where('id',$course_id)
    						  ->update(['deleted_at'=>date('Y-m-d h:i:s')]);      
    						 // dd($del);
    	if ($del) {
    	    return redirect()->back()->with('success','Product deleted successfully');
    	}else{
    	    return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }

    public function get_units($subject_id){

    	$units = Unit::where('subject_id',$subject_id)
    				  ->whereNull('deleted_at')
    				  ->get()
    				  ->toArray();
    	$unit_options = '';
    	if(!empty($units)){

			foreach ($units as $key => $value) {
				$unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
			}
		
    	}
    	return $unit_options;
    }

    public function append_subject_content(Request $request,$content_count,$sub_subcategory_id){

 
    	$contents 	   = '';
    	$subject_count = Subject::where('sub_sub_category_id',$sub_subcategory_id)
    							->whereNull('deleted_at')
	    				  		->count();
	   	if($content_count<=$subject_count){

	    	$subjects = Subject::where('sub_sub_category_id',$sub_subcategory_id)
	    						->whereNull('deleted_at')
		    				  	->get()
		    				  	->toArray();
		    
		 	$rand = rand(10000,999999);
		    if(!empty($subjects)){
			   	$contents .= '<tr id="subject_div" class="subject_div_class">
			   					<td>
			   						<div class="form-group in-group">
			   							<span class="error1"></span>
			   							<select class="form-control subject_id" id="subject_id" name="content[subject_id][]" required>
			   							    <option value="" readonly>Choose Subject</option>';
			   							    foreach($subjects as $subject){
			   							        $contents .='<option value="'.$subject['id'].'">'.ucfirst($subject['name']).'</option>';
			   							    }
			   							$contents .='</select>
			   						</div>
			   					
			   					</td>
			   					<td>
				   					<div class="form-group in-group">
				   						<span class="error2"></span>
				   						<select class="form-control unit_id  js-example-basic-multiple unit_class random'.$rand.'" id="unit_id" name="content[subject_id][unit_id][]" multiple="" required>
				   							<option value="" readonly>Choose Unit</option>
				   						</select>
				   					</div>
			   					</td>
			   					<td>
			   						<a href="javascript:;" class="remove-subject">
			   							<i class="mdi mdi-minus-box plus-icn" id="plus-icn" title="Remove">
			   							</i>
			   						</a>
			   					</td>
			   				</tr>
			   				<script type="text/javascript">
			   					$(document).ready(function() {
	        						$("tr:last #unit_id").multipleSelect();
	     						});  
	     					</script> ';
		    }
	   	}
	    // echo'<pre>'; print_r($contents);die;
	    return $contents;
    }

    public function course_image_delete($course_image_id){

		$del         = TrainerContentImage::where('id',$course_image_id)->first();   
		// dd($del); 
	    $destination = base_path().'/'.TrainerContentImageBasePath;
	    // dd($destination);
	    if (file_exists($destination.'/'.$del->name)){
	        unlink($destination.'/'.$del->name);
	    }     
	    $del->delete();

	    if ($del){
	        return redirect()->back()->with('success','Product image deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
    }
    public function course_file_delete($course_file_id){

		$del         = TrainerContentFile::where('id',$course_file_id)->first();   
		// dd($del); 
	    $destination = base_path().'/'.TrainerContentBasePath;
	    // dd($destination);
	    if (file_exists($destination.'/'.$del->file)){
	        unlink($destination.'/'.$del->file);
	    }     
	    $del->delete();

	    if ($del){
	        return redirect()->back()->with('success','Product file deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
    }
    public function get_subjects($sub_sub_category_id){

    	$subjects = Subject::where('sub_sub_category_id',$sub_sub_category_id)
    					    ->whereNull('deleted_at')
    					    ->get()
    					    ->toArray();
    	$options= '<option value="">Choose Subject</option>';
    	foreach ($subjects as $key => $value) {
    		
    		$options .='<option value="'.$value['id'].'">'.ucfirst($value['name']).'</option>';
    	}
    	return $options;
    }
}
