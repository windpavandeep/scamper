<?php

namespace App\Http\Controllers\backEnd\studentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exam,App\User,App\UserExam,Excel;
use TCPDF;
use Carbon\Carbon;
class StudentExamController extends Controller
{	
	// $user_exams = '';
	public function index($student_id){

		$user_exams = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time')
    						  ->join('users as u','u.id','user_exams.user_id')
    						  ->join('exam as e','e.id','user_exams.exam_id')
    						  ->where('user_id',$student_id)
    						  ->get()
    						  ->toArray();
    	
    						 // $this->export_student_report($user_exams);
		// echo'<pre>'; print_r($user_exams); die;
		$page = 'student_management';
    	return view('backEnd.studentManagement.exams.index',compact('page','user_exams','student_id'));

	}

	public function export_student_report(Request $request){

			// dd('enter');
		if($request->isMethod('post')){
			$data = $request->input();
			$student_id = $data['student_id'];
			$export_type = $data['export_type'];
			// dd($export_type);

			$user_exams = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time','total_attempt')
    						  ->join('users as u','u.id','user_exams.user_id')
    						  ->join('exam as e','e.id','user_exams.exam_id')
    						  ->where('user_id',$student_id);
            if(!empty($data['date'])){
                $createdAt = Carbon::parse($data['date'])->format('d-m-Y');
                $user_exams = $user_exams->where('start_date',$createdAt)
                                         ->orWhere('expiry_date',$createdAt);
            }

            $user_exams = $user_exams->get()
                                     ->toArray();
			if($export_type=='excel'){
			   $extension = 'xls';

			}elseif($export_type=='pdf'){
				$extension = 'pdf';
			}else{
			   $extension = 'csv';
			}
			if($extension=='pdf'){

                include('vendor/tcpdf/tcpdf.php');                     
                $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $tcpdf->AddPage('M','A4');
                $tcpdf->SetCreator(PDF_CREATOR);
                $title ='Scamper Skills';
                // $subtitle='Krishikulture';
                $tcpdf->SetTitle('Student-Analysis-Report');
                $tcpdf->SetSubject('Student-Analysis-Report');
                $tcpdf->setPrintHeader(true);
                $tcpdf->setPrintFooter(false);
                $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $tcpdf->SetMargins(10, 10, 10, true);
                $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $tcpdf->SetFont('helvetica', '', 9);
                $tcpdf->setFontSubsetting(false);
                $tcpdf->setFontSubsetting(false);
          
				$html =<<<EOD
                    <html>
                        <head>
                            <meta charset="utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <title>Flags</title>
                            <style>
                                .th{
                                    font-weight:bold;
                                    background-color:#dfdfdf;    
                                }
                                img{
                                    width:200px;
                                    height:auto;
                                }
                                .result_heading th{
                                	font-size:12px;                            
                                }
                                  .result_detail td{
                                	font-size:10px;                            
                                }
                            </style>
                        </head>
                        <body style="font-family:helvetica; background:#fff; margin:0; padding:20px;"> 
                            <table style="width:100%; 
                             background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
                                    <tr>
                                        <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
                                            <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skill"/>
                                        </th>
                                    </tr>
                            </table>    
                              
                            <table border="1" style="width:100%; font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
                                <thead style="background:#dfdfdf;">
                                    <tr class="result_heading">
                                        <th class="th">Exam Name</th>
                                        <th class="th">Marks Obtained</th>
                                        <th class="th">Total Marks</th>
                                        <th class="th">Total Question Attempt</th>
                                        <th class="th">Set Name</th>
                                        <th class="th">Duration(In minutes)</th>
                                        <th class="th">Exam Start Date</th>
                                        <th class="th">Exam End Date</th>
                                        <th class="th">Exam Given On</th>
                                    </tr>
                                </thead>
                                <tbody>
EOD;

                                foreach($user_exams as $exam){
                                    
                                   
                                    $exam['exam_start_date']        = date('d-m-Y',strtotime($exam['start_date']));
                               	   	$exam['exam_end_date']        = date('d-m-Y',strtotime($exam['expiry_date']));
                                   	$exam['given_on']         	  = date('d-m-Y',strtotime($exam['start_date_time']));
                                   	$exam['duration']         	  = (int)(date('i',$exam['duration'] / 1000));

                                
                                    $html .=<<<EOD
                                    <tr class="result_detail">
                                        <td>{$exam['name']}</td>
                                        <td>{$exam['marks_obtained']}</td>
                                        <td>{$exam['total_marks']}</td>
                                        <td>{$exam['total_attempt']}</td>
                                        <td>{$exam['set_name']}</td>
                                        <td>{$exam['duration']}</td>
                                        <td>{$exam['exam_start_date']}</td>
                                        <td>{$exam['exam_end_date']}</td>
                                        
                                        <td>{$exam['given_on']}</td>
                                    </tr>
EOD;
                                }
    $html .=<<<EOD
                                </tbody>
                            </table>
                        </body>    
                    </html> 
EOD;
                
                // $pdf = PDF::loadHtml($html);
                $tcpdf->writeHTML($html, true, false, true, false, '');
                $url = public_path();
                $save=$tcpdf->Output('Student-Analysis-Report.pdf', 'I');
                $tcpdf->Output('Student-Analysis-Report.pdf','D');
                return Redirect::back();
					
			}else{
				
     
    			$student_detail   = [];
    			$student_detail[] = ['Exam Name','Marks Obtained','Total Marks','Total Question Attempt','Set Name','Duration(In minutes)','Exam Start Date','Exam End Date','Exam Given On'];

    			$detail = [];
    			foreach ($user_exams as $key => $value){
    			
    			    $detail['name']             	  = ucfirst($value['name']);
    			    $detail['marks_obtained']   	  = $value['marks_obtained'];
    			    $detail['total_marks']      	  = $value['total_marks'];
    			    $detail['total_question_attempt'] = $value['total_attempt'];
    			    $detail['set_name']        		  = $value['set_name'];
    			    if(!empty($value['duration'])){
                            $detail['duration'] =(int)(date('i',$value['duration'] / 1000));
                    }else{
                        $detail['duration'] = 0;
                    }
    		     	$detail['exam_start_date']        = date('d-m-Y',strtotime($value['start_date']));
    			   	$detail['exam_end_date']          = date('d-m-Y',strtotime($value['expiry_date']));
    			    $detail['given_on']         	  = date('d-m-Y',strtotime($value['start_date_time']));
    			    $student_detail[]           = $detail;
    			}
    			// dd($student_detail);
    			Excel::create('Student-Analysis-Report',function($excel)use ($student_detail){
    			    $excel->setTitle('Student-List');
    			    $excel->setCreator('Laravel')->setCompany('Scamper Skills');
    			    $excel->setDescription('Student-Analysis-Report');
    			    $excel->sheet('sheet1',function($sheet) use ($student_detail){
    			    $sheet->fromArray($student_detail, null, 'A1', false, false);
    			    });
    			})->download($extension);  

    		}
    	}
	}

	public function exam_details($user_exam_id){

		$user_exam = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time','total_attempt')
    						  ->join('users as u','u.id','user_exams.user_id')
    						  ->join('exam as e','e.id','user_exams.exam_id')
    						  ->where('user_exams.id',$user_exam_id)
    						  ->first();
    	
    						 // $this->export_student_report($user_exams);
		// echo'<pre>'; print_r($user_exam); die;
		$page = 'student_management';
    	return view('backEnd.studentManagement.exams.form',compact('page','user_exam','user_exam_id'));

	}

}
