<?php

namespace App\Http\Controllers\backEnd\studentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country, App\State, App\City, App\TrainerDomain,App\Subject,App\User;
use Excel,App\Category,App\SubCategory;
use TCPDF,App\Transaction,App\SubscriptionPlan,App\UserCourse;
class StudentController extends Controller
{
    public function index(){

    	$student_details = User::where('user_type','user')
    							->whereNull('deleted_at')
                                ->orderBy('id','Desc')
    							->get()
    							->toArray();

    	// echo'<pre>'; print_r($student_details);die;
        $countries = Country::get()->toArray();
    	$domains = Category::get()->toArray();
        $page = 'student_management';
    	return view('backEnd.studentManagement.index',compact('page','student_details','countries','domains'));
    }

    public function add(Request $request){

    	if($request->isMethod('post')){

    		$data  				= $request->input();
    		// dd($data);
    		$student  						= new User;
    		$student->first_name 			= $data['first_name'];
    		$student->last_name 			= $data['last_name'];
    		$student->email 				= $data['email'];
    		$student->contact 				= $data['contact'];
    		$student->dob 					= date('Y-m-d',strtotime($data['dob']));
    		$student->gender 				= $data['gender'];
    		$student->country_id 			= $data['country_id'];
    		$student->state_id 			    = $data['state_id'];
    		$student->city_id 				= $data['city_id'];
    		$student->exam_preparation_id 	= $data['sub_category_id'];
    		$student->added_by  			= 'admin';
    		$student->user_type 			= 'user';
    		$student->verified_account      = 'yes';
            $student->status                = $data['status'];
            $student->district              = $data['district'];
            $student->address               = $data['address'];
    		if(!empty($_FILES['image']['name'])){
    		    $image      = pathinfo($_FILES['image']['name']);
    		    $ext        = $image['extension'];
    		    $tmp_name   = $_FILES['image']['tmp_name'];
    		    $random_no  = uniqid();
    		    $new_name   = $random_no.'.'.$ext;
    		    $destination= StudentProfileBasePath.'/'.$new_name;

    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
    		        move_uploaded_file($tmp_name, $destination);
    		        $student->image = $new_name;
    		    }else{
    		        return redirect()->back()->with('error','Invalid image extension');
    		    }
    		}
    		if($student->save()){
                $user_course                  = new UserCourse;
                $user_course->user_id         = $student->id;
                $user_course->category_id     = $data['category_id'];
                $user_course->sub_category_id = $data['sub_category_id'];
                if($user_course->save()){
				    return redirect('/admin/students')->with('success','Student added successfully');
                }else{
                    return redirect()->with('error',COMMON_ERROR);
                }
			}else{
				return redirect()->with('error',COMMON_ERROR);
			}
    	}		
    	$countries 		 = Country::select('*')->get()->toArray();
    	$domains         = Category::select('*')->whereNull('deleted_at')->get()->toArray();
    	$subjects        = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
    	$page = 'student_management';
    	return view('backEnd.studentManagement.form',compact('page','countries','subjects','domains'));
    }

    public function edit(Request $request,$student_id){

    	$student_details = User::where('id',$student_id)
                                ->with('user_courses')
    							->first();
    							// dd($student_details->gender);die;
    	if($request->isMethod('post')){

    		$data  				= $request->input();
    		// dd($data);
    		$student_details->first_name 			= $data['first_name'];
    		$student_details->last_name 			= $data['last_name'];
    		$student_details->email 				= $data['email'];
    		$student_details->contact 				= $data['contact'];
    		$student_details->dob 					= date('Y-m-d',strtotime($data['dob']));
    		$student_details->gender 				= $data['gender'];
    		$student_details->country_id 			= $data['country_id'];
    		$student_details->state_id 			    = $data['state_id'];
    		$student_details->city_id 				= $data['city_id'];
    		$student_details->exam_preparation_id 	= $data['sub_category_id'];
            $student_details->status                = $data['status'];
    		$student_details->added_by  			= 'admin';
    		$student_details->user_type 			= 'user';
    		$student_details->verified_account      = 'yes';
            $student_details->district              = $data['district'];
            $student_details->address               = $data['address'];
    		if(!empty($_FILES['image']['name'])){
    		    $image      = pathinfo($_FILES['image']['name']);
    		    $ext        = $image['extension'];
    		    $tmp_name   = $_FILES['image']['tmp_name'];
    		    $random_no  = uniqid();
    		    $new_name   = $random_no.'.'.$ext;
    		    $destination= StudentProfileBasePath.'/'.$new_name;

    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
    		    	if (file_exists($destination.'/'.$student_details->image)){
    		    	    unlink($destination.'/'.$student_details->image);
    		    	}
    		        move_uploaded_file($tmp_name, $destination);
    		        $student_details->image = $new_name;
    		    }else{
    		        return redirect()->back()->with('error','Invalid image extension');
    		    }
    		}
    		if($student_details->save()){

                $update  = UserCourse::where('user_id',$student_id)->update(['category_id'=>$data['category_id'],'sub_category_id'=>$data['sub_category_id']]);
				return redirect()->back()->with('success','Student edited successfully');
			}else{
				return redirect()->with('error',COMMON_ERROR);
			}
    	}		
    	$countries 		 = Country::select('*')->get()->toArray();
    	$domains         = Category::select('*')->whereNull('deleted_at')->get()->toArray();
    	$subjects        = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
    	$states = '';
    	$cities = '';
    	if(!empty($student_details->country_id)){

    		$states          = State::where('country_id',$student_details->country_id)->get()->toArray();
    		$cities          = City::where('state_id',$student_details->state_id)->get()->toArray();
    	}

        $sub_categories = '';
        if(!empty(@$student_details['user_courses'])){
            $sub_categories = SubCategory::where('category_id',$student_details['user_courses']['category_id'])
                                          ->whereNull('deleted_at')
                                          ->orderBy('name','asc')
                                          ->get()
                                          ->toArray();   
        }  
    	$page = 'student_management';
    	return view('backEnd.studentManagement.form',compact('page','student_details','countries','subjects','domains','student_id','states','cities','sub_categories'));
    }

    public function delete($student_id){

    	$del = User::where('id',$student_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
    	if ($del) {
    	    return redirect()->back()->with('success','Student deleted successfully');
    	}else{
    	    return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }

    public function send_credentials_mail($student_id){
        
        $mail =  User::sendCredentialMail($student_id);        
        if ($mail == true) {
            return redirect()->back()->with('success','Credential mail has been sent successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }

    public function validate_email(Request $request){

		$data = $request->all();
    
        if($data['student_id']==null){
            $check_email = User::where('email',$data['email'])
                                ->whereNull('deleted_at')
                                ->count(); 
        }else{
            $check_email = User::where('email',$data['email'])
                                ->where('id','<>',$data['student_id'])
                                ->whereNull('deleted_at')
                                ->count();
        }
        if ($check_email > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

	public function validate_contact(Request $request){

		$data = $request->all();
    
        if($data['student_id']==null){
            $check_email = User::where('contact',$data['contact'])
                                ->whereNull('deleted_at')
                                ->count(); 
        }else{
            $check_email = User::where('contact',$data['contact'])
                                ->where('id','<>',$data['student_id'])
                                ->whereNull('deleted_at')
                                ->count();
        }
        if ($check_email > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

    public function export(Request $request){

        if($request->isMethod('post')){
            // dd('enter');
            $data = $request->input();
            $extension = '';    
            $students = User::select('users.*','c.name as country_name','s.name as state_name','city.name as city_name','td.name as preperation_name')
                            ->join('countries as c','c.id','users.country_id')
                            ->join('states as s','s.id','users.state_id')
                            ->join('cities as city','city.id','users.city_id')
                            ->join('trainer_domains as td','td.id','users.exam_preparation_id')
                            ->where([
                                'user_type'        =>'user',
                                'td.deleted_at'    =>null,
                                'users.deleted_at' =>null
                            ]);
            if(!empty($data['pincode'])){
                $students = $students->where('pincode',$data['pincode']);  
            }
            if(!empty($data['contact'])){
                $students = $students->where('contact',$data['contact']); 
            }
            if(!empty($data['email_id'])){
                $students = $students->where('users.id',$data['email_id']); 
            }
            if(!empty($data['date'])){
                $date = date('Y-m-d',strtotime($data['date']));
                $students = $students->whereDate('users.created_at',$date); 
            }
            if(!empty($data['student_id'])){
                // dd($data['student_id']);
                $students = $students->where('users.id',$data['student_id']); 
            }
            if(!empty($data['country_id'])){

                $students = $students->where('users.country_id',$data['country_id']); 
            }
            if(!empty($data['state_id'])){
                $students = $students->where('users.state_id',$data['state_id']); 
            }
            if(!empty($data['city_id'])){
                $students = $students->where('users.city_id',$data['city_id']); 
            }
            if(!empty($data['domain_id'])){
                $students = $students->where('users.exam_preparation_id',$data['domain_id']); 
            }
            // dd($students->count());
            $students = $students->get()
                                  ->toArray();                   
            // if()
            $extension = $data['export_type'];
                                  // dd($data);
            if($extension=='pdf'){

                include('vendor/tcpdf/tcpdf.php');                     
                $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $tcpdf->AddPage('M','A4');
                $tcpdf->SetCreator(PDF_CREATOR);
                $title ='Scamper Skills';
                // $subtitle='Krishikulture';
                $tcpdf->SetTitle('Student-List');
                $tcpdf->SetSubject('Student-List');
                $tcpdf->setPrintHeader(true);
                $tcpdf->setPrintFooter(false);
                $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $tcpdf->SetMargins(10, 10, 10, true);
                $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $tcpdf->SetFont('helvetica', '', 9);
                $tcpdf->setFontSubsetting(false);
                $tcpdf->setFontSubsetting(false);
          
                    $html =<<<EOD
                        <html>
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <title>Flags</title>
                                <style>
                                    .th{
                                        font-weight:bold;
                                        background-color:#dfdfdf;    
                                    }
                                    img{
                                        width:200px;
                                        height:auto;
                                    }
                                    .result_heading th{
                                        font-size:12px;                            
                                    }
                                      .result_detail td{
                                        font-size:10px;                            
                                    }
                                </style>
                            </head>
                            <body style="font-family:helvetica; background:#fff; margin:0; padding:20px;"> 
                                <table style="width:100%; 
                                 background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
                                                <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skill"/>
                                            </th>
                                        </tr>
                                </table>    
                                  
                                <table border="1" style="width:100%; font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
                                    <thead style="background:#dfdfdf;">
                                        <tr class="result_heading">
                                            <th class="th">Student Name</th>
                                            <th class="th">Email</th>
                                            <th class="th">Contact Number</th>
                                            <th class="th">Date Of Birth</th>
                                            <th class="th">Gender</th>
                                            <th class="th">Country Name</th>
                                            <th class="th">State Name</th>
                                            <th class="th">City Name</th>
                                            <th class="th">Exam Preparation Name</th>
                                            <th class="th">Added By</th>
                                            <th class="th">Status</th>
                                            <th class="th">Joined On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
EOD;

                foreach($students as $student){
                    

                  
                    if($student['status']=='A'){
                        $student['status']           = 'Active';
                    }else{
                        $student['status']           = 'Inactive';
                    }
                    $student['student_name']     = ucfirst($student['first_name']).' '.ucfirst($student['last_name']);
                    $student['dob']              = date('d-m-Y',strtotime($student['dob']));
                    $student['country']          = ucfirst($student['country_name']);
                    $student['state_name']       = ucfirst($student['state_name']);
                    $student['city']             = ucfirst($student['city_name']);
                    $student['preperation_name'] = ucfirst($student['preperation_name']);
                    $student['added_by']         = ucfirst($student['added_by']);
                    $student['joined_on']        = date('d-m-Y',strtotime($student['created_at']));

                    $html .=<<<EOD
                    <tr class="result_detail">
                        <td>{$student['student_name']}</td>
                        <td>{$student['email']}</td>
                        <td>{$student['contact']}</td>
                        <td>{$student['dob']}</td>
                        <td>{$student['gender']}</td>
                        <td>{$student['country']}</td>
                        <td>{$student['state_name']}</td>
                        <td>{$student['city']}</td>
                        <td>{$student['preperation_name']}</td>
                        <td>{$student['added_by']}</td>
                        <td>{$student['status']}</td>
                        <td>{$student['joined_on']}</td>
                    </tr>
EOD;
                }
                $html .=<<<EOD
                            </tbody>
                        </table>
                    </body>    
                </html> 
EOD;
                            
                            // $pdf = PDF::loadHtml($html);
                $tcpdf->writeHTML($html, true, false, true, false, '');
                $url = public_path();
                $save=$tcpdf->Output('Student-List.pdf', 'I');
                $tcpdf->Output('Student-List.pdf','D');
                return Redirect::back();
            }
                                // echo'<pre>'; print_r($students);die;
            $student_detail   = [];
            $student_detail[] = ['Student Name','Email','Contact Number','Date Of Birth','Gender','Country Name','State Name','City Name','Exam Preparation Name','Added By','Status','Joined On'];

            $detail = [];
            foreach ($students as $key => $value){
         
                $detail['student_name']     = ucfirst($value['first_name']).' '.ucfirst($value['last_name']);
                $detail['email']            = $value['email'];
                $detail['contact']          = $value['contact'];
                $detail['dob']              = date('d-m-Y',strtotime($value['dob']));
                $detail['gender']           = ucfirst($value['gender']);
                $detail['country']          = ucfirst($value['country_name']);
                $detail['state_name']       = ucfirst($value['state_name']);
                $detail['city']             = ucfirst($value['city_name']);
                $detail['preperation_name'] = ucfirst($value['preperation_name']);
                $detail['added_by']         = ucfirst($value['added_by']);
                if($value['status']=='A'){
                    $detail['status']           = 'Active';
                }else{
                    $detail['status']           = 'Inactive';
                }
                $detail['joined_on']        = date('d-m-Y',strtotime($value['created_at']));
                $student_detail[]           = $detail;
            }

            // if(isset()){
            //    $extension = 'xls';

            // }else{
            //    $extension = 'csv';
            // }
            Excel::create('Student-List',function($excel)use ($student_detail){
                $excel->setTitle('Student-List');
                $excel->setCreator('Laravel')->setCompany('Scamper Skills');
                $excel->setDescription('Student File');
                $excel->sheet('sheet1',function($sheet) use ($student_detail){
                $sheet->fromArray($student_detail, null, 'A1', false, false);
                });
            })->download($extension);  
        }
        

    }

    public function subscriptions(Request $request,$student_id){

        $subscriptions = Transaction::select('transactions.*','u.first_name','u.last_name')
                                      ->with('subscription_plan.trainer_detail')
                                      ->join('users as u','u.id','transactions.user_id')
                                      // ->join('subscription_plans as s','s.id','transactions.subscription_plan_id')
                                      ->where('transactions.user_id',$student_id)
                                      ->where('transactions.subscription_plan_id','<>','0');
                                      
        if($request->isMethod('post')){
            // dd('enter');
            $data = $request->input();
            $details = [];
            if(!empty($data['date'])){
                $date = date('Y-m-d',strtotime($data['date']));
                $subscriptions = $subscriptions->where('transactions.purchased_on',$date); 
            }
            // if()
            $subscriptions = $subscriptions->get()
                                           ->toArray();
            $extension = $data['export_type'];
                                  // dd($data);
            if($extension=='pdf'){

                include('vendor/tcpdf/tcpdf.php');                     
                $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $tcpdf->AddPage('M','A4');
                $tcpdf->SetCreator(PDF_CREATOR);
                $title ='Scamper Skills';
                // $subtitle='Krishikulture';
                $tcpdf->SetTitle('Student-Subscriptions');
                $tcpdf->SetSubject('Student-Subscriptions');
                $tcpdf->setPrintHeader(true);
                $tcpdf->setPrintFooter(false);
                $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $tcpdf->SetMargins(10, 10, 10, true);
                $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $tcpdf->SetFont('helvetica', '', 9);
                $tcpdf->setFontSubsetting(false);
                $tcpdf->setFontSubsetting(false);
          
                    $html =<<<EOD
                        <html>
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <title>Flags</title>
                                <style>
                                    .th{
                                        font-weight:bold;
                                        background-color:#dfdfdf;    
                                    }
                                    img{
                                        width:200px;
                                        height:auto;
                                    }
                                    .result_heading th{
                                        font-size:12px;                            
                                    }
                                      .result_detail td{
                                        font-size:10px;                            
                                    }
                                </style>
                            </head>
                            <body style="font-family:helvetica; background:#fff; margin:0; padding:20px;"> 
                                <table style="width:100%; 
                                 background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
                                                <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skill"/>
                                            </th>
                                        </tr>
                                </table>    
                                  
                                <table border="1" style="width:100%; font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
                                    <thead style="background:#dfdfdf;">
                                        <tr class="result_heading">
                                            <th class="th">Subscription Plan Title</th>
                                            <th class="th">Teacher Name</th>
                                            <th class="th">Purchased On</th>
                                            <th class="th">Valid Till</th>
                                            <th class="th">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
EOD;

                
                foreach($subscriptions as $detail){
                 
                  
                    
                    $details['title']            = ucfirst($detail['subscription_plan']['title']);
                    $details['teacher_name']     = ucfirst($detail['subscription_plan']['trainer_detail']['first_name']).' '.ucfirst($detail['subscription_plan']['trainer_detail']['last_name']);
                    $details['purchased_on']     = date('d-m-Y',strtotime($detail['purchased_on']));
                    $details['valid_till']       = date('d-m-Y',strtotime($detail['valid_till']));
                    $today = date('d-m-Y');
                    $expiry_date = date('d-m-Y',strtotime($detail['valid_till']));
                    if($today>$expiry_date){
                        
                        $details['status']             = 'Active';                             
                    }else{

                        $details['status']             = 'Expired';
                    }
                    $html .=<<<EOD
                    <tr class="result_detail">
                        <td>{$details['title']}</td>
                        <td>{$details['teacher_name']}</td>
                        <td>{$details['purchased_on']}</td>
                        <td>{$details['valid_till']}</td>
                        <td>{$details['status']}</td>
                    </tr>
EOD;
                }
                $html .=<<<EOD
                            </tbody>
                        </table>
                    </body>    
                </html> 
EOD;
                            
                            // $pdf = PDF::loadHtml($html);
                $tcpdf->writeHTML($html, true, false, true, false, '');
                $url = public_path();
                $save=$tcpdf->Output('Student-Subscriptions-List.pdf', 'I');
                $tcpdf->Output('Student-Subscriptions-List.pdf','D');
                return Redirect::back();
            }
                                // echo'<pre>'; print_r($students);die;

            $student_detail   = [];
            $student_detail[] = ['Subscription Plan Title','Teacher Name','Purchased On','Valid Till','Status'];

            $details = [];

            foreach ($subscriptions as $key => $detail){
         
                $details['title']            = ucfirst($detail['subscription_plan']['title']);
                $details['teacher_name']     = ucfirst($detail['subscription_plan']['trainer_detail']['first_name']).' '.ucfirst($detail['subscription_plan']['trainer_detail']['last_name']);
                $details['purchased_on']     = date('d-m-Y',strtotime($detail['purchased_on']));
                $details['valid_till']       = date('d-m-Y',strtotime($detail['valid_till']));
                $today = date('d-m-Y');
                $expiry_date = date('d-m-Y',strtotime($detail['valid_till']));
                if($today>$expiry_date){
                     
                    $details['status']             = 'Active';                             
                }else{

                    $details['status']             = 'Expired';
                }
                $student_detail[]           = $details;
            }

            // if(isset()){
            //    $extension = 'xls';

            // }else{
            //    $extension = 'csv';
            // }
            Excel::create('Student-Subscriptions-List',function($excel)use ($student_detail){
                $excel->setTitle('Student-Subscriptions-List');
                $excel->setCreator('Laravel')->setCompany('Scamper Skills');
                $excel->setDescription('Student-Subscriptions-List');
                $excel->sheet('sheet1',function($sheet) use ($student_detail){
                $sheet->fromArray($student_detail, null, 'A1', false, false);
                });
            })->download($extension); 
        }
        // echo'<pre>'; print_r($subscriptions);die;
        $subscriptions = $subscriptions->get()
                                       ->toArray();
        $page = 'student_management';
        return view('backEnd.studentManagement.subscriptions',compact('page','subscriptions','student_id'));
    }


}
