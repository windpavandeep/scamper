<?php

namespace App\Http\Controllers\backEnd\examManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Set;
class SetController extends Controller
{
    public function index(){
       		
        $details = Set::whereNull('deleted_at')	
                        ->get()
                        ->toArray(); 
       	$page = 'sets';
       	return view('backEnd.examManagement.sets.index',compact('page','details'));
        }

    public function add(Request $request){

   	  	if($request->isMethod("post")){

    		$data            = $request->all();
            $set             = new Set;
            $set->name       = $data['name'];
            if($set->save()){

				return redirect('admin/sets')->with('success','Set added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
        $page = 'sets';
   		return view('backEnd.examManagement.sets.form',compact('page'));
    }

    public function edit(Request $request,$set_id){

        $set = Set::where('id',$set_id)->first();
        if($request->isMethod("post")){

            $data       = $request->all();
            $set->name = $data['name'];
            if($set->save()){

                return redirect()->back()->with('success','Set edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }

        $page = 'sets';
        return view('backEnd.examManagement.sets.form',compact('page','set_id','set'));
    }

    public function validate_name(Request $request){

        $data = $request->all();

        if($data['set_id']==null){
            $check_name = Set::where('name',$data['name'])
                              ->whereNull('deleted_at')
                              ->count();
        }else{
            $check_name = Set::where('name',$data['name'])
                              ->where('id','<>',$data['set_id'])
                              ->whereNull('deleted_at')
                              ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

   	
    public function delete($set_id){

        $del = Set::where('id',$set_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Set deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
