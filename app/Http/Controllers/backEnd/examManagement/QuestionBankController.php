<?php

namespace App\Http\Controllers\backEnd\examManagement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\QuestionBank,App\QuestionBankOption,App\Category,App\Subject,App\Unit,App\SubCategory;
use App\SubSubCategory,App\Language;
class QuestionBankController extends Controller
{

	public function question_bank(){

		$question_banks = QuestionBank::select('c.name as category_name','sc.id','sc.name as sub_category_name')
									  ->join('categories as c','c.id','question_banks.category_id')
									  ->join('sub_categories as sc','sc.id','question_banks.sub_category_id')
									  ->join('units','units.id','question_banks.unit_id')
									  ->whereNull('question_banks.deleted_at')
									  ->whereNull('c.deleted_at')
									  ->whereNull('sc.deleted_at')
									  ->groupBy('category_name','sc.id','sub_category_name')
									  ->get()
									  ->toArray();
		// echo'<pre>'; print_r($question_banks); die;
		// $question_banks = array_unique($question_banks);
		// $question_banks = $question_banks->Unique('category_id')->toArray();
		$page = 'question_banks';
		return view('backEnd.examManagement.questionBank.question_banks',compact('page','question_banks'));
	}

	public function index(Request $request, $sub_category_id){

		$paginate     = QuestionBank::select('question_banks.*','s.name as subject_name','u.name as unit_name','c.name as category_name','sc.name as sub_category_name')
									  ->with('question_bank_options','sub_sub_category')
									  ->join('sub_categories as sc','sc.id','question_banks.sub_category_id')
									  ->join('categories as c','c.id','question_banks.category_id')
									  ->join('subjects as s','s.id','question_banks.subject_id')
									  ->join('units as u','u.id','question_banks.unit_id')
									  ->where('question_banks.sub_category_id',$sub_category_id)
									  ->whereNull('s.deleted_at')
									  ->whereNull('u.deleted_at')
									  ->whereNull('question_banks.deleted_at')
									  ->orderBy('question_banks.id','Desc');

		
		$subject_id  ='';
		$unit_id     ='';
		$units       = '';
		$unit_ids      = '';

		$subject_ids   = array_map(function($query){ return $query= $query['subject_id'];} , $paginate->get()->toArray());

		$subjects      = Subject::whereIn('id',$subject_ids)
								 ->whereNull('deleted_at')
								 ->get()
								 ->toArray();

		$unit_ids     = array_map(function($query){ return $query= $query['unit_id'];} , $paginate->get()->toArray());
		if(!empty($subjects)){

			$subject_ids   = array_map(function($query){ return $query= $query['id'];} , $subjects);
		}							 
		
		
		if($request->isMethod('get')){

			
			// dd($unit_id)				  
			if(!empty(@$_GET['unit_id'])){
				
				$unit_id  	= @$_GET['unit_id'];	
				$paginate = $paginate->where('question_banks.unit_id',$unit_id);

			}
			if(@!empty(@$_GET['subject_id'])){
				
				$subject_id	= @$_GET['subject_id'];
				$paginate = $paginate->where('question_banks.subject_id',$subject_id);
				$units = Unit::whereNull('deleted_at')->whereIn('id',$unit_ids)->where('subject_id',$subject_id)->get()->toArray();
			}

			if(!empty($subject_id) && !empty($unit_id)){

				$paginate = $paginate->where('question_banks.subject_id',$subject_id)
									 ->where('question_banks.unit_id',$unit_id);
			}
		}
									  // echo'<pre>'; print_r($sub_category_id); die;
									  // dd('enter');

		$paginate      = $paginate->paginate(20);
		$question_bank = $paginate->toArray();
		$question_bank = $question_bank['data'];

		//dd($question_bank);
		
		$page = 'question_banks';
		return view('backEnd.examManagement.questionBank.index',compact('page','question_bank','paginate','subjects','units','sub_category_id','subject_id','unit_id'));
		
	}
    public function add_question_bank(Request $request){

    	if($request->isMethod('post')){
    		// echo "<pre>"; print_r($request->input()); die;
    		// echo "<pre>"; print_r($_FILES); die;
    		$data = $request->all();
    		foreach ($data['question'] as $key => $value) {
    			
	    		$question_bank 						= New QuestionBank;
	    		$question_bank->category_id 		= $request->category_id;
	    		$question_bank->sub_category_id 	= $request->sub_category_id;
	    		$question_bank->sub_sub_category_id	= $request->sub_sub_category_id;
	    		$question_bank->subject_id 			= $request->subject_id;
	    		$question_bank->unit_id 			= $request->unit_id;
	    		$question_bank->question 			= $value['ques'];
	    		$question_bank->marks 				= $value['marks'];
	    		$question_bank->answer_explanation 	= $value['ans_explanation'];
	    		$question_bank->correct_answer 		= $value['answer'];

	    		if(!empty($_FILES['question']['name'][$key]['ques_image'])){
	    		    $image      = pathinfo($_FILES['question']['name'][$key]['ques_image']);
	    		    $ext        = strtolower($image['extension']);
	    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ques_image'];
	    		    $random_no  = uniqid();
	    		    $new_name   = $random_no.'.'.$ext;
	    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

	    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
	    		        move_uploaded_file($tmp_name, $destination);
	    		        $question_bank->ques_image = $new_name;
	    		    }
	    		}
	    		if(!empty($_FILES['question']['name'][$key]['ans_explanation_image'])){
	    		    $image      = pathinfo($_FILES['question']['name'][$key]['ans_explanation_image']);
	    		    $ext        = strtolower($image['extension']);
	    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ans_explanation_image'];
	    		    $random_no  = uniqid();
	    		    $new_name   = $random_no.'.'.$ext;
	    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

	    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
	    		        move_uploaded_file($tmp_name, $destination);
	    		        $question_bank->ans_image = $new_name;
	    		    }
	    		}


	    		if($question_bank->save()){
	    			for ($i=0; $i < 4; $i++) { 
	    				
	    				$option 				  = new QuestionBankOption;
	    				$option->question_bank_id = $question_bank->id;
	    				$option->options 		  = $value['option'][$i];
	    				$option->position 		  = $i+1;
	    				

	    				if(!empty($_FILES['question']['name'][$key]['option_image'][$i])){
			    		    $image      = pathinfo($_FILES['question']['name'][$key]['option_image'][$i]);
			    		    $ext        = strtolower($image['extension']);
			    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['option_image'][$i];
			    		    $random_no  = uniqid();
			    		    $new_name   = $random_no.'.'.$ext;
			    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

			    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
			    		        move_uploaded_file($tmp_name, $destination);
			    		        $option->option_image = $new_name;
			    		    }
			    		}
			    		$option->save();
	    			}
	    		}
    		}
    		return redirect('admin/question-banks/'.$request->sub_category_id)->with('success','Questions added successfully');
		}
    	$categories = Category::whereNull('deleted_at')->get()->toArray();
    	$subjects    = Subject::whereNull('deleted_at')->get()->toArray();

    	$page 		= 'question_banks';	
    	return view('backEnd.examManagement.questionBank.add_question_bank',compact('page','categories','subjects'));
    }

    public function edit_question(Request $request, $question_id){
    	// echo "22"; die;
		// echo $question_id; die;

    	$question_bank = QuestionBank::with('question_bank_options')
    								  ->whereNull('deleted_at')
    								  ->where('id',$question_id)
    								  ->first();

    		// echo "<pre>"; print_r($question_id); die;
    	if(!empty($question_bank)){
    		$question_bank = $question_bank->toArray();
    	}

    	if($request->isMethod('post')) {
    		$data = $request->all();
    		$question = QuestionBank::with('question_bank_options')
								  	 ->where('id',$question_id)
								  	 ->whereNull('deleted_at')
								     ->first();
    								  // echo "<pre>"; print_r($question); die;
    			
    		if(!empty($question)){
	    		foreach ($data['question'] as $key => $value) {
	    			$question->category_id 			= $data['category_id'];
	    			$question->sub_category_id 		= $data['sub_category_id'];
	    			$question->sub_sub_category_id	= @$data['sub_sub_category_id'];
	    			$question->subject_id 			= $data['subject_id'];
	    			$question->unit_id 				= $data['unit_id'];
		    		$question->question 			= $value['ques'];
		    		$question->marks 				= $value['marks'];
		    		$question->answer_explanation 	= $value['ans_explanation'];
		    		$question->correct_answer 		= $value['answer'];

		    		if(!empty($_FILES['question']['name'][$key]['ques_image'])){
		    		    $image      = pathinfo($_FILES['question']['name'][$key]['ques_image']);
		    		    $ext        = strtolower($image['extension']);
		    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ques_image'];
		    		    $random_no  = uniqid();
		    		    $new_name   = $random_no.'.'.$ext;
		    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

		    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
		    		    	if (file_exists($destination.'/'.$question->ques_image)){
		    		    	    unlink($destination.'/'.$question->ques_image);
		    		    	}
		    		        move_uploaded_file($tmp_name, $destination);
		    		        $question->ques_image = $new_name;
		    		    }
		    		}
		    		if(!empty($_FILES['question']['name'][$key]['ans_explanation_image'])){
		    		    $image      = pathinfo($_FILES['question']['name'][$key]['ans_explanation_image']);
		    		    $ext        = strtolower($image['extension']);
		    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ans_explanation_image'];
		    		    $random_no  = uniqid();
		    		    $new_name   = $random_no.'.'.$ext;
		    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

		    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
		    		    	if (file_exists($destination.'/'.$question->ans_image)){
		    		    	    unlink($destination.'/'.$question->ans_image);
		    		    	}
		    		        move_uploaded_file($tmp_name, $destination);
		    		        $question->ans_image = $new_name;
		    		    }
		    		}


		    		if($question->save()){
		    			foreach ($value['option'] as $key1 => $value1) {
		    			// echo "<pre>"; print_r($value1); die;
		    				# code...
		    				$option = QuestionBankOption::where('question_bank_id',$question_id)->where('position',$key1+1)->first();
		    				if(!empty($option)){

		    					$option->options = $value1;
			    				

			    				if(!empty($_FILES['question']['name'][$key]['option_image'][$key1])){
					    		    $image      = pathinfo($_FILES['question']['name'][$key]['option_image'][$key1]);
					    		    $ext        = strtolower($image['extension']);
					    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['option_image'][$key1];
					    		    $random_no  = uniqid();
					    		    $new_name   = $random_no.'.'.$ext;
					    		    $destination= QuestionBankImageBasePath.'/'.$new_name;

					    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					    		    	if (file_exists($destination.'/'.$option->option_image)){
					    		    	    unlink($destination.'/'.$option->option_image);
					    		    	}
					    		        move_uploaded_file($tmp_name, $destination);
					    		        $option->option_image = $new_name;
					    		    }
					    		}
					    		$option->save();
		    				}
		    			}
		    		}
	    		}
	    		return redirect()->back()->with('success','Question edited successfully');
    		}
    	}
    	$categories  = Category::whereNull('deleted_at')->get()->toArray();
    	$subjects    = Subject::where('sub_sub_category_id',$question_bank['sub_sub_category_id'])->whereNull('deleted_at')->get()->toArray();
    	$subcategories = '';
    	$sub_sub_categories ='';
    	$units              = '';
    	if(!empty($question_bank)){
    		$subcategories = SubCategory::where('category_id',$question_bank['category_id'])
    									 ->whereNull('deleted_at')
    									 ->get()
    									 ->toArray();
    		$sub_sub_categories = SubSubCategory::where('sub_category_id',$question_bank['sub_category_id'])
	    									 ->whereNull('deleted_at')
	    									 ->get()
	    									 ->toArray();
    		$units = Unit::where('subject_id',$question_bank['subject_id'])
					 	  ->whereNull('deleted_at')
					 	  ->get()
					 	  ->toArray();
    	}
    	$page = 'question_banks';
    	if(!empty($question_bank)){

    		return view('backEnd.examManagement.questionBank.edit_question',compact('page','question_bank','question_id','categories','subjects','subcategories','sub_sub_categories','units'));
    	}
    }

    public function validate_edit_question(Request $request, $question_id){

        $data = $request->all();
		

        $question_detail = QuestionBank::where('id',$question_id)
        								->with('question_bank_options')
        								->first();
       	if(!empty($question_detail)){
       		$question_detail = $question_detail->toArray();
       	}
       	// echo "<pre>"; print_r($question_detail); die;

        foreach ($data['question'] as $key => $value) {
        	
            if(empty($data['ques_text']) || empty($value['marks']) || empty($data['ans_text'])){
            	
            	if( (empty($_FILES['question']['name'][$key]['ques_image'])) || (empty($_FILES['question']['name'][$key]['ans_explanation_image'])) ){
            		
            		if( (empty($question_detail['ques_image'])) && (empty($data['ques_text'])) || (empty($question_detail['ans_image'])) && (empty($data['ans_text']))  ){
            			
            			
                		return json_encode(false);
            		}
            		return json_encode(false);
            	}
            	return json_encode(false);
            }
            
            for ($i=0; $i < 4; $i++) { 
            	if(empty($value['option'][$i])){
            		if(empty($_FILES['question']['name'][$key]['option_image'][$i])){
	            		if(empty($question_detail['question_bank_options'][$i]['option_image'])){

            				return json_encode(false);
	            		}
            		}
            	}

            }
        }

        return json_encode(true);
    }

   	public function delete_question($question_id){
   		// echo "string";die;
    	$del = QuestionBank::where('id',$question_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
    	if ($del) {
    	    return redirect()->back()->with('success','Question deleted successfully');
    	}else{
    	    return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }

    public function import_file(Request $request){


    	$languages = Language::where('deleted_at',null)->get()->toArray();

    	$subjects = Subject::where('deleted_at',null)->get()->toArray();

    	if($request->isMethod('post')){

            // echo "<pre>"; print_r($request->input()); die;

	        $category_id 		 = $request->category_id;
	        $sub_category_id 	 = $request->sub_category_id;
	        $sub_sub_category_id = $request->sub_sub_category_id;
	        $subject_id 		 = $request->subject_id;
	        $unit_id 			 = $request->unit_id;
	    	if ($request->hasFile('csv_file')) {
	           
	            $path = $request->file('csv_file')->getRealPath();
	            // echo "<pre>"; print_r($path); //die;
	            $data = \Excel::load($path)->get()->toArray();
	            // echo "<pre>"; print_r($data); die;

	            foreach ($data as $key => $value) {
	            	if((is_string($value['marks'])) || (is_string($value['correct_answer']))){
	            		return redirect()->back()->with('error','Marks or Correct Answer must contain digits');
	            	}
	            }

	            foreach ($data as $key => $value) {
		            $question 						= new QuestionBank;
		            $question->category_id		 	= $category_id;
		            $question->sub_category_id		= $sub_category_id;

		            if(!empty(@$sub_sub_category_id)){
		            	$question->sub_sub_category_id  = $sub_sub_category_id;
		            }
		            $question->subject_id		 	= $subject_id;
		            $question->unit_id		 		= $unit_id;
		            $question->question 			= $value['question'];
		            $question->marks 				= $value['marks'];
		            $question->correct_answer 		= $value['correct_answer'];
		            $question->answer_explanation 	= $value['answer_explanation'];
		            if($question->save()){

		    			for ($i=1; $i < 5; $i++) { 
		    				if($i == '1'){
		    					$opt = $value['optiona'];
		    				}elseif($i == '2'){
		    					$opt = $value['optionb'];
		    				}elseif($i == '3'){
		    					$opt = $value['optionc'];
		    				}elseif($i == '4'){
		    					$opt = $value['optiond'];
		    				}

			    			$option 				  = new QuestionBankOption;
		    				$option->question_bank_id = $question->id;
		    				$option->options 		  = $opt;
		    				$option->position 		  = $i;
		    				$option->save();
		    			}
		    		}
		        }
		        return redirect('admin/question-banks')->with('success','Questions added successfully.');	
		    }else{
		        return redirect()->back()->with('error',COMMON_ERROR);	
			}
    	}
    	$categories = Category::whereNull('deleted_at')->get()->toArray();
    	$subjects    = Subject::whereNull('deleted_at')->get()->toArray();
        $page = 'question_banks';
        return view('backEnd.examManagement.questionBank.import_file',compact('page','languages','subjects','units','exam_id','categories','subjects'));
    }


   	public function get_units($subject_id){

		$units = Unit::where('subject_id',$subject_id)
					  ->whereNull('deleted_at')
					  ->get()
					  ->toArray();
		$unit_options = '<option value="">Select Unit</option>';
		if(!empty($units)){

			foreach ($units as $key => $value) {
				$unit_options .=  "<option value=".$value['id'].">".ucfirst($value['name'])." </option> ";
			}
		
		}
		return $unit_options;
	}
}
