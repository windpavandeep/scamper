<?php

namespace App\Http\Controllers\backEnd\examManagement\allTestExams;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language, App\Question, App\Category, App\Exam, App\Option, App\Unit, App\Subject, App\SubCategory, App\SubSubCategory,App\Set;
use App\UserExam,App\Country,App\State;
use Excel,TCPDF;
use Carbon\Carbon;
class ExamController extends Controller
{
    
    public function index(){

    		

    	$exams = Exam::withCount('user_exams')
    				  ->whereHas('user_exams')
    				  ->get()
    				  ->toArray();

		$page = 'all_test_exam_result';
    	return view('backEnd.examManagement.allTestExams.index',compact('page','exams'));
    }

    public function details($exam_id){

		$user_exams = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time','total_attempt','u.first_name','u.last_name','u.email','u.contact','u.pincode')
						  ->join('users as u','u.id','user_exams.user_id')
						  ->join('exam as e','e.id','user_exams.exam_id')
						  ->where('user_exams.exam_id',$exam_id)
						  ->get()
						  ->toArray();
						  // dd($exam_id);
		$countries = Country::get()->toArray();
		$page = 'all_test_exam_result';
    	return view('backEnd.examManagement.allTestExams.student.index',compact('page','user_exams','exam_id','countries'));
    }

    public function student_exam_detail($exam_id){

		$user_exam = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time','total_attempt','u.first_name','u.last_name','u.email','u.contact','u.first_name','u.last_name','u.email','u.contact')
						  ->join('users as u','u.id','user_exams.user_id')
						  ->join('exam as e','e.id','user_exams.exam_id')
						  ->where('user_exams.id',$exam_id)
						  ->first();
						  // dd($user_exams);
		$page = 'all_test_exam_result';
    	return view('backEnd.examManagement.allTestExams.student.form',compact('page','user_exam','exam_id'));
    }

    public function export_students_exam_report(Request $request){

    		if($request->isMethod('post')){
    			// dd('enter');
                $data = $request->input();
                // echo'<pre>'; print_r($data);die;
    			$exam_id = $data['exam_id'];
    			// dd($data);
    			$export_type = $data['export_type'];

    			$user_exams = UserExam::select('user_exams.id','user_exams.exam_id','user_id','e.name','user_exams.total_marks','duration','user_exams.marks_obtained','user_exams.set_name','e.start_date','e.expiry_date','start_date_time','total_attempt','u.first_name','u.last_name','u.email','u.contact','u.state_id','u.country_id','u.city_id','u.id as user_table_id','u.pincode')
						  ->join('users as u','u.id','user_exams.user_id')
						  ->join('exam as e','e.id','user_exams.exam_id')
						  ->where('user_exams.exam_id',$exam_id);
						  
				if(!empty(@$data['country_id'])){
					$user_exams = $user_exams->where('u.country_id',@$data['country_id']);
				}
                if(!empty(@$data['state_id'])){
                    $user_exams = $user_exams->where('u.state_id',@$data['state_id']);
                }
                if(!empty(@$data['city_id'])){
                    $user_exams = $user_exams->where('u.city_id',@$data['city_id']);
                }
                if(!empty($data['student_id'])){
                    $user_exams = $user_exams->where('user_id',$data['student_id']);
                }
                if(!empty($data['pincode'])){
                    $user_exams = $user_exams->where('pincode',$data['pincode']);
                }
                if(!empty($data['date'])){
                    $createdAt = Carbon::parse($data['date'])->format('d-m-Y');
                    // dd($createdAt);
                    $user_exams = $user_exams->where('start_date',$createdAt)
                                             ->orWhere('expiry_date',$createdAt);
                }
				$user_exams = $user_exams->get()
						  				 ->toArray();
						  // dd($user_exams);
    			if($export_type=='excel'){
    			   $extension = 'xls';

    			}elseif($export_type=='pdf'){
    				$extension = 'pdf';
    			}else{
    			   $extension = 'csv';
    			}
    			if($extension=='pdf'){


                    include('vendor/tcpdf/tcpdf.php');                     
                    $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                    $tcpdf->AddPage('M','A4');
                    $tcpdf->SetCreator(PDF_CREATOR);
                    $title ='Scamper Skills';
                    // $subtitle='Krishikulture';
                    $tcpdf->SetTitle('All-Test-Exam-Report');
                    $tcpdf->SetSubject('All-Test-Exam-Report');
                    $tcpdf->setPrintHeader(true);
                    $tcpdf->setPrintFooter(false);
                    $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                    $tcpdf->SetMargins(10, 10, 10, true);
                    $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                    $tcpdf->SetFont('helvetica', '', 9);
                    $tcpdf->setFontSubsetting(false);
                    $tcpdf->setFontSubsetting(false);


    				$html =<<<EOD
                        <html>
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <meta name="viewport" content="width=device-width, initial-scale=1">
                                <title>Flags</title>
                                <style>
                                 
                                    img{
                                        width:200px;
                                        height:auto;
                                    }
                                   
                                </style>
                            </head>
                            <body style="font-family:helvetica; background:red; margin:0; padding:20px;"> 
                                <table style="width:100%; 
                                 background:green;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
                                        <tr>
                                            <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
                                                <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skill"/>
                                            </th>
                                        </tr>
                                </table>    
                                  
                                <table border="1" style="width:100%; font-weight: lighter; background:grey;border: 1px solid #ddd;text-align:center;" cellpadding="3" cellspacing="0">
                                    <thead style="background:pink;">
                                        <tr  class="result_heading">
                                        	<th class="th">Student ID</th>
                                            <th class="th">Student Name</th>
                                            <th class="th">Student Email</th>
                                            <th class="th">Student Contact Number</th>
                                            <th class="th">State</th>
                                            <th class="th">Exam Name</th>
                                            <th class="th">Marks Obtained</th>
                                            <th class="th">Total Marks</th>
                                            <th class="th">Total Question Attempt</th>
                                            <th class="th">Set Name</th>
                                            <th class="th">Duration</th>
                                            <th class="th">Exam Start Date</th>
                                            <th class="th">Exam End Date</th>
                                            <th class="th">Exam Given On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
EOD;

                                    foreach($user_exams as $exam){
                                        
                                       
                                        $exam['exam_start_date']        = date('d-m-Y',strtotime($exam['start_date']));
                                   	   	$exam['exam_end_date']        = date('d-m-Y',strtotime($exam['expiry_date']));
                                       	$exam['given_on']         	  = date('d-m-Y',strtotime($exam['start_date_time']));
                                       	$exam['duration']         	  = (int)(date('i',$exam['duration'] / 1000));
                                       	$state_name                   = State::where('id',$exam['state_id'])->value('name');
                                    
                                        $html .=<<<EOD
                                        <tr class="result_detail">
                                        	<td>{$exam['user_table_id']}</td>
                                            <td>{$exam['first_name']} {$exam['last_name']}</td>
                                            <td>{$exam['email']}</td>
                                            <td>{$exam['contact']}</td>
                                            <td>{$state_name}</td>
                                            <td>{$exam['name']}</td>
                                            <td>{$exam['marks_obtained']}</td>
                                            <td>{$exam['total_marks']}</td>
                                            <td>{$exam['total_attempt']}</td>
                                            <td>{$exam['set_name']}</td>
                                            <td>{$exam['duration']} minutes</td>
                                            <td>{$exam['exam_start_date']}</td>
                                            <td>{$exam['exam_end_date']}</td>
                                            
                                            <td>{$exam['given_on']}</td>
                                        </tr>
EOD;
                                    }
        $html .=<<<EOD
                                    </tbody>
                                </table>
                            </body>    
                        </html> 
EOD;
    					$tcpdf->writeHTML($html, true, false, true, false, '');
                        $url = public_path();
                        $save=$tcpdf->Output('All-Test-Exam-Report.pdf', 'I');
                        $tcpdf->Output('All-Test-Exam-Report.pdf','D');
                        return Redirect::back();
                    
    			}else{
    				
         
        			$student_detail   = [];
        			$student_detail[] = ['Student ID','Student Name','Student Email','Student Contact Number','State','Exam Name','Marks Obtained','Total Marks','Total Question Attempt','Duration','Set Name','Exam Start Date','Exam End Date','Exam Given On'];

        			$detail = [];
        			foreach ($user_exams as $key => $value){
        				$detail['user_table_id']          = $value['user_table_id'];
        				$state_name                       = State::where('id',$value['state_id'])->value('name');
        				$detail['first_name']             = ucfirst($value['first_name']).' '.ucfirst($value['last_name']);
        				$detail['email']             	  = ucfirst($value['email']);
        				$detail['contact']             	  = $value['contact'];
        				$detail['state_name']             = ucfirst($state_name);
        			    $detail['name']             	  = ucfirst($value['name']);
        			    $detail['marks_obtained']   	  = $value['marks_obtained'];
        			    $detail['total_marks']      	  = $value['total_marks'];
        			    $detail['total_question_attempt'] = $value['total_attempt'];
        			    $detail['set_name']        		  = $value['set_name'];
        			    $detail['duration']         	  = $value['duration'];
        		     	$detail['exam_start_date']        = date('d-m-Y',strtotime($value['start_date']));
        			   	$detail['exam_end_date']          = date('d-m-Y',strtotime($value['expiry_date']));
        			    $detail['given_on']         	  = date('d-m-Y',strtotime($value['start_date_time']));
        			   /* $detail['gender']           	  = ucfirst($value['gender']);
        			    $detail['country']          	  = ucfirst($value['country_name']);
        			    $detail['state_name']       	  = ucfirst($value['state_name']);
        			    $detail['city']             	  = ucfirst($value['city_name']);
        			    $detail['preperation_name'] 	  = ucfirst($value['preperation_name']);
        			    $detail['added_by']         	  = ucfirst($value['added_by']);
        			    $detail['status']           	  = ucfirst($value['status']);
        			    $detail['joined_on']        	  = date('d-m-Y',strtotime($value['created_at']));*/
        			    $student_detail[]           = $detail;
        			}
        			// dd($student_detail);
        			Excel::create('All-Test-Exam-Report',function($excel)use ($student_detail){
        			    $excel->setTitle('All-Test-Exam-Report');
        			    $excel->setCreator('Laravel')->setCompany('Scamper Skills');
        			    $excel->setDescription('All-Test-Exam-Report');
        			    $excel->sheet('sheet1',function($sheet) use ($student_detail){
        			    $sheet->fromArray($student_detail, null, 'A1', false, false);
        			    });
        			})->download($extension);  

        		}
        	}
    	}
}
