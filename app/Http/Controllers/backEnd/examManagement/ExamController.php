<?php

namespace App\Http\Controllers\backEnd\examManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language, App\Question, App\Category, App\Exam, App\Option, App\Unit, App\Subject, App\SubCategory, App\SubSubCategory,App\Set;
use App\QuestionSet,URL,App\User,App\Notification;
class ExamController extends Controller
{
    
    public function index(){

    	$exams = Exam::where('deleted_at',null)->get()->toArray();

		$page = 'exam_management';
    	return view('backEnd.examManagement.index',compact('page','exams'));
    }

    public function add(Request $request){

    	$categories = Category::where('deleted_at',null)->get()->toArray();
    	$languages = Language::where('deleted_at',null)->get()->toArray();

    	if($request->isMethod('post')){
    		// echo "<pre>"; print_r($request->input()); die;

    		$exam 							= new Exam;
    		$exam->language_id 				= $request->language_id;
    		$exam->categories_id 			= $request->category_id;
    		$exam->name 					= $request->name;
    		$exam->code 					= $request->code;
    		$exam->exam_validity			= $request->exam_validity;
    		$exam->start_date 				= !empty($request->start_date)?$request->start_date : NULL;
    		$exam->expiry_date 				= !empty($request->expiry_date)?$request->expiry_date : NULL;
    		$exam->start_time 				= !empty($request->start_time)?$request->start_time : NULL;
    		$exam->end_time 				= !empty($request->end_time)?$request->end_time : NULL;
    		$exam->total_questions 			= $request->total_questions;
    		$exam->total_marks 				= $request->total_marks;
    		$exam->negative_marking 		= $request->negative_marking;
    		$exam->trams_condition 			= $request->trams_condition;
    		$exam->sub_categories_id  		= !empty($request->sub_category_id)? $request->sub_category_id : NULL;
    		$exam->sub_sub_categories_id 	= !empty($request->sub_sub_category_id )? $request->sub_sub_category_id  : NULL;

    		if($exam->save()){
    			$product_id = '';
    			User::send_notification($exam->id,$product_id);
    			return redirect('admin/exams')->with('success','Exam added successfully');
    		}else{
    			return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
		$page = 'exam_management';
    	return view('backEnd.examManagement.form',compact('page','categories','languages'));
    }
    public function edit(Request $request, $exam_id = null){

    	$exam = Exam::where('id',$exam_id)
    				->where('deleted_at',null)
    				->with('sub_categories')
    				->with('sub_sub_categories')
    				->first();
    	if(!empty($exam)){
    		$exam = $exam->toArray();
    	}
    	// echo "<pre>"; print_r($exam); die;

    	$categories = Category::where('deleted_at',null)->get()->toArray();
    	$languages = Language::where('deleted_at',null)->get()->toArray();

    	if($request->isMethod('post')){
    		// echo "<pre>"; print_r($request->input()); die;
			$exam = Exam::where('id',$exam_id)
						->where('deleted_at',null)
						->first();
			if(!empty($exam)){

	    		$exam->language_id 				= $request->language_id;
	    		$exam->categories_id 			= $request->category_id;
	    		$exam->name 					= $request->name;
	    		$exam->code 					= $request->code;
	    		$exam->exam_validity			= $request->exam_validity;
	    		$exam->start_date 				= !empty($request->start_date)?$request->start_date : NULL;
	    		$exam->expiry_date 				= !empty($request->expiry_date)?$request->expiry_date : NULL;
	    		$exam->start_time 				= !empty($request->start_time)?$request->start_time : NULL;
    			$exam->end_time 				= !empty($request->end_time)?$request->end_time : NULL;
	    		$exam->total_questions 			= $request->total_questions;
	    		$exam->total_marks 				= $request->total_marks;
	    		$exam->negative_marking 		= $request->negative_marking;
	    		$exam->trams_condition 			= $request->trams_condition;
	    		$exam->sub_categories_id  		= !empty($request->sub_category_id)? $request->sub_category_id : NULL;
	    		$exam->sub_sub_categories_id 	= !empty($request->sub_sub_category_id )? $request->sub_sub_category_id  : NULL;

	    		if($exam->save()){
	    			return redirect()->back()->with('success','Exam edited successfully');
	    		}else{
	    			return redirect()->back()->with('error',COMMON_ERROR);
				}
			}else{
    			return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
    	$subcategories = '';
    	$sub_sub_categories = '';
    	if(!empty($exam)){

	    	$subcategories = SubCategory::where('category_id',$exam['categories_id'])
	    									 ->whereNull('deleted_at')
	    									 ->get()
	    									 ->toArray();
			$sub_sub_categories = SubSubCategory::where('sub_category_id',$exam['sub_categories_id'])
	    									 ->whereNull('deleted_at')
	    									 ->get()
	    									 ->toArray();
    	}
		$page = 'exam_management';
    	return view('backEnd.examManagement.form',compact('page','categories','languages','exam','exam_id','subcategories','sub_sub_categories'));
    }

    public function delete(Request $request, $exam_id = null){

    	$del = Exam::where('id',$exam_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);
    	if($del){
    		return redirect('admin/exams')->with('success','Exam deleted successfully');
		}else{
			return redirect()->back()->with('error',COMMON_ERROR);
		}
    }

    public function add_question(Request $request,$exam_id = null){

    	$exam = Exam::where('id',$exam_id)
    				->first();
    	/*if(!empty($exam)){
    		$units = Unit::where('category_id',$exam->categories_id)
    						->where('deleted_at',null)
    						->get()
    						->toArray();
    	}else{*/
    		$units[] = ''; 
    	// }
    	$subjects = Subject::where('sub_sub_category_id',$exam->sub_sub_categories_id)
    						->where('deleted_at',null)
    						->get()
    						->toArray();

    	if($request->isMethod('post')){
    		// echo "<pre>"; print_r($_FILES); die;
    		$data = $request->all();
    		foreach ($data['question'] as $key => $value) {
    			
	    		$question 						= New Question;
	    		$question->exam_id 				= $exam_id;
	    		$question->subject_id 			= $request->subject_id;
	    		$question->unit_id 				= $request->unit_id;
	    		$question->question 			= $value['ques'];
	    		$question->marks 				= $value['marks'];
	    		$question->answer_explanation 	= $value['ans_explanation'];
	    		$question->correct_answer 		= $value['answer'];

	    		if(!empty($_FILES['question']['name'][$key]['ques_image'])){
	    		    $image      = pathinfo($_FILES['question']['name'][$key]['ques_image']);
	    		    $ext        = $image['extension'];
	    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ques_image'];
	    		    $random_no  = uniqid();
	    		    $new_name   = $random_no.'.'.$ext;
	    		    $destination= ExamImageBasePath.'/'.$new_name;

	    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
	    		        move_uploaded_file($tmp_name, $destination);
	    		        $question->ques_image = $new_name;
	    		    }
	    		}
	    		if(!empty($_FILES['question']['name'][$key]['ans_explanation_image'])){
	    		    $image      = pathinfo($_FILES['question']['name'][$key]['ans_explanation_image']);
	    		    $ext        = $image['extension'];
	    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ans_explanation_image'];
	    		    $random_no  = uniqid();
	    		    $new_name   = $random_no.'.'.$ext;
	    		    $destination= ExamImageBasePath.'/'.$new_name;

	    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
	    		        move_uploaded_file($tmp_name, $destination);
	    		        $question->ans_image = $new_name;
	    		    }
	    		}


	    		if($question->save()){

	    			if(!empty($data['set_ids'])){
	    				foreach ($data['set_ids'] as $key => $set_id) {
	    					
		    				$set    		  = new QuestionSet;
		    				$set->exam_id     = $exam_id;
		    				$set->question_id = $question->id;
		    				$set->set_id      = $set_id;
		    				$set->save();  
	    				}
	    			}

	    			for ($i=0; $i < 4; $i++) { 
	    				
	    				$option 				= new Option;
	    				$option->question_id 	= $question->id;
	    				$option->options 		= $value['option'][$i];
	    				$option->position 		= $i+1;
	    				

	    				if(!empty($_FILES['question']['name'][$key]['option_image'][$i])){
			    		    $image      = pathinfo($_FILES['question']['name'][$key]['option_image'][$i]);
			    		    $ext        = $image['extension'];
			    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['option_image'][$i];
			    		    $random_no  = uniqid();
			    		    $new_name   = $random_no.'.'.$ext;
			    		    $destination= ExamImageBasePath.'/'.$new_name;

			    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
			    		        move_uploaded_file($tmp_name, $destination);
			    		        $option->option_image = $new_name;
			    		    }
			    		}
			    		$option->save();
	    			}
	    		}
    		}
    		return redirect('admin/exams')->with('success','Question added successfully');
		}
		$sets = Set::whereNull('deleted_at')->get()->toArray();
    	$page = 'exam_management';
    	return view('backEnd.examManagement.add_question',compact('page','exam_id','units','subjects','sets'));
    }
    public function question_paper($exam_id){

    	$paginate = Question::with('options','sets.set_name')
    						  ->whereHas('sets.set_name')	
    						  ->where('exam_id',$exam_id)
    						  ->whereNull('deleted_at');
    						  
    	$set_id = '';
    	if(!empty(@$_GET['set_id'])){
    		$set_id  = $_GET['set_id'];
    		$paginate->whereHas('sets',function($query)use($set_id){
    					$query->where('set_id',$set_id);
    				  });

    	}					  
    	$paginate  = $paginate->paginate(20);
    	$questions = $paginate->toArray();
    	$questions = $questions['data'];
    	// echo'<pre>'; print_r($questions); die; 
    	$sets = Set::whereNull('deleted_at')
    				->orderBy('id','Asc')
    		        ->get()
    		        ->toArray();
    	$page = 'exam_management';
    	return view('backEnd.examManagement.question_paper',compact('page','questions','exam_id','paginate','sets','set_id'));
    }

    public function delete_question($question_id){

    	$del = Question::where('id',$question_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);

    	if ($del) {
    	    return redirect()->back()->with('success','Question deleted successfully');
    	}else{
    	    return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }
   /* public function import_file(Request $request,$exam_id = null){

    	$languages = Language::where('deleted_at',null)->get()->toArray();

    	$exam = Exam::where('id',$exam_id)
    				->first();
    	if(!empty($exam)){
    		$units = Unit::where('category_id',$exam->categories_id)
    						->where('deleted_at',null)
    						->get()
    						->toArray();
    	}else{
    		$units[] = ''; 
    	}
    	$subjects = Subject::where('deleted_at',null)->get()->toArray();

    	if($request->isMethod('post')){

            // echo "<pre>"; print_r($request->input()); die;
	        $subject_id = $request->subject_id;
	        $unit_id 	= $request->unit_id;
	    	if ($request->hasFile('csv_file')) {
	           
	            $path = $request->file('csv_file')->getRealPath();
	            // echo "<pre>"; print_r($path); //die;
	            $data = \Excel::load($path)->get()->toArray();
	            // echo "<pre>"; print_r($data); die;

	            foreach ($data as $key => $value) {
	            	if((!is_int($value['marks'])) || (!is_int($value['correct_answer']))){
	            		return redirect()->back()->with('error','Marks or Correct Answer must contain digits');
	            	}
	            }

	            foreach ($data as $key => $value) {

	            	

		            $question 						= new Question;
		            $question->question 			= $value['question'];
		            $question->marks 				= $value['marks'];
		            $question->correct_answer 		= $value['correct_answer'];
		            $question->answer_explanation 	= $value['answer_explanation'];
		            $question->subject_id		 	= $subject_id;
		            $question->unit_id		 		= $unit_id;
		            $question->exam_id		 		= $exam_id;
		            if($question->save()){
		    			
		    			$option 				= new Option;
	    				$option->question_id 	= $question->id;
	    				$option->options 		= $value['option'];
	    				$option->save();
		    		}
		        }
		        return redirect('admin/exams')->with('success','Questions added successfully.');	
		    }else{
		        return redirect()->back()->with('error',COMMON_ERROR);	
			}
    	}

    	$page = 'exam_management';
    	return view('backEnd.examManagement.import_file',compact('page','languages','subjects','units','exam_id'));
    }*/
    public function import_file(Request $request,$exam_id = null){


    	$languages = Language::where('deleted_at',null)->get()->toArray();

    	$sub_sub_category_id = Exam::where('id',$exam_id)->value('sub_sub_categories_id');
    	if(!empty($exam)){
    		$units = Unit::where('subject_id',$exam->categories_id)
    						->where('deleted_at',null)
    						->get()
    						->toArray();
    	}else{
    		$units[] = ''; 
    	}
    	$subjects = Subject::where('sub_sub_category_id',$sub_sub_category_id)
    						->where('deleted_at',null)->get()->toArray();

    	if($request->isMethod('post')){

            // echo "<pre>"; print_r($request->input()); die;
	        $subject_id = $request->subject_id;
	        $unit_id 	= $request->unit_id;
	        $set_ids    = $request->set_ids;
	    	if ($request->hasFile('csv_file')) {
	           
	            $path = $request->file('csv_file')->getRealPath();
	            // echo "<pre>"; print_r($path); //die;
	            $data = \Excel::load($path)->get()->toArray();
	            // echo "<pre>"; print_r($data); die;

	            foreach ($data as $key => $value) {
	            	if((is_string($value['marks'])) || (is_string($value['correct_answer']))){
	            		return redirect()->back()->with('error','Marks or Correct Answer must contain digits');
	            	}
	            }

	            foreach ($data as $key => $value) {
		            $question 						= new Question;
		            $question->question 			= $value['question'];
		            $question->marks 				= $value['marks'];
		            $question->correct_answer 		= $value['correct_answer'];
		            $question->answer_explanation 	= $value['answer_explanation'];
		            $question->subject_id		 	= $subject_id;
		            $question->unit_id		 		= $unit_id;
		            $question->exam_id		 		= $exam_id;
		            if($question->save()){

		            	if(!empty($set_ids)){
		    				foreach ($set_ids as $key => $set_id) {
		    					
			    				$set    		  = new QuestionSet;
			    				$set->exam_id     = $exam_id;
			    				$set->question_id = $question->id;
			    				$set->set_id      = $set_id;
			    				$set->save();  
		    				}
	    				}

		    			for ($i=1; $i < 5; $i++) { 
		    				if($i == '1'){
		    					$opt = $value['optiona'];
		    				}elseif($i == '2'){
		    					$opt = $value['optionb'];
		    				}elseif($i == '3'){
		    					$opt = $value['optionc'];
		    				}elseif($i == '4'){
		    					$opt = $value['optiond'];
		    				}

			    			$option 				= new Option;
		    				$option->question_id 	= $question->id;
		    				$option->options 		= $opt;
		    				$option->position 		= $i;
		    				
		    				$option->save();
		    			}
		    		}
		        }
		        return redirect('admin/exams')->with('success','Questions added successfully.');	
		    }else{
		        return redirect()->back()->with('error',COMMON_ERROR);	
			}
    	}
    	$sets = Set::whereNull('deleted_at')->get()->toArray();
        $page = 'exam_management';
        return view('backEnd.examManagement.import_file',compact('page','languages','subjects','units','exam_id','sets'));
    }

    public function validate_add_questions(Request $request){

        $data = $request->all();

        // echo "<pre>"; print_r($data); //die;
        // echo "<pre>"; print_r($data); die;

        foreach ($data['question'] as $key => $value) {

            if(empty($value['ques']) || empty($value['marks']) || empty($value['ans_explanation'])){
            	if( (empty($_FILES['question']['name'][$key]['ques_image'])) || (empty($_FILES['question']['name'][$key]['ans_explanation_image'])) ){

                	return json_encode(false);
            	}
            }

            for ($i=0; $i < 4; $i++) { 
            	if(empty($value['option'][$i])){
            		if(empty($_FILES['question']['name'][$key]['option_image'][$i])){
            			return json_encode(false);
            		}
            	}

            }
        }

        return json_encode(true);
    }

    public function code_generate(Request $request){

    	// echo "<pre>"; print_r($request->input()); die;
    	$exam_count 			= Exam::count();
    	$category_id 			= $request->category_id;
    	$sub_category_id 		= $request->sub_category_id;
    	$sub_sub_category_id 	= $request->sub_sub_category_id;

    	if(!empty($sub_sub_category_id)){
			$sub_sub_catgeory = SubSubCategory::where('id',$sub_sub_category_id)
												->where('deleted_at',null)
												->first();
			if(!empty($sub_sub_catgeory)){
				$name = substr($sub_sub_catgeory->name, 0, 4);
			}else{
				if(!empty($sub_category_id)){
					$sub_category = SubCategory::where('id',$sub_category_id)
									->where('deleted_at',null)
									->first();
					if(!empty($sub_category)){
						$name = substr($sub_category->name, 0, 4);
					}else{
						if(!empty($category_id)){
							$category = Category::where('id',$category_id)
					    						->where('deleted_at',null)
					    						->first();
					    	if(!empty($category)){
					    		$name = substr($category->name, 0, 4);
					    	}else{
					    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
					    	}
						}
					}
				}else{
					if(!empty($category_id)){
						$category = Category::where('id',$category_id)
				    						->where('deleted_at',null)
				    						->first();
				    	if(!empty($category)){
				    		$name = substr($category->name, 0, 4);
				    	}else{
				    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
				    	}
					}else{
			    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
			    	}
				}

			}
		}elseif(!empty($sub_category_id)){
			$sub_category = SubCategory::where('id',$sub_category_id)
	    									->where('deleted_at',null)
											->first();
			if(!empty($sub_category)){
				$name = substr($sub_category->name, 0, 4);
			}else{
				if(!empty($category_id)){
					$category = Category::where('id',$category_id)
			    						->where('deleted_at',null)
			    						->first();
			    	if(!empty($category)){
			    		$name = substr($category->name, 0, 4);
			    	}else{
			    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
			    	}
				}else{
		    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
		    	}
			}
		}elseif(!empty($category_id)){
			$category = Category::where('id',$category_id)
			    						->where('deleted_at',null)
			    						->first();
	    	if(!empty($category)){
	    		$name = substr($category->name, 0, 4);
	    	}else{
	    		$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
	    	}
		}else{
			$name = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);
		}

		$name  = strtoupper($name);
    	$month = date('m');
    	$year  = date('Y');
		$serial_no = $exam_count + 1;
    	if($serial_no <= 9){
    		$serial_no = '0'.$serial_no;
    	}
    	$exam_code = $name.''.$month.''.$year.''.$serial_no;

    	echo $exam_code;

    }

    public function get_exam_name($category_id = null){

    	$name = Exam::where('id',$category_id)
    						->where('deleted_at',null)
    						->orderBy('id','desc')
    						->value('name');
    	if(!empty($name)){
    		$exam_name = $name;
    	}else{
    		$exam_name = '';
    	}
    	echo $exam_name; die;

    }

    public function edit_question(Request $request, $question_id,$exam_id){
    	// echo "22"; die;
		// echo $question_id; die;

    	$question = Question::with('sets')
    	                    ->where('id',$question_id)
    						->where('deleted_at',null)
    						->with('options')
    						->first();

    	if(!empty($question)){
    		$question = $question->toArray();
    	}
        $set_ids = array_map(function($query){ $query = $query['set_id']; return $query; },$question['sets']);
    	if($request->isMethod('post')) {
    		// echo "<pre>"; print_r($request->input()); die;
    		$data = $request->all();
    		$question = Question::where('id',$question_id)
    						->where('deleted_at',null)
    						->with('options')
    						->first();
    		if(!empty($question)){
    			// echo "<pre>"; print_r($question); die;
	    		foreach ($data['question'] as $key => $value) {
	    			
		    		$question->question 			= $value['ques'];
		    		$question->marks 				= $value['marks'];
                    $question->subject_id           = $request->subject_id;
                    $question->unit_id              = $request->unit_id;
		    		$question->answer_explanation 	= $value['ans_explanation'];
		    		$question->correct_answer 		= $value['answer'];

		    		if(!empty($_FILES['question']['name'][$key]['ques_image'])){
		    		    $image      = pathinfo($_FILES['question']['name'][$key]['ques_image']);
		    		    $ext        = $image['extension'];
		    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ques_image'];
		    		    $random_no  = uniqid();
		    		    $new_name   = $random_no.'.'.$ext;
		    		    $destination= ExamImageBasePath.'/'.$new_name;

		    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
		    		    	// if (file_exists($destination.'/'.$question->ques_image)){
		    		    	//     unlink($destination.'/'.$question->ques_image);
		    		    	// }
		    		        move_uploaded_file($tmp_name, $destination);
		    		        $question->ques_image = $new_name;
		    		    }
		    		}
		    		if(!empty($_FILES['question']['name'][$key]['ans_explanation_image'])){
		    		    $image      = pathinfo($_FILES['question']['name'][$key]['ans_explanation_image']);
		    		    $ext        = $image['extension'];
		    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['ans_explanation_image'];
		    		    $random_no  = uniqid();
		    		    $new_name   = $random_no.'.'.$ext;
		    		    $destination= ExamImageBasePath.'/'.$new_name;

		    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
		    		    	// if (file_exists($destination.'/'.$question->ans_image)){
		    		    	//     unlink($destination.'/'.$question->ans_image);
		    		    	// }
		    		        move_uploaded_file($tmp_name, $destination);
		    		        $question->ans_image = $new_name;
		    		    }
		    		}


		    		if($question->save()){
                        
                        $new_set_ids      = array_map(function($q){ 
                                                                      $q;
                                                                      return $q;
                                                                       }, $data['set_ids']);
                        $prev_set_ids     = array_diff($set_ids,$new_set_ids);

                        if(!empty($prev_set_ids)){

                            foreach ($prev_set_ids as $key => $id) {
                                
                                $del        = QuestionSet::where([
                                                                    'question_id'=>$question_id,
                                                                    'set_id'     =>$id
                                                                 ])
                                                                 ->delete();  
                            }
                        }
                        if(!empty($data['set_ids'])){
                            foreach ($data['set_ids'] as $key => $set_id) {
                                $del        = QuestionSet::where([
                                                                    'question_id'=>$question_id,
                                                                    'set_id'     =>$set_id
                                                                 ])
                                                                 ->delete(); 
                                $set              = new QuestionSet;
                                $set->exam_id 	  = $exam_id;
                                $set->question_id = $question->id;
                                $set->set_id      = $set_id;
                                $set->save();  
                            }
                        }

		    			foreach ($value['option'] as $key1 => $value1) {
		    			// echo "<pre>"; print_r($value1); die;
		    				# code...
		    				$option = Option::where('question_id',$question->id)->where('position',$key1+1)->first();
		    				if(!empty($option)){

		    					$option->options = $value1;
			    				

			    				if(!empty($_FILES['question']['name'][$key]['option_image'][$key1])){
					    		    $image      = pathinfo($_FILES['question']['name'][$key]['option_image'][$key1]);
					    		    $ext        = $image['extension'];
					    		    $tmp_name   = $_FILES['question']['tmp_name'][$key]['option_image'][$key1];
					    		    $random_no  = uniqid();
					    		    $new_name   = $random_no.'.'.$ext;
					    		    $destination= ExamImageBasePath.'/'.$new_name;

					    		    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					    		    	// if (file_exists($destination.'/'.$option->option_image)){
					    		    	//     unlink($destination.'/'.$option->option_image);
					    		    	// }
					    		        move_uploaded_file($tmp_name, $destination);
					    		        $option->option_image = $new_name;
					    		    }
					    		}
					    		$option->save();
		    				}
		    			}
		    		}
	    		}
	    		return redirect()->back()->with('success','Question edited successfully');
    		}
    	}
                // echo "<pre>"; print_r($question); die;
    	$sets = Set::whereNull('deleted_at')
    		        ->get()
    		        ->toArray();
        $set_ids = array_map(function($query){ $query = $query['set_id']; return $query; },$question['sets']);
                   
        $subjects= Subject::whereNull('deleted_at')->get()->toArray();
        $units   = Unit::where('subject_id',$question['subject_id'])->whereNull('deleted_at')
                        ->get()->toArray(); 
    	$page = 'exam_management';
    	return view('backEnd.examManagement.edit_question',compact('page','question','question_id','exam_id','sets','set_ids','subjects','units'));
    }


    public function validate_edit_question(Request $request, $question_id){

        $data = $request->all();
		

        $question_detail = Question::where('id',$question_id)->with('options')->first();
       	if(!empty($question_detail)){
       		$question_detail = $question_detail->toArray();
       	}
       	// echo "<pre>"; print_r($question_detail); die;

        foreach ($data['question'] as $key => $value) {
        	
            if(empty($data['ques_text']) || empty($value['marks']) || empty($data['ans_text'])){
            	
            	if( (empty($_FILES['question']['name'][$key]['ques_image'])) || (empty($_FILES['question']['name'][$key]['ans_explanation_image'])) ){
            		
            		if( (empty($question_detail['ques_image'])) && (empty($data['ques_text'])) || (empty($question_detail['ans_image'])) && (empty($data['ans_text']))  ){
            			
            			
                		return json_encode(false);
            		}

            	}
            }
            
            for ($i=0; $i < 4; $i++) { 
            	if(empty($value['option'][$i])){
            		if(empty($_FILES['question']['name'][$key]['option_image'][$i])){
	            		if(empty($question_detail['options'][$i]['option_image'])){

            				return json_encode(false);
	            		}
            		}
            	}

            }
        }

        return json_encode(true);
    }
}
