<?php

namespace App\Http\Controllers\backEnd\notificationManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification,App\User,Mail;
class NotificationController extends Controller
{
	public function index(Request $request){

		$students = User::where([
							'status'     	  =>'A',
							'deleted_at' 	  =>null,
							'verified_account'=>'yes',
							'user_type'       =>'user'
						])
						 ->get()
						 ->toArray();

		$trainers = User::where([
							'status'     	  =>'A',
							'deleted_at' 	  =>null,
							'verified_account'=>'yes',
							'user_type'       =>'trainer'
						])
						 ->get()
						 ->toArray();
		$users = [];
		if($request->isMethod('post')){
			$data  = $request->input();

			if(empty($data['student_email']) && empty($data['trainer_email'])){
				return redirect()->back()->with('error','Please select any email to send notifications');
			}
			if(empty($data['desc'])){
				return redirect()->back()->with('error','Please enter description to send notifications');
			}
			if(!empty($data['student_email'])){
				foreach ($data['student_email'] as $key => $value) {
					$users[] = $value; 
				}
			}
			if(!empty($data['trainer_email'])){
				foreach ($data['trainer_email'] as $key => $value) {
					$users[] = $value; 
				}
			}
			$users_new = [];
			// echo'<pre>'; print_r($data);die;
			foreach ($users as $key => $value) {
				$user = User::with('device_tokken')
							->where([
								'status'     	  =>'A',
								'deleted_at' 	  =>null,
								'verified_account'=>'yes',
								'id'              =>$value
							])
						->first();
				$notifications    		= new Notification;
				$notifications->user_id = $user->id;
				$notifications->title = $data['desc'];
				$user_name = ucfirst($user['first_name']).' '.ucfirst($user['last_name']);
				if($notifications->save()){
						$this->send_push_notification($user_name,$notifications->title,$notifications->user_id);
					try{

						$this->send_notification_email($user_name,$user->email,$notifications->title);
					}
					catch (Exception $e) {
						continue;	
					}
				}
				// $users_new[] = $user;
			}

			return redirect()->back()->with('success','Notification sent successfully');
				// echo'<pre>'; print_r($users_new);die;
		}	

		$page = 'notifications';
		return view('backEnd.notificationManagement.form',compact('page','students','trainers'));
	}

	public function send_push_notification($user_name,$message,$user_id){
		// dd($token2);
		try{
			$users = User::select('users.*','ud.device_token','ud.user_id','uc.sub_category_id')
			             ->join('user_devices as ud','ud.user_id','users.id')
			             ->join('user_courses as uc','uc.user_id','users.id')
			             ->where('users.user_type','user')
			             ->where('users.id',$user_id)
			             ->whereNull('users.deleted_at')
			             ->get()
			             ->toArray();
			             // dd($users);
			foreach ($users as $key => $user) {
		        $msg = array(
		            'body'  => $message,
		            'title' =>'New Notification from Scamper Skills',
		            'receiver'=>$user_name,
		            'icon'  =>url("http://scamperskills.com/public/images/system/logo.png"),                    
		            'sound' => 'default',/*Default sound*/
		            // 'click_action' => $url,
		        );
		        $token = "AAAAuaa7yIo:APA91bHEXFNby3VNdumSba9lGvaaNro0pyXepJ_s94Zp053jZWlhFlXT9gAoQbh2pYg8cI0GMXeQu3epk9eUPq1mbZVOOmSi9H7vXyQ7s0M6DVeyAphNSFW5u7ac11nnrQhl7nVChdV5";
		        $headers = array(
		                    'Authorization: key=' . $token,
		                    'Content-Type: application/json'
		                );

		        $token2=$user['device_token'];
		        $fields = array(
		            'to'          => $token2,
		            'notification'=> $msg,
		            'image_url'   =>'http://scamperskills.com/public/images/system/logo.png', 
		            // 'data' => array("type" => 'banner',"course_id"=>$get_banner->course_id)
		        );
		        $ch = curl_init();
		        curl_setopt( $ch,CURLOPT_URL,'https://fcm.googleapis.com/fcm/send');
		        curl_setopt( $ch,CURLOPT_POST, true );
		        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		        $result = curl_exec($ch);
		        // dd($result);
		        curl_close($ch);
		    }



	    }
	    catch (Exception $e) {
	    	return true;
	    }
    
	}

	public function send_notification_email($user_name,$email,$description){
	    
	    try{

		    if(!empty($email)){
		 
	            $company_name = PROJECT_NAME;               
	            if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){

	                Mail::send('emails.request_response',['user_name'=>$user_name, 'email'=>$email, 'desc'=>$description],function($message) use ($email,$company_name){
	                            $message->to($email,$company_name)->subject('New Notification From Scamper Skills ');
	                });
	            }
		    }                              
	    }
	    catch (Exception $e) {
	    	return true;
	    }
	}
}
