<?php

namespace App\Http\Controllers\backEnd\trainerManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrainerDomain;
class DomainController extends Controller
{
    public function index(Request $request){
   		
        $details = TrainerDomain::whereNull('deleted_at')
                                ->get()
                                ->toArray(); 
       	$page = 'domains';
       	return view('backEnd.trainerManagement.domain.index',compact('page','details'));
    }

    public function add(Request $request){

   	  	if($request->isMethod("post")){
    		$data         = $request->all();
    		$domain       = new TrainerDomain;
            $domain->name = $data['name'];
            if($domain->save()){

				return redirect('admin/domains')->with('success','Domain added successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
    	}
        $page = 'domains';
   		return view('backEnd.trainerManagement.domain.form',compact('page'));
    }

    public function edit(Request $request,$domain_id){

        $domain = TrainerDomain::where('id',$domain_id)->first();
        if($request->isMethod("post")){

            $data         = $request->all();
            $domain->name = $data['name'];
            if($domain->save()){

                return redirect()->back()->with('success','Domain edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
        $page = 'domains';
        return view('backEnd.trainerManagement.domain.form',compact('page','domain_id','domain'));
    }

    public function validate_name(Request $request){

        $data = $request->all();
       // dd($data);
        if($data['domain_id']==null){
            $check_name = TrainerDomain::where('name',$data['name'])
                                      ->whereNull('deleted_at')
                                      ->count(); 
        }else{
            $check_name = TrainerDomain::where('name',$data['name'])
                                      ->where('id','<>',$data['domain_id'])
                                      ->whereNull('deleted_at')
                                      ->count();
        }
        if ($check_name > 0) {
            return 'false';
        }else{
            return 'true';
        }
    }

   	
    public function delete($domain_id){

        $del = TrainerDomain::where('id',$domain_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
        if ($del) {
            return redirect()->back()->with('success','Domain deleted successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
