<?php

namespace App\Http\Controllers\backEnd\reportManagement\trainerManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,App\Subject,App\Trainer;
use App\Country, App\State, App\City, App\TrainerDomain, App\User,App\EmploymentStatus;
use Excel;
use TCPDF;
use Carbon\Carbon;
class TrainerController extends Controller
{
	public function index(){


		$trainer_details = Trainer::select('*')
                                    ->join('users as u','u.id','trainers.id')
                                    ->where([
                                    	'admin_approval'  =>'A',
                                    	'verified_account'=>'yes',
                                    	'user_type'       =>'trainer'
                                    ])
                                    ->whereNull('deleted_at')
                                    ->get()
                                    ->toArray();
                                   // dd($trainer_details);
        $page ='teacher_list';
		return view('backEnd.reportManagement.teachers.index',compact('trainer_details','page'));
	}

	public function add(Request $request){

		if($request->isMethod('post')){

			$data  				= $request->input();
			// dd($data);
			$user  				= new User;
			$user->first_name 	= $data['first_name'];
			$user->last_name 	= $data['last_name'];
			$user->email 		= $data['email'];
			$user->contact 		= $data['contact'];
			$user->dob 			= date('Y-m-d',strtotime($data['dob']));
			$user->gender 		= $data['gender'];
			$user->country_id 	= $data['country_id'];
			$user->state_id 	= $data['state_id'];
			$user->city_id 		= $data['city_id'];
			$user->status 	    = $data['status'];
			$user->added_by  	= 'admin';
			$user->user_type 	= 'trainer';
			$user->verified_account = 'yes';
			$user->district          = $data['district'];
			$user->address           = $data['address'];
			if(!empty($_FILES['image']['name'])){
			    $image      = pathinfo($_FILES['image']['name']);
			    $ext        = $image['extension'];
			    $tmp_name   = $_FILES['image']['tmp_name'];
			    $random_no  = uniqid();
			    $new_name   = $random_no.'.'.$ext;
			    $destination= TrainerProfileBasePath.'/'.$new_name;

			    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
			        move_uploaded_file($tmp_name, $destination);
			        $user->image = $new_name;
			    }else{
			        return redirect()->back()->with('error','Invalid image extension');
			    }
			}
			if($user->save()){

				$trainer 					 	= new Trainer;
				$trainer->id           	     	= $user->id;
				$trainer->pincode     		    = $data['pincode'];
				$trainer->employment_status_id  = $data['employment_status_id'];
				$trainer->organization_name  	= $data['organization_name'];
				$trainer->experience         	= $data['total_experiance'];
				$trainer->trainer_domain_id     = $data['trainer_domain_id'];
				$trainer->subject_id     	 	= $data['subject_id'];
				$trainer->admin_approval     	= 'A';
				if($trainer->save()){
					return redirect('/admin/trainers')->with('success','Teacher added successfully');
				}else{
					return redirect()->with('error',COMMON_ERROR);
				}
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}	

		$countries 		 = Country::select('*')->get()->toArray();
		$domains         = TrainerDomain::select('*')->whereNull('deleted_at')->orderBy('name','asc')->get()->toArray();
		$employee_status = EmploymentStatus::select('*')->whereNull('deleted_at')->get()->toArray();
		$subjects        = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
		
		$page ='trainers';
		return view('backEnd.trainerManagement.trainer.form',compact('page','countries','domains','subjects','employee_status'));	
	}

	public function edit(Request $request,$trainer_id){

		$trainer_details = User::join('trainers as t','t.id','users.id')
								->where('users.id',$trainer_id)
                                ->first();
                                // dd($trainer_details);
		if($request->isMethod('post')){

			$data  				= $request->input();
	
			$trainer_details->first_name 		= $data['first_name'];
			$trainer_details->last_name 		= $data['last_name'];
			$trainer_details->email 			= $data['email'];
			$trainer_details->contact 			= $data['contact'];
			$trainer_details->dob 				= date('Y-m-d',strtotime($data['dob']));
			$trainer_details->gender 			= $data['gender'];
			$trainer_details->country_id 		= $data['country_id'];
			$trainer_details->state_id 	    	= $data['state_id'];
			$trainer_details->city_id 			= $data['city_id'];
			$trainer_details->status 			= $data['status'];
			$trainer_details->added_by  	   	= 'admin';
			$trainer_details->district          = $data['district'];
			$trainer_details->address           = $data['address'];
			if(!empty($_FILES['image']['name'])){
			    $image      = pathinfo($_FILES['image']['name']);
			    $ext        = $image['extension'];
			    $tmp_name   = $_FILES['image']['tmp_name'];
			    $random_no  = uniqid();
			    $new_name   = $random_no.'.'.$ext;
			    $destination= TrainerProfileBasePath.'/'.$new_name;

			    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
			        if (file_exists($destination.'/'.$trainer_details->image)){
			            unlink($destination.'/'.$trainer_details->image);
			        }
			        move_uploaded_file($tmp_name, $destination);
			        $trainer_details->image = $new_name;
			    }else{
			        return redirect()->back()->with('error','Invalid image extension');
			    }
			}
			if($trainer_details->save()){
				$trainer                        = Trainer::where('id',$trainer_id)->first();
				$trainer->pincode     		    = $data['pincode'];
				$trainer->employment_status_id  = $data['employment_status_id'];
				$trainer->organization_name  	= $data['organization_name'];
				$trainer->experience         	= $data['total_experiance'];
				$trainer->trainer_domain_id     = $data['trainer_domain_id'];
				$trainer->subject_id     	 	= $data['subject_id'];
				if($trainer->save()){
					return redirect()->back()->with('success','Teacher edited successfully');
				}else{
					return redirect()->with('error',COMMON_ERROR);
				}
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}	

		$countries 		 = Country::select('*')->get()->toArray();
		$domains         = TrainerDomain::select('*')->whereNull('deleted_at')->orderBy('name','asc')->get()->toArray();
		$employee_status = EmploymentStatus::select('*')->whereNull('deleted_at')->get()->toArray();
		$subjects        = Subject::select('*')->whereNull('deleted_at')->get()->toArray();
		$states = '';
		$cities = '';
		if(!empty($trainer_details->country_id)){

			$states          = State::where('country_id',$trainer_details->country_id)->get()->toArray();
			$cities          = City::where('state_id',$trainer_details->state_id)->get()->toArray();
		}
			// echo'<pre>';print_r($states);die;
		$page ='trainers';
		return view('backEnd.trainerManagement.trainer.form',compact('page','countries','domains','subjects','employee_status','trainer_details','states','cities','trainer_id'));	
	}

	public function delete($trainer_id){

		$del = User::where('id',$trainer_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
		if ($del) {
		    return redirect()->back()->with('success','Teacher deleted successfully');
		}else{
		    return redirect()->back()->with('error',COMMON_ERROR);
		}
	}

	public function send_credentials_mail($trainer_id){
	    
	    $mail =  User::sendCredentialMail($trainer_id);        
	    if ($mail == true) {
	        return redirect()->back()->with('success','Credential mail has been sent successfully');
	    }else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}

	public function validate_email(Request $request){

		$data = $request->all();
    
        if($data['trainer_id']==null){
            $check_email = User::where('email',$data['email'])
                                ->whereNull('deleted_at')
                                ->count(); 
        }else{
            $check_email = User::where('email',$data['email'])
                                ->where('id','<>',$data['trainer_id'])
                                ->whereNull('deleted_at')
                                ->count();
        }
        if ($check_email > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

	public function validate_contact(Request $request){

		$data = $request->all();
    
        if($data['trainer_id']==null){
            $check_email = User::where('contact',$data['contact'])
                                ->whereNull('deleted_at')
                                ->count(); 
        }else{
            $check_email = User::where('contact',$data['contact'])
                                ->where('id','<>',$data['trainer_id'])
                                ->whereNull('deleted_at')
                                ->count();
        }
        if ($check_email > 0) {
            return 'false';
        }else{
            return 'true';
        }
	}

	public function export(Request $request){
		if($request->method('post')){
			$data = $request->input();
			$extension = $data['export_type'];	
			$trainers = Trainer::select('*')
	                            ->with(['user_details'=>function($query){
	                            	$query->whereNull('deleted_at')
	                            		  ->Where('user_type','trainer')
	                            		  ->Where('verified_account','yes');
	                             },'user_details.country','user_details.state','user_details.city','employment_status'=>function($query){
	                                            $query->whereNull('deleted_at');},
	                                        'subject'=>function($query){$query->whereNull('deleted_at');},
	                                        'domain'=>function($query){$query->whereNull('deleted_at');}
	                                    ])
	                            ->where('admin_approval','A');
	        if(!empty($data['trainer_id'])){
	        	$trainers = $trainers->where('id',$data['trainer_id']);
	        }
	        if(!empty($data['date'])){
                $createdAt = Carbon::parse($data['date'])->format('Y-m-d');
                $trainers = $trainers->whereDate('created_at',$createdAt);
            }
	                            

	        $trainers = $trainers->get()
	                             ->toArray();
            if($extension=='pdf'){

                include('vendor/tcpdf/tcpdf.php');                     
                $tcpdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $tcpdf->AddPage('M','A4');
                $tcpdf->SetCreator(PDF_CREATOR);
                $title ='Scamper Skills';
                // $subtitle='Krishikulture';
                $tcpdf->SetTitle('Teacher-List');
                $tcpdf->SetSubject('Teacher-List');
                $tcpdf->setPrintHeader(true);
                $tcpdf->setPrintFooter(false);
                $tcpdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $tcpdf->SetMargins(10, 10, 10, true);
                $tcpdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $tcpdf->SetFont('helvetica', '', 9);
                $tcpdf->setFontSubsetting(false);
                $tcpdf->setFontSubsetting(false);
      
                $html =<<<EOD
                    <html>
                        <head>
                            <meta charset="utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <title>Flags</title>
                            <style>
                                .th{
                                    font-weight:bold;
                                    background-color:#dfdfdf;    
                                }
                                img{
                                    width:200px;
                                    height:auto;
                                }
                                .result_heading th{
                                    font-size:12px;                            
                                }
                                  .result_detail td{
                                    font-size:10px;                            
                                }
                            </style>
                        </head>
                        <body style="font-family:helvetica; background:#fff; margin:0; padding:20px;"> 
                            <table style="width:100%; 
                             background:#fff;padding: 20px; text-align:left;" cellpadding="5" cellspacing="0">
                                    <tr>
                                        <th style="padding: 20px 10px;text-align:center;font-size:100%;font-weight:bolder;">
                                            <img src="http://scamperskills.com/public/images/system/logo.png" alt="Scamper Skill"/>
                                        </th>
                                    </tr>
                            </table>    
                              
                       


                            <table border="1" style="width:100%; font-weight: lighter; background:#fff;border: 1px solid #ddd;text-align:center;" cellpadding="5" cellspacing="0">
                                <thead style="background:#dfdfdf;">
                                    <tr class="result_heading">
                                        <th class="th">Trainer Name</th>
                                        <th class="th">Email</th>
                                        <th class="th">Contact Number</th>
                                        <th class="th">Gender</th>
                                        <th class="th">Country Name</th>
                                        <th class="th">State Name</th>
                                        <th class="th">City Name</th>
                                        <th class="th">Pincode</th>
                                        <th class="th">Employment Status</th>
                                        <th class="th">Organization Name</th>
                                        <th class="th">Experience(In Years)</th>
                                        <th class="th">Domain Name</th>
                                        <th class="th">Subject Name</th>
                                        <th class="th">Status</th>
                                        <th class="th">Joined On</th>
                                    </tr>
                                </thead>
                                <tbody>
EOD;

	                        foreach($trainers as $detail){
	                          
	                            if($detail['user_details']['status']=='A'){
	                                $detail['status']           = 'Active';
	                            }else{
	                                $detail['status']           = 'Inactive';
	                            }
	                            $detail['student_name']     = ucfirst($detail['user_details']['first_name']).' '.ucfirst($detail['user_details']['last_name']);

	                            
	                            $detail['email']       		 = $detail['user_details']['email'];
	                            $detail['contact']     		 = $detail['user_details']['contact'];
	                            $detail['gender']      		 = ucfirst($detail['user_details']['gender']);
	                            $detail['country']     		 = ucfirst($detail['user_details']['country']['name']);
	                            $detail['state_name']  		 = ucfirst($detail['user_details']['state']['name']);
	                            $detail['city']        		 = ucfirst($detail['user_details']['city']['name']);
	                            $detail['pincode']     		 = $detail['pincode'];
	                            $detail['employment_status'] = ucfirst($detail['employment_status']['name']);
	                            $detail['organization_name'] = ucfirst($detail['organization_name']);
	                            $detail['experience']        = $detail['experience'];
	                            $detail['trainer_domain']    = ucfirst($detail['domain']['name']);
	                            $detail['subject']           = ucfirst($detail['subject']['name']);
	                            $detail['registered_on']     = date('d-m-Y',strtotime($detail['user_details']['created_at']));
	                            $html .=<<<EOD
	                            <tr class="result_detail">
	                                <td>{$detail['student_name']}</td>
	                                <td>{$detail['email']}</td>
	                                <td>{$detail['contact']}</td>
	                                <td>{$detail['gender']}</td>
	                                <td>{$detail['country']}</td>
	                                <td>{$detail['state_name']}</td>
	                                <td>{$detail['city']}</td>
	                                <td>{$detail['pincode']}</td>
	                                <td>{$detail['employment_status']}</td>
	                                <td>{$detail['organization_name']}</td>
	                                <td>{$detail['experience']}</td>
	                                <td>{$detail['trainer_domain']}</td>
	                                <td>{$detail['subject']}</td>
	                                <td>{$detail['status']}</td>
	                                <td>{$detail['registered_on']}</td>
	                            </tr>
EOD;
	                        }
	                        $html .=<<<EOD
	                                    </tbody>
	                                </table>
	                            </body>    
	                        </html> 
EOD;
	                                    
	                                    // $pdf = PDF::loadHtml($html);
	                        $tcpdf->writeHTML($html, true, false, true, false, '');
	                        $url = public_path();
	                        $save=$tcpdf->Output('Teacher-List.pdf', 'I');
	                        $tcpdf->Output('Teacher-List.pdf','D');
	                        return Redirect::back();
	                    }
	                            // echo'<pre>'; print_r($trainers);die;
			$trainers_detail   = [];
	        $trainers_detail[] = ['Trainer Name','Email','Contact Number','Gender','Country Name','State Name','City Name','Pincode','Employment Status','organization_name','Experience(In Years)','Domain Name','Subject Name','Status','Joined On'];

	        $detail = [];
	        foreach ($trainers as $key => $value){
	     
	            $detail['student_name']		 = ucfirst($value['user_details']['first_name']).' '.ucfirst($value['user_details']['last_name']);
	            $detail['email']       		 = $value['user_details']['email'];
	            $detail['contact']     		 = $value['user_details']['contact'];
	            $detail['gender']      		 = ucfirst($value['user_details']['gender']);
	            $detail['country']     		 = ucfirst($value['user_details']['country']['name']);
	            $detail['state_name']  		 = ucfirst($value['user_details']['state']['name']);
	            $detail['city']        		 = ucfirst($value['user_details']['city']['name']);
	            $detail['pincode']     		 = $value['pincode'];
	            $detail['gender']      		 = ucfirst($value['user_details']['gender']);
	            $detail['employment_status'] = ucfirst($value['employment_status']['name']);
	            $detail['organization_name'] = ucfirst($value['organization_name']);
	            $detail['experience']        = $value['experience'];
	            $detail['trainer_domain']    = ucfirst($value['domain']['name']);
	            $detail['subject']           = ucfirst($value['subject']['name']);
	            if($value['user_details']['status']=='A'){
	                $detail['status']           = 'Active';
	            }else{
	                $detail['status']           = 'Inactive';
	            }
	            
	            $detail['registered_on']     = date('d-m-Y',strtotime($value['user_details']['created_at']));
	            $trainers_detail[]           = $detail;
	        }

	        Excel::create('Teacher-List',function($excel)use ($trainers_detail){
	            $excel->setTitle('Teacher-List');
	            $excel->setCreator('Laravel')->setCompany('Scamper Skills');
	            $excel->setDescription('Teacher File');
	            $excel->sheet('sheet1',function($sheet) use ($trainers_detail){
	            $sheet->fromArray($trainers_detail, null, 'A1', false, false);
	            });
	        })->download($extension);  
		}
		

	}
}
