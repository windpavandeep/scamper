<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SuccessStory;
class OurFacultyController extends Controller
{

	public function index(){

		$detail  = SuccessStory::where('type','O')->get()->toArray();
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'success_stories';
		return view('backEnd.contentManagement.ourFaculty.index',compact('page','detail'));
	}

	public function add(Request $request){

	    if($request->isMethod('post')){

	    	$data                       = $request->input();
	    	           // echo'<pre>'; print_r($data);die;
	    	$success_story              = new SuccessStory;
            $success_story->name       	= $data['name'];
            $success_story->designation = $data['designation'];
            $success_story->type        = 'O';
            $success_story->ratings     = $data['ratings'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= OurFacultyImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    move_uploaded_file($tmp_name, $destination);
                    $success_story->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($success_story->save()){
            	return redirect('/admin/our-faculties')->with('success','Our Faculty Added Successfully');
            }else{
            	return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'our_faculty';
		return view('backEnd.contentManagement.ourFaculty.form',compact('page'));
	}

	public function edit(Request $request,$our_faculty_id){

	    $our_faculty  = SuccessStory::where('id',$our_faculty_id)->first();
	    if($request->isMethod('post')){

	    	$data                     = $request->input();
            $our_faculty->name        = $data['name'];
            $our_faculty->designation = $data['designation'];
            $our_faculty->ratings     = $data['ratings'];
            $our_faculty->type        = 'O';
            if(!empty($_FILES['image']['name'])){
                $image = pathinfo($_FILES['image']['name']);
                $ext   = $image['extension'];
                $destination = base_path().'/'.OurFacultyImageBasePath;
                $source = $_FILES['image']['tmp_name'];
                $random_no  = rand(10000,55555);
                $new_name = $random_no.'.'.$ext;
                if ($ext == 'jpg'|| $ext =='png'|| $ext =='jpeg') {
                    move_uploaded_file($source, $destination.'/'.$new_name);
                    if (!empty($our_faculty->image)) {
                        if (file_exists($destination.'/'.$our_faculty->image)) {
                            unlink($destination.'/'.$our_faculty->image);
                        }
                    }
                    $our_faculty->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Sorry, This File type is not supported');
                }
            }
            if($our_faculty->save()){
            	return redirect()->back()->with('success','Our faculty edited Successfully');
            }else{
            	return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'our_faculty';
		return view('backEnd.contentManagement.ourFaculty.form',compact('page','our_faculty_id','our_faculty'));
	}

	public function delete($our_faculty_id){

		$our_faculty = SuccessStory::find($our_faculty_id);    
	    $destination = base_path().'/'.OurFacultyImageBasePath;

	    if (file_exists($destination.'/'.$our_faculty->image)){
	        unlink($destination.'/'.$our_faculty->image);
	    }     
	    $our_faculty->delete();

	    if ($our_faculty){
	        return redirect()->back()->with('success','Our faculty deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}

    public function banner_image_edit(Request $request){

        $our_faculty_banner_image  = SuccessStory::where('type','OB')->first();
        if($request->isMethod('post')){

            $data                           = $request->input();
            $our_faculty_banner_image->type = 'OB';
            if(!empty($_FILES['image']['name'])){
                $image = pathinfo($_FILES['image']['name']);
                $ext   = $image['extension'];
                $destination = base_path().'/'.OurFacultyImageBasePath;
                $source = $_FILES['image']['tmp_name'];
                $random_no  = rand(10000,55555);
                $new_name = $random_no.'.'.$ext;
                if ($ext == 'jpg'|| $ext =='png'|| $ext =='jpeg') {
                    move_uploaded_file($source, $destination.'/'.$new_name);
                    if (!empty($our_faculty_banner_image->image)) {
                        if (file_exists($destination.'/'.$our_faculty_banner_image->image)) {
                            unlink($destination.'/'.$our_faculty_banner_image->image);
                        }
                    }
                    $our_faculty_banner_image->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Sorry, This File type is not supported');
                }
            }
            if($our_faculty_banner_image->save()){
                return redirect()->back()->with('success','Our faculty banner image edited Successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
            // echo'<pre>'; print_r($section2_details); die;
        $page = 'our_faculty_banner_image';
        return view('backEnd.contentManagement.ourFacultyBannerImage.form',compact('page','our_faculty_banner_image'));
    }
}
