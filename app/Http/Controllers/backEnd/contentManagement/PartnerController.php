<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AboutUs;
class PartnerController extends Controller
{
        public function index(){

        	$our_partners = AboutUs::where('type','partner')
        								->get()
        								->toArray();
        		// echo'<pre>'; print_r($section2_details); die;
        	$page = 'about_us';
        	return view('backEnd.contentManagement.aboutUs.ourPartners.index',compact('page','our_partners'));
        }

        public function add(Request $request){

    	    if($request->isMethod('post')){

    	    	$data                  = $request->input();
    	            // echo "<pre>"; print_r($request->input()); die;
    	    	$our_partner       = new AboutUs;
       			$our_partner->type = 'partner';
                if(!empty($_FILES['image']['name'])){
                    $image      = pathinfo($_FILES['image']['name']);
                    $ext        = $image['extension'];
                    $tmp_name   = $_FILES['image']['tmp_name'];
                    $random_no  = uniqid();
                    $new_name   = $random_no.'.'.$ext;
                    $destination= AboutUsImageBasePath.'/'.$new_name;

                    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                        move_uploaded_file($tmp_name, $destination);
                        $our_partner->image = $new_name;
                    }else{
                        return redirect()->back()->with('error','Invalid image extension');
                    }
                }
                if($our_partner->save()){
                    
                    return redirect('/admin/about-us/our-partners')->with('success','Our partner image added successfully');
                }else{
                    return redirect()->back()->with('error',COMMON_ERROR);
                }
            }
        	$page = 'about_us';
        	return view('backEnd.contentManagement.aboutUs.ourPartners.form',compact('page'));
        }

        public function edit(Request $request,$partner_id){

        	$our_partner = AboutUs::where('id',$partner_id)->first();

    	    if($request->isMethod('post')){

    	    	$data              = $request->input();
    	    	$our_partner->type = 'partner';

                if(!empty($_FILES['image']['name'])){
                    $image = pathinfo($_FILES['image']['name']);
                    $ext   = $image['extension'];
                    $destination = base_path().'/'.AboutUsImageBasePath;
                    $source = $_FILES['image']['tmp_name'];
                    $random_no  = rand(10000,55555);
                    $new_name = $random_no.'.'.$ext;
                    if ($ext == 'jpg'|| $ext =='png'|| $ext =='jpeg') {
                        move_uploaded_file($source, $destination.'/'.$new_name);
                        if (!empty($our_partner->image)) {
                            if (file_exists($destination.'/'.$our_partner->image)) {
                                unlink($destination.'/'.$our_partner->image);
                            }
                        }
                        $our_partner->image = $new_name;
                    }else{
                        return redirect()->back()->with('error','Sorry, This File type is not supported');
                    }
                }
                if($our_partner->save()){
                    
                    return redirect()->back()->with('success','Our partner image edited successfully');
                }else{
                    return redirect()->back()->with('error',COMMON_ERROR);
                }
            }
        	$page = 'about_us';
        	return view('backEnd.contentManagement.aboutUs.ourPartners.form',compact('page','our_partner','partner_id'));
        }

        public function delete($partner_id){

        	$our_partner_image = AboutUs::find($partner_id);    
            $destination = base_path().'/'.AboutUsImageBasePath;

            if (file_exists($destination.'/'.$our_partner_image->image)){
                unlink($destination.'/'.$our_partner_image->image);
            }     
            $our_partner_image->delete();

            if ($our_partner_image){
                return redirect()->back()->with('success','Our partner image deleted successfully');
            }
            else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
}
