<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Content,App\RetailerImage,App\ContactUsContent;
class ContentController extends Controller
{
    public function header_content(Request $request){

        $content = Content::where('type','H')->first();
        if($request->isMethod('post')){

            $content->contact_no    = $request->contact_no;
            $content->email         = $request->email;
            if($content->save()){
                return redirect('admin/header/content')->with('success','Content updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'header_content';
    	return view('backEnd.contentManagement.headerContent.form',compact('page','content'));
    }
    public function footer_content(Request $request){

        $content = Content::where('type','F')->first();
        if($request->isMethod('post')){
            
            $content->address       = $request->address;
            $content->email         = $request->email;
            $content->contact_no    = $request->contact_no;
            $content->description   = $request->description;
            if($content->save()){
                return redirect('admin/footer/content')->with('success','Content updated successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'footer_content';
        return view('backEnd.contentManagement.footerContent.form',compact('page','content'));
    }

    public function edit_privacy_terms(Request $request){

        $privacy_terms = Content::where('type','P')->first();
        // dd($privacy_terms);
        if($request->isMethod('post')){
            $data                         = $request->input();
            $privacy_terms->description   = $data['desc'];
            if($privacy_terms->save()){
                return redirect()->back()->with('success','Privacy & terms edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'privacy_terms';
        return view('backEnd.contentManagement.privacyTerms.form',compact('page','privacy_terms'));
    }

    public function edit_terms_conditions(Request $request){

        $terms_conditions = Content::where('type','T')->first();
        // echo'<pre>'; print_r($terms_conditions);die;
        if($request->isMethod('post')){
            $data                         = $request->input();
        // dd($data);
            $terms_conditions->description   = $data['desc'];
            if($terms_conditions->save()){
                return redirect()->back()->with('success','Terms & conditions edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'terms_conditions';
        return view('backEnd.contentManagement.termConditions.form',compact('page','terms_conditions'));
    }

    public function edit_retailer_image(Request $request){

        $retailer_image = RetailerImage::select('*')->first();
     
        if($request->isMethod('post')){
            
            $data  = $request->input();
            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= RetailerImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    if(!empty($retailer_image->image)){
                        if (file_exists($destination.'/'.$retailer_image->image)){
                            unlink($destination.'/'.$retailer_image->image);
                        }
                    }
                    move_uploaded_file($tmp_name, $destination);
                    $retailer_image->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($retailer_image->save()){
                return redirect()->back()->with('success','Retailer image edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'retailer_image';
        return view('backEnd.contentManagement.retailerImage.form',compact('page','retailer_image'));
    }

    public function edit_contact_us_content(Request $request){

        $contact_us_content = ContactUsContent::select('*')->first();
        if($request->isMethod('post')){
            $data                           = $request->input();
            $contact_us_content->contact_no = $data['contact_no'];
            $contact_us_content->address    = $data['address'];
            $contact_us_content->email      = $data['email'];
            $contact_us_content->latitude   = $data['latitude'];
            $contact_us_content->longitude  = $data['longitude'];
            $contact_us_content->country    = $data['country'];
            $contact_us_content->city       = $data['city'];
            if($contact_us_content->save()){
                return redirect()->back()->with('success','Contact us content edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $latitude  = $contact_us_content->latitude;
        $longitude = $contact_us_content->longitude;
        $page      = 'contact_us_content';
        return view('backEnd.contentManagement.contactUs.form',compact('page','contact_us_content','latitude','longitude'));
    }

    public function edit_disclaimer(Request $request){

        $disclaimer = Content::where('type','D')->first();
        // echo'<pre>'; print_r($terms_conditions);die;
        if($request->isMethod('post')){
            $data                      = $request->input();
        // dd($data);
            $disclaimer->description   = $data['desc'];
            if($disclaimer->save()){
                return redirect()->back()->with('success','Disclaimer edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);

            }        
        }
        $page = 'disclaimer';
        return view('backEnd.contentManagement.disclaimer.form',compact('page','disclaimer'));
    }
}
