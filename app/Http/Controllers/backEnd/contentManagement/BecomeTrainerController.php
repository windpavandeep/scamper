<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BecomeTrainer;
class BecomeTrainerController extends Controller
{
    public function index(){

    	/*$become_trainers = BecomeTrainer::orderBy('id','Desc')
    									 ->get()
    									 ->toArray();*/
    	$page = 'become_trainer';
    	return view('backEnd.contentManagement.becomeTrainer.index',compact('page'));
    }

    public function section1_edit(Request $request){

    	$section1 = BecomeTrainer::where('type','section1')->first();
    		
	    if($request->isMethod('post')){

	    	$data                        = $request->input();
            $section1->description = $data['desc'];

            if($section1->save()){
                
                return redirect()->back()->with('success','Section1 edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$page = 'become_trainer';
    	return view('backEnd.contentManagement.becomeTrainer.section1.form',compact('page','section1'));
    }

    public function section2_index(){

    	$become_trainers = BecomeTrainer::where('type','section2')
    									 ->orderBy('id','Desc')
    									 ->get()
    									 ->toArray();
    	$page = 'become_trainer';
    	return view('backEnd.contentManagement.becomeTrainer.section2.index',compact('page','become_trainers'));
    }
    public function add(Request $request){

	    if($request->isMethod('post')){

	    	$data                        = $request->input();
            $become_trainer              = new BecomeTrainer;
            $become_trainer->title       = $data['title'];
            $become_trainer->description = $data['desc'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= BecomeTrainerBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    move_uploaded_file($tmp_name, $destination);
                    $become_trainer->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($become_trainer->save()){
                
                return redirect('/admin/become-trainer/section2')->with('success','Section2 added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$page = 'become_trainer';
    	return view('backEnd.contentManagement.becomeTrainer.section2.form',compact('page'));
    }

    public function edit(Request $request,$become_trainer_id){

    	$become_trainer = BecomeTrainer::where('id',$become_trainer_id)->first();
    		
	    if($request->isMethod('post')){

	    	$data                         = $request->input();
            $become_trainer->title       = $data['title'];
            $become_trainer->description = $data['desc'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= BecomeTrainerBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                	if(!empty($become_trainer->image)){
	                    if (file_exists($destination.'/'.$become_trainer->image)){
	    		    	    unlink($destination.'/'.$become_trainer->image);
	    		    	}
    		    	}
    		        move_uploaded_file($tmp_name, $destination);
    		        $become_trainer->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($become_trainer->save()){
                
                return redirect()->back()->with('success','Section2 edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$page = 'become_trainer';
    	return view('backEnd.contentManagement.becomeTrainer.section2.form',compact('page','become_trainer','become_trainer_id'));
    }

    public function delete($become_trainer_id){

    	$become_trainer_del = BecomeTrainer::find($become_trainer_id);    
        $destination        = base_path().'/'.BecomeTrainerBasePath;

        if (file_exists($destination.'/'.$become_trainer_del->image)){
            unlink($destination.'/'.$become_trainer_del->image);
        }     
        $become_trainer_del->delete();

        if ($become_trainer_del){
            return redirect()->back()->with('success','Section2 deleted successfully');
        }
        else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
