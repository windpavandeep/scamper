<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SuccessStory;
class StoryController extends Controller
{

	public function index(){

		$detail  = SuccessStory::where('type','S')->get()->toArray();
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'success_stories';
		return view('backEnd.contentManagement.successStory.index',compact('page','detail'));
	}

	public function add(Request $request){

	    if($request->isMethod('post')){

	    	$data                       = $request->input();
	    	$success_story              = new SuccessStory;
            $success_story->name       	= $data['name'];
            $success_story->designation = $data['designation'];
            $success_story->description = $data['desc'];
            $success_story->type        = 'S';
            $success_story->ratings     = $data['ratings'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= SuccessStoryImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    move_uploaded_file($tmp_name, $destination);
                    $success_story->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($success_story->save()){
            	return redirect('/admin/success-stories')->with('success','Success story Added Successfully');
            }else{
            	return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'success_stories';
		return view('backEnd.contentManagement.successStory.form',compact('page'));
	}

	public function edit(Request $request,$sucess_story_id){

	    $success_story  = SuccessStory::where('id',$sucess_story_id)->first();
	    if($request->isMethod('post')){

	    	$data                       = $request->input();
            $success_story->name       	= $data['name'];
            $success_story->designation = $data['designation'];
            $success_story->description = $data['desc'];
            $success_story->ratings     = $data['ratings'];
            $success_story->type        = 'S';
            if(!empty($_FILES['image']['name'])){
                $image = pathinfo($_FILES['image']['name']);
                $ext   = $image['extension'];
                $destination = base_path().'/'.SuccessStoryImageBasePath;
                $source = $_FILES['image']['tmp_name'];
                $random_no  = rand(10000,55555);
                $new_name = $random_no.'.'.$ext;
                if ($ext == 'jpg'|| $ext =='png'|| $ext =='jpeg') {
                    move_uploaded_file($source, $destination.'/'.$new_name);
                    if (!empty($success_story->image)) {
                        if (file_exists($destination.'/'.$success_story->image)) {
                            unlink($destination.'/'.$success_story->image);
                        }
                    }
                    $success_story->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Sorry, This File type is not supported');
                }
            }
            if($success_story->save()){
            	return redirect()->back()->with('success','Success story edited Successfully');
            }else{
            	return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
			// echo'<pre>'; print_r($section2_details); die;
		$page = 'success_stories';
		return view('backEnd.contentManagement.successStory.form',compact('page','sucess_story_id','success_story'));
	}

	public function delete($sucess_story_id){

		$success_story = SuccessStory::find($sucess_story_id);    
	    $destination = base_path().'/'.SuccessStoryImageBasePath;

	    if (file_exists($destination.'/'.$success_story->image)){
	        unlink($destination.'/'.$success_story->image);
	    }     
	    $success_story->delete();

	    if ($success_story){
	        return redirect()->back()->with('success','Success story deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}
}
