<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Faq;
class FaqController extends Controller
{
	public function index(){
    	$faqs = Faq::select('id','title','description')
				   ->whereNull('deleted_at')
				   ->get()
				   ->toArray();						   

    	$page = 'faq';
    	return view('backEnd.contentManagement.faqs.index',compact('page','faqs'));
    }

    public function add(Request $request){
		
		if($request->isMethod("post")){
			$data      	      = $request->input();
			$faq			  = new Faq;
			$faq->title 	  = $data['title'];
			$faq->description = $data['desc'];
			if($faq->save()){
				return redirect('/admin/faqs')->with('success','Faq Added Successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'faq';
    	return view('backEnd.contentManagement.faqs.form',compact('page'));    	
    }

    public function edit(Request $request,$faq_id){
		
		$faq_details  = Faq::where('id',$faq_id)->first();
		// dd($faq_details);
		// echo'<pre>'; print_r($faq_details->toArray()); die;
		if($request->isMethod("post")){

			$data      			      = $request->input();
			$faq_details->title 	  = $data['title'];
			$faq_details->description = $data['desc'];
			if($faq_details->save()){
				return redirect()->back()->with('success','Faq Edited Successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'faq';
    	return view('backEnd.contentManagement.faqs.form',compact('page','faq_details','faq_id'));    	
    }

    public function delete($faq_id){

    	$del = Faq::where('id',$faq_id)->delete();

        if($del) {
            return redirect()->back()->with('success','Faq Deleted Successfully');
        }else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
