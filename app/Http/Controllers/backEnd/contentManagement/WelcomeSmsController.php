<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WelcomeSms;
class WelcomeSmsController extends Controller
{

	public function edit(Request $request){
		$sms_content = WelcomeSms::select('*')->first();
		if($request->isMethod('post')){

			$data   		  		  = $request->input();
			$sms_content->description = $data['desc'];
			if($sms_content->save()){
				return redirect()->back()->with('success','Welcome sms edited successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'welcome_sms';
		return view('backEnd.contentManagement.welcomeSms.form',compact('page','sms_content'));
	}
}
