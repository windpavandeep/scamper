<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AdvertisementPopup;
class PopContentController extends Controller
{	
	public function index(){

	    $pop_ups = AdvertisementPopup::select('*')
	    							 ->get()
	    							 ->toArray();
	    							 // echo'<pre>'; print_r($pop_ups);die;
	    $page = 'pop_up';
	    return view('backEnd.contentManagement.popUp.index',compact('page','pop_ups'));
	}

	public function add(Request $request){

	    if($request->isMethod('post')){
	        	
	        $data                = $request->input();
	       	if(!empty($data['title'])&&!empty($data['description']) || empty($_FILES['images']['0'])){

		        $pop_up              = new AdvertisementPopup;
		        $pop_up->title       = @$data['title'];
		        $pop_up->description = @$data['description'];
		        $pop_up->url         = $data['url'];
		        $pop_up->status      = $data['status'];
		        if(!empty($_FILES['image']['name'])){
		            $image      = pathinfo($_FILES['image']['name']);
		            $ext        = $image['extension'];
		            $tmp_name   = $_FILES['image']['tmp_name'];
		            $random_no  = uniqid();
		            $new_name   = $random_no.'.'.$ext;
		            $destination= AdvertisementPopUpBasePath.'/'.$new_name;

		            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
		                move_uploaded_file($tmp_name, $destination);
		                $pop_up->image = $new_name;
		            }else{
		                return redirect()->back()->with('error','Invalid image extension');
		            }
		        }
		        if($pop_up->save()){
		            return redirect('/admin/advertisement/pop-ups')->with('success','Advertisement pop up added successfully');
		        }else{
		            return redirect()->back()->with('error',COMMON_ERROR);
		        }
	       	}else{
	       		return redirect()->back()->with('error','Please enter title, description or image.');
	       	}
	    // echo'<pre>'; print_r($data); die;
	    }
	    $page = 'pop_up';
	    return view('backEnd.contentManagement.popUp.form',compact('page','pop_ups'));
	}

	public function edit(Request $request,$pop_id){

	    $pop_up = AdvertisementPopup::where('id',$pop_id)->first();

	    if($request->isMethod('post')){
	        
	        $data                = $request->input();
	        $pop_up->title       = @$data['title'];
	        $pop_up->description = @$data['description'];
	        $pop_up->url         = $data['url'];
	        $pop_up->status      = $data['status'];
	        if(!empty($_FILES['image']['name'])){
	            $image      = pathinfo($_FILES['image']['name']);
	            $ext        = $image['extension'];
	            $tmp_name   = $_FILES['image']['tmp_name'];
	            $random_no  = uniqid();
	            $new_name   = $random_no.'.'.$ext;
	            $destination= AdvertisementPopUpBasePath.'/'.$new_name;

	            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
	                move_uploaded_file($tmp_name, $destination);
	                $pop_up->image = $new_name;
	            }else{
	                return redirect()->back()->with('error','Invalid image extension');
	            }
	        }
	        if($pop_up->save()){
	            return redirect()->back()->with('success','Advertisement popup edited successfully');
	        }else{
	            return redirect()->back()->with('error',COMMON_ERROR);
	        }
	    // echo'<pre>'; print_r($data); die;
	    }

	    $page = 'pop_up';
	    return view('backEnd.contentManagement.popUp.form',compact('page','pop_up','pop_id'));
	}

	public function delete($pop_id){

		$pop_up      = AdvertisementPopup::find($pop_id);    
	    $destination = base_path().'/'.AdvertisementPopUpBasePath;
	    if(!empty($pop_up->image)){

		    if (file_exists($destination.'/'.$pop_up->image)){
		        unlink($destination.'/'.$pop_up->image);
		    }     
	    }
	    $pop_up->delete();

	    if ($pop_up){
	        return redirect()->back()->with('success','Advertisement popup deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}

	public function validate_pop_status(Request $request){

		$data = $request->input();
	    							 // dd($data);
	    if(!empty(@$data['pop_id'])){

	    	$pop_up_count = AdvertisementPopup::select('*')
	    										->where('status','A')
	    										->where('id','<>',$data['pop_id'])
				    							->get()
				    							->count();
	    }else{
	    	$pop_up_count = AdvertisementPopup::select('*')
	    										->where('status','A')
				    							->get()
				    							->count();
	    }
		
	  	if(($pop_up_count>=1) && ($data['status']=='A')){
	  		$resp = 'false';
	  	}else{
	  		$resp='true';
	  	}
	  	return $resp;
	}
}
