<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewsEvent;
class NewsEventController extends Controller
{
	public function index(){

		$news_events = NewsEvent::select('*')->get()->toArray();
		$page = 'news_events';
		return view('backEnd.contentManagement.newsEvents.index',compact('page','news_events'));
	}

	public function add(Request $request){

		if($request->isMethod('post')){
			$data              = $request->input();
			$news         	   = new NewsEvent;
			$news->title 	   = $data['title'];

			if(!empty($_FILES['image']['name'])){
			    $image      = pathinfo($_FILES['image']['name']);
			    $ext        = $image['extension'];
			    $tmp_name   = $_FILES['image']['tmp_name'];
			    $random_no  = uniqid();
			    $new_name   = $random_no.'.'.$ext;
			    $destination= NewsEventImageBasePath.'/'.$new_name;

			    if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
			        move_uploaded_file($tmp_name, $destination);
			        $news->image = $new_name;
			    }else{
			        return redirect()->back()->with('error','Invalid image extension');
			    }
			}
			$news->description = $data['desc'];
			if($news->save()){
				return redirect('/admin/news-events')->with('success','News event added successfully');
			}else{
				return redirect()->back('error',COMMON_ERROR);
			}
		}
		$page = 'news_events';
		return view('backEnd.contentManagement.newsEvents.form',compact('page'));
	}
	            
	public function edit(Request $request,$news_event_id){

		$news_event = NewsEvent::where('id',$news_event_id)->first();
		if($request->isMethod('post')){

			$data               = $request->input();
			$news_event->title 	= $data['title'];

			if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= NewsEventImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                	if(!empty($news_event->image)){
	                    if (file_exists($destination.'/'.$news_event->image)){
	    		    	    unlink($destination.'/'.$news_event->image);
	    		    	}
    		    	}
    		        move_uploaded_file($tmp_name, $destination);
    		        $news_event->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
			$news_event->description = $data['desc'];
			if($news_event->save()){
				return redirect()->back()->with('success','News event edited successfully');
			}else{
				return redirect()->back('error',COMMON_ERROR);
			}
		}
		$page = 'news_events';
		return view('backEnd.contentManagement.newsEvents.form',compact('page','news_event_id','news_event'));
	}
	public function delete($news_event_id){

		$news_event_del = NewsEvent::find($news_event_id);    
	    $destination    = base_path().'/'.NewsEventImageBasePath;

	    if (file_exists($destination.'/'.$news_event_del->image)){
	        unlink($destination.'/'.$news_event_del->image);
	    }     
	    $news_event_del->delete();

	    if ($news_event_del){
	        return redirect()->back()->with('success','News event deleted successfully');
	    }
	    else{
	        return redirect()->back()->with('error',COMMON_ERROR);
	    }
	}
}
