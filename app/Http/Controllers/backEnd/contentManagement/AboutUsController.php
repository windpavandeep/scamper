<?php

namespace App\Http\Controllers\backEnd\contentManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AboutUs;
class AboutUsController extends Controller
{
    
	public function index(){
				   
    	$page = 'about_us';
    	return view('backEnd.contentManagement.aboutUs.index',compact('page'));
    }

    public function edit_section1(Request $request){

		$section1_detail = AboutUs::where('type','section1')->first();
		if($request->isMethod("post")){

			$data      	                  = $request->input();
			$section1_detail->description = $data['desc'];
			if($section1_detail->save()){
				return redirect('/admin/about-us/section1/edit')->with('success','Section1 content edited Successfully');
			}else{
				return redirect()->back()->with('error',COMMON_ERROR);
			}
		}
		$page = 'about_us';
    	return view('backEnd.contentManagement.aboutUs.section1.form',compact('page','section1_detail'));    	
    }

    public function section2_index(){

    	$section2_details = AboutUs::where('type','section2')
    								->orderBy('id','Desc')
    								->get()
    								->toArray();
    		// echo'<pre>'; print_r($section2_details); die;
    	$page = 'about_us';
    	return view('backEnd.contentManagement.aboutUs.section2.index',compact('page','section2_details'));
    }

    public function add_section2(Request $request){

	    if($request->isMethod('post')){

	    	$data                  = $request->input();
            $section2              = new AboutUs;
            $section2->title       = $data['title'];
            $section2->type        = 'section2';
            $section2->description = $data['desc'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= AboutUsImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    move_uploaded_file($tmp_name, $destination);
                    $section2->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($section2->save()){
                
                return redirect('/admin/about-us/section2')->with('success','Section2 content added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$page = 'about_us';
    	return view('backEnd.contentManagement.aboutUs.section2.form',compact('page'));
    }

    public function edit_section2(Request $request,$section2_id){

    	$section2_detail = AboutUs::where('id',$section2_id)->first();
	    if($request->isMethod('post')){

	    	$data                         = $request->input();
            $section2_detail->title       = $data['title'];
            $section2_detail->type        = 'section2';
            $section2_detail->description = $data['desc'];

            if(!empty($_FILES['image']['name'])){
                $image      = pathinfo($_FILES['image']['name']);
                $ext        = $image['extension'];
                $tmp_name   = $_FILES['image']['tmp_name'];
                $random_no  = uniqid();
                $new_name   = $random_no.'.'.$ext;
                $destination= AboutUsImageBasePath.'/'.$new_name;

                if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    if (file_exists($destination.'/'.$section2_detail->image)){
    		    	    unlink($destination.'/'.$section2_detail->image);
    		    	}
    		        move_uploaded_file($tmp_name, $destination);
    		        $section2_detail->image = $new_name;
                }else{
                    return redirect()->back()->with('error','Invalid image extension');
                }
            }
            if($section2_detail->save()){
                
                return redirect()->back()->with('success','Section2 content edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	$page = 'about_us';
    	return view('backEnd.contentManagement.aboutUs.section2.form',compact('page','section2_detail','section2_id'));
    }

    public function delete_section2($section2_id){

    	$section2 = AboutUs::find($section2_id);    
        $destination = base_path().'/'.AboutUsImageBasePath;

        if (file_exists($destination.'/'.$section2->image)){
            unlink($destination.'/'.$section2->image);
        }     
        $section2->delete();

        if ($section2){
            return redirect()->back()->with('success','Section2 content deleted successfully');
        }
        else{
            return redirect()->back()->with('error',COMMON_ERROR);
        }
    }
}
