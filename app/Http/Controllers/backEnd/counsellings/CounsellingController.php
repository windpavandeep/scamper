<?php

namespace App\Http\Controllers\backEnd\counsellings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FreeCounselling,App\Category;
class CounsellingController extends Controller
{

	public function index(){
		$counsellings = FreeCounselling::select('*')->get()->toArray();
		$page = 'counselling_requests';
		return view('backEnd.counsellings.index',compact('page','counsellings'));
	}

	public function request_details($counselling_request_id){

		/*$counsellings = FreeCounselling::select('free_counsellings.*','c.name as category_name','sc.name as sub_category_name','ssc.name as sub_sub_category_name','s.name as sate_name')
										->join('categories as c','c.id','free_counsellings.category_id')
										->join('sub_categories as sc','sc.id','free_counsellings.sub_category_id')
										->join('sub_sub_categories as ssc','ssc.id','free_counsellings.sub_sub_category_id')
										->join('states as s','s.id','free_counsellings.state_id')
										->where('free_counsellings.id',$counselling_request_id)
										->first();*/
		$counsellings = FreeCounselling::select('free_counsellings.*','c.name as category_name','sc.name as sub_category_name','s.name as state_name')
										->with('sub_sub_category')
										->join('categories as c','c.id','free_counsellings.category_id')
										->join('sub_categories as sc','sc.id','free_counsellings.sub_category_id')
										->join('states as s','s.id','free_counsellings.state_id')
										->where('free_counsellings.id',$counselling_request_id)
										->first();
										// echo'<pre>'; print_r($counsellings->toArray()); die;
		$page = 'counselling_requests';
		return view('backEnd.counsellings.form',compact('page','counsellings'));
	}
}
