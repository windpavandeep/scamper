<?php

namespace App\Http\Controllers\backEnd\contactUs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactUs, App\State, App\City, App\TrainerDomain,App\Subject,App\User,Mail;
class ContactUsController extends Controller
{
    public function index(){

    	$contact_us = ContactUs::orderBy('id','desc')
    							->get()
    							->toArray();
    	// echo'<pre>'; print_r($franchise_details);die;
    	$page = 'contact_us';
    	return view('backEnd.contactUs.index',compact('page','contact_us'));
    }

    public function detail($contact_us_id = null,Request $request){

    	$contact_us_details = ContactUs::where('id',$contact_us_id)
							            ->first();

            // dd($contact_us_details);die;
        if($request->isMethod('post')){
            $data = $request->input();
            // dd($data);
            $reply   = $data['reply'];
            $email   = $contact_us_details->email;
            $mail = $this->admin_reply($reply,$email);

            if($mail){
                // echo'not'; die;
                $contact_us_details->reply_status = 'R';
                $contact_us_details->reply        = $reply;
                if($contact_us_details->save()){
                    return redirect()->back()->with('success','Reply sent successfully');
                }else{
                    return redirect()->back()->with('error',COMMON_ERROR);
                } 
            }else{
                // echo'error'; die;
                return redirect()->back()->with('error',COMMON_ERROR);
            }
            // die;
        }
    	// echo'<pre>'; print_r($franchise_details);die;
    	$page = 'contact_us';
    	return view('backEnd.contactUs.form',compact('page','contact_us_details'));
    }


    public function admin_reply($reply,$email){
        try{

            Mail::raw($reply,function ($message)use($email){
                $message->to($email)
                        ->subject('Scamper Skills Support');
            });
            return true;
        }catch(\Exception $e){
            return false;
        }
    }


}
