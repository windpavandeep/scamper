<?php

namespace App\Http\Controllers\backEnd\downloadManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Download;
class DownloadController extends Controller
{
   	public function index(){

   		$downloads = Download::get()
   							  ->toArray();

    	$page = 'downloads';
       	return view('backEnd.downloadManagement.index',compact('page','downloads'));
    }

    public function add(Request $request){

   		if($request->isMethod('post')){

   			$data      = $request->all();
            // dd($_FILES);
   			$downloads = new Download;
   			$downloads->name = $data['name'];

			if(!empty($_FILES['pdf']['name'])){
			    $image      = pathinfo($_FILES['pdf']['name']);
			    $ext        = $image['extension'];
			    $tmp_name   = $_FILES['pdf']['tmp_name'];
			    $random_no  = uniqid();
			    $new_name   = $random_no.'.'.$ext;
			    $destination= DownloadBasePath.'/'.$new_name;

			    if($ext == 'pdf'){
			        move_uploaded_file($tmp_name, $destination);
			        $downloads->file = $new_name;
			    }else{
			        return redirect()->back()->with('error','Invalid file extension');
			    }
			}	
   			if($downloads->save()){

                return redirect('/admin/downloads')->with('success','Download content added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
		$page = 'downloads';
       	return view('backEnd.downloadManagement.form',compact('page'));
    }


    public function edit(Request $request,$download_content_id){

   		$download_content 	    = Download::where('id',$download_content_id)->first();
        if($request->isMethod('post')){

            $data                   = $request->all();
   			$download_content->name = $data['name'];
			if(!empty($_FILES['pdf']['name'])){
              $image      = pathinfo($_FILES['pdf']['name']);
              $ext        = $image['extension'];
              $tmp_name   = $_FILES['pdf']['tmp_name'];
              $random_no  = uniqid();
              $new_name   = $random_no.'.'.$ext;
              $destination= DownloadBasePath.'/'.$new_name;

              	if($ext == 'pdf'){
                  	if (file_exists($destination.'/'.$download_content->file)){
                      	unlink($destination.'/'.$download_content->file);
                 	}
                  	move_uploaded_file($tmp_name, $destination);
                  	$download_content->file = $new_name;
             	}else{
                  	return redirect()->back()->with('error','Invalid image extension');
               	}
           	}	
            if($download_content->save()){

                return redirect()->back()->with('success','Download content edited successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
			
		}
		$page = 'downloads';
       	return view('backEnd.downloadManagement.form',compact('page','download_content_id','download_content'));
    }

   	public function delete($download_content_id){
   		$del= Download::where('id',$download_content_id)->delete();     
   						 // dd($del);
	   	if ($del) {
	   	    return redirect()->back()->with('success','Download content deleted successfully');
	   	}else{
	   	    return redirect()->back()->with('error',COMMON_ERROR);
	   	}
   	}

}
