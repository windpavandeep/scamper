<?php

namespace App\Http\Controllers\backEnd\discountCoupon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DiscountCoupon,App\User;
class DiscountCouponController extends Controller
{
    
	public function index(){

		$detail = DiscountCoupon::whereNull('deleted_at')
								 ->get()
								 ->toArray();
		$page = 'discount_coupons';
		return view('backEnd.discountCoupon.index',compact('page','detail'));	
	}

    public function add(Request $request){

    	if($request->isMethod('post')){
	    	
	    	$data                             	 = $request->all();  
	    	// echo'<pre>'; print_r($data);die;
	    	$discount_coupon                  	 = new DiscountCoupon;
	    	$discount_coupon->coupon_code 	  	 = $data['coupon_code'];
	    	$discount_coupon->discount_amount 	 = $data['discount_amount'];
	    	$discount_coupon->uses_per_user_limit= $data['uses_limit'];
	    	$discount_coupon->discount_type   	 = $data['discount_type'];
	    	$discount_coupon->start_date 	  	 = $data['start_date'];
	    	$discount_coupon->end_date 		  	 = $data['end_date'];
	    	$discount_coupon->status 		  	 = $data['status'];

	    	if($discount_coupon->save()){
	    		return redirect('admin/discount-coupons')->with('success','Discount coupon added successfully');
	    	}else{
	    		return redirect()->back()->with('error',COMMON_ERROR);
	    	}
    	}

    	$page = 'discount_coupons';
    	return view('backEnd.discountCoupon.form',compact('page'));	
    }

    public function edit(Request $request,$discount_coupon_id){

    	$details = DiscountCoupon::where('id',$discount_coupon_id)
    							  ->first();	
 		// prx($discount_coupon_id);
    	if($request->isMethod('post')){

    		$data                        = $request->all();
	    	$details->coupon_code 	     = $data['coupon_code'];
	    	$details->discount_amount    = $data['discount_amount'];
	    	$details->uses_per_user_limit= $data['uses_limit'];
	    	$details->discount_type      = $data['discount_type'];
	    	$details->start_date 	     = $data['start_date'];
	    	$details->end_date 		     = $data['end_date'];
	    	$details->status 		     = $data['status'];
    		
    		if($details->save()){
	    		return redirect()->back()->with('success','Discount coupon edited successfully');
	    	}else{
	    		return redirect()->back()->with('error',COMMON_ERROR);
	    	}
    	}						  
 		$page = 'discount_coupons';
    	return view('backEnd.discountCoupon.form',compact('page','discount_coupon_id','details'));   
    }

    public function delete($discount_coupon_id){

    	$del = DiscountCoupon::where('id',$discount_coupon_id)
    						 ->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
    	if($del){
    		return redirect('admin/discount-coupons')->with('success','Discount coupon deleted successfully');
    	}else{
    		return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }

    public function validate_coupon_code(Request $request){
    	$data = $request->all();
        $coupon_code  = trim($data['coupon_code']);
        // dd($coupon_code);
        if (!empty(@$data['discount_coupon_id'])) {
            //edit form validation 
            
            if (!empty($data['coupon_code'])) {
                $check_coupon_code = DiscountCoupon::where('coupon_code',$coupon_code)
                                    ->where('id','<>',$data['discount_coupon_id'])
                                    ->whereNull('deleted_at')
                                    ->count();
                // prx($check_name);
            }
        }else{
            //add form validation
            $check_coupon_code = DiscountCoupon::where('coupon_code',$coupon_code)
                                ->whereNull('deleted_at')
                                ->count();
        }
        if ($check_coupon_code > 0) {
            $res = 'false';
        }else{
            $res = 'true';
        }

        return $res;
    }

    public function discount_coupon_mail(Request $request,$discount_coupon_id){

        if($request->isMethod('post')){
            $data = $request->input();
            // dd($data);
            // prX($discount_coupon_id);
            $mail = User::sendDiscountCouponMail($data);

            if($mail==true){    
                return redirect()->back()->with('success','Discount coupon mail send successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }

        }
        $details = User::where([
                            'status'    =>'A',
                            'deleted_at'=>null,
                            'user_type' =>'user'
                        ])
                        ->get()
                        ->toArray();
        $discount_coupon = DiscountCoupon::where('id',$discount_coupon_id)->first(); 
    	

        $page = 'discount_coupons';
        return view('backEnd.discountCoupon.discount_coupon',compact('page','discount_coupon_id','details','discount_coupon'));   
	}
}
