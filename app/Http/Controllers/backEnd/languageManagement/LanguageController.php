<?php

namespace App\Http\Controllers\backEnd\languageManagement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
class LanguageController extends Controller
{
    public function index(){

        $languages = Language::where('deleted_at',null)->get()->toArray();
    	
    	$page = 'language';
    	return view('backEnd.languageManagement.index',compact('page','languages'));
    }

    public function add(Request $request){

        if($request->isMethod('post')){

            $langauge           = new Language;
            $langauge->name     = $request->name;

            if($langauge->save()){
                return redirect('admin/language')->with('success','Langauge added successfully');
            }else{
                return redirect()->back()->with('error',COMMON_ERROR);
            }
        }
    	
    	$page = 'language';
    	return view('backEnd.languageManagement.form',compact('page','countries','subjects','domains'));
    }

    public function edit(Request $request,$language_id){

    	$language = Language::where('id',$language_id)
    							->first();
    							// dd($student_details->gender);die;
    	if($request->isMethod('post')){

    		
    		
    		$language->name = $request->name;
    		
    		if($language->save()){
				return redirect('admin/language')->with('success','Language edited successfully');
			}else{
				return redirect()->with('error',COMMON_ERROR);
			}
    	}		
    	
    	$page = 'language';
    	return view('backEnd.languageManagement.form',compact('page','language','language_id'));
    }

    public function delete($language_id){

    	$del = Language::where('id',$language_id)->update(['deleted_at'=>date('Y-m-d h:i:s')]);           
    	if ($del) {
    	    return redirect()->back()->with('success','Language deleted successfully');
    	}else{
    	    return redirect()->back()->with('error',COMMON_ERROR);
    	}
    }

    public function check_name(Request $request){

        $check_name_exist = Language::where('deleted_at',null)
                                    ->where('name',$request->name)
                                    ->count();
        if($check_name_exist > 0){
            return json_encode(false);
        }else{
            return json_encode(true);

        }

    }
    public function check_edit_name(Request $request, $language_id){

        $check_name_exist = Language::where('deleted_at',null)
                                    ->where('name',$request->name)
                                    ->where('id','!=',$language_id)
                                    ->count();
        if($check_name_exist > 0){
            return json_encode(false);
        }else{
            return json_encode(true);

        }

    }

}
