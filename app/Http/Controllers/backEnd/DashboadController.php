<?php

namespace App\Http\Controllers\backEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User,App\Trainer,App\TrainerContent,App\Transaction,App\Exam;
class DashboadController extends Controller
{
	public function index(){

		$students = User::whereNull('deleted_at')
						 ->where('user_type','user')
					     ->count();
		$teachers = User::join('trainers as t','t.id','users.id')
						 ->where('t.admin_approval','A')
						 ->whereNull('users.deleted_at')
					     ->count();
		$courses = TrainerContent::whereNull('deleted_at')
								  ->where('trainer_id','0')
								  ->count();
		$earnings = Transaction::select('transactions.*','tc.title','tc.trainer_id','u.first_name','u.last_name','tc.title')
								->sum('admin_commission');
		$pending_payments = Transaction::where('paid_status','<>','Y')
								->sum('trainer_price');
		$exams            = Exam::whereNull('deleted_at')
								 ->count();
	    $page = 'dashboard';
	    return view('backEnd.dashboard',compact('page','students','teachers','courses','earnings','pending_payments','exams'));
	}
}
