<?php

namespace App\Http\Middleware;

use Closure;
use Auth, Session;
class CheckTrainerAuth
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(!Auth::check()){
            return redirect('/')->with('error','Please login first');
        }else{
            if(Auth::User()->user_type == 'user'){
                return redirect('/')->with('error','You are not authorised');
            }
            if(Auth::User()->deleted_at != null || Auth::User()->user_type == 'user'){
                Auth::logout();
                Session::flush();
                return redirect('/')->with('error','You are not authorised'); 
            }

            if(Auth::User()->verified_account == 'no'){
                Auth::logout();
                Session::flush();
                return redirect('/')->with('error','Your account is not activated. Please contact to admin for more information');    
            }
        }
        return $next($request);
    }
}
