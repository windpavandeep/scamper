<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\AccessRight, App\Admin;
use Session;
class CheckAdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }*/
        $path = $request->path();

        if(!Auth::guard('admin')->check())
        {  
            return redirect('admin/login');
        } 
       
       //check if user has permission to access this page.
        if(($path != 'admin/dashboard') && (Auth::guard('admin')->user()->admin_type != '0') ){
           $path = preg_replace('/\d/', '', $path);
           // echo "<pre>"; print_r($path); die;
           //paths that does not need permssions
           $allowed_path = array('validate-email',
                               'admin/logout',
                               'admin/validate/student/email',
                               'admin/validate/student/contact',
                               'admin/validate/trainer/email',
                               'admin/validate/trainer/contact',
                               'admin/validate/domain/name',
                               'admin/validate/subcategory/name',
                               'admin/validate/sub-subcategory/name',
                               'admin/validate/subject/name',
                               'admin/validate/unit/name',
                               'admin/sub-subcategory/add/name/validate',
                               'admin/check/edit/language/name/',
                               'admin/check/language/name',
                               'admin/validate/category/name',
                               'admin/validate/coordinator-id',
                               'admin/validate/pincode',
                               'admin/get/exam/name/',
                               'admin/code/generate',
                               'admin/validate/add/questions',
                               'admin/coupon_code/validate',
                               'admin/check/email',
                               'admin/validate/edit/question/',
                               'admin/validate/edit/question-bank/',
                               'admin/validate/set/name',
                               'admin/validate/pop-up/status'
                           );
           //if requested path is not one of them that don't need permission. then check it for permission 
           if(!in_array($path,$allowed_path)) {
              
               $res = $this->checkPermission($path);
               // echo "<pre>"; print_r($res); die;
               if(!$res){
                   
                   if($request->ajax()){
                       echo json_encode('unauthorize'); die;    
                   }
                   return redirect()->back()->with("error",UNAUTHORIZE_ERR);
               } 
           }            
        } 
        return $next($request);
    }
           
    function checkPermission($path){
       //return true; //by passing route check 
        $user_rights = Auth::guard('admin')->user()->access_rights;
        $user_rights = explode(',',$user_rights);
        $rights = AccessRight::select('id','route')->whereIn('id',$user_rights)->get()->toArray();
        foreach ($rights as $key => $right) {
            if(strpos($right['route'], $path) !== false) { 
                return true;    
            }
        }
        return false;
    }
    
}
