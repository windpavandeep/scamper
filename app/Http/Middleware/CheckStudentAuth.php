<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckStudentAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(!Auth::check()){
            return redirect('/')->with('error','Please login first');
        }else{
            if(Auth::User()->user_type == 'trainer'){
                return redirect('/')->with('error','You are not authorised');
            }
            if(Auth::User()->deleted_at != null || Auth::User()->user_type == 'trainer'){
                Auth::logout();
                Session::flush();
                return redirect('/')->with('error','You are not authorised'); 
            }
        }
        return $next($request);
    }
}
