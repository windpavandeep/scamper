<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;
class Trainer extends Model
{
	public function user_details(){
		return $this->hasOne('App\User','id','id');
	}

	public function subject(){
		return $this->hasOne('App\Subject','id','subject_id')->select('id','name');	
	}


	public function domain(){
		return $this->hasOne('App\TrainerDomain','id','trainer_domain_id')->select('id','name');	
	}


    public function employment_status(){
    	return $this->hasOne('App\EmploymentStatus','id','employment_status_id')->select('id','name');
    }

    public static function request_response_email($trainer_id=null,$request=null){

        $detail = Trainer::join('users as u','u.id','trainers.id')
                          ->where('trainers.id',$trainer_id)
                          ->first();
        // dd($request);
        $email        = $detail->email;
        $user_name    = $detail->first_name.' '.$detail->last_name;
        
        $approval     = $detail->admin_approval;

        $company_name = PROJECT_NAME; 
        if($request=='response'){
        	$subject = 'Signup Request Response';
	        if($approval=='A'){

	            $desc = "Your request for signup as a trainer has been approved by admin."; 
	            $resn_desc='';
	        }else{

	            $desc = "Your request for signup as a trainer has been rejected due to following reasons.";
	            $resn_desc = ucfirst($detail->signup_rejection_reason);
	        }
        }else{
        	$subject = 'Signup Request';
        	$desc = "Your request for signup as a trainer has been forward to admin. You will not be able to login into your account before admin approval.";
	        $resn_desc='';
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){

            Mail::send('emails.request_response',['user_name'=>$user_name,'resn_desc'=>$resn_desc, 'desc'=>$desc],function($message) use ($email,$company_name,$subject){
                        $message->to($email,$company_name)->subject($subject);
            });
            return true;
        }
    }

    public static function subscription_plan_response_email($subscription_plan_id=null,$request=null){

        $detail = SubscriptionPlan::select('subscription_plans.*','u.first_name','u.last_name','contact','u.email')
                                                ->join('users as u','u.id','subscription_plans.trainer_id')
                                                ->where('subscription_plans.id',$subscription_plan_id)
                                                ->first();
        // dd($request);
        $email        = $detail->email;
        $user_name    = $detail->first_name.' '.$detail->last_name;
        
        $approval     = $detail->admin_approval;

        $company_name = PROJECT_NAME; 
        if($request=='response'){
            $subject = 'Subscription Plan Request Response';
            if($approval=='A'){

                $desc = "Your request for a new subscription plan has been approved by admin."; 
                $resn_desc='';
            }else{

                $desc = "Your request for a new subscription plan has been rejected due to following reasons.";
                $resn_desc = ucfirst($detail->rejection_reason);
            }
        }else{
            $subject = 'Subscription Plan Request';
            $desc = "Your request for for a new subscription plan has been forward to admin.";
            $resn_desc='';
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL) === false){

            Mail::send('emails.request_response',['user_name'=>$user_name,'resn_desc'=>$resn_desc, 'desc'=>$desc],function($message) use ($email,$company_name,$subject){
                        $message->to($email,$company_name)->subject($subject);
            });
            return true;
        }
    }
}