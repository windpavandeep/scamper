<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePageSecondSection extends Model
{
    protected $table = 'home_page_second_section';
}
