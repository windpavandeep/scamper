<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exam';

    public function sub_categories(){

    	return $this->hasOne('App\SubCategory','id','sub_categories_id')->whereNull('deleted_at');
    }
    public function sub_sub_categories(){

    	return $this->hasOne('App\SubSubCategory','id','sub_sub_categories_id')->whereNull('deleted_at');
    }

    public function user_exams(){

    	return $this->hasMany('App\UserExam','exam_id','id');
    }

    public function user_courses(){

        return $this->hasMany('App\UserCourse','sub_category_id','sub_categories_id');
    }
}
