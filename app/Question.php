<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
	public function options(){
		return $this->hasMany('App\Option','question_id','id');
	}

	public function sets(){
		return $this->hasMany('App\QuestionSet','question_id','id');
	}
}