<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSet extends Model
{
	public function set_name(){
		return $this->hasOne('App\Set','id','set_id')->select('id','name');
	}
}
