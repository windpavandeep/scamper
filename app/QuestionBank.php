<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
	public function question_bank_options(){

		return $this->hasMany('App\QuestionBankOption','question_bank_id','id');
	}

	public function sub_sub_category(){

		return $this->hasOne('App\SubSubCategory','id','sub_sub_category_id')->select('id','name');
	}
}
