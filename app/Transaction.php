<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	public function trainer_contents(){

		return $this->hasOne('App\TrainerContent','id','course_id');
	}

	public function student_details(){
		return $this->hasOne('App\User','id','user_id');	
	}

	public function subscription_plan(){
		return $this->hasOne('App\SubscriptionPlan','id','subscription_plan_id');	
	}

	public function coordinator(){
		return $this->hasOne('App\ReferralCode','pincode','refer_code');	
	}

	public function coordinator_details(){
		return $this->hasOne('App\ReferralCode','coordinator_id','refer_code');	
	}
}
