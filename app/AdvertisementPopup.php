<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementPopup extends Model
{
    public static function get_advertisement_pop_up(){

    	$pop_ups = AdvertisementPopup::select('*')
	    							 ->where('status','A')
	    							 ->first();
	  	return $pop_ups;
    }
}
