<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FreeCounselling extends Model
{
	public function sub_sub_category(){
		return $this->hasOne('App\SubSubCategory','id','sub_sub_category_id');
	}
}
