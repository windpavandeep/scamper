<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainerContent extends Model
{
    //
	public function category(){
        return $this->hasOne('App\Category','id','category_id')->select('id','name');
    }
	
	public function sub_category(){
        return $this->hasOne('App\SubCategory','id','sub_category_id');
    }

    public function course_images(){
    	return $this->hasMany('App\TrainerContentImage','trainer_content_id','id'); 
    }

    public function sub_sub_category(){

    	return $this->hasOne('App\SubSubCategory','id','sub_subcategory_id')->select('id','name');
    }

    public function subject(){
        return $this->hasOne('App\Subject','id','subject_id')->select('id','name');
    }

    public function trainer_detail(){
        return $this->hasOne('App\User','id','trainer_id');   
    }

    public function course_image(){
        return $this->hasOne('App\TrainerContentImage','trainer_content_id','id'); 
    }

    public function course_files(){

        return $this->hasMany('App\TrainerContentFile','trainer_content_id','id');   
    }

    public function content_subjects(){
        return $this->hasMany('App\TrainerContentSubject','trainer_content_id','id');
    }

}
