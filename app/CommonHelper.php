<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject,App\Unit,App\TrainerContentSubject,App\TrainerContentSubjectUnit,App\TrainerContentViews;
use DateTime;

class CommonHelper extends Model
{
	public static function videoSection($activecoursevideo){ ?>
        <div class="row video_replace_slider">
        <?php 
        $checkcount  = 1;
        if($activecoursevideo ){
            foreach($activecoursevideo as $activecoursevideofile){
        if($activecoursevideofile['course_files']){ 
            foreach($activecoursevideofile['course_files'] as $activecoursevideovalue){
                $video_url  =   "https://scamperskills.com/public/images/trainerContents/".$activecoursevideovalue['file'];
                $image_name  = $activecoursevideofile['course_image']['name'];
                $image_path  = SITELINK.TrainerContentImageBasePath.'/'.$image_name; 

                $TrainerContentSubject = TrainerContentSubject::where(['trainer_content_id'=>$activecoursevideofile['id']])->first();
                $subject_name   = Subject::where(['id'=>$TrainerContentSubject['subject_id']])->first();
                $TrainerContentSubjectUnit = TrainerContentSubjectUnit::where(['trainer_content_subject_id'=>$TrainerContentSubject['id']])->first();
                $unit_name  = Unit::where(['id'=>$TrainerContentSubjectUnit['unit_id']])->first();

                $video_created  = $activecoursevideovalue['created_at'];
                $current_date  = date('Y-m-d');
                $date1 = new DateTime($current_date);
                $date2 = new DateTime($video_created);
                $interval = $date1->diff($date2);
                $total_view = TrainerContentViews::where(['trainer_content_file_id'=>$activecoursevideovalue['id']])->count();
            ?>
        <div class="col-sm-4">
            <div class="vid_sing">
                <div class="img_app text-center">
                    <img src="<?php echo $image_path;?>" class="img-fluid">
                    <div class="video-play-icon " id="video-trigger">
                        <a class="wpsuper-lightbox-video vbox-item video-show-poup"  data-toggle="modal" rel="<?php echo $activecoursevideovalue['file'];?>" rel1="<?php echo $activecoursevideovalue['id'];?>">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
                <div class="meta_teach1">
                    <div class="d-flex">
                        <span class="nam_lang">
                            <p>#<?php echo $subject_name['name'];?></p>
                        </span>
                    </div>
                    <a class="lect_ttl1 video-show-poup"  data-toggle="modal" rel="<?php echo $activecoursevideovalue['file'];?>" rel1="<?php echo $activecoursevideovalue['id'];?>"><?php echo $activecoursevideofile['title']; ?></a>
                    <p class="topc_tpe"><?php echo $unit_name['name'];?></p>
                    <p class="tme_seson1" id="total_view<?php echo $activecoursevideovalue['id'];?>"><i class="fa fa-eye" aria-hidden="true"></i> : <?php echo $total_view;?>k</p>  <p class="tme_seson2">
                        <?php
                        if( $interval->y >0){                                                  echo  $interval->y . " years, " . $interval->m." months, ".$interval->d." days ago";
                        } elseif($interval->m > 0){
                            echo  $interval->m." months, ".$interval->d." days ago";
                        } else{
                            echo $interval->d." days ago";
                        }
                        ?>  
                        </p> 
                </div>
            </div>      
        </div>
        <?php $checkcount++; } }  } } ?>
        <?php if($checkcount <= 1){ ?>
        <div class="col-sm-4">
            <div class="vid_sing">
                <p class="vid_head" style="text-align: center;">No Video Available</p>
            </div>
        </div>   
       <?php } ?>
    </div>
<?php
    }
public static function dashboardVideoSection($activecoursevideo){  ?>
    <div id="slider_rel_vids" class="owl-carousel custm_slider video_replace_slider init-slider_rel_vids">
        <?php 
            $checkcount  = 1;
            if($activecoursevideo ){
                foreach($activecoursevideo as $activecoursevideofile){
                    if($activecoursevideofile['course_files']){ 
                        foreach($activecoursevideofile['course_files'] as $activecoursevideovalue){
                            $video_url  =   "https://scamperskills.com/public/images/trainerContents/".$activecoursevideovalue['file'];
                            $image_name  = $activecoursevideofile['course_image']['name'];
                            $image_path  = SITELINK.TrainerContentImageBasePath.'/'.$image_name; 
                            $TrainerContentSubject = TrainerContentSubject::where(['trainer_content_id'=>$activecoursevideofile['id']])->first();
                            $subject_name   = Subject::where(['id'=>$TrainerContentSubject['subject_id']])->first();
                            $TrainerContentSubjectUnit = TrainerContentSubjectUnit::where(['trainer_content_subject_id'=>$TrainerContentSubject['id']])->first();
                            $unit_name  = Unit::where(['id'=>$TrainerContentSubjectUnit['unit_id']])->first();

                            $video_created  = $activecoursevideovalue['created_at'];
                            $current_date  = date('Y-m-d');
                            $date1 = new DateTime($current_date);
                            $date2 = new DateTime($video_created);
                            $interval = $date1->diff($date2);
                            $total_view = TrainerContentViews::where(['trainer_content_file_id'=>$activecoursevideovalue['id']])->count();
                        
                    ?>
                    <div class="item">
                        <div class="vid_sing">
                            <div class="img_app text-center">
                                <img src="<?php echo $image_path;?>" class="img-fluid">
                                <div class="video-play-icon " id="video-trigger">
                                    <a class="wpsuper-lightbox-video vbox-item video-show-poup" data-toggle="modal" rel="<?php echo $activecoursevideovalue['file'];?>" rel1="<?php echo $activecoursevideovalue['id'];?>">
                                        <i class="fa fa-play"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="vid_meta_teach">
                                <div class="d-flex">
                                    <span class="nam_lang">
                                        <p>#<?php echo $subject_name['name'];?></p>
                                    </span>
                                </div>
                                 <a class="vid_lect_ttl video-show-poup" data-toggle="modal" rel="<?php echo $activecoursevideovalue['file'];?>" rel1="<?php echo $activecoursevideovalue['id'];?>"><?php echo $activecoursevideofile['title']; ?></a>
                                <p class="vid_topc"><?php echo $unit_name['name'];?></p>
                                <p Class="vid_view_seson" id="total_view<?php echo $activecoursevideovalue['id'];?>"><i class="fa fa-eye" aria-hidden="true"></i> : <?php echo $total_view;?>k</p>  <p class="vid_time_seson">
                                    <?php
                                        if( $interval->y >0){
                                            echo  $interval->y . " years, " . $interval->m." months, ".$interval->d." days ago";
                                        } elseif($interval->m > 0){
                                            echo  $interval->m." months, ".$interval->d." days ago";
                                        } else{
                                            echo $interval->d." days ago";
                                        }
                                    ?>  
                                </p> 
                            </div>
                        </div>                                                                
                    </div>
        <?php $checkcount++; } } } } ?>
        <?php if($checkcount <= 1){ ?>
            <div class="item">
                <div class="vid_sing">
                    <p class="vid_head">No Video Available</p>
                </div>
            </div>
        <?php } ?>
    </div>
<?php }

public static function notesSection($activecoursepdf){ ?>
    <div class="row pdf_replace_slider">
    <?php 
    $checkcount  = 1;
        if($activecoursepdf ){
            foreach($activecoursepdf as $activecoursepdffile){
                if($activecoursepdffile['course_files']){ 
                    foreach($activecoursepdffile['course_files'] as $activecoursepdfvalue){
                        $pdf_url  =  "https://scamperskills.com/public/images/trainerContents/".$activecoursepdfvalue['file'];
                        $image_name  = $activecoursepdffile['course_image']['name'];
                        $image_path  = SITELINK.TrainerContentImageBasePath.'/'.$image_name;
                        $TrainerContentSubject = TrainerContentSubject::where(['trainer_content_id'=>$activecoursepdffile['id']])->first();
                        $subject_name   = Subject::where(['id'=>$TrainerContentSubject['subject_id']])->first(); 
                ?>
                <div class="col-sm-3">
                    <!-- <a href="<?php echo $pdf_url;?>#toolbar=0&navpanes=0&scrollbar=0"> -->
                    <div class="notes_sing">
                        <span>
<!-- <img src="<?php echo $image_path; ?>" class="img-fluid" > -->
                            <img class="img-fluid pdf-view" src="<?php echo $image_path; ?>"  href="<?php echo $pdf_url;?>">
                        </span>
                    </div>
                    <div class="notes_lect_div">           
                        <p class="notes_topc_tpe">#<?php echo $subject_name['name']; ?></p>
                        <a class="lect_ttl_notes" href="<?php echo $pdf_url;?>#toolbar=0&navpanes=0&scrollbar=0"> <?php echo $activecoursepdffile['title']; ?> </a>
                    </div>                        
                    <!-- </a> -->
                </div>
    <?php $checkcount++; } }  } } ?>
    <?php if($checkcount <= 1){ ?>
    <div class="col-sm-3">
        <div class="vid_sing">
            <p class="notes_head" style="text-align: center;">No Notes Available</p>
        </div>
    </div>   
    <?php } ?> 
</div>
<?php } 
public static function dashboardNotesSection($activecoursepdf){ ?>  
    <div id="slider_notes" class="owl-carousel custm_slider pdf_replace_slider init-slider_rel_nids">
    <?php 
    $checkcount =1;
     if($activecoursepdf ){
        foreach($activecoursepdf as $activecoursepdffile){
            if($activecoursepdffile['course_files']){ 
                foreach($activecoursepdffile['course_files'] as $activecoursepdffilevalue){
                    $pdf_url  =   "https://scamperskills.com/public/images/trainerContents/".$activecoursepdffilevalue['file'];
                    $image_name  = $activecoursepdffile['course_image']['name'];
                    $image_path  = SITELINK.TrainerContentImageBasePath.'/'.$image_name;
                    $TrainerContentSubject = TrainerContentSubject::where(['trainer_content_id'=>$activecoursepdffile['id']])->first();
                    $subject_name   = Subject::where(['id'=>$TrainerContentSubject['subject_id']])->first(); 
            ?>
        <div class="item">
            <a href="<?php echo $pdf_url;?>#toolbar=0&navpanes=0&scrollbar=0">
            <div class="notes_sing">
                <span><img src="<?php echo $image_path; ?>" class="img-fluid" ></span>  
            </div>   
            <div class="notes_lect_div">           
                <p class="notes_topc_tpe">#<?php echo $subject_name['name']; ?></p>
                <a class="lect_ttl_notes" href="<?php echo $pdf_url;?>#toolbar=0&navpanes=0&scrollbar=0"> <?php echo $activecoursepdffile['title']; ?> </a>
            </div>
           </a>
        </div>
    <?php  $checkcount++; } } } } ?>
    <?php if($checkcount <= 1){ ?>
    <div class="item">
        <div class="vid_sing">
            <p class="notes_head">No Notes Available</p>
        </div>
    </div>
    <?php } ?>
    </div>
<?php }
}
?>