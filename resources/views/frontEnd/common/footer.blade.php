<!-- footer page --> 
<!-- Footer-section -->
<style type="text/css">
	.footer-bottm .scamper_right_footer {
		padding-left: 76px;
	}

	.footer-bottm .scamper_left_footer {
		padding-left: 76px;
	}
</style>
	<footer class="footer-sec" id="footer">
		<div class="col-md-12 col-sm-12 col-xs-12 p-0">
			<div class="footer-bottm">
				<div class="row">
					<div class="col-sm-7">
						<div class="row scamper_right_footer">
							@foreach($footer_categories_details as $key=>$category)
								<div class="col-sm-4">
									<div class="wrap-div footer-min">
										<a  href="{{url('/courses?category_id='.$category['id'])}}" class="nav-link"
										  >
										    <h3> {{ ucfirst($category['name']) }} </h3>
										</a>
										
										@if(!empty($category['sub_categories']))     
										<ul type="none" class="footer-content-list p-0">
											@foreach($category['sub_categories'] as $key=>$subcategory)
												<li class="corse_main">
													<a href="{{url('/courses?sub_category_id='.$subcategory['id'])}}">{{ ucfirst($subcategory['name']) }}</a>
													 @if(!empty($subcategory['sub_sub_categories'])) 
													<i class="fas fa-caret-right my_down"></i>
													<ul type="none" class="footer-content-list_sc p-0">
														    @foreach($subcategory['sub_sub_categories'] as $key=>$value)
														<li>
															<!-- <i class="fas fa-caret-right"></i> -->
															<a class="dropdown-item" tabindex="-1" href="{{url('/courses?sub_subcategory_id='.$value['id'])}}">
																{{ ucfirst($value['name']) }}
															</a>
														</li>
														<!-- <li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
														<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
														<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
														<li><i class="fas fa-caret-right"></i> Course Sub-Category</li> -->
														@endforeach
													</ul>
													@endif
												</li>
											@endforeach
											<!-- <li class="corse_main"><a href="#">Course Category</a></li>
											<li class="corse_main"><a href="#">Course Category</a>
												<ul type="none" class="footer-content-list_sc p-0">
													<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
													<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
													<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
													<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
													<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												</ul>
											</li>
											<li class="corse_main"><a href="#">Course Category</a></li> -->
										</ul>
										@endif
									</div>
								</div>
							@endforeach
							<!-- <div class="col-sm-4">
								<div class="wrap-div footer-min">
									<h3>Competetive Exam</h3>
									<ul type="none" class="footer-content-list p-0">
										<li class="corse_main"><a href="#">Course Category</a>
											<ul type="none" class="footer-content-list_sc p-0">
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
											</ul>
										</li>
										<li class="corse_main"><a href="#">Course Category</a></li>
										<li class="corse_main"><a href="#">Course Category</a>
											<ul type="none" class="footer-content-list_sc p-0">
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
											</ul>
										</li>
										<li class="corse_main"><a href="#">Course Category</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="wrap-div footer-min">
									<h3> School  Exams </h3>
									<ul type="none" class="footer-content-list p-0">
										<li class="corse_main"><a href="#">Course Category</a>
											<ul type="none" class="footer-content-list_sc p-0">
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
											</ul>
										</li>
										<li class="corse_main"><a href="#">Course Category</a></li>
										<li class="corse_main"><a href="#">Course Category</a>
											<ul type="none" class="footer-content-list_sc p-0">
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
												<li><i class="fas fa-caret-right"></i> Course Sub-Category</li>
											</ul>
										</li>
										<li class="corse_main"><a href="#">Course Category</a></li>
									</ul>
								</div>
							</div> -->
						</div>
					</div>
					<div class="col-sm-5">
						<div class="row scamper_left_footer">
							<div class="col-sm-6">
								<div class="wrap-div footer-min">
									<h3> Popular Courses </h3>
									<ul type="none" class="footer-content-list p-0">
										<li class="corse_main"><a href="#">Course Page 1</a></li>
										<li class="corse_main"><a href="#">Course Page 2</a></li>
										<li class="corse_main"><a href="#">Course Page 3</a></li>
										<li class="corse_main"><a href="#">Course Page 4</a></li>
										<li class="corse_main"><a href="#">Course Page 5</a></li>
											
									</ul>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="wrap-div footer-min">
									<h3> Quick Links </h3>
									<ul type="none" class="footer-content-list p-0">
										@if(!Auth::check())
											<li class="corse_main"><a href="{{ url('/register/user') }}"> Register Student </a></li>
											<li class="corse_main"><a href="{{ url('/become-trainer') }}"> Register Teacher </a></li>
										@endif
										<li class="corse_main"><a href="#" class="Counselling">Free Counselling </a></li>
										@if(!Auth::check())
											<li class="corse_main"><a href="{{url('/login') }}"> Login</a></li>
										@endif
										<!-- <li><a href="career.html"> Investors</a></li>
										<li><a href="#"> Brochures</a></li>
										<li><a href="#">Site map</a></li>
										<li><a href="#"> Privacy </a></li>
										<li><a href="#"> Legal </a></li>
										<li><a href="{{ url('/franchise') }}"> Franchise </a></li>
										<li><a href="#"> Terms & Condtions </a></li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="botom_ftrr">
			<div class="row">
				<div class="col-sm-7 col-12">
					<ul class="list-inline text-left">
						<li class="list-inline-item"><a href="{{url('/disclaimer')}}">Disclaimer</a></li>
						<li class="list-inline-item"><a href="{{url('/term-conditions')}}">Terms & Conditions</a></li>
						<li class="list-inline-item"><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
						<li class="list-inline-item"><a href="{{url('/sitemaps')}}">Sitemap</a></li>
						<li class="list-inline-item"><a href="{{url('/faqs')}}">FAQ</a></li>
					</ul>
				</div>
				<div class="col-sm-5 col-12">
					<ul class="list-inline text-right">
						<li class="list-inline-item"><a href="javascript:;">&copy; 2019, Scamper Skills. All rights Reserved.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer section end -->
<!-- footer page