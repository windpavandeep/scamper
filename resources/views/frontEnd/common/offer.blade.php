<?php
	$advertisement_popup = App\AdvertisementPopup::get_advertisement_pop_up();
	if (!empty($advertisement_popup->image)) {
	    if (file_exists(AdvertisementPopUpBasePath.'/'.$advertisement_popup->image)) {
	       $offer_image = AdvertisementPopUpImgPath.'/'.$advertisement_popup->image;
	    }
	}else{
	    $offer_image = url(systemImgPath.'/off.png');
	}

?>
<style type="text/css">
	.offer_popup .offer_img {
	  background: url("{{$offer_image}}");
	  background-size: cover;
	  background-repeat: no-repeat;
	  /*background-position: center;*/
	  width: 100%;
	  float: left;
	  height: 440px;
	  float: left;
	  position: relative;
	}
</style>
@if(!empty($advertisement_popup))
	<div class="modal fade offer_popup" id="offer_modal">
		<div class="modal-dialog ">
			<div class="modal-content">
				<div class="offer_close">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="offer_img">
						<div class="row">
							<div class="col-sm-6">
								<div class="modal_content">
									<h1>{{@$advertisement_popup->title}}</h1>
								</div>
								<div class="coupon_code text-center">
									<h4>
										<strong>{{@$advertisement_popup->description}}</strong>
									</h4>  
									<strong>
										<a href="{{url(@$advertisement_popup->url)}}" target="blank">Go to offer page</a>
									</strong>	
								</div>
							</div>				        		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif