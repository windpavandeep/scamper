<!-- footer page --> 
<style type="text/css">
	.botom_ftrr {
		float: left;
		width: 100%;
		border-top: 1px solid #adadad;
		padding: 10px 0;
		font-size: 13px;
	}
	.botom_ftrr a {
		color: #303030;
		/* text-decoration: underline; */
		padding-right: 10px;
		border-right: 1px solid #adadad;
	}
	.botom_ftrr li:last-child a {
		border-right: 1px solid transparent;
	}
</style>
<!-- Footer-section -->
	<footer class="footer-sec" id="footer">
		<div class="col-md-12 col-sm-12 col-xs-12 p-0">
			<div class="footer-bottm">
				<div class="row">
					<div class="col-sm-7">
						<div class="row text-center">
							<!-- <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6">
								<div class="wrap-div footer-min">
									<h3> COURSES </h3>
									<ul type="none" class="footer-content-list p-0">
										<li><a href="#"> CBSE </a></li>
										<li><a href="#"> ICSE </a></li>
										<li><a href="#"> CAT </a></li>
										<li><a href="#"> IAS </a></li>
										<li><a href="#"> JEE </a></li>
										<li><a href="#"> NEET </a></li>
										<li><a href="#"> GRE </a></li>
									</ul>
								</div>
							</div> -->
							<div class="col-sm-4">
								<div class="wrap-div footer-min">
									<h3> Entrance Exam </h3>
									<ul type="none" class="footer-content-list p-0">
										<li><a href="#">Course Page 1</a></li>
										<li><a href="#">Course Page 2</a></li>
										<li><a href="#">Course Page 3</a></li>
										<li><a href="#">Course Page 4</a></li>
										<li><a href="#">Course Page 5</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="wrap-div footer-min">
									<h3>Competetive Exam</h3>
									<ul type="none" class="footer-content-list p-0">
										<li><a href="#">Course Page 1</a></li>
										<li><a href="#">Course Page 2</a></li>
										<li><a href="#">Course Page 3</a></li>
										<li><a href="#">Course Page 4</a></li>
										<li><a href="#">Course Page 5</a></li>
									</ul>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="wrap-div footer-min">
									<h3> School  Exams </h3>
									<ul type="none" class="footer-content-list p-0">
										<li><a href="#">Course Page 1</a></li>
										<li><a href="#">Course Page 2</a></li>
										<li><a href="#">Course Page 3</a></li>
										<li><a href="#">Course Page 4</a></li>
										<li><a href="#">Course Page 5</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="row">
							<div class="col-sm-6">
								<div class="wrap-div footer-min">
									<h3> Popular Courses </h3>
									<ul type="none" class="footer-content-list p-0">
										<li><a href="#">Course Page 1</a></li>
										<li><a href="#">Course Page 2</a></li>
										<li><a href="#">Course Page 3</a></li>
										<li><a href="#">Course Page 4</a></li>
										<li><a href="#">Course Page 5</a></li>
									</ul>
								</div>
							</div>

							<div class="col-sm-6">
								<div class="wrap-div footer-min">
									<h3> Quick Links </h3>
									<ul type="none" class="footer-content-list p-0">
										@if(!Auth::check())
											<li><a href="{{ url('/register/user') }}"> Register Student </a></li>
											<li><a href="{{ url('/register/trainer') }}"> Register Trainer </a></li>
										@endif
										<li><a href="#"> Free Counselling </a></li>
										@if(!Auth::check())
											<li><a href="{{url('/login') }}"> Login</a></li>
										@endif
										<!-- <li><a href="career.html"> Investors</a></li>
										<li><a href="#"> Brochures</a></li>
										<li><a href="#">Site map</a></li>
										<li><a href="#"> Privacy </a></li>
										<li><a href="#"> Legal </a></li>
										<li><a href="{{ url('/franchise') }}"> Franchise </a></li>
										<li><a href="#"> Terms & Condtions </a></li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="botom_ftrr">
			<div class="row">
				<div class="col-sm-7">
					<ul class="list-inline text-left">
						<li class="list-inline-item"><a href="javascript:;">Discalimer</a></li>
						<li class="list-inline-item"><a href="{{url('/term-conditions')}}">Terms & Conditions</a></li>
						<li class="list-inline-item"><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
						<li class="list-inline-item"><a href="{{url('/sitemaps')}}">Sitemap</a></li>
						<li class="list-inline-item"><a href="javascript:;">FAQ</a></li>
					</ul>
				</div>
				<div class="col-sm-5">
					<ul class="list-inline text-right">
						<li class="list-inline-item"><a href="javascript:;">&copy; 2019, Scamper Skills. All rights Reserved.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- footer section end -->
<!-- footer page