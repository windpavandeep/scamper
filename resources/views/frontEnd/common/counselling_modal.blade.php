<?php
    $states     = App\State::get_states();
    $categories = App\Category::get_categories();
    $user_detail= App\User::where('id',@Auth::User()->id)->first();
    $readonly   = '';
    if(!empty(@$user_detail)){
        $readonly = 'readonly';
    }

?>
<div class="modal fade offer_pop" id="modal_quer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content consult-modal-content">
            <div class="offer_close">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="sec_heading mdal_head text-center">
                    <h2>Free Counselling</h2>
                    <p class="divider">
                        <img src="http://scamperskills.com/public/images/system/secdivider.png" class="img-fluid">
                    </p>                    
                </div>
                <div class="formss_modal">
                    <form id="counselling_form" action="{{url('/counselling/request')}}" method="post"> 
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <div>
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="{{isset($user_detail->first_name)?$user_detail->first_name:''}} {{isset($user_detail->last_name)?$user_detail->last_name:''}}" {{$readonly}}> 
                                    </div>
                                    <div>
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{isset($user_detail->email)?$user_detail->email:''}}" {{$readonly}}>
                                    </div>
                                    <div>
                                        <label>Contact Number</label>
                                        <input type="text" name="contact_no" class="form-control" value="{{isset($user_detail->contact)?$user_detail->contact:''}}" {{$readonly}}>
                                    </div>
                                    <div class="nice_selc">
                                        <label>Select Category</label>
                                        <select class="niceselc form-control" name="category_id" id="category_id">
                                            <option data-display="Category" value="">Select Category</option>
                                            @foreach($categories as $category)
                                                <option data-display="Category" value="{{$category['id']}}">{{ucfirst($category['name'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="nice_selc">
                                        <label>Select Sub Category</label>
                                        <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                            <option data-display="Sub Category" value="">Sub Category</option>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="nice_selc">
                                        <label>Select Sub Sub-Category</label>
                                        <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                            <option data-display="Sub Sub-Category" value="">Sub Sub-Category</option>
                                           
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="nice_selc">
                                        <label>Select State</label>
                                        <select class="niceselc form-control" name="state_id">
                                            <option data-display="Sub State" value="">Select State</option>
                                            @foreach($states as $state)
                                                <option data-display="State" value="{{$state['id']}}">{{ucfirst($state['name'])}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea cols="5" placeholder="Description" class="form-control" name="desc"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center log_btn">
                               <!--  <input type="hidden" name="_token" value="i7QHHH5mYczLNUdfWPZaTv1axKKU3Nr9SO1VU0uF"> -->
                                @csrf
                                <button type="submit" class="btn btn_gradient btn_active">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $('#category_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/sub-categories') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#sub_category').html(resp.sub_category);
                    $("#sub_category").selectpicker("refresh");
                    $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                    $("#sub_subcategory").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });
    </script>
    <script type="text/javascript">

        $('#sub_category').on('change', function() {
            $('.loader').show();
            var sc = $(this).val();
            $.ajax({
                type:"get",
                url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
                success:function(resp){
                    $('#sub_subcategory').html(resp);
                    $("#sub_subcategory").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){

        
            $('.Counselling').on('click',function(){
                var checkAuth = "{{(Auth::check())?'true':'false'}}";
                var user_type = "{{@Auth::User()->user_type}}";
                if(checkAuth=='true' && user_type=='trainer'){
                    swal({
                        title: "Not Authorized",
                        text: "You are not authorized for counselling.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    });
                }else{

                    $('#modal_quer').modal('show');
                    $("#counselling_form").trigger("reset");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $('#counselling_form').validate({
            rules:{
                name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                email:{
                    required:true,
                    email:true,
                    maxlength:100,
                },
                contact_no:{
                    required:true,
                    digits:true,
                    minlength:10,
                    maxlength:10,
                },
                category_id:{
                    required:true,
                },
                sub_category_id:{
                    required:true,
                },
                state_id:{
                    required:true,
                },
                desc:{
                    maxlength:1000,
                },
            },
            errorPlacement:function(error,element){
                error.appendTo(element.parent().after());   
            },
        });
    </script>
<!-- modal -->