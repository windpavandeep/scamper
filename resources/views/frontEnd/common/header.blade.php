@include('frontEnd.common.top_header')

<style type="text/css">
.dropdown-submenu {
    position: relative;
}

.dropdown-submenu> a:after {
    content: ">";
    float: right;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: 0px;
    margin-left: 0px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}
/*24/07/19 abhi*/
.nav-item:hover > .f-lvl-drpdn {
    display: block;
}
/*end*/
</style>
<header class="header trans_hdr innr_hedr">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand logo_ch" href="{{ url('/') }}"><img src="{{ asset('/public/images/system/logow.png') }}" class="img-fluid" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <!-- <ul class="navbar-nav ml-auto my_nav"> -->
                    <?php
                        $categories_detail = App\Category::get_category_details();
                       // echo'<pre>'; print_r($categories_detail); die;
                    ?>
                    
                    <ul class="navbar-nav ml-auto my_nav">                        
                        
                        @foreach($categories_detail as $key=>$category)
                            <li class="nav-item">
                                @if(!empty($category['sub_categories']))
                                    <a href="{{url('/courses?category_id='.$category['id'])}}" id="dLabel" class="nav-link dropdown-toggle f-drpdn-item"
                                       ><!-- 24/07/19 abhi -->
                                        {{ ucfirst($category['name']) }}
                                    </a> 
                                @else
                                    <a id="dLabel" data-toggle="dropdown" class="nav-link dropdown-toggle f-drpdn-item"
                                       href="{{url('/courses?sub_category_id='.$category['id'])}}"><!-- 24/07/19 abhi -->
                                        {{ ucfirst($category['name']) }}
                                    </a> 
                                @endif  
                                    
                                @if(!empty($category['sub_categories']))                         
                                    <ul class="dropdown-menu f-lvl-drpdn my_dropdown" role="menu" aria-labelledby="dropdownMenu">
                                        @foreach($category['sub_categories'] as $key=>$subcategory)
                                            @if(!empty($subcategory['sub_sub_categories'])) 
                                                <li class="dropdown-submenu">

                                            @else
                                                <li class="">

                                            @endif
                                                <a class="dropdown-item" tabindex="-1" href="{{url('/courses?sub_category_id='.$subcategory['id'])}}">
                                                    {{ ucfirst($subcategory['name']) }}
                                                </a>    
                                                <ul class="dropdown-menu">
                                                    @foreach($subcategory['sub_sub_categories'] as $key=>$value)
                                                        <li><a class="dropdown-item" tabindex="-1" href="{{url('/courses?sub_subcategory_id='.$value['id'])}}">{{ ucfirst($value['name']) }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                  </ul>
            </div>
        </nav>
    </div>
</header>