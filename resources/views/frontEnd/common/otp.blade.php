

        <div class="page-wrapper">
            <div class="home_page_wrapper inner_page">
            	
            	<!-- register -->
            	<section class="register_sec user_otp">
            		<div class="container">
                        <div class="row">
                            <div class="col-sm-6 offset-3">
                                <div class="wrp_white">
                                    <div class="sec_heading text-center">
                                        <h2>OTP Verification</h2>
                                        <p class="divider"><img src="img/secdivider.png" class="img-fluid" alt="divider"></p>
                                    </div>
                                    <div class="form_head text-center">
                                        <h5>Enter the OTP sent on the registered mobile number</h5> 
                                        <p><strong>+ 91 94786 16633</strong> <small class="edt_num cp" data-toggle="modal" data-target="#numb_mod"><i class="fa fa-edit"></i> Edit</small></p>
                                        <h6>00:54 s</h6>
                                        <form action="" method="POST" role="form">
                                            <div class="col-sm-12">
                                                <div class="clearfix text-center otp_fields">
                                                    <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" />
                                                    <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" />
                                                    <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" />
                                                    <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="text-center log_btn">
                                                    <p class="text-center resnd_otp">Haven't recieved OTP yet? <strong>Resend Code</strong></p>
                                                    <a href="trainerRegister.php" class="btn btn_gradient btn_active">Submit</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            	</section>
            </div>
        </div>

        <!-- edit number modal -->
        <div class="modal modal_fit" id="numb_mod">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Change Mobile Number</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body text-center">
                        <img src="img/chnum.png" class="img-fluid dast" />
                        <p class="text-center emmob">Enter Mobile Number</p>
                        <div class="from_reset">
                            <form action="" method="POST" role="form">
                                <div class="col-sm-12">
                                    <div class="clearfix text-left">
                                        <input type="text" class="form-control" value="94786 16633" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="userRegister.php">Submit</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>

                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('.otp_fields input').keyup(function(){
                    var currleng = $(this).val().length;
                    var maxleng = $(this).attr("maxlength");
                    if(currleng == maxleng){
                        $(this).next().focus();
                    }
                });
                $('.otp_fields input').keydown(function(e) {
                    if ((e.which == 8 || e.which == 46) && $(this).val() =='') {
                        $(this).prev('input').focus();
                    }
                });
            });
        </script>

    </body>
</html>