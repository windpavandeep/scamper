<form action="https://www.example.com/payment/success/" method="POST">
    <script
        src="https://checkout.razorpay.com/v1/checkout.js"
        data-key="rzp_test_QYhZB5gAfNTMNX" // Enter the Key ID generated from the Dashboard
        data-amount="29935" // Amount is in currency subunits. Default currency is INR. Hence, 29935 refers to 29935 paise or INR 299.35.
        data-currency="INR"
        data-buttontext="Pay with Razorpay"
        data-name="Acme Corp"
        data-description="A Wild Sheep Chase is the third novel by Japanese author Haruki Murakami"
        data-image="https://example.com/your_logo.jpg"
        data-prefill.name="Hashish garg"
        data-prefill.email="promatics.hashishgarg@gmail.com"
        data-theme.color="#F37254"
    >
    </script>
    <input type="hidden" custom="Hidden Element" name="hidden">
</form>