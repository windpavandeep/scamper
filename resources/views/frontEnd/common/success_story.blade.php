<?php
	$details = App\SuccessStory::get_success_stories();
?>

@if(!empty($details))
	<section class="testimonial_sec" style="overflow: hidden;">
		<div class="container">
			<div class="sec_heading text-center">
				<h2>Success Stories</h2>
				<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
			</div>
			<!-- <div class=""> -->
		        <div class="col-md-12 clearfix">
		            <!-- <div class=""> -->
		                <div id="testimonial-slider" class="owl-carousel">
		                    @foreach($details as $detail)
			                    <?php  
			                    	if (!empty($detail['image'])) {
			                    		if (file_exists(SuccessStoryImageBasePath.'/'.$detail['image'])) {
			                    			$image = SuccessStoryImageImgPath.'/'.$detail['image'];
			                    		}else{
			                    			$image = DefaultImgPath;
			                    		}
			                    	}else{
			                    		$image = DefaultImgPath;
			                    	}
			                    ?>
			                    <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ $image }}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                {{ ucfirst($detail['name']) }}
			                                <small>{{ ucfirst($detail['designation']) }}</small>
			                                <ul type="none" class="list-inline">
			                                	@for($i=1;$i<=$detail['ratings'];$i++)
			                                	    <li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	@endfor
			                                	
			                                </ul>
			                            </h4>
			                        </div>
			                        <p class="description">
			                          	{!! ucfirst($detail['description']) !!}
			                        </p>
			                    </div>
			                @endforeach
		                </div>
		            <!-- </div> -->
		        </div>
		    <!-- </div> -->
		</div>
	</section>
@endif