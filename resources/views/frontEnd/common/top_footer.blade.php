<?php 
	$footer_content = App\Content::where('type','F')->first();
?>
<!-- footer page -->
<section class="footer_top">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<aside class="footer_widget">
					<div class="footer_logo_heading">
						<a class="footer_logo" href=""><img src="{{ asset('/public/images/system/logow.png') }}" class="img-fluid" ></a>
					</div>
					<div class="footer_widget_content">
						<p> {{@$footer_content->description}}</p>
					</div>
				</aside>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<aside class="footer_widget">
					<h3 class="footer_widget_title">Company</h3>
					<div class="footer_widget_content">
						<ul class="quick_link" type="none">
							<li><a href="{{url('/about-us')}}">About Us</a></li>
							<li><a href="{{url('/downloads')}}">Downloads</a></li>
							<li><a href="{{url('/news-events')}}">News &amp; Events</a></li>
							<li><a href="{{url('/become-trainer')}}">Register as a Teacher</a></li>
							<li><a href="{{ url('/become-retailer')}}">Become A Reseller</a></li>
							<li><a href="{{url('/contact-us')}}">Contact Us</a></li>
						</ul>
					</div>
				</aside>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
				<aside class="footer_widget">
                    <h3 class="footer_widget_title">download now</h3>
                    <div class="footer_widget_content">
                        <div class="download_icon">
                            <ul class="download_icon_menu" type="none">
                                <!-- <li><a href="javascript:;"><img src="img/as.png" alt=""></a></li> -->
                                <li><a href="https://play.google.com/store/apps/details?id=com.scamper.com" target="blank"><img src="{{ asset('/public/images/system/gp.png') }}" alt=""></a></li>
                            </ul>
                        </div>
                        <ul class="social_bookmark left" type="none">
							<li><a href="{{ url('https://www.facebook.com/Scamper-Skills-271722276524406/') }}"><i class="fab fa-facebook"></i></a></li>
							<li><a href="{{ url('https://twitter.com/ScamperSkills') }}"><i class="fab fa-twitter"></i></a></li>
							<li><a href="{{url('https://www.youtube.com/channel/UCxtI7bJ45mP6ZMYdnu4qdHQ')}}"><i class="fab fa-youtube"></i></a></li>
							<li><a href="{{ url('https://www.instagram.com/scamperskills') }}"><i class="fab fa-instagram"></i></a></li>
						</ul>
                    </div>
                </aside>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <aside class="footer_widget">
                    <h3 class="footer_widget_title">Address</h3>
                    <div class="footer_widget_content quick_address">
                        <p>{{@$footer_content->address}}</p>
                        <!-- <p> Reno, New York City, USA</p> -->
                        <!-- <p>+880044 545 989 626</p> -->
                        <p>{{@$footer_content->contact_no}}</p>
                        <a href="javascript:;">{{@$footer_content->email}}</a>
                    </div>
                </aside>
            </div>
        </div>
	</div>
</section>
<!-- footer page