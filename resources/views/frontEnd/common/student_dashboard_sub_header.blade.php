<section class="main_cntnt_dash page_div ">
    <div class="catg_div_sec padtb40">
        <!--  -->
        <div id="slider_catg" class="owl-carousel custm_slider">
            <div class="item">
                <a href="{{url('student/prep-exam')}}">
                <div class="catg_wrap">
                    <span><i class="fa fa-book"></i></span>
                    <p>Prep Test</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('student/exam')}}">
                <div class="catg_wrap">
                    <span><i class="fas fa-file-alt"></i></span>
                    <p>Exams</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('/')}}">
                <div class="catg_wrap">
                    <span><i class="fas fa-cart-plus"></i></span>
                    <p>Buy Courses</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('student/chat_with_teacher')}}">
                <div class="catg_wrap">
                    <span><i class="fas fa-comment-dots"></i></span>
                    <p>Chat with Teacher</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('student/notes')}}">
                <div class="catg_wrap">
                    <span><i class="fa fa-file-signature"></i></span>
                    <p>Notes</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('student/downloads')}}">
                <div class="catg_wrap">
                    <span><i class="fa fa-download"></i></span>
                    <p>Downloads</p>
                </div>
                </a>
            </div>
            <div class="item">
                <a href="{{url('student/free_counselling')}}">
                <div class="catg_wrap">
                    <span><i class="fa fa-globe"></i></span>
                    <p>Free Counselling</p>
                </div>
                </a>
            </div>
        </div>
        <!--  -->
    </div>
</section>