<?php 
    $header_content = App\Content::where('type','H')->first();
?>
<div class="top_header">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 text-left">
                <span class="top_numb"><span class="adm">For Admission Assitance:</span><span class="con">{{@$header_content->contact_no}}</span></span>
                <!-- <span class="top_numb"><i class="fa fa-phone-square"></i> {{@$header_content->contact_no}}</span> -->
                <!-- <span class="top_numb"><i class="fa fa-envelope"></i> {{@$header_content->email}}</span> -->
            </div>
            <div class="col-sm-6 col-md-6 text-right">
                <ul class="top_div list-inline">
                    <li class="list-inline-item">
                        <a href="javascript:;" class="nav-link Counselling">Free Counselling</a>
                    </li>
                    @if(!Auth::check())
                        <!-- <li class="list-inline-item midle_li">
                            <a href="{{ url('user/register') }}">Register</a>
                        </li> -->
                        <li class="list-inline-item">
                            <a class="nav-link " href="{{url('/become-trainer')}}" id="drop-prof">Register As A Teacher </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="nav-link" href="{{url('/register/user')}}" id="drop-prof">Register Free</a>
                            <!-- <div class="dropdown-menu" aria-labelledby="drop-prof">
                              <a class="dropdown-item" href="{{ url('/register'.'/user') }}">Student</a>
                              <a class="dropdown-item" href="{{ url('/register'.'/trainer')  }}">Trainer</a>
                            </div> -->
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ url('/login') }}">Login</a>
                        </li>
                    @else
                        @if(Auth::User()->user_type=='trainer')
                            <li class="list-inline-item" style="padding-left: 2px;padding-right: 6px;">
                                <a href="{{url('/trainer/profile')}}">Profile</a>
                            </li>
                        @else
                            <li class="list-inline-item" style="padding-left: 2px;padding-right: 6px;">
                                <a href="{{url('/student/prep-exam')}}">Dashboard</a>
                            </li>
                        @endif
                        
                        <li class="list-inline-item">
                            <a href="{{ url('logout') }}">Logout</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
