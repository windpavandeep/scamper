<?php
    $details = App\SuccessStory::get_our_faculties();
?>

@if(!empty($details))
    <section class="fac_sec">
    	<div class="about-header-overlay"></div>
    	<div class="container">
    		<div class="sec_heading text-center head_wh">
    			<h2>Our Faculty</h2>
    			<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
    		</div>
            <div class="row">
                <div class="col-md-12 clearfix">
            		<div class="team_imgs">
            			<div id="hm_slider" class="owl-carousel">
                            @foreach($details as $detail)
                                <?php  
                                    if (!empty($detail['image'])) {
                                        if (file_exists(OurFacultyImageBasePath.'/'.$detail['image'])) {
                                            $image = OurFacultyImageImgPath.'/'.$detail['image'];
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    }else{
                                        $image = DefaultImgPath;
                                    }
                                ?>
                                <div class="testimonial">
                                    <div class="testimonial-review">
                                        <div class="pic">
                                            <img src="{{$image}}" alt="" class="img-fluid">
                                        </div>
                                        <h4 class="testimonial-title">
                                            {{ ucfirst($detail['name']) }}
                                            <small>{{ ucfirst($detail['designation']) }}</small>
                                            <ul type="none" class="list-inline">
                                                @for($i=1;$i<=$detail['ratings'];$i++)
                                                    <li class="list-inline-item"><i class="fa fa-star" style="color: #ffb001;"></i>
                                                @endfor
                                            </ul>
                                        </h4>
                                    </div>
                                </div>
                            @endforeach
                        </div>
            		</div>
    	       </div>
           </div>
       </div>
    </section>
<!-- <script type="text/javascript">
    $(document).ready(function(){
        $(".team_imgs").slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
              }
            },
            {
              breakpoint: 481,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        });
    })
</script> -->
@endif