<?php
    $active = '';
    // dd($page);
?>
<header class="header innr_hedr dash_header">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand logo_ch" href="{{ url('/') }}"><img src="{{ asset(systemImgPath.'/logo.png') }}" class="img-fluid" alt="Logo" /></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarNavDropdown">
                <!-- <div class="my_ul"> -->
                    <ul class="navbar-nav mr-auto my_ul">
                            @if(Auth::check())
                                @if(Auth::User()->user_type=='trainer')
                                    <li class="nav-item <?php if($page=='dashboard'){echo'active';}?>">
                                        <a class="nav-link " href="{{url('/') }}">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li class="nav-item <?php if($page=='subscriptions'){echo'active';}?>">
                                        <a class="nav-link" href="{{ url('/trainer/subscriptions')  }}">Subscription Plans</a>
                                    </li>
                                    <li class="nav-item <?php if($page=='profile'){echo'active';}?>">
                                        <a class="nav-link" href="{{ url('/trainer/profile') }}">Profile</a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link " href="{{url('/student/dashboard') }}">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                   <!--  <li class="nav-item <?php if($page=='subscription_history'){echo'active';}?>">
                                        <a class="nav-link" href="{{url('/student/subscription-history')}}">Subscription History</a>
                                    </li> -->
                                    <li class="nav-item <?php if($page=='profile'){echo'active';}?>">   
                                        <a class="nav-link" href="{{ url('/student/profile') }}">Profile</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item">
                                    <a class="nav-link " href="javascript:;">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:;">Profile</a>
                                </li>
                                <li class="nav-item">   
                                    <a class="nav-link" href="javascript:;">Profile</a>
                                </li>
                            @endif
                        <!-- </li> -->
                    </ul>
                    <ul class="navbar-nav my_new_ul">
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="javascript:;"><i class="fas fa-bell"></i></a>
                        </li> -->
                        <li class="name_item nav-item dropdown drop-profile">
                            <a class="nav-link dropdown-toggle" href="#" id="drop-prof" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php
                                    $image = DefaultUserPath;
                                    $usr_img = @Auth::User()->image;
                                    if(@Auth::check() && @Auth::User()->user_type=='trainer'){
                                        if(!empty($usr_img)){
                                            $image = TrainerProfileImgPath.'/'.$usr_img;
                                        }
                                    }else{
                                        if(!empty($usr_img)){
                                            $image = StudentProfileImgPath.'/'.$usr_img;
                                        }
                                    }
                                    // echo '1'; die;
                                ?>
                                <img src="{{ $image }}" class="img-fluid" alt="Trainer Image">
                            </a>

                            <div class="dropdown-menu drop-menu" aria-labelledby="drop-prof">
                                @if(Auth::check())
                                    @if(Auth::User()->user_type=='trainer')
                                        <a class="dropdown-item" href="{{ url('/trainer/profile') }}">Profile</a>
                                    @else
                                        <a class="dropdown-item" href="{{ url('/student/profile') }}">Profile</a>
                                    @endif
                                @endif
                                <!-- <a class="dropdown-item" href="javascript:;">Settings</a> -->
                                <a class="dropdown-item" href="{{url('/logout')}}">Logout</a>
                            </div>
                        </li>
                    </ul>
                <!-- </div> -->
            </div>
        </nav>
    </div>
</header>