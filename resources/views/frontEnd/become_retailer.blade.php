@extends('frontEnd.layouts.master')
@section('title','Become A Reseller')
@section('content')

<div class="page-wrapper">
	<!-- header index -->
	@include('frontEnd.common.cms_header')
	<!-- header index -->

    <div class="home_page_wrapper inner_page">

        <!-- enquiry form  -->
        <section class="fren_sec">
            <div class="container">
                <div class="wrap_downloads wrap_canvas">
                    <div class="table_header be-resell">
                        <div class="row">
                            <div class="col-sm-12 col-md-9"><h3>Become A Reseller</h3></div>
                        </div>
                    </div>
                    <div class="table_div">
                        <div class="form_div">
                            <div class="text-center">
                                <?php 
                                    if(!empty($retailer_image['image'])){
                                        if(file_exists(RetailerImageBasePath.'/'.$retailer_image['image'])){
                                            $image = RetailerImageImgPath.'/'.$retailer_image['image'];
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    }else{
                                        $image = DefaultImgPath;
                                    }    
                                ?>
                                <img src="{{ $image }}" class="img-fluid fr_img">
                            </div>
                            <form action="" method="post" id="franchise_form">
                                <div class="row">
                                    <div class="col-sm-8 offset-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Enter Name" name="name">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter Mail ID" name="email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Contact Number" name="contact_no">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" placeholder="Enter Address" name="address"></textarea>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="country" id="country">
                                                            <option value="" data-display="Select Country">Select Country</option>
                                                            <?php foreach ($countries as $key => $value) {
                                                                
                                                            ?>
                                                                <option value="{{@$value['id']}}">{{@$value['name']}}</option>
                                                            <?php } ?>
                                                            
                                                        </select>
                                                    </div>
                                                    <span class="country_error"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="state" id="state">
                                                            <option value="" data-display="Select State">Select State</option>
                                                            <!-- <option value="1">Punjab</option>
                                                            <option value="2">Himachal</option>
                                                            <option value="3">Haryana</option> -->
                                                        </select>
                                                    </div>
                                                    <span class="state_error"></span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="city" id="city">
                                                            <option data-display="Select City">Select City</option>
                                                           <!--  <option value="1">Punjab</option>
                                                            <option value="2">Himachal</option>
                                                            <option value="3">Haryana</option> -->
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Enter Pin Code" name="pin_code">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="inrad_lab">Experience in Education Industry</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="fomr-group">
                                                    <ul class="list-inline">
                                                        <li class="list-inline-item">
                                                            <label class="custom_label text-left"> Yes
                                                                <input type="radio" name="radio" checked class="rdio_btn" value="Y">
                                                                <span class="checkmark radio"></span>
                                                            </label>
                                                        </li>
                                                        <li class="list-inline-item">
                                                            <label class="custom_label text-left"> No
                                                                <input type="radio" name="radio" class="rdio_btn" value="N">
                                                                <span class="checkmark radio"></span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="3" placeholder="If Yes (Please describe) . . ." name="description" id="desc"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" placeholder="Your Enquiry . . ." name="query_desc"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center log_btn">
                                        <button type="submit" class="btn btn_gradient btn_active">Proceed</button>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- enquiry form / -->


    	<!-- Footer section -->
    	@include('frontEnd.common.top_footer')
    	<!-- Footer section -->
    </div>
</div>

@endsection
@section('scripts')

<script type="text/javascript">
    $('#franchise_form').validate({
        rules:{
            email:{
                required:true,
                email: true,
            },
            name:{
                required:true,
                regex: /^[A-Za-z'.\s]+$/,

            },
            contact_no:{
                required:true,
                regex: /^[0-9'.\s]{7,15}$/,
            },
            address:{
                required:true
            },
            state:{
                required:true
            },
            country:{
                required:true
            },
            pin_code:{
                required:true,
                regex: /^[0-9'.\s]{0,8}$/,
            },
            description:{
                required:true
            },
            query_desc:{
               required:true  
            },
        },
        messages: {
            "name": {
                regex: "*This field can contain only Character",
            },
            "contact_no": {
                regex: "*This field can contain only 7 to 15 Digits",
            },
            "pin_code": {
                regex: "*This field can contain only max 8 Digits",
            },
        },
        errorPlacement: function(error, element) {
            if(element.attr("id") == "state") {
                error.appendTo($('.state_error'));
            }else if(element.attr("id") == "country") {
                error.appendTo($('.country_error'));
            }else{
                error.insertAfter(element);
            }
        },
    })
</script>

<script type="text/javascript">
    $(document).on('change','#country',function(){


        var country_id = $(this).val();

        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/states') }}"+"/"+country_id,
            success:function(resp){

                $('#state').html(resp);
                $('#city').html('<option value="">Select City </option>');
                $("#state").selectpicker("refresh");
                $("#city").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });

    $('#state').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/cities') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#city').html(resp);
                $("#city").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">
        $(function () {
              $('[data-toggle="tooltip"]').tooltip()
          })
    $(document).ready(function() {
        $('.niceselc').selectpicker();
        $('.dtpckr').datepicker();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".course_tables").slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
        });
        $("#testimonial-slider").slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $(".part_slick").slick({
            dots: false,
            arrows: false,
            speed: 10000,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
        });
    });
</script>

<script type="text/javascript">
    $(document).on('click','.rdio_btn',function(){
        var radio_btn_val =  $("input[name='radio']:checked").val();
        if(radio_btn_val == 'N'){
            $('#desc').hide();
            $('#desc').next('.error').hide();
        }else{
            $('#desc').show();
            // $('#desc').next('.error').show();

        }
    })
</script>

@stop