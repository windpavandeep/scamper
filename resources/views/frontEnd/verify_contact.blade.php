<?php
    
    if($user_type=='user'){
        $title = 'Student';
    }elseif($user_type=='trainer'){
        $title = 'Teacher';
    }    
?>

@extends('frontEnd.layouts.master')
@section('title',$title.' Registration')
@section('content')
<style type="text/css">
    .form_head h5 {
        margin: 40px 0 10px;
        font-size: 15px;
    }
</style>
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="register_sec user_mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                            <form method="post" id="contact_verify_form" action="">
                                <div class="wrp_white mob_num_div">
                                    <div class="sec_heading text-center">
                                        <h2>Signup as a {{$title}}</h2>
                                        <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                    </div>
                                    <div class="form_head">
                                        <h5 class="text-center">Enter your Valid Mobile Number</h5>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Mobile Number" maxlength="10">
                                            <span class="error" id="fisr_no"></span>
                                        </div>
                                        @csrf
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                <a href="javascript:;" class="btn show_otp_div btn_gradient btn_active">Proceed</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        Already have an account? <a href="{{ url('/login') }}" class="thm-clr">Click here</a>
                                    </div>
                                </div>
                                <div class="wrp_white otp_div" >
                                    <div class="sec_heading text-center">
                                        <h2>OTP Verification</h2>
                                        <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                    </div>
                                    <div class="form_head text-center">
                                        <h5>Enter the OTP sent on the registered mobile number</h5> 
                                        <p><strong id="entered_no">+ 91 94786 16633</strong> <small class="edt_num cp" id="edit_contact"><i class="fa fa-edit"></i> Edit</small></p>
                                        <h6 id="demo">02:00</h6>
                                        <div class="col-sm-12">
                                            <div class="clearfix text-center otp_fields">
                                                <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" name="otp1" id="otp1" />
                                                <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" name="otp2" id="otp2"/>
                                                <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" name="otp3" id="otp3"/>
                                                <input type="text" class="form-control inpt_otp" minlength="1" maxlength="1" name="otp4" id="otp4"/>
                                            </div>
                                            <span class="error" style="text-align: center;" id="otp_err"></span>
                                        <h5 id="generate_new_otp" style="margin-top: 2px;font-size: 15px;"></<h5>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                <p class="text-center resnd_otp" id="resnd_otp">Haven't recieved OTP yet? <strong> <span id="resed_txt"><a href="javascript:;">Resend Code</a></span> </strong></p>
                                                <button type="submit" class="btn btn_gradient btn_active" id="final_sbmt">Submit</button>
                                                <input type="hidden" name="verified" id="verified" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            
            @include('frontEnd.common.top_footer')
        </div>
    </div>

    <!-- edit number modal -->
    <div class="modal modal_fit" id="numb_mod">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title">Change Mobile Number</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <form method="post" id="change_number_form">
                    <div class="modal-body text-center">
                        <img src="{{ asset('/public/images/system/chnum.png') }}" class="img-fluid dast" />
                        <p class="text-center emmob">Enter Mobile Number</p>
                        <div class="from_reset">
                            <div class="col-sm-12">
                                <div class="clearfix text-left">
                                    <input type="text" class="form-control" value="" name="contact" maxlength="10" id="change_number" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="button" class="btn btn-primary" id="change_submit">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>                    
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#contact').on('input',function(){
            $('#fisr_no').text('');
        })
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.otp_div').hide();
            $('.show_otp_div').click(function(){
                // $('#contact_verify_form').submit();
                var contact = $('#contact').val();
                // alert(contact.length);
                if(contact.length == 0){
                    $('#fisr_no').text('*This field is required');
                } else{
                    if(contact.length < 10 || contact.length > 10){
                        $('#fisr_no').text('Only 10 digits are allowed');
                        return false;
                    } else{
                        $.ajax({
                            type:'get',
                            url:"{{ url('validate/contact') }}"+'/'+contact,
                            success:function(resp1){
                                // alert(resp1);
                                if(resp1 == 'true'){
                                    var data = $('#contact_verify_form').serialize();
                                    $.ajax({
                                        type:'post',
                                        url:"{{ url('send/otp') }}",
                                        data:data,
                                        success:function(resp){
                                            if(resp.status == 'true'){
                                                $('#entered_no').text(resp.contact);
                                                $('#change_number').val(resp.contact);
                                                $('.mob_num_div').slideUp();
                                                $('.otp_div').slideDown();
                                                $('#resnd_otp').css('display','none');
                                                var fiveMinutes = 60 * 2;
                                                var display = document.querySelector('#demo');
                                                stopTimer();
                                                startTimer(fiveMinutes, display);
                                            } else{
                                                swal('Oops','Some error occured. Please try again later after sometime','error');
                                                return false;
                                            }
                                        }
                                    })

                                } else{
                                    $('#fisr_no').text('This mobile number is already registered');
                                    return false;
                                }
                            }
                        })
                    }
                }
            });

            $('#change_submit').click(function(){
                $('#change_number_form').submit();

            })
        })
    </script>

    <script type="text/javascript">
        $('#contact_verify_form').validate({
            rules:{
                contact:{
                    required:true,
                    number:true,
                    regex:/^[0-9]+$/,
                    minlength:10,
                    // maxlength:10
                },
                otp1:{
                    // required:true,
                    minlength:1,
                    maxlength:1,
                    regex:/^[0-9]+$/,
                },
                otp2:{
                    // required:true,
                    minlength:1,
                    maxlength:1,
                    regex:/^[0-9]+$/,
                },
                otp3:{
                    // required:true,
                    minlength:1,
                    maxlength:1,
                    regex:/^[0-9]+$/,
                },
                otp4:{
                    // required:true,
                    minlength:1,
                    maxlength:1,
                    regex:/^[0-9]+$/,
                }
            },
            submitHandler:function(form){
                var data = $('#contact_verify_form').serialize();
                var verified = $('#verified').val();

                if(verified == '1'){
                    var otp1 = $('#otp1').val();
                    var otp2 = $('#otp2').val();
                    var otp3 = $('#otp3').val();
                    var otp4 = $('#otp4').val();
                    if(otp1 != '' && otp2 != '' && otp3 != '' && otp4 != ''){
                        $.ajax({
                            type:"post",
                            url:"{{ url('verify/otp') }}",
                            data:data,
                            success:function(resp){
                                if(resp.status == 'true'){
                                    form.submit();
                                } else{
                                    $('#otp_err').text('*Invalid OTP');
                                    return false;
                                }
                            }
                        })
                    } else{
                        $('#otp_err').text('*This field is required');
                        return false;
                    }
                } else{
                    $('#otp_err').text('*Invalid OTP');
                    return false;
                }
            }

        });

        $('#change_number_form').validate({
            rules:{
                contact:{
                    required:true,
                    number:true,
                    regex:/^[0-9]+$/,
                    minlength:10
                }
            },
            submitHandler:function(form){
                var data = $('#change_number_form').serialize();

                var prev_number = $('#contact').val();
                var new_number = $('#change_number').val();

                if(prev_number == new_number){
                    // if there is no change in number
                    // alert('no');
                    $('#numb_mod').modal('hide');
                } else{
                    $.ajax({
                        type:'post',
                        url:"{{ url('/send/otp') }}",
                        data:data,
                        success:function(resp){
                            // alert(resp);
                            if(resp.status == 'true'){
                                $('#contact').val(resp.contact);
                                $('#entered_no').text(resp.contact);
                                $('#change_number').val(resp.contact);

                                $('#otp1').val('');
                                $('#otp2').val('');
                                $('#otp3').val('');
                                $('#otp4').val('');
                                $('#otp_err').text('');

                                var fiveMinutes = 60 * 2;
                                var display = document.querySelector('#demo');
                                stopTimer();
                                startTimer(fiveMinutes, display);

                                $('#numb_mod').modal('hide');

                            } else{
                                swal('Oops','Some error occured. Please try again later after sometime','error');
                            }
                        }
                    })
                }
            }
        })
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.otp_fields input').keyup(function(){
                var currleng = $(this).val().length;
                var maxleng = $(this).attr("maxlength");
                if(currleng == maxleng){
                    $(this).next().focus();
                }
            });
            $('.otp_fields input').keydown(function(e) {
                if ((e.which == 8 || e.which == 46) && $(this).val() =='') {
                    $(this).prev('input').focus();
                }
            });
        });
    </script>

    <script>
        var myVar;
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            myVar = setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.textContent = minutes + ":" + seconds;

                if (--timer < 0) {
                    timer = duration;
                }
                var data = $('#change_number_form').serialize();
                if(minutes == '00' && seconds == '00'){

                    $.ajax({
                        type:'post',
                        url:"{{ url('/destroy/otp') }}",
                        data:data,
                        success:function(resp){
                            // alert(resp);
                            $('#demo').text('');
                            $('#generate_new_otp').text('Your OTP has expired. Click on resend code link to generate a new one.');
                            $('#resnd_otp').css('display','');
                        }
                    })
                    clearInterval(myVar);
                }
            }, 1000);
        }

        function stopTimer(){
            clearInterval(myVar);
        }
    </script>
    <script type="text/javascript">
        $('#edit_contact').click(function(){
            // alert('1');
            $('#numb_mod').modal('show');
        })
        $('#final_sbmt').click(function(){
            $('#verified').val('1');
        })
    </script>

    <script type="text/javascript">
        $('#resed_txt').click(function(){
            $('.loader').show();
            var contact  = $('#contact').val();

            $.ajax({
                type:'post',
                url:"{{ url('send/otp') }}",
                data:{
                    contact:contact,
                    _token:"{{csrf_token()}}",
                },
                success:function(resp){
                    if(resp.status == 'true'){
                        $('#otp_contact').val(contact);
                        var fiveMinutes = 60 * 2;
                        var display = document.querySelector('#demo');
                        stopTimer();
                        startTimer(fiveMinutes, display);
                        $('#resnd_otp').css('display','none');
                        $('#generate_new_otp').text('');
                        swal('Success','OTP sent successfully on your mobile number','success');
                        // $('#OTPModal').modal('show');
                    } else{
                        swal('Oops','Some error occured. Please try again later after sometime','error');
                    }
                    $('.loader').hide();
                }
            })
        })
    </script>
@endsection