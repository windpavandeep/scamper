@extends('frontEnd.layouts.master')
@section('title','Course Detail')
@section('content') 
<div class="page-wrapper">
	@include('frontEnd.common.cms_header')
    <div class="home_page_wrapper inner_page">
    	<section class="course_dtl_sec">
    		<div class="container">
                <div class="content_detl">
                	<div class="row pos_rel">
                		<div class="col-sm-5 pos_sticky">
                			<div class="course_img">
                				<div class="slick_course">
                					@foreach($course_details->course_images as $value)
                						<?php  
                							if (!empty($value['name'])) {
                								if (file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                									$image = TrainerContentImageImgPath.'/'.$value['name'];
                								}else{
                									$image = DefaultImgPath;
                								}
                							}else{
                								$image = DefaultImgPath;
                							}
                						?>
	                					<div class="item_crs">
	                						<img src="{{$image}}" class="img-fluid" alt="course">
	                					</div>
                					@endforeach
                				</div>
                				<div class="slick_thumb">
                					@foreach($course_details->course_images as $value)
                						<?php  
                							if (!empty($value['name'])) {
                								if (file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                									$image = TrainerContentImageImgPath.'/'.$value['name'];
                								}else{
                									$image = DefaultImgPath;
                								}
                							}else{
                								$image = DefaultImgPath;
                							}
                						?>
                						<div class="item_thumb"><img src="{{$image}}" class="img-fluid" alt="course"></div>
                					@endforeach
                				</div>
                			</div>
                		</div>
                		<div class="col-sm-7">
                			<div class="course_Meta">
                				<h3>{{ ucfirst(@$course_details->sub_category['name']) }} ({{ ucfirst(@$course_details->title) }})</h3>
                				<p class="meta_price">
                					@if(@$course_details->content_availability=='free')
                						Free
                					@else
                						₹{{number_format(@$course_details->final_price) }} (Paid)
                					@endif
                				</p>
                				<ul class="meta_auth bb1" type="none">
                					<!-- <li>Availability: <strong> In stock </strong></li>
                					<li>Author: <strong> R S Aggarwal </strong></li> -->
                				</ul>
                				<p class="desc_crs">{!! ucfirst(@$course_details->description)  !!}</p>
                    			<div class="btns_adby text-left bb1 " id="purchased">

									<?php

									    if(@$course_details->upload_type=='pdf'){
									        $title = 'Download PDF';
									    }else{
									        $title ='Download Video';
									    }
									    // $image = DefaultImgPath; 
									    $file = '';
									    $file_url = 'javascript:;';
									    if(!empty($course_details->file)) {

									        if(file_exists(TrainerContentBasePath.'/'.$course_details->file)) {
									            $file_url = TrainerContentImgPath.'/'.$course_details->file;
									        }
									    // dd($file_url);
									    }else{
									    	$trainer_files = '';
									    }   
									    // dd($trainer_files);
									?>
								
									@if(!empty($check_user_course_exist))
										<div style="margin-bottom:15px;margin-top:15px">
											<div class="col-md-4">Download Course
													
													<p>
													@if(empty($trainer_files))
												   		@foreach($course_details->course_files as $key=>$value)
												   			<?php
												   				$file_url = 'javascript:;';
												   				$file_type= ''; 
												   				if(!empty($value['file'])) {

												   				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
												   				        $file_url = TrainerContentImgPath.'/'.$value['file'];
												   				        $file_name= pathinfo($value['file']);
				                                                        $ext      = $file_name['extension'];
				                                                        if($ext=='pdf'){
				                                                        	$file_type = 'pdf';
				                                                        }else{
				                                                        	$file_type = 'video';
				                                                        }
												   				    }
												   				}
												   			?>
												   			@if(@$ext=='pdf')
												   				({{ $key+1 }}). Pdf
												   				<a href="{{$file_url}}"  target="_blank">

												   					<i class="fas fa-file-pdf"></i>
												   					
												   				</a>
												   			@else
												   				({{$key+1}}). Video
												   				<a href="{{$file_url}}"  target="_blank">
												   					<i class="fas fa-video"></i>
												   				</a>
												   			@endif

												   		@endforeach
											   		@else
											   			<a href="{{$file_url}}"  target="_blank">
														<i data-toggle="tooltip" title="{{$title}}" class="fas fa-download cp" data-original-title="{{$title}}"></i></a>
											   		@endif
											   	</p>
											
											</div>
										</div>
										<a href="javascript:;" class="btn btn_gradient btn_active" disabled><i class="fa fa-cart"></i> Purchased</a>
									@else                    				
	                    				@if(@$course_details->content_availability!='free')
											<a href="javascript:;" class="btn btn_gradient btn_active" id="buy_button"><i class="fa fa-cart"></i> Buy Now</a>
										@else
											<div style="margin-bottom:15px;margin-top:15px">
												<div class="">Download Course
													<div class="col-sm-7">

														@if(empty($trainer_files))
													   		@foreach($course_details->course_files as $key=>$value)
													   			<?php
													   				$file_url = 'javascript:;';
													   				$file_type= ''; 
													   				if(!empty($value['file'])) {

													   				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
													   				        $file_url = TrainerContentImgPath.'/'.$value['file'];
													   				        $file_name= pathinfo($value['file']);
					                                                        $ext      = $file_name['extension'];
					                                                        if($ext=='pdf'){
					                                                        	$file_type = 'pdf';
					                                                        }else{
					                                                        	$file_type = 'video';
					                                                        }
													   				    }
													   				}
													   			?>
													   			@if(@$file_type=='pdf')
													   				({{$key+1}}). Pdf
													   				<a href="{{$file_url}}"  target="_blank">
													   					<i class="fas fa-file-pdf"></i>
													   				</a>
													   			@else
													   			
													   			({{$key+1}}). Video
													   				<a href="{{$file_url}}"  target="_blank">
													   					<i class="fas fa-video"></i>
													   				</a>
													   			@endif

													   		@endforeach
													   			</div>
												   		@else
												   			<a href="{{$file_url}}"  target="_blank">
															<i data-toggle="tooltip" title="{{$title}}" class="fas fa-download cp" data-original-title="{{$title}}"></i></a>
												   		@endif
												   	</p>
												</div>
											</div>
										@endif
									@endif
									
									<!-- <a href="javascript:;" class="btn btn_gradient btn_active"><i class="fa fa-heart"></i> </a> -->
								</div>
								@if(!empty($faqs))
									<div class="iner_crs">
										<h3><i class="fas fa-question"></i> FAQ</h3>
										<div class="faq_sec">
											<!--  -->
											@foreach($faqs as $value)
												<div class="quest_wrp">
													<h6>{{ucfirst($value['title'])}}</h6>
													<p><strong>Ans. </strong>{{ucfirst($value['description'])}}</p>
												</div>
											@endforeach
										</div>
									</div>
								@endif
                			</div>
						</div>
					</div>
				</div>
    		</div>
    	</section>
    	<!-- course details -->
    	@if(empty($trainer_content_page))
	    	@if(!empty($suggested_trainers))
		    	<section class="suggested_trnr">
					<div class="about-header-overlay"></div>
					<div class="container">
						<div class="sec_heading text-center">
		    				<h2>Suggested Courses</h2>
		    				<p class="divider"><img src="{{ url('public/images/system/secdivider.png')}}" class="img-fluid" alt="divider"></p>
		    			</div>
						<div class="team_imgs">
							<div id="fac-slider" class="owl-carousel">
			                    @foreach($suggested_trainers as $key=>$value)
			                    <div class="testimonial">
			                        <div class="testimonial-review">
		                            	<?php

	                            		    if (!empty($value['course_image']['name'])) {
	                            		        // dd($student_details->image);
	                            		        if (file_exists(TrainerContentImageBasePath.'/'.$value['course_image']['name'])) {
	                            		            $image = TrainerContentImageImgPath.'/'.$value['course_image']['name'];
	                            		        }else{
	                            		            $image = DefaultImgPath;
	                            		        }
	                            		    }else{
	                            		        $image = DefaultImgPath;
	                            		    }
		                            		$courses_id = base64_encode($value['id'].'-'.$value['trainer_id']);
		                            		  
		                            	?>
		                                <a href="{{url('/course/detail/'.$courses_id) }}">
				                            <div class="pic">
				                                <img src="{{ $image }}" alt="" class="img-fluid">
				                            </div>
				                        </a>
			                            <h4 class="testimonial-title">
			                                <a href="{{url('/course/detail/'.$courses_id) }}">{{ ucfirst($value['title']) }}
			                                </a>
			                                <small>{{ ucfirst($value['name']) }}</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div>
			                    @endforeach
			                    <!-- <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ url('public/images/system/profile2.jpg')}}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                williamson
			                                <small>Web Developer</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div>
			                    <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ url('public/images/system/profile3.jpg')}}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                williamson
			                                <small>Web Developer</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div>
			                    <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ url('public/images/system/profile1.jpg')}}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                williamson
			                                <small>Web Developer</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div>
			                    <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ url('public/images/system/profile2.jpg')}}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                williamson
			                                <small>Web Developer</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div>
			                    <div class="testimonial">
			                        <div class="testimonial-review">
			                            <div class="pic">
			                                <img src="{{ url('public/images/system/profile3.jpg')}}" alt="" class="img-fluid">
			                            </div>
			                            <h4 class="testimonial-title">
			                                williamson
			                                <small>Web Developer</small>
			                                <ul type="none" class="list-inline">
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                	<li class="list-inline-item"><i class="fa fa-star"></i></li>
			                                </ul>
			                            </h4>
			                        </div>
			                    </div> -->
			                </div>
						</div>
					</div>
				</section>
			@endif
		@endif
		@include('frontEnd.common.offer')
    	@include('frontEnd.common.top_footer')
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var pop_up_cookie = $.cookie("pop_up");
		// var abc = $.cookie("test");
		// alert(pop_up_cookie);
		if(pop_up_cookie==null){
			$('#offer_modal').modal('show');
			// $.cookie("test", 1, { expires : 1 });
		 	$.cookie("pop_up", 1, { expires : 1 });
		}
	});
	
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
    function call_rzor_pay(price,course_id){
        // alert(course_id)
        price = price*100;
        var options = {
            "key": "rzp_test_QYhZB5gAfNTMNX",
            "amount":price,
            "name": "Scamper Skills",
            "currency": "INR",
            // "description":"Course ID:"+course_id,
            "image": "https://pro.subodh.live/scamperskills/public/images/system/logo.png",
            "handler": function (response){
                // alert()
                if(response.razorpay_payment_id != ''){
                    $('.loader').show();
                    $.ajax({
                        method:"post",
                        url:"{{ url('/user/course/pay') }}",
                        data:{
                        	price:price, 
                        	course_id:course_id,
                        	razorpay_payment_id:response.razorpay_payment_id, 
                        	"_token":"{{ csrf_token() }}" },
                        success:function(resp){
                            // alert(resp);
                            if(resp == 'false'){
                                swal("Oops!","Something went wrong. Please try again later,","error");     
                            } else{
                            	if(resp!=''){
                            		
                            		$('#purchased').html(resp);
                                	swal('Course Purchased',"Course purchased successfully","success"); 
                            	}else{
                            		swal("Oops!","Something went wrong. Please try again later,","error");
                            	}
                            }
                            $('.loader').hide();
                        }
                    })
                } else{
                    swal("Oops!","Something went wrong. Please try again later",'error');     
                }
            },
            "theme": {
                "color": "#0449fe"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }

    $('#buy_button').click(function(e){

        var checkAuth = "{{(Auth::check())?'true':'false'}}";
        var user_type = "{{@Auth::User()->user_type}}";
        // alert(user_type);
        if(checkAuth=='true'){
        	if(user_type=='user'){

        		var course_id = "{{@$course_id}}";
        		var price     = "{{@$course_details->final_price}}";
        		// alert(course_id); 
        		var user_id       = "{{@Auth::user()->id}}";
       			call_rzor_pay(price,course_id);
        	}else{
	    		swal({
		            title: "Not Authorized",
		        	text: "You are not authorized to purchase this course.",
		        	icon: "warning",
		        	buttons: true,
		        	dangerMode: true,
		        });
        	}
        }else{
        	window.location.href = "{{url('/login')}}";
        }
    });
</script>
<script type="text/javascript">
	$('.slick_course').slick({
	 	slidesToShow: 1,
	 	slidesToScroll: 1,
	 	arrows: false,
	 	fade: false,
	 	asNavFor: '.slick_thumb',
	 });

	 $('.slick_thumb').slick({
	 	slidesToShow: 4,
	 	slidesToScroll: 1,
	 	asNavFor: '.slick_course',
	 	dots: false,
	 	arrows: false,
	 	focusOnSelect: true,
	 	// responsive: [
	 	// 	{
   //            breakpoint: 991,
   //            settings: {
   //              slidesToShow: 3,
   //            }
   //          },
   //          {
   //            breakpoint: 768,
   //            settings: {
   //              slidesToShow: 2,
   //            }
   //          },
   //          {
   //            breakpoint: 481,
   //            settings: {
   //              slidesToShow: 0,
   //            }
   //          }
   //        ]
	 });
	 $("#fac-slider").slick({
    	dots: false,
    	// arrows: false,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		// responsive: [
		//     {
		//       breakpoint: 768,
		//       settings: {
		//         slidesToShow: 2,
		//       }
		//     },
		//     {
		//       breakpoint: 481,
		//       settings: {
		//         slidesToShow: 1,
		//       }
		//     }
		//   ]
    });
</script>
@stop