@extends('frontEnd.layouts.master')
@section('title','Downloads')
@section('content')
	<div class="page-wrapper">
		@include('frontEnd.common.cms_header')
		<div class="home_page_wrapper inner_page">
			<!-- register -->
			<section class="downloads_sec">
				<div class="container">
					<div class="wrap_downloads wrap_canvas">
						<div class="table_header">
							<div class="row">
								<div class="col-sm-9 col-md-9"><h3>Downloads</h3></div>
								<!-- <div class="col-sm-3 col-md-3">
									<div class="nice_selc">
										<select class="niceselc form-control">
											<option data-display="Downloads">Downloads</option>
											<option value="1">IAS</option>
											<option value="1">CAT</option>
											<option value="1">Maths</option>
										</select>
									</div>
								</div> -->
							</div>
						</div>
						<div class="table_div">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead class="thead-dark">
										<tr>
											<th scope="col">Title</th>
											<th scope="col">Uploaded on</th>
											<th scope="col">Download</th>
										</tr>
									</thead>
									<tbody>
										@if(!empty($downloads))
											@foreach($downloads as $download)
												<tr>
													<td>{{ucfirst($download['name'])}}</td>
													<td>{{date('d/m/Y',strtotime($download['created_at']))}}
													</td>
													<td class="icoss">
														<?php  
														    
														    $file     = DefaultImgPath; 
														    $file_url = 'javascript:;';
														    if(!empty($download['file'])) {
														        if(file_exists(DownloadBasePath.'/'.$download['file'])) {
														            $file_url = DownloadImgPath.'/'.$download['file'];
														        }else{
														        	continue;
														        }
														    }else{
														    	continue;
														    }
														?>
														<a href="{{$file_url}}">
															<i data-toggle="tooltip" title="Download PDF" class="fas fa-download cp"></i>
														</a>
													</td>
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
			 @include('frontEnd.common.top_footer')
		</div>
	</div>
@endsection