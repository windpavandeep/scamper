@extends('frontEnd.layouts.login')
@section('title','Login')
@section('content')
    <style type="text/css">
        .form_head div > .img-fluid {
            width: 70%;
            margin: 30px 0 40px;
        }
    </style>
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="register_sec user_mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2 col-xs-12 col-md-6 offset-md-3">
                            <div class="wrp_white">
                                <div class="sec_heading text-center">
                                    <h2>Login</h2>
                                    <p class="divider"><img src="{{asset(systemImgPath.'/secdivider.png')}}" class="img-fluid" alt="divider"></p>
                                </div>
                                <div class="form_head">
                                    <div class="text-center">
                                        <img src="{{asset(systemImgPath.'/logo.png')}}" class="img-fluid">
                                    </div>
                                    <!-- <h5 class="text-center">Enter your Valid Mobile Number</h5> -->
                                    <?php
                                        $email = Session::get('email');
                                        $password = Session::get('password');
                                    ?>
                                    <form method="post" id="login_form">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Enter Email or Number" value="{{ $email }}">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Enter Password" name="password" value="{{ $password }}">
                                        </div>
                                        <div class="form-group ">
                                            <a href="{{ url('/forgot-password') }}" class="thm-clr">Forgot Password?</a>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                @csrf
                                                <button class="btn btn_gradient btn_active">Login</button>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            Don't have an account? <a href="{{ url('register/user') }}" class="thm-clr">Click here</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#login_form').validate({
        rules:{
            email:{
                required:true
            },
            password:{
                required:true
            }
        }
    })
</script>
@endsection