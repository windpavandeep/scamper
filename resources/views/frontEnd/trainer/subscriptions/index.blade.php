@extends('frontEnd.layouts.master')
@section('title','Subscriptions')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">
            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec subscription_plans">
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Subscriptions</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>

                                    <div class="main_cntnt_dash">
                                        <div class="cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{url('/trainer/subscription/add')}}" class="btn btn_gradient btn_active">Add Subscription Plan</a>
                                            </div>
                                            <div class="row">
                                                @if(!empty($details))
                                                    @foreach($details as $key=>$value)
                                                        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="card_single card_shd text-center bl_grd">
                                                                <!--  -->
                                                                <div class="single_cors wow fadeIn" data-wow-delay=".3s">
                                                                    <div class="cors_head">
                                                                        <h2 class="cors_title">INR {{$value['price']}}</h2>
                                                                        <p>{{ ucfirst($value['title']) }}</p>
                                                                    </div>
                                                                    <div class="cors_body">
                                                                        <ul class="cors_list" type="none">
                                                                         
                                                                                {!! ucfirst($value['description']) !!}
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="cors_body" style="margin-bottom: 10px">
                                                                        @if($value['admin_approval']=='P')
                                                                            Pending for admin approval
                                                                        @elseif($value['admin_approval']=='R')
                                                                            Rejected
                                                                        @else
                                                                            Approved
                                                                        @endif
                                                                    </div>
                                                                    <div class="cors_footer">
                                                                        <a href="{{ url('/trainer/subscription/edit/'.$value['id'])}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                                    </div>
                                                                </div>
                                                                <a href="{{ url('/trainer/subscription/delete/'.$value['id'])}}" style="color:black">
                                                                    <i class="fa fa-times remv_plan"></i>
                                                                </a>
                                                                <!--  -->
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <center style="margin-top: 17%;padding-left: 40%;">No Data Found</center>
                                                @endif
                                               <!--  <div class="col-sm-4">
                                                    <div class="card_single card_shd text-center rd_grd">
                                                 
                                                        <div class="single_cors wow fadeIn" data-wow-delay=".3s">
                                                            <div class="cors_head">
                                                                <h2 class="cors_title">INR 3700</h2>
                                                                <p>Two Months Subscription</p>
                                                            </div>
                                                            <div class="cors_body">
                                                                <ul class="cors_list" type="none">
                                                                    <li><i class="fa fa-check"></i>Unlimited user</li>
                                                                    <li><i class="fa fa-check"></i>Social activity analytics</li>
                                                                    <li><i class="fa fa-times"></i>customr care</li>
                                                                </ul>
                                                            </div>
                                                            <div class="cors_footer">
                                                                <a href="javascript:;" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                            </div>
                                                        </div>
                                                        <i class="fa fa-times remv_plan"></i>
                                           
                                                    </div>
                                                </div> -->
                                               <!--  <div class="col-sm-4">
                                                    <div class="card_single card_shd text-center gr_grd">
                                                 
                                                        <div class="single_cors wow fadeIn" data-wow-delay=".3s">
                                                            <div class="cors_head">
                                                                <h2 class="cors_title">INR 5500</h2>
                                                                <p>Three Months Subscription</p>
                                                            </div>
                                                            <div class="cors_body">
                                                                <ul class="cors_list" type="none">
                                                                    <li><i class="fa fa-check"></i>Unlimited user</li>
                                                                    <li><i class="fa fa-check"></i>Social activity analytics</li>
                                                                    <li><i class="fa fa-check"></i>customr care</li>
                                                                </ul>
                                                            </div>
                                                            <div class="cors_footer">
                                                                <a href="javascript:;" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                            </div>
                                                        </div>
                                                        <i class="fa fa-times remv_plan"></i>
                                                       
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection