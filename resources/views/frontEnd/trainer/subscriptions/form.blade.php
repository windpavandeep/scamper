<?php
    if(isset($subscription_plan_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('frontEnd.layouts.master')
@section('title',$task.' Subscription Plan')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="home_page_wrapper inner_page trnr_dash_div">
        @include('frontEnd.common.dashboard_header')
        <section class="sec_dashboard db_main">
            <div class="container-fluid">
                <div class="wrap_dash_sec add_subs_plan"> <!-- change class -->
                    <div class="row"> 
                        <div class="col-sm-3 col-md-4">
                            <div class="sidebar_wrap card_shd">
                                @include('frontEnd.trainer.common.sidebar')
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-8">
                            <div class="mainside_wrap">
                                <!--  -->
                                <div class="page_head">
                                    <h4>{{$task}} Subscription Plan</h4>
                                    <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                </div>
                                <div class="main_cntnt_dash">
                                    <div class="card_shd cnt_skamper">
                                        <!--  -->
                                        <div class="cont_shd_frm">
                                            <div class="row">
                                                <div class="col-sm-10 offset-1">
                                                    <form class="" method="post" action="" id="subscription_plan">
                                                        <div class="form-group">
                                                            <label>{{$task}} Plan Title</label>
                                                            <input type="text" class="form-control" placeholder="Enter Plan Title" name="title" value="{{ isset($details['title'])? $details['title']: '' }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{$task}} Plan Price</label>
                                                            <input type="text" class="form-control" placeholder="Enter Plan Price" name="price" value="{{ isset($details['price'])? $details['price']: '' }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{$task}} Plan Validity(In Weeks)</label>
                                                            <input type="text" class="form-control" placeholder="Enter Plan Validity In Weeks" name="plan_validity" value="{{ isset($details['validity'])? $details['validity']: '' }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label>{{$task}} Description</label>
                                                            <textarea class="form-control ckeditor" placeholder="Add Benefits of the Plans" name="description" id="description">{{ isset($details['description'])? $details['description']: '' }}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="text-right log_btn">
                                                                <button class="btn btn_gradient btn_active" type="submit">{{$task}} Plan</button>
                                                            </div>
                                                        @csrf
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('frontEnd.common.top_footer')
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        CKEDITOR.replace('editor');
    </script>
    <script type="text/javascript">
        $('#subscription_plan').validate({
            ignore: [],
            rules:{
                title:{
                    required:true,
                    minlength:2,
                    maxlength:35,
                    regex:/^[a-zA-z ]+$/
                },
                price:{
                    required:true,
                    min:1,
                    maxlength:7,
                    digits:true,
                },
                plan_validity:{
                    required:true,
                    min:1,
                    maxlength:2,
                    digits:true,
                },
                description:{
                   required: function(){
                       CKEDITOR.instances.description.updateElement();
                   },
                   minlength:2,
                   maxlength:500,
                },
            },
            messages:{
                plan_validity:{
                    min:'Please enter a digit greater than or equal to 1.',
                },
            },
            errorPlacement:function(error,element){
         
                error.appendTo(element.parent().after());
                
            },
        })
    </script>
@endsection