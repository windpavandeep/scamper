@extends('frontEnd.layouts.master')
@section('title','Profile')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec view_prof_page"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>My Profile</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{url('/trainer/profile/edit')}}" class="btn btn_gradient btn_active">Edit Profile</a>
                                            </div>
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-sm-10 offset-1">
                                                        <form class="" method="" action="" id="">
                                                            <div class="text-center img_user">
                                                                <div class="pos_rel pic_top">
                                                                    <?php
                                                                        $image = DefaultUserPath;
                                                                        $usr_img = $trainer_details->user_details->image;
                                                                        if(!empty($usr_img)){
                                                                            $image = TrainerProfileImgPath.'/'.$usr_img;
                                                                        }
                                                                        // echo '1'; die;
                                                                    ?>
                                                                    <img src="{{ $image }}" class="img-fluid" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>First Name</label>
                                                                        <!-- <input type="text" class="form-control" value="James"> -->
                                                                        <input type="text" class="form-control" value="{{ ucfirst($trainer_details->user_details->first_name) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Last Name</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst($trainer_details->user_details->last_name) }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Date of Birth</label>
                                                                <input type="text" class="form-control dtpckr" value="{{ date('d-m-Y',strtotime(@$trainer_details->user_details->dob)) }}" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="email" class="form-control" value="{{ $trainer_details->user_details->email }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Gender</label>
                                                                    <select class="niceselc form-control">
                                                                        <option data-display="Select Gender" readonly>Male</option>
                                                                        <option value="1" <?php if($trainer_details->user_details->gender=='male'){echo'selected';}?>>Male</option>
                                                                        <option value="3" <?php if($trainer_details->user_details->gender=='female'){echo'selected';}?>Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Country</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($trainer_details->user_details->country))
                                                                            <option value="selected">{{$trainer_details->user_details->country->name}}</option>
                                                                        @else
                                                                            <option value="">Select Country</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>State</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($trainer_details->user_details->state))
                                                                            <option value="selected">{{$trainer_details->user_details->state->name}}</option>
                                                                        @else
                                                                            <option value="">Select State</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>City</label>
                                                                            <select class="niceselc form-control">
                                                                                @if(!empty($trainer_details->user_details->city))
                                                                                    <option value="selected">{{ ucfirst($trainer_details->user_details->city->name) }}</option>
                                                                                @else
                                                                                    <option value="">Select City</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Area Pincode</label>
                                                                        <input type="text" class="form-control" value="{{ $trainer_details->pincode }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>District</label>
                                                                    <input type="text" class="form-control" value="{{ ucfirst(@$trainer_details->user_details->district) }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" value="{{ ucfirst(@$trainer_details->user_details->address) }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Employement Status</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($trainer_details->employment_status))
                                                                            <option value="selected">{{$trainer_details->employment_status->name}}</option>
                                                                        @else
                                                                            <option value="">Select Employment Status</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Name of Organization</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst(@$trainer_details->organization_name) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Total Experience</label>
                                                                        <input type="text" class="form-control" value="{{@$trainer_details->experience }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>Your Domain</label>
                                                                            <select class="niceselc form-control">
                                                                                @if(!empty($trainer_details->domain))
                                                                                    <option value="selected">{{$trainer_details->domain->name}}</option>
                                                                                @else
                                                                                    <option value="">Select Domain Name</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>Subject</label>
                                                                            <select class="niceselc form-control">
                                                                                @if(!empty($trainer_details->subject))
                                                                                    <option value="selected">{{$trainer_details->subject->name}}</option>
                                                                                @else
                                                                                    <option value="">Select Subject Name</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
           @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
