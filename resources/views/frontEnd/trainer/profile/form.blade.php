@extends('frontEnd.layouts.master')
@section('title','Edit Profile')
@section('content')

    <div class="page-wrapper">
    	@include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec edit_prof_page"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Edit Profile</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{url('/trainer/change-password')}}" class="btn btn_gradient btn_active">Change Password</a>
                                            </div>
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-sm-10 offset-1">
                                                        <form class="" method="post" action="{{url('/trainer/profile/edit')}}" id="trainer_profile" enctype="multipart/form-data">
                                                            <div class="text-center img_user">
                                                                <div class="pos_rel pic_top">
                                                                    <span class="img_edtt pos_rel">
                                                                        <?php
                                                                            $image = DefaultUserPath;
                                                                            $usr_img = $trainer_details->user_details->image;
                                                                            if(!empty($usr_img)){
                                                                                $image = TrainerProfileImgPath.'/'.$usr_img;
                                                                            }
                                                                            // echo '1'; die;
                                                                        ?>
                                                                        <img src="{{ $image }}" class="img-fluid" id="prof_ch">
                                                                        <span class="edt_inpt">
                                                                            <i class="far fa-edit"></i>
                                                                            <input type="file" name="image" class="file_img">
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>First Name</label>
                                                                        <!-- <input type="text" class="form-control" value="James"> -->
                                                                        <input type="text" class="form-control" value="{{ucfirst(@$trainer_details->user_details->first_name)}}" name="first_name" placeholder="Enter First Name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Last Name</label>
                                                                        <input type="text" class="form-control" value="{{ucfirst(@$trainer_details->user_details->last_name)}}" name="last_name" placeholder="Enter Last Name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Date of Birth</label>
                                                                <input type="text" class="form-control dtpckr" value="{{date('d-m-Y',strtotime(@$trainer_details->user_details->dob))}}" autocomplete="off" name="dob" placeholder="DD/MM/YYYY">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="email" class="form-control" value="{{@$trainer_details->user_details->email}}" name="email" disabled="">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Gender</label>
                                                                    <select class="niceselc form-control" name="gender">
                                                                        <option data-display="Select Gender" value="">Select Gender</option>
                                                                        <option value="male" <?php if($trainer_details->user_details->gender=='male'){echo "selected";}?>>Male</option>
                                                                        <option value="female" <?php if($trainer_details->user_details->gender=='female'){echo "selected";}?>>Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Country</label>
                                                                    <select class="niceselc form-control" name="country_id" id="country_id">
                                                                        <option data-display="Select Country" value="">Select Country</option>
                                                                        @foreach($countries as $country)
                                                                            <option value="{{ $country['id'] }}" <?php if($trainer_details->user_details->country_id==$country['id']){echo 'selected';}?>>{{ ucfirst($country['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc state_div">
                                                                    <label>State</label>
                                                                    <select class="niceselc form-control" name="state_id" id="state_id">
                                                                        <option data-display="Select State" value="">Select State</option>
                                                                        @foreach($states as $key=>$value)
                                                                            <option value="{{ $value['id'] }}" <?php if($trainer_details->user_details->state_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                        @endforeach
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>City</label>
                                                                            <select class="niceselc form-control"  name="city_id" id="city_id">
                                                                                <option data-display="Select City"  value="">Select City</option>
                                                                                @foreach($cities as $key=>$value)
                                                                                    <option value="{{ $value['id'] }}"  <?php if($trainer_details->user_details->city_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Area Pincode</label>
                                                                        <input type="text" class="form-control" placeholder="Enter Pincode" name="pincode" value="{{ @$trainer_details->pincode }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>District</label>
                                                                    <input type="text" class="form-control" value="{{ ucfirst(@$trainer_details->user_details->district) }}" name="district">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" value="{{ ucfirst(@$trainer_details->user_details->address) }}" name="address">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Employement Status</label>
                                                                   <select class="niceselc form-control" name="employment_status_id">
                                                                        <option data-display="Select Employement Status" value="">Select Employement Status</option>
                                                                        @foreach($employee_status as $value)
                                                                            <option value="{{ $value['id'] }}"  <?php if($trainer_details->employment_status_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                        @endforeach
                                                                   </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Name of Organization</label>
                                                                        <input type="text" class="form-control" value="{{ucfirst(@$trainer_details->organization_name)}}" name="organization_name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Total Experience</label>
                                                                        <input type="text" class="form-control" value="{{ @$trainer_details->experience }}" name="total_experience" placeholder="Enter Total Experience">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>Your Domain</label>
                                                                            <select class="niceselc form-control" name="domain_id">
                                                                                <option data-display="Choose Domain" value="">Choose Domain</option>
                                                                                @foreach($domains as $domain)
                                                                                    <option value="{{ $domain['id'] }}" <?php if($trainer_details->trainer_domain_id==$domain['id']){echo 'selected';}?>>{{ ucfirst($domain['name']) }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nice_selc">
                                                                            <label>Subject</label>
                                                                            <select class="niceselc form-control" name="subject_id">
                                                                                <option data-display="Select Subject" value="">Select Subject</option>
                                                                                @foreach($subject as $key=>$value)
                                                                                    <option value="{{ $value['id'] }}" <?php if($trainer_details->subject_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="text-right log_btn">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn_gradient btn_active">Update Information</button><!-- 
                                                                    <a href="trainerProfile.php" class="btn btn_gradient btn_active"></a> -->
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('.file_img').on('change', function () {
          var input = this;
            var reader = new FileReader();
            reader.onload = function(){
              var dataURL = reader.result;
             var output = document.getElementById('prof_ch');
              output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        });
    </script>
    <script type="text/javascript">
        $('#country_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/states') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#city_id').html('<option value="">Select City </option>');
                    $("#state_id").selectpicker("destroy");
                    $('#state_id').html(resp);
                    $("#state_id").selectpicker("destroy");
                    $("#state_id").selectpicker("refresh");
                    $("#city_id").selectpicker("destroy");
                    $("#city_id").selectpicker("refresh");
                }
            })
        });

        $('#state_id').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/cities') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#city_id').html(resp);
                    $("#city_id").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });
    </script>
    <script type="text/javascript">
        $('#trainer_profile').validate({
            rules:{
                first_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                last_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                dob:{
                    required:true
                },
                address:{
                    required:true,
                    minlength:2,
                    maxlength:500
                },
                district:{
                    required:true,
                    minlength:2,
                    maxlength:150
                },
                gender:{
                    required:true
                },
                country_id:{
                    required:true
                },
                state_id:{
                    required:true
                },
                city_id:{
                    required:true
                },
                domain_id:{
                    required:true
                },
                employment_status_id :{
                    required:true
                },
                total_experience:{
                    required:true,
                    number:true,
                    maxlength:4,
                    regex:/^[0-9 .]+$/,
                },
                subject_id:{
                    required:true     
                },
                image:{
                    accept: "jpg|jpeg|png"
                },  
                pincode:{
                    required:true,
                    minlength:4,
                    maxlength:10,
                    regex:/^[0-9]+$/,     
                },
                organization_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z - ']+$/
                }
            },
            messages:{
                image:{
                    accept:'Please select an image of .jpeg, .jpg, .png file format.',
                },
            },
            errorPlacement:function(error,element){
                 error.appendTo(element.parent().after());
            },
        })
    </script>
    <script type="text/javascript">
        $('.state_div').on('click', function(){
            // alert('io');
            $("#state_id").selectpicker("refresh");
        })
    </script>
@endsection
        <!-- script files -->