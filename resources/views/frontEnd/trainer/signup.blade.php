@extends('frontEnd.layouts.login')
@section('title','Teacher Signup')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
        	<section class="register_sec user_regst trnr_reg">
        		<div class="container">
                    <div class="row">
                        <div class="col-md-8 offset-md-2 col-sm-12">
                            <div class="wrp_white">
                                <div class="sec_heading text-center">
                                    <h2>Signup as a Teacher</h2>
                                    <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                </div>
                                <div class="form_head text-center">
                                    <!-- <h5>Enter the OTP sent on the registered mobile number</h5> -->
                                    <form class="" method="post" action="" id="trainer_signup">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Last name" name="last_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control dtpckr" placeholder="Date of Birth" autocomplete="off" name="dob">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter Valid Email" name="email">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Create Password" name="password" id="password">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="gender">
                                                    <option data-display="Select Gender" value="">Select Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="country_id" id="country_id">
                                                    <option data-display="Select Country" value="">Select Country</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="state_id" id="state_id">
                                                    <option data-display="Select State" value="">Select State</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="city_id" id="city_id">
                                                            <option data-display="Select City" value="">Select City</option>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="niceselc form-control" placeholder="Enter Pincode" name="pincode">
                                                </div>
                                            </div>
                                        </div>
                                            <div class="form-group">
                                                <input type="text" class="niceselc form-control" placeholder="District" name="district">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="niceselc form-control" placeholder="Address" name="address">
                                            </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="employment_status_id">
                                                    <option data-display="Employement Status" value="">Employement Status</option>
                                                    @foreach($employee_status as $value)
                                                        <option value="{{ $value['id'] }}">{{ ucfirst($value['name']) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Name of Organization" name="organization_name">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Total Experience(in years)" name="total_experiance">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Name of Organization">
                                        </div> -->
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="language_id">
                                                    <option data-display="Choose Category" value="" readonly>Choose language</option>
                                                    @foreach(@$languages as $language)
                                                        <option value="{{ $language['id'] }}">{{ucfirst($language['name'])}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="domain_id">
                                                            <option data-display="Choose Domain" value="">Choose Domain</option>
                                                            @foreach($domains as $domain)
                                                                <option value="{{ $domain['id'] }}">{{ ucfirst($domain['name']) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="nice_selc">
                                                        <select class="niceselc form-control" name="subject_id">
                                                            <option data-display="Select Subject" value="">Select Subject</option>
                                                            @foreach($subject as $key=>$value)
                                                                <option value="{{ $value['id'] }}">{{ ucfirst($value['name']) }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            
                                                <label class="custom_label text-left">
                                                    <a href="{{url('/term-conditions')}}" target="blank"> 
                                                        I agree to the Privacy & Terms
                                                    </a>
                                                    <input type="checkbox" checked="checked" name="terms">
                                                    <span class="checkmark"></span>
                                                </label>
                                            
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                @csrf
                                                <button type="submit" class="btn btn_gradient btn_active">Submit for Admin Approval</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <p class="text-center">Already have an account? <a href="{{ url('login') }}">Click here</a></p>
                            </div>
                        </div>
                    </div>
                </div>
        	</section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#country_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/states') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#state_id').html(resp);
                    $('#city_id').html('<option value="">Select City </option>');
                    $("#state_id").selectpicker("refresh");
                    $("#city_id").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });

        $('#state_id').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/cities') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#city_id').html(resp);
                    $("#city_id").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });
    </script>
    <script type="text/javascript">
        $('#trainer_signup').validate({
            rules:{
                first_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                last_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                email:{
                    required:true,
                    email:true,
                    remote:"{{ url('/validate/email') }}"
                },
                dob:{
                    required:true
                },
                address:{
                    required:true,
                    minlength:2,
                    maxlength:500
                },
                district:{
                    required:true,
                    minlength:2,
                    maxlength:150
                },
                password:{
                    required:true,
                    regex:/^[a-zA-z 0-9 -)*&^$#@!(,.]+$/
                },
                confirm_password:{
                    required:true,
                    equalTo:"#password"
                },
                gender:{
                    required:true
                },
                country_id:{
                    required:true
                },
                state_id:{
                    required:true
                },
                city_id:{
                    required:true
                },
                domain_id:{
                    required:true
                },
                terms:{
                    required:true
                },
                employment_status_id :{
                    required:true
                },
                total_experiance:{
                    required:true,
                    maxlength:4,
                    regex:/^[0-9 .]+$/,
                },
                subject_id:{
                    required:true     
                },
                pincode:{
                    required:true,
                    minlength:4,
                    maxlength:10,
                    regex:/^[0-9]+$/,     
                },
                organization_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z - ']+$/
                },
                language_id:{
                    required:true,    
                }
            },
            messages:{
                email:{
                    remote:"This email-id is already registered"
                },
                password:{
                    regex:"Password can only consist of alphabets, numbers and special characters"
                },
                confirm_password:{
                    equalTo:"Please enter the same password again"
                }
            },
            errorPlacement:function(error,element){
                if(element.attr("name") == "terms") {
                    error.appendTo(element.parent().parent().after());
                } else{
                    error.appendTo(element.parent().after());
                }
            },
        })
    </script>
@endsection