@extends('frontEnd.layouts.master')
@section('title','Dashbaord')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">
            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec db_overview">
                        <div class="row">
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Overall Performance</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua. <a class=""> Upgrade Plan</a></h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/subsr.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>0</h5>
                                                    <p>Total Subscribed Users</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/subsr.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>0</h5>
                                                    <p>Total Subscribed Users</p>
                                                    <p class="th_mon"><small>This Month</small></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/vidlec.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>{{@$video_count}}</h5>
                                                    <p>Total Videos</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/note.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>{{@$pdf_count}}</h5>
                                                    <p>Total Notes Uploaded</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/timer.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>INR {{@$pending_earning_count}}</h5>
                                                    <p>Pending Earnings</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="card_single card_shd text-center">
                                                    <img src="{{ asset(systemImgPath.'/toter.png') }}" class="img-fluid" alt="Info image">
                                                    <h5>INR {{@$total_earning_count}}</h5>
                                                    <p>Overall Total Earning</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
