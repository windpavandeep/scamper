@extends('frontEnd.layouts.master')
@section('title','Add Content')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
	@include('frontEnd.common.dashboard_header')
    <div class="home_page_wrapper inner_page trnr_dash_div">
        <section class="sec_dashboard db_main">
            <div class="container-fluid">
                <div class="wrap_dash_sec change_pswd"> <!-- change class -->
                    <div class="row"> 
                        <div class="col-sm-3 col-md-4">
                            <div class="sidebar_wrap card_shd">
                                @include('frontEnd.trainer.common.sidebar')
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-8">
                            <div class="mainside_wrap">
                                <!--  -->
                                <div class="page_head">
                                    <h4>Add Content</h4>
                                    <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                </div>
                                <div class="main_cntnt_dash">
                                    <div class="card_shd cnt_skamper">
                                        <!--  -->
                                        <div class="cont_shd_frm">
                                            <div class="row">
                                                <div class="col-sm-10 offset-1">
                                                    <form class="" method="post" action="" id="add_content_form" enctype="multipart/form-data">
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose Category</label>
                                                                <select class="niceselc form-control" name="category_id" id="category_id">
                                                                    <option data-display="Choose Category" value="" readonly>Choose Category</option>
                                                                    @foreach($categories as $category)
                                                                        <option value="{{ $category['id'] }}">{{ucfirst($category['name'])}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose Sub-Category</label>
                                                                <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                                                    <option data-display="Choose Category" value="" readonly>Choose Sub-Category</option>
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose Sub-Category 2</label>
                                                                <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                                                    <option data-display="Choose Category" value="" readonly>Choose Sub-Category 2</option>
                                                                    
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose Subject</label>
                                                                <select class="niceselc form-control sub_sub_category_class"  name="subject_id" id="subject_id">
                                                                    <option data-display="Choose Subject" value="" readonly>Choose Subject</option>
                                                                    @foreach($subjects as $subject)
                                                                        <option value="{{ $subject['id'] }}">{{ ucfirst($subject['name']) }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Choose Unit</label>
                                                            <select class="niceselc form-control" name="unit_id" id="unit_id">
                                                                <option data-display="Choose Unit" value="" readonly>Choose Unit</option>
                                                                
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose language</label>
                                                                <select class="niceselc form-control" name="language_id">
                                                                    <option data-display="Choose Category" value="" readonly>Choose language</option>
                                                                    @foreach($languages as $language)
                                                                        <option value="{{ $language['id'] }}">{{ucfirst($language['name'])}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                            <input type="text" class="form-control" name="title" placeholder="Title">
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="nice_selc">
                                                                <label>Choose upload option</label>
                                                                <select class="niceselc form-control" name="upload_optn" id="upload_optn">
                                                                    <option value="" readonly>Choose upload option</option>
                                                                    <option value="pdf">Pdf</option>
                                                                    <option value="video">Video</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="pdf_div" style="display: none">
                                                            <span class="error" id="pdf_file_req" style="color:red"></span>
                                                            <label>Upload PDF</label>
                                                            <input type="file" class="form-control" name="pdf" placeholder="Upload PDF" accept=".pdf" id="pdf_file">
                                                            <span class="error" id="pdf_file_req" style="color:red"></span>
                                                        </div>
                                                        <div class="form-group" id="video_div" style="display: none">
                                                            <label>Upload Video</label>
                                                            <input type="file" class="form-control" name="video" placeholder="Upload Video" accept=".mp4,.mov,.avi" id="video_file">
                                                            <span class="error" id="vide_req" style="color:red"></span>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="custom_label text-left"> Free
                                                                        <input type="radio" checked="checked" value="free" name="content_type" class="abc">
                                                                        <span class="checkmark radio"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <div class="form-group">
                                                                    <label class="custom_label text-left"> Paid
                                                                        <input type="radio" value="paid" name="content_type" class="abc">
                                                                        <span class="checkmark radio"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="form-group" id="paid_amount_container">
                                                                    <!-- <label>Add Money(INR)</label> -->
                                                                    <input type="text" name="paid_amount" class="form-control" placeholder="If Paid" id="paid_amount">
                                                                    <span class="error" id="payment_error" style="color:red"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Add Description</label>
                                                            <textarea class="form-control ckeditor" name="description" id="description" placeholder="Add Description of the content why users should buy this?"></textarea>
                                                        </div>
                                                        <div class="pic-outer text-left">
                                                            <label>Upload Images
                                                                </label>
                                                            <div class="form-group form-group-8">
                                                                <div class="write_review_modal_image_container rvw_upld">                          
                                                                    <div class="photo-container">
                                                                        <div class="photos-sec">
                                                                            <div class="flex">
                                                                            </div>
                                                                        </div>
                                                                        <div class="add-phots">
                                                                            <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload Image</a>
                                                                            <input type="file" name="images[]" id="id_proof" class="review_modal_image rem0 id_proof">
                                                                        </div>
                                                                        <span id="img-error" style="color: red;"></span>
                                                                    </div>                                                      
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" >
                                                            <div class="text-right log_btn">
                                                                @csrf
                                                                <button type="submit" class="btn btn_gradient btn_active">Add Course</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    	@include('frontEnd.common.top_footer')
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#category_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                $("#sub_category").selectpicker("refresh");

                $("#unit_id").html('<option data-display="Choose Unit" value="">Choose Unit</unit_id>');
                $("#unit_id").selectpicker("refresh");
                $("#subject_id").html('<option data-display="Choose Subject" value="">Choose Subject</option>');
                $("#subject_id").selectpicker("refresh");

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $("#sub_subcategory").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        var sc = $(this).val();
 
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $("#sub_subcategory").selectpicker("refresh");

                $("#unit_id").html('<option data-display="Choose Unit" value="">Choose Unit</unit_id>');
                $("#unit_id").selectpicker("refresh");
                $("#subject_id").html('<option data-display="Choose Subject" value="">Choose Subject</option>');
                $("#subject_id").selectpicker("refresh");

                $('.loader').hide();
            }
        })
    });
    
</script>
<script type="text/javascript">
    $('#sub_subcategory').on('change', function (){
        var value = $(this).val();

        if(value==''){

            value = '0';
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('/get/subjects') }}"+"/"+value,
            
            success:function(resp){
                $("#unit_id").html('<option data-display="Choose Unit" value="">Choose Unit</unit_id>');
                $("#unit_id").selectpicker("refresh");
                $('#subject_id').html(resp);
                $("#subject_id").selectpicker("refresh");
                $('.loader').hide();
            },
        });
    });
</script>
<script type="text/javascript">
    $('#subject_id').on('change', function (){
        var value = $(this).val();

        if(value==''){

            value = '0';
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('/get/units') }}"+"/"+value,
            
            success:function(resp){
                $('#unit_id').html(' <option value="">Choose Unit</option>');
                $('#unit_id').html(resp);
                $("#unit_id").selectpicker("refresh");
                $('.loader').hide();
            },
        });
    });
</script>

<script type="text/javascript">
    $('#upload_optn').on('change',function(){
        var selectd = $(this).val();
        if(selectd == 'pdf'){
            $('#video_div').css('display','none');
            $('#pdf_div').css('display','block');
        } else if(selectd == 'video'){
            $('#pdf_div').css('display','none');
            $('#video_div').css('display','block');
        } else{
            $('#pdf_div').css('display','none');
            $('#video_div').css('display','none');
        }
    })
</script>
<script type="text/javascript">
    CKEDITOR.replace('editor');
</script>
<script type="text/javascript">
    $('.abc').click(function(){
        var value = $(this).val();
        if(value=='paid'){
            $('#paid_amount_container').show();
        }
        if(value=='free'){
            $('#paid_amount_container').hide();
        }
    });
    $(document).ready(function(){
        $('#paid_amount_container').hide();        
    })
</script>
<script type="text/javascript">
    $('#add_content_form').validate({
        ignore: [],
        rules:{

            category_id:{
                required:true,
            },
            sub_category_id:{
               required:true,
            },
            sub_subcategory_id:{
                required:true,
            },
            subject_id:{
                required:true,
            },

            title:{
                required:true,
                minlength:2,
                maxlength:255,
            },
            upload_optn:{
                required:true,
            },
            content_type:{
                required:true,
            },
            description:{
                required: function(){
                    CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:1200,
            },
            language_id:{
                required:true,
            },
            "images[]":{
                required:true,
            },
            paid_amount:{
                min:1,
                digits:true,
                maxlength:7,
            },
        },

        errorPlacement:function(error,element){
            if(element.attr("name") == "images[]") {
                error.appendTo(element.parent().parent().after());
            }else{

                error.appendTo(element.parent().after());
            }   
        },

        submitHandler:function(form){
            var content_type = $('input[name=content_type]:checked', '#add_content_form').val()
            var upload_optn  = $('#upload_optn').val();
            var paid_amount  = $('#paid_amount').val();
            // alert(upload_optn);
            $('#pdf_file_req').text('');
            $('#vide_req').text('');
            if(upload_optn=='pdf'){
                var pdf_file = $("#pdf_file")[0].files.length;
                if(pdf_file==0){
                    $('#pdf_file_req').text('*This field is required.');
                    return false;
                }

            }
            if(upload_optn=='video'){
                var video_file = $("#video_file")[0].files.length;
                if(video_file==0){
                    $('#vide_req').text('*This field is required.');
                    return false;
                }
            }
            $('#payment_error').text('');
            if(content_type=='paid'){

                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }
            // alert(content_type);
            form.submit();
        }, 
    });
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_image', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='jpg' || extension=='jpeg' || extension=='png')){
                dataURL = reader.result;
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only jpg,jpeg,png file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld img');
            output.src = dataURL;
            var len = $('.photos-sec > .flex > div').length;
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.photos-sec> .flex').append('<div class="flex-item uploaded-section"><div class="phot-upld"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="images['+inc_val+']" class="review_modal_image rem'+inc_val+' id_proof">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }
            /*}else{
                $('#img-error').text('Maximum file size can be 2 MB');
            }*/                       
            
            //this_addr.after('<input type="file" name="upld-gal-hidden['+inc_val+']" accept="image/*" class="review_modal_image" id="upld-gal-hidden['+inc_val+']">')

            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.photos-sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>

@endsection
