@extends('frontEnd.layouts.master')
@section('title','Contents')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec cntnt_mgmt"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Content Management</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                        <div class="text-right log_btn">
                                            <a href="{{ url('/trainer/content/add') }}" class="btn btn_gradient btn_active">Add Content</a>
                                        </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper pad-20">
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="wrap_cms_table">
                                                            <div class="sort_table">
                                                                <form>
                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="nice_selc">
                                                                                    <select class="niceselc form-control" id="category_id">
                                                                                        <option data-display="Category" value="">Category</option>
                                                                                        @foreach($categories as $category)
                                                                                            <option value="{{ $category['id'] }}">{{ucfirst($category['name'])}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                                                            <div class="form-group">
                                                                                <div class="nice_selc">
                                                                                    <select class="niceselc form-control" id="subject_id">
                                                                                        <option data-display="Subject" value="">Subject</option>

                                                                                        @foreach($subjects as $subject)
                                                                                            <option value="{{$subject['id']}}">{{ucfirst($subject['name'])}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="form-group text-center">
                                                                                <label class="lab_tog">Pdf</label>
                                                                                <label class="switch">
                                                                                    <input type="checkbox" id="content_type" checked="">
                                                                                    <span class="toglr round"></span>
                                                                                </label>
                                                                                <label class="lab_tog">Videos</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered">
                                                                    <thead class="thead-dark">
                                                                        <tr>
                                                                            <th>Sr No.</th>
                                                                            <th>Title</th>
                                                                            <th>Category</th>
                                                                            <th>Uploaded on</th>
                                                                            <th>Type</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="conetnt_data">
                                                                        @if(!empty($details))
                                                                            @foreach($details as $key=>$value)
                                                                                <tr>
                                                                                    <td>{{$key+1}}</td>
                                                                                    <td>{{ ucfirst($value['title'] ) }}</td>
                                                                                    <td>{{ ucfirst($value['category_name']) }}</td>
                                                                                    <td>{{ date('d/m/y',strtotime($value['created_at'])) }}</td>
                                                                                    <td>{{ ucfirst($value['content_availability']) }}</td>
                                                                                    <td class="icoss">
                                                                                        <?php
                                                                                            if($value['upload_type']=='pdf'){
                                                                                                $title = 'Download PDF';
                                                                                            }else{
                                                                                                $title ='Download Video';
                                                                                            }

                                                                                        ?>
                                                                                        <?php  
                                                                                            
                                                                                            // $image = DefaultImgPath; 
                                                                                            $file = '';
                                                                                            $file_url = 'javascript:;';
                                                                                            if(!empty($value['file'])) {
                                                                                                if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
                                                                                                    $file_url = TrainerContentImgPath.'/'.$value['file'];
                                                                                                }
                                                                                            }   
                                                                                            // dd($file_url);
                                                                                        ?>
                                                                                        <a href="{{$file_url}}"  target="_blank">
                                                                                           
                                                                                            <i data-toggle="tooltip" title="{{$title}}" class="fas fa-download cp" data-original-title="{{$title}}"></i>
                                                                                        </a>
                                                                                        <a href="{{url('/trainer/content/delete/'.$value['id'])}}">
                                                                                            <i data-toggle="tooltip" title="Remove" class="fas fa-trash cp rmv_tr"></i>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr >
                                                                                <td colspan="6" style="text-align: center;">No Record Found</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#content_type').change(function(){

            var content_type = $("#content_type").is(":checked"); 
            
            var category_id      = $('#category_id').val();
            var subject_id       = $('#subject_id').val();
            $('.loader').show();
            // alert(content_type);
            // alert(category_id);
            // alert(subject_id);
            if(content_type==true){
                content_type='pdf';
            }else{
                content_type='video';
            }

            $.ajax({
                type:"post",
                url: "{{ url('/trainer/get/content/data') }}",
                data:{
                    _token:"{{csrf_token()}}",
                    category_id:category_id,
                    subject_id:subject_id,
                    content_type:content_type,
                },
                success:function(resp){
                    // alert(resp);

                    $('#conetnt_data').html(resp);
                    $('.loader').hide();
                },
            })
           // $('.loader').show();
        });
        $('#category_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {

            var content_type = $("#content_type").is(":checked"); 
            var subject_id   = $('#subject_id').val();
            var category_id  = clickedIndex;
            $('.loader').show();
            // alert(content_type);
            if(content_type==true){
                content_type='pdf';
            }else{
                content_type='video';
            }

            $.ajax({
                type:"post",
                url: "{{ url('/trainer/get/content/data') }}",
                data:{
                    _token:"{{csrf_token()}}",
                    category_id:category_id,
                    subject_id:subject_id,
                    content_type:content_type,
                },
                success:function(resp){
                    // alert(resp);

                    $('#conetnt_data').html(resp);
                    $('.loader').hide();
                },
            })
            
        });
        $('#subject_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {

            var content_type = $("#content_type").is(":checked");  
            var category_id  = $('#category_id').val();
            var subject_id   = clickedIndex;
             $('.loader').show();
            if(content_type==true){
                content_type='pdf';
            }else{
                content_type='video';
            }

            $.ajax({
                type:"post",
                url: "{{ url('/trainer/get/content/data') }}",
                data:{
                    _token:"{{csrf_token()}}",
                    category_id:category_id,
                    subject_id:subject_id,
                    content_type:content_type,
                },
                success:function(resp){
                    // alert(resp);

                    $('#conetnt_data').html(resp);
                    $('.loader').hide();
                },
            })
        });
     
    </script>
@endsection
