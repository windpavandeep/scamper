@extends('frontEnd.layouts.master')
@section('title','Chats')
@section('content')

<div class="page-wrapper">
	@include('frontEnd.common.dashboard_header')

    <div class="home_page_wrapper inner_page trnr_dash_div">

        <section class="sec_dashboard db_main">
            <div class="container-fluid">
                <div class="wrap_dash_sec chat_mgmt"> <!-- change class -->
                    <div class="row"> 
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <div class="sidebar_wrap card_shd">
                                @include('frontEnd.trainer.common.sidebar')
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                            <div class="mainside_wrap">
                                <!--  -->
                                <div class="page_head">
                                    <h4>Chat Management</h4>
                                    <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6>
                                </div>

                                <div class="main_cntnt_dash">
                                    <div class="card_shd cnt_skamper pad-0">
                                        <!--  -->
                                        <div class="cont_shd_frm subscription_plans">
                                            <div class="row">
                                                <div class="col-xl-4 col-lg-5 col-md-5 col-sm-5 col-xs-12 user-list">
                                                    <div class="chating_user bl_grd">
                                                        <!--  -->
                                                        <div class="srch_bar_chat">
                                                        	<div class="pos_rel">
                                                        		<input type="text" name="" class="form-control input_catch" placeholder="Search Keyword . . ." />
                                                        		<i class="fa fa-search"></i>
                                                        	</div>
                                                        </div>
                                                        <ul class="inbox_usrs">
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile2.jpg') }}">
                                                        					<i class="fa fa-circle offline"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Akshay Thakur </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>How are you ?</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li class="active">
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/g2.jpg') }}">
                                                        					<i class="fa fa-circle online"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Laurence </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>attachment</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile2.jpg') }}">
                                                        					<i class="fa fa-circle offline"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Michal Roger </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>Yes Sir, we have.</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile3.jpg') }}">
                                                        					<i class="fa fa-circle online"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Jorden </a>
                                                        						<small class="float-right">11:36AM</small>
                                                        					</h5>
                                                        					<p>How can i submit?.</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile1.jpg') }}">
                                                        					<i class="fa fa-circle offline"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> John Norman </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>You might have heard</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile2.jpg') }}">
                                                        					<i class="fa fa-circle offline"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Michal Roger </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>Yes Sir, we have.</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile3.jpg') }}">
                                                        					<i class="fa fa-circle online"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> Jorden </a>
                                                        						<small class="float-right">11:36AM</small>
                                                        					</h5>
                                                        					<p>How can i submit?.</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        	<li>
                                                        		<div class="messge_list">
                                                        			<div class="noti_mini">
                                                        				<div class="noti_img pos_rel">
                                                        					<img class="img-responsive" src="{{ url( systemImagePath.'/profile1.jpg') }}">
                                                        					<i class="fa fa-circle offline"></i>
                                                        				</div>
                                                        				<div class="noti_desc">
                                                        					<h5><a href="other_user_profile.html"> John Norman </a>
                                                        						<small class="float-right">2:36PM</small>
                                                        					</h5>
                                                        					<p>You might have heard</p>
                                                        				</div>
                                                        			</div>
                                                        		</div>    
                                                        	</li>
                                                        </ul>
                                                        <!--  -->
                                                    </div>
                                                </div>

                                                <div class="col-xl-8 col-lg-7 col-md-7 col-sm-7 col-xs-12 chat-info">
                                                    <div class="side_Users">
                                                        <!--  -->
                                                        <div class="side_wrp_dp">
                                                        	<div class="head_chatr">
                                                        		<img src="{{ url( systemImagePath.'/g2.jpg') }}" class="img-fluid" />
                                                        		<div class="chtr_meta">
                                                        			<h6 class="chtr_nm">Laurence Aniston</h6>
                                                        			<small class="stts_chtr">Active Now</small>
                                                        		</div>
                                                        	</div>
                                                        	<div class="body_chatr">
                                                        		<div class="scrol_cls">
																	<div class="main_msg_single rcvd_msg">
																		<div class="msg_usr_img">
																			<img src="{{ url( systemImagePath.'/g2.jpg') }}" alt="" class="img-responsive img-circle">
																		</div><!--msg_usr_img end-->
																		<div class="msg_dtl text-left">
																			<div class="msg_inr_dtl img_box">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
																			</div><!--msg_inr_dtl end-->
																			<span>Sat, Aug 23, 1:08 PM</span>
																		</div><!--msg_dtl end-->
																	</div><!--main_msg_single end-->	
																	<div class="main_msg_single sent_msg">
																		<div class="msg_dtl text-right">
																			<div class="msg_inr_dtl img_box">
																				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
																			</div><!--msg_inr_dtl end-->
																			<span>Sat, Aug 23, 1:08 PM</span>
																		</div><!--msg_dtl end-->
																	</div><!--main_msg_single end-->
																	<div class="main_msg_single rcvd_msg">
																		<div class="msg_usr_img">
																			<img src="{{ url( systemImagePath.'/g2.jpg') }}" alt="" class="img-responsive img-circle">
																		</div><!--msg_usr_img end-->
																		<div class="msg_dtl text-left">
																			<div class="msg_inr_dtl img_box">
																				<p>Sant in culpa qui officia deserunt mollit anim id est laborum.</p>
																			</div><!--msg_inr_dtl end-->
																			<span>Sat, Aug 23, 1:08 PM</span>
																		</div><!--msg_dtl end-->
																	</div><!--main_msg_single end-->
																	<div class="main_msg_single rcvd_msg">
																		<div class="msg_usr_img">
																			<img src="{{ url( systemImagePath.'/g2.jpg') }}" alt="" class="img-responsive img-circle">
																		</div><!--msg_usr_img end-->
																		<div class="msg_dtl text-left">
																			<div class="msg_inr_dtl img_box">
																				<p>Do you really wanna go to library?</p>
																			</div><!--msg_inr_dtl end-->
																			<span>Sat, Aug 23, 1:08 PM</span>
																		</div><!--msg_dtl end-->
																	</div><!--main_msg_single end-->
																	<div class="main_msg_single sent_msg">
																		<div class="msg_dtl text-right">
																			<div class="msg_inr_dtl img_box">
																				<p>Sure. Why Not?</p>
																			</div><!--msg_inr_dtl end-->
																			<span>Sat, Aug 23, 1:08 PM</span>
																		</div><!--msg_dtl end-->
																	</div><!--main_msg_single end-->
																</div>
                                                        	</div>
                                                        	<div class="footer_chatr">
                                                        		<div class="row">
                                                            		<div class="col-sm-11 col-md-10">
                                                            			<div class="input_writ pos_rel">
                                                            				<input type="text" class="form-control" placeholder="write here . . ." name="">
                                                            				<i class="far fa-paper-plane"></i>
                                                            			</div>
                                                            		</div>
                                                            		<div class="col-sm-1 col-md-2">
                                                            			<div class="clip_atch text-center">
                                                            				<span class="pos_rel">
                                                                				<i class="fa fa-paperclip"></i>
                                                                				<input type="file" name="" class="form-control"></div>
                                                                			</span>
                                                            			</div>
                                                            		</div>
                                                            	</div>
                                                        	</div>
                                                        </div>
                                                        <!--  -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    	@include('frontEnd.common.top_footer')
    </div>
</div>
@endsection