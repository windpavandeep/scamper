@extends('frontEnd.layouts.master')
@section('title','Earnings')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">
            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec earning_mgmt"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.trainer.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>My Earnings</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>

                                    <div class="main_cntnt_dash">
                                        <div class="main_cntnt_dash">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="card_single card_shd text-center">
                                                        <img src="{{ asset(systemImgPath.'/toter.png') }}" class="img-fluid" alt="Info image">
                                                        <h5>{{$total_earnings}}</h5>
                                                        <p>Total Earnings</p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-6">
                                                    <div class="card_single card_shd text-center">
                                                        <img src="{{ asset(systemImgPath.'/toter.png') }}" class="img-fluid" alt="Info image">
                                                        <h5>{{$current_month_earnings}}</h5>
                                                        <p>Pending Earnings</p>
                                                        <!-- <p class="th_mon"><small>This Month</small></p> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card_shd cnt_skamper pad-20">
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="wrap_cms_table">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered">
                                                                    <thead class="thead-dark">
                                                                        <tr>
                                                                            <th>Transaction ID</th>
                                                                            <th>Student Name</th>
                                                                            <th>Course Name</th>
                                                                            <th>Type</th>
                                                                            <th>Amount</th>
                                                                            <!-- <th>Action</th> -->
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @if(!empty($earnings))
                                                                            @foreach($earnings as $key=>$value)
                                                                                <tr>
                                                                                    <td>{{ $value['razor_pay_id'] }}</td>
                                                                                    <td>
                                                                                        {{ ucfirst($value['first_name']) }} 
                                                                                        {{ ucfirst($value['last_name']) }}
                                                                                    </td>
                                                                                    <td>
                                                                                        {{ ucfirst($value['title']) }}
                                                                                    </td>
                                                                                    <td>{{$value['upload_type']}}</td>
                                                                                    <td>{{$value['trainer_price']}}</td>
                                                                                    <?php
                                                                                        $course_id = base64_encode($value['course_id'].'-'.$value['trainer_id']);
                                                                                    
                                                                                    ?>
                                                                                   <!--  <td class="icoss">
                                                                                        <a href="{{url('/course/detail/'.base64_encode($value['course_id'].'-').base64_encode($value['trainer_id'].'-').base64_encode('trainer_content') )}}"  target="_blank">
                                                                                           
                                                                                            <i data-toggle="tooltip" title="View Content" class="fas fa-eye cp rmv_tr" data-original-title="View Content"></i>
                                                                                        </a>
                                                                                    </td> -->
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr>
                                                                                <td colspan="6" style="text-align: center;">No Data Found</td>
                                                                            </tr>
                                                                        @endif
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection