@extends('frontEnd.layouts.master')
@section('title','Register As A Teacher')
@section('content')
 
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="why_sec bene_sec">
                <div class="container">
                    <div class="row rowpp">
                        <div class="col-sm-12 col-md-8 offset-md-2">
                            <div class="sec_heading text-center">
                                <h2>Register As A Teacher</h2>
                                <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                            </div>
                            <div class="side_feat text-center">

                                <p class="home-section-txt">{!!@$section1_content->description !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @if(!empty($section2_content))
                @foreach($section2_content as $key=>$value)
                    <?php
                        $class="";
                        if( $key%2 == 0 ){
                            $class="sec_gray";
                        }  
                    ?>
                    <section class="why_sec bene_sec {{$class}}">
                        <div class="container">
                            <div class="row rowpp">
                                    <?php
                                        if(!empty($value['image'])){
                                            if(file_exists(BecomeTrainerBasePath.'/'.$value['image'])){
                                                $image = BecomeTrainerImgPath.'/'.$value['image'];
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    ?>
                                    @if( $key%2 == 0 )

                                        <div class="col-md-6 col-xs-12">
                                            <div class="img_app text-center">
                                                <img src="{{$image}}" class="img-fluid">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="sec_heading text-left">
                                                <h2>{{ucfirst($value['title'])}}</h2>
                                                <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                            </div>
                                            <div class="side_feat">
                                                <p class="app-header">Download the ScamperSkills app now!</p>
                                                <p class="home-section-txt">{!! ucfirst($value['description']) !!}</p>
                                                <div class="text-left">
                                                    @if(!Auth::check())
                                                        <a href="{{url('/register/trainer')}}" class="btn btn_gradient btn_active" data-animation="fadeIn" data-delay="1110ms"></i>Apply as a Teacher</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                    <div class="col-md-6 col-xs-12">
                                        <div class="sec_heading text-left">
                                            <h2>{{ucfirst($value['title'])}}</h2>
                                            <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                        </div>
                                        <div class="side_feat">
                                            <p class="app-header">Download the ScamperSkills app now!</p>
                                            <p class="home-section-txt">{!! ucfirst($value['description']) !!}</p>
                                            <div class="text-left">
                                                @if(!Auth::check())
                                                    <a href="{{url('/register/trainer')}}" class="btn btn_gradient btn_active" data-animation="fadeIn" data-delay="1110ms">&nbsp;&nbsp;</i>Apply as a Teacher</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="img_app text-center">
                                                <img src="{{ $image }}" class="img-fluid">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </section>
                @endforeach
            @endif
           
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
