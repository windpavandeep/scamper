<div class="pic_top pos_rel text-center">
    <div class="pos_rel pic_pos">
        <?php
            $image = DefaultUserPath;
            $usr_img = Auth::User()->image;
            if(!empty($usr_img)){
                $image = TrainerProfileImgPath.'/'.$usr_img;
            }

            $name = '';
            if(Auth::check()){
                $name = ucfirst(Auth::User()->first_name).' '.Auth::User()->last_name;
            }
            // echo '1'; die;
        ?>

        <img src="{{ $image }}" class="img-fluid" alt="">
        <!-- <i data-toggle="tooltip" title="15 days remaining" class="fa fa-bolt"></i> -->
    </div>
    <h4 class="name_db">{{ $name }}</h4>
   <!--  <span class="pln_typ">$55 /-year</span>
    <button href="javascript:;" class=" btn upg_pl">$16<sub>/mon</sub> Upgrade Plan</button> -->
</div>
<div class="side_menu">
    <ul type="none" class="ul_side">
        <li class="<?php if($page == 'dashboard'){ echo 'active'; } ?>"><a href="{{ url('/trainer/dashboard') }}"><i class="far fa-chart-bar"></i> Dashboard</a></li>
        <li class="<?php if( ($page == 'profile')||($page == 'edit_profile')||($page == 'change_password') ) { echo 'active'; } ?>"><a href="{{ url('/trainer/profile') }}"><i class="fas fa-user-tie"></i> Profile Management</a></li>
        <li class="<?php if($page == 'contents'){ echo 'active'; } ?>"><a href="{{ url('/trainer/contents') }}"><i class="fas fa-tasks"></i> Content Management</a></li>
       <!--  <li class="<?php if($page == 'chats'){ echo 'active'; } ?>"><a href="{{ url('/trainer/chats') }}"><i class="far fa-comment-dots"></i> Chat Management</a></li> -->
        <li class="<?php if($page == 'subscriptions'){ echo 'active'; } ?>"><a href="{{ url('/trainer/subscriptions') }}"><i class="far fa-clipboard"></i> Subscriptions</a></li>
        <li class="<?php if($page == 'earnings'){ echo 'active'; } ?>"><a href="{{ url('/trainer/earnings') }}"><i class="fa fa-hand-holding-usd"></i> Total Earnings</a></li>
        <li><a href="{{url('/logout')}}"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
    </ul>
</div>