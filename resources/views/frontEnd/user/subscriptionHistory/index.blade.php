@extends('frontEnd.layouts.master')
@section('title','Subscription History')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div user_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec usr_subs">
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.user.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>My Subscription History</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                    
                                        <div class="card_shd cnt_skamper pad-20">
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="wrap_cms_table">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered">
                                                                    <thead class="thead-dark">
                                                                        <tr>
                                                                            <th>Subscription Type</th>
                                                                            <th>Amount</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>One Month Subscription</td>
                                                                            <td>INR 3700</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Two Month Subscription</td>
                                                                            <td>INR 2100</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Three Month Subscription</td>
                                                                            <td>INR 1900</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        @include('frontEnd.common.top_footer')
    </div>
@endsection