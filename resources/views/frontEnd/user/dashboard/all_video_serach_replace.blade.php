  <div class="selc_fltr video_serach_replace">
    <form class="form-inline justify-content-between">
        <div class="wrap_selc">
            <select name="subject_id" class="custom-select subject-change-video" id="subject_id">
                <option selected  value="" id="subject_option_id">Select Subject</option>
                    <?php 
                        if(!empty($subject)){
                            foreach($subject as $subjectval){ ?>
                                <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                       <?php }
                        }
                    ?>
            </select>
            <select name="unit_id" class="custom-select unit-change-video" id="unit_id">
                <option value="">Select Units</option>
            </select>

            <input class="form-control" type="text" name="title" value="" placeholder="Serach By Name" autocomplete="off" id="title_id">
        </div>
    </form>
</div>

<script>
 $('.subject-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-video').empty();
                    $('.unit-change-video').html(resp.unit_options);
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


    $('.unit-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_subject_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


    $('#title_id').keyup(function() {
        var title = this.value;
        if(title){
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_video_by_title') }}"+"/"+title,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });

</script>
