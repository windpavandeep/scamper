 <div class="selc_fltr pdf_search_replace">
    <form class="form-inline justify-content-between">
        <div class="wrap_selc">
            <select name="subject_id" class="custom-select subject-change-notes" id="subject_id">
                <option selected  value="" id="subject_option_id">Select Subject</option>
                    <?php 
                        if(!empty($subject)){
                            foreach($subject as $subjectval){ ?>
                                <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                       <?php }
                        }
                    ?>
            </select>
            <select name="unit_id" class="custom-select unit-change-notes" id="unit_id">
                <option value="">Select Units</option>
            </select>

            <input class="form-control" type="text" name="title" value="" placeholder="Serach By Name" autocomplete="off" id="title_id">
        </div>
       
    </form>
</div>

<script type="text/javascript">
    var active_id   = "<?php echo $active_id; ?>";
     $('.subject-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-notes').empty();
                    $('.unit-change-notes').html(resp.unit_options);
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });


     $('.unit-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_subject_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });

    $('#title_id').keyup(function() {
        var title = this.value;
        if(title){
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_notes_by_title') }}"+"/"+title+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });
</script>