<div class="selc_fltr  pdf_serach_replace">
   <form class="form-inline justify-content-between">
        <div class="wrap_selc">
            <select name="subject_notes" class="custom-select subject-change-notes">
                <option selected id="subject_option_id-notes">Select Subject</option>
                    <?php 
                    if(!empty($subject)){
                        foreach($subject as $subjectval){ ?>
                             <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                    <?php } } ?>   
            </select>
            <select name="unit_notes" class="custom-select unit-change-notes" id="unit-id-notes">
                <option selected>Select Units</option>
            </select>
        </div>
        <div class="text-right">
            <a href="{{url('/student/related-notes')}}/{{$activeMainCourse['id']}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
        </div>
    </form>
</div>



<script>
    $('.subject-change').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/unit') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change').html(resp);
                    //$("#unit_id").selectpicker("refresh");
                }
            })
        }
    });

    $('.subject-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-notes').empty();
                    $('.unit-change-notes').html(resp.unit_options);
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });


     $('.unit-change-pdf').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/subject_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });

</script>