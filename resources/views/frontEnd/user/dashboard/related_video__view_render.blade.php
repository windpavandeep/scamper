<?php use App\CommonHelper;?>
<?php echo CommonHelper::videoSection($activecoursevideo);?>
<script>
$('.video-show-poup').on('click',function(){
        $('.loader_ques').show();
        var video  = $(this).attr('rel');
        var trainer_content_file_id = $(this).attr('rel1');
        var site_link  = "<?php echo SITELINK; ?>";
        var url  = "<?php echo TrainerContentBasePath; ?>";
        var vidoelink  = site_link+url+'/'+video;
        var video = $('#vidBox video');
        var host = window.location.host;
        var proto = window.location.protocol;
        var ajax_url = proto+"//"+host+"/student/";
        $.ajax({
            type:"get",
            url: ajax_url+'trainer_content_views'+"/"+trainer_content_file_id,
            success:function(resp){
                $('source', video).attr('src', vidoelink);
                $("#vidBox video")[0].load();
                $("#vidBox video")[0].play();
                $('.loader_ques').hide();
                $('#modal_vid').show();
                $('#total_view'+trainer_content_file_id).replaceWith('<i class="fa fa-eye" aria-hidden="true"></i> : '+resp.total_view+'k');
            }
        })
});

$('.close').click(function(){
    $('video').trigger('pause');
    $(this).closest('#modal_vid').hide();

});
</script>