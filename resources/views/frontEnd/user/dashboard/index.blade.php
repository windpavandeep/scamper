
@extends('frontEnd.layouts.student_master')
@section('title','Dashboard')
@section('content')
<style>  
@media print  
{  
iframe {display:none;}  
}  
</style> 
<?php use App\CommonHelper;?>
 <div class="page-wrapper">
    <div class="app_dash_wraper">

        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_home_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                            @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">
                                <!--  -->
                                @include('frontEnd.common.student_dashboard_sub_header')
                                <div class="card_shd">
                                    <ul class="nav nav-tabs pos_rel course_replace" id="myTab" role="tablist">
                                        <?php 

                                            $maincoursecount  =  1;
                                            if(!empty($maincourse) && count($maincourse)>0) {

                                                foreach($maincourse as $maincoursevalue) {
                                                    $topclass  =  '';
                                                    if($maincoursecount == 1){
                                                        $topclass = "active"; 
                                                    }
                                                    if($maincoursecount <= 1){
                                                  ?>
                                                    <li class="nav-item wid_cuss">
                                                        <a class="main-course-click nav-link <?php echo $topclass;?>" rel="<?php echo  $maincoursevalue['id']; ?>" id="home-tab" data-toggle="tab" href="#home<?php echo $maincoursevalue['id']; ?>" role="tab" aria-controls="home" aria-selected="true"><?php echo $maincoursevalue['title']; ?></a>
                                                    </li>
                                                    <input type="hidden" name="active_main_course_id" value="<?php echo  $maincoursevalue['id']; ?>" id="active_main_course_id">
                                               <?php } $maincoursecount++; }  ?>

                                               <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">More</a>
                                                        <div class="dropdown-menu">
                                                            <?php $maincoursecountsecond =  1; 
                                                            foreach($maincourse as $maincoursevalue) { 
                                                                if($maincoursecountsecond > 1){ 
                                                                ?>
                                                            <a class="dropdown-item main-course-click" id="subj<?php echo $maincoursevalue['id']; ?>" rel="<?php echo  $maincoursevalue['id']; ?>">
                                                                <?php echo $maincoursevalue['title']; ?></a>
                                                            <?php } $maincoursecountsecond++; } ?>
                                                        </div>
                                                   </li>

                                        <?php  }
                                        ?>
                                        <div class="wrpa_hedr_cont">
                                            <ul class="d-flex align-items-center" type="none">
                                                <!-- <li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li> -->
                                                <li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
                                                <li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
                                            </ul>
                                        </div>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <section class="main_cntnt_dash page_div">
                                                <div class="live_div_sec padtb40">
                                                    <div class="sec_heading text-center">
                                                        <h2>Upcoming Live Classes</h2>
                                                        <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                                    </div>
                                                    <!--  -->
                                                    <div class="wrap_vids">
                                                        <div class="selc_fltr">
                                                            <form class="form-inline justify-content-between">
                                                                <div class="wrap_selc">
                                                                    <select name="subject_id_upcomming" class="custom-select subject-change-upcoming" id="subject_id">
                                                                        <option  value="" id="subject_option_id">Select Subject</option>
                                                                            <?php 
                                                                                if(!empty($subject)){
                                                                                    foreach($subject as $subjectval){ ?>
                                                                                        <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                               <?php }
                                                                                }
                                                                            ?>
                                                                    </select>
                                                                    <select name="unit_id" class="custom-select unit-change-upcomming" id="unit_id-upcomming">
                                                                        <option value="">Select Units</option>
                                                                    </select>
                                                                </div>
                                                                <div class="text-right"> 
                                                                    <a href="{{url('student/live-lectures')}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All</a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div id="live_classes" class="owl-carousel custm_slider">
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/ssc.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">Preparation of JEE/NEET AIEEE</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/deepshika.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">New syllabus of Civil Services</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/ssc.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">Preparation of JEE/NEET AIEEE</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/deepshika.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">New syllabus of Civil Services</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/ssc.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">Preparation of JEE/NEET AIEEE</p>
                                                                </div>
                                                            </div>
                                                            <div class="item">
                                                                <div class="vid_sing">
                                                                    <div class="img_app text-center">
                                                                        <img src="{{ asset('public/img/deepshika.jpg') }}" class="img-fluid">
                                                                    </div>
                                                                    <p class="vid_head">New syllabus of Civil Services</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </section>
                                            <section class="main_cntnt_dash page_div">
                                                <div class="vid_div_sec padtb40">
                                                    <div class="sec_heading text-center">
                                                        <h2>Related Videos</h2>
                                                        <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                                    </div>
                                                    <!--  -->
                                                    <div class="wrap_vids">
                                                        <div class="selc_fltr  video_serach_replace">
                                                            <form class="form-inline justify-content-between">
                                                                <div class="wrap_selc">
                                                                    <select name="subject_video" class="custom-select subject-change-video">
                                                                        <option selected id="subject_option_id-video">Select Subject</option>
                                                                            <?php 
                                                                            if(!empty($subject)){
                                                                                foreach($subject as $subjectval){ ?>
                                                                                     <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                            <?php } } ?>   
                                                                    </select>
                                                                    <select name="unit_video" class="custom-select unit-change-video" id="unit-id-video">
                                                                        <option selected>Select Units</option>
                                                                    </select>
                                                                </div>
                                                                <div class="text-right">
                                                                    <a href="{{url('/student/all-videos')}}/{{$activeMainCourse['id']}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All</a>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <?php echo CommonHelper::dashboardVideoSection($activecoursevideo);?>
                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </section>                                        
                                            <section class="main_cntnt_dash page_div">
                                                <div class="notes_div_sec padtb40">
                                                    <div class="sec_heading text-center">
                                                        <h2>Related Notes</h2>
                                                        <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                                    </div>
                                                    <!--  -->
                                                  <!--   <div class="selc_fltr mb-1">
                                                        <form class="form-inline justify-content-end">
                                                            <div class="text-right col-12">
                                                                <a href="{{url('/student/related-notes')}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
                                                            </div>
                                                        </form>
                                                    </div> -->
                                                    <div class="selc_fltr  pdf_serach_replace">
                                                        <form class="form-inline justify-content-between">
                                                            <div class="wrap_selc">
                                                                <select name="subject_notes" class="custom-select subject-change-notes">
                                                                    <option selected id="subject_option_id-notes">Select Subject</option>
                                                                        <?php 
                                                                        if(!empty($subject)){
                                                                            foreach($subject as $subjectval){ ?>
                                                                                 <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                        <?php } } ?>   
                                                                </select>
                                                                <select name="unit_notes" class="custom-select unit-change-notes" id="unit-id-notes">
                                                                    <option selected>Select Units</option>
                                                                </select>
                                                            </div>
                                                            <div class="text-right">
                                                                <a href="{{url('/student/related-notes')}}/{{$activeMainCourse['id']}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <?php echo CommonHelper::dashboardNotesSection($activecoursepdf);?>
                                                </div>
                                            </section>
                                            <section class="main_cntnt_dash page_div">
                                                <div class="teach_talk padtb40">
                                                    <div class="sec_heading text-center">
                                                        <h2>Chat with Teachers</h2>
                                                        <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                                    </div>
                                                    <!--  -->
                                                    <div id="slider_teach" class="owl-carousel custm_slider">
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t1.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Aniruddha Roy</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t2.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Anjan Roy</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t3.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Anuradha Bhattacharhjee</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t4.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Arijit Kapoor</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t5.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Deepshikha Arora</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t6.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Monosasish Das</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t7.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Sharaleepa Majumder Deb</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="teach_sing">
                                                                <span><img src="{{url('public/img/t8.jpg')}}" class="img-fluid"></span>
                                                                <div class="meta_teacher">
                                                                    <h3><a>Shidhartha Mukharjee</a></h3>
                                                                    <p><i>Entrance Exam</i></p>
                                                                    <p class="ratein"><i class="fa fa-star"></i> 4.5/5</p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </section>
                                            <section class="main_cntnt_dash page_div">
                                                <div class="solved_div_sec padtb40">
                                                    <div class="sec_heading text-center">
                                                        <h2>Previous Solved Exams</h2>
                                                        <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                                    </div>
                                                    <div class="selc_fltr mb-1">
                                                        <form class="form-inline justify-content-end">
                                                            <div class="text-right col-12">
                                                                <a href="{{url('/student/solved-exams')}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All Notes</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--  -->
                                                    <div id="slider_solved" class="owl-carousel custm_slider">
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Math - knowing Our Numbers</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Mathematics-Whole</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Math-Playing with Numbers</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Mathematics-Understandings</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Math-Basic Geomatrical</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Mathematics-Integers</a>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div class="notes_sing">
                                                                <span><img src="https://icons.iconarchive.com/icons/papirus-team/papirus-mimetypes/512/app-pdf-icon.png" class="img-fluid" ></span>
                                                                <a>Mathematics-Fraction Data</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--  -->
                                                </div>
                                            </section>
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <!-- content goes here -->
                                            <div class="no_cnt text-center">
                                                <h2>Science</h2>
                                                <p class="">No data Yet</p>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <!-- content goes here -->
                                            <div class="no_cnt text-center">
                                                <h2>Physics</h2>
                                                <p class="">No data Yet</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>
<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>
<!-- modal video -->
<div class="modal video-right-click-disabled" id="modal_vid">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="vidBox">
            <video id="demo" loop="" controls="" width="100%" height="100%" controlsList="nodownload">
              <source src="" type="video/mp4">
            </video>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- modal video -->


<!-- modal pdf -->
<div class="modal notes-right-click-disabled " id="modal_pid">
  <div class="modal-dialog modal-lg">
    <div class="modal-content disableEvent">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="pidBox">
            <iframe src="#" height="100%" width="100%" id="myiframe"></iframe>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- modal pdf -->
@endsection

@section('scripts')
<script type="text/javascript">
    document.onmousedown=disableclick;
status="Right Click Disabled";
Function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;
   }
}
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.loader_ques').hide();

//         $(document).on({
//     "contextmenu": function(e) {
//         console.log("ctx menu button:", e.which); 

//         // Stop the context menu
//         e.preventDefault();
//     },
//     "mousedown": function(e) { 
//         console.log("normal mouse down:", e.which); 
//     },
//     "mouseup": function(e) { 
//         console.log("normal mouse up:", e.which); 
//     }
// });

        // document.oncontextmenu = function() {  
        //     return false;  
        // };  



        //window.frames["your_iframe_id"].document.oncontextmenu = function(){ return false; };

        // $('.video-right-click-disabled').bind('contextmenu', function(e) {
        //     return false;
        // });

        $('.pdf-right-click-disabled').bind('contextmenu', function(e) {
            return false;
        });  
          
    });
</script>
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script>
$('.video-show-poup').on('click',function(){
        $('.loader_ques').show();
        var video  = $(this).attr('rel');
        var trainer_content_file_id = $(this).attr('rel1');
        var site_link  = "<?php echo SITELINK; ?>";
        var url  = "<?php echo TrainerContentBasePath; ?>";
        var vidoelink  = site_link+url+'/'+video;
        var video = $('#vidBox video');
        var host = window.location.host;
        var proto = window.location.protocol;
        var ajax_url = proto+"//"+host+"/student/";
        $.ajax({
            type:"get",
            url: ajax_url+'trainer_content_views'+"/"+trainer_content_file_id,
            success:function(resp){
                $('source', video).attr('src', vidoelink);
                $("#vidBox video")[0].load();
                $("#vidBox video")[0].play();
                $('.loader_ques').hide();
                $('#modal_vid').show();
                $('#total_view'+trainer_content_file_id).replaceWith('<i class="fa fa-eye" aria-hidden="true"></i> : '+resp.total_view+'k');
            }
        })
});

$('.close').click(function(){
    $('video').trigger('pause');
    $(this).closest('#modal_vid').hide();

});
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $("#slider_catg").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
        });
        $("#slider_teach").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
        });
        $("#slider_notes").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
        $("#slider_rel_vids").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3 ,
            slidesToScroll: 2,
        });
        $("#live_classes").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3 ,
            slidesToScroll: 2,
        });
        $("#slider_solved").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
    });
</script>

<script type="text/javascript">
    $('.main-course-click').on('click', function () { 
        $('.loader_ques').show();
        var main_course_id  =   $(this).attr('rel');
        var main_course  = <?php echo json_encode($maincourse);?>;
        $.ajax({
            type:"post",
            url: "{{ url('student/get_subject_by_course') }}",
            data:{'main_course':main_course,'course_id':main_course_id,'_token': "{{ csrf_token() }}"},
            dataType:'json', 
            success:function(resp){
                $('.loader_ques').hide();
                $('.course_replace').replaceWith(resp.render_view);
                $('.subject-change').empty();
                $('.unit-change').empty();
                $('.subject-change-video').empty();
                $('.unit-change-video').empty();
                $('.video_replace_slider').empty();
                $('.video_serach_replace').empty();
                $('.subject-change-notes').empty();
                $('.unit-change-notes').empty();
                $('.pdf_replace_slider').empty();
                $('.pdf_serach_replace').empty();
                $('.subject-change').replaceWith(resp.subject_options);
                $('.subject-change-video').replaceWith(resp.subject_options);
                $('#sub_subcategory_id').val(resp.sub_subcategory_id);
                $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                $('.video_serach_replace').replaceWith(resp.video_serach_replace);
                $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                $('.pdf_serach_replace').replaceWith(resp.pdf_serach_replace);
            }
        })
    });
</script>
<script>
    $('.subject-change').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/unit') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change').html(resp);
                    //$("#unit_id").selectpicker("refresh");
                }
            })
        }
    });

    $('.subject-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-video').empty();
                    $('.unit-change-video').html(resp.unit_options);
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


     $('.unit-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/subject_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });



    $('.subject-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-notes').empty();
                    $('.unit-change-notes').html(resp.unit_options);
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });


     $('.unit-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/subject_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });

</script>

<script type="text/javascript">
    $('.notes-show-poup').on('click',function(){
        //e.preventDefault();
        //e.stopPropagation();
        var pdf  = $(this).attr('rel');
        var finalpdf = pdf+"#toolbar=0";
    
        var pid = $('#pidBox');
        $('iframe', pid).attr('src', finalpdf);
        $('#modal_pid').show();
    });

    $('.close').click(function(){
        $(this).closest('#modal_pid').hide()
    });
</script>

@endsection