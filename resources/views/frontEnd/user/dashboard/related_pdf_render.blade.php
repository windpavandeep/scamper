<?php use App\CommonHelper;?>
<?php echo CommonHelper::dashboardNotesSection($activecoursepdf);?>
<script type="text/javascript">
     
     $(".init-slider_rel_nids").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
</script>

<script type="text/javascript">
    $('.notes-show-poup').on('click',function(){
        var pdf  = $(this).attr('rel');
        var finalpdf = pdf+"#toolbar=0";
    
        var pid = $('#pidBox');
        $('iframe', pid).attr('src', finalpdf);
        $('#modal_pid').show();
    });

    $('.close').click(function(){
        $(this).closest('#modal_pid').hide()
    });
</script>
