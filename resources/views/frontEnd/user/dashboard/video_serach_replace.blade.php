 <div class="selc_fltr  video_serach_replace">
    <form class="form-inline justify-content-between">
        <div class="wrap_selc">
        <select name="subject_video" class="custom-select subject-change-video">
        <option selected id="subject_option_id-video">Select Subject</option>
            <?php 
            if(!empty($subject)){
                foreach($subject as $subjectval){ ?>
                     <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
            <?php } } ?>   
        </select>
        <select name="unit_video" class="custom-select unit-change-video" id="unit-id-video">
        <option selected>Select Units</option>
        </select>
        </div>
        <div class="text-right">
        <a href="{{url('/student/all-videos')}}/{{$activeMainCourse['id']}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> View All</a>
        </div>
    </form>
</div>


<script>
    $('.subject-change').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/unit') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change').html(resp);
                    //$("#unit_id").selectpicker("refresh");
                }
            })
        }
    });

    $('.subject-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-video').empty();
                    $('.unit-change-video').html(resp.unit_options);
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


     $('.unit-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        var active_id  = $('#active_main_course_id').val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/subject_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });

</script>