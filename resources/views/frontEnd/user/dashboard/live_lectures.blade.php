@extends('frontEnd.layouts.student_master')
@section('title','Dashboard')
@section('content')
<div class="page-wrapper">
    <div class="app_dash_wraper">

        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_vid_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                            @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">
                                <!--  -->
                                <section class="main_cntnt_dash page_div ">
                                    <div class="catg_div_sec padtb40">
                                        <!--  -->
                                        <div id="slider_catg" class="owl-carousel custm_slider">
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fa fa-book"></i></span>
                                                    <p>Prep Test</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fas fa-file-alt"></i></span>
                                                    <p>Exams</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fas fa-cart-plus"></i></span>
                                                    <p>Buy Courses</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fas fa-comment-dots"></i></span>
                                                    <p>Chat with Teacher</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fa fa-file-signature"></i></span>
                                                    <p>Notes</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fa fa-download"></i></span>
                                                    <p>Downloads</p>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="catg_wrap">
                                                    <span><i class="fa fa-globe"></i></span>
                                                    <p>Free Counselling</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                </section>
                                <section class="main_cntnt_dash page_div card_shd">
                                    <div class="vid_div_sec padtb40">
                                        <div class="sec_heading text-center">
                                            <h2>Live Lectures</h2>
                                            <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                        </div>
                                        <div class="selc_fltr">
                                            <form class="form-inline justify-content-end">
                                                <div class="wrap_selc">
                                                    <select name="as" class="custom-select">
                                                        <option selected>Select Subject</option>
                                                        <option value="Maths">Maths</option>
                                                        <option value="English">English</option>
                                                        <option value="Chemistry">Chemistry</option>
                                                        <option value="Physics">Physics</option>
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                        <!--  -->
                                        <div class="wrap_vids">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="vid_sing liv_sing">
                                                        <div class="img_app text-center pos_rel">
                                                            <img src="https://images.pexels.com/photos/159844/cellular-education-classroom-159844.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" class="img-fluid">
                                                            <span class="text-success liv_spn"><i class="fa fa-circle"></i> Live Now</span>
                                                        </div>
                                                        <div class="meta_teach">
                                                            <div class="d-flex">
                                                                <span class="tech_img">
                                                                    <img src="{{url('public/img/profile2.jpg')}}" class="img-fluid">
                                                                </span>
                                                                <span class="nam_lang">
                                                                    <a>Anuj gupta</a>
                                                                    <p>English</p>
                                                                </span>
                                                            </div>
                                                            <a class="lect_ttl" href="liveVideoLecture.php">Time Distance And Speed Useful Tricks For SBI PO/IBPS PO 2020</a>
                                                            <p class="topc_tpe">#Current Affairs</p>
                                                            <p class="tme_seson">Lesson 2 ~ 9:00 PM</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  -->
                                    </div>
                                </section>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>    
    </div>
</div>

<!-- modal video -->
<div class="modal" id="modal_vid">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="vidBox">
            <video id="demo" loop="" controls="" width="100%" height="100%">
              <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
            </video>
        </div>
      </div>

    </div>
  </div>
</div>

@endsection