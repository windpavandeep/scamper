<style type="text/css">
    /*.modal_pid_class .modal-dialog.modal-lg {
    margin-top: 173px;   
    margin-left: 271px;
    width: 1059px;
    max-width: 1500px; 

    
}
.modal_pid_class .modal-body #pidBox{
    width: 1059px;
    max-width: 3000px;
    height: 350px;
    max-height: 410px;
    overflow: hidden;
}*/

</style>
@extends('frontEnd.layouts.student_master')
@section('title','Notes')
@section('content')
<?php use App\CommonHelper;?>
<div class="page-wrapper">
    <div class="app_dash_wraper">

        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_note_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                            @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">
                                <!--  -->
                                <section class="main_cntnt_dash page_div card_shd">
                                    <div class="notesg_div_sec padtb40">
                                        <div class="sec_heading text-center">
                                            <h2>Related Notes</h2>
                                            <p class="divider"><img src="{{ asset(systemImgPath.'/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                        </div>
                                        <!--  -->
                                        <div class="notes_wrap_all">
                                            <div class="selc_fltr">
                                                <form class="form-inline justify-content-between">
                                                    <div class="wrap_selc">
                                                        <select name="subject_id" class="custom-select subject-change-notes" id="subject_id">
                                                            <option selected  value="" id="subject_option_id">Select Subject</option>
                                                                <?php 
                                                                    if(!empty($subject)){
                                                                        foreach($subject as $subjectval){ ?>
                                                                            <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                   <?php }
                                                                    }
                                                                ?>
                                                        </select>
                                                        <select name="unit_id" class="custom-select unit-change-notes" id="unit_id">
                                                            <option value="">Select Units</option>
                                                        </select>

                                                        <input class="form-control" type="text" name="title" value="" placeholder="Serach By Name" autocomplete="off" id="title_id">
                                                    </div>
                                                   
                                                </form>
                                            </div>
                                            <?php echo CommonHelper::notesSection($activecoursepdf);?>
                                        </div>
                                        <!--  -->
                                    </div>
                                </section>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>

<!-- modal pdf -->
<div class="modal notes-right-click-disabled modal_pid_class disableEvent" id="modal_pid">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="pidBox">
            <iframe src="#" height="100%" width="100%" style="pointer-events:none;"></iframe>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- modal pdf -->


@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $('.loader_ques').hide();

        $(".disableEvent").on("contextmenu",function(){
            // ('right click disabled');
           return false;
        }); 
    });
</script>

<script type="text/javascript">
    $('.notes-show-poup').on('click',function(){
        var pdf  = $(this).attr('rel');
        var finalpdf = pdf+"#toolbar=0";
    
        var pid = $('#pidBox');
        $('iframe', pid).attr('src', finalpdf);
        $('#modal_pid').show();
    });

    $('.close').click(function(){
        $(this).closest('#modal_pid').hide()
    });
</script>

<script type="text/javascript">
    var active_id   = "<?php echo $active_id; ?>";
     $('.subject-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-notes').empty();
                    $('.unit-change-notes').html(resp.unit_options);
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });


     $('.unit-change-notes').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_subject_unit_notes') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });

    $('#title_id').keyup(function() {
        var title = this.value;
        if(title){
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_notes_by_title') }}"+"/"+title+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_notes_active_course') }}"+"/"+active_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.pdf_replace_slider').empty();
                    $('.pdf_replace_slider').replaceWith(resp.related_pdf_render_view);
                }
            })
        }
    });
</script>

@endsection