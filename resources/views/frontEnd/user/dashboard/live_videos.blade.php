@extends('frontEnd.layouts.student_master')
@section('title','Dashboard')
@section('content')
<?php use App\CommonHelper;?>
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_vid_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                            @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">

                                <!--  -->
                                <section class="main_cntnt_dash page_div card_shd">
                                    <div class="vid_div_sec padtb40">
                                        <div class="sec_heading text-center">
                                            <h2>Related Videos</h2>
                                            <p class="divider"><img src="http://www.scamperskills.com/public/images/system/secdivider.png" class="img-fluid" alt="divider"></p>
                                        </div>
                                       <div class="wrap_vids">
                                            <div class="selc_fltr video_serach_replace">
                                                <form class="form-inline justify-content-between">
                                                    <div class="wrap_selc">
                                                        <select name="subject_id" class="custom-select subject-change-video" id="subject_id">
                                                            <option selected  value="" id="subject_option_id">Select Subject</option>
                                                                <?php 
                                                                    if(!empty($subject)){
                                                                        foreach($subject as $subjectval){ ?>
                                                                            <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                   <?php }
                                                                    }
                                                                ?>
                                                        </select>
                                                        <select name="unit_id" class="custom-select unit-change-video" id="unit_id">
                                                            <option value="">Select Units</option>
                                                        </select>

                                                        <input class="form-control" type="text" name="title" value="" placeholder="Serach By Name" autocomplete="off" id="title_id">
                                                    </div>
                                                   
                                                </form>
                                            </div>
                                            <?php echo CommonHelper::videoSection($activecoursevideo);?>
                                        </div>                                     
                                        <!--  -->
                                    </div>
                                </section>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>

<!-- modal video -->
<div class="modal" id="modal_vid">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div id="vidBox">
            <video id="demo" loop="" controls="" width="100%" height="100%" controlsList="nodownload">
              <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
            </video>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- modal video -->

@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        //  document.oncontextmenu = function() {  
        //     return false;  
        // };  

        $('.loader_ques').hide();
    });
</script>
<script>
$('.video-show-poup').on('click',function(){
        $('.loader_ques').show();
        var video  = $(this).attr('rel');
        var trainer_content_file_id = $(this).attr('rel1');
        var site_link  = "<?php echo SITELINK; ?>";
        var url  = "<?php echo TrainerContentBasePath; ?>";
        var vidoelink  = site_link+url+'/'+video;
        var video = $('#vidBox video');
        var host = window.location.host;
        var proto = window.location.protocol;
        var ajax_url = proto+"//"+host+"/student/";
        $.ajax({
            type:"get",
            url: ajax_url+'trainer_content_views'+"/"+trainer_content_file_id,
            success:function(resp){
                $('source', video).attr('src', vidoelink);
                $("#vidBox video")[0].load();
                $("#vidBox video")[0].play();
                $('.loader_ques').hide();
                $('#modal_vid').show();
                $('#total_view'+trainer_content_file_id).replaceWith('<i class="fa fa-eye" aria-hidden="true"></i> : '+resp.total_view+'k');
            }
        })
});

$('.close').click(function(){
    $('video').trigger('pause');
    $(this).closest('#modal_vid').hide();

});
</script>
<script type="text/javascript">
    $('.subject-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Subject"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.unit-change-video').empty();
                    $('.unit-change-video').html(resp.unit_options);
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


    $('.unit-change-video').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        if(clickedIndex && clickedIndex != "Select Units"){
            $('.loader_ques').show();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_subject_unit_video') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });


    $('#title_id').keyup(function() {
        var title = this.value;
        if(title){
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_video_by_title') }}"+"/"+title,
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        } else{
            $.ajax({
                type:"get",
                url: "{{ url('student/get/view_all_video') }}",
                success:function(resp){
                    $('.loader_ques').hide();
                    $('.video_replace_slider').empty();
                    $('.video_replace_slider').replaceWith(resp.related_video_render_view);
                }
            })
        }
    });

</script>

@endsection