<div class="sngl_qust add_next_question">
   <?php if(!empty($nextquestion)) {
    $optioncount = 1;
    $user_exam_data = DB::table('user_exam_questions')->where(['user_exam_id'=>$user_exam_id,'question_id'=>$nextquestion['id']])->first();
    ?>
    <input type="hidden" name="question" value="<?php echo $nextquestion['id']; ?>" id="get-question_id">
    <div class="quest_top">
        <h5>Q<?php echo $index+1; ?>.<?php echo $nextquestion['question']; ?></h5>
    </div>
    <div class="opt-mul">
        <ul>
            <?php 
            if($nextquestion['question_bank_options']){
               
                foreach($nextquestion['question_bank_options'] as $optionval){  ?>
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input option-selected optionclass<?php echo $optionval['position']; ?>" name="answer<?php echo $nextquestion['id']; ?>" id="switch<?php echo $optioncount;?>" value="<?php echo $optionval['position'];?>" <?php if(@$user_exam_data->given_answer == $optionval['position']) {?> checked="checked" <?php } ?> disabled='disabled'>
                            <label class="custom-control-label" for="switch<?php echo $optioncount;?>">
                                <?php if(!empty($optionval['options'])){ echo $optionval['options']; } elseif($optionval['option_image']) { ?>
                                <img src="{{url('public/images/exam')}}/{{$optionval['option_image']}}">  <?php } ?>
                            </label>
                        </div>
                    </li>
            <?php $optioncount++;}
            }
            ?>
        </ul>
    </div>

    <?php 
        if(@$user_exam_data->ans_status == 'true'){ ?>
            <p class="right-answer">Your answer is rigth</p>
        <?php } else{ ?>
            <p class="wrong-answer">Your answer is wrong</p>
        <?php } ?>
    
    <?php 
    $user_exam_question_options = DB::table('user_exam_question_options')->where(['user_exam_question_id'=>@$user_exam_data->id])->first(); 
    ?>
    <p class="correct_answer">Correct Answer: <?php if(!empty( @$user_exam_question_options->options )){ echo @$user_exam_question_options->options; } elseif(@$user_exam_question_options->option_image) { ?>
        <img src="{{url('public/images/exam')}}/{{@$user_exam_question_options->options->option_image}}">  <?php } ?>
    </p>
    <br>
    <br>
    <p>Answer Explanation</p><?php if(!empty( @$user_exam_data->answer_explanation )){ echo  @$user_exam_data->answer_explanation; } elseif(@$user_exam_data->answer_explanation_image) { ?>
        <img src="{{url('public/images/exam')}}/{{@$user_exam_data->answer_explanation_image}}">  <?php } ?>
    <?php }   ?>
</div>
