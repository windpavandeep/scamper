<style type="text/css">
.canvasjs-chart-canvas {
    box-shadow: 3px 5px 14px #dbd9d9;
}
@media (min-width: 576px){
    .chart-box {
        flex: 1 1 !important;
    }
}

</style>
@extends('frontEnd.layouts.student_master')
@section('title','Prep Test')
@section('content')
<?php 
use App\UserExam;
?>
 
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_home_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                             @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">
                                
                                <!-- include('frontEnd.common.student_dashboard_sub_header') -->
                                            
                                <div class="card_shd">
                                    <input type="hidden" name="active_course"  value="<?php echo $active_course_id;?>" id="active_course_id">
                                    <ul class="nav nav-tabs pos_rel course_replace" id="myTab" role="tablist">
                                        
                                        <?php 

                                            $maincoursecount  =  1;
                                            if(!empty($maincourse) && count($maincourse)>0) {

                                                foreach($maincourse as $maincoursevalue) {
                                                    $topclass  =  '';
                                                    if($maincoursecount == 1){
                                                        $topclass = "active"; 
                                                    }
                                                    if($maincoursecount <= 1){
                                                  ?>
                                                    <li class="nav-item wid_cuss">
                                                        <a class="main-course-click nav-link <?php echo $topclass;?>" rel="<?php echo  $maincoursevalue['id']; ?>" id="home-tab" data-toggle="tab" href="#home<?php echo $maincoursevalue['id']; ?>" role="tab" aria-controls="home" aria-selected="true"><?php echo $maincoursevalue['title']; ?></a>
                                                    </li>
                                               <?php } $maincoursecount++; }  ?>

                                               <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">More</a>
                                                        <div class="dropdown-menu">
                                                            <?php $maincoursecountsecond =  1; 
                                                            foreach($maincourse as $maincoursevalue) { 
                                                                if($maincoursecountsecond > 1){ 
                                                                ?>
                                                            <a class="dropdown-item main-course-click" id="subj<?php echo $maincoursevalue['id']; ?>" rel="<?php echo  $maincoursevalue['id']; ?>"><?php echo $maincoursevalue['title']; ?></a>
                                                            <?php } $maincoursecountsecond++; } ?>
                                                        </div>
                                                   </li>

                                        <?php  }
                                        ?>
                                        <div class="wrpa_hedr_cont">
                                            <ul class="d-flex align-items-center" type="none">
                                               <!--  <li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li> -->
                                                <li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
                                                <li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
                                            </ul>
                                        </div>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <?php 
                                        $maincoursecount  =  1;
                                        if(!empty($maincourse) && count($maincourse)>0) {
                                            foreach($maincourse as $maincoursevalue) {  
                                                $top_class = "";
                                                if( $maincoursecount  == 1) {
                                                    $top_class  = "active";
                                                }
                                                ?>
                                                <div class="tab-pane fade show <?php echo $top_class; ?>" id="home<?php echo $maincoursevalue['id'];?>" role="tabpanel" aria-labelledby="home-tab">
                                                                                        
                                                    <section class="main_cntnt_dash page_div">
                                                        <div class="prep_strt_sec padtb40">
                                                            <div class="sec_heading text-left d-flex justify-content-between">
                                                                <h2 class="chg_font">Prep Test</h2>
                                                            </div>
                                                            <div class="selc_fltr">
                                                                <form class="form-inline justify-content-between" action="{{url('student/prep-exam-instructions')}}" id="prep-su-subject" method="post">

                                                                    <div class="wrap_selc">
                                                                        <input type="hidden" name="sub_subcategory_id" id="sub_subcategory_id" value="<?php echo $sub_subcategory_id;?>">
                                                                        <select name="subject_id" class="custom-select subject-change" id="subject_id">
                                                                            <option selected  value="" id="subject_option_id">Select Subject</option>
                                                                            <?php 
                                                                                if(!empty($subject)){
                                                                                    foreach($subject as $subjectval){ ?>
                                                                                        <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                               <?php }
                                                                                }
                                                                            ?>
                                                                        </select>
                                                                        <select name="unit_id" class="custom-select unit-change" id="unit_id">
                                                                            <option value="">Select Units</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="text-right">
                                                                        <button type="submit" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-eye"></i> Start Test</button>
                                                                    </div>
                                                                    @csrf
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </section>

                                                    <section class="main_cntnt_dash page_div">
                                                        <div class="sub_reslt_div_sec padtb40">
                                                            <div class="sec_heading text-left d-flex justify-content-between">
                                                                <h2 class="chg_font">Analysis Report</h2>
                                                            </div>
                                                            <!--  -->
                                                            <div class="chrt_perfor change-analysis-report">
                                                                <div class="row">
                                                                    <div class="col-sm-3 chart-box">
                                                                        <div class="wrp_chrt_cir">
                                                                            <h4>All Subjects</h4>
                                                                            <?php
                                                                                $trade  = "Poor"; 
                                                                                $color  = "red";
                                                                                $colorclass = "chartr";
                                                                               // dd($all_subject_score);
                                                                                if($all_subject_score > 0 && $all_subject_score <= 50){
                                                                                    //dd('here');
                                                                                    $trade  = "Poor";
                                                                                    $color  = "red"; 
                                                                                    $colorclass = "chartr";
                                                                                } 
                                                                                if($all_subject_score > 50 && $all_subject_score <= 70){
                                                                                   // dd('yha bhi');
                                                                                    $trade  = "Good";
                                                                                    $color  = "orange"; 
                                                                                    $colorclass = "charto";
                                                                                } 
                                                                                if($all_subject_score > 70 && $all_subject_score <= 85){
                                                                                    $trade  = "Very Good";
                                                                                    $color  = "blue"; 
                                                                                    $colorclass = "chartb";
                                                                                } 
                                                                                if($all_subject_score > 85 && $all_subject_score <= 100){
                                                                                    $trade  = "Excellent";
                                                                                    $color  = "green"; 
                                                                                    $colorclass = "chartg";
                                                                                }

                                                                                if($no_test_given == 1){
                                                                                    $trade  = "No Test Given";
                                                                                    $color  = "red"; 
                                                                                    $colorclass = "chartr";
                                                                                }
                                                                               // echo "<pre>";print_r($colorclass); die;
                                                                            ?>
                                                                            <div class="chart <?php echo $colorclass; ?>" data-percent="<?php echo $all_subject_score; ?>" data-scale-color="<?php echo $color; ?>"><span class="txt_abslt"><?php echo $all_subject_score; ?>%</span></div>
                                                                            <p class="text-center">
                                                                                <?php echo  $trade; ?>
                                                                           </p>
                                                                        </div>
                                                                    </div>
                                                                    <?php 
                                                                    if(!empty($subject)){
                                                                        foreach($subject as $subjectval){ 
                                                                            $subject_analysis  = UserExam::where(['user_id'=>$userId,'subject_id'=>$subjectval['id'],'exam_id'=>0])->get()->toArray();
                                                                            
                                                                            $total_marks  = 0;
                                                                            $total_marks_obtained  = 0;
                                                                            $no_test_given  = 0;
                                                                            if(!empty($subject_analysis)){
                                                                                foreach($subject_analysis as $subject_analysis_val){
                                                                                    $total_marks  =   $total_marks+$subject_analysis_val['total_marks'];
                                                                                    $total_marks_obtained  = $total_marks_obtained+$subject_analysis_val['marks_obtained'];
                                                                                }
                                                                            } else{
                                                                               // die('here');
                                                                                $no_test_given  = 1;
                                                                            }
                                                                        

                                                                            if($total_marks > 0){    
                                                                                $subject_score  =  round($total_marks_obtained*100/$total_marks);
                                                                               $subject_score  = intval($subject_score);
                                                                            } else{
                                                                                $subject_score  = 0;
                                                                            }

                                                                            $trade  = "Poor"; 
                                                                            $color  = "red";
                                                                            $colorclass = "chartr";
                                                                            if($subject_score >0 && $subject_score <= 50){
                                                                                $trade  = "Poor";
                                                                                $color  = "red"; 
                                                                                $colorclass = "chartr";
                                                                            } 
                                                                            if($subject_score >50 && $subject_score <= 70){
                                                                                //echo "here"; die;
                                                                                $trade  = "Good";
                                                                                $color  = "orange";
                                                                                $colorclass = "charto"; 
                                                                            } 
                                                                            if($subject_score >70 && $subject_score <= 85){
                                                                                $trade  = "Very Good";
                                                                                $color  = "blue"; 
                                                                                $colorclass = "chartb";
                                                                            } 
                                                                            if($subject_score >85 && $subject_score <= 100){
                                                                                $trade  = "Excellent";
                                                                                $color  = "green"; 
                                                                                $colorclass = "chartg";
                                                                            }
                                                                            if($no_test_given == 1){

                                                                                $trade  = "No Test Given";
                                                                                $color  = "red"; 
                                                                                $colorclass = "chartr";
                                                                            }
                                                                            ?>
                                                                            <div class="col-sm-3 chart-box">
                                                                                <div class="wrp_chrt_cir">
                                                                                    <h4><?php echo $subjectval['name']; ?></h4>
                                                                                    <div class="chart <?php echo $colorclass;?>" data-percent="<?php echo $subject_score; ?>" data-scale-color="<?php echo $color; ?>"><span class="txt_abslt"><?php echo $subject_score;?>%</span></div>
                                                                                    <p class="text-center"><?php echo $trade; ?></p>
                                                                                </div>
                                                                            </div>
                                                                    <?php }
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>

                                                    <section class="main_cntnt_dash page_div">
                                                        <div class="grph_anal_sec padtb40">
                                                            <div class="sec_heading text-left d-flex justify-content-between">
                                                                <h2 class="chg_font">Graph Analysis</h2>
                                                                <span class="wrap_selc">
                                                                    <select name="subject_id" class="custom-select graph_change_subject" >
                                                                    <option value="all_subject">All Subjects</option>    
                                                                        <?php 
                                                                            if(!empty($subject)){
                                                                                foreach($subject as $subjectval){ ?>
                                                                                    <option value="<?php echo $subjectval['id']; ?>"><?php echo $subjectval['name']; ?></option>
                                                                           <?php }
                                                                            }
                                                                        ?>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                            <!--  -->
                                                            <div class="chrt_grph_anal graph_analysis_replace">
                                                                <div class="grph_alsis row">
                                                                <?php 
                                                                //if(!empty($graph_data) && count($graph_data) > 0){  ?>
                                                                    <div id="chrt_grph" class="chrt_grph"></div>
                                                                    <div class="graph-score_math">
                                                                        <span class="graph-score-attempts"><strong>Total Attempts </strong> <p class="graph-score-attempts-numbers"><strong><?php echo $total_attempt;?></strong></p></span>
                                                                        <span class="graph-score-average"><strong>Average Score</strong> <p class="graph-score-average-numbers"><strong><?php echo $average_score;?></strong></p></span>

                                                                        <span class="graph-score-remarks <?php echo $trade_color_class;?>"><strong>Remarks</strong> <p class="graph-score-remarks-numbers <?php echo $trade_color_class;?>"><strong><?php echo $graph_trade;?></strong></p></span>  

                                                                    </div>
                                                                 <?php //} else {  ?>
                                                                    <!-- <div class="no-graph-data">No Analysis Found Yet!! </div> -->
                                                                <?php // } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>

                                                    <section style="display: flex; justify-content: space-between;" class="main_cntnt_dash page_div accuracy_score">

                                                        <div style = "width:50%" class="grph_score_sec padtb40">
                                                            <div class="sec_heading text-left d-flex justify-content-between">
                                                                <h2 class="chg_font">Accuracy</h2>
                                                            </div>
                                                            <!--  -->
                                                            <div class="chrt_score_anal">
                                                                <div class="grph_alsis col-sm-8">
                                                                    <!-- <div id="chrt_scor"></div> -->
                                                                    <div class="col-sm-12 chart-box">
                                                                        <div class="wrp_chrt_cir">
                                                                            <h4>Correct Answer</h4>
                                                                            <div class="chart <?php echo $colorclass;?>" data-percent="<?php echo $subject_score; ?>" data-scale-color="<?php echo $color; ?>"><span class="txt_abslt"><?php echo $subject_score;?>%</span></div>
                                                                            <p class="text-center"><?php echo $trade; ?></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div style = "width:50%" class="grph_score_sec padtb40">
                                                            <div class="sec_heading text-left d-flex justify-content-between">
                                                                <h2 class="chg_font">Score Analytics</h2>
                                                            </div>
                                                            <!--  -->
                                                            <div class="chrt_score_anal">
                                                                <div class="grph_alsis col-sm-12">
                                                                    <div id="chrt_scor"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                        <?php   $maincoursecount++; } } ?>
                                      
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>
<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>
<!-- modal video -->
<div class="modal" id="modal_vid">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div id="vidBox">
                <video id="demo" loop="" controls="" width="100%" height="100%">
                  <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
                </video>
            </div>
          </div>
        </div>
   </div>
</div>
<!-- modal video -->

@endsection

@section('scripts')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script> <!-- circular chart -->
<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script> -->


<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script src="{{url('public/frontEnd/css/student/canvasjs.min.js')}}"></script> 
<script src="{{url('public/frontEnd/css/student/jquery.canvasjs.min.js')}}"></script> 


<script>
    window.onload = function () {

       //Better to construct options first and then pass it as a parameter
    var options = {
        animationEnabled: true,
        title: {
            // text: "Mobile Phones Used For",                
            // fontColor: "black"
        },  
        toolTip:{enabled: false},
        axisY: {
            tickThickness: 0,
            lineThickness: 0,
            valueFormatString: " ",
            gridThickness: 0                    
        },
        axisX: {
            tickThickness: 0,
            lineThickness: 0,
            labelFontSize: 18,
            labelFontColor: "black"             
        },
        data: [{
            indexLabelFontSize: 15,
            // toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
            indexLabelPlacement: "inside",
            indexLabelFontColor: "white",
            indexLabelTextAlign: "right",
            indexLabelFontWeight: 400,
            color: "#62C9C3",
            type: "bar",
            dataPoints: [
                { y: 29, label: "Poor", indexLabel: ">50%",color: "red"},
                { y: 32, label: "Good", indexLabel: "50%-70%" ,color: "orange"},
                { y: 64, label: "Very Good", indexLabel: "70%-85%",color: "blue" },
                { y: 73, label: "Excellent", indexLabel: "85%-100%", color:"green" }
            ]
        }]
    };

    $("#chrt_scor").CanvasJSChart(options);

    var graph_data  = [];
    graph_data =  <?php echo json_encode($json_graph_data);?>;

        var chart = new CanvasJS.Chart("chrt_grph", {
            animationEnabled: true,
            theme: "light2",
            toolTip:{
                content: "Test Date: {label} <br/>Total Score: {y}",
            },
            axisY:{
                includeZero: true,
                maximum: 40,
                title: "Total Marks",
                labelFontColor: "red",
            },
            axisX:{
                labelFontColor: "red",
            },
            data: [{
            type: "line",
            markerType: "circle",
            markerSize: 8,
            markerColor: "#4682B4",
            dataPoints:graph_data
          },],
        });
    

    showDefaultText(chart, "No Data Found Yet! Give Test to Start Analysis");
    chart.render();

    function showDefaultText(chart, text){
           var dataPoints = chart.options.data[0].dataPoints;
           var isEmpty = !(dataPoints && dataPoints.length > 0);
           
           if(!isEmpty){
            for(var i = 0; i < dataPoints.length; i++){
              isEmpty = !dataPoints[i].y;
                if(!isEmpty)
                break;
            }
           }
           if(!chart.options.subtitles)
             chart.options.subtitles = [];
           if(isEmpty)
            chart.options.subtitles.push({
                text : text,
                verticalAlign : 'center',
                fontColor: "red",
                fontSize: 25,
            });
           else
                chart.options.subtitles = [];
    }

 
}
</script>




<!-- script files -->
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#slider_catg").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
        });
        $("#slider_notes").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
        $(function() {
          $('.chartb').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "blue"
          });
          $('.chartr').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "red"
          });
          $('.chartg').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "green"
          });
          $('.charto').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "orange",
          });
        });
    });
</script>
<script type="text/javascript">
    $('.main-course-click').on('click', function () { 
        $('.loader_ques').show();
        var main_course_id  =   $(this).attr('rel');
        var main_course  = <?php echo json_encode($maincourse);?>;
        $.ajax({
            type:"post",
            url: "{{ url('student/get_subject_by_Condtion') }}",
            data:{'main_course':main_course,'course_id':main_course_id,'_token': "{{ csrf_token() }}"},
            dataType:'json', 
            success:function(resp){
                $('.loader_ques').hide();
                $('.course_replace').replaceWith(resp.render_view);
                $('.subject-change').empty();
                $('.unit-change').empty();
                $('.change-analysis-report').empty();
                $('.graph_change_subject').empty();
                $('.subject-change').html(resp.subject_options);
                $('.graph_change_subject').empty();
                $('#sub_subcategory_id').val(resp.sub_subcategory_id);
                $('.change-analysis-report').replaceWith(resp.analysis_report);
                $('.graph_change_subject').html(resp.subject_options_graph);
                $('.graph_analysis_replace').empty();
                $('.graph_analysis_replace').replaceWith(resp.graph_report);
                $('#active_course_id').val(resp.active_course_id);
            }
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.loader_ques').hide();
    });
</script>
<script>
    $('.subject-change').on('change', function () { //alert('ge');
        $('.loader_ques').show();
        var clickedIndex  = $(this).val();
        //alert(clickedIndex);
        //$('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/unit') }}"+"/"+clickedIndex,
            success:function(resp){
                $('.loader_ques').hide();
                $('.unit-change').empty();
                $('.unit-change').html(resp);
                //$("#unit_id").selectpicker("refresh");
            }
        })
    });
</script>
<script type="text/javascript">
    $('#prep-su-subject').validate({
        rules:{
            subject_id:{
                required:true
            },
            unit_id:{
                required:true
            },
           
        },
        messages:{
            subject_id:{
                remote:"Please first select subject"
            },
            unit_id:{
                regex:"Please first select unit"
            }
        },
        errorPlacement:function(error,element){
            if(element.attr("name") == "terms") {
                error.appendTo(element.parent().parent().after());
            } else{
                error.appendTo(element.parent().after());
            }
        },
    })
</script>
<script type="text/javascript">
    $('.graph_change_subject').on('change', function () { 
            $('.loader_ques').show();
            var clickedIndex  = $(this).val();
            var active_course_id  =  $('#active_course_id').val();
            $.ajax({
                type:"get",
                url: "{{ url('student/get/graph_analysis') }}"+"/"+clickedIndex+"/"+active_course_id,
                success:function(resp){
                    $('.loader_ques').hide();
                    // if(resp.status == true){
                    //     chart.data = [
                    //         resp.graph_data
                    //     ]
                    // } else{
                        $('.graph_analysis_replace').empty();
                        $('.graph_analysis_replace').replaceWith(resp.graph_data);
                    //}
                }
            })
        });
</script>




@endsection