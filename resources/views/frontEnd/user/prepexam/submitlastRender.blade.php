<style type="text/css">
.myModalreportcardclass .modal-title { 
    text-align: center;
    color: #e89a36;
    text-shadow: 1px 2px 2px #e89a36;
}

.myModalreportcardclass .test_unit {
    text-align: center;
    color: #0094de;
}

.myModalreportcardclass .modal-header {
    display: flex;
    text-align: center;
    color: #000;
}


.myModalreportcardclass .modal-header p{
    font-weight: normal;
    color: #000;
}
.myModalreportcardclass .time_tkn{
    color: #000;
}
.myModalreportcardclass .time_per_q p {
    font-size: 14px;    
}

.myModalreportcardclass .rounded-lg.time_per_q {
    position: relative;
    margin-top: 110px;
}

.myModalreportcardclass .gt_dshbrd {
    text-align: center;
    margin-bottom: 9px;
}

.myModalreportcardclass .progress_mtpl {    
    font-size: 11px;
    font-weight: 600;
}

.myModalreportcardclass .progress_mtpl p {
    color: #fff;
    font-size: 13px;
    font-weight: 500;
}

.remarks{
    text-align: center;      
    margin: 10px 0px;
}

.remarks .ex{
    color:  #b1c668;   
    font-weight: bold;
    text-shadow: 0px 0px 0px gray;
}

.remarks .vg{
    color: #72b3f4;   
    font-weight: bold;
    text-shadow: 0px 0px 0px gray;
}

.remarks .gd{
    color:#feb44c;    
    font-weight: bold;
    text-shadow: 0px 0px 0px gray;
}

.remarks .pr{
    color: #f25656;    
    font-weight: bold;
    text-shadow: 0px 0px 0px gray;
}

.remarks_heading{
    font-weight: 500;
    text-shadow: 0px 1px 0px gray;
}

.progress .progress-bar { border-width: 25px !important;}

</style>
<div class="modal fade myModalreportcardclass" id="myModalreportcard">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <div class="col-sm-6 ">
                    <p class="d_date">Date: <?php echo date('d F Y');?> </p>
                </div>
                <div class="col-sm-3 offset-3">
                    <p class="t_marks"> Total Marks: 40</p>
                    <p class="t_time"> Total Time: 20 Min</p>
                </div>
            </div>
                <h1 class="modal-title">PREP TEST REPORT CARD</h1>
                <p class="test_unit"> <?php echo $subcategory_name; ?>, <?php echo $subject_name;?>, <?php echo $unit_name; ?> </p>
                <div class="modal-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-4 col-lg-4 mb-4">
                                <div class="bg-white rounded-lg p-4 shadow">
                                    <h2 class="h6  text-center mb-4">Total Score</h2>
                                    <!-- Progress bar 1 -->
                                    <div class="progress mx-auto" data-value='<?php echo $reviewtestrecord['total_mark_obtain']; ?>' style="thick:8">
                                        <span class="progress-left">
                                        <span class="progress-bar border-primary"></span>
                                        </span>
                                        <span class="progress-right">
                                        <span class="progress-bar border-primary"></span>
                                        </span>
                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                            <div class="h2"><?php echo $reviewtestrecord['total_mark_obtain'];?> / <?php echo $reviewtestrecord['total_marks'];?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 mb-4">
                                <div class="rounded-lg  time_per_q">                                           
                                    <div class="row text-center">
                                        <div class="col-12">                                                    
                                            <p class="time_tkn">Time Taken : <?php echo $reviewtestrecord['time_taken'];?></p>
                                        </div>
                                        <div class="col-12">
                                            <p class="time_tkn">Per Q. Time Taken: <?php echo $reviewtestrecord['per_question_time']; ?></p>
                                        </div>
                                    </div>
                                    <!-- END -->
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 mb-4">
                                <div class="bg-white rounded-lg p-4 shadow">
                                    <h2 class="h6 text-center mb-4">Percentage Score</h2>
                                    <!-- Progress bar 2 -->
                                    <div class="progress mx-auto" data-value='<?php echo $reviewtestrecord['percentage_score'];?>'>
                                        <span class="progress-left">
                                        <span class="progress-bar border-danger"></span>
                                        </span>
                                        <span class="progress-right">
                                        <span class="progress-bar border-danger"></span>
                                        </span>
                                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                            <div class="h2"><?php echo $reviewtestrecord['percentage_score'];?>%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                        <div class="remarks"> 
                            <h6 class="remarks_heading">Test Remarks </h6>
                            <?php 
                            $average_score = $reviewtestrecord['percentage_score'];
                            if($average_score >= 0 && $average_score <= 50){ ?>
                               <p class="pr"> Poor </p>
                            <?php } 
                            if($average_score >50 && $average_score <= 70){ ?>
                                <p class="gd"> Good </p>
                            <?php } 
                            if($average_score >70 && $average_score <= 85){ ?>
                                <p class="vg"> Very Good </p>
                            <?php } 
                            if($average_score >85 && $average_score <= 100){ ?>
                                <p class="ex"> Excellent </p>
                           <?php  }
                            ?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="label">
                            <label> <b> Questions :  </b> </label>
                        </div>
                        <div class="progress_mtpl">
                          <div class="progress-bar" style="width:<?php echo $reviewtestrecord['correct_answer']*10;?>%">
                            <p> Correct <b> <?php echo $reviewtestrecord['correct_answer'];?> </b> </p>
                          </div>                             
                          <div class="progress-bar bg-danger" style="width:<?php echo $reviewtestrecord['incorrect_answer']*10;?>%">
                            <p> Incorrect <b> <?php echo $reviewtestrecord['incorrect_answer'];?> </b> </p>
                          </div>
                           <div class="progress-bar bg-warning" style="width:<?php echo $reviewtestrecord['left']*10; ?>%">
                            <p> Left <b> <?php echo $reviewtestrecord['left']; ?> </b> </p>
                          </div>
                        </div> 
                    </div>
                    <!-- Multi Progress Bar -->
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <form action="{{url('student/prep-test-review')}}" method="post">
                        @csrf
                       <?php 
                        foreach($question_array as $value)
                        {
                            echo '<input type="hidden" name="question[]" value="'. $value. '">';
                        }
                        ?>
                        <input type="hidden" name="user_exam_id" value="<?php echo @$user_exam_id;?>">
                        <input type="hidden" name="subject_name" value="<?php echo @$subject_name;?>">
                        <input type="hidden" name="unit_name" value="<?php echo @$unit_name;?>">
                        <input type="hidden" name="subcategory_name" value="<?php echo @$subcategory_name;?>">
                        <button class="btn btn-success right-side-question" type="submit">Review Test</button>
                    </form>
                </div>
                <div class="gt_dshbrd">
                  <a href="{{url('student/prep-exam')}}"> Go To Dashboard </a>
                </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function() {
    
    $(".progress").each(function() {
    
      var value = $(this).attr('data-value');
      var left = $(this).find('.progress-left .progress-bar');
      var right = $(this).find('.progress-right .progress-bar');
    
      if (value > 0) {
        if (value <= 50) {
          right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
        } else {
          right.css('transform', 'rotate(180deg)')
          left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
        }
      }
    
    })
    
    function percentageToDegrees(percentage) {
    
      return percentage / 100 * 360
    
    }
    
    });   
</script>


