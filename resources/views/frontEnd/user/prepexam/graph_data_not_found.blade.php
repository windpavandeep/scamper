<div class="chrt_grph_anal graph_analysis_replace">
    <div class="grph_alsis row">
            <div id="chrt_grph2" class="chrt_grph"></div>
            <div class="graph-score_math">
                <span class="graph-score-attempts"><strong>Total Attempts </strong> <p class="graph-score-attempts-numbers"><strong><?php echo $total_attempt;?></strong></p></span>
                <span class="graph-score-average"><strong>Average Score</strong> <p class="graph-score-average-numbers"><strong><?php echo $average_score;?></strong></p></span>

                <span class="graph-score-remarks <?php echo $trade_color_class;?>"><strong>Remarks</strong> <p class="graph-score-remarks-numbers <?php echo $trade_color_class;?>"><strong><?php echo $graph_trade;?></strong></p></span>  

            </div>
    </div>
</div>


<script>
    //window.onload = function () {
    var graph_data  = [];
    graph_data =  <?php echo json_encode($json_graph_data);?>;
        console.log(graph_data);
        var chart = new CanvasJS.Chart("chrt_grph2", {
            animationEnabled: true,
            theme: "light2",
            toolTip:{
                content: "Test Date: {label} <br/>Total Score: {y}",
            },
            axisY:{
                includeZero: true,
                maximum: 40,
                title: "Total Marks",
                labelFontColor: "red",
            },
            axisX:{
                labelFontColor: "red",
            },
            data: [{
            type: "line",
            markerType: "circle",
            markerSize: 8,
            markerColor: "#4682B4",
            dataPoints:graph_data
          },]
        });
    

    showDefaultText(chart, "No Data Found Yet! Give Test to Start Analysis");
    chart.render();

    function showDefaultText(chart, text){
           var dataPoints = chart.options.data[0].dataPoints;
           var isEmpty = !(dataPoints && dataPoints.length > 0);
           
           if(!isEmpty){
            for(var i = 0; i < dataPoints.length; i++){
              isEmpty = !dataPoints[i].y;
                if(!isEmpty)
                break;
            }
           }
           if(!chart.options.subtitles)
             chart.options.subtitles = [];
           if(isEmpty)
            chart.options.subtitles.push({
                 text : text,
                 verticalAlign : 'center',
                 fontColor: "red",
                 fontSize: 25,
               });
           else
                chart.options.subtitles = [];
    }

   // }
</script>

