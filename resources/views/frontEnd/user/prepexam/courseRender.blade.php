<ul class="nav nav-tabs pos_rel course_replace" id="myTab" role="tablist">
    <?php 

        $maincoursecount  =  1;
        if(!empty($maincourse) && count($maincourse)>0) {

            foreach($maincourse as $maincoursevalue) {
                $topclass  =  '';
                if($maincoursecount == 1){
                    $topclass = "active"; 
                }
                if($maincoursecount <= 1){
              ?>
                <li class="nav-item wid_cuss">
                    <a class="main-course-click nav-link <?php echo $topclass;?>" rel="<?php echo  $maincoursevalue['id']; ?>" id="home-tab" data-toggle="tab" href="#home<?php echo $maincoursevalue['id']; ?>" role="tab" aria-controls="home" aria-selected="true"><?php echo $maincoursevalue['title']; ?></a>
                </li>
           <?php } $maincoursecount++; }  ?>

           <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">More</a>
                    <div class="dropdown-menu">
                        <?php $maincoursecountsecond =  1; 
                        foreach($maincourse as $maincoursevalue) { 
                            if($maincoursecountsecond > 1){ 
                            ?>
                        <a class="dropdown-item main-course-click" id="subj<?php echo $maincoursevalue['id']; ?>" rel="<?php echo  $maincoursevalue['id']; ?>"><?php echo $maincoursevalue['title']; ?></a>
                        <?php } $maincoursecountsecond++; } ?>
                    </div>
               </li>

    <?php  }
    ?>
    <div class="wrpa_hedr_cont">
        <ul class="d-flex align-items-center" type="none">
            <!-- <li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li> -->
            <li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
            <li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
        </ul>
    </div>
</ul>


<script type="text/javascript">
    $('.main-course-click').on('click', function () { 
        $('.loader_ques').show();
        var main_course_id  =   $(this).attr('rel');
        var main_course  = <?php echo json_encode($maincourse);?>;
        $.ajax({
            type:"post",
            url: "{{ url('student/get_subject_by_Condtion') }}",
            data:{'main_course':main_course,'course_id':main_course_id,'_token': "{{ csrf_token() }}"},
            dataType:'json', 
            success:function(resp){
                $('.loader_ques').hide();
                $('.course_replace').replaceWith(resp.render_view);
                $('.subject-change').empty();
                $('.subject-change').html(resp.subject_options);
                $('.graph_change_subject').empty();
                $('.graph_change_subject').html(resp.subject_options_graph);
                $('#sub_subcategory_id').val(resp.sub_subcategory_id);
                $('.change-analysis-report').empty();
                $('.change-analysis-report').replaceWith(resp.analysis_report);
                $('#active_course_id').val(resp.active_course_id);
                $('.graph_analysis_replace').empty();
                $('.graph_analysis_replace').replaceWith(resp.graph_report);
            }
        })
    });
</script>