<?php 
use App\UserExam;
?>
<div class="chrt_perfor change-analysis-report">
    <div class="row">
        <div class="col-sm-3">
            <div class="wrp_chrt_cir">
                <h4>All Subjects</h4>
                <?php
                    $trade  = "Poor"; 
                    $color  = "red";
                    $colorclass = "chartr";
                    if($all_subject_score >0 && $all_subject_score <= 50){
                        $trade  = "Poor";
                        $color  = "red"; 
                        $colorclass = "chartr";
                    } 
                    if($all_subject_score >50 && $all_subject_score <= 70){
                        $trade  = "Good";
                        $color  = "orange"; 
                        $colorclass = "charto";
                    } 
                    if($all_subject_score >70 && $all_subject_score <= 85){
                        $trade  = "Very Good";
                        $color  = "blue";
                         $colorclass = "chartb"; 
                    } 
                    if($all_subject_score >85 && $all_subject_score <= 100){
                        $trade  = "Excellent";
                        $color  = "green"; 
                        $colorclass = "chartg";
                    }

                    if($no_test_given == 1){
                        $trade  = "No Test Given";
                        $color  = "red"; 
                        $colorclass = "chartr";
                    }
                ?>
                <div class="chart <?php echo $colorclass;?>" data-percent="<?php echo $all_subject_score; ?>" data-scale-color="<?php echo $color; ?>"><span class="txt_abslt"><?php echo $all_subject_score; ?>%</span></div>
                <p class="text-center"> <?php echo  $trade; ?></p>
            </div>
        </div>
        <?php 
        if(!empty($subject)){
            foreach($subject as $subjectval){ 
                $subject_analysis  = UserExam::where(['user_id'=>$userId,'subject_id'=>$subjectval['id'],'exam_id'=>0])->get()->toArray();
                $total_marks  = 0;
                $total_marks_obtained  = 0;
                $no_test_given  = 0;
                if(!empty($subject_analysis)){
                    foreach($subject_analysis as $subject_analysis_val){
                        $total_marks  =   $total_marks+$subject_analysis_val['total_marks'];
                        $total_marks_obtained  = $total_marks_obtained+$subject_analysis_val['marks_obtained'];
                    }
                } else{
                    $no_test_given  = 1;
                }

                if($total_marks > 0){    
                    $subject_score  =  round($total_marks_obtained*100/$total_marks);
                } else{
                    $subject_score  = 0;
                }

                $trade  = "Poor"; 
                $color  = "red";
                $colorclass = "chartr";
                if($subject_score >0 && $subject_score <= 50){
                    $trade  = "Poor";
                    $color  = "red"; 
                    $colorclass = "chartr";
                } 
                if($subject_score >50 && $subject_score <= 70){
                    $trade  = "Good";
                    $color  = "orange"; 
                    $colorclass = "charto";
                } 
                if($subject_score >70 && $subject_score <= 85){
                    $trade  = "Very Good";
                    $color  = "blue"; 
                    $colorclass = "chartb";
                } 
                if($subject_score >85 && $subject_score <= 100){
                    $trade  = "Excellent";
                    $color  = "green";
                    $colorclass = "chartg"; 
                }

                if($no_test_given == 1){
                    $trade  = "No Test Given";
                    $color  = "red"; 
                    $colorclass = "chartr";
                }
                ?>
                <div class="col-sm-3">
                    <div class="wrp_chrt_cir">
                        <h4><?php echo $subjectval['name']; ?></h4>
                        <div class="chart <?php echo $colorclass;?>" data-percent="<?php echo $subject_score; ?>" data-scale-color="<?php echo $color; ?>"><span class="txt_abslt"><?php echo $subject_score;?>%</span></div>
                        <p class="text-center"><?php echo $trade; ?></p>
                    </div>
                </div>
        <?php }
            }
        ?>
    </div>
</div>

<script type="text/javascript">
    $(function() {
          $('.chartb').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "blue"
          });
          $('.chartr').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "red"
          });
          $('.chartg').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "green"
          });
          $('.charto').easyPieChart({
            size: 160,
            scaleLength: 0,
            lineWidth: 25,
            lineCap: "circle",
            animate: 2000,
            barColor: "orange",
          });
        });
</script>