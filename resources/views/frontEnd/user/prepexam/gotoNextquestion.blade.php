<div class="sngl_qust add_next_question">
<?php if(!empty($nextquestion)) {
    $quescount  = 1;
    $optioncount = 1;
    ?>
    <input type="hidden" name="question" value="<?php echo $nextquestion['id']; ?>" id="get-question_id">
    <div class="quest_top">
        <h5>Q<?php echo $nextccount; ?>.<?php echo $nextquestion['question']; ?></h5>
    </div>
    <div class="opt-mul">
        <ul>
            <?php 
            if($nextquestion['question_bank_options']){
               
                foreach($nextquestion['question_bank_options'] as $optionval){  ?>
                    <li>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input option-selected optionclass<?php echo $nextquestion['id']; ?><?php echo $optionval['position']; ?>" name="answer<?php echo $nextquestion['id']; ?>" id="switch<?php echo $optioncount;?>" value="<?php echo $optionval['position'];?>">
                            <label class="custom-control-label" for="switch<?php echo $optioncount;?>">
                                <?php if(!empty($optionval['options'])){ echo $optionval['options']; } elseif($optionval['option_image']) { ?>
                                <img src="{{url('public/images/exam')}}/{{$optionval['option_image']}}">  <?php } ?>
                            </label>
                        </div>
                    </li>
            <?php $optioncount++;}
            }
            ?>
        </ul>
    </div>
<?php }   ?>
</div>
