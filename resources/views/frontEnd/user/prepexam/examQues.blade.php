@extends('frontEnd.layouts.student_master')
@section('title','Prep Exams')
@section('content')
<style type="text/css">
.modal-header {display: block;text-align: center;}
.modal-title{ color: #aaa;}
.progress {width: 150px;height: 150px;background: none;position: relative;}
.progress::after {content: "";width: 100%;height: 100%;border-radius: 50%;border: 6px solid #eee;position: absolute;top: 0;left: 0;}
.progress>span {width: 50%;height: 100%;overflow: hidden;position: absolute;top: 0;z-index: 1;}
.progress .progress-left {left: 0;}
.progress .progress-bar {width: 100%;height: 100%;background: none;border-width: 6px;border-style: solid;position: absolute;top: 0;}
.progress .progress-left .progress-bar {left: 100%;border-top-right-radius: 80px;border-bottom-right-radius: 80px;border-left: 0;-webkit-transform-origin: center left;transform-origin: center left;}
.progress .progress-right {right: 0;}
.progress .progress-right .progress-bar {left: -100%;border-top-left-radius: 80px;border-bottom-left-radius: 80px;border-right: 0;-webkit-transform-origin: center right;transform-origin: center right;}
.progress .progress-value {position: absolute;top: 0;left: 0;}
body {background: #99ccff;min-height: 100vh;}
.rounded-lg {border-radius: 1rem;}
.text-gray {color: #aaa;}
div.h4 {line-height: 1rem;}
.progress_mtpl {display: -ms-flexbox;display: flex;height: 3rem;overflow: hidden;line-height: 0;font-size: 1.05rem;background-color: #e9ecef;border-radius: .25rem;}
.progress_mtpl p{margin-bottom: 0px !important;}
.modal-footer {justify-content: center;}
.modal-footer .btn {padding: 0.575rem 3.75rem;}
.modal-header .d_date{text-align: start;margin-left: 10px;}            
.modal-header .t_time, .t_marks{text-align: start;}
.time_per_q p{font-size: 20px;}
.review-test-first-question {background: #ffd7bf !important;color: #ff7800 !important;border: none !important;margin: 10px 4px !important;border-radius: 30px !important;font-size: 14px !important;padding: 3px 16px !important;}
</style>
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon exam_page_ques_main"> <!-- change class -->
                    <div class="chpr_ttl_tme d-flex justify-content-between">
                        <h5>Prep Test
                                <p>{{ucfirst(@$sub_subcategory->name)}},<?php echo @$subject['name'];?>,<?php echo $unit['name'];?></p></h5>
                        <div>
                            <span class="tme_crnt" id="timer">Time Left:<?php echo $totaltime;?></span>
                            <a class="instt_hr" data-toggle="modal" data-target="#inst_modal"><i class="fa fa-info"></i> instructions</a>
                        </div>
                    </div>
                   <!--  <div class="chpr_ttl_tme">
                        <h5><?php //echo @$unit['name'];?></h5>
                        <span class="tme_crnt" id="timer1">Time Left:<?php //echo $totaltime;?></span>
                    </div>
 -->                
                         <!--  <div class="chpr_subss">
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="javascript:;">Prep Test</a></li>
                                    <li class="list-inline-item"><a href="javascript:;">{{ucfirst(@$sub_subcategory->name)}}</a></li>
                                    <li class="list-inline-item"><a href="javascript:;"><?php //echo $subject['name']; ?></a></li>
                                    <li class="list-inline-item"><a href="javascript:;"><?php// echo $unit['name'];?></a></li>
                                </ul>
                            </div> -->
                        <div class="wrpr_main_exam d-flex ">
                                <div class="main_left d-flex flex-column justify-content-start">
                                        <div class="question_activity">
                                            <div class="sngl_qust add_next_question">
                                                <?php if(!empty($question)) {
                                                    $quescount  = 1;
                                                    $optioncount = 1;
                                                    foreach($question  as $questionval) {
                                                    if($quescount == 1){  
                                                    ?>
                                                    <input type="hidden" name="question" value="<?php echo $questionval['id']; ?>" id="get-question_id">
                                                    <div class="quest_top">
                                                        <h5>Q<?php echo $quescount; ?>.<?php echo $questionval['question']; ?></h5>
                                                    </div>
                                                    <div class="opt-mul">
                                                        <ul>
                                                            <?php 
                                                            if($questionval['question_bank_options']){
                                                               
                                                                foreach($questionval['question_bank_options'] as $optionval){  ?>
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input option-selected optionclass<?php echo $questionval['id']; ?><?php echo $optionval['position']; ?>" name="answer<?php echo $questionval['id']; ?>" id="switch<?php echo $optioncount;?>" value="<?php echo $optionval['position'];?>">
                                                                            <label class="custom-control-label" for="switch<?php echo $optioncount;?>">
                                                                                <?php if(!empty($optionval['options'])){ echo $optionval['options']; } elseif($optionval['option_image']) { ?>
                                                                                <img src="{{url('public/images/exam')}}/{{$optionval['option_image']}}">  <?php } ?>
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                            <?php $optioncount++;}
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>

                                                <?php } $quescount++; } } ?>
                                            </div>
                                            
                                        </div>

                                        <!-- <div class="footr_exam d-flex justify-content-between">

                                            <button class="btn btn_mark_rev mark-for-review">Mark for Review</button>
                                            <button class="btn btn_sav_nxt prep-test-submit-class">Save & Next</button>
                                        </div> -->

                                        <div class="footr_exam">
                                            <div class=" d-flex justify-content-start">
                                                <button class="btnus review-test-first-question right-side-question" rel="<?php echo $first_question_id; ?>" rel1="1">Review Test</button>
                                                <button class="btnus btn_sav_nxt  prep-test-submit-class">Save & Next</button>
                                                <button class="btnus btn_outline_rev mark-for-review">Review Later</button>
                                                <button class="btnus btn_outline_clr clear_exam_section">Clear Selection</button>
                                            </div>

                                            <div class="d-flex justify-content-between">
                                                <div class=" text-left">
                                                    <button class="btnus btn_outline_prev go_to_prevoius_question"><i class="fa fa-long-arrow-alt-left"></i> Prev</button>
                                                    <button class="btnus btn_outline_nex go_to_next_question" rel="<?php echo $first_question['id']; ?>">Next <i class="fa fa-long-arrow-alt-right"></i></button>
                                                </div>
                                                <form action="#"  method="post" id="prep-submit-form">
                                                    <input type="hidden" name="subject_id" id="subject_id" value="<?php echo $subject_id;?>">
                                                    <input type="hidden" name="unit_id" id="unit_id" value="<?php echo $unit_id;?>">
                                                    <input type="hidden" name="question_ids" id="question_hidden_field">
                                                    <input type="hidden" name="answer_option" id="answer_hidden_field">
                                                    <input type="hidden" name="total_marks" value="<?php echo $total_marks;?>">
                                                    <input type="hidden"  name="start_date_time" value="<?php echo $start_date_time; ?>">
                                                    <button type="button" class="btnus btn_end_exm final_prep_submit">End Test</button>
                                                @csrf
                                                </form>
                                            </div>
                                       </div>
                                </div>
                                <div class="main_right">
                                        <div class="nmbr_info">
                                            <ul class="ul_numb" type="none">
                                                <li><span class="comn_reco no_ans">1</span> Not Answered</li>
                                                <li><span class="comn_reco yes_ans">2</span> Answered</li>
                                                <li><span class="comn_reco mrk_rev">3</span> Marked Review</li>
                                            </ul>
                                        </div>
                                        <div class="question_serl">
                                            <ul class="ul_numb" type="none">
                                                <?php 
                                                    if(!empty($question)){
                                                        $question_count  =  1;
                                                        foreach($question as $questionval){  ?>
                                                            <input type="hidden" name="answer" id="answer-set-for-change<?php echo $questionval['id'];?>">
                                                            <li class="right-question-li"><a href="javascript:void(0)" class="right-side-question" rel="<?php echo $questionval['id']; ?>" rel1="<?php echo $question_count;?>"><span class="comn_reco no_vst result-status<?php echo $question_count; ?>"><?php echo $question_count;?></a></span></li>
                                                        <?php $question_count++; }
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                       <!--  <form action="{{url('student/prep-test-submit')}}"  method="post" id="prep-submit-form">
                                            <input type="hidden" name="subject_id" value="<?php //echo $subject_id;?>">
                                            <input type="hidden" name="unit_id" value="<?php //echo $unit_id;?>">
                                            <input type="hidden" name="question_ids" id="question_hidden_field">
                                            <input type="hidden" name="answer_option" id="answer_hidden_field">
                                            <div class="footr_exam d-flex justify-content-center">
                                                <button class="btn btn_sav_nxt final_prep_submit">Submit Exam</button>
                                            </div>
                                        @csrf
                                        </form> -->
                                </div>
                        
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>

<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>
<div class="modal" id="inst_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="app_wrap_comon prep_exam_inst_sec"> <!-- change class -->
                    <div class="preInstr">
                        <div class="main_inst text-center">
                            <div class="inr_int">
                                <h3 class="text-uppercase">Instructions</h3>
                                <h5>Please read instructions carefully</h5>
                            </div>
                            <ol>
                                <li>Total duration for exam is <?php echo $totaltime;?> min.</li>
                                <li>CLock wil be on server. When clock reach zero, exam will end itself and get submitted.</li>
                                <li>Below are the color and symbol that will help you in the exam to understand.</li>
                                <li>No time limit to attemp qusetuion in assignment.</li>
                                <li>Type of question will be mentioned along with the questions.</li>
                                <li>One question can be attempted only once.</li>
                            </ol>
                           <!--  <div class="btn_start">
                                <a href="examQues.php" class="btn btn_gradient btn_active"> Start Test</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container append-report_mode" >
</div>

@endsection
@section('scripts')

<script>
$( document ).ready(function() {
    $('.review-test-first-question').hide();
    $('.loader_ques').hide();
    $('.go_to_prevoius_question').hide();
})
</script>
<script>
    function incTimer() {
        if(totalSecs == 0){
            $('#timer').replaceWith('');
            $("#prep-submit-form").submit();
        }
        var currentMinutes = Math.floor(totalSecs / 60);
        var currentSeconds = totalSecs % 60;
        if(currentSeconds <= 9) currentSeconds = "0" + currentSeconds;
        if(currentMinutes <= 9) currentMinutes = "0" + currentMinutes;
        totalSecs--;
        $("#timer").text("Time Left: "+currentMinutes + ":" + currentSeconds);
        //$("#timer1").text(currentMinutes + ":" + currentSeconds);
        setTimeout('incTimer()', 1000);
    }

    totalSecs = <?php echo $totaltimesecond;?>

    $(document).ready(function() {
        incTimer();
    });
</script>

<script type="text/javascript">
    //var nextccount  =  1;
    var next_question_total_count  = 1;
    var questionelems = [];
    var answerelems = [];
    var subcategory_name = "<?php echo $sub_subcategory->name;?>";
    var subject_name  = "<?php echo $subject['name'];?>";
    var unit_name  = "<?php echo $unit['name'];?>";
    var start_date_time = "<?php echo $start_date_time;?>";
    var first_question_id  = "<?php echo $first_question_id;?>";

     $('.final_prep_submit').on('click', function () { 

        $('.loader_ques').show();
        var total_question_count  = <?php echo $total_question_count; ?>;
        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        var subject_id  = $('#subject_id').val();
        var unit_id  =  $('#unit_id').val();

        var question =<?php echo json_encode($question);?>;

        $.ajax({
            type:"post",
            url: "{{ url('student/prep-test-submit') }}",
            data:{'_token': "{{ csrf_token() }}",'nextccount':next_question_total_count,'total_question_count':total_question_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id,'subject_id':subject_id,'unit_id':unit_id,'question':question},
            success:function(resp){
                $('#timer').replaceWith('');
                $('.final_prep_submit').replaceWith('<a style="margin: 10px 4px;border-radius: 30px;font-size: 14px;padding: 3px 16px;" href="<?php echo url('student/prep-exam') ?>" class="btnus btn_end_exm prep-exam-back-to-dashboard">Back To Dashboard</a>');
                $('.loader_ques').hide();
                $('.append-report_mode').html(resp.render_view);
               // $('.add_next_question').replaceWith('');
                $('#myModalreportcard').modal();
            }
        })
    });


    $('.prep-test-submit-class').on('click', function () { 
        $('.loader_ques').show();
       
        var total_question_count  = <?php echo $total_question_count; ?>;
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var answerval = $("input:radio.option-selected:checked").val();
        var replaceclass  =  "yes_ans";
        if(answerval == undefined){
            replaceclass  = "no_ans";
        }

        var question_id  =  $('#get-question_id').val();
        $('#answer-set-for-change'+question_id).val(answerval);

        questionelems = $.grep(questionelems, function(e){ 
             return e != question_id; 
        });
        questionelems.push(question_id);
        //answerelems[question_id] =  answerval;
      
       // console.log(answerelems);

        answerelems = $.grep(answerelems, function(e){ 
            console.log(e[0]);
            console.log('chck');
             return e[0] != question_id; 
        });

        answerelems.push([question_id, answerval]);

        $('#question_hidden_field').val(JSON.stringify(questionelems));
        $('#answer_hidden_field').val(JSON.stringify(answerelems));

        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        var answerelems_decode = $('#answer_hidden_field').val();

        $.ajax({
            type:"post",
            url: "{{ url('student/save-next-prep-test') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'nextccount':next_question_total_count,'total_question_count':total_question_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id,'answerelems':answerelems_decode,'question_id':question_id},
            success:function(resp){
                //var new_question_ids  = $('#question_hidden_field').val();
                console.log(".optionclass"+resp.new_question_id+resp.option_value);
                $('.prep-test-submit-class').html('Save & Next');
                $('.final_prep_submit').html('End Test');
                $('.loader_ques').hide();
                $('.go_to_prevoius_question').show();
                $('.go_to_next_question').show();
                 $('.clear_exam_section').show();
                $('.add_next_question').replaceWith(resp.render_view);
                 MathJax.typeset(['.add_next_question']);
                $('.result-status'+next_question_total_count).removeClass('no_vst');
                if(replaceclass == 'yes_ans'){
                    $('.result-status'+next_question_total_count).removeClass('no_ans');
                    $('.result-status'+next_question_total_count).removeClass('mrk_rev');
                }
                $('.result-status'+next_question_total_count).addClass(replaceclass);
                next_question_total_count  = resp.nextccountplus;

               
                $('.go_to_prevoius_question').attr('rel',resp.next_question_id);
                $(".go_to_next_question").attr('rel',resp.next_question_id);

                if(resp.nextccountplus == 11){
                    $('.prep-test-submit-class').hide();
                    $('.mark-for-review').hide();
                    $('.go_to_next_question').hide();
                    $('.clear_exam_section').hide();
                    $('.final_prep_submit').html('Submit Test');
                    $('.go_to_prevoius_question').hide();
                    $('.review-test-first-question').show();
                }

                if(next_question_total_count == 10){
                    $('.prep-test-submit-class').html('Save');
                    $('.final_prep_submit').html('Submit Test');
                    $('.go_to_next_question').hide();
                    $('.review-test-first-question').hide();
                }
                $(".optionclass"+resp.new_question_id+resp.option_value).prop("checked", true);
            }
        })
    });


    $('.mark-for-review').on('click', function () { 
        
        $('.loader_ques').show();
        var total_question_count  = <?php echo $total_question_count; ?>;
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;

        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        var question_id  = $('#get-question_id').val();
        var answerelems_decode = $('#answer_hidden_field').val();
        var answerval = null;
        questionelems = $.grep(questionelems, function(e){ 
            return e != question_id; 
        });
        questionelems.push(question_id);
        answerelems = $.grep(answerelems, function(e){ 
             return e[0] != question_id; 
        });
        answerelems.push([question_id, answerval]);

        console.log(answerelems);
        $.ajax({
            type:"post",
            url: "{{ url('student/mark-for-review-prep-test') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'nextccount':next_question_total_count,'total_question_count':total_question_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id,'answerelems':answerelems_decode},
            success:function(resp){
                    $(".optionclass"+question_id+resp.option_value).prop("checked", true);
                    $('.prep-test-submit-class').html('Save & Next');
                    $('.final_prep_submit').html('End Test');
                    $('.loader_ques').hide();
                    $('.go_to_prevoius_question').show();
                    $('.go_to_next_question').show();
                     $('.clear_exam_section').show();
                    $('.add_next_question').replaceWith(resp.render_view);
                    MathJax.typeset(['.add_next_question']);
                    $('.result-status'+next_question_total_count).removeClass('no_vst');
                    $('.result-status'+next_question_total_count).addClass('mrk_rev');
                    next_question_total_count  = resp.nextccountplus;
                    $('.go_to_prevoius_question').attr('rel',resp.next_question_id);
                    $(".go_to_next_question").attr('rel',resp.next_question_id);
                    if(resp.nextccountplus == 11){
                        $('.prep-test-submit-class').hide();
                        $('.mark-for-review').hide();
                        $('.go_to_next_question').hide();
                        $('.clear_exam_section').hide();
                        $('.final_prep_submit').html('Submit Test');
                        $('.go_to_prevoius_question').hide();
                        $('.review-test-first-question').show();
                    }
                    if(next_question_total_count == 10){
                        $('.prep-test-submit-class').html('Save');
                        $('.final_prep_submit').html('Submit Test');
                        $('.go_to_next_question').hide();
                        $('.review-test-first-question').hide();
                    }
            }
        })
    });
    
    $('.right-side-question').click('click', function () {
        $('.loader_ques').show();
        $('.review-test-first-question').hide();

        var question_id  =  $(this).attr('rel');
        var question_count  = $(this).attr('rel1');
        var answeroption  =  $('#answer-set-for-change'+question_id).val();
        //var finalansweroption  =  JSON.parse(answeroption);
       // console.log(answeroption);
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        $.ajax({
            type:"post",
            url: "{{ url('student/back-to-question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'question_id':question_id,'question_count':question_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id},
            success:function(resp){
                $('.prep-test-submit-class').html('Save & Next');
                $('.final_prep_submit').html('End Test');
                $('.go_to_next_question').show();
                $('.clear_exam_section').show();
                $('.loader_ques').hide();
                 $('.go_to_prevoius_question').show();
                next_question_total_count = question_count;
               // alert(next_question_total_count);
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                $(".optionclass"+question_id+answeroption).prop("checked", true);
                $('.go_to_prevoius_question').attr('rel',resp.question_id);
                $(".go_to_next_question").attr('rel',resp.question_id);
                if(next_question_total_count <= 1 ){
                    $('.go_to_prevoius_question').hide();
                }

                if(next_question_total_count <= 10){
                    $('.prep-test-submit-class').show();
                    $('.mark-for-review').show();
                    $('.go_to_next_question').show();

                }

                if(next_question_total_count == 10){
                    $('.prep-test-submit-class').html('Save');
                    $('.final_prep_submit').html('Submit Test');
                    $('.go_to_next_question').hide();
                }
            }
        })

    });
</script>

<script>
    $('.clear_exam_section').on('click',function() { 
        var question_id  =  $('#get-question_id').val();
        $('input:radio[name=answer'+question_id+']').prop("checked", false );
        $('.result-status'+next_question_total_count).removeClass('yes_ans');
        $('.result-status'+next_question_total_count).removeClass('no_ans');
        $('.result-status'+next_question_total_count).removeClass('mrk_rev');
        $('.result-status'+next_question_total_count).addClass('no_vst');
    });
</script>

<script type="text/javascript">
  
    $('.go_to_prevoius_question').on('click',function(){ 
        $('.loader_ques').show();
        $('.review-test-first-question').hide();
        var question_id  =  $(this).attr('rel');
        var question  =  new Array();
        var answerelems_decode = $('#answer_hidden_field').val();
       // console.log(next_question_total_count);
        question =  <?php  echo json_encode($question); ?>;
        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        $.ajax({
            type:"post",
            url: "{{ url('student/go_to_prevoius_question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'question_id':question_id,'question':question,'answerelems':answerelems_decode,'next_question_total_count':next_question_total_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id},
            success:function(resp){
                console.log(".optionclass"+resp.next_question_id+resp.option_value);
                $('.prep-test-submit-class').html('Save & Next');
                $('.final_prep_submit').html('End Test');
                $('.loader_ques').hide();
                --next_question_total_count;
                $('.go_to_prevoius_question').show();
                $('.clear_exam_section').show();
                $('.go_to_next_question').hide();
                $('.go_to_prevoius_question').attr('rel',resp.next_question_id);
                $(".go_to_next_question").attr('rel',resp.next_question_id);
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                $(".optionclass"+resp.next_question_id+resp.option_value).prop("checked", true);
                $('.go_to_next_question').show();
                if(next_question_total_count <= 1 ){
                    $('.go_to_prevoius_question').hide();
                }

                if(next_question_total_count <= 10){
                    $('.prep-test-submit-class').show();
                    $('.mark-for-review').show();
                    $('.go_to_next_question').show();
                }

                if(next_question_total_count == 10){
                    $('.prep-test-submit-class').html('Save');
                    $('.final_prep_submit').html('Submit Test');
                    $('.go_to_next_question').hide();
                }
            }
        })
    });
</script>


<script type="text/javascript">
    
    $('.go_to_next_question').on('click',function(){ 
        $('.loader_ques').show();
        $('.review-test-first-question').hide();
        var question_ids  = $('#question_hidden_field').val();
        var answer_option = $('#answer_hidden_field').val();
        var question_id  =  $(this).attr('rel');
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var answerelems_decode = $('#answer_hidden_field').val();
      //  console.log(answerelems_decode);
        $.ajax({
            type:"post",
            url: "{{ url('student/go_to_next_question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'question_id':question_id,'question':question,'answerelems':answerelems_decode,'next_question_total_count':next_question_total_count,'subject_name':subject_name,'unit_name':unit_name,'question_ids':question_ids,'answer_option':answer_option,'start_date_time':start_date_time,'subcategory_name':subcategory_name,'first_question_id':first_question_id},
            success:function(resp){
                //console.log(".optionclass"+resp.next_question_id+resp.option_value);
                $('.prep-test-submit-class').html('Save & Next');
                $('.final_prep_submit').html('End Test');
                $('.loader_ques').hide();
                $('.go_to_next_question').show();
                $('.go_to_prevoius_question').show();
                $('.clear_exam_section').show();
                $('.go_to_prevoius_question').attr('rel',resp.next_question_id);
                $(".go_to_next_question").attr('rel',resp.next_question_id);
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                if(next_question_total_count == question.length-1){
                    $('.go_to_next_question').hide();
                }
                $(".optionclass"+resp.next_question_id+resp.option_value).prop("checked", true);
                next_question_total_count++;

                if(next_question_total_count <= 10){
                    $('.prep-test-submit-class').show();
                    $('.mark-for-review').show();
                }

                if(next_question_total_count == 10){
                    $('.final_prep_submit').html('Submit Test');
                    $('.prep-test-submit-class').html('Save');
                    $('.go_to_next_question').hide();
                }
            }
        })
    });
</script>

<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>



@endsection