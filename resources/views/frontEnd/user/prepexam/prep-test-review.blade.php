
@extends('frontEnd.layouts.student_master')
@section('title','Prep Exams')
@section('content')
<style type="text/css">
.correct_answer{
    font-weight: bold;
}

.ans_expl{
    font-weight: bold;
}

.not-answer{color:#e89a36 !important;}

.option-right-color p {color:green;font-weight: bold;}
.option-select-wrong-color p {color:red;}
.option-select-wrong-color::before {background-color: red !important;}
.option-right-color::before {background-color: green !important;}
</style>
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon exam_page_ques_main"> <!-- change class -->
                    <div class="chpr_ttl_tme d-flex justify-content-between">
                        <h5>Prep Test
                                <p>{{ucfirst(@$sub_subcategory)}},<?php echo @$subject_name;?>,<?php echo $unit_name;?></p></h5>
                        <div>
                            <a class="instt_hr" data-toggle="modal" data-target="#inst_modal"><i class="fa fa-info"></i> instructions</a>
                        </div>
                    </div>
                        <div class="wrpr_main_exam d-flex ">
                                <div class="main_left d-flex flex-column justify-content-start">
                                        <div class="question_activity">
                                            <div class="sngl_qust add_next_question">
                                                <?php if(!empty($question)) {
                                                    $quescount  = 1;
                                                    $optioncount = 1;
                                                    foreach($question  as $questionval) {
                                                    if($quescount == 1){  
                                                        $user_exam_data = DB::table('user_exam_questions')->where(['user_exam_id'=>$user_exam_id,'question_id'=>$questionval['id']])->first();


                                                    ?>
                                                    <input type="hidden" name="question" value="<?php echo $questionval['id']; ?>" id="get-question_id">
                                                    <div class="quest_top">
                                                        <h5>Q<?php echo $quescount; ?>.<?php echo $questionval['question']; ?></h5>
                                                    </div>
                                                    <?php 
                                                        $user_exam_question_options = DB::table('question_bank_options')->where(['question_bank_id'=>@$questionval['id'],'position'=>@$questionval['correct_answer']])->first();
                                                    ?>
                                                    <div class="opt-mul">
                                                        <ul>
                                                            <?php 
                                                            if($questionval['question_bank_options']){
                                                               
                                                                foreach($questionval['question_bank_options'] as $optionval){  ?>
                                                                    <li>
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input option-selected optionclass<?php echo $optionval['position']; ?>" name="answer<?php echo $questionval['id']; ?>" id="switch<?php echo $optioncount;?>" value="<?php echo $optionval['position'];?>" <?php if($user_exam_data->given_answer == $optionval['position']) {?> checked="checked" <?php } ?> disabled='disabled'>
                                                                            <label class="custom-control-label <?php if($user_exam_question_options->position == $optionval['position']){ ?> option-right-color <?php }  elseif($user_exam_data->given_answer != $user_exam_question_options->position){ if($user_exam_data->given_answer == $optionval['position']) { ?> option-select-wrong-color <?php } } ?>" for="switch<?php echo $optioncount;?>">
                                                                                <?php if(!empty($optionval['options'])){ echo $optionval['options']; } elseif($optionval['option_image']) { ?>
                                                                                <img src="{{url('public/images/ exam')}}/{{$optionval['option_image']}}">  <?php } ?>
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                            <?php $optioncount++;}
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>

                                                    <?php 
                                                        if(@$user_exam_data->ans_status == 'true'){ ?>
                                                            <p class="right-answer">Your answer is correct</p>
                                                        <?php } elseif(@$user_exam_data->ans_status == 'not_attempt'){ ?>
                                                            <p class="not-answer">Not answered</p>
                                                        <?php } else{ ?>
                                                            <p class="wrong-answer">Your answer is wrong</p>
                                                        <?php } ?>
                                                    <p class="correct_answer"><u>Correct Answer:</u><?php if(!empty( @$user_exam_question_options->options )){ echo @$user_exam_question_options->options; } elseif(@$user_exam_question_options->option_image) { ?>
                                                        <img src="{{url('public/images/exam')}}/{{@$user_exam_question_options->options->option_image}}">  <?php } ?>
                                                    </p>        
        
                                                    <p class="ans_expl"><u>Solution</u></p><?php if(!empty( @$questionval['answer_explanation'] )){ echo  @$questionval['answer_explanation']; } elseif(@$questionval['answer_explanation_image']) { ?>
                                                        <img src="{{url('public/images/exam')}}/{{@$questionval['answer_explanation_image']}}">  <?php } ?></p>

                                                <?php } $quescount++; } } ?>
                                            </div>
                                            
                                        </div>

                                        <div class="footr_exam">
                                            <div class="d-flex justify-content-between">
                                                <div class=" text-left">
                                                    <button class="btnus btn_outline_prev go_to_prevoius_question"><i class="fa fa-long-arrow-alt-left"></i> Prev</button>
                                                    <button class="btnus btn_outline_nex go_to_next_question">Next <i class="fa fa-long-arrow-alt-right"></i></button>
                                                </div>
                                                <form action="{{url('student/prep-exam')}}" method="post">
                                                    <button type="submit" class="btnus btn_end_exm ">Go To Dashboard</button>
                                                    @csrf
                                                </form>
                                            </div>
                                       </div>
                                </div>
                                <div class="main_right">
                                        <div class="nmbr_info">
                                            <ul class="ul_numb" type="none">
                                                <li><span class="comn_reco no_ans">1</span> Not Answered</li>
                                                <li><span class="comn_reco right_ans">2</span>Correct</li>
                                                <li><span class="comn_reco wrong-ans">3</span>Incorrect</li>
                                            </ul>
                                        </div>
                                        <div class="question_serl">
                                            <ul class="ul_numb" type="none">
                                                <?php 
                                                    if(!empty($question)){
                                                        $question_count  =  1;
                                                        foreach($question as $questionval){ 
                                                             $user_exam_data = DB::table('user_exam_questions')->where(['user_exam_id'=>$user_exam_id,'question_id'=>$questionval['id']])->first();
                                                         ?>
                                                            <li class="right-question-li"><a href="javascript:void(0)" class="right-side-question" rel="<?php echo $questionval['id']; ?>"><span class="comn_reco <?php  if(@$user_exam_data->ans_status == 'true'){ echo "right_ans";} elseif(@$user_exam_data->ans_status == 'not_attempt'){ echo "no_ans"; } else { echo "wrong-ans";}?>"><?php echo $question_count;?></a></span></li>
                                                        <?php $question_count++; }
                                                    }
                                                ?>
                                            </ul>
                                        </div>
                                </div>
                        
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>

<div class="loader_ques">
    <img src="https://mir-s3-cdn-cf.behance.net/project_modules/disp/35771931234507.564a1d2403b3a.gif" class="img-fluid">
</div>
<div class="modal" id="inst_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div class="app_wrap_comon prep_exam_inst_sec"> <!-- change class -->
                    <div class="preInstr">
                        <div class="main_inst text-center">
                            <div class="inr_int">
                                <h3 class="text-uppercase">Instructions</h3>
                                <h5>Please read instructions carefully</h5>
                            </div>
                            <ol>
                                <li>Total duration for exam is 20min.</li>
                                <li>CLock wil be on server. When clock reach zero, exam will end itself and get submitted.</li>
                                <li>Below are the color and symbol that will help you in the exam to understand.</li>
                                <li>No time limit to attemp qusetuion in assignment.</li>
                                <li>Type of question will be mentioned along with the questions.</li>
                                <li>One question can be attempted only once.</li>
                            </ol>
                           <!--  <div class="btn_start">
                                <a href="examQues.php" class="btn btn_gradient btn_active"> Start Test</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
$( document ).ready(function() {
    $('.loader_ques').hide();
    $('.go_to_prevoius_question').hide();
})
</script>

<script>
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
</script>

<script type="text/javascript">
    var user_exam_id  = "<?php echo $user_exam_id;?>";
    $('.go_to_next_question').on('click',function(){ 
        $('.loader_ques').show();
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var question_id  = $('#get-question_id').val();
         $.ajax({
            type:"post",
            url: "{{ url('student/go_to_next_prep_review_question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'user_exam_id':user_exam_id,'question_id':question_id},
            success:function(resp){
                $('.loader_ques').hide();
                $('.go_to_next_question').show();
                $('.go_to_prevoius_question').show();
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                if(resp.index == 9){
                    $('.go_to_next_question').hide();   
                }
            }
        })
    });

    $('.go_to_prevoius_question').on('click',function(){ 
        $('.loader_ques').show();
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var question_id  = $('#get-question_id').val();
         $.ajax({
            type:"post",
            url: "{{ url('student/go_to_prep_review_question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'user_exam_id':user_exam_id,'question_id':question_id},
            success:function(resp){
                $('.loader_ques').hide();
                $('.go_to_next_question').show();
                $('.go_to_prevoius_question').show();
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                if(resp.index == 0){
                    $('.go_to_prevoius_question').hide();   
                }
            }
        })
    });

     $('.right-side-question').click('click', function () { 
        $('.loader_ques').show();
        var question  =  new Array();
        question =  <?php  echo json_encode($question); ?>;
        var question_id  =  $(this).attr('rel');
         $.ajax({
            type:"post",
            url: "{{ url('student/go_to_rigth_side_review_question') }}",
            data:{'question':question,'_token': "{{ csrf_token() }}",'user_exam_id':user_exam_id,'question_id':question_id},
            success:function(resp){
                $('.loader_ques').hide();
                $('.go_to_next_question').show();
                $('.go_to_prevoius_question').show();
                $('.add_next_question').replaceWith(resp.render_view);
                MathJax.typeset(['.add_next_question']);
                if(resp.index == 0){
                    $('.go_to_prevoius_question').hide();   
                }
                if(resp.index == 9){
                    $('.go_to_next_question').hide();   
                }
            }
        })
    });
</script>



@endsection