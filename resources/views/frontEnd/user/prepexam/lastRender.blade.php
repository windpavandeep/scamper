<style type="text/css">
	.welcome-message-last {
		padding: 200px 268px;
		text-align: center;
	}
	.thnx_title {
		font-weight: bold;
		font-size: 42px;
		color: #1ce31c;
	}
</style>
<div class="sngl_qust add_next_question">
	<p class="welcome-message-last">
		<span class="thnx_title">Thank You! </span> <br>
        For giving this PrepTest, their are no more questions left you can now summit the test or go back and review your previous questions.
    </p>
</div>
