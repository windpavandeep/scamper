@extends('frontEnd.layouts.student_master')
@section('title','Prep Exams')
@section('content')
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon prep_exam_inst_sec"> <!-- change class -->
                    <div class="preInstr">
                        <div class="inst_hed_cont text-center text-uppercase">
                            <h3 class="inst_hea">Prep Test</h3>
                            <h4 class="inst_hea_sub">{{ucfirst($sub_subcategory->name)}},<?php echo $subject['name']; ?>, <?php echo $unit['name'];?></h4>
                        </div>
                        <div class="main_inst text-center">
                            <div class="inr_int">
                                <h3 class="text-uppercase">Instructions</h3>
                                <h5>Please read instructions carefully</h5>
                            </div>
                            <ol>
                                <li>Total duration for exam is <?php echo $totaltime;?> min.</li>
                                <li>CLock wil be on server. When clock reach zero, exam will end itself and get submitted.</li>
                                <li>Below are the color and symbol that will help you in the exam to understand.</li>
                                <li>No time limit to attemp qusetuion in assignment.</li>
                                <li>Type of question will be mentioned along with the questions.</li>
                                <li>One question can be attempted only once.</li>
                            </ol>
                            <div class="btn_start">
                                <a  href="{{url('student/exam-ques')}}/{{base64_encode($post_data['subject_id'])}}/{{base64_encode($post_data['unit_id'])}}/{{base64_encode($totaltime)}}/{{base64_encode($sub_subcategory_id)}}" class="btn btn_gradient btn_active"> Start Test</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    // function goToTest(){
    //     var url = "{{url('student/exam-ques')}}/{{base64_encode($post_data['subject_id'])}}/{{base64_encode($post_data['unit_id'])}}/{{base64_encode($totaltime)}}";
    //     window.open(url,'window','toolbar=no, menubar=no, resizable=yes'); 
    // }
</script>
@endsection