@extends('frontEnd.layouts.login')
@section('title','Student Signup')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
        	
        	<!-- register -->
        	<section class="register_sec user_regst">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-8 offset-md-2 col-sm-12">
        					<div class="wrp_white">
        						<div class="sec_heading text-center">
		            				<h2>Signup as a Student</h2>
		            				<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
		            			</div>
		            			<div class="form_head text-center">
		            				<!-- <h5>Enter the OTP sent on the registered mobile number</h5> -->
                                    <form method="post" action="" id="user_signup">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Last name" name="last_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Enter Email" name="email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control dtpckr" value="" placeholder="Date of Birth" name="dob" readonly="">
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Create Password" name="password" id="password">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="form-group">
                                                    <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="gender">
                                                    <option data-display="Select Gender" value="">Select Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="country_id" id="country_id">
                                                    <option data-display="Select Country" value="">Select Country</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country['id'] }}">{{ $country['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="state_id" id="state_id">
                                                    <option data-display="Select State" value="">Select State</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="city_id" id="city_id">
                                                    <option data-display="Select City" value="">Select City</option>
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="pincode" class="niceselc form-control" placeholder="Pincode">
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <input type="text" class="niceselc form-control" value="" name="district" placeholder="District">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <input type="text" class="niceselc form-control" value="" name="address" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="language_id">
                                                    <option data-display="Choose Category" value="" readonly>Choose language</option>
                                                    @foreach(@$languages as $language)
                                                        <option value="{{ $language['id'] }}">{{ucfirst($language['name'])}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="category_id" id="category_id">
                                                    <option data-display="Preparing for Exam" value="">Select Category</option>
                                                    @foreach($domains as $domain)
                                                        <option value="{{ $domain['id'] }}">{{ ucfirst($domain['name']) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="nice_selc">
                                                <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                                    <option data-display="Preparing for Exam" value="">Select Sub Category</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                           
                                                <label class="custom_label text-left">
                                                    <a href="{{url('/term-conditions')}}" target="blank"> I agree to the Privacy & Terms
                                                    </a>
                                                    <input type="checkbox" checked="checked" name="terms">
                                                    <span class="checkmark"></span>
                                                </label>
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                @csrf
                                                <button type="submit" class="btn btn_gradient btn_active">Submit</button>
                                            </div>
                                        </div>
                                    </form>
		            			</div>
                                <p class="text-center">Already have an account? <a href="{{url('/login')}}">Click here</a></p>
        					</div>
        				</div>
        			</div>
        		</div>
        	</section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#country_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/states') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#state_id').html(resp);
                $('#city_id').html('<option value="">Select City </option>');
                $("#state_id").selectpicker("refresh");
                $("#city_id").selectpicker("refresh");
            }
        })
    });

    $('#state_id').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/cities') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#city_id').html(resp);
                $("#city_id").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });
</script>
<script type="text/javascript">
    $('#user_signup').validate({
        rules:{
            first_name:{
                required:true,
                regex:/^[a-zA-z ]+$/
            },
            last_name:{
                required:true,
                regex:/^[a-zA-z ]+$/
            },
            email:{
                required:true,
                email:true,
                remote:"{{ url('/validate/email') }}"
            },
            dob:{
                required:true
            },
            address:{
                required:true,
                minlength:2,
                maxlength:500
            },
            district:{
                required:true,
                minlength:2,
                maxlength:150
            },
            pincode:{
                required:true,
                digits:true,
                minlength:5,
                maxlength:9
            },
            password:{
                required:true,
                regex:/^[a-zA-z 0-9 -)*&^$#@!(,.]+$/
            },
            confirm_password:{
                required:true,
                equalTo:"#password"
            },
            gender:{
                required:true
            },
            country_id:{
                required:true
            },
            state_id:{
                required:true
            },
            city_id:{
                required:true
            },
            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
            terms:{
                required:true
            },
            language_id:{
                required:true,    
            }
        },
        messages:{
            email:{
                remote:"This email-id is already registered"
            },
            password:{
                regex:"Password can only consist of alphabets, numbers and special characters"
            },
            confirm_password:{
                equalTo:"Please enter the same password again"
            }
        },
        errorPlacement:function(error,element){
            if(element.attr("name") == "terms") {
                error.appendTo(element.parent().parent().after());
            } else{
                error.appendTo(element.parent().after());
            }
        },
    })
</script>
<script type="text/javascript">
    $('#category_id').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
        // alert(clickedIndex);
        if(clickedIndex==''||clickedIndex==0){
            clickedIndex = 0;
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                $('.loader').hide();
            }
        })
    });
</script>
@endsection