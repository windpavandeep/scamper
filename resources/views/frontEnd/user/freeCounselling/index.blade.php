@extends('frontEnd.layouts.student_master')
@section('title','Exam')
@section('content')
<div class="page-wrapper">
    <div class="app_dash_wraper">
        <section class="sec_dashboard db_main">
            <div class="page_container">
                <div class="app_wrap_comon app_home_sec"> <!-- change class -->
                    <div class="page_divider"> 
                        <div class="side_wid">
                            <div class="sidebar_chd">
                                <!--  -->
                                @include('frontEnd.user.common.student_dashboard_sidebar')
                                <!--  -->
                            </div>
                        </div>
                        <div class="main_wid">
                            <!-- header index -->
                             @include('frontEnd.common.student_dashboard_header')
                            <!-- header index -->
                            <div class="mainside_wrap">
                                
                                @include('frontEnd.common.student_dashboard_sub_header')
                                            
                                <div class="card_shd">
                                    <ul class="nav nav-tabs pos_rel" id="myTab" role="tablist">

                                       <!--  <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Math</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Science</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Physics</a>
                                        </li> -->
                                        <div class="wrpa_hedr_cont">
                                            <ul class="d-flex align-items-center" type="none">
                                               <!--  <li>Helpline: 96448315498<br> <small>Mon-Fri : 7:00 AM 6:00 PM</small></li> -->
                                                <li class="centrd"><a class="text-primary" href="javascript:;"><i class="fa fa-video"></i> Getting Started</li>
                                                <li><a class="text-primary" href="javascript:;"><i class="fa fa-gift"></i> Refer a Friend</a></li>
                                            </ul>
                                        </div>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <!-- content goes here -->
                                            <div class="no_cnt text-center">
                                                <h2> Coming Soon</h2>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <!--  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
    </div>
</div>

<!-- modal video -->
<div class="modal" id="modal_vid">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div id="vidBox">
                <video id="demo" loop="" controls="" width="100%" height="100%">
                  <source src="http://www.scamperskills.com/public/images/homeContent/5db20b6bb47b9.mp4" type="video/mp4">
                </video>
            </div>
          </div>
        </div>
   </div>
</div>
<!-- modal video -->

@endsection

@section('scripts')
<script>
    $('.subject-change').on('change', function () { //alert('ge');
        var clickedIndex  = $(this).val();
        //alert(clickedIndex);
        //$('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/unit') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#unit_id').html(resp);
                //$("#unit_id").selectpicker("refresh");
            }
        })
    });
</script>
<script type="text/javascript">
    $('#prep-su-subject').validate({
        rules:{
            subject_id:{
                required:true
            },
            unit_id:{
                required:true
            },
           
        },
        messages:{
            subject_id:{
                remote:"Please first select subject"
            },
            unit_id:{
                regex:"Please first select unit"
            }
        },
        errorPlacement:function(error,element){
            if(element.attr("name") == "terms") {
                error.appendTo(element.parent().parent().after());
            } else{
                error.appendTo(element.parent().after());
            }
        },
    })
</script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-pie-chart/2.1.6/jquery.easypiechart.min.js"></script> <!-- circular chart -->
<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>

<script>
    am4core.ready(function() {

    am4core.useTheme(am4themes_animated);

    var chart = am4core.create('chrt_grph', am4charts.XYChart)
    chart.colors.step = 5;

    chart.legend = new am4charts.Legend()
    chart.legend.position = 'right'
    chart.legend.paddingBottom = 20
    chart.legend.labels.template.maxWidth = undefined

    var xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
    xAxis.dataFields.category = 'category'
    xAxis.renderer.cellStartLocation = 0.1
    xAxis.renderer.cellEndLocation = 0.9
    xAxis.renderer.grid.template.location = 0;

    var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.min = 0;

    function createSeries(value, name) {
        var series = chart.series.push(new am4charts.ColumnSeries())
        series.dataFields.valueY = value
        series.dataFields.categoryX = 'category'
        series.name = name

        series.events.on("hidden", arrangeColumns);
        series.events.on("shown", arrangeColumns);

        var bullet = series.bullets.push(new am4charts.LabelBullet())
        bullet.interactionsEnabled = false
        bullet.dy = 3;
        bullet.label.text = '{valueY}'
        bullet.label.fill = am4core.color('#ffffff')

        return series;
    }

    chart.data = [
        {
            category: 'Jan 2019',
            first: 40,
            second: 55,
            third: 60
        },
        {
            category: 'Feb 2019',
            first: 30,
            second: 78,
            third: 69
        },
        {
            category: 'Mar 2019',
            first: 27,
            second: 40,
            third: 45
        },
        {
            category: 'Apr 2019',
            first: 50,
            second: 33,
            third: 22
        }
    ]
    createSeries('first', 'Total Attempt');
    createSeries('second', 'Avg Score');
    createSeries('third', 'Remarks');

    function arrangeColumns() {

        var series = chart.series.getIndex(0);

        var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
        if (series.dataItems.length > 1) {
            var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
            var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
            var delta = ((x1 - x0) / chart.series.length) * w;
            if (am4core.isNumber(delta)) {
                var middle = chart.series.length / 2;

                var newIndex = 0;
                chart.series.each(function(series) {
                    if (!series.isHidden && !series.isHiding) {
                        series.dummyData = newIndex;
                        newIndex++;
                    }
                    else {
                        series.dummyData = chart.series.indexOf(series);
                    }
                })
                var visibleCount = newIndex;
                var newMiddle = visibleCount / 2;

                chart.series.each(function(series) {
                    var trueIndex = chart.series.indexOf(series);
                    var newIndex = series.dummyData;

                    var dx = (newIndex - trueIndex + middle - newMiddle) * delta

                    series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                    series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                })
            }
        }
    }
    }); // end am4core.ready()
</script>

<script>
    window.onload = function () {

    //Better to construct options first and then pass it as a parameter
    var options = {
        animationEnabled: true,
        title: {
            // text: "Mobile Phones Used For",                
            // fontColor: "black"
        },  
        axisY: {
            tickThickness: 0,
            lineThickness: 0,
            valueFormatString: " ",
            gridThickness: 0                    
        },
        axisX: {
            tickThickness: 0,
            lineThickness: 0,
            labelFontSize: 18,
            labelFontColor: "black"             
        },
        data: [{
            indexLabelFontSize: 15,
            // toolTipContent: "<span style=\"color:#62C9C3\">{indexLabel}:</span> <span style=\"color:#CD853F\"><strong>{y}</strong></span>",
            indexLabelPlacement: "inside",
            indexLabelFontColor: "white",
            indexLabelTextAlign: "right",
            indexLabelFontWeight: 400,
            color: "#62C9C3",
            type: "bar",
            dataPoints: [
                { y: 29, label: "Poor", indexLabel: "43%" },
                { y: 32, label: "Good", indexLabel: "67%" },
                { y: 64, label: "Very Good", indexLabel: "78%" },
                { y: 73, label: "Excellent", indexLabel: "95%" }
            ]
        }]
    };

    $("#chrt_scor").CanvasJSChart(options);
    }
</script>

<!-- script files -->
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#slider_catg").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
        });
        $("#slider_notes").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 2,
        });
        $(function() {
          $('.chartb').easyPieChart({
            size: 160,
            barColor: "blue",
            scaleLength: 0,
            lineWidth: 10,
            // trackColor: "#373737",
            lineCap: "circle",
            animate: 2000,
          });
          $('.chartr').easyPieChart({
            size: 160,
            barColor: "red",
            scaleLength: 0,
            lineWidth: 10,
            // trackColor: "#373737",
            lineCap: "circle",
            animate: 2000,
          });
          $('.chartg').easyPieChart({
            size: 160,
            barColor: "green",
            scaleLength: 0,
            lineWidth: 10,
            // trackColor: "#373737",
            lineCap: "circle",
            animate: 2000,
          });
          $('.charto').easyPieChart({
            size: 160,
            barColor: "orange",
            scaleLength: 0,
            lineWidth: 10,
            // trackColor: "#373737",
            lineCap: "circle",
            animate: 2000,
          });
        });
    });
</script>
@endsection