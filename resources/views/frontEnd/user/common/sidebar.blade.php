<div class="pic_top pos_rel text-center">
    <?php
        $image = DefaultUserPath;
        $usr_img = Auth::User()->image;
        if(!empty($usr_img)){
            $image = StudentProfileImgPath.'/'.$usr_img;
        }

        $name = '';
        if(Auth::check()){
            $name = ucfirst(Auth::User()->first_name).' '.Auth::User()->last_name;
        }
    ?>
    <div class="pos_rel pic_pos">
        <img src="{{$image}}" class="img-fluid" alt="">
    </div>
    <h4 class="name_db">{{$name}}</h4>
</div>
<div class="side_menu">
    <ul type="none" class="ul_side">
        <li class="<?php if( ($page == 'profile')||($page == 'edit_profile')||($page == 'change_password') ) { echo 'active'; } ?>"><a href="{{ url('/student/profile') }}"><i class="fas fa-user-tie"></i> Profile Management</a></li>
        <!-- <li class="<?php if($page == 'subscription_history'){ echo 'active'; }?>"><a href="{{ url('/student/subscription-history') }}"><i class="far fa-clipboard"></i> Subscriptions History </a></li> -->
         <li class="<?php if($page == 'purchased_contents'){ echo 'active'; }?>"><a href="{{url('/student/purchased-contents')}}"><i class="far fa-square"></i> Contents</a></li>
        <li><a href="{{url('/logout')}}"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
    </ul>
</div>