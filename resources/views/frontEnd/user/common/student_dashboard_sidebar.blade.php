<style type="text/css">
    .ul_side li:hover{
        background: gray;
    }

    .ul_side li a:hover{
        color: #fff;
    }
</style>

<?php
   //echo $page; die; 
?>



<div class="logo_side">
    <a class="navbar-brand logo_ch" href="{{ url('/') }}"><img src="{{ asset(systemImgPath.'/logo.png') }}" class="img-fluid" alt="Logo" /></a>
</div>
<div class="side_menu">
    <div class="pic_top pos_rel text-center">
        <div class="pos_rel pic_pos">
            <?php
                $image = DefaultUserPath;
                $usr_img = @Auth::User()->image;
                if(!empty($usr_img)){
                    $image = StudentProfileImgPath.'/'.$usr_img;
                }
                $name = '';
                if(Auth::check()){
                    $name = ucfirst(Auth::User()->first_name).' '.Auth::User()->last_name;
                }
            ?>
            <img src="{{ $image }}" class="img-fluid" alt="">
        </div>
        <h4 class="name_db">{{$name}}</h4>
    </div>
    <ul type="none" class="ul_side">
        <li <?php if($page == 'Dashboard') { ?> class="active" <?php } ?>><a href="{{url('/student/dashboard')}}"><i class="far fa-chart-bar"></i> <span class="side_span">Dashboard</span></a></li>
        <li <?php if($page == 'LiveLectures') { ?> class="active" <?php } ?> ><a href="{{url('/student/live_lectures')}}"><i class="fas fa-chalkboard-teacher"></i> <span class="side_span">Live Classes</span></a></li>
        <li <?php if($page == 'Video Lectures') { ?> class="active" <?php } ?> ><a href="{{url('/student/video_lectures')}}"><i class="far fa-file-video"></i> <span class="side_span">Video Lectures</span></a></li>
        <li <?php if($page == 'Study Materials') { ?> class="active" <?php } ?> ><a href="{{url('/student/study_materials')}}"><i class="far fa-file"></i> <span class="side_span">Study Materials</span></a></li>
        <li <?php if($page == 'Prep Exams') { ?> class="active" <?php } ?> ><a href="{{url('/student/prep-exam')}}"><i class="far fa-file-alt"></i> <span class="side_span">Prep Test</span></a></li>
        <li <?php if($page == 'Exams') { ?> class="active" <?php } ?>><a href="{{url('/student/exam_progress_report')}}"><i class="far fa-file-excel"></i> <span class="side_span">Full Exams</span></a></li>
        <li <?php if($page == 'Chat With Teacher') { ?> class="active" <?php } ?>><a href="{{url('student/chat_with_teacher')}}"><i class="far fa-comment"></i> <span class="side_span">Chat with Teacher</span></a></li>
        <li <?php if($page == 'Expert Opinions') { ?> class="active" <?php } ?>><a href="{{url('student/expert_opinions')}}"><i class="far fa-user-circle"></i> <span class="side_span">Expert Opinions</span></a></li>
        <li <?php if($page == 'Grievance Cell') { ?> class="active" <?php } ?>><a href="{{url('student/grievance_cell')}}"><i class="far fa-edit"></i> <span class="side_span">Grievance Cell</span></a></li>
        <li <?php if($page == 'My Subscriptions') { ?> class="active" <?php } ?>><a href="{{url('student/my_subscriptions')}}"><i class="far fa-address-book" ></i> <span class="side_span">My Subscriptions</span></a></li>
        <li <?php if($page == 'Notification') { ?> class="active" <?php } ?> ><a href="{{url('student/notification')}}"><i class="far fa-bell"></i> <span class="side_span">Notifications</span></a></li>
        <li <?php if($page == 'Student Feedback') { ?> class="active" <?php } ?> ><a href="{{url('student/submit_feedback')}}"><i class="far fa-comments"></i> <span class="side_span">Student Feedback</span></a></li>
        <li <?php if($page == 'Term Condition') { ?> class="active" <?php } ?>><a href="{{url('student/term_condition')}}"><i class="far fa-file-pdf"></i> <span class="side_span">T & C</span></a></li>
        <li <?php if($page == 'Privacy Policy') { ?> class="active" <?php } ?>><a href="{{url('student/privacy_policy')}}"><i class="   fas fa-lock"></i> <span class="side_span">Privacy Policy </span></a></li>
        <li><a href="{{url('logout')}}"><i class="fas fa-sign-out-alt"></i> <span class="side_span">Logout</span></a></li>
    </ul>
</div>
