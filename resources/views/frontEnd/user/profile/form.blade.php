@extends('frontEnd.layouts.master')
@section('title','Edit Profile')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec edit_prof_page user_edt_page"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.user.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Edit Profile</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{url('/student/change-password')}}" class="btn btn_gradient btn_active">Change Password</a>
                                            </div>
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-sm-10 offset-1">
                                                        <form class="" method="post" action="" id="student_profile" enctype="multipart/form-data">
                                                            <div class="text-center img_user">
                                                                <div class="pos_rel pic_top"> 
                                                                    <span class="img_edtt pos_rel">  
                                                                        <?php
                                                                            $image = DefaultUserPath;
                                                                            $student_img = $student_details->image;
                                                                            if(!empty($student_img)){
                                                                                $image = StudentProfileImgPath.'/'.$student_img;
                                                                            }
                                                                        ?>
                                                                        <img src="{{ $image }}" class="img-fluid" id="prof_ch">
                                                                        <span class="edt_inpt">
                                                                            <i class="far fa-edit"></i>
                                                                            <input type="file" name="image" class="file_img">
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>First Name</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst(@$student_details->first_name) }}" name="first_name">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Last Name</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst(@$student_details->last_name) }}" name="last_name">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Date of Birth</label>
                                                                <input type="text" class="form-control dtpckr" value="{{ date('d-m-Y',strtotime(@$student_details->dob)) }}" autocomplete="off" name="dob">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="email" class="form-control" value="{{ @$student_details->email }}" disabled="" name="email">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Gender</label>
                                                                    <select class="niceselc form-control" name="gender">
                                                                        <option data-display="Select Gender" readonly value="">Select Gender</option>
                                                                        <option value="male" <?php if($student_details->gender=='male'){echo'selected';}?>>Male</option>
                                                                        <option value="female" <?php if($student_details->gender=='female'){echo'selected';}?>>Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Country</label>
                                                                    <select class="niceselc form-control" name="country_id" id="country_id">
                                                                       <option data-display="Select Country" value="">Select Country</option>
                                                                        @foreach($countries as $country)
                                                                            <option value="{{ $country['id'] }}" <?php if($student_details->country_id==$country['id']){echo 'selected';}?>>{{ ucfirst($country['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>State</label>
                                                                    <select class="niceselc form-control" name="state_id" id="state_id">
                                                                        <option data-display="Select State" value="">Select State</option>
                                                                        @foreach($states as $key=>$value)
                                                                            <option value="{{ $value['id'] }}" <?php if($student_details->state_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>City</label>
                                                                    <select class="niceselc form-control" name="city_id" id="city_id">
                                                                        <option data-display="Select City"  value="">Select City</option>
                                                                        @foreach($cities as $key=>$value)
                                                                            <option value="{{ $value['id'] }}"  <?php if($student_details->city_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Pincode</label>
                                                                    <input type="text" name="pincode" placeholder="Pincode" value="{{isset($student_details->pincode)?$student_details->pincode:''}}" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>District</label>
                                                                    <input type="text" class="form-control" value="{{isset($student_details->district)?$student_details->district:''}}" name="district">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" value="{{isset($student_details->address)?$student_details->address:''}}" name="address">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Category</label>
                                                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                                                        <option data-display="Select Category" value="">Select Category</option>
                                                                        @foreach($domains as $domain)
                                                                            <option value="{{ $domain['id'] }}" <?php if(@$student_details['user_courses']['category_id']==$domain['id']){echo 'selected';}?>>{{ ucfirst($domain['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Sub Category</label>
                                                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                                                        <option data-display="Select Sub Category" value="">Select Sub Category</option>
                                                                        @if(!empty($sub_categories))
                                                                            @foreach($sub_categories as $sub_category)
                                                                                <option value="{{ $sub_category['id'] }}" <?php if(@$student_details['user_courses']['sub_category_id']==$sub_category['id']){echo 'selected';}?>>{{ ucfirst($sub_category['name']) }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                @csrf
                                                                <div class="text-right log_btn">
                                                                    <button type="submit" class="btn btn_gradient btn_active">Update Information</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('.file_img').on('change', function () {
          var input = this;
            var reader = new FileReader();
            reader.onload = function(){
              var dataURL = reader.result;
             var output = document.getElementById('prof_ch');
              output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
        });
    </script>
    <script type="text/javascript">
        $('#country_id').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/states') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#state_id').html(resp);
                    $('#city_id').html('<option value="">Select City </option>');
                    $("#state_id").selectpicker("refresh");
                    $("#city_id").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });

        $('#state_id').on('changed.bs.select',function(e, clickedIndex, isSelected, previousValue){
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/cities') }}"+"/"+clickedIndex,
                success:function(resp){
                    $('#city_id').html(resp);
                    $("#city_id").selectpicker("refresh");
                    $('.loader').hide();
                }
            })
        });
    </script>
    <script type="text/javascript">
        $('#student_profile').validate({
            rules:{
                first_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                last_name:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z ]+$/
                },
                address:{
                    required:true,
                    minlength:2,
                    maxlength:500
                },
                district:{
                    required:true,
                    minlength:2,
                    maxlength:150
                },
                category_id:{
                   required:true,
                },
                sub_category_id:{
                   required:true,
                },
                dob:{
                    required:true
                },
                gender:{
                    required:true
                },
                country_id:{
                    required:true
                },
                state_id:{
                    required:true
                },
                city_id:{
                    required:true
                },
                /*exam_preperation_id:{
                    required:true
                },*/
                image:{
                    accept: "jpg|jpeg|png"
                },
            },
            messages:{
                image:{
                    accept:'Please select an image of .jpeg, .jpg, .png file format.',
                },
            },
            errorPlacement:function(error,element){    
                error.appendTo(element.parent().after());
            },
        })
    </script>
    <script type="text/javascript">
        $('.state_div').on('click', function(){
            // alert('io');
            $("#state_id").selectpicker("refresh");
        })
    </script>
    <script type="text/javascript">
        $('#category_id').on("change",function(){
            var category_id = $(this).val();
            if(category_id==''){
                category_id = 0;
            }
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/sub-categories') }}"+"/"+category_id,
                success:function(resp){
                    $('#sub_category').html(resp.sub_category);
                    $('.loader').hide();
                }
            })
        });
    </script>
@endsection