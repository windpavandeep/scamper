@extends('frontEnd.layouts.master')
@section('title','Profile')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec view_prof_page view_usr"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    <!--  -->
                                     @include('frontEnd.user.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>My Profile</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{url('/student/profile/edit')}}" class="btn btn_gradient btn_active">Edit Profile</a>
                                            </div>
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-sm-10 offset-1">
                                                        <form class="" method="" action="" id="">
                                                            <div class="text-center img_user">
                                                                <div class="pos_rel pic_top">   
                                                                    <?php
                                                                        $image = DefaultUserPath;
                                                                        if(!empty(@$student_details->image)){
                                                                            $image = StudentProfileImgPath.'/'.$student_details->image;
                                                                        }
                                                                    ?>
                                                                    <img src="{{$image}}" class="img-fluid" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>First Name</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst(@$student_details->first_name) }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-6 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label>Last Name</label>
                                                                        <input type="text" class="form-control" value="{{ ucfirst(@$student_details->last_name) }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Date of Birth</label>
                                                                <input type="text" class="form-control dtpckr" value="{{date('d-m-Y',strtotime(@$student_details->dob))}}" autocomplete="off">
                                                                
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="email" class="form-control" value="{{ @$student_details->email }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Gender</label>
                                                                    <select class="niceselc form-control">
                                                                        <option data-display="Select Gender" readonly>Male</option>
                                                                        <option value="1" <?php if(@$student_details->gender=='male'){echo'selected';}?>>Male</option>
                                                                        <option value="2" <?php if(@$student_details->gender=='femail'){echo'selected';}?>>Female</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Country</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($student_details->country_name))
                                                                            <option value="selected">{{ucfirst(@$student_details->country_name) }}</option>
                                                                        @else
                                                                            <option value="">Select Country</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>State</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($student_details->state_name))
                                                                            <option value="selected">{{$student_details->state_name}}</option>
                                                                        @else
                                                                            <option value="">Select State</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>City</label>
                                                                    <select class="niceselc form-control">
                                                                        @if(!empty($student_details->city_name))
                                                                            <option value="selected">{{ ucfirst($student_details->city_name) }}</option>
                                                                        @else
                                                                            <option value="">Select City</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                    
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Pincode</label>
                                                                    <input type="text" name="pincode" placeholder="Pincode" value="{{isset($student_details->pincode)?$student_details->pincode:''}}" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>District</label>
                                                                    <input type="text" class="form-control" value="{{isset($student_details->district)?$student_details->district:''}}" name="district">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Address</label>
                                                                    <input type="text" class="form-control" value="{{isset($student_details->address)?$student_details->address:''}}" name="address">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Category</label>
                                                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                                                        <option data-display="Select Category" value="">Select Category</option>
                                                                        @foreach($domains as $domain)
                                                                            <option value="{{ $domain['id'] }}" <?php if(@$student_details['user_courses']['category_id']==$domain['id']){echo 'selected';}?>>{{ ucfirst($domain['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="nice_selc">
                                                                    <label>Sub Category</label>
                                                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                                                        <option data-display="Select Sub Category" value="">Select Sub Category</option>
                                                                        @if(!empty($sub_categories))
                                                                            @foreach($sub_categories as $sub_category)
                                                                                <option value="{{ $sub_category['id'] }}" <?php if(@$student_details['user_courses']['sub_category_id']==$sub_category['id']){echo 'selected';}?>>{{ ucfirst($sub_category['name']) }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group">
                                                                <div class="text-right log_btn">
                                                                    <a href="" class="btn btn_gradient btn_active">Change Password</a>
                                                                </div>
                                                            </div> -->
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection