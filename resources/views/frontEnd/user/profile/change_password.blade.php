@extends('frontEnd.layouts.master')
@section('title','Change Password')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.dashboard_header')
        <div class="home_page_wrapper inner_page trnr_dash_div">

            <section class="sec_dashboard db_main">
                <div class="container-fluid">
                    <div class="wrap_dash_sec change_pswd"> <!-- change class -->
                        <div class="row"> 
                            <div class="col-sm-3 col-md-4">
                                <div class="sidebar_wrap card_shd">
                                    @include('frontEnd.user.common.sidebar')
                                </div>
                            </div>
                            <div class="col-sm-9 col-md-8">
                                <div class="mainside_wrap">
                                    <!--  -->
                                    <div class="page_head">
                                        <h4>Change Password</h4>
                                        <!-- <h6>Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h6> -->
                                    </div>
                                    <div class="main_cntnt_dash">
                                        <div class="card_shd cnt_skamper">
                                            <div class="text-right log_btn">
                                                <a href="{{ url('/student/profile/edit') }}" class="btn btn_gradient btn_active">Edit Profile</a>
                                            </div>
                                            <!--  -->
                                            <div class="cont_shd_frm">
                                                <div class="row">
                                                    <div class="col-sm-10 offset-1">
                                                        <div class="text-center img_user">
                                                            <div class="pos_rel pswd_img">
                                                                <img src="{{ asset(systemImgPath.'/pswd_ch.png') }}" class="img-fluid">
                                                            </div>
                                                        </div>
                                                        <form class="" method="post" action="" id="change_pw">
                                                            <div class="form-group">
                                                                <label>Current Password</label>
                                                                <input type="password" class="form-control" placeholder="Current Password" name="current">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>New Password</label>
                                                                <input type="password" class="form-control" placeholder="New Password" name="new" id="new_pw">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Confirm New Password</label>
                                                                <input type="password" class="form-control" placeholder="Confirm New Password" name="confirm">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="text-right log_btn">
                                                                    @csrf
                                                                    <button class="btn btn_gradient btn_active">Update Password</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--  -->
                                        </div>
                                    </div>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#change_pw').validate({
            rules:{
                current:{
                    required:true
                },
                new:{
                    required:true   
                },
                confirm:{
                    required:true,
                    equalTo:"#new_pw"
                },
            },
            messages:{
                confirm:{
                    equalTo:"Confirm password and new password must be same."
                },  
            }
        })
    </script>
@endsection
