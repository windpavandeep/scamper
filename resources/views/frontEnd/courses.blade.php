@extends('frontEnd.layouts.master')
@section('title','Courses')
@section('content')
<style type="text/css">
    .product_scamper img{
        height: 300px;
        width: 100%;
        object-fit:contain;
        box-shadow: 0 0 10px 5px #D0D0D0;
    }

        .product_scamper:hover img{
            transform: scale(1.1);
            transition: all 0.8s ease;
        }


	.product_scamperViewBtn .product_scamperViewBtn_Design{
        /*background: #e80000;*/
        background: #0997de;
        color: #fff;
        padding: 7px 17px;
        position: absolute;
        bottom: 227px;
        right: 26%;
        text-decoration: none;
        transition: all 0.6s ease;
        opacity: 0;
        box-shadow: 0 0 10px 5px #D0D0D0;
    }

    .product_scamper_div:hover .product_scamperViewBtn_Design{
        opacity: 1;
    }

    .product_scamperFooter{
        text-align: center;
        display: block;
        margin-top: 10px;
    }

    .product_scamperFooter .product_scamper_tittle{
        color:#0997de;
        font-weight: bold;
    }

    .product_scamper2{
        margin-top: 52px;
    }
    
    .scamperskillsSearch{
        margin-bottom: 28px;
    }

	.scamperskillsSearch i{
        float: right;
        bottom: 24px;
        position: relative;
    }

   /*.product_scamper1 .col-sm-3.product_scamper_div {
		margin-bottom: 36px;
	}*/


	.wrap_downloads.wrap_canvas.scamperskills_find {
		margin-top: 15px;
		padding: 8px;

	}

	.home_page_wrapper.inner_page .fren_sec.courses1 {
		padding: 24px 0;
	}



</style>

<div class="page-wrapper">
	<!-- header index -->
	@include('frontEnd.common.cms_header')
	<!-- header index -->

    <div class="home_page_wrapper inner_page">

        <!-- enquiry form  -->
        <section class="fren_sec courses1">
            <div class="container">
	        	<div class="sec_heading text-center">
					<h2>Courses</h2>
						<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
				</div>
				<div class="wrap_downloads wrap_canvas scamperskills_find">
                    <div class="row">
                        <div class="col-sm-9 ">
                        	<h6 style="margin-left: 10px;">
                        		<a href="{{url('/')}}" title="Homepage">Home
                        		</a> 
                        		@if(!empty($category))
                        			/ <a href="{{url('courses?category_id='.$category->id)}}">{{ucfirst($category->name)}}</a>
                        		@endif
                        		@if(!empty($sub_category))
                        			/ <a href="{{url('courses?sub_category_id='.$sub_category->id)}}">{{ucfirst($sub_category->name)}}</a>
                        		@endif 
                        		@if(!empty($sub_subcategory))
                        			/ <a href="{{url('courses?sub_subcategory_id='.$sub_subcategory->id)}}">{{ucfirst($sub_subcategory->name)}}</a>
                        		@endif
                        	</h6>

                        </div>
                    </div>
                </div>
                @if(!empty($our_courses))   
	                <div class="wrap_downloads wrap_canvas">
	                    <!-- <div class="table_header">
	                        <div class="row">
	                            <div class="col-sm-9"><h3>Products Detail</h3></div>
	                        </div>
	                    </div>    --> 
	                   
	                    <div class="table_div">

	                        <div class="form_div">
	                            <!-- Search form -->
	                       
	                            <div class="row product_scamper1">
	                               
	                                	@foreach($our_courses as $value)
	                                		<?php
	                                			$course_image = '';
	                    					    if (!empty($value['course_image']['name'])) {
	                    			                // dd($second_section_content->image);
	                    			                if (file_exists(TrainerContentImageBasePath.'/'.$value['course_image']['name'])) {
	                    			                   $course_image = TrainerContentImageImgPath.'/'.$value['course_image']['name'];
	                    			                }else{
	                    			                    $course_image = DefaultImgPath;
	                    			                    
	                    			                }
	                    			            }else{
	                    			                $course_image = DefaultImgPath;
	                    			            }
	                    			            $course_id = base64_encode($value['id'].'-'.$value['trainer_id']);

	                    			            $final_price = $value['final_price'];
	                    			            if(!empty($final_price)){
	                    			                if(!empty($value['gst'])&& $value['gst']>'0'){

	                    			                    $final_price = ($value['paid_amount']*$gst_precent)/100;
	                    			                    $final_price = $value['paid_amount']+$final_price;
	                    			                }
	                    			                if($final_price<'1'){
	                    			                    continue;
	                    			                }
	                    			            }else{
	                    			                continue;
	                    			            }

	                                		?>
			                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product_scamper_div">
			                                    <div class="product_scamper">
			                                        <img src="{{$course_image}}" class="my_img">
			                                    </div>
			                                    <div class="product_scamperViewBtn">
			                                        <a class="product_scamperViewBtn_Design" href="{{url('/course/detail/'.@$course_id)}}"> View Course 
			                                        </a>
			                                    </div> 


			                                    <div class="product_scamperFooter">                                 
			                                        <div class="product_scamper_tittle">
			                                            <p>{{@$value['sub_category']['name']}}</p>
			                                        </div>
			                                         <div class="product_scamper_tittle middle-title">
			                                            <p>{{mb_strimwidth(ucfirst($value['title']), 0, 35, "")}}...</p>
			                                        </div>
			                                         <div class="product_scamper_tittle">
			                                         	@if(@$value['content_availability']=='paid')
						                                	<i class="fas fa-rupee-sign">
						                                		<span>{{$final_price}}</span>
						                                	</i>
					                                	@else
					                                		<span style="font-family: 'Font Awesome 5 Free';font-weight: 900;">Free</span>		
					                                	@endif
			                                        </div>
			                                    </div>                                
			                                </div> 
		                                @endforeach
	                                


	                                     <!--   <div class="col-sm-3 product_scamper_div">
	                                    <div class="product_scamper">
	                                        <img src="http://www.scamperskills.com/public/images/trainerContents/trainerContentImage/5d449f148f00c.jpg" class="my_img">
	                                    </div>

	                                    <div class="product_scamperViewBtn">
	                                        <a class="product_scamperViewBtn_Design" href=""> View Product 
	                                        </a>
	                                    </div> 

	                                    <div class="product_scamperFooter">                                 
	                                        <div class="product_scamper_tittle">
	                                            <p>UGC NET</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <p>NCERT FINGERTIPS</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <span>$</span><span>120</span>
	                                        </div>
	                                    </div>                                
	                                </div>  

	                                       <div class="col-sm-3 product_scamper_div">
	                                    <div class="product_scamper">
	                                        <img src="http://www.scamperskills.com/public/images/trainerContents/trainerContentImage/5d44a1929a5de.jpg" class="my_img">
	                                    </div>

	                                    <div class="product_scamperViewBtn">
	                                        <a class="product_scamperViewBtn_Design" href=""> View Product 
	                                        </a>
	                                    </div> 

	                                    <div class="product_scamperFooter">                                 
	                                        <div class="product_scamper_tittle">
	                                            <p>ADMIN EXAM</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <p>NCERT FINGERTIPS</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <span>$</span><span>120</span>
	                                        </div>
	                                    </div>                                
	                                </div>  


	                                       <div class="col-sm-3 product_scamper_div">
	                                    <div class="product_scamper">
	                                        <img src="http://www.scamperskills.com/public/images/trainerContents/trainerContentImage/5d44a097d993f.jpg" class="my_img">
	                                    </div>

	                                    <div class="product_scamperViewBtn">
	                                        <a class="product_scamperViewBtn_Design" href=""> View Product 
	                                        </a>
	                                    </div> 

	                                    <div class="product_scamperFooter">                                 
	                                        <div class="product_scamper_tittle">
	                                            <p>CHEMISTRY</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <p>NCERT FINGERTIPS</p>
	                                        </div>
	                                         <div class="product_scamper_tittle">
	                                            <span>$</span><span>120</span>
	                                        </div>
	                                    </div>                                
	                                </div>    -->                         
	                            </div>
	                        </div>
	                    </div>
	                                                    
	                </div>
                @else
                	<div class="">
                    	<center>
                    		<h5 style="margin-top: 85px;padding-bottom: 53px;">No course found  
                    		</h5>
                    	</center>
                	</div>
                @endif
            </div>
        </section>
    	@include('frontEnd.common.top_footer')
    </div>
</div>
@endsection