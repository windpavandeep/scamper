@extends('frontEnd.layouts.login')
@section('title','Forgot Password')
@section('content')
    <style type="text/css">
        .form_head div > .img-fluid {
            width: 70%;
            margin: 30px 0 40px;
        }
    </style>
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="register_sec user_mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2 col-xs-12 col-md-6 offset-md-3">
                            <div class="wrp_white">
                                <div class="sec_heading text-center">
                                    <h2>Forgot Password</h2>
                                    <p class="divider"><img src="{{asset(systemImgPath.'/secdivider.png')}}" class="img-fluid" alt="divider"></p>
                                </div>
                                <div class="form_head">
                                    <div class="text-center">
                                        <img src="{{asset(systemImgPath.'/logo.png')}}" class="img-fluid">
                                    </div>
                                    <!-- <h5 class="text-center">Enter your Valid Mobile Number</h5> -->
                                    <form method="post" id="forgot-password" action="{{url('/forgot-password') }}">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Enter your registered email id" value="{{ @$user_email }}">
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                @csrf
                                                <button class="btn btn_gradient btn_active">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#forgot-password').validate({ 
        rules:{
            email:{
                required:true,
                email:true,
                maxlength:100,
            },
        },
    });
</script>
@endsection