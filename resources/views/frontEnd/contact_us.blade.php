@extends('frontEnd.layouts.master')
@section('title','Contact Us')
@section('content')

<div class="page-wrapper">
	<!-- header index -->
	@include('frontEnd.common.cms_header')
	<!-- header index -->

    <div class="home_page_wrapper inner_page">

        <!-- enquiry form  -->
        <section class="fren_sec">
            <div class="container">
                <div class="wrap_downloads wrap_canvas">
                    <div class="table_header">
                        <div class="row">
                            <div class="col-sm-9"><h3>Contact Us</h3></div>
                        </div>
                    </div>
                    <div class="table_div">
                        <div class="form_div">
                        	<div class="colm_sec">
                            	<div class="row">
                            		<div class="col-sm-12 col-md-4">
                            			<div class="first_div my_cnct">
                            				<div class="icon_div text-center">
                            					<!-- <img src="{{ url('public/images/system/support.png')}}" alt="customer-support" class="img-fluid"> -->
                                                <!-- <i class="far fa-phone"></i> -->
                                                <i class="fas fa-phone" style="color:#0094de"></i>
                            				</div>
                            				<h3>Telephone</h3>
                            				<p>{{ucfirst(@$contact_us_contents->country)}}: +91{{@$contact_us_contents->contact_no}}</p>
                            			</div>
                            		</div>
                            		<div class="col-sm-12 col-md-4">
                            			<div class="first_div my_cnct">
                            				<div class="icon_div text-center">
                            					<i class="far fa-map"></i>
                            				</div>
                            				<h3>Address</h3>
                            				<p>{{ucfirst(@$contact_us_contents->city)}}: {{ucfirst(@$contact_us_contents->address)}}</p>
                            			</div>
                            		</div>
                            		<div class="col-sm-12 col-md-4">
                            			<div class="first_div">
                            				<div class="icon_div text-center">
                            					<i class="far fa-envelope"></i>
                            				</div>
                            				<h3>Email</h3>
                            				<p>{{ucfirst(@$contact_us_contents->email)}}</p>
                            			</div>
                            		</div>			
                            	</div>
                        	</div>
                            <!-- <div class="text-center">
                                <img src="img/franch.jpg" class="img-fluid fr_img">
                            </div> -->
                            <form action="{{url('/contact-us')}}" method="post" id="contact_us_form">
                                <div class="row">
                                    <div class="col-xs-12 col-md-4">
                                        <div class="map_cont text-center">
                                            @if(!empty(@$contact_us_contents->latitude))
                                                <iframe src="https://maps.google.com/maps?q={{$contact_us_contents['latitude']}},{{$contact_us_contents['longitude']}}&hl=es;z=14&amp;output=embed" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            @else
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7804.2555149733635!2d8.522386571054984!3d12.034724565901149!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x11ae872a934f05b5%3A0x7be93e4ae5c8f2dc!2sNigerian+Airforce+Barracks%2C+Kano%2C+Nigeria!5e0!3m2!1sen!2sin!4v1556002285514!5m2!1sen!2sin" width="350" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email Address" name="email">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Contact Number" name="contact_no">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" rows="5" placeholder="Your Enquiry . . ." name="enquiry"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center log_btn">
                                        <button type="submit" class="btn btn_gradient btn_active">Submit</button>
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- enquiry form / -->


    	<!-- Footer section -->
    	@include('frontEnd.common.top_footer')
    	<!-- Footer section -->
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
        $(function () {
              $('[data-toggle="tooltip"]').tooltip()
          })
    $(document).ready(function() {
        $('.niceselc').selectpicker();
        $('.dtpckr').datepicker();
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".course_tables").slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
        });
        $("#testimonial-slider").slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
        });
        $(".part_slick").slick({
            dots: false,
            arrows: false,
            speed: 10000,
            autoplay: true,
            autoplaySpeed: 0,
            cssEase: 'linear',
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
        });
    });
</script>

<script type="text/javascript">
    $('#contact_us_form').validate({
        rules:{
            email:{
                required:true,
                email: true,
            },
            first_name:{
                required:true,
                regex: /^[A-Za-z'.\s]+$/,
            },
            last_name:{
                required:true,
                regex: /^[A-Za-z'.\s]+$/,
            },
            contact_no:{
                required:true,
                regex: /^[0-9'.\s]{7,15}$/,
            },
            enquiry:{
                required:true
            },
            
        },
        messages: {
            "first_name": {
                regex: "*This field contain only Character",
            },
            "last_name": {
                regex: "*This field contain only Character",
            },
            "contact_no": {
                regex: "*This field contain only 7 to 15 Digits",
            },
            
        },
        
    })
</script>
@stop