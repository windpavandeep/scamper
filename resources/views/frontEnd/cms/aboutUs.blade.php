@extends('frontEnd.layouts.master')
@section('title','About Us')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="why_sec bene_sec about-sec-1">
                <div class="container">
                    <div class="col-sm-12 col-md-8 offset-md-2">
                        <div class="sec_heading text-center">
                            <h2>About Us</h2>
                            <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                        </div>
                        <div class="side_feat text-center">

                            <p class="home-section-txt">{!!$first_section->description !!}</p>
                        </div>
                    </div>
                </div>
            </section>
            @if(!empty($second_section))
                @foreach($second_section as $key=>$value)
                    <?php
                        $class="";
                        if( $key%2 == 0 ){
                            $class="sec_gray";
                        }  
                    ?>
                    <section class="why_sec bene_sec {{$class}}">
                        <div class="container">
                            <div class="row rowpp">
                                    <?php
                                        if(!empty($value['image'])){
                                            if(file_exists(AboutUsImageBasePath.'/'.$value['image'])){
                                                $image = AboutUsImageImgPath.'/'.$value['image'];
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    ?>
                                    @if( $key%2 == 0 )

                                        <div class="col-md-6 col-xs-12">
                                            <div class="img_app text-center">
                                                <img src="{{$image}}" class="img-fluid">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="sec_heading text-left">
                                                <h2>{{ucfirst($value['title'])}}</h2>
                                                <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                            </div>
                                            <div class="side_feat">
                                                <p class="app-header">Download the ScamperSkills app now!</p>
                                                <p class="home-section-txt">{!! ucfirst($value['description']) !!}</p>
                                                <div class="text-left">
                                                    <a href="javascript:;" class="btn btn_gradient btn_active Counselling" data-animation="fadeIn" data-delay="1110ms"><i class="fa fa-user"></i> Free Counselling</a>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                    <div class="col-md-6 col-xs-12">
                                        <div class="sec_heading text-left">
                                            <h2>{{ucfirst($value['title'])}}</h2>
                                            <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                                        </div>
                                        <div class="side_feat">
                                            <p class="app-header">Download the ScamperSkills app now!</p>
                                            <p class="home-section-txt">{!! ucfirst($value['description']) !!}</p>
                                            <div class="text-left">
                                                <a href="javascript:;" class="btn btn_gradient btn_active Counselling" data-animation="fadeIn" data-delay="1110ms"><i class="fa fa-user"></i> Free Counselling</a>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-md-6 col-xs-12">
                                            <div class="img_app text-center my_img">
                                                <img src="{{ $image }}" class="img-fluid">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </section>
                @endforeach
            @endif
            @if(!empty($our_partners))
                <section class="patr_sec gradient-blue">
                    <div class="container">
                        <div class="sec_heading text-center head_wh">
                            <h2>Our Partners</h2>
                            <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                        </div>
                        <div class="wrap_partn">
                            <div class="part_slick">
                                @foreach($our_partners as $key=>$value)
                                    <?php
                                        if(!empty($value['image'])){
                                            if(file_exists(AboutUsImageBasePath.'/'.$value['image'])){
                                                $image = AboutUsImageImgPath.'/'.$value['image'];
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    ?>
                                    <div class="item_part">
                                        <img src="{{ $image }}" class="img-fluid">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            @include('frontEnd.common.counselling_modal')
            @include('frontEnd.common.success_story')
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
