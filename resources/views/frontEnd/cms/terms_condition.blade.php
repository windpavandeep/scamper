@extends('frontEnd.layouts.master')
@section('title','Terms & Conditions')
@section('content')
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="why_sec bene_sec">
                <div class="container">
                    <div class="row rowpp">
                        <div class="col-sm-12 col-md-12">
                            <div class="sec_heading text-center">
                                <h2>Terms & Conditions</h2>
                                <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                            </div>
                            <div class="side_feat">
                                <p class="home-section-txt" style="overflow-wrap: break-word;">{!! ucfirst(@$terms_conditions->description) !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
