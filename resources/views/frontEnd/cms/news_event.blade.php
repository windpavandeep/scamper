@extends('frontEnd.layouts.master')
@section('title','News & Events')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.cms_header')
		<div class="home_page_wrapper inner_page">
			<!-- register -->
			<section class="newsEvnt_sec">
				<div class="container">
					<div class="wrap_downloads wrap_canvas">
						<div class="table_header">
							<h3>News & Events</h3>
						</div>
						<div class="table_div">
							<div class="news_botom">
								@if(!empty($news_events))
									@foreach($news_events as $value)
										<div class="row">
											<div class="col-sm-5 col-md-4">
												<?php
												    if(!empty($value['image'])){
												        // echo'<pre>'; print_r($stories->image);die;
												     // echo'<pre>'; print_r($become_trainer->image);die;
												        if(file_exists(NewsEventImageBasePath.'/'.$value['image'])){
												            $image = NewsEventImageImgPath.'/'.$value['image'];
												        }else{
												            $image = DefaultImgPath;
												        }
												    }else{
												        $image = DefaultImgPath;
												    }   
												     // $media_image = systemImgPath.'/'.'index.png';
												?>
												<div class="news_pic">
													<img src="{{$image}}" class="img-fluid" alt="image news" />
												</div>
											</div>
											<div class="col-sm-7 col-md-8">
												<div class="news_contnt">
													<h3 class="ttl_news">{{ucfirst($value['title'])}}</h3>
													<ul class="meta_news list-inline" type="none">
														<li class="dt_pos list-inline-item"><i class="far fa-calendar"></i> {{date('d/m/Y',strtotime($value['updated_at']))}}</li>
														<li class="tm_pos list-inline-item"><i class="far fa-clock"></i> {{date('H:i A',strtotime($value['updated_at']))}}</li>
													</ul>
													<p class="desc_news">{!!ucfirst($value['description']) !!}</p>
												</div>
											</div>
										</div>
									@endforeach
								@else
									No Data Found
								@endif
							</div>
							<div class="pagntn">
								<ul class="pagination justify-content-end">
								
								  	{{ $paginate->links() }}
								 
								</ul> 
							</div>
							<!-- <div class="pagntn">
								<ul class="pagination justify-content-end">
								  	<li class="page-item"><a class="page-link" href="#">Previous</a></li>
								  	<li class="page-item"><a class="page-link" href="#">1</a></li>
								  	<li class="page-item active"><a class="page-link" href="#">2</a></li>
								  	<li class="page-item"><a class="page-link" href="#">3</a></li>
								  	<li class="page-item"><a class="page-link" href="#">Next</a></li>
								</ul> 
							</div> -->
						</div>
					</div>
				</div>
			</section>
			@include('frontEnd.common.top_footer')
		</div>
	</div>
@endsection