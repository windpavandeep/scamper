@extends('frontEnd.layouts.master')
@section('title','FAQ')
@section('content')
<style type="text/css">
    .fa-angle-down{
        color: #ffb001
    }
    .fa-angle-up{
        color: #ffb001
    }
</style>
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="why_sec bene_sec">
                <div class="container">
                    <div class="row rowpp">
                        <div class="col-sm-12 col-md-12">
                            <div class="sec_heading text-center">
                                <h2>FAQ</h2>
                                <p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
                            </div>
                            <section class="pricing-faq-section">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="faq-content-container">
                                                <!-- <h3 class="faq-title">Frequently Asked Questions</h3> -->
                                                <ul class="faq-qa-list">
                                                    @foreach($faqs as $faq)
                                                        <li class="faq-qa-list-item">
                                                          {{ ucfirst($faq['title']) }}
                                                            <span class="faq-qa-list-item-arrow-icn"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                                                            <p class="faq-qa-answer">{{ ucfirst($faq['description']) }}</p>
                                                        </li>
                                                    @endforeach
                                                      
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        	@include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('.faq-qa-answer').hide(); 
        $('.faq-qa-list-item').on('click',function(){
            $(this).children('p').toggle(300);
            $(this).children('.faq-qa-list-item-arrow-icn').children('i').toggleClass('fa-angle-down fa-angle-up');
            $(this).siblings().children('p').hide();
            $(this).siblings().children('.faq-qa-list-item-arrow-icn').children('i').removeClass('fa-angle-up');
            $(this).siblings().children('.faq-qa-list-item-arrow-icn').children('i').addClass('fa-angle-down');
        });
    });
</script>
@endsection