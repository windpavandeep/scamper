@extends('frontEnd.layouts.master')
@section('title','Sitemap')
@section('content')
    <div class="page-wrapper">
    	@include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="fren_sec">
                <div class="container">
                    <div class="wrap_downloads wrap_canvas">
                        <div class="table_header">
                            <div class="row">
                                <div class="col-sm-12"><h3>Sitemap</h3></div>
                            </div>
                        </div>
                        <div class="table_div">
                            <div class="sitemap">
                                <ul type="disc" class="d-flex sitemap_ul">
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <li><a href="{{url('/about-us')}}">About Us</a></li>
                                    <li><a href="{{url('/downloads')}}">Downloads</a></li>
                                    <li><a href="{{url('/news-events')}}">News & Events</a></li>
                                    <li><a href="{{url('/become-trainer')}}">Register As A Teacher</a></li>
                                    <li><a href="{{url('/become-retailer')}}">Become A Reseller</a></li>
                                    <li><a href="{{url('/contact-us')}}">Contact Us</a></li>
                                    
                                    <li><a href="{{url('/term-conditions')}}">Terms & Conditions</a></li>
                                    <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                                    <li><a href="{{url('/disclaimer')}}">Disclaimer</a></li>
                                    <!-- <li><a href="{{url('/faq')}}">FAQ</a></li> -->
                                    
                                    <li><a href="javascript:;" class="Counselling">Free Counselling</a></li>
                                    @if(!Auth::check())
                                        <li><a href="{{url('/register/user')}}">Register Free</a></li>
                                        <li><a href="{{url('/login')}}">Login</a></li>
                                        <li><a href="{{url('/forgot-password')}}">Forgot Password</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection