@extends('frontEnd.layouts.master')
@section('title','Home')
@section('content')
<?php
    
    if (!empty($why_choose_us_image->image)) {

            $image = HomeContentImageImgPath.'/'.$why_choose_us_image->image;
        
    }else{
        $image = DefaultImgPath;
    }

    $faculty_background_image = DefaultImgPath;
	if (!empty($our_faculty_banner_image)) {
	// dd($second_section_content->image);
		if (file_exists(OurFacultyImageBasePath.'/'.$our_faculty_banner_image)) {
		  $faculty_background_image = OurFacultyImageImgPath.'/'.$our_faculty_banner_image;
		}else{
			$faculty_background_image = DefaultImgPath;
		}
	   
	}

    if (!empty($third_section_video)){
        if (file_exists(HomeContentImageBasePath.'/'.$third_section_video)) {
           $video = HomeContentImageImgPath.'/'.$third_section_video;
        }else{

            $video="https://vimeo.com/195304295";
        }
    }else{

        $video="https://vimeo.com/195304295";
    }

?>

<style type="text/css">
    .why_us_sec {
        background-image: url("{{$image}}");
    }
    .fac_sec {
		background: url("{{$faculty_background_image}}");
    }
</style>
<div class="page-wrapper">
	@include('frontEnd.common.header')
    <div class="home_page_wrapper ">
    	<section class="slider_sec">
    		<div class="container">
			    <div class="row">
			        <div id="homeSlider">
		                <div class="slide_item">
	                        <div class="slide-content-wrapper d-flex align-items-center">
	                            <div class="slide-content text-left">
	                                <h3 class="slide-subtitle" data-animation="fadeIn" data-delay="300ms">{{@$first_section->first_title}}</h3>
	                                <h2 class="slide-title" data-animation="fadeIn" data-delay="500ms">{{@$first_section->second_title}}
	                                </h2>
	                                
	                                @if(!Auth::check())
	                                	<a href="{{url('/register/user')}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"><i class="fa fa-user-plus"></i> Register</a>
	                                @endif
	                                <a href="javascript:;" class="btn btn_gradient Counselling" data-animation="fadeIn" data-delay="1110ms"><i class="fas fa-user-tie"></i> Free Counselling</a>
	                            	<p class="slide-description" data-animation="fadeIn" data-delay="700ms" >{{@$first_section->third_title}}<br>
	                                <p class="" style="color: #fff;"><i class="fas fa-mobile-alt"></i> Toll Free: {{@$first_section->contact_no}}</p>
	                            	    <a class="dwnlld_a" href="https://play.google.com/store">
	                            	    	<img src="{{asset('public/images/system/gp.png')}}" style="">
	                            	    </a>
	                            	</p>
	                            </div>

	                            <div class="slide-thumbnail-image" data-animation="fadeInUp" data-delay="1200ms" >
	                                <img src="{{ asset('/public/images/system/2.png') }}" class="img-fluid">
	                            </div>
	                        </div>
		                </div>
			        </div>
			    </div>
			</div>
    	</section>
    	<section class="why_us_sec">
    		<div class="container">
    			<div class="row">
        			<div class="col-sm-7 offset-5">
        				<div class="sec_heading text-left">
            				<h2>Why Choose Us</h2>
            				<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
            			</div>
        				<div class="wrap_choose">
        					<div class="row">
        						<?php foreach ($second_section as $key => $value) {
        							
                                    $image = DefaultImgPath;
                                    if (!empty($value['image'])) {
                                        // dd($second_section_content->image);
                                        if (file_exists(HomeContentImageBasePath.'/'.$value['image'])) {
                                           $image = HomeContentImageImgPath.'/'.$value['image'];
                                        }
                                            
                                    }
        						?>
        						<div class="col-sm-6">
        							<div class="wrp_single">
        								<img class="img-fluid" src="{{ $image }}">
        								<h3 class="hed_why">{{@$value['title']}}</h3>
        								<p class="why_desc"> {{@$value['description']}}</p>
        							</div>
        						</div>
        						<?php } ?>
        					</div>
        				</div>
        			</div>
        		</div>
    		</div>
    	</section>
    	<?php foreach ($third_section as $key => $value) {
    	
    		if($key == 0){
    			$cls = '';
    			
    		}elseif($key == 1){
    			$cls = 'sec_gray';
    			
    		}else{
    			$cls = 'sec_gray';
    			
    		}	
		    if (!empty($value['image'])) {
                // dd($second_section_content->image);
                if (file_exists(HomeContentImageBasePath.'/'.$value['image'])) {
                   $image = HomeContentImageImgPath.'/'.$value['image'];
                }else{
                    $image = DefaultImgPath;
                    
                }
            }else{
                $image = DefaultImgPath;
            }
    		if($key%2 != 1){

    	?>
	    	<section class="bene_sec ">
			    <div class="container">
			        <div class="row rowpp">
			            <div class="col-md-6 col-xs-12">
			            	<div class="sec_heading text-left">
	            				<h2>{{@$value['title']}}</h2>
	            				<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
	            			</div>
			                <div class="side_feat">
		                        <p class="home-section-txt">{{@$value['description']}}</p>
		                        <div class="text-left">
		                        	@if(!Auth::check())
			                        	<a href="{{url('/register/user')}}" class="btn btn_gradient btn_active" data-animation="fadeIn" data-delay="1110ms"><i class="fa fa-file-word"></i> Student Registration</a>
			                        @endif
			                    </div>
			                </div>
			            </div>
			            <div class="col-md-6 col-xs-12">
			            	<div class="img_app text-center">
			            		<img src="{{ $image }}" class="img-fluid">
			            	</div>
			            </div>
			        </div>
			    </div>
			</section>
		<?php } else { ?>
			<section class="bene_sec {{$cls}}">
			    <div class="container">
			        <div class="row rowpp">
			            <div class="col-md-6 col-xs-12">
			            	<div class="img_app text-center">
			            		<img src="{{  $image }}" class="img-fluid">
			            		<div class="video-play-icon " id="video-trigger">
	                                <a class="wpsuper-lightbox-video vbox-item" data-vbtype="video">
	                                    <i class="fa fa-play"></i>
	                                </a>
	                            </div>
			            	</div>
			            </div>
			            <div class="col-md-6 col-xs-12">
			            	<div class="sec_heading text-left">
	            				<h2>{{@$value['title']}}</h2>
	            				<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
	            			</div>
			                <div class="side_feat">
		                        <!-- <p class="app-header">Download the ScamperSkills app now!</p> -->
		                        <p class="home-section-txt">{{@$value['description']}}</p>
		                        <div class="text-left">
		                        	<a href="javascript:;" class="btn btn_gradient btn_active Counselling" data-animation="fadeIn" data-delay="1110ms"><i class="fa fa-user"></i>Get In Touch</a>
		                        </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
		<?php }} ?>
		<section class="cors_sec">
			<div class="container">
				<!-- Price table section start -->
		        <div id="cors_id" class="standard-section section-gray">
		            <div class="container">
		            	<div class="sec_heading text-center">
            				<h2>Our Courses</h2>
            					<p class="divider"><img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider"></p>
            			</div>
		                <div class="row">
		                    <div class="col-sm-12">
		                        <div class="course_tables">

		                        	<?php foreach ($our_courses as $key => $value) {
		                        		
		                        		$key = $key+1;
		                        		$fade_right_cls = 'fadeInRight';
		                        		$with_bg_cls = 'with_bg';
		                        		if($key%2==0){
		                        			$fade_right_cls = '';
		                        			$with_bg_cls = '';
		                        		}
		                        	?>
			                            <div class="single_cors {{$with_bg_cls}} wow {{$fade_right_cls}}" data-wow-delay=".2s">
			                                <div class="cors_head">
			                                    <h2 class="cors_title">{{@$value['sub_category']['name']}}</h2>
			                                    <p>{{@$value['title']}}</p>
			                                </div>
			                                <div class="cors_body">
			                                	
			                                    <p>{!!@substr($value['description'],0,400)!!}</p>
			                                </div>
			                                <div class="cors_footer">
			                                	<?php
			                                	    $course_id = base64_encode($value['id']);
			                                	
			                                	?>
			                                    <a href="{{url('/course/detail/'.@$course_id)}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"> Buy your Plan</a>
			                                </div>
			                            </div>
		                            <?php } ?>
		   
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
			</div>
		</section>
		@include('frontEnd.common.our_faculty')
		@include('frontEnd.common.success_story')
        <section class="counter-section ptb-80 gradient-blue">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="single-counter">
                            <div class="counter-icon">
                                <i class="fa fa-download"></i>
                            </div>
                            <p><span class="counter" >5000</span> +</p>
                            <h4 class="counter-title">Downloads</h4>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="single-counter">
                            <div class="counter-icon">
                                <i class="fa fa-user-graduate"></i>
                            </div>
                            <p><span class="counter" >1000</span> +</p>
                            <h4 class="counter-title">Achievers</h4>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="single-counter">
                            <div class="counter-icon">
                                <i class="fa fa-file-signature"></i>
                            </div>
                            <p><span class="counter" >800</span> +</p>
                            <h4 class="counter-title">Test Given</h4>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="single-counter">
                            <div class="counter-icon">
                                <i class="fa fa-chalkboard-teacher"></i>
                            </div>
                            <p><span class="counter" >200</span> +</p>
                            <h4 class="counter-title">Faculty</h4>
                        </div>
                    </div>
                </div>
            </div>
             <div class="modal fade offer_popu" id="video_modal">
            	<div class="modal-dialog modal-lg">
            		<div class="modal-content">
            			<div class="offer_close">
            				<button type="button" class="close" data-dismiss="modal" style="padding-right: 6px;">&times;</button>
            			</div>
            			<div class="modal-body">
							<div id="vidBox">
							    <video id="demo" loop controls width="100%" height="100%">
							      <source src="{{$video}}" type="video/mp4">
							    </video>
							</div>	
            			</div>
            		</div>
            	</div>
            </div>
        </section>
       	@include('frontEnd.common.top_footer')
    	@include('frontEnd.common.footer')
    </div>
</div>
@include('frontEnd.common.offer')
@include('frontEnd.common.counselling_modal')
@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript">

	$('#video-trigger').click(function(){

		$('#video_modal').modal('show');		
	});	

</script>
<script type="text/javascript">
	$(document).ready(function(){
		var pop_up_cookie = $.cookie("pop_up");
		if(pop_up_cookie==null){
			$('#offer_modal').modal('show');
		 	$.cookie("pop_up", 1, { expires : 1 });
		}
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){	    
	    $("#hm_slider").slick({
	    	dots: true,
			infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: false,
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 481,
		      settings: {
		        arrows: true,
		        slidesToShow: 1,
		      }
		    }
		  ]
	    });
	});
</script>

@endsection