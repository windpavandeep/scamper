@extends('frontEnd.layouts.master')
@section('title','Order Details')
@section('content')
<style type="text/css">
    .pcode_pdetail .sidebar_wrap.card_shd .left_sidebar{
    height: auto;
    }
    .pcode_pdetail .sidebar_wrap.card_shd .left_sidebar ul li{
    list-style-type: none;
    padding: 10px;
    display: inline-flex;
    }
    .btn.btn_gradient.btn_active {
    font-size: 2px;   
    }
    .pcode_pdetail .sidebar_wrap.card_shd .left_sidebar .ul_side .Coupons1 input{
    width: 40%;
    }
    .pcode_pdetail .sidebar_wrap.card_shd .left_sidebar .ul_side .Coupons2{
    display: inline;
    }
    .subscription_history1 .Coupons2 .pc_value{
    width: 80%;
    }
    .pcode_pdetail .sidebar_wrap.card_shd .left_sidebar .ul_side .btn.btn_gradient.btn_active.two{
    float: right;
    font-size: 13px;
    }
    .col-sm-3 .sidebar_wrap.card_shd{
    padding: 0px 0 0px;
    }
    .side_menu.left_2sidebar .sidebar_wrap.card_shd ul li{
    list-style-type: none;
    padding: 10px;
    }
    .side_menu.left_2sidebar .sidebar_wrap.card_shd ul .first_li{
    color: #0094de;
    font-weight: bold;
    }
    .side_menu.left_2sidebar .sidebar_wrap.card_shd ul li .Detail_right{
    float: right;
    color: #7a7b7c;
    }
    .side_menu.left_2sidebar .sidebar_wrap.card_shd ul li .btn.btn_gradient.btn_active.three{
    display: block;
    margin: auto;
    font-size: 13px;
    }
    .description_Right .description_Right_ul li{
    list-style-type: none;
    }
    .description_Right .description_Right_ul .first_li2{
    font-weight: bold;
    }
    .description_Right .description_Right_ul li .Detail_right{  
    color: #7a7b7c;
    padding-left: 10px;
    }
    .main_cntnt_dash .description_Right {
    margin-top: 18px;
    }
    .description_Right .description_Right_ul li .Detail_right{  
    color: #7a7b7c;
    padding-left: 10px;
    }
    .description_Right .description_Right_ul li .btn.btn_gradient.btn_active.four  {
    font-size: 13px;
    margin-top: 15px;
    width: 125px;
    }
    .side_menu.left_2sidebar {
    margin-top: 10px;
    }
    .main_cntnt_dash .card_shd.cnt_skamper .img-fluid.rfrl_code
    {
    width: 21%;
    height: 21%;
    padding: 25px 0px;
    }
    .main_cntnt_dash .card_shd.cnt_skamper.rfrl_code_div{
    padding: 10px;
    }
    .col-sm-4.rfrl_codedIvBtn span .btn.btn_gradient.btn_active.one{
    font-size: 13px;
    }
    .main_cntnt_dash .pos_rel.pic_top .img-fluid.book_img{
    border-radius: 0%;
  	height: 220px;
	width: 365px;
	/*margin-top: 18px;*/
	border: none;
	object-fit: contain;
	top: 16px;
	position: relative;	
	margin-bottom: 27px;
    }
    .top_right_heading .page_head .main_label{
    font-weight: bold;
    font-size: 25px;
    }
    .pcode_pdetail .security_span label{
    color: #34a608;
    /*float: right;*/
    padding: 0px 0px 77px;
    }
    .subscription_history .Detail_right i{
    background: linear-gradient(45deg, #13B3F7 0%, #0131ff 100%);
    border-radius: 100%;
    color: #fff;
    padding: 6px;
    font-size: 8px;
    }
    .subscription_history . i .Detail_right{
    background: linear-gradient(45deg, #13B3F7 0%, #0131ff 100%);
    border-radius: 100%;
    color: #fff;
    padding: 6px;
    font-size: 8px;
    }
    .after_heading .after_heading{
    display: inline;
    }
    .home_page_wrapper.inner_page.trnr_dash_div .sec_dashboard.db_main {
    padding: 40px 0 0 0;
    }
    .view_prof_page .cont_shd_frm {
    pointer-events: unset;
    }

    .card_shd.cnt_skamper{
    	padding: 15px !important;
    }

</style>
<div class="page-wrapper">
@include('frontEnd.common.cms_header')
<div class="home_page_wrapper inner_page trnr_dash_div">
    <section class="sec_dashboard db_main">
        <div class="container-fluid">
            <div class="wrap_dash_sec view_prof_page view_usr">
                <div class="row">
                    <div class="col-sm-9 top_right_heading">
                        <div class="page_head">
                            <center>
                                <label class="main_label">Order Details</label> 
                                <div class="after_heading">
                                    <img src="{{asset('public/images/becomeTrainer/5d2d6ef7ac6fa.png')}}" class="img-fluid rfrl_code" alt=""/>
                                </div>
                            </center>
                        </div>
                        <div class="main_cntnt_dash">
                            <div class="card_shd cnt_skamper rfrl_code_div">
                                <div class="row">
                                    <div class="col-sm-10 offset-2">
                                        <div class="page_head">
                                            <img src="{{asset('public/images/system/final_curve-resized.png')}}" class="img-fluid rfrl_code" alt="">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <input style="width: 50%" type="text" class=" form-control " name="pincode" placeholder="Pincode" value="" id="pincode" disabled="">
                                                </div>
                                                <span class="error1" id="pincode_error"></span>
                                            </div>
                                            <div class="col-sm-4 rfrl_codedIvBtn">
                                                <span>   
                                                	<a href="javascript:;" class="btn btn_gradient btn_active one" id="edit_butn">Edit</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="main_cntnt_dash">
                            <div class="card_shd cnt_skamper productdetail_img">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="text-center img_user">
                                            <div class="pos_rel pic_top">  
                                                <?php
                                                    $image = '';
                                                    if (!empty($order_details['course_image']['name'])) {
                                                        // dd($student_details->image);
                                                        if (file_exists(TrainerContentImageBasePath.'/'.$order_details['course_image']['name'])) {
                                                            $image = TrainerContentImageImgPath.'/'.$order_details['course_image']['name'];
                                                        }else{
                                                            $image = DefaultImgPath;
                                                        }
                                                    }else{
                                                        $image = DefaultImgPath;
                                                    }
                                                    ?> 
                                                <img src="{{$image}}" class="img-fluid book_img" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 description_Right">
                                        <ul class="ul_side description_Right_ul">
                                            <li class="first_li2">{{ ucfirst($order_details->title) }}</li>

                                            <li class="subscription_history">
                                                <span class="TItle_left"> MRP : </span>
                                                <span class="Detail_right">{{CURRENCY}}{{$final_price}}</span>
                                            </li>
                                            <!-- <li class="subscription_history">
                                                <span class="TItle_left"> Qty :</span> 1
                                            </li> -->

                                            <li class="subscription_history">
                                                @if($order_details->trainer_id>0)
                                                	<a class="btn btn_gradient btn_active four" href="{{url('/course/detail/'.base64_encode($order_details['id'].'-'.$order_details['trainer_id']))}}">Remove</a>
                                                @else
                                                	<a class="btn btn_gradient btn_active four" href="{{url('/course/detail/'.base64_encode($order_details['id']))}}">Remove</a>
                                                @endif

                                                
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 pcode_pdetail">
                        <span class="security_span">
	                        <label> 
	                        <i class="fa fa-check" aria-hidden="true"></i>
	                        100% Secure
	                        </label>
                        </span>
                        <div class="sidebar_wrap card_shd">
                            <div class="left_sidebar">
                                <ul class="ul_side">
                                    <li> Promo Code </li>
                                    <li class="subscription_history1">
                                        <label class="Coupons2">
                                        <input type="text" name="" class="form-control pc_value" placeholder="Promo Code" id="promo_code" applide_promo_code=''> 
                                        </label> 
                                        <button class="btn btn_gradient btn_active two" id="apply"> Apply</button> 
                                    </li>
                                </ul>
                            </div>
                            <div class="side_menu left_2sidebar">
                                <div class="sidebar_wrap card_shd">
                                    <ul class="ul_side">
                                        <li class="first_li"> Price Detail</li>
                                        <li class="subscription_history">
                                            <span class="TItle_left"> Price </span> 
                                            <span class="Detail_right"> {{CURRENCY}}{{$order_details->paid_amount}}</span>
                                        </li>
                                        <li class="subscription_history">
                                            <span class="TItle_left">Discount
                                            </span>
                                            <span class="Detail_right" id="promotional_cost" promo_discount=''>- </span> 
                                        </li>
                                        <li class="subscription_history">
                                            <span class="TItle_left">GST @if($gst_precent>'0') ({{$gst_precent}}%)

                                            @endif
                                            </span>
                                            <span class="Detail_right" id="gst_amount">
                                                @if(@$gst_price>0)
                                                    {{CURRENCY}}{{$gst_price}}
                                                @else
                                                    -
                                                @endif
                                            </span>
                                            @if(@$gst_price>0)
                                                <input type="hidden" id="gst_value" value="{{CURRENCY}}{{$gst_price}}">
                                            @else
                                                -
                                                <input type="hidden" id="gst_value" value="-">
                                            @endif
                                        </li>
                                        <li class="subscription_history">
                                            <span class="TItle_left">Order Total </span>
                                            <span class="Detail_right final_total" id="order_total">{{CURRENCY}}{{$final_price}}</span> 
                                        </li>
                                        <hr>
                                        <li class="subscription_history">
                                            <span class="TItle_left"> Total  </span>
                                            <span class="Detail_right final_total" id="final_price" final_price="{{$final_price}}">{{CURRENCY}}{{$final_price}}</span> 
                                        </li>
                                        <li class="subscription_history">
                                            <button class="btn btn_gradient btn_active three" id="place_order"> Place Order </button> 
                                        </li>
                                    </ul>
                                    <input type="hidden" name="check error" id="all_error" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('frontEnd.common.top_footer')
    </div>
</div>
@endsection
@section('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
    function call_rzor_pay(course_id,total_cost,promo_code,referral_code,gst,discount_amount){
        // alert(course_id);
        // alert(total_cost);
        // alert(promo_code);
        // alert(referral_code);
        // alert(gst);
        // alert(discount_amount);
        price       = total_cost*100;
        var options = {
            // "key": "rzp_live_CDdlkrYbhjrMIX",  
            "key": "rzp_test_QYhZB5gAfNTMNX",
            "amount":price,  
            "name":"Scamper Skills",
            "currency":"INR",
    
            // "description":"Total Amount: "+total_amount+', '+"GST : "+gst+'%',
            "image": "http://scamperskills.com/public/images/system/logo.png",
            "handler": function (response){
                // alert()
                if(response.razorpay_payment_id != ''){
                    // $('.loader').show();
                    $.ajax({
                        method:"post",
                        url:"{{ url('/student/course/pay') }}",
                        data:{
                            price:price, 
                            course_id:course_id,
                            discount_coupon:promo_code,
                            refer_code:referral_code,
                            gst:gst,
                            discount_amount:discount_amount,
                            razorpay_payment_id:response.razorpay_payment_id, 
                            "_token":"{{ csrf_token() }}" },
                        success:function(resp){
                            // alert(resp);
                            if(resp == 'false'){
                                swal("Oops!","Something went wrong. Please try again later,","error");     
                            } else{
                                if(resp!=''){

                                	toastr.success('Course purchased successfully.').fadeOut(8000);
                                	
                                    window.location.href = resp;
                                }else{
                                    swal("Oops!","Something went wrong. Please try again later,","error");
                                }
                            }
                            // $('.loader').hide();
                        }
                    })
                } else{
                    swal("Oops!","Something went wrong. Please try again later",'error');     
                }
            },
            "theme": {
                "color": "#0449fe"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }
    
</script>
<script type="text/javascript">
    $('#apply').on('click',function(){
        var discount_coupon = $('#promo_code').val();
    
        var course_id  = "{{@$course_id}}";
        var gst_value  = $('#gst_value').val();
        // alert(gst_value);
        $('.loader').show();
        if(discount_coupon!=''){
    
            var apply_text =  $('#apply').html();
            // $('#gst_amount').html(gst_value);
            if(apply_text=='Remove'){
                $('#apply').html(' Apply');
                $('#promo_code').removeAttr('disabled');
                $('#promo_code').val('');
                $('#promo_code').attr('applide_promo_code','');
                $('#promotional_cost').attr('promo_discount','')
                $('#promotional_cost').html('-');
                $('.final_total').html("{{CURRENCY}}"+"{{$final_price}}");
                $('#final_price').attr('final_price',"{{$final_price}}");
                $('#gst_amount').text(gst_value);

            }
            if(apply_text==' Apply'){
                $.ajax({
                    type:"post",
                    url:"{{ url('/student/validate/discount-coupon') }}",
                    data:{
                        coupon_code:discount_coupon,
                        course_id:course_id
                    },
                    success:function(resp){
                     
                        console.log(resp);
                        if(resp == 'false'){
                           
                            /*swal("Oops!","Something went wrong. Please try again later,","error");*/
                            swal("Error","Invalid promo code,","error");
                            $('#all_error').val('1');
                             $('#promotional_cost').attr('promo_discount','')
                             $('#promo_code').attr('applide_promo_code','');
                        } else{
                            if(resp!=''){
                                // alert(resp.gst_amount);
                                $('#promo_code_eror');
                                $('#gst_amount').html("{{CURRENCY}}"+resp.gst_amount);
                                $('#promotional_cost').attr('promo_discount',resp.discount_amount);
                                $('#promotional_cost').html("-"+" "+"{{CURRENCY}}"+resp.discount_amount);
                                $('.final_total').html("{{CURRENCY}}"+resp.final_total);
                                $('#final_price').attr('final_price',resp.final_total);
                                $('#promo_code').attr('applide_promo_code',resp.coupon_code);
                                $('#apply').html('Remove');
                                $('#promo_code').attr('disabled','disabled');
                                $('#all_error').val('0');
                                // $('#purchased').html(resp);
                                // $('#price').html("{{CURRENCY}}"+resp.price);
                                // swal('Course Purchased',"Course purchased successfully","success"); 
                                
                            }else{

                                $('#gst_amount').html(gst_value);
                                // $('#gst_amount').html("{{CURRENCY}}"+resp.gst_amount);
                                $('#all_error').val('1');
                                $('#promotional_cost').attr('promo_discount','')
                                swal("Oops!","Something went wrong. Please try again later,","error");
                                $('#promo_code_eror').html('promo_discount','');
                                $('#promo_code').attr('applide_promo_code','');
                            }
                        }
                    }
                        
                });
            }
                $('.loader').hide();
            
        }else{
            $('.loader').hide();
            swal("Error","Please enter promo code,","error"); 
            $('#promo_code_eror').html('');
            $('#all_error').val('0');
            $('#promo_code').attr('applide_promo_code','');
            $('#promotional_cost').attr('promo_discount','')
            $('#promotional_cost').html("{{CURRENCY}}"+0);
            $('.final_total').html("{{CURRENCY}}"+"{{$final_price}}");
            $('#final_price').attr('final_price',"{{$final_price}}")
           
        }
    
        
       /* if(apply_text=='Remove'){
            $('#apply').html('Apply');
            $('#promo_code').val('');
        }*/
    });
</script>
<script type="text/javascript">
    $('#place_order').on('click',function(){
        var error          = $('#all_error').val();
        var discount_coupon = $('#promo_code').val();
    
        var total_cost     = $('#final_price').attr('final_price');
        var refferal_code  = $('#pincode').val();
        var gst            = "{{@$gst_precent}}";
        var discount_amount= $('#promotional_cost').attr('promo_discount'); 
        var course_id      = "{{@$course_id}}";
        var promo_code     = $('#promo_code').attr('applide_promo_code');
    
        
        // if(refferal_code!=''){
            
            if(error==2){
                return false;
            }else
            if(error<1){
                // alert(total_cost);
                call_rzor_pay(course_id,total_cost,promo_code,refferal_code,gst,discount_amount);
            }else{
                swal("Error","Invalid promo code,","error");
            }
        // }
    });
</script>
<script type="text/javascript">
    $('#edit_butn').on('click',function(){
        $('#pincode').removeAttr('disabled');
        $('#pincode').focus();
    });
</script>
<script type="text/javascript">
    $('#pincode').on('input',function(){
        // alert('entre');
        $('#all_error').val('0');
        
       //  var zip = $('#pincode').val();
       //  var reg = /^[0-9]+$/;
       // /* if(zip == ''){
       //      $('#all_error').val('2');
       //      $('#pincode_error').html('*This field is required');
       //  }*/
       //  if(zip ==''){
       //      $('#pincode_error').html('');
       //  }else if((zip.length)>9){
       //      $('#all_error').val('2');
       //      $('#pincode_error').html('*Pincode must be less then 9 digits');
       //  }
       //  else if (!reg.test(zip)){
       //      // alert('enter');
       //      $('#all_error').val('2');
       //      $('#pincode_error').html('*Please enter only digits');
       //  }else{
       //      $('#pincode_error').html('');
       //  }
        // alert($('#all_error').val());
    });
</script>
<script type="text/javascript">
    $('#promo_code').on('input',function(){
        var discount_coupon = $('#promo_code').val();
        // alert(discount_coupon);
        if(discount_coupon==''){
            $('#all_error').val('0');
            $('#apply').html(' Apply');
        }
    });
</script>
<script type="text/javascript">
    $('#remove_butn').on('click',function(){
        // alert('enter');
        var value = $('#remove_butn').attr('url_redirect');
        window.location.href = value;
    });
</script>
@endsection
