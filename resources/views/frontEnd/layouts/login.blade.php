<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <title>{{ PROJECT_NAME }} - @yield('title')</title>
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <!-- <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/font-awesome.css') }}"> -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap-select.css') }}">
        <!-- <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/select2.min.css') }}" /> -->
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/fontawesome-all.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/developer.css') }}">

        <?php
            $b_type = getBrowsere();
            // echo '<pre>'; print_r($b_type); die;
        ?>
        @if ($b_type['name'] == 'Internet Explorer' || $b_type['name'] == 'Unknown')
            <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/ie_style.css') }}">
        @endif
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/responsive-style.css') }}" />

        <script type="text/javascript" src="{{ url( frontEndJsPath.'/jquery.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap.js') }}"></script>

        <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap-select.js') }}"></script> 
        <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap-datepicker.js') }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap-datepicker.css') }}">
        <link href="{{ url( frontEndCssPath.'/toastr.min.css') }}" rel="stylesheet">
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-P452Y7L8MH"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-P452Y7L8MH');
        </script>
    </head>
    <?php
        function getBrowsere() 
        { 
            $u_agent = $_SERVER['HTTP_USER_AGENT']; 
            $bname = 'Unknown';
            $platform = 'Unknown';
            $version= "";
            $ub = '';

            //First get the platform?
            if (preg_match('/linux/i', $u_agent)) {
                $platform = 'linux';
            }
            elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                $platform = 'mac';
            }
            elseif (preg_match('/windows|win32/i', $u_agent)) {
                $platform = 'windows';
            }

            // Next get the name of the useragent yes seperately and for good reason
            if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Internet Explorer'; 
                $ub = "MSIE"; 
            } 
            elseif(preg_match('/Firefox/i',$u_agent)) 
            { 
                $bname = 'Mozilla Firefox'; 
                $ub = "Firefox"; 
            } 
            elseif(preg_match('/Chrome/i',$u_agent)) 
            { 
                $bname = 'Google Chrome'; 
                $ub = "Chrome"; 
            } 
            elseif(preg_match('/Safari/i',$u_agent)) 
            { 
                $bname = 'Apple Safari'; 
                $ub = "Safari"; 
            } 
            elseif(preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Opera'; 
                $ub = "Opera"; 
            } 
            elseif(preg_match('/Netscape/i',$u_agent)) 
            { 
                $bname = 'Netscape'; 
                $ub = "Netscape"; 
            } 

            // finally get the correct version number
            $known = array('Version', $ub, 'other');
            $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
            if (!preg_match_all($pattern, $u_agent, $matches)) {
                // we have no matching number just continue
            }

            // see how many we have
            $i = count($matches['browser']);
            if ($i != 1) {
                //we will have two since we are not using 'other' argument yet
                //see if version is before or after the name
                if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                    $version= $matches['version'][0];
                }
                else {
                    $version= $matches['version'][1];
                }
            }
            else {
                $version= $matches['version'][0];
            }

            // check if we have a number
            if ($version==null || $version=="") {$version="?";}

            $okkkk = array(
                'userAgent' => $u_agent,
                'name'      => $bname,
                'version'   => $version,
                'platform'  => $platform,
                'pattern'    => $pattern
            );
            // echo '<pre>'; print_r($okkkk); die;
            return $okkkk; 

        } 
    ?>
    <body>
        @yield('content')
        <!-- <div class="loader">
            <img src="{{ url( 'public/images/system/lodr.gif') }}" class="img-resposnive">
        </div>
        <script type="text/javascript">
            $('.loader').show();
            $(document).ready(function(){
                $('.loader').hide();
            });
        </script> -->
        <!-- <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap.js') }}"></script> -->
        <!-- <script type="text/javascript" src="{{ url( frontEndJsPath.'/select2.min.js') }}"></script> -->
        <script type="text/javascript" src="{{ url(frontEndJsPath.'/sweetalert.min.js') }}"></script>
        <!-- <script type="text/javascript" src="{{ url( frontEndJsPath.'/jquery.timepicker.js') }}"></script> -->
        <script src="{{ url( frontEndJsPath.'/toastr.min.js') }}"></script>
        <script src="{{ url( frontEndJsPath.'/jquery.validate.js') }}"></script>
        <script type="text/javascript" src="{{ url( frontEndJsPath.'/waypoints.js') }}"></script>
        <script type="text/javascript" src="{{ url( frontEndJsPath.'/counterup.js') }}"></script>
        <script type="text/javascript" src="{{ url( frontEndJsPath.'/wow.js') }}"></script>
        @include('frontEnd.common.counselling_modal')
        @include('frontEnd.common.notifications')
        <script type="text/javascript">
            $(document).ready(function() {
                $('.niceselc').selectpicker({
                    liveSearch:true,
                    virtualScroll:100,
                    size:5
                });
                $('.dtpckr').datepicker({ autoclose: true, format: 'dd-mm-yyyy',});
            });
        </script>
        <script>
            $(window).on("scroll load" , function() {
                if ($(this).scrollTop() > 100){  
                    $('.header').addClass("fixed_hedr");
                    $('.logo_ch').find("img").attr('src', "{{ asset('public/images/system/logo.png') }}");
                }
                else {
                    $('.header').removeClass("fixed_hedr");
                    $('.logo_ch').find("img").attr('src', "{{ asset('public/images/system/logow.png') }}");
                    $('.innr_hedr .logo_ch').find("img").attr('src', "{{ asset('public/images/system/logo.png') }}");
                }
            });
        </script>
        @yield('scripts')
	</body>
</html>	