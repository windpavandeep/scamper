<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ PROJECT_NAME }} - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/bootstrap.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/magnific-popup.css') }}" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/slick-theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/bootstrap-select.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/bootstrap-datepicker.css') }}" />
    <link href="https://vjs.zencdn.net/7.7.5/video-js.css" rel="stylesheet" />
    <link href="{{ url( frontEndCssPath.'/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/custom.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndStudentCssPath.'/responsive-style.css') }}" />
   
   
    <?php
        $b_type = getBrowsere();
    ?>
    @if ($b_type['name'] == 'Internet Explorer' || $b_type['name'] == 'Unknown')
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/ie_style.css') }}">
    @endif 

    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P452Y7L8MH"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-P452Y7L8MH');
    </script>
    <script type="text/javascript">
        if(performance.navigation.type == 2){
           location.reload(true);
        }
    </script>

    <script type="text/javascript">
        window.onpageshow = function (event) {
          if (event.persisted) {
            window.location.reload(); //reload page if it has been loaded from cache
          }
        };
    </script>

    <?php
        function getBrowsere() 
        { 
            $u_agent = $_SERVER['HTTP_USER_AGENT']; 
            $bname = 'Unknown';
            $platform = 'Unknown';
            $version= "";
            $ub = '';

            //First get the platform?
            if (preg_match('/linux/i', $u_agent)) {
                $platform = 'linux';
            }
            elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                $platform = 'mac';
            }
            elseif (preg_match('/windows|win32/i', $u_agent)) {
                $platform = 'windows';
            }

            // Next get the name of the useragent yes seperately and for good reason
            if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Internet Explorer'; 
                $ub = "MSIE"; 
            } 
            elseif(preg_match('/Firefox/i',$u_agent)) 
            { 
                $bname = 'Mozilla Firefox'; 
                $ub = "Firefox"; 
            } 
            elseif(preg_match('/Chrome/i',$u_agent)) 
            { 
                $bname = 'Google Chrome'; 
                $ub = "Chrome"; 
            } 
            elseif(preg_match('/Safari/i',$u_agent)) 
            { 
                $bname = 'Apple Safari'; 
                $ub = "Safari"; 
            } 
            elseif(preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Opera'; 
                $ub = "Opera"; 
            } 
            elseif(preg_match('/Netscape/i',$u_agent)) 
            { 
                $bname = 'Netscape'; 
                $ub = "Netscape"; 
            } 

            // finally get the correct version number
            $known = array('Version', $ub, 'other');
            $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
            if (!preg_match_all($pattern, $u_agent, $matches)) {
                // we have no matching number just continue
            }

            // see how many we have
            $i = count($matches['browser']);
            if ($i != 1) {
                //we will have two since we are not using 'other' argument yet
                //see if version is before or after the name
                if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                    $version= $matches['version'][0];
                }
                else {
                    $version= $matches['version'][1];
                }
            }
            else {
                $version= $matches['version'][0];
            }

            // check if we have a number
            if ($version==null || $version=="") {$version="?";}

            $okkkk = array(
                'userAgent' => $u_agent,
                'name'      => $bname,
                'version'   => $version,
                'platform'  => $platform,
                'pattern'    => $pattern
            );
            // echo '<pre>'; print_r($okkkk); die;
            return $okkkk; 

        } 
    ?>
</head>
<body>
    <!-- <div class="loader">
        <img src="{{ url( 'public/images/system/lodr.gif') }}" class="img-resposnive">
    </div> -->
    @yield('content')
    
    <!-- scripts files -->
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/jquery-2.2.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/slick.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/waypoints.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/counterup.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/moment.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url( frontEndJsPath.'/toastr.min.js') }}"></script>
    <script src="{{ url( frontEndJsPath.'/jquery.validate.js') }}"></script> 
    <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
    <script src="https://vjs.zencdn.net/7.7.5/video.js"></script>
    <script type="text/javascript" src="{{ url( frontEndStudentJsPath.'/wow.js') }}"></script>
    
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3.0.0/es5/tex-mml-chtml.js"></script>
    
    

    @include('frontEnd.common.notifications')

<script type="text/javascript">
    $(document).ready(function() {
        $('.niceselc').selectpicker();
        $('.dtpckr').datepicker();
    });
</script>


<script type="text/javascript">
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
</script>

<script type="text/javascript">
    $('#homeSlider').slick({
          dots: true,
          infinite: true,
          speed: 300,
          autoplay: true,
            autoplaySpeed: 2000,
          slidesToShow: 1,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
              }
            }
          ]
        });
</script>

<script>
    wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
    });
    wow.init();
</script>

<script>
    $('.closd').hide();
    $('.page_divider').removeClass('added');
    $('.opend').click(function(){
        $(this).hide();
        $('.closd').show();
        $('.page_divider').addClass('added');
    });
    $('.closd').click(function(){
        $(this).hide();
        $('.opend').show();
        $('.page_divider').removeClass('added');
    });
</script>

<script type="text/javascript">
    $( document ).ready( function () {
        $( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
            var $el = $( this );
            var $parent = $( this ).offsetParent( ".dropdown-menu" );
            if ( !$( this ).next().hasClass( 'show' ) ) {
                $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
            }
            var $subMenu = $( this ).next( ".dropdown-menu" );
            $subMenu.toggleClass( 'show' );
            
            $( this ).parent( "li" ).toggleClass( 'show' );

            $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
                $( '.dropdown-menu .show' ).removeClass( "show" );
            } );
            
             if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
                $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
            }

            return false;
        } );
    } );
</script>
    
<script type="text/javascript">
    $(document).ready(function(){
        $('.loader').hide();
    })
</script>
    
@yield('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.niceselc').selectpicker({
            liveSearch:true,
            virtualScroll:100,
            size:5

        });
        $('.dtpckr').datepicker({ autoclose: true, format: 'dd-mm-yyyy',});
    });
</script>

</body>
</html>