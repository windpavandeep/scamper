<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ PROJECT_NAME }} - @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/fontawesome-all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/slick-theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/developer.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/select2.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/jquery.timepicker.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/jquery.rateyo.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/multiselect.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/animate.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/jquery.mCustomScrollbar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/videopopup.css') }}" />

    <link href="{{ url( frontEndCssPath.'/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap-multiselect.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/datatables.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/magnific-popup.css') }}" />
    <?php
        $b_type = getBrowsere();
    ?>
    @if ($b_type['name'] == 'Internet Explorer' || $b_type['name'] == 'Unknown')
        <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/ie_style.css') }}">
    @endif 

    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url( frontEndCssPath.'/responsive-style.css') }}" />
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P452Y7L8MH"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-P452Y7L8MH');
    </script>
    <script type="text/javascript">
        if(performance.navigation.type == 2){
           location.reload(true);
        }
    </script>

    <script type="text/javascript">
        window.onpageshow = function (event) {
          if (event.persisted) {
            window.location.reload(); //reload page if it has been loaded from cache
          }
        };
    </script>

    <?php
        function getBrowsere() 
        { 
            $u_agent = $_SERVER['HTTP_USER_AGENT']; 
            $bname = 'Unknown';
            $platform = 'Unknown';
            $version= "";
            $ub = '';

            //First get the platform?
            if (preg_match('/linux/i', $u_agent)) {
                $platform = 'linux';
            }
            elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                $platform = 'mac';
            }
            elseif (preg_match('/windows|win32/i', $u_agent)) {
                $platform = 'windows';
            }

            // Next get the name of the useragent yes seperately and for good reason
            if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Internet Explorer'; 
                $ub = "MSIE"; 
            } 
            elseif(preg_match('/Firefox/i',$u_agent)) 
            { 
                $bname = 'Mozilla Firefox'; 
                $ub = "Firefox"; 
            } 
            elseif(preg_match('/Chrome/i',$u_agent)) 
            { 
                $bname = 'Google Chrome'; 
                $ub = "Chrome"; 
            } 
            elseif(preg_match('/Safari/i',$u_agent)) 
            { 
                $bname = 'Apple Safari'; 
                $ub = "Safari"; 
            } 
            elseif(preg_match('/Opera/i',$u_agent)) 
            { 
                $bname = 'Opera'; 
                $ub = "Opera"; 
            } 
            elseif(preg_match('/Netscape/i',$u_agent)) 
            { 
                $bname = 'Netscape'; 
                $ub = "Netscape"; 
            } 

            // finally get the correct version number
            $known = array('Version', $ub, 'other');
            $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
            if (!preg_match_all($pattern, $u_agent, $matches)) {
                // we have no matching number just continue
            }

            // see how many we have
            $i = count($matches['browser']);
            if ($i != 1) {
                //we will have two since we are not using 'other' argument yet
                //see if version is before or after the name
                if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                    $version= $matches['version'][0];
                }
                else {
                    $version= $matches['version'][1];
                }
            }
            else {
                $version= $matches['version'][0];
            }

            // check if we have a number
            if ($version==null || $version=="") {$version="?";}

            $okkkk = array(
                'userAgent' => $u_agent,
                'name'      => $bname,
                'version'   => $version,
                'platform'  => $platform,
                'pattern'    => $pattern
            );
            // echo '<pre>'; print_r($okkkk); die;
            return $okkkk; 

        } 
    ?>
</head>
<body>
    @yield('content')
    
    <!-- scripts files -->
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/jquery.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap-select.js') }}"></script> 
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/jquery.timepicker.js') }}"></script>
    <script type="text/javascript" src="{{ url(frontEndJsPath.'/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/jquery.magnific-popup.min.js') }}"></script>  
    <script src="{{ url( frontEndJsPath.'/toastr.min.js') }}"></script>
    <script src="{{ url( frontEndJsPath.'/jquery.validate.js') }}"></script> 
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/slick.js') }}"></script> 
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/datatables.js') }}"></script> 
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/bootstrap-datepicker.min.js') }}"></script>  
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/waypoints.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/counterup.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/wow.js') }}"></script>
    <script type="text/javascript" src="{{ url( frontEndJsPath.'/videopopup.js') }}"></script>
    
    @include('frontEnd.common.notifications')
    @include('frontEnd.common.counselling_modal')
    <div class="loader">
        <img src="{{ url( 'public/images/system/lodr.gif') }}" class="img-resposnive">
    </div>

    <!-- <script type="text/javascript">
        $(".nav-item, .dropdown").hover(
            function () {
                $('>.dropdown-menu', this).stop(true, true).fadeIn("fast");
            },
            function () {
                $('>.dropdown-menu', this).stop(true, true).fadeOut("fast");
            });
    </script> -->
    <script type="text/javascript">
        $(document).ready(function(){
            $('.loader').hide();
        })
    </script>
    
    @yield('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.niceselc').selectpicker({
                liveSearch:true,
                virtualScroll:100,
                size:5

            });
            $('.dtpckr').datepicker({ autoclose: true, format: 'dd-mm-yyyy',});
        });
    </script>

    <script type="text/javascript">
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    </script>

    <script type="text/javascript">
        $('#homeSlider').slick({
              dots: true,
              infinite: true,
              speed: 300,
              autoplay: true,
                autoplaySpeed: 2000,
              slidesToShow: 1,
              slidesToScroll: 1,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
    </script>
    <script>
        wow = new WOW({
            animateClass: 'animated',
            offset: 100,
            callback: function(box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        });
        wow.init();
    </script>
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(window).on("scroll load" , function() {
            if ($(this).scrollTop() > 100){  
                $('.header').addClass("fixed_hedr");
                $('.logo_ch').find("img").attr('src', "{{ asset('public/images/system/logo.png') }}");
            }
            else {
                $('.header').removeClass("fixed_hedr");
                $('.logo_ch').find("img").attr('src', "{{ asset('public/images/system/logow.png') }}");
                $('.innr_hedr .logo_ch').find("img").attr('src', "{{ asset('public/images/system/logo.png') }}");
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".course_tables").slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
            $("#testimonial-slider").slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
            $(".part_slick").slick({
                dots: false,
                arrows: false,
                speed: 10000,
                autoplay: true,
                autoplaySpeed: 0,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
                responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#hm_slider").slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2,
                  }
                },
                {
                  breakpoint: 481,
                  settings: {
                    slidesToShow: 1,
                  }
                }
              ]
            });
        })
    </script>    
</body>
</html>