@extends('frontEnd.layouts.master')
@section('title','Course Detail')
@section('content') 
<style type="text/css">
	.social_links li {
	display: inline;
	padding: 9px;
}

	.loader{
	z-index: 1051 !important;
}

/*#scamper_order_Modal {
	z-index: 0 !important;
}*/
/*
26_august
*/
.modal-body.scamper_order_Modalbody .scamper_order_Modalimg{
	display: block;
	margin: auto;
	box-shadow: 1px 0px 3px 3px gray;
}

.modal-body.scamper_order_Modalbody .scamper_order_Modal_h4{
	text-align: center; 
	display: block; 
	margin:10px 0px;
}

.modal-body.scamper_order_Modalbody .scamper_order_Modal_label{
	font-size: 13px;
	color:#0094de;
	font-weight: bold;
}

.modal-body.scamper_order_Modalbody .btn.btn-default.scamper_order_Modal_buttonFooter{
	background: linear-gradient(45deg, #13B3F7 0%, #0131ff 100%); 
	color: #fff; 
	border:none; 
	border-radius: 70px;
}
/*End*/
/*.outer-footer.product-share {
	float: right;
	position: absolute;
	top: 17px;
	right: 0;
}*/

.social_links {
	margin-top: 6px;
}
Paid
.course_Meta.course_Meta_right {
	margin-top: 0px;
}

/*.course_Meta h3 {
    width: 74%;
}*/
.social-share-links {
    margin-top: 17px;
    border: 1px solid #ddd;
    padding: 10px 0;
    width: 100%;
    margin-bottom: 15px;
    margin-top: 5px;
}
.link-share{
	font-weight: bold;
	padding-left: 8px;
}
.wrap_downloads.wrap_canvas.scamperskills_find{
	padding: 8px;
}
.img-zoom-div{
    /*height: 100vh;*/
    /*border: 1px solid black;*/
}
.wrap_downloads.wrap_canvas.scamperskills_find {
    width: 100%;
    /*margin-left: 11px;*/
}

.course_dtl_sec.dsfsf_scamper {
	position: relative;
	padding: 0px;
}
.course_tables .cors_body {  /* Param 9Aug-2019 */
	box-shadow: 1px 2px 5px 0px gray;
}
/*new zoom*/
/*new zoom*/
</style>
<!-- <script type="text/javascript" src="{{ url( frontEndJsPath.'/jqzoom.js') }}"></script> -->
<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!-- <script type="text/javascript" src="{{ url('public/frontEnd/js/imagezoom.js') }}"></script> -->
<link href="{{ url('public/frontEnd/css/drift-basic.css')}}" rel="stylesheet" type="text/css">
<script src="{{ url('public/frontEnd/js/Drift.js') }}"></script>

<div class="page-wrapper">
	@include('frontEnd.common.cms_header')
    <div class="home_page_wrapper inner_page">
    	<div class="container">
	    	<div class="wrap_downloads wrap_canvas scamperskills_find">
	            <div class="row">
	                <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
	                	<h6 style="margin-left: 10px;">
	                		<a href="{{url('/')}}" title="Homepage">Home
	                		</a> 
	                	
	            			/ <a href="{{url('courses?category_id='.$course_details['category_id'])}}">{{ 	ucfirst($course_details['category']['name']) }}</a>
	            		
	            	
	            			/ <a href="{{url('courses?sub_category_id='.$course_details['sub_category_id'])}}">{{	ucfirst($course_details['sub_category']['name']) }}</a>
	            	
	            		
	            			/ <a href="{{url('courses?sub_subcategory_id='.$course_details['sub_subcategory_id'])}}">{{	ucfirst($course_details['sub_sub_category']['name']) }}</a>
	                		/ {{ ucfirst($course_details['title']) }}
	                	</h6>

	                </div>
	            </div>
	        </div>
	    </div>
    	<section class="course_dtl_sec dsfsf_scamper">
    		<div class="container">
                <div class="content_detl">
                	<div class="row pos_rel">
                		<div class="col-sm-5 pos_sticky">
                			<div class="course_img">
                				<div class="slick_course" id="bzoom">
                					@foreach($course_details->course_images as $value)
                						<?php  
                							if (!empty($value['name'])) {
                								if (file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                									$image = TrainerContentImageImgPath.'/'.$value['name'];
                								}else{
                									$image = DefaultImgPath;
                								}
                							}else{
                								$image = DefaultImgPath;
                							}
                						?>
	                					<div class="item_crs">
	                						<!-- <img src="{{$image}}" class="img-fluid" alt="course"  data-imagezoom="true"> -->
	                						<img src="{{$image}}" class="img-fluid drift-demo-trigger" alt="course" data-zoom="{{$image}}">
	                					</div>
                					@endforeach
                				</div>
                				<div class="slick_thumb">
                					@foreach($course_details->course_images as $value)
                						<?php  
                							if (!empty($value['name'])) {
                								if (file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                									$image = TrainerContentImageImgPath.'/'.$value['name'];
                								}else{
                									$image = DefaultImgPath;
                								}
                							}else{
                								$image = DefaultImgPath;
                							}
                						?>
                						<div class="item_thumb"><img src="{{$image}}" class="img-fluid" alt="course"></div>
                					@endforeach
                				</div>
                			</div>
                		</div>
                		<div class="col-sm-7 col-sm-7 col-sm-7 col-xs-12">
                			<div class="course_Meta">
	                				<div class="zomer"></div>
                					<div class="course-1">
                						<div class="course-in">
		                					<h3>{{ ucfirst(@$course_details->title) }}</h3>
		                					<p class="meta_price">
		                					@if(@$course_details->content_availability=='free')
		                						Free
		                					@else
		                						₹{{number_format(@$course_details->final_price) }}
		                					@endif
		                					</p>
	                					</div>
	                					<div class="outer-footer product-share">
										    <div class="social-share-links ">
										        <a class="link-share">Share</a>
										        <ul class="social_links" type="none">
										            <?php
										                $current_url= Request::fullUrl();
										                // dd($current_url)
										                // echo '<pre>'; print_r($current_url); die;
										                //$twitter    = "http://twitter.com/share?text=Im Sharing on Twitter&url=".$current_url."&hashtags=Scamper Skills";
										                $twitter    = "http://twitter.com/share?url=".$current_url."&hashtags=Scamper Skills";
										                $google     = "https://plus.google.com/share?url=".$current_url;

										                $pin_img    = TrainerContentImageImgPath.'/'.$course_details['course_image']['name'];
										                $pinterest  = "http://pinterest.com/pin/create/button/?url=".$current_url."&media=".$pin_img."&description=Scamper Skills";
										                //fb
										                $share_url = url('https://www.facebook.com/sharer/sharer.php'.'?u='.$current_url);
										            ?>
										            <li>
													<a href="javascript:;" class="fb"  copy_url="{{URL::current()}}" id="copy_url" data-toggle="tooltip" data-placement="bottom" title="Copy">
										            		<img src="{{ url( systemImagePath.'/copy.png') }}" width="25" height="25">
										            	</a>
										            </li>
										            <li><a href="javascript:;" onClick="fbpopup('<?php echo $share_url; ?>')" class="fb" data-toggle="tooltip" data-placement="bottom" title="Share On Facebook"><img src="{{ url( systemImagePath.'/facebook.png') }}" width="25" height="25"></a></li>

										            <li><a href="javascript:;" onClick="fbpopup('<?php echo $pinterest ?>')"><img src="{{ url( systemImagePath.'/pinterest.png') }}" width="25" height="25" data-toggle="tooltip" data-placement="bottom" title="Share On Pinterest"></i></a></li>

										            <li><a href="javascript:void(0)" onClick="fbpopup('<?php echo $twitter ?>')" class="tw"><img src="{{ url( systemImagePath.'/twiter.png') }}" width="25" height="25" data-toggle="tooltip" data-placement="bottom" title="Share On Twitter"></a></li>

										           <!--  <li><a href="javascript:void(0)" onClick="fbpopup('<?php echo $google ?>')" class="go"><img src="{{ url( systemImagePath.'/google-plus.png') }}" width="25"></a></li> -->
										            <!-- <li><a href="javascript:;" class="ins"><img src="{{ url( systemImgPath.'/instagram.png') }}" width="25"></a></li> -->

										        </ul>											  
			                				</div>
			                			</div>
			                			<ul class="meta_auth bb1" type="none">
	                					<!-- <li>Availability: <strong> In stock </strong></li>
	                					<li>Author: <strong> R S Aggarwal </strong></li> -->
	                					</ul>
			                		</div>
                				
	                				<div class="course-1">
                						<div style="font-weight: bold;font-size: 20px;">Course Description</div>
                						<p class="desc_crs">{!! ucfirst(@$course_details->description)  !!}</p>
		                    			<div class="btns_adby text-left bb1 " id="purchased">

											<?php

											    if(@$course_details->upload_type=='pdf'){
											        $title = 'Download PDF';
											    }else{
											        $title ='Download Video';
											    }
											    // $image = DefaultImgPath; 
											    $file     = '';
											    $file_ext = '';
											    $file_url = 'javascript:;';
											    if(!empty($course_details->file)) {

											        if(file_exists(TrainerContentBasePath.'/'.$course_details->file)) {
											            $file_url = TrainerContentImgPath.'/'.$course_details->file;
											            $file_name= pathinfo($course_details->file);
							                            $file_ext = $file_name['extension'];
											        }
											    // dd($file_url);
											    }  
											    // dd($trainer_files);
											?>
											@if(!empty($check_user_course_exist))
												<div style="margin-bottom:15px;margin-top:15px">
													<div class="col-md-4">Download Course
															
															<p>
															@if(empty($trainer_files))
														   		@foreach($course_details->course_files as $key=>$value)
														   			<?php
														   				$file_url = 'javascript:;';
														   				$file_type= ''; 
														   				if(!empty($value['file'])) {

														   				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
														   				        $file_url = TrainerContentImgPath.'/'.$value['file'];
														   				        $file_name= pathinfo($value['file']);
						                                                        $ext      = $file_name['extension'];
						                                                        if($ext=='pdf'){
						                                                        	$file_type = 'pdf';
						                                                        }else{
						                                                        	$file_type = 'video';
						                                                        }
														   				    }
														   				}
														   			?>
														   			@if(@$ext=='pdf')
														   				({{ $key+1 }}). Pdf
														   				<a href="{{$file_url}}"  target="_blank">

														   					<i class="fas fa-file-pdf"></i>
														   					
														   				</a>
														   			@else
														   				({{$key+1}}). Video
														   				<a href="{{$file_url}}"  target="_blank">
														   					<i class="fas fa-video"></i>
														   				</a>
														   			@endif

														   		@endforeach
													   		@else
													   			<a href="{{$file_url}}"  target="_blank">
																<i data-toggle="tooltip" title="{{$title}}" class="fas fa-download cp" data-original-title="{{$title}}"></i></a>
													   		@endif
													   	</p>
													
													</div>
												</div>
												<a href="javascript:;" class="btn btn_gradient btn_active" disabled><i class="fa fa-cart"></i> Purchased</a>
											@else                    				
			                    				@if(@$course_details->content_availability!='free')
												<a href="javascript:;" class="btn btn_gradient btn_active" id="buy_button" <?php if(!(@Auth::check())){
														Session::put('course_url',URL::current()); }?><i class="fa fa-cart"></i> Buy Now
												</a>
												@else
													<div style="margin-bottom:15px;margin-top:15px">
														<div class="">Download Course
															<div class="col-sm-7">

																@if(empty($course_details->file))
															   		@foreach($course_details->course_files as $key=>$value)
															   			<?php
															   				$file_url = 'javascript:;';
															   				$file_type= ''; 
															   				if(!empty($value['file'])) {

															   				    if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
															   				        $file_url = TrainerContentImgPath.'/'.$value['file'];
															   				        $file_name= pathinfo($value['file']);
							                                                        $ext      = $file_name['extension'];
							                                                        if($ext=='pdf'){
							                                                        	$file_type = 'pdf';
							                                                        }else{
							                                                        	$file_type = 'video';
							                                                        }
															   				    }
															   				}
															   			?>
															   			@if(@$file_type=='pdf')
															   				({{$key+1}}). Pdf
															   				<a href="{{$file_url}}"  target="_blank">
															   					<i class="fas fa-file-pdf"></i>
															   				</a>
															   			@else
															   			
															   			({{$key+1}}). Video
															   				<a href="{{$file_url}}"  target="_blank">
															   					<i class="fas fa-video"></i>
															   				</a>
															   			@endif

															   		@endforeach
															</div>
														   	@else
												   			<a href="{{$file_url}}"  target="_blank">
												   				@if($file_ext=='pdf')
												   					(1). pdf
																	<i data-toggle="tooltip" title="{{$title}}" class="fas fa-file-pdf" data-original-title="{{$title}}"></i>
																@else
																	(1). video
																	<i data-toggle="tooltip" title="{{$title}}" class="fas fa-video" data-original-title="{{$title}}"></i>	
																@endif
															</a>
													   		@endif
														   	</p>
														</div>
													</div>
												</div>
												@endif
											@endif
											<!-- <div class="outer-footer product-share">
											    <div class="social-share-links ">
											        <a class="link-share">Share</a>
											        <ul class="social_links" type="none">
											            <?php
											                $current_url= Request::fullUrl();
											                // dd($current_url)
											                // echo '<pre>'; print_r($current_url); die;
											                //$twitter    = "http://twitter.com/share?text=Im Sharing on Twitter&url=".$current_url."&hashtags=Scamper Skills";
											                $twitter    = "http://twitter.com/share?url=".$current_url."&hashtags=Scamper Skills";
											                $google     = "https://plus.google.com/share?url=".$current_url;

											                $pin_img    = TrainerContentImageImgPath.'/'.$course_details['course_image']['name'];
											                $pinterest  = "http://pinterest.com/pin/create/button/?url=".$current_url."&media=".$pin_img."&description=Scamper Skills";
											                //fb
											                $share_url = url('https://www.facebook.com/sharer/sharer.php'.'?u='.$current_url);
											            ?>
											            <li>
														<a href="javascript:;" class="fb"  copy_url="{{URL::current()}}" id="copy_url" data-toggle="tooltip" data-placement="bottom" title="Copy">
											            		<img src="{{ url( systemImagePath.'/copy.png') }}" width="25" height="25">
											            	</a>
											            </li>
											            <li><a href="javascript:;" onClick="fbpopup('<?php echo $share_url; ?>')" class="fb" data-toggle="tooltip" data-placement="bottom" title="Share On Facebook"><img src="{{ url( systemImagePath.'/facebook.png') }}" width="25" height="25"></a></li>

											            <li><a href="javascript:;" onClick="fbpopup('<?php echo $pinterest ?>')"><img src="{{ url( systemImagePath.'/pinterest.png') }}" width="25" height="25" data-toggle="tooltip" data-placement="bottom" title="Share On Pinterest"></i></a></li>

											            <li><a href="javascript:void(0)" onClick="fbpopup('<?php echo $twitter ?>')" class="tw"><img src="{{ url( systemImagePath.'/twiter.png') }}" width="25" height="25" data-toggle="tooltip" data-placement="bottom" title="Share On Twitter"></a></li>

											           <!--  <li><a href="javascript:void(0)" onClick="fbpopup('<?php echo $google ?>')" class="go"><img src="{{ url( systemImagePath.'/google-plus.png') }}" width="25"></a></li> -->
											            <!-- <li><a href="javascript:;" class="ins"><img src="{{ url( systemImgPath.'/instagram.png') }}" width="25"></a></li> -->

											        </ul>
											    </div>
											<!-- </div>  -->
											<!-- <a href="javascript:;" class="btn btn_gradient btn_active"><i class="fa fa-heart"></i> </a> -->
										</div>
									</div>
								@if(!empty($faqs))
									<div class="iner_crs">
										<h3><i class="fas fa-question"></i> FAQ</h3>
										<div class="faq_sec">
											<!--  -->
											@foreach($faqs as $value)
												<div class="quest_wrp">
													<h6>{{ucfirst($value['title'])}}</h6>
													<p><strong>Ans. </strong>{{ucfirst($value['description'])}}</p>
												</div>
											@endforeach
										</div>
									</div>
								@endif
                			</div>
						</div>
					</div>
				</div>
    		</div>
    	</section>
    	<!-- course details -->
    	@if(empty($trainer_content_page))
	    	@if(!empty($suggested_trainers))
		    	<section class="cors_sec">
					<div class="container">
				        <div id="cors_id" class="standard-section section-gray">
				            <div class="container">
				            	<div class="sec_heading text-center">
		            				<h2>Related Courses</h2>
	            					<p class="divider">
	            						<img src="{{ asset('/public/images/system/secdivider.png') }}" class="img-fluid" alt="divider">
	            					</p>
		            			</div>
				                <div class="row">
				                    <div class="col-sm-12">
				                        <div class="course_tables">

				                        	<?php foreach ($suggested_trainers as $key => $value) {
				                        		
				                        		$key = $key+1;
				                        		$fade_right_cls = 'fadeInRight';
				                        		
				                        		if($key%2==0){
				                        			$fade_right_cls = '';
				                        		}
				                        		$image = '';
		                    				    if (!empty($value['course_image']['name'])) {
    	                            		        // dd($student_details->image);
    	                            		        if (file_exists(TrainerContentImageBasePath.'/'.$value['course_image']['name'])) {
    	                            		            $image = TrainerContentImageImgPath.'/'.$value['course_image']['name'];
    	                            		        }else{
    	                            		            $image = DefaultImgPath;
    	                            		        }
    	                            		    }else{
    	                            		        $image = DefaultImgPath;
    	                            		    }
    		                            		$courses__id = base64_encode($value['id'].'-'.$value['trainer_id']);
				                        	?>
					                            <div class="single_cors  wow {{$fade_right_cls}}" data-wow-delay=".2s">
					                                <div class="cors_head">
					                                    <!-- <h2 class="cors_title">{{@$value['sub_category']['name']}}</h2> -->
					                                </div>
					                                <div class="cors_body">
					                                	<img src="{{$image}}" class="my_img">
					                                    <p>{{mb_strimwidth(ucfirst($value['title']), 0, 35, "...")}}</p>
					                                </div>
					                                <div class="cor_price">
					                                	@if(@$value['content_availability']=='paid')
						                                	<i class="fas fa-rupee-sign">
						                                		<span>{{$value['final_price']}}</span>			                                		
						                                	</i>
					                                	@else
					                                		<span style="font-family: 'Font Awesome 5 Free';font-weight: 900;">Free</span>		
					                                	@endif
					                                </div>
					                                <div class="cors_footer">
					                                	
					                                    <a href="{{url('/course/detail/'.@$courses__id)}}" class="btn btn_gradient btn_active" data-animation="fadeInLeft" data-delay="900ms"> Buy your Plan</a>
					                                </div>
					                            </div>
				                            <?php } ?>
				   
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
					</div>
				</section>
				
			@endif
		@endif
		@include('frontEnd.common.offer')
    	@include('frontEnd.common.top_footer')
    	<div class="container scamper_order_container">
    	  <!-- Modal -->

    	  <div class="modal fade" id="scamper_order_Modal" role="dialog">

    	    <div class="modal-dialog modal-xs">
    	      	<div class="modal-content scamper_order_Modalcontent offer_pop ">
	    	      	<div class="offer_close">
	  	                <button type="button" class="close" data-dismiss="modal">×</button>
	  	            </div>
    	     <!--    <div class="modal-header"> -->
    	         <!--  <button type="button" class="close" data-dismiss="modal">&times;</button> -->
    	         <!--  <h4 class="modal-title">Scamper Skills</h4>
    	        </div> -->
    	        @foreach($course_details->course_images as $value)
					<?php  
						if (!empty($value['name'])) {
							if (file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
								$image = TrainerContentImageImgPath.'/'.$value['name'];
							}else{
								$image = DefaultImgPath;
							}
						}else{
							$image = DefaultImgPath;
						}
					?>
					
				@endforeach
    	        <div class="modal-body scamper_order_Modalbody">
    	         <img class="scamper_order_Modalimg" src="{{$image}}" alt="" class="img-fluid" width="150px" height="150px">
    	         <hr>
    	          <h4 class="scamper_order_Modal_h4"> <b>{{ ucfirst($course_details->title)}} </b></h4>
    	        

    	         <div class="row form-group">
    	         	<div class="col-sm-6">
    	         		<label class="scamper_order_Modal_label" > <b> Refferel Code : </b> </label>
    	         		 <input type="" class="form-control" id="rc_data">      
    	         		
    	         		 		  
    	         	</div>
    	         	<div class="col-sm-6">
    	         		<label class="scamper_order_Modal_label">Promo Code : </b> </label>
    	         		  <input type="" class="form-control" id="pc">        	
    	         		  <span id="promo_code_eror" class="error1" style="font-size: 13px;"></span>           		 
    	         	</div>         	
    	         </div>  

    	        <div class="row ">
    				<div class="col-sm-6">
    					<label class="scamper_order_Modal_label" id="price"><b>Price : </b> </label>
    					
    				</div>  

    	         	<div class="col-sm-6">
    	         		
    	         			<p class="scamper_order_Modal_p">+ 	
    	         				{{CURRENCY}}{{$course_details->paid_amount}} 
    	         			</p>  
    	         		
    	         	</div>
    	    
    				<div class="col-sm-6">
    					<label class="scamper_order_Modal_label"><b> GST({{$course_details->gst}}%): </b> </label>
    				</div>  

    	         	<div class="col-sm-6">
    	         		
    	         			<p class="scamper_order_Modal_p"> + {{CURRENCY}}{{$course_details->gst_price}} </p>  
    	         		
    	         	</div>

    	         		<div class="col-sm-6">
    					<label class="scamper_order_Modal_label"><b>Promotional Cost : </b> </label>
    				</div>  

    	         	<div class="col-sm-6">
    	         		
    	         			<p id="promotional_cost"  promo_discount=""> - {{CURRENCY}} 0 </p>  
    	         		
    	         	</div>


	         		<div class="col-sm-6">
						<label class="scamper_order_Modal_label" >
							<b>Total Cost : </b>
						</label>
					</div>  

    	         	<div class="col-sm-6">
    	         		
    	         		<p id="total_cost" total_cost="{{$course_details->final_price}}"> {{CURRENCY}}{{$course_details->final_price}} </p>  
    	         		
    	         	</div>
    	         </div>

    	        <div>      
    	       
    	        <div class="modal-footer">
    	          <button class="btn btn-default scamper_order_Modal_buttonFooter" type="button">Submit</button>
    	        </div>
    	      </div>
    	    </div>
    	  </div>
    	</div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var pop_up_cookie = $.cookie("pop_up");
		// var abc = $.cookie("test");
		// alert(pop_up_cookie);
		if(pop_up_cookie==null){
			$('#offer_modal').modal('show');
			// $.cookie("test", 1, { expires : 1 });
		 	$.cookie("pop_up", 1, { expires : 1 });
		}
	});
	
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
    function call_rzor_pay(course_id,total_cost,promo_code,referral_code,gst,discount_amount){
        
        price       = total_cost*100;
        var options = {
            "key": "rzp_test_QYhZB5gAfNTMNX",
            "amount":price,  
            "name":"Scamper Skills",
            "currency":"INR",

            // "description":"Total Amount: "+total_amount+', '+"GST : "+gst+'%',
            "image": "http://scamperskills.com/public/images/system/logo.png",
            "handler": function (response){
                // alert()
                if(response.razorpay_payment_id != ''){
                    $('.loader').show();
                    $.ajax({
                        method:"post",
                        url:"{{ url('/user/course/pay') }}",
                        data:{
                        	price:price, 
                        	course_id:course_id,
                        	discount_coupon:promo_code,
                        	refer_code     :referral_code,
                        	gst:gst,
                        	discount_amount:discount_amount,
                        	razorpay_payment_id:response.razorpay_payment_id, 
                        	"_token":"{{ csrf_token() }}" },
                        success:function(resp){
                            // alert(resp);
                            if(resp == 'false'){
                                swal("Oops!","Something went wrong. Please try again later,","error");     
                            } else{
                            	if(resp!=''){
                            		
                            		$('#purchased').html(resp);
                            		$('#scamper_order_Modal').modal('hide');
                                	swal('Course Purchased',"Course purchased successfully","success"); 
                            	}else{
                            		swal("Oops!","Something went wrong. Please try again later,","error");
                            	}
                            }
                            $('.loader').hide();
                        }
                    })
                } else{
                    swal("Oops!","Something went wrong. Please try again later",'error');     
                }
            },
            "theme": {
                "color": "#0449fe"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
    }

    $('#buy_button1').click(function(e){

        var checkAuth = "{{(Auth::check())?'true':'false'}}";
        var user_type = "{{@Auth::User()->user_type}}";
        // alert(user_type);
        if(checkAuth=='true'){
        	if(user_type=='user'){
        		var course_id      = "{{@$course_id}}";
        		var price          = "{{@$course_details->final_price}}";
        		var gst            = "{{@$course_details->gst}}";
        		var total_amount   = "{{@$course_details->paid_amount}}";
        		var user_id        = "{{@Auth::user()->id}}";
       			call_rzor_pay(price,course_id,gst,total_amount);
        	}else{
	    		swal({
		            title: "Not Authorized",
		        	text: "You are not authorized to purchase this course.",
		        	icon: "warning",
		        	buttons: true,
		        	dangerMode: true,
		        });
        	}
        }else{
        	window.location.href = "{{url('/login')}}";
        }
    });
</script>
<script type="text/javascript">
/*	$('.slick_course').slick({
	 	slidesToShow: 1,
	 	slidesToScroll: 1,
	 	arrows: false,
	 	fade: false,
	 	asNavFor: '.slick_thumb',
	 });

	 $('.slick_thumb').slick({
	 	slidesToShow: 4,
	 	slidesToScroll: 1,
	 	asNavFor: '.slick_course',
	 	dots: false,
	 	arrows: false,
	 	focusOnSelect: true,
	 	// responsive: [
	 	// 	{
   //            breakpoint: 991,
   //            settings: {
   //              slidesToShow: 3,
   //            }
   //          },
   //          {
   //            breakpoint: 768,
   //            settings: {
   //              slidesToShow: 2,
   //            }
   //          },
   //          {
   //            breakpoint: 481,
   //            settings: {
   //              slidesToShow: 0,
   //            }
   //          }
   //        ]
	 });*/
	 $("#fac-slider").slick({
    	// dots: false,
    	arrows: true,
		infinite: false,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		// responsive: [
		//     {
		//       breakpoint: 768,
		//       settings: {
		//         slidesToShow: 2,
		//       }
		//     },
		//     {
		//       breakpoint: 481,
		//       settings: {
		//         slidesToShow: 1,
		//       }
		//     }
		//   ]
    });
</script>
<script>
	function fbpopup(popwhat) {
	    window.open( popwhat, "fbshare", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" )
	} 
</script>
<script>
	$('#copy_url').hover(function(){
		$(this).tooltip('hide')
		$(this).attr('data-original-title', 'Copy Link');
		$(this).tooltip('show');
	});
</script>
<script> 
	$('#copy_url').on('click',function(e){
		
		e.preventDefault();
        var copyText = $(this).attr('copy_url');
		
   		document.addEventListener('copy', function(e) {
      		e.clipboardData.setData('text/plain', copyText);
      		e.preventDefault();
   		}, true);
		document.execCommand('copy');
		$(this).tooltip('hide')
		$(this).attr('data-original-title', 'Link Copied');
		$(this).tooltip('show'); 
	});
</script>
<script>
	new Drift(document.querySelector('.drift-demo-trigger'), {
		paneContainer: document.querySelector('.zomer'),
		inlinePane: 600,
		inlineOffsetY: -85,
		containInline: true,
		hoverBoundingBox: true
	});
</script>
<script type="text/javascript">
	$('#buy_button').click(function(){
		var checkAuth = "{{(Auth::check())?'true':'false'}}";
        var user_type = "{{@Auth::User()->user_type}}";
        if(checkAuth=='true'){
        	if(user_type=='user'){


				$('#scamper_order_Modal').modal('show');
				$('#pc').val('');
				$('#promo_code_eror').html('');
        // alert(user_type);
        		
       			// call_rzor_pay(price,course_id,gst,total_amount);
        	}else{
	    		swal({
		            title: "Not Authorized",
		        	text: "You are not authorized to purchase this course.",
		        	icon: "warning",
		        	buttons: true,
		        	dangerMode: true,
		        });
        	}
        }else{
        	window.location.href = "{{url('/login')}}";
        }
	});
</script>
<!-- <script type="text/javascript">
	$('#payment').click(function(){
		var value      = $('#discount_coupon').val();
		var refer_code = $('#referral_code').val();
		if(refer_code==''){
			refer_code = '';
		}
		var course_id  = "{{@$course_id}}";
		if(value!=''){

			$.ajax({
			    method:"post",
			    url:"{{ url('/student/validate/discount-coupon') }}",
			    data:{
			    	coupon_code:value,
			    	course_id:course_id,
			    	"_token":"{{ csrf_token() }}"
			    },
			    success:function(resp){
			        if(resp == 'false'){
			            swal("Oops!","Something went wrong. Please try again later,","error");     
			        } else{
			        	
			        		call_rzor_pay(price,course_id,value,resp.discount_amount,refer_code);
			        		// $('#purchased').html(resp);
			            	// swal('Course Purchased',"Course purchased successfully","success"); 
			        /*	}else{
			        		swal("Oops!","Something went wrong. Please try again later,","error");
			        	}*/
			        }
			    }
			});
		}else{

			call_rzor_pay(price,course_id,value,discount_amount,refer_code);
		}
	});
</script> -->
<script type="text/javascript">
	$('#pc').on('input',function(){
		var discount_coupon = $(this).val();

		var course_id  = "{{@$course_id}}";

		$('.loader').show();
		if(discount_coupon!=''){

			$.ajax({
			    type:"post",
			    url:"{{ url('/student/validate/discount-coupon') }}",
			    data:{
			    	coupon_code:discount_coupon,
			    	course_id:course_id,

			    },
			    success:function(resp){
			     
			    	
			        if(resp == 'false'){
			        	$('#promo_code_eror').html('Invalid promo code.');
			            /*swal("Oops!","Something went wrong. Please try again later,","error");*/

			        } else{
			        	if(resp!=''){
			        		// alert(resp.final_total);
			        		$('#promo_code_eror').html('');
			        		$('#promotional_cost').attr('promo_discount',resp.discount_amount);
			        		$('#promotional_cost').html("-"+" "+"{{CURRENCY}}"+resp.discount_amount);
			        		$('#total_cost').html("{{CURRENCY}}"+resp.final_total);
			        		$('#total_cost').attr('total_cost',resp.final_total);
			        		// $('#purchased').html(resp);
			        		// $('#price').html("{{CURRENCY}}"+resp.price);
			            	// swal('Course Purchased',"Course purchased successfully","success"); 
			            	
			        	}else{
			        		swal("Oops!","Something went wrong. Please try again later,","error");
			        	}
			        }
			        $('.loader').hide();
			    }
			});
		}else{
			$('.loader').hide();
			$('#promo_code_eror').html('');
			$('#promotional_cost').html("{{CURRENCY}}"+0);
			$('#total_cost').html("{{CURRENCY}}"+"{{$course_details->final_price}}");
			$('#total_cost').attr('total_cost',"{{$course_details->final_price}}");
			
		}
	});
</script>
<script type="text/javascript">
	$('.scamper_order_Modal_buttonFooter').click(function(){
		var promo_code_error = $('#promo_code_eror').html();
		if(promo_code_error==''){

			var course_id      = "{{@$course_id}}";
			var promo_code     = $('#pc').val();
			var refferal_code  = $('#rc_data').val();
			var total_cost     = $('#total_cost').attr('total_cost');
			var gst            = "{{@$course_details->gst}}";
			var discount_amount= $('#promotional_cost').attr('promo_discount'); 
			call_rzor_pay(course_id,total_cost,promo_code,refferal_code,gst,discount_amount);
		}
	});
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQhN-xkQiUIQ9toO-KRdb9wqtc_cGbAqo&libraries=places&callback=initMap">
    </script>
<script type="text/javascript">
    
    // var map, infoWindow;
    function initMap() {
        // Try HTML5 geolocation.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                  lat: position.coords.latitude,
                  lng: position.coords.longitude
                };

                // alert(pos.lat);
                // alert(pos.lng);
                var geocoder = new google.maps.Geocoder;
                var current_state = getLatLng(geocoder,pos.lat,pos.lng);
                // return current_state;
          });
        }   
    }
    function getLatLng(geocoder,latitude,longitude) {
        var latlng = {lat:latitude, lng:longitude};
        var componentForm = {
        	postal_code: 'short_name'
        };
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) {
                var val =[];
                for (var i = 0; i<results[1]['address_components'].length; i++) {
                    var addressType = results[1]['address_components'][i]['types'][0];
                    
                    if (componentForm[addressType]) {
                        var postal_code='';
                        postal_code =results[1]['address_components'][i][componentForm[addressType]];
                       
                        if (addressType == 'postal_code') {
                        	
                        	if(postal_code==''){
                        		swal("Oops!","Please allow the location service","error");  
                        	}else{
                        		// alert(postal_code);
                        		$.ajax({
                        		    type:'get',
                        		    url: "{{ url('/student/pincode') }}"+"/"+postal_code,
                        		    
                        		});
                        	}
                        }
                    }

                }
                // $.ajax({
                //     type:'post',
                //     url: "{{ url('/user/location') }}",
                //     data:{
                //         location:val,
                //         latlng:latlng,
                //         _token:"{{csrf_token()}}",
                //     }
                // });
            }
          }
        });
    }
</script>
@stop