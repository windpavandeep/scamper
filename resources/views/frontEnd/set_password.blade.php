@extends('frontEnd.layouts.login')
@section('title','Set Password')
@section('content')
    <style type="text/css">
        .form_head div > .img-fluid {
            width: 70%;
            margin: 30px 0 40px;
        }
    </style>
    <div class="page-wrapper">
        @include('frontEnd.common.cms_header')
        <div class="home_page_wrapper inner_page">
            <section class="register_sec user_mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 offset-3">
                            <div class="wrp_white">
                                <div class="sec_heading text-center">
                                    <h2>Set Password</h2>
                                    <p class="divider"><img src="{{asset(systemImgPath.'/secdivider.png')}}" class="img-fluid" alt="divider"></p>
                                </div>
                                <div class="form_head">
                                    <div class="text-center">
                                        <img src="{{asset(systemImgPath.'/logo.png')}}" class="img-fluid">
                                    </div>
                                    <!-- <h5 class="text-center">Enter your Valid Mobile Number</h5> -->
                                    <form method="post" id="set-password" action="{{url('/set-password') }}">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" placeholder="Enter Email or Number" value="{{ @$user_email }}" disabled="">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="New Password" name="password" value="" id="password">
                                            
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password" id="confirm_password">
                                        </div>
                                        <div class="form-group">
                                            <div class="text-center log_btn">
                                                @csrf
                                                <input name="user_id" type="hidden" value="{{ @$user_id }}">
                                                <input name="security_code" type="hidden" value="{{ @$security_code }}">
                                                <button class="btn btn_gradient btn_active">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @include('frontEnd.common.top_footer')
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#set-password').validate({ 
        rules:{
            password:{
                required:true,
                // minlength:6,
                // maxlength:20,
                regex:/^[a-zA-Z 0-9@./#,-_]+$/
            },
            confirm_password:{
                required:true,
                equalTo: '#password'
            }
        },
        messages:{
            password:{
                regex:"Password can only consist of 'a-z,0-9,@./#-_'"
            },
            confirm_password:{
                equalTo: 'Please enter the same password again',
            },
        }
    });
</script>
@endsection