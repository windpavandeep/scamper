@extends('backEnd.layouts.master')
@section('title','Retailer Detail')
@section('content')
<style type="text/css">
    .ck{
        margin-top: 8px !important;
    }
    .reply_field{
        margin-left: 10px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Retailer Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/retailers')}}">Retailer Request</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Retailer Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-body">
                        <h4 class="card-title">Retailer Detail</h4>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{ucfirst(@$franchise_details['name'])}}
                                </label>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email: </label>
                            <div class="col-sm-8">
                                
                                <label class="col-sm-8 ck">
                                    {{@$franchise_details['email']}}
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{@$franchise_details['contact_no']}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Address: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{ucfirst(@$franchise_details['address'])}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Country: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                
                                    {{@$franchise_details['country']['name']}}
                                </label>
                            </div>
                        </div>
                        
                        <?php if(!empty($franchise_details['state']['name'])){ ?>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">State: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{@$franchise_details['state']['name']}}
                                </label>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">City: </label>
                            
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{@$franchise_details['city']['name']}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Pin Code: </label>
                            
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{@$franchise_details['pin_code']}}
                                </label>
                            </div>
                        </div>
                        <?php if(!empty($franchise_details['edu_detail'])) { ?>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label"> Experience in Education In: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-10 ck">
                                    {{ucfirst(@$franchise_details['edu_detail'])}}
                                </label>
                            </div>
                        </div>
                        <?php } ?>
                        
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label"> Query: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-10 ck">
                                    {{ucfirst(@$franchise_details['query_desc'])}}
                                </label>
                            </div>
                        </div>
                        @if(!empty($contact_us_details['reply']))
                            <div class="form-group row reply">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Previous Reply: </label>
                                <div class="col-md-8">
                                    <label class="col-sm-12 ck">{{ ucfirst(@$contact_us_details->reply) }}</label>
                                </div>
                            </div>
                        @endif
                        <!-- <div class="form-group row reply ">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Reply: </label>
                            <div class="col-sm-8">
                                <textarea type="text" class="form-control reply_field" id="fname12" placeholder="Reply" rows="8" name="reply"></textarea>
                            </div>
                        </div> -->
                    </div>
                        <!-- <div class="border-top">
                            <div class="card-body">
                                
                                @csrf
                                <button class="btn btn-primary" style="float: right;margin-bottom: 20px">
                                    Submit
                                </button>
                            </div>
                        </div> -->
                    
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    