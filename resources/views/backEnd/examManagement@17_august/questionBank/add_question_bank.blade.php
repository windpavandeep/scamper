@extends('backEnd.layouts.master')
@section('title','Add Question Bank')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<style type="text/css">
    .card-link {
        padding: 0;
        font-size: 22px;
    }
    .form-check.radio_rell {
        display: inline-block;
        margin-right: 20px;
    }
    .form-check.radio_rell .form-check-input {
        margin-top: 0.1rem;
    }
    .btn.btn-primary {
        color: #fff!important;
    }
    .fa.fa-minus-circle.rem_ques_icon.card-link {
        color: #2966fc;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add Questions</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/question-banks') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add Questions</a></li>
                            <!-- <li class="breadcrumb-item active" aria-current="page">Add Exam</li> -->
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                   <!--  -->
                   <div class="card-body">
                        <h5 class="card-title">Add Questions</h5>
                        <form action="" method="POST" enctype="multipart/form-data" id="add_exam_form">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="">Choose Category</option>
                                        @if(!empty(@$categories))
                                        <?php foreach ($categories as $key => $category) { ?>
                                            <option value="{{@$category['id']}}">{{@$category['name']}}</option>
                                        <?php } ?>
                                        @endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_category_id" id="sub_category">
                                        <option value="">Choose SubCategory</option>
                                        
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_sub_category_id" id="sub_subcategory">
                                        <option value="">Choose Sub Category-2</option>
                                     
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subject: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="subject_id" id="subject_id">
                                        <option value="">Choose Subject</option>
                                        @if(!empty(@$subjects))
                                        <?php foreach ($subjects as $key => $subject) { ?>
                                            <option value="{{@$subject['id']}}">{{@$subject['name']}}</option>
                                        <?php } ?>
                                        @endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="unit_id" id="unit_id">
                                        <option value="">Choose Unit</option>
                                      <!--   @if(!empty(@$subjects))
                                            <option value="{{@$unit['id']}}">{{@$unit['name']}}</option>
                                        @endif -->
                                    </select> 
                                </div>
                            </div>
                        

                            <!-- <div class="acrddn add_qutions no_ques_div" style="display: none;">
                                <div id="accordion" class="apnd_quest">
                                    <div class="card">
                                        <div class="card-header">
                                            No Question 
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        
                            <div class="acrddn add_qutions">
                                <?php $count = 0; ?>
                                <input type="hidden" value="{{ $count }}" name="count" id="counter" />
                                <div id="accordion" class="apnd_quest">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo{{$count}}">
                                            <a class="card-link" data-toggle="collapse" href="#collapseOne{{$count}}">
                                              Question  
                                            </a>
                                            <i class="fa fa-minus-circle rem_ques_icon card-link"></i>
                                            <span class="markss float-right">
                                                <input type="text" name="question[{{$count}}][marks]" placeholder="Marks" class="form-control marks_feld">
                                                <span class="marks_error"></span>
                                            </span>
                                        </div>
                                        <div id="collapseOne{{$count}}" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                <!-- Fields -->
                                                <div class="field_add">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Enter Question</label>
                                                                <textarea class="form-control ckeditor" id="ckeditorq" rows="4" name="question[{{$count}}][ques]"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Question Image</label>
                                                                <input type="file" name="question[{{$count}}][ques_image]" class="ques_img">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option A: (Answer)</label>
                                                                <!-- <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][0]"> -->
                                                                <textarea class="form-control ckeditor" name="question[{{$count}}][option][0]"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option A Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][0]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B: (Answer)</label>
                                                                <!-- <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][1]"> -->
                                                                <textarea class="form-control ckeditor" name="question[{{$count}}][option][1]"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][1]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C: (Answer)</label>
                                                                <!-- <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][2]"> -->
                                                                <textarea class="form-control ckeditor" name="question[{{$count}}][option][2]"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][2]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D: (Answer)</label>
                                                                <!-- <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][3]"> -->
                                                                <textarea class="form-control ckeditor" name="question[{{$count}}][option][3]"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][3]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Correct Answer</label>
                                                                <div class="col-sm-12">
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios1" value="1" checked>
                                                                        <label class="form-check-label" for="exampleRadios1">
                                                                        Option A
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios2" value="2">
                                                                        <label class="form-check-label" for="exampleRadios2">
                                                                        Option B
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios3" value="3">
                                                                        <label class="form-check-label" for="exampleRadios3">
                                                                        Option C
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios4" value="4">
                                                                        <label class="form-check-label" for="exampleRadios4">
                                                                        Option D
                                                                        </label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Answer Explanation</label>
                                                                <textarea class="form-control ckeditor" id="ckeditorq" rows="4" name="question[{{$count}}][ans_explanation]"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Answer Explanation Image</label>
                                                                <input type="file" name="question[{{$count}}][ans_explanation_image]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fields -->
                                            </div>
                                        </div>
                                    </div>
                                    
                                   
                                </div>
                            </div>
                            <div class="add_btn_ssbmit">
                                <a class="btn btn-primary ad_quest">Add More</a>
                            </div>
                            <div class="add_btn_ssbmit text-right">
                                <button type="submit" class="btn btn-primary sbmt_btn">Submit</button>
                            </div>
                            {{csrf_field()}}
                        </form>

                    </div>
                   <!--  -->
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
<script type="text/javascript">
    $(document).on('click','.rem_ques_icon',function(){

        var div_length = $(this).closest('#accordion').children('div').length;
        if(div_length == 1){
            $('.no_ques_div').show();
            $('.sbmt_btn').attr('disabled','');
        }
        $(this).closest('.card').remove();
    })
</script>
<script type="text/javascript">
    CKEDITOR.replace( 'ckeditor' );
</script>
<script type="text/javascript">
    $(document).on('click','.ad_quest',function(){
        $('.no_ques_div').hide();
        $('.sbmt_btn').removeAttr('disabled');

        var count     = $('#counter').val();
        var count     = parseInt(count) + 1;

        // $('.apnd_quest').append('<div class="card"><div class="card-header"> <a class="card-link" data-toggle="collapse" href="#collapseOne"> Question </a> <i class="fa fa-minus-circle rem_ques_icon card-link"></i><span class="markss float-right"> <input type="text" placeholder="Marks" class="form-control"> </span></div><div id="#collapseOne" class="collapse show" data-parent="#accordion"><div class="card-body"><div class="field_add"><div class="row"><div class="col-sm-12"><div class="form-group"> <label>Enter Question</label><textarea class="form-control ckeditor" id="ckeditorq" rows="4" ></textarea></div></div></div><div class="row"><div class="col-sm-6"><div class="form-group"> <label>Option A: (Answer)</label> <input type="text" class="form-control" placeholder="Enter Option"></div></div><div class="col-sm-6"><div class="form-group"> <label>Option B: (Answer)</label> <input type="text" class="form-control" placeholder="Enter Option"></div></div></div><div class="row"><div class="col-sm-6"><div class="form-group"> <label>Option C: (Answer)</label> <input type="text" class="form-control" placeholder="Enter Option" ></div></div><div class="col-sm-6"><div class="form-group"> <label>Option D: (Answer)</label> <input type="text" class="form-control" placeholder="Enter Option" ></div></div></div><div class="row"><div class="col-sm-12"><div class="form-group"> <label>Correct Answer</label><div class="col-sm-12"> <span class="form-check radio_rell"> <input class="form-check-input" type="radio" id="exampleRadios1" value="1" checked> <label class="form-check-label" for="exampleRadios1"> Option A </label> </span> <span class="form-check radio_rell"> <input class="form-check-input" type="radio" id="exampleRadios2" value="2"> <label class="form-check-label" for="exampleRadios2"> Option B </label> </span> <span class="form-check radio_rell"> <input class="form-check-input" type="radio" id="exampleRadios3" value="3"> <label class="form-check-label" for="exampleRadios3"> Option C </label> </span> <span class="form-check radio_rell"> <input class="form-check-input" type="radio" id="exampleRadios4" value="4"> <label class="form-check-label" for="exampleRadios4"> Option D </label> </span></div></div></div></div><div class="row"><div class="col-sm-12"><div class="form-group"> <label>Answer Explanation</label><textarea class="form-control ckeditor" id="ckeditorq" rows="4" ></textarea></div></div></div></div></div></div></div>');
        
        $('.apnd_quest').append(`<div class="card">
                                        <div class="card-header" id="headingTwo`+count+`">
                                            <a class="card-link" data-toggle="collapse" href="#collapseOne`+count+`">
                                              Question  
                                            </a>
                                            <i class="fa fa-minus-circle rem_ques_icon card-link"></i>
                                            <span class="markss float-right">
                                                <input type="text" name="question[`+count+`][marks]" placeholder="Marks" class="form-control marks_feld">
                                                <span class="marks_error"></span>
                                            </span>
                                        </div>
                                        <div id="collapseOne`+count+`" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                
                                                <div class="field_add">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Enter Question</label>
                                                                <textarea class="form-control ckeditor" id="ckeditorq`+count+`" rows="4" name="question[`+count+`][ques]"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Question Image</label>
                                                                <input type="file" name="question[`+count+`][ques_image]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option A: (Answer)</label>
                                                                <textarea class="form-control ckeditor" name="question[`+count+`][option][0]" id="question[`+count+`][option][0]" id="question[`+count+`][option][0]">
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option A Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[`+count+`][option_image][0]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B: (Answer)</label>
                                                                <textarea class="form-control ckeditor" name="question[`+count+`][option][1]" id="question[`+count+`][option][1]" id="question[`+count+`][option][1]">
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[`+count+`][option_image][1]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C: (Answer)</label>
                                                                <textarea class="form-control ckeditor" name="question[`+count+`][option][2]" id="question[`+count+`][option][2]" id="question[`+count+`][option][2]">
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[`+count+`][option_image][2]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D: (Answer)</label>
                                                                <textarea class="form-control ckeditor" name="question[`+count+`][option][3]" id="question[`+count+`][option][3]">
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[`+count+`][option_image][3]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Correct Answer</label>
                                                                <div class="col-sm-12">
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[`+count+`][answer]" id="exampleRadios1" value="1" checked>
                                                                        <label class="form-check-label" for="exampleRadios1">
                                                                        Option A
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[`+count+`][answer]" id="exampleRadios2" value="2">
                                                                        <label class="form-check-label" for="exampleRadios2">
                                                                        Option B
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[`+count+`][answer]" id="exampleRadios3" value="3">
                                                                        <label class="form-check-label" for="exampleRadios3">
                                                                        Option C
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[`+count+`][answer]" id="exampleRadios4" value="4">
                                                                        <label class="form-check-label" for="exampleRadios4">
                                                                        Option D
                                                                        </label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Answer Explanation</label>
                                                                <textarea class="form-control ckeditor" id="ckeditorans`+count+`" rows="4" name="question[`+count+`][ans_explanation]"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                     <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Answer Explanation Image</label>
                                                                <input type="file" name="question[`+count+`][ans_explanation_image]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>`);

        $('#counter').val(count);
        
        CKEDITOR.replace( 'ckeditorans'+count );
        CKEDITOR.replace( 'ckeditorq'+count );
        var i;
        for(i=0; i<4;i++){
            
            CKEDITOR.replace('question'+'['+count+']'+"[option]"+'['+i+']');
        }
    });
</script>

<script>
    var form = $('#add_exam_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            category_id:{
                required:true,
            },
            sub_category_id:{
                required:true,
            },
            unit_id:{
                required:true,
            },
            subject_id:{
                required:true,
            },
        },
        submitHandler:function(form){
            // var formdata = $('#add_exam_form').serialize();
            var err = 0;
            $('.marks_feld').each(function(key){
                
                var field_val = $(this).val().trim();
                var error     = $(this).next('.marks_error');

                if(field_val == ''){
                    err = 1;
                    error.text('*This field is required').css({'color':'red'});    

                } 
                else{
                    

                    var regex = /^[0-9 .+]{1,10}$/;  
                    if(!field_val.match(regex)) {
                        err = 1;
                        error.text('*Only digits allowed').css({'color':'red'});    
                    } else{
                        error.text('');    
                    }  
                }
            });
            if(err == 1){
                return false;
            }

            var form = $('form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            // var ques_img = $('.ques_img').val();
            // alert(img); return false;
            // $.each(form.find('input[type="file"]'), function(i, tag) {
            //       $.each($(tag)[0].files, function(i, file) {
            //         formData.append(tag.name, file);
            //       });
            // });
            $.ajax({
                type:'post',
                url :"{{url('admin/validate/add/questions')}}",
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success:function(resp){
                    console.log(resp);
                    if(resp == 'true'){
                        form.submit();
                    }else{
                        toastr.error('All fields are required.');
                    }
                }
            })

            // 
        },
    });
</script>
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                // $('#unit_id').html(resp.units);

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).on('change', '#subject_id', function() {
        var subject_id = $(this).val();
        // alert(subject_id);
        if(subject_id!=''){

            $('.loader').show();
            $.ajax({
                 type:"get",
                url: "{{ url('get/units') }}"+"/"+subject_id,
                success:function(resp){
                    $('#unit_id').html(resp);
                    $('.loader').hide();
                }
            })
        }else{
            $('#unit_id').html(' <option value="">Choose Unit</option>');
        }
    });
</script>
@endsection    