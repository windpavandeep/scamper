@extends('backEnd.layouts.master')
@section('title','Import Questions')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Import Questions</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/question-banks') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Import Questions</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="import_file_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Import Questions</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="">Choose Category</option>
                                        @if(!empty(@$categories))
                                        <?php foreach ($categories as $key => $category) { ?>
                                            <option value="{{@$category['id']}}">{{@$category['name']}}</option>
                                        <?php } ?>
                                        @endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_category_id" id="sub_category">
                                        <option value="">Choose SubCategory</option>
                                        
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_sub_category_id" id="sub_subcategory">
                                        <option value="">Choose Sub Category-2</option>
                                     
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Subject: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="subject_id" id="subject_id">
                                        <option value="">Choose Subject</option>
                                        @foreach($subjects as $subject)
                                           <option value="{{$subject['id']}}">{{ ucfirst($subject['name']) }}</option>
                                        @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Unit: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="unit_id" id="unit_id">
                                        <option value="">Choose Unit</option>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload File</label>
                                <div class="col-md-7">
                                   <input type="file" name="csv_file" id="img_upload">
                                </div>
                            </div>
                            <div class="form-group row">
                              
                                <div class="col-md-7">
                                    
                                    <u><a href="{{DefaultCsvImgPath}}" style="margin-left: 259px;">Download Sample Csv File</a></u>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#import_file_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            subject_id:{
                required:true,
            },
            unit_id:{
                required:true,
            },
            csv_file:{
                required:true,
            },
            
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'csv' || ext == 'xlsx')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    $(this).val('');
                    alert('Please select .csv, .xlsx file format.');
                }
            } 

        });

    });
</script>
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                // $('#unit_id').html(resp.units);

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).on('change', '#subject_id', function() {
        var subject_id = $(this).val();
        // alert(subject_id);
        if(subject_id!=''){

            $('.loader').show();
            $.ajax({
                 type:"get",
                url: "{{ url('get/units') }}"+"/"+subject_id,
                success:function(resp){
                    $('#unit_id').html(resp);
                    $('.loader').hide();
                }
            })
        }else{
            $('#unit_id').html(' <option value="">Choose Unit</option>');
        }
    });
</script>
@endsection

@endsection    