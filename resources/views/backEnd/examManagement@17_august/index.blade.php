@extends('backEnd.layouts.master')
@section('title','Exam Management')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exam Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Exams</h5>
                        <div class="add_btn">
                            <!-- <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                Export <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right export-import" role="menu">
                                    <li>
                                        <a href="{{url('admin/companies/export?excel')}}" class="report_optn" type="excel">Export to Excel</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export?csv')}}" class="report_optn" type="csv">Export to CSV</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export/pdf')}}" class="report_optn" type="pdf">Export to PDF</a>
                                    </li>
                                </ul>
                            </div> -->
                            <a href="{{ url('admin/exam/add') }}" class="btn btn-primary">Add Exam</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Exam Name</th>
                                        <th>Exam Code</th>
                                        <th>Exam Validity</th>
                                        <th width="16%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($exams))
                                        <?php foreach($exams as $key=>$value){
                                            if($value['exam_validity'] == 'Y'){
                                                $status = 'Yes';
                                            }else{
                                                $status = 'No';
                                            }
                                        ?>
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>{{ $value['code'] }}</td>
                                                <td>{{ $status }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/exam/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/exam/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/exam/question/'.$value['id']) }}" title="Add Questions"><i class="fa fa-file"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/exam/question/paper/'.$value['id']) }}" title="Question Paper"><i class="fa fa-question-circle"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/import/file/'.$value['id']) }}" title="Import CSV File"><i class="fa fa-upload"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?> 
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection