@extends('backEnd.layouts.master')
@section('title','Edit Question')
@section('content')

<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>

<style type="text/css">
    .card-link {
        padding: 0;
        font-size: 22px;
    }
    .form-check.radio_rell {
        display: inline-block;
        margin-right: 20px;
    }
    .form-check.radio_rell .form-check-input {
        margin-top: 0.1rem;
    }
    .btn.btn-primary {
        color: #fff!important;
    }
    .fa.fa-minus-circle.rem_ques_icon.card-link {
        color: #2966fc;
    }
    .immg_div {
        display: block;
    }
    .immg_div .modal-upload-image-preview {
        width: 120px;
        float: unset;
        height: 120px;
        object-fit: cover;
        border: 1px solid #ddd;
        padding: 3px;
        margin-bottom: 5px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exams</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/exams') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{ url('admin/exam/question/paper/'.$exam_id) }}">Question Paper</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Question</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                   <!--  -->
                   <div class="card-body">
                        <h5 class="card-title">Edit Questions</h5>
                        <form action="{{ url('admin/exam/question/edit/'.$question_id.'/'.$exam_id)}}" method="POST" enctype="multipart/form-data" id="add_exam_form">
                            
                        
                            <div class="acrddn add_qutions">
                                <?php $count = 0; ?>
                                <input type="hidden" value="{{ $count }}" name="count" id="counter" />
                                <div id="accordion" class="apnd_quest">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo{{$count}}">
                                            <a class="card-link" data-toggle="collapse" href="#collapseOne{{$count}}">
                                              Question  
                                            </a>
                                           
                                            <span class="markss float-right">
                                                <input type="text" name="question[{{$count}}][marks]" placeholder="Marks" class="form-control marks_feld" value="{{isset($question['marks'])? $question['marks'] : ''}}">
                                                <span class="marks_error"></span>
                                            </span>
                                        </div>
                                        <div id="collapseOne{{$count}}" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                <!-- Fields -->
                                                <div class="field_add">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Enter Question</label>
                                                                <textarea class="form-control ckeditor" id="ckeditorq" rows="4" name="question[{{$count}}][ques]">{{isset($question['question'])? $question['question'] : ''}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php  
                                                        $image_url = 'javascript:;';
                                                        if (!empty($question['ques_image'])) {
                                                            // dd($student_details->image);

                                                            if (file_exists(ExamImageBasePath.'/'.$question['ques_image'])) {
                                                                $image = ExamImageImgPath.'/'.$question['ques_image'];
                                                                $image_url = ExamImageImgPath.'/'.$question['ques_image'];
                                                            }else{
                                                                $image = DefaultImgPath;
                                                            }
                                                        }else{
                                                            $image = DefaultImgPath;
                                                        }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Question Image</label>
                                                            <div class="form-group">
                                                                
                                                                <img src="{{$image}}" width="100%" height="100%" id="old_question_image" alt="No image" class="modal-upload-image-preview">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <input type="file" name="question[{{$count}}][ques_image]" class="ques_img" id="question_image">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php foreach ($question['options'] as $key => $value) { 
                                                        if($key == 0){
                                                            $txt = 'A';
                                                        }elseif($key == 1){
                                                            $txt = 'B';
                                                        }elseif($key == 2){
                                                            $txt = 'C';
                                                        }elseif($key == 3){
                                                            $txt = 'D';
                                                        }
                                                    ?>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label>Option {{$txt}}: (Answer)</label>
                                                                 <!--    <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][{{$key}}]" value="{{isset($value['options'])? $value['options'] : ''}}"> -->

                                                                    <textarea class="form-control ckeditor" name="question[{{$count}}][option][{{$key}}]">{{isset($value['options'])? $value['options'] : ''}}</textarea>
                                                                </div>
                                                            </div>
                                                            <?php 
                                                                if (!empty($value['option_image'])) {
                                                                    // dd($student_details->image);

                                                                    if (file_exists(ExamImageBasePath.'/'.$value['option_image'])) {
                                                                        $image = ExamImageImgPath.'/'.$value['option_image'];
                                                                       
                                                                    }else{
                                                                        $image = DefaultImgPath;
                                                                    }
                                                                }else{
                                                                    $image = DefaultImgPath;
                                                                }
                                                            ?>
                                                            <div class="col-sm-6">
                                                                
                                                            <!-- </div> -->
                                                            <!-- <div class="col-sm-3"> -->
                                                                <div class="form-group">
                                                                    <label>Option {{$txt}} Image: (Answer)</label>
                                                                    <div class="immg_div">
                                                                        <!-- <label>Option {{$txt}} Image: (Answer)</label> -->
                                                                        <img src="{{$image}}" width="100%" id="old_answer_image{{$key+1}}" alt="No image" class="modal-upload-image-preview">
                                                                    </div>
                                                                    <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][{{$key}}]" id="answer_image{{$key+1}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <!-- <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B: (Answer)</label>
                                                                <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][1]">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option B Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][1]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C: (Answer)</label>
                                                                <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][2]">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option C Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][2]">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D: (Answer)</label>
                                                                <input type="text" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option][3]">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label>Option D Image: (Answer)</label>
                                                                <input type="file" class="form-control" placeholder="Enter Option" name="question[{{$count}}][option_image][3]">
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Correct Answer</label>
                                                                <div class="col-sm-12">
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios1" value="1" <?php if(isset($question['correct_answer'])){ if($question['correct_answer'] == '1'){ echo "checked"; }} ?>>
                                                                        <label class="form-check-label" for="exampleRadios1">
                                                                        Option A
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios2" value="2" <?php if(isset($question['correct_answer'])){ if($question['correct_answer'] == '2'){ echo "checked"; }} ?>>
                                                                        <label class="form-check-label" for="exampleRadios2">
                                                                        Option B
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios3" value="3" <?php if(isset($question['correct_answer'])){ if($question['correct_answer'] == '3'){ echo "checked"; }} ?>>
                                                                        <label class="form-check-label" for="exampleRadios3">
                                                                        Option C
                                                                        </label>
                                                                    </span>
                                                                    <span class="form-check radio_rell">
                                                                        <input class="form-check-input" type="radio" name="question[{{$count}}][answer]" id="exampleRadios4" value="4" <?php if(isset($question['correct_answer'])){ if($question['correct_answer'] == '4'){ echo "checked"; }} ?>>
                                                                        <label class="form-check-label" for="exampleRadios4">
                                                                        Option D
                                                                        </label>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label>Answer Explanation</label>
                                                                <textarea class="form-control ckeditor" id="ckeditora" rows="4" name="question[{{$count}}][ans_explanation]">{{isset($question['answer_explanation'])? $question['answer_explanation'] : ''}}</textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php 
                                                        if (!empty($question['ans_image'])) {
                                                            // dd($student_details->image);

                                                            if (file_exists(ExamImageBasePath.'/'.$question['ans_image'])) {
                                                                $image = ExamImageImgPath.'/'.$question['ans_image'];
                                                               
                                                            }else{
                                                                $image = DefaultImgPath;
                                                            }
                                                        }else{
                                                            $image = DefaultImgPath;
                                                        }
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <label>Answer Explanation Image</label>
                                                            <div class="form-group">
                                                                <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <input type="file" name="question[{{$count}}][ans_explanation_image]" id="img_upload">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fields -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="add_btn_ssbmit text-right">
                                <input type="hidden" name="ques_text" class="ques_text">
                                <input type="hidden" name="ans_text" class="ans_text">
                                <button type="submit" class="btn btn-primary sbmt_btn">Submit Questions</button>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
<script type="text/javascript">
    CKEDITOR.replace( 'ckeditor' );
</script>
<script>
    var form = $('#add_exam_form');
    var question_id = "{{$question_id}}";
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            unit_id:{
                required:true,
            },
            subject_id:{
                required:true,
            },
        },
        submitHandler:function(form){
            // var formdata = $('#add_exam_form').serialize();
            var err = 0;
            $('.marks_feld').each(function(key){
                
                var field_val = $(this).val().trim();
                var error     = $(this).next('.marks_error');

                if(field_val == ''){
                    err = 1;
                    error.text('*This field is required').css({'color':'red'});
                } 
                else{
                    var regex = /^[0-9 .+]{1,10}$/;  
                    if(!field_val.match(regex)) {
                        err = 1;
                        error.text('*Only digits allowed').css({'color':'red'});    
                    } else{
                        error.text('');    
                    }  
                }
            });
            if(err == 1){
                return false;
            }
            var ques_text = CKEDITOR.instances['ckeditorq'].getData();
            var ans_text  = CKEDITOR.instances['ckeditora'].getData();
            $('.ques_text').val(ques_text);
            $('.ans_text').val(ans_text);
            var form = $('form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);
            // var desc = $('textarea[name="DSC"]').val();
            
            // console.log(desc); return false;
            // var ques_img = $('.ques_img').val();
            // alert(img); return false;
            // $.each(form.find('input[type="file"]'), function(i, tag) {
            //       $.each($(tag)[0].files, function(i, file) {
            //         formData.append(tag.name, file);
            //       });
            // });
            $.ajax({
                type:'post',
                url :"{{url('admin/validate/edit/question')}}"+'/'+question_id,
                data: formData,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
                success:function(resp){
                    // console.log(resp); return false;
                    if(resp == 'true'){
                        form.submit();
                    }else{
                        toastr.error('All fields are required.');
                    }
                }
            })

            // 
        },
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_question_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#question_image').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_answer_image1').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#answer_image1').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    alert('Please select an image of .jpeg, .jpg, .png file format.');
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_answer_image2').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#answer_image2').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    alert('Please select an image of .jpeg, .jpg, .png file format.');
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_answer_image3').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#answer_image3').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    alert('Please select an image of .jpeg, .jpg, .png file format.');
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_answer_image4').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#answer_image4').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    alert('Please select an image of .jpeg, .jpg, .png file format.');
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }

        });

    });
</script>
@endsection    