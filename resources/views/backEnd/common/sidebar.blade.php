<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <?php
                    $active = '';
                    $selected = '';
                    // $page     ='';
                    if($page == 'dashboard'){
                        $active = 'active';
                        $selected = 'selected';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{ url('/admin/dashboard') }}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>

               <?php
                    $active   = '';
                    $selected = '';
                    if(($page == 'student_management')){
                        $active   = 'active';
                        $selected = 'selected';
                    }
                ?>
             
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/students')}}" aria-expanded="false">
                        <i class="mdi mdi-chart-bar"></i>
                        <span class="hide-menu">Student Management</span>
                    </a>
                </li>
                <?php
                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'new_signup_request')||($page == 'trainers')||($page == 'subscription_plan')||($page == 'domains')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}">
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Teacher Management </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if($page=='trainers'){echo'active';}?>">
                            <a href="{{ url('/admin/trainers') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Teachers</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='new_signup_request'){echo'active';}?>">
                            <a href="{{ url('/admin/trainer/signup-requests') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu"> New Signup Requests </span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='subscription_plan'){echo'active';}?>">
                            <a href="{{ url('/admin/trainer/subscription-plan/requests') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Subscription Plan Requests </span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='domains'){echo'active';}?>">
                            <a href="{{ url('/admin/domains') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Domains </span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'categories')||($page=='sub_categories')||($page=='sub_sub_categories')||($page=='units')||($page=='subjects')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}">
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-trello"></i>
                        <span class="hide-menu">Category Management </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='categories')||($page=='sub_categories')||($page=='sub_sub_categories')){echo'active';}?>">
                            <a href="{{ url('/admin/categories') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Categories</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='subjects'){echo'active';}?>">
                            <a href="{{ url('/admin/subjects') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Subjects</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
                     $active   = '';
                     $selected = '';
                     if(($page == 'course_management')){
                         $active   = 'active';
                         $selected = 'selected';
                     }
                 ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/courses')}}" aria-expanded="false">
                        <i class="mdi mdi-message-video"></i>
                        <span class="hide-menu">Product Management</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'franchise'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/retailers')}}" aria-expanded="false">
                        <i class="mdi mdi-database-minus"></i>
                        <span class="hide-menu">Retailer Requests</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'counselling_requests'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/counselling/requests')}}" aria-expanded="false">
                        <i class="mdi mdi-layers"></i>
                        <span class="hide-menu">Counselling Requests</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'referrals'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/referrals')}}" aria-expanded="false">
                        <i class="mdi mdi-google-circles-group"></i>
                        <span class="hide-menu">Referrals</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'contact_us'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark sidebar-link {{ $active }}" href="{{url('/admin/contact-us')}}" aria-expanded="false">
                        <i class="mdi mdi-contact-mail"></i>
                        <span class="hide-menu">Contact Us</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'pending_payment')||($page == 'paid')||($page == 'earnings')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-credit-card-multiple"></i>
                        <span class="hide-menu">Payment Management </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='pending_payment')){echo'active';}?>">
                            <a href="{{ url('/admin/pending/payments') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Pending Payments</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='paid'){echo'active';}?>">
                            <a href="{{ url('/admin/paid/payments') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Paid Payments</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='earnings'){echo'active';}?>">
                            <a href="{{ url('/admin/earnings') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Admin Earnings</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'first_section') || ($page=='second_section') || ($page=='third_section')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-monitor"></i>
                        <span class="hide-menu">Home Page Content </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='first_section')){echo'active';}?>">
                            <a href="{{ url('/admin/home/page/first/section') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">First Section</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='second_section'){echo'active';}?>">
                            <a href="{{ url('/admin/home/page/second/section') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Second Section</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='third_section'){echo'active';}?>">
                            <a href="{{ url('/admin/home/page/third/section') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Third Section</span>
                            </a>
                        </li>
                    </ul>
                </li>
               
                <?php  
                    $active   = '';
                    $selected = '';
                    if(($page == 'exam_management')||($page == 'question_banks')||($page == 'sets')||($page == 'all_test_exam_result')){

                            $active   = 'active';
                            $selected = 'selected';
                            $in ='in';
                    } 
                ?>
                 <li class="sidebar-item {{ $selected }}"> 
                     <a class="sidebar-link has-arrow waves-effect waves-dark sidebar-link {{ $active }}" href="javascript:;" aria-expanded="false">
                         <i class="mdi mdi-message-video"></i>
                         <span class="hide-menu">Exam Management</span>
                     </a>
                     <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if($page=='exam_management'){echo'active';}?>">
                            <a href="{{url('/admin/exams')}}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Exams</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='question_banks'){echo'active';}?>">
                            <a href="{{url('/admin/question-banks')}}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Question Bank</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a href="{{url('/admin/sets')}}" class="sidebar-link <?php if(($page=='sets')){echo'active';}?>">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Sets</span>
                            </a>
                        </li>  
                        <li class="sidebar-item <?php if($page=='all_test_exam_result'){echo'active';}?>">
                            <a href="{{url('/admin/all/test/exams/')}}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">All Test Exam Result</span>
                            </a>
                        </li>
                    </ul>
                 </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'header_content') || ($page=='footer_content')|| ($page=='faq')|| ($page=='about_us')|| ($page=='success_stories')|| ($page=='our_faculty')|| ($page=='pop_up')|| ($page=='privacy_terms')||($page=='our_faculty_banner_image')||($page=='terms_conditions')||($page=='become_trainer')||($page=='retailer_image')||($page=='contact_us_content')|| ($page=='disclaimer')|| ($page=='welcome_sms')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-content-paste"></i>
                        <span class="hide-menu">Content Management </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='header_content')){echo'active';}?>">
                            <a href="{{ url('/admin/header/content') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Header Content</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='footer_content'){echo'active';}?>">
                            <a href="{{ url('/admin/footer/content') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Footer Content</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='faq'){echo'active';}?>">
                            <a href="{{ url('/admin/faqs') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">FAQS</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='about_us'){echo'active';}?>">
                            <a href="{{ url('/admin/about-us') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">About Us</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='privacy_terms'){echo'active';}?>">
                            <a href="{{ url('/admin/privacy-policy/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Privacy Policy</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='terms_conditions'){echo'active';}?>">
                            <a href="{{ url('/admin/term-condition/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Terms &amp; Conditions</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='disclaimer'){echo'active';}?>">
                            <a href="{{ url('/admin/disclaimer/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Disclaimer</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='success_stories'){echo'active';}?>">
                            <a href="{{ url('/admin/success-stories') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Success Stories</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='our_faculty'){echo'active';}?>">
                            <a href="{{ url('/admin/our-faculties') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Our Faculty</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='our_faculty_banner_image'){echo'active';}?>">
                            <a href="{{ url('/admin/our-faculty-banner-image/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Our Faculty Banner Image</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='retailer_image'){echo'active';}?>">
                            <a href="{{ url('/admin/retailer-image/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Retailer Image</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='pop_up'){echo'active';}?>">
                            <a href="{{ url('/admin/advertisement/pop-ups') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Adv. Pop Ups</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='become_trainer'){echo'active';}?>">
                            <a href="{{ url('/admin/become-trainers') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Become Trainer</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='become_trainer'){echo'active';}?>">
                            <a href="{{ url('/admin/news-events') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">News & Events</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='contact_us_content'){echo'active';}?>">
                            <a href="{{ url('/admin/contact-us/content/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-account-multiple"></i>
                                <span class="hide-menu">Contact Us Content</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='welcome_sms'){echo'active';}?>">
                            <a href="{{ url('/admin/welcome-sms/edit') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Welcome Sms</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'discount_coupons'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="{{ url('admin/discount-coupons')}}" aria-expanded="false">
                        <i class="mdi mdi-paper-cut-vertical"></i>
                        <span class="hide-menu">Discount Coupon </span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'students_reorts')||($page == 'teacher_list')||($page == 'all_test_exam_result_report')||($page == 'sales_report')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link has-arrow waves-effect waves-dark {{$active}}" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-equal-box"></i>
                        <span class="hide-menu">Report Management </span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='students_reorts')){echo'active';}?>">
                            <a href="{{ url('/admin/student/reports') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Students</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if(($page=='teacher_list')){echo'active';}?>">
                            <a href="{{ url('/admin/teacher/reports') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Teachers</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if(($page=='all_test_exam_result_report')){echo'active';}?>">
                            <a href="{{ url('/admin/all/test/exam/reports') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">All Test Exam Result</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if(($page=='sales_report')){echo'active';}?>">
                            <a href="{{ url('/admin/sales/reports') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Sales Report</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'downloads')||($page == 'downloads')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="{{url('/admin/downloads')}}" aria-expanded="false">
                        <i class="mdi mdi-download"></i>
                        <span class="hide-menu">Download Management</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if($page == 'language'){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="{{ url('admin/language')}}" aria-expanded="false">
                        <i class="mdi mdi-language-css3"></i>
                        <span class="hide-menu">Language Management </span>
                    </a>
                    <!-- <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='header_content')){echo'active';}?>">
                            <a href="{{ url('/admin/header/content') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Header Content</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if($page=='footer_content'){echo'active';}?>">
                            <a href="{{ url('/admin/footer/content') }}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Footer Content</span>
                            </a>
                        </li>
                        
                    </ul> -->
                    
                
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'notifications')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="{{url('/admin/send/notifications')}}" aria-expanded="false">
                        <i class="mdi mdi-notification-clear-all"></i>
                        <span class="hide-menu">Notification Management</span>
                    </a>
                </li>
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'sub_admin')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="{{url('/admin/sub-admins')}}" aria-expanded="false">
                        <i class="mdi mdi-server-security"></i>
                        <span class="hide-menu">Sub Admin Management</span>
                    </a>
                </li>
               
                
                <?php

                    $active   = '';
                    $selected = '';
                    $in = '';
                    if(($page == 'walkthroughs')||($page == 'homepage_banners')){
                        $active   = 'active';
                        $selected = 'selected';
                        $in ='in';
                    }
                ?>
                <li class="sidebar-item {{ $selected }}"> 
                    <a class="sidebar-link waves-effect waves-dark {{$active}}" href="#" aria-expanded="false">
                        <i class="mdi mdi-apps"></i>
                        <span class="hide-menu">App Content Management</span>
                    </a>
                    <ul aria-expanded="true" class="collapse first-level {{$in}}">
                        <li class="sidebar-item <?php if(($page=='walkthroughs')){echo'active';}?>">
                            <a href="{{ url('admin/walkthroughs')}}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Walkthroughs</span>
                            </a>
                        </li>
                        <li class="sidebar-item <?php if(($page=='homepage_banners')){echo'active';}?>">
                            <a href="{{ url('admin/homepage-banners')}}" class="sidebar-link">
                                <i class="mdi mdi-file-image"></i>
                                <span class="hide-menu">Hompage Banners</span>
                            </a>
                        </li>
                    <li>
                </li>

            </ul>
        </nav>
    </div>
</aside>