<div class="modal fade offer_popup" id="earning_export_id" style="margin-top: 10%;">
	<div class="modal-dialog ">
		<div class="modal-content">
			<div class="offer_close">
				<button type="button" class="close" data-dismiss="modal" style="padding-right: 5px;">&times;</button>
			</div>
			<div class="modal-body">
				<div class="offer_img">
					<div class="row">
						<div class="modal_content">
							<div class="col-md-12">
				                <div class="card">
				                    <form class="form-horizontal" method="post" action="{{url('/admin/earning/export')}}" id="earning_export_form>
				                       <div class="card-body">
				                           <h4 class="card-title">Export</h4>
				                           	<div class="form-group row">
				                           	   	<label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Category: </label>
				                           	   	<div class="col-sm-7">
				                           	     	<select class="form-control dtpckr" name="category_id" id="category_id">
				                           	          	<option value=''>Select Category</option>
				                           	           	@if(!empty($categories))
				                           	               	@foreach($categories as $key=>$value)
				                           	                   	<option value="{{ $value['id'] }}">{{ ucfirst($value['name']) }}</option>
				                           	               	@endforeach
				                           	           	@endif
				                           	      	</select>
				                           	   </div>
				                           	</div>
				                       
				                           <div class="form-group row">
				                           	   	<label for="fname" class="col-sm-5 text-right control-label col-form-label">Select State: </label>
				                           	   	<div class="col-sm-7">
				                           	     	<select class="form-control" name="state_id" id="state_id">
				                           	          	<option value=''>Select State</option>
				                           	           	@if(!empty($states))
				                           	               	@foreach($states as $key=>$value)
				                           	                   	<option value="{{ $value['id'] }}">{{ ucfirst($value['name']) }}</option>
				                           	               	@endforeach
				                           	           	@endif
				                           	      	</select>
				                           	   </div>
				                           	</div>
				                            <div class="form-group row">
			                            	   	<label for="fname" class="col-sm-5 text-right control-label col-form-label">District: </label>
			                            	   	<div class="col-sm-7">
			                            	     	<input type="text" name="district" class="form-control" maxlength="255">
			                            	   </div>
			                            	</div>
			                            	<div class="form-group row">
			                            	   	<label for="fname" class="col-sm-5 text-right control-label col-form-label">Coordinatore: </label>
			                            	   	<div class="col-sm-7">
			                            	     	<select class="form-control" name="pincode" id="pincode">
				                           	          	<option value=''>Select Coordinatore</option>
				                           	           	@if(!empty($refferals))
				                           	               	@foreach($refferals as $key=>$value)
				                           	                   	<option value="{{ $value['pincode'] }}">{{ ucfirst($value['coordinator_name']) }}</option>
				                           	               	@endforeach
				                           	           	@endif
				                           	      	</select>
			                            	   </div>
			                            	</div>
				                           <div class="form-group row">
				                               <label for="lname5" class="col-sm-5 text-right control-label col-form-label">Select Date:</label>
				                               <div class="col-sm-7">
				                                   <div class="input-group">
				                                       <input type="text" class="form-control mydatepicker" placeholder="MM/DD/YYYY" name="date" value="{{ isset($student_details['dob'])? date('d-m-Y',strtotime($student_details['dob'])): '' }}" id="mydate">
				                                       <div class="input-group-append">
				                                           <span class="input-group-text"><i class="fa fa-calendar"></i></span>
				                                       </div>
				                                   </div>
				                               </div>
				                           </div>
				                           
				                           
				                       <div class="border-top">
				                       	<center><span class="error1" id="all_error"></span></center>
				                           <div class="card-body">
				                           		<input type="hidden" name="export_type" value="" id="export_type">
				                           		@csrf
				                               <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px" id="submit_button">Submit</button>
				                           </div>
				                       </div>
				                    </form>
				                </div>
				            </div>
						</div>       		
					</div>
				</div>
			</div>
		</div>
	</div>
</div>