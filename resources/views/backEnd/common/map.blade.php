<style type="text/css">
  #infowindow-content .title {
    font-weight: bold;
  }
  #infowindow-content {
    display: none;
  }
  #map #infowindow-content {
    display: inline;
  }
  #map{
    width:100%;
    height:225px;
    /*display: none;*/
  }
</style>
<input type="hidden" id="administrative_area_level_1" name="state" value="">
<!-- <div id="map"></div> -->
<div id="infowindow-content">
    <img src="" width="16" height="16" id="place-icon">
    <span id="place-name"  class="title"></span><br>
    <span id="place-address"></span>
</div>

<?php  
    // $language_id = App\User::getLanguageId();
    $language_id = '1';
    // prx($language_id);
?>

<script type="text/javascript">
    //prevent form to submit on enter button click
    $(document).ready(function() {
        $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
        });
    });
</script> 

<!-- Map script -->
<script>
    
    var latitude = {{ (isset($latitude)) ? $latitude : '28.7041' }};
    var longitude = {{ (isset($longitude)) ? $longitude : '77.1025' }};
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:latitude, lng:longitude },
          zoom: 10,
        });

        var input = document.getElementById('address');
        
        var autocomplete = new google.maps.places.Autocomplete(input);

        var componentForm = {
          // street_number: 'short_name',
          // route: 'long_name',
          // locality: 'long_name',
          administrative_area_level_2: 'long_name',
          administrative_area_level_1: 'long_name',
          country: 'long_name',
          // postal_code: 'short_name'
        };
        
        // Set the data fields to return when the user selects a place.
        // autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        autocomplete.setFields(['address_components','geometry','icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);

        var pos = {
            lat: latitude,
            lng: longitude
        };
        var marker = new google.maps.Marker({
            position: pos,
            map: map
        });

        /*var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });*/


        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }   
            }

            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();

            $('#latitude').val(latitude);
            $('#longitude').val(longitude);
            // console.log(place);

            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  // (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || ''),
                  (place.address_components[3] && place.address_components[3].short_name || ''),
                  (place.address_components[5] && place.address_components[5].short_name || ''),
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
    }

    function reInitMap(latitude,longitude) {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:latitude, lng:longitude },
          zoom: 17,
        });

        var input = document.getElementById('address');
        
        var autocomplete = new google.maps.places.Autocomplete(input);

        var componentForm = {
            // street_number: 'short_name',
            // route: 'long_name',
            // locality: 'long_name',
            administrative_area_level_2: 'long_name',
            administrative_area_level_1: 'long_name',
            country: 'long_name',
            // postal_code: 'short_name'
        };
        
        // Set the data fields to return when the user selects a place.
        // autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
        autocomplete.setFields(['address_components','geometry','icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        /*var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });*/
         var pos = {
            lat: latitude,
            lng: longitude
        };
        var marker = new google.maps.Marker({
            position: pos,
            map: map
        });


        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }   
            }

            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();

            $('#latitude').val(latitude);
            $('#longitude').val(longitude);
            // console.log(place);

            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }
            
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                  (place.address_components[0] && place.address_components[0].short_name || ''),
                  // (place.address_components[1] && place.address_components[1].short_name || ''),
                  (place.address_components[2] && place.address_components[2].short_name || ''),
                  (place.address_components[3] && place.address_components[3].short_name || ''),
                  (place.address_components[5] && place.address_components[5].short_name || ''),
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
    }
</script>
@if($language_id == '1')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQhN-xkQiUIQ9toO-KRdb9wqtc_cGbAqo&libraries=places&callback=initMap"></script>
@else
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQhN-xkQiUIQ9toO-KRdb9wqtc_cGbAqo&language=hi&region=DL&libraries=places&callback=initMap"></script>   
@endif