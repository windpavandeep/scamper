@extends('backEnd.layouts.master')
@section('title','Counselling Request Detail')
@section('content')
<style type="text/css">
    .ck{
        margin-top: 8px !important;
    }
    .reply_field{
        margin-left: 10px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Counselling Request Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/counselling/requests')}}">Counselling Requests</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Counselling Request Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    <div class="card-body">
                        <h4 class="card-title">Counselling Request Detail</h4>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{ucfirst(@$counsellings['name'])}}
                                </label>
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email: </label>
                            <div class="col-sm-8">
                                
                                <label class="col-sm-8 ck">
                                    {{@$counsellings['email']}}
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{@$counsellings['contact_no']}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{ucfirst(@$counsellings['category_name'])}}
                                </label>
                            </div>
                        </div>
                     
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                    
                                        {{ucfirst(@$counsellings['sub_category_name'])}}
                                    </label>
                                </div>
                            </div>
                           
                        @if(!empty(@$counsellings['sub_sub_category']))
                            @if(!empty($counsellings['sub_sub_category']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-SubCategory Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-3 ck">
                                        
                                            {{ucfirst(@$counsellings['sub_sub_category']['name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">State Name: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                
                                    {{ucfirst(@$counsellings['state_name'])}}
                                </label>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description: </label>
                            
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                    {{ucfirst(@$counsellings['description'])}}
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">Request Date: </label>
                            <div class="col-sm-8">
                                <label class="col-sm-3 ck">
                                
                                    {{ date('d-m-Y',strtotime($counsellings['updated_at'])) }}
                                </label>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    