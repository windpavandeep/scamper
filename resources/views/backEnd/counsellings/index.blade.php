@extends('backEnd.layouts.master')
@section('title','Counselling Requests')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Counselling Requests</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Counselling Requests</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <!-- <th>Last Name</th> -->
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Request Date</th>
                                        <th>Other Date</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($counsellings))
                                        @foreach($counsellings as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ $value['contact_no'] }}</td>
                                                <td>{{ date('d-m-Y',strtotime($value['created_at'])) }}</td>
                                                <td>{{ date('d-m-Y',strtotime($value['updated_at'])) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/counselling/request/detail/'.$value['id']) }}" title="Info"><i class="fa fa-info"></i>
                                                    </a>
                                                    
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection