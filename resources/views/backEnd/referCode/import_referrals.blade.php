@extends('backEnd.layouts.master')
@section('title','Import Referrals')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Import Referrals</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/referrals') }}">Referrals</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Import Referrals</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/admin/import/referrals')}}" id="referral_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Import Referrals</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload File</label>
                                <div class="col-md-7">
                                   <input type="file" name="csv_file" id="img_upload">
                                </div>
                            </div>
                            <div class="form-group row">
                              
                                <div class="col-md-7">
                                    
                                    <u><a href="{{url('/public/images/system/coordinator_upload_sample.csv')}}" style="margin-left: 259px;">Download Sample Csv File</a></u>
                                </div>
                            </div>

                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                @csrf
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@endsection    