@extends('backEnd.layouts.master')
@section('title','Referrals')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Referrals</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Referrals</h5>
                        <div class="add_btn">
                            <a href="{{ url('/admin/import/referrals') }}" class="btn btn-primary">Import Referrals</a>
                            <a href="{{ url('/admin/referral/add') }}" class="btn btn-primary">Add Referral</a>

                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Coordinator Id</th>
                                        <th>Pincode</th>
                                        <th width="15%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty(@$detail))
                                        @foreach($detail as $key=>$value)
                                            <tr>
                                                <td>{{ $value['coordinator_id'] }}</td>
                                                <td>{{ $value['pincode'] }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/referral/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/referral/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                   
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection