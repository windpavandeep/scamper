<?php 
    if(isset($referral_id)){
        $task    = 'Edit';
        $action  =  url('/admin/referral/edit/'.$referral_id);
    }else{
        $task    = 'Add';
        $action  =  url('/admin/referral/add');
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Referral')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Referral</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/referrals') }}">Referrals</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Referral</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="referral_form">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Referral</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Coordinator ID: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="pincode3" placeholder="Coordinator ID" name="referral_id" value="{{ isset($detail['coordinator_id'])? $detail['coordinator_id']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname12" placeholder="Name" name="coordinator_name" value="{{ isset($detail['coordinator_name'])? $detail['coordinator_name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Pincode: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname1" placeholder="Pincode" name="pincode" value="{{ isset($detail['pincode'])? $detail['pincode']: '' }}">
                                </div>
                            </div>
                           

                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                               
                                <input type="hidden" name="coordinator_id" id="coordinator_id" value="{{@$referral_id}}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
<script>
    var form = $('#referral_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            referral_id:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z0-9 ]+$/,
                remote:{
                    url:"{{url('admin/validate/coordinator-id')}}",
                    data:{
                        coordinator_id:function(){
                            return $('#coordinator_id').val();
                        },
                    },   
                }, 
            },
            coordinator_name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/
            },
            pincode:{
                required:true,
                digits:true,
                minlength:4,
                maxlength:10,
                remote:{
                    url:"{{url('admin/validate/pincode')}}",
                    data:{
                        coordinator_id:function(){
                            return $('#coordinator_id').val();
                        },
                    },   
                }, 
            },
           
        },
        messages:{
            
            referral_id:{
                remote:'This coordinator id already registered',
            },

            pincode:{
                remote:'This pincode already exists',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

@endsection

@endsection    