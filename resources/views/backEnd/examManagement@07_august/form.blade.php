<?php 
    if(isset($exam_id)){
        $task    = 'Edit';
        $action  =  url('/admin/exam/edit/'.$exam_id);
    }else{
        $task    = 'Add';
        $action  =  url('/admin/exam/add');
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Exam')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Exam</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/exams') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Exam</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="exam_form">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Exam</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Choose Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="">Choose Category</option>
                                        <?php foreach ($categories as $key => $value) { ?>
                                            <option value="{{@$value['id']}}" <?php if(isset($exam['categories_id'])){ if($exam['categories_id'] == $value['id']){ echo "selected"; }} ?>>{{@$value['name']}}</option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Choose Sub-Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_category_id" id="sub_category_id">

                                        <option value="{{ isset($exam['sub_categories']['id'])? $exam['sub_categories']['id'] : '' }}">{{ isset($exam['sub_categories']['name'])? $exam['sub_categories']['name'] : 'Choose Sub-Category' }}</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Choose Sub-Category 2: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_sub_category_id" id="sub_sub_category_id">
                                        <option value="{{ isset($exam['sub_sub_categories']['id'])? $exam['sub_sub_categories']['id'] : '' }}">{{ isset($exam['sub_sub_categories']['name'])? $exam['sub_sub_categories']['name'] : 'Choose Sub-Category 2' }}</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control exm_nme" placeholder="Name" name="name" value="{{ isset($exam['name'])? $exam['name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Code: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control cde" placeholder="Code" name="code" value="{{ isset($exam['code'])? $exam['code']: '' }}" maxlength="20">
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" class="btn btn-primary gen_btn">Generate</button>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Select Exam validity:</label>
                                <div class="col-sm-7">
                                    <span class="exam_validity_error"></span>
                                    <div class="input-group">
                                        <select class="form-control" name="exam_validity" id="exam_validity">
                                            <option value="">Select Exam Validity</option>
                                            <option value="Y" <?php if(isset($exam['exam_validity'])){ if($exam['exam_validity'] == 'Y'){ echo "selected"; }} ?>>Yes</option>
                                            <option value="N" <?php if(isset($exam['exam_validity'])){ if($exam['exam_validity'] == 'N'){ echo "selected"; }} ?>>No</option>
                                        </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="validity_div">
                                <div class="form-group row">
                                    <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Start Date:</label>
                                    <div class="col-sm-7">
                                        <span class="start_date_error"></span>
                                        <div class="input-group">
                                            <input type="text" class="form-control mydatepicker" placeholder="DD-MM-YYYY" name="start_date" value="{{ isset($exam['start_date'])? $exam['start_date']: '' }}" id="startdate">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Start Time:</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <select class="form-control" name="start_time">
                                                <option value="">Select Time</option>
                                                <?php for ($i=1; $i < 25 ; $i++) { ?>
                                                    <option value="{{$i}}" <?php if(isset($exam['start_time'])){ if($exam['start_time'] == $i){echo "selected"; } } ?>>{{$i}}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname5" class="col-sm-3 text-right control-label col-form-label">End Date:</label>
                                    <div class="col-sm-7">
                                        <span class="expiry_date_error"></span>
                                        <div class="input-group">
                                            <input type="text" class="form-control mydatepicker" placeholder="DD-MM-YYYY" name="expiry_date" value="{{ isset($exam['expiry_date'])? $exam['expiry_date']: '' }}" id="mydate">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="lname5" class="col-sm-3 text-right control-label col-form-label">End Time:</label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <select class="form-control" name="end_time">
                                                <option value="">End Time</option>
                                                <?php for ($i=1; $i < 25 ; $i++) { ?>
                                                    <option value="{{$i}}" <?php if(isset($exam['end_time'])){ if($exam['end_time'] == $i){echo "selected"; } } ?>>{{$i}}</option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Questions: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Total Questions" name="total_questions" value="{{ isset($exam['total_questions'])? $exam['total_questions']: '' }}" maxlength="5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Marks: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Total Marks" name="total_marks" value="{{ isset($exam['total_marks'])? $exam['total_marks']: '' }}" maxlength="5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Negative Marking: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Negative Marking" name="negative_marking" value="{{ isset($exam['negative_marking'])? $exam['negative_marking']: '' }}" maxlength="5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Choose Language: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="language_id" id="language_id">
                                        <option value="">Choose Language</option>
                                        <?php foreach ($languages as $key => $value) { ?>
                                            <option value="{{@$value['id']}}" <?php if(isset($exam['language_id'])){ if($exam['language_id'] == $value['id']){ echo "selected"; }} ?>>{{@$value['name']}}</option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Terms &amp; Condition: </label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" placeholder="Terms &amp; Condition" name="trams_condition" value="{{ isset($exam['trams_condition'])? $exam['trams_condition']: '' }}">{{isset($exam['trams_condition'])? $exam['trams_condition'] : ''}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var today = new Date();
        $('#startdate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose:true,
            startDate: "today",
            maxDate: today
        }).on('changeDate', function (selected) {
             // $(this).datepicker('hide');
            var minDate = new Date(selected.date.valueOf());
            $('#mydate').datepicker('setStartDate', minDate);
        });
        $('#mydate').datepicker({
            format: 'dd-mm-yyyy',
            autoclose:true,
            startDate: "today",
            maxDate: today
        }).on('changeDate', function (selected) {
             // $(this).datepicker('hide');
            var minDate = new Date(selected.date.valueOf());
            $('#startdate').datepicker('setStartDate', minDate);
        });
    });
    
</script>

<script type="text/javascript">
    $(document).on('click','.gen_btn',function(){
        var category_id = $('#category_id').children('option:selected').val();
        var sub_category_id = $('#sub_category_id').children('option:selected').val();
        var sub_sub_category_id = $('#sub_sub_category_id').children('option:selected').val();
        var _token = "{{csrf_token()}}";
        $('.loader').show();
        $.ajax({
            type:'post',
            url:"{{ url('admin/code/generate')}}",
            data:{'category_id':category_id ,'sub_category_id':sub_category_id ,'sub_sub_category_id':sub_sub_category_id ,'_token':_token},
            success:function(resp){
                // console.log(resp);
                $('.cde').val(resp);
                $('.loader').hide();
            }
        })
    })
</script>
<script>
    var form = $('#exam_form');
    form.validate({
        rules:{
            category_id:{
                required:true,
            },
            // sub_category_id:{
            //     required:true,
            // },
            // sub_sub_category_id:{
            //     required:true,
            // },
            exam_validity:{
                required:true,
            },
            name:{
                required:true,
            },
            code:{
                required:true,
            },
            expiry_date:{
                required:true,
            },
            start_date:{
                required:true,
            },
            start_time:{
                required:true,
            },
            end_time:{
                required:true,
            },
            total_questions:{
                required:true,
                regex:/^[0-9 ]+$/
            },
            total_marks:{
                required:true,
                regex:/^[0-9 ]+$/
            },
            negative_marking:{
                required:true,
                regex:/^[0-9 .]+$/
            },
            language_id:{
                required:true,
            },
            trams_condition:{
                required:true,
            },
        },
        messages:{
            total_questions: {
                regex: "This field contain only digits.",
            },
            total_marks: {
                regex: "This field contain only digits.",
            },
            negative_marking: {
                regex: "This field contain only digits.",
            },

        },
        errorPlacement: function(error, element) {
            if(element.attr("id") == "mydate") {
                error.appendTo($('.expiry_date_error'));
            }else if(element.attr("id") == "startdate") {
                error.appendTo($('.start_date_error'));
            }else if(element.attr("id") == "exam_validity") {
                error.appendTo($('.exam_validity_error'));
            }else{
                element.before(error);
            }

        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>



<script type="text/javascript">
    $(document).on('change','#category_id',function(){
        var category_id = $(this).children('option:selected').val();
        $('.loader').show();
        $.ajax({
            type:'get',
            url:"{{url('/get/sub-categories')}}"+'/'+category_id,
            success:function(resp){

                $('#sub_category_id').html(resp['sub_category']);
                $('#sub_sub_category_id').html('<option data-display="Choose Sub-Category 2" value="">Choose Sub-Category 2</option>');
                $('.cde').val('');
                 $('.loader').hide();
            },
        })
        $.ajax({
            type:'get',
            url:"{{url('admin/get/exam/name')}}"+'/'+category_id,
            success:function(resp){
                // console.log(resp);
                $('.exm_nme').val(resp);
                $('.loader').hide();
            },
        })
    })
</script>
<script type="text/javascript">
    $(document).on('change','#sub_category_id',function(){
        var sub_category_id = $(this).children('option:selected').val();
        $('.loader').show();
        $.ajax({
            type:'get',
            url:"{{url('get/sub-subcategories')}}"+'/'+sub_category_id,
            success:function(resp){

                $('#sub_sub_category_id').html(resp);
                $('.cde').val('');
                $('.loader').hide();
            },
        })
    })
</script>
<script type="text/javascript">
    $(document).on('change','#sub_sub_category_id',function(){
        $('.cde').val('');
    });
</script>



<script type="text/javascript">
    <?php if(isset($exam['exam_validity'])){ 
            if($exam['exam_validity'] == 'N'){ ?>
                $('.validity_div').hide();
    <?php }else{ ?>
        $('.validity_div').show();

    <?php } }else{ ?>
        $('.validity_div').hide();
    <?php } ?>
    
    $(document).on('change','#exam_validity',function(){
        var validity = $(this).children('option:selected').val();

        if(validity == 'Y'){
            $('.validity_div').show();
        }else{
            $('.validity_div').hide();
        }

    })
</script>
@endsection

@endsection    