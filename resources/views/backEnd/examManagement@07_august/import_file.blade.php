@extends('backEnd.layouts.master')
@section('title','Import File')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Import File</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/import/file/'.$exam_id) }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Import File</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('admin/import/file/'.$exam_id)}}" id="import_file_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Import File</h4>
                            
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Subject: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="subject_id" id="subject_id">
                                        <option value="">Choose Subject</option>
                                        @foreach($subjects as $subject)
                                           <option value="{{$subject['id']}}">{{ ucfirst($subject['name']) }}</option>
                                        @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Unit: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="unit_id" id="unit_id">
                                        <option value="">Choose Unit</option>
                                     
                                   </select>
                                </div>
                            </div>
                            
                            <!-- <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                <div class="col-md-7">
                                    
                                    <img src="" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload File</label>
                                <div class="col-md-7">
                                   <input type="file" name="csv_file" id="img_upload">
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#import_file_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            subject_id:{
                required:true,
            },
            unit_id:{
                required:true,
            },
            csv_file:{
                required:true,
            },
            
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'csv' || ext == 'xlsx')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }else{

                    $(this).val('');
                    alert('Please select .csv, .xlsx file format.');
                }
            } 

        });

    });
</script>
<script type="text/javascript">
    $(document).on('change', '#subject_id', function() {
        var subject_id = $(this).val();
        // alert(subject_id);
        if(subject_id!=''){

            $('.loader').show();
            $.ajax({
                 type:"get",
                url: "{{ url('get/units') }}"+"/"+subject_id,
                success:function(resp){
                    $('#unit_id').html(resp);
                    $('.loader').hide();
                }
            })
        }else{
            $('#unit_id').html(' <option value="">Choose Unit</option>');
        }
    });
</script>
@endsection

@endsection    