@extends('backEnd.layouts.master')
@section('title','Exams')
@section('content')
<style type="text/css">
    .card-link {
        padding: 0;
        font-size: 22px;
    }
    .strong {
        font-weight: bold;
        font-size: 18px;
    }
    .comn_spp {
        margin: 0 20px;
        font-size: 17px;
    }
    p.markss {
        margin: 0;
    }
    .edt_sp {
        color: brown;
        cursor: pointer;
        text-decoration: underline;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exams</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/exams') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Question Paper</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!--  -->
                    <div class="card-body">
                        <h5 class="card-title">Question Paper</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/exam/question/'.$exam_id) }}" class="btn btn-primary">Add Question</a>
                        </div>
                        <div class="acrddn add_qutions">
                            <div id="accordion">
                                @if(empty($questions))
                                <div class="card">
                                    <div class="card-header">
                                        <p style="text-align: center;">No Record Found</p>
                                    </div>
                                </div>
                                @endif
                                @if(!empty($questions))
                                    @foreach($questions as $key=>$question)
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link" data-toggle="collapse" href="#collapse{{$key+1}}">
                                              Question {{$key+1}}
                                            </a>
                                            <p class="markss float-right">
                                                <span class="comn_spp mrks_num">Marks: {{$question['marks']}}</span>
                                                <a href="{{url('/admin/exam/question/edit/'.$question['id'].'/'.$exam_id)}}"><span class="comn_spp edt_sp"><i class="fa fa-edit"></i>Edit</span></a>
                                                <a href="{{url('/admin/exam/question/delete/'.$question['id'])}}"><span class="comn_spp edt_sp"><i class="fa fa-trash"></i>
                                                Delete</span></a>
                                            </p>
                                        </div>
                                        <div id="collapse{{$key+1}}" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                <!-- Fields -->
                                                <div class="field_add">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="strong">Question</label>
                                                                <p class="questionttl">
                                                                    @if(!empty($question['question']))
                                                                        {!! ucfirst(@$question['question']) !!}
                                                                    @else
                                                                        <div class="col-md-7">
                                                                            <?php  
                                                                                $image_url = 'javascript:;';
                                                                                if (!empty($question['ques_image'])) {
                                                                                    // dd($student_details->image);

                                                                                    if (file_exists(ExamImageBasePath.'/'.$question['ques_image'])) {
                                                                                        $image = ExamImageImgPath.'/'.$question['ques_image'];
                                                                                        $image_url = ExamImageImgPath.'/'.$question['ques_image'];
                                                                                    }else{
                                                                                        $image = DefaultImgPath;
                                                                                    }
                                                                                }else{
                                                                                    $image = DefaultImgPath;
                                                                                }
                                                                            ?>
                                                                            <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                                <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="margin-bottom: 20px;">
                                                                            </a>
                                                                        </div>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        @foreach($question['options'] as $key=>$value)
                                                                
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="strong">Option @if($key==0)
                                                                    A
                                                                    @elseif($key==1)
                                                                        B
                                                                    @elseif($key==2)
                                                                        C
                                                                    @else
                                                                        D
                                                                    @endif</label>
                                                                    <p class="answ_tl">
                                                                        @if(!empty($value['options']))
                                                                            {{ ucfirst(@$value['options']) }}
                                                                        @else
                                                                            <div class="col-md-7">
                                                                                <?php  
                                                                                    $image_url = 'javascript:;';
                                                                                    if (!empty($value['option_image'])) {
                                                                                        // dd($student_details->image);
                                                                                        if (file_exists(ExamImageBasePath.'/'.$value['option_image'])) {
                                                                                            $image = ExamImageImgPath.'/'.$value['option_image'];
                                                                                            $image_url = ExamImageImgPath.'/'.$value['option_image'];
                                                                                        }else{
                                                                                            $image = DefaultImgPath;
                                                                                        }
                                                                                    }else{
                                                                                        $image = DefaultImgPath;
                                                                                    }
                                                                                ?>
                                                                                <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="margin-bottom: 10px;">
                                                                                </a>
                                                                            </div>    
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>  
                                                                  
                                                        @endforeach
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="strong">Correct Answer</label>
                                                                <p class="answ_tl">
                                                                    <strong>Option @if(@$question['correct_answer']=='1')
                                                                        A
                                                                    @elseif(@$question['correct_answer']=='2')
                                                                        B
                                                                    @elseif(@$question['correct_answer']=='3')
                                                                        C
                                                                    @elseif(@$question['correct_answer']=='4')
                                                                        D
                                                                    @endif
                                                                    </strong>- 
                                                                    @if(!empty(@$question['answer_explanation']))
                                                                        {!! ucfirst(@$question['answer_explanation']) !!}
                                                                    @else
                                                                        <?php
                                                                            $image_url = 'javascript:;';
                                                                            if (!empty($question['ans_image'])) {
                                                                                // dd($student_details->image);
                                                                                if (file_exists(ExamImageBasePath.'/'.$question['ans_image'])) {
                                                                                    $image = ExamImageImgPath.'/'.$question['ans_image'];
                                                                                    $image_url = ExamImageImgPath.'/'.$question['ans_image'];
                                                                                }else{
                                                                                    $image = DefaultImgPath;
                                                                                }
                                                                            }else{
                                                                                $image = DefaultImgPath;
                                                                            }
                                                                        ?>
                                                                        <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                            <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="float: right; margin-right: 80%">
                                                                        </a>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fields -->
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                               <!--  <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne2">
                                          Question 2
                                        </a>
                                        <p class="markss float-right">
                                            <span class="comn_spp mrks_num">Marks: 1</span>
                                            <span class="comn_spp edt_sp"><i class="fa fa-pencil"></i> Edit</span>
                                        </p>
                                    </div>
                                    <div id="collapseOne2" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="field_add">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="strong">Question</label>
                                                            <p class="questionttl">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option A</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option B</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option C</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option D</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="strong">Correct Answer</label>
                                                            <p class="answ_tl"><strong>Option C</strong> - consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse" href="#collapseOne3">
                                          Question 3
                                        </a>
                                        <p class="markss float-right">
                                            <span class="comn_spp mrks_num">Marks: 1</span>
                                            <span class="comn_spp edt_sp"><i class="fa fa-pencil"></i> Edit</span>
                                        </p>
                                    </div>
                                    <div id="collapseOne3" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            <div class="field_add">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="strong">Question</label>
                                                            <p class="questionttl">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option A</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option B</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option C</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label class="strong">Option D</label>
                                                            <p class="answ_tl">consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label class="strong">Correct Answer</label>
                                                            <p class="answ_tl"><strong>Option C</strong> - consectetur elit, sed do eiusmod.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>

                    </div>
                   <!--  -->
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    