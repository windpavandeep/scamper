@extends('backEnd.layouts.master')
@section('title','Walkthroughs')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">App Content Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Walkthroughs</h5>
                      <!--   <div class="add_btn">
                            <a href="{{ url('admin/walkthrough/add') }}" class="btn btn-primary">Add Category</a>
                        </div> -->
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($details))
                                        @foreach($details as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['title']) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/walkthrough/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                   
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection