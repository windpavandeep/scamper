<?php
	if(isset($walkthrough_id)){	
		$task = "Edit";
		$form_name = '_edit';
	}
	else{	
		$task = "Add";
		$form_name = '';
	}

?>
@extends('backEnd.layouts.master')
@section('title', $task .' Walkthrough')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Walkthrough</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/walkthroughs') }}">App Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Walkthrough</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="edit"  enctype=multipart/form-data>
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Walkthrough</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Title" name="title" value="{{ isset($walkthrough['title'])? $walkthrough['title']: '' }}">
                                </div>
                            </div>
                            <?php
                                if(!empty($walkthrough->image)){
                                    // echo'<pre>'; print_r($stories->image);die;
                                 // echo'<pre>'; print_r($become_trainer->image);die;
                                    if(file_exists(WalkthroughBasePath.'/'.$walkthrough->image)){
                                        $image = WalkthroughImgPath.'/'.$walkthrough->image;
                                    }else{
                                        $image = DefaultImgPath;
                                    }
                                }else{
                                    $image = DefaultImgPath;
                                }   
                                 // $media_image = systemImgPath.'/'.'index.png';
                            ?>

                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Image :</label>
                                <div class="col-md-3 p-l-15 ">
                                    <img src="{{$image}}"  style="width: 133px; height: 131px;" id="old_image" alt="No image" >
                                </div>
                            </div>                                  
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">New Image :</label>
                                <div class="col-md-6">
                                    <input type="file" onChange="readURL(this);" id="img_upload" name="image" />    
                                </div>
                            </div>    

                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
/*
    $('#Section2_add').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
       
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:30,
            },
            image:{
                required:true,
                accept: "jpg|jpeg|png|gif",
            },
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });*/


    $('#edit').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:100,
            },
       		image:{
       		    accept: "jpg|jpeg|png|gif",
       		},
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });

</script>

<script type="text/javascript">
    $(document).ready(function(){
        readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#old_image')
                        .attr('src', e.target.result)
                        .width(138)
                        .height(131);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
@endsection
@endsection    