
<?php
	if(isset($banner_image_id)){	
		$task = "Edit";
		$form_name = 'edit';
	}
	else{	
		$task = "Add";
		$form_name = 'add';
	}

?>
@extends('backEnd.layouts.master')
@section('title', $task .' Homepage Banner')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Homepage Banner</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/homepage-banners') }}">App Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Homepage Banner</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="{{$form_name}}"  enctype=multipart/form-data>
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Homepage Banner</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="category_id" id="category_id">
                                        <option value="">Choose Category</option>
                                        @if(!empty(@$categories))
	                                    	@foreach($categories as $category)
                                            <option value="{{@$category['id']}}" <?php if(@$banner_images['category_id']==$category['id']){echo'selected';}?>>{{@$category['name']}}</option>
                                        	@endforeach
                                        @endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub Category: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_category_id" id="sub_category">
                                        <option value="">Choose SubCategory</option>
                                        @if(!empty(@$sub_categories))
	                                      	@foreach($sub_categories as $sub_category)
	                                            <option value="{{@$sub_category['id']}}" <?php if(@$banner_images['sub_category_id']==$sub_category['id']){echo'selected';}?>>{{@$sub_category['name']}}</option>
	                                     	@endforeach
                                     	@endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="sub_sub_category_id" id="sub_subcategory">
                                        <option value="">Choose Sub Category-2</option>
                                      
                                     	@if(!empty(@$sub_sub_categories))
	                                      	@foreach($sub_sub_categories as $sub_sub_category)
	                                            <option value="{{@$sub_sub_category['id']}}" <?php if(@$banner_images['sub_sub_category_id']==$sub_sub_category['id']){echo'selected';}?>>{{@$sub_category['name']}}</option>
	                                     	@endforeach
                                     	@endif
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Product: </label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="course_id" id="course_id">
                                        <option value="">Choose Product</option>@if(!empty(@$products))
	                                      	@foreach($products as $product)
	                                            <option value="{{@$product['id']}}" <?php if(@$banner_images['course_id']==$product['id']){echo'selected';}?>>{{@$product['title']}}</option>
	                                     	@endforeach
                                     	@endif
                                    </select> 
                                </div>
                            </div>
                            

                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Image :</label>
                                <div class="col-md-3 p-l-15 ">
                                    <img src="{{$image}}"  style="width: 133px; height: 131px;" id="old_image" alt="No image" >
                                </div>
                            </div>                                  
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">New Image :</label>
                                <div class="col-md-6">
                                    <input type="file" onChange="readURL(this);" id="img_upload" name="image" />    
                                </div>
                                <div class="col-md-offset-3 col-md-12">
                                    <div class="alert-warning" style="padding: 5px;margin-right: 35%;margin-top: 1%;margin-left: 255px;">
                                        <span> <i class="fa fa-warning m-r-5"> </i> Recommended size of the image is 800 x 500 (W x H) pixels </span>   
                                    </div>  
                                </div>
                            </div>    
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>

    $('#add').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
       
        rules:{
            course_id:{
                required:true,
               
            },
            category_id:{
            	required:true,
            },
            sub_category_id:{
            	required:true,
            },
            sub_sub_category_id:{
            	required:true,
            },
            image:{
                required:true,
                accept: "jpg|jpeg|png",
            },
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });


    $('#edit').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            course_id:{
                required:true,
            },
            category_id:{
            	required:true,
            },
            sub_category_id:{
            	required:true,
            },
            sub_sub_category_id:{
            	required:true,
            },
       		image:{
       		    accept: "jpg|jpeg|png",
       		},
        },
        messages:{
            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });

</script>

<script type="text/javascript">
    $(document).ready(function(){
        readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#old_image')
                        .attr('src', e.target.result)
                        .width(138)
                        .height(131);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
    <script type="text/javascript">
        $('#category_id').on("change",function(){
            var category_id = $(this).val();
            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/sub-categories') }}"+"/"+category_id,
                success:function(resp){
                    $('#sub_category').html(resp.sub_category);
                    
                    $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                    $('#course_id').html('<option data-display="Choose Product" value="">Choose Product</option>');
                    $('.loader').hide();
                }
            })
        });
    </script>
    <script type="text/javascript">

        $('#sub_category').on('change', function() {
            $('.loader').show();
            var sc = $(this).val();
            $.ajax({
                type:"get",
                url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
                success:function(resp){

                    $('#sub_subcategory').html(resp);
                    // $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                    $('.loader').hide();
                    $('#course_id').html('<option data-display="Choose Product" value="">Choose Product</option>');
                }
            })
        });
    </script>
    <script type="text/javascript">

        $('#sub_subcategory').on('change', function() {
            $('.loader').show();
            var sc = $(this).val();
            $.ajax({
                type:"get",
                url: "{{ url('/admin/get/products') }}",
                data:{
                    sc:sc,
                },
                success:function(resp){
                    // alert(resp);
                    $('#course_id').html(resp);
                    $('.loader').hide();
                }
            })
        });
    </script>
@endsection
@endsection    