@extends('backEnd.layouts.master')
@section('title','Homepage Banners')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">App Content Management</h4>
             
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Homepage Banners</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/homepage-banner/add') }}" class="btn btn-primary">Add Homepage Banner</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($details))
                                        @foreach($details as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['title']) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/homepage-banner/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/homepage-banner/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection