@extends('backEnd.layouts.master')
@section('title','Exam Details')
@section('content')
<style type="text/css">
    .labe_data{
        margin-top: 8px
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Student Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/students') }}">Student Management</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/student/exam/'.$user_exam->user_id) }}">Exams</a></li>
                            <li class="breadcrumb-item active" aria-current="page"> Exam Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  
                        <div class="card-body">
                            <h4 class="card-title">Exam Details</h4>
                            @if(!empty(@$user_exam))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam Name: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{ucfirst(@$user_exam->name)}}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Marks Obtained: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{ @$user_exam['marks_obtained'] }}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Marks: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{ @$user_exam['total_marks'] }}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Set Name: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{ @$user_exam['set_name'] }}</label>
                                    </div>
                                </div>
                           
                               
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Question Attempt: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{ isset($user_exam['total_attempt'])?$user_exam['total_attempt']:'0' }}</label>
                                    </div>
                                </div>
                        
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Duration: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">
                                            <?php
                                                if(!empty(@$user_exam['duration'])){
                                                    $time = (int)(date('i',@$user_exam['duration'] / 1000));
                                                    if($time<'10'){
                                                        echo $time.' minuet';
                                                    }else{
                                                        echo $time.' minuets';
                                                    }
                                                }else{
                                                    echo'0 minuet';
                                                }
                                            ?>
                                    </label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam Start Date: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['start_date']))}}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam End Date: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['expiry_date']))}}</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam Given On: </label>
                                    <div class="col-sm-7">
                                        <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['start_date_time']))}}</label>
                                    </div>
                                </div>
                            @else
                                <h4><center>No exam details found</center></h4>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    