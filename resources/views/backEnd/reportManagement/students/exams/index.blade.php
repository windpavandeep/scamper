@extends('backEnd.layouts.master')
@section('title','Students')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Student Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/admin/students')}}">Student Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Exams</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Exams</h5>
                        <!-- <form method="post" action="{{url('/admin/student/analysis/report')}}" id="form_data"> -->
                            <div class="add_btn">
                                <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                Export <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right export-import" role="menu">
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="excel">Export to Excel</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="csv">Export to CSV</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="pdf">Export to PDF</a>
                                    </li>
                                </ul>
                                <!-- <a href="{{ url('admin/student/add') }}" class="btn btn-primary">Add Student</a> -->
                            </div>
                            <!-- <input type="hidden" name="export_type" value="" id="export_type"> -->
                            <!-- <input type="hidden" name="student_id" value="{{@$student_id}}"> -->
                            <!-- @csrf -->
                        <!-- </form> -->
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Exam Name</th>
                                        <th>Marks Obtained</th>
                                        <th>Total Marks</th>
                                        <th>Set Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($user_exams))
                                        @foreach($user_exams as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>{{ $value['marks_obtained'] }}</td>
                                                <td>{{ $value['total_marks'] }}</td>
                                                <td>{{ $value['set_name'] }}</td>
                                                
                                                <td>
                                                   <!--  <a href="{{ url('/admin/student/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/student/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ url('admin/student/credential/mail/'.$value['id']) }}" title="Send Credential"><i class="fa fa-envelope"></i></a> -->
                                                     <a href="{{ url('admin/student/exam/detail/'.$value['id']) }}" title="Exam Detail"><i class="fa fa-eye"></i></a>
                                                   
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="form_modal2" class="modal fade" role="dialog" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                   <h4 class="modal-title">Export</h4>
               </div>
               <form action="{{url('/admin/student/analysis/report')}}" class="form-horizontal" method="post" id="filter_modal">
                   <div class="modal-body">
                       <div class="form-group">
                           <div class="col-md-10">
                               <div class="form-group row">
                                   <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Date: </label>
                                   <div class="col-sm-7">
                                       <input type="date" class="form-control date_range" id="date" name="date" value="" placeholder="Select interval">
                                   </div>
                               </div>
                           </div>
                        
                       </div>
                   </div>
                   <input type="hidden" name="export_type" value="" id="export_type">
                   <input type="hidden" name="student_id" value="{{@$student_id}}">
                   @csrf
                   <div class="modal-footer">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Close</button>
                       <button class="btn btn-primary" id="export_btn" type="submit">Export to </button>
                   </div>
               </form>
           </div>
       </div>
   </div>
    @include('backEnd.common.footer')
</div>
@endsection
@section('scripts')

    <script>
        $('.report_optn').click(function(){
            var value  = $(this).attr('type');
            $('#export_type').val(value);
            $('#form_data').submit();

            if(value == 'excel'){
                $('#export_btn').text('Export to excel');
                $('#form_modal2').modal('show');
            }else if(value == 'pdf'){
                $('#export_btn').text('Export to pdf');
                $('#form_modal2').modal('show');
            }else{
                // $('#filter_modal').attr('action',csv_url);
                $('#export_btn').text('Export to csv');
                $('#form_modal2').modal('show');
            }
        });
    </script>
@endsection