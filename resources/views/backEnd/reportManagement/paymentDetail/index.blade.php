@extends('backEnd.layouts.master')
@section('title','Pending Payments')
@section('content')
<style type="text/css">
    .payment_button{
        text-align:right;
        margin-top: 20px;
        margin-right: 20px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Payment Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Admin Earnings</h5>
                            <h5 class="card-title">Total Earnings:  {{CURRENCY}}{{number_format($total_earning,2)}}</h5>
                            <div class="add_btn">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                    Export <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right export-import" role="menu" id="export">
                                        <li export_type="excel" class="export_list">
                                            <a href="javascript:;" class="report_optn" type="excel">Export to Excel</a>
                                        </li>
                                        <li export_type="csv" class="export_list"> 
                                            <a href="javascript:;" class="report_optn" type="csv">Export to CSV</a>
                                        </li>
                                        <li export_type="pdf" class="export_list"> 
                                            <a href="javascript:;" class="report_optn" type="pdf">Export to Pdf</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <th>Student Name</th>
                                        <th>Course Title</th>
                                        <th>Received On</th>
                                        <th>Received Amount</th>
                                        <th>Commission</th>
                                        <!-- <th>Paid To Trainer</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($earnings))
                                        @foreach($earnings as $key=>$value)
                                            <tr>
                                                <td>{{ $value['razor_pay_id'] }} </td>
                                                
                                                <!-- <td>{{ ucfirst($value['title']) }}</td> -->
                                                
                                                
                                                <td>
                                                    {{ ucfirst($value['first_name']) }} {{ ucfirst($value['last_name']) }} 
                                                </td>
                                                <td>
                                                    {{ ucfirst($value['title']) }} 
                                                </td>
                                                <td>
                                                    {{ date('d/m/Y',strtotime($value['purchased_on'])) }} 
                                                </td>
                                                <td>
                                                    {{ number_format($value['final_total'],2) }}
                                                </td>
                                                <td>
                                                    {{ 
                                                        number_format($value['admin_commission'],2) 
                                                    }}
                                                </td>
                                                <!-- <td>
                                                    {{ 
                                                        number_format($value['trainer_price'],2) 
                                                    }}
                                                </td> -->
                                                <!-- <td>
                                                    <a href="{{ url('/admin/pending/payment/detail/'.$value['id']) }}" title="Info"><i class="fa fa-eye"></i>
                                                    </a>
                                                    
                                                </td> -->
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    @include('backEnd.common.earning_export_modal')
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#submit_button').on('click',function(e){
/*
            var category = $('#category_id').val();
            var state_id = $('#state_id').val();
            var mydate   = $('#mydate').val();*/
            $('#earning_export_form').submit();
            /*if(category==''&&state_id==''&&mydate==''){
                e.preventDefault();
                $('#all_error').text('Please select any data to export');
            }else{
                $('#all_error').text('');
                $('#earning_export_form').submit();
            }*/
        
    });
</script>
<script type="text/javascript">
    $('.export_list').click(function(){
        var value = $(this).attr('export_type'); 
        $('#export_type').val(value)
        $('#earning_export_id').modal('show');
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.mydatepicker').datepicker({ autoclose: true, format: 'dd-mm-yyyy',});
    });
</script>
@endsection