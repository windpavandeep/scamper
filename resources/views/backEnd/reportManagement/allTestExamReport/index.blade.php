@extends('backEnd.layouts.master')
@section('title','All Test Exams')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exam Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All Test Exams</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Exam Name</th>
                                        <th>Exam Code</th>
                                        <th>Total Students Appeared </th>
                                        <th>Exam Start Date</th>
                                        <th>Exam End Date</th>
                                        <th width="16%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($exams))
                                        @foreach($exams as $value)
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>{{ ucfirst($value['code']) }}</td>
                                                <td>{{ ucfirst($value['user_exams_count']) }}</td>
                                                <td>
                                                    {{ date('d-m-Y',strtotime($value['start_date'])) }}
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y',strtotime($value['expiry_date'])) }}
                                                </td>
                                                <td>
                                           
                                                    <a href="{{ url('/admin/all/exam/student/'.$value['id']) }}" title="Import CSV File"><i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection