@extends('backEnd.layouts.master')
@section('title','Student')
@section('content')
<style type="text/css">
    .labe_data{
        margin-top: 8px
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exam Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/all/test/exams') }}">All Test Exams</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/all/exam/student/'.$user_exam->exam_id) }}">All Test Exam Students</a></li>
                            <li class="breadcrumb-item">All Test Student Exam Detail</li>
                        <!--     <li class="breadcrumb-item active" aria-current="page"> Student</li> -->
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                  
                        <div class="card-body">
                            <h4 class="card-title">All Test Student Exam Detail</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Student Name: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ucfirst(@$user_exam->first_name)}} {{ucfirst(@$user_exam->last_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ucfirst(@$user_exam->email)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact Number: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ucfirst(@$user_exam->contact)}}</label>
                                </div>
                            </div>
                       
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Marks Obtained: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">
                                        @if(!empty(@$user_exam['marks_obtained']))
                                            {{ $user_exam['marks_obtained'] }}
                                        @else
                                            0
                                        @endif
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Marks: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ @$user_exam['total_marks'] }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Set Name: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ @$user_exam['set_name'] }}</label>
                                </div>
                            </div>
                       
                           
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Question Attempt: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{ isset($user_exam['total_attempt'])?$user_exam['total_attempt']:'0' }}</label>
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Duration: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{(int)(date('i',@$user_exam['duration'] / 1000))}} minuets</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam Start Date: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['start_date']))}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam End Date: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['expiry_date']))}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Exam Given On: </label>
                                <div class="col-sm-7">
                                    <label class="labe_data">{{date('d-m-Y',strtotime($user_exam['start_date_time']))}}</label>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    