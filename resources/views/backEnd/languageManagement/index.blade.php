@extends('backEnd.layouts.master')
@section('title','Language')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Language Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Language</h5>
                        <div class="add_btn">
                            <!-- <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                Export <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right export-import" role="menu">
                                    <li>
                                        <a href="{{url('admin/companies/export?excel')}}" class="report_optn" type="excel">Export to Excel</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export?csv')}}" class="report_optn" type="csv">Export to CSV</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export/pdf')}}" class="report_optn" type="pdf">Export to PDF</a>
                                    </li>
                                </ul>
                            </div> -->
                            <a href="{{ url('admin/language/add') }}" class="btn btn-primary">Add Language</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($languages))
                                        @foreach($languages as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/language/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/language/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection