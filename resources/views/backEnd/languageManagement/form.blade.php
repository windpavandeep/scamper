<?php 
    if(isset($language_id)){
        $task    = 'Edit';
        $id      = "language_edit_form";
        $action  =  url('/admin/language/edit/'.$language_id);
    }else{
        $task    = 'Add';
        $id      = "language_add_form";
        $action  =  url('/admin/language/add');
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Language')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Language</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/language') }}">Language Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Language</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="{{$id}}">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Language</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ isset($language['name'])? $language['name']: '' }}">
                                </div>
                            </div>
                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#language_add_form');
    form.validate({
        // errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/,
                remote: "{{ url('admin/check/language/name')}}"
            },
            
        },
        messages:{
            name: {
                remote: "This name is already exist"
            }
        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<?php if(isset($language_id)){ ?>
<script>
    var form = $('#language_edit_form');
    var language_id = "{{@$language_id}}";
    form.validate({
        // errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/,
                remote: "{{ url('admin/check/edit/language/name')}}"+'/'+language_id
            },
            
        },
        messages:{
            name: {
                remote: "This name is already exist"
            }
        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<?php } ?>
@endsection

@endsection    