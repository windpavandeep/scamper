<?php 
    if(isset($course_id)){
        $task    = 'Edit';
        $form_id = 'edit_course_form';
        $action  =  url('/admin/course/edit/'.$course_id);
        $disabled= 'disabled';
    }else{
        $task    = 'Add';
        $form_id = 'add_course_form';
        $action  =  url('/admin/course/add');
        $disabled= '';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Product')
@section('content')
<style type="text/css">
    .plus-icn {
        font-size: 30px;
        line-height: 1.1;
        color: #27a9e3;
    }
    .error2{
        color: red;
    }

    .ms-parent {
        width: 215px !important;
    }
</style>
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>

<script src="{{url( backEndJsPath.'/multiple-select.min.js')}}"></script>
<script src="{{url( backEndJsPath.'/multiple-select.js')}}"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Product</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/courses') }}">Product Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Product</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="{{$form_id}}" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Product</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                       <option value="">Choose Category</option>
                                       @foreach($categories as $category)
                                           <option value="{{ $category['id'] }}" <?php if(@$category['id']==@$course_detail['category_id']){echo'selected';}?>>{{ucfirst($category['name'])}}</option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                        <option data-display="Choose Category" value="">    Choose Sub-Category</option>
                                        @if(!empty($sub_category))
                                            @foreach($sub_category as $value)
                                                <option value="{{$value['id']}}" <?php if(@$value['id']==@$course_detail['sub_category_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2:</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                         <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                            <option data-display="Choose Category" value="" readonly>Choose Sub-Category 2</option>
                                            @if(!empty($sub_sub_category))
                                                @foreach($sub_sub_category as $value)
                                                    <option value="{{$value['id']}}" <?php if(@$value['id']==@$course_detail['sub_subcategory_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subject:</label>
                                <div class="col-md-7">
                                    <div class="table-responsive" >
                                        <table id="" class="table table-striped table-bordered">
                                            <thead>
                                                <tr count="1">
                                                    <th width="40%">Subject</th>
                                                    <th>Unit</th>
                                                    <th></th>                              
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <span class="error1" id="subject_div_error"></span>
                                                @if(!empty(@$course_detail['content_subjects']))
                                                    @if(!@$course_detail['content_subjects']->isEmpty())
                                                        @foreach(@$course_detail['content_subjects'] as $key=>$value)

                                                            <?php
                                                                $arr = [];
                                                                $unit_ids = array_map(function($q) use($arr){ 
                                                                          $arr = $q['unit_id'];
                                                                          return $arr;
                                                                           }, $value['content_unit']->toArray());
                                                                // echo'<pre>'; print_r($unit_ids);die;
                                                            ?>
                                                            <tr id="subject_div" class="subject_div_class" subject_attr="{{$key}}">
                                                                <td>
                                                                    
                                                                    <div class="form-group in-group">
                                                                        <span class="error1"></span>
                                                                        <select class="form-control subject_id" id="subject_id" name="content[{{$key}}][subject]" required="">
                                                                           
                                                                            @foreach(@$subjects as $subject)
                                                                                <option value="{{ $subject['id'] }}" <?php if(@$subject['id']==@$value['subject_id']){echo'selected';}?>>{{ ucfirst($subject['name']) }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td id="unit_div">
                                                                    <div class="form-group in-group">
                                                                        <span class="error2"></span>
                                                                        <select class="form-control unit_id unit_class random" id="unit_id" name="content[{{$key}}][unit_id][]" multiple="" required="">
                                                                            @foreach($units as $key1=>$unit)
                                                                                @if(($unit['subject_id']==$value['subject_id']))
                                                                                    <option value="{{ $unit['id'] }}" <?php if(in_array($unit['id'], $unit_ids)){echo'selected';}?>>{{ ucfirst($unit['name']) }}</option>
                                                                                @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                @if(@$key==0)
                                                                    <a href="javascript:;" id="plus-icn"><i class="mdi mdi-plus-box plus-icn" title="Add New"></i></i></td>
                                                                @else
                                                                    <a href="javascript:;" id="plus-icn" class="remove-subject"><i class="mdi mdi-minus-box plus-icn" title="Delete"></i></i></td>
                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr id="subject_div" class="subject_div_class" subject_attr="1">
                                                            <td>
                                                                <div class="form-group in-group">
                                                                    <span class="error1" id="subject_error"></span>
                                                                    <select class="form-control subject_id" id="subject_id" name="content[subject_id]" required="">
                                                                        <option value="" readonly>Choose Subject</option>
                                                                       
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td id="unit_div">
                                                                <div class="form-group in-group">
                                                                    <span class="error2" id="unit_error"></span>
                                                                    <select class="form-control unit_id unit_class js-example-basic-multiple random" id="unit_id" name="content[subject_id][unit_id][]" multiple="" required="">
                                                                      
                                                                        
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td><a href="javascript:;" id="plus-icn"><i class="mdi mdi-plus-box plus-icn" title="Add New"></i></i></td>
                                                        </tr>
                                                    @endif
                                                @else
                                                    <tr id="subject_div" class="subject_div_class" subject_attr="1">
                                                        <td>
                                                            <div class="form-group in-group">
                                                                <span class="error1" id="subject_error"></span>
                                                                <select class="form-control subject_id" id="subject_id" name="content[subject_id]" required="">
                                                                    <option value="" readonly>Choose Subject</option>
                                                                   
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td id="unit_div">
                                                            <div class="form-group in-group">
                                                                <span class="error2" id="unit_error"></span>
                                                                <select class="form-control unit_id unit_class js-example-basic-multiple random" id="unit_id" name="content[subject_id][unit_id][]" multiple="" required="">
                                                                  
                                                                    
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td><a href="javascript:;" id="plus-icn"><i class="mdi mdi-plus-box plus-icn" title="Add New"></i></i></td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Language: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="language_id">
                                        <option data-display="Choose Category" value="" readonly>Choose language</option>
                                        @foreach($languages as $language)
                                            <option value="{{ $language['id'] }}"  <?php if(@$course_detail['language_id']==$language['id']){echo'selected';}?>>{{ucfirst($language['name'])}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Title:</label>
                                <div class="col-md-7">
                                    <input type="text" name="course_title" class="form-control" placeholder="Title" value="{{ isset($course_detail->title)?$course_detail->title:''}}">
                                </div>
                            </div>
                        
                            @if(!empty($course_detail['course_files']))
                                @if(!($course_detail['course_files']->isEmpty())) 
                                    <div class="form-group row"> 
                                        <label for="lname34" class="col-sm-3 text-right control-label col-form-label">Uploaded Files</label>
                                        <div class="col-md-7">
                                           
                                            @foreach($course_detail['course_files'] as $key => $value )
                                                <?php  
                                                    
                                                    $file     = DefaultImgPath; 
                                                    $file_url = 'javascript:;';
                                                    if(!empty($value['file'])) {
                                                        if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
                                                            $file_url = TrainerContentImgPath.'/'.$value['file'];
                                                            $file_name = pathinfo($value['file']);
                                                            $ext = $file_name['extension'];
                                                            if($ext=='mov'||$ext=='mp4'||$ext=='avi') {
                                                                $file = DefaultVideoImgPath;;  
                                                            }else if($ext=='pdf'){
                                                                $file = DefaultPdfImgPath;
                                                            }
                                                        }
                                                    }   
                                                    $count = count($course_detail['course_files']);
                                                    // dd($image_url);
                                                ?>
                                                <div class="flex-item">
                                                    <div class="phot-upld">
                                                        @if($count>1)
                                                        <a href="{{ url('/admin/course/file/delete/'.$value['id']) }}" title="Delete" class="btn btn-remov del_btn">
                                                        <i class="fa fa-times" style="float:right"></i>
                                                        </a>
                                                        @endif
                                                        <a href="{{$file_url}}" target="_blank"> 
                                                            <img src="{{$file}}"  width="120px" height="120px"  class="img-responsive modal-upload-image-preview">
                                                        </a>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    
                                @endif
                            @endif
                       
                            <div class="form-group row" id="video_div" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Upload File: </label>
                                <div class="col-sm-7">
                                    <span class="error" id="vide_req" style="color:red"></span>
                                    <!-- <label>Upload Video</label> -->
                                    <div class="write_review_modal_image_container rvw_upld">
                                        
                                        <span id="img-error" style="color: red;"></span>
                                        <div class="photos-sec upload_video_sec">
                                            <div class="flex image_upload_class upload_video">
                                            </div>
                                        </div>
                                        <div class="add-phots">
                                            <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload New File</a>
                                            <input type="file" name="file[]" id="video_id" class=" review_modal_video review_modal_video_css id_proof_document rem0" multiple>
                                        </div>
                                    </div>        
                                    <!-- <input type="file" class="form-control" name="video[]" placeholder="Upload Video" accept=".mp4,.mov,.avi" id="video_file" multiple=""> -->
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Content Type: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="content_type" id="content_type">
                                        <option value="free" selected="" <?php if(@$course_detail['content_availability']=='free'){echo'selected';}?>>Free</option>
                                        <option value="paid" <?php if(@$course_detail['content_availability']=='paid'){echo'selected';}?>>Paid</option>
                                        <option value="subscriber" <?php if(@$course_detail['content_availability']=='subscriber'){echo'selected';}?>>Subscriber</option>
                                    </select>
                                </div>
                            </div>
                     
                            <div class="form-group row" id="paid_amount_container">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image12">Paid</label>
                                <div class="col-md-7">
                                <span class="error" id="payment_error" style="color:red"></span>
                                    <input type="text" name="paid_amount" class="form-control" placeholder="Paid" id="payment" value="{{ isset($course_detail->paid_amount)?$course_detail->paid_amount:''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">{{$task}} Description:</label>
                                <div class="col-md-7">
                                   <textarea class="form-control ckeditor" name="description" id="description" placeholder="Add Description of the content why users should buy this?">{{ isset($course_detail->description)?$course_detail->description:''}}</textarea>
                                     
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Give Free Questions: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="free_question" id="content_type">
                                        <option value="Y" selected="" <?php if(@$course_detail['free_question']=='Y'){echo'selected';}?>>Yes</option>
                                        <option value="N" <?php if(@$course_detail['free_question']=='N'){echo'selected';}?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">No Of Exams:</label>
                                <div class="col-md-7">
                                    <input type="text" name="no_of_exams" class="form-control" placeholder="No Of Exams" value="{{ isset($course_detail->no_of_exams)?$course_detail->no_of_exams:''}}">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Show Answer After Exams: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="show_answer_after_exam" id="content_type">
                                        <option value="Y" selected="" <?php if(@$course_detail['show_answer_after_exam']=='Y'){echo'selected';}?>>Yes</option>
                                        <option value="N" <?php if(@$course_detail['show_answer_after_exam']=='N'){echo'selected';}?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Show Subscriber Video: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="subscriber_video" id="content_type">
                                        <option value="Y" selected="" <?php if(@$course_detail['subscriber_video ']=='Y'){echo'selected';}?>>Yes</option>
                                        <option value="N" <?php if(@$course_detail['subscriber_video']=='N'){echo'selected';}?>>No</option>
                                    </select>
                                </div>
                            </div>
                            @if(@$course_detail->include_gst)
                                <?php
                                    $checked = '';
                                    if(@$course_detail->include_gst=='Y'){
                                        $checked = 'checked data-toggle';
                                    }
                                ?>
                                <div class="form-group row gst_class">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Include GST:</label>
                                    <div class="col-md-7">
                                       <span class="category_place" category_place_id="" title="More">
                                            <input data-toggle="toggle" type="checkbox" data-on="Yes" data-off="No" name="include_gst" {{$checked}}>
                                        </span>
                                    </div>
                                </div>
                            @else
                                <div class="form-group row gst_class">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Include GST:</label>
                                    <div class="col-md-7">
                                       <span class="category_place" category_place_id="" title="More">
                                            <input data-toggle="toggle" type="checkbox" data-on="Yes" data-off="No" name="include_gst" checked data-toggle="toggle">
                                        </span>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Start Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="Start Date" autocomplete="off" name="start_date" id="start_date" value="{{isset($course_detail->start_date)?date('d-m-Y',strtotime($course_detail->start_date)) :''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">End Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="End Date" autocomplete="off" name="end_date" id="end_date" value="{{isset($course_detail->end_date)?date('d-m-Y',strtotime($course_detail->end_date)) :''}}">
                                    <span id="end_date_error" class="error1"></span>
                                </div>
                            </div>
                            @if(!empty($course_detail['course_images']))
                                @if(!($course_detail['course_images']->isEmpty())) 
                                    
                                        <div class="form-group row"> 
                                            <label for="lname34" class="col-sm-3 text-right control-label col-form-label">Uploaded Images</label>
                                            <div class="col-md-7">
                                               
                                                @foreach($course_detail['course_images'] as $key => $value )
                                                    <?php  
                                                        
                                                        $image = DefaultImgPath; 
                                                        $image_url = 'javascript:;';
                                                        if(!empty($value['name'])) {
                                                            if(file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                                                                $image_url = TrainerContentImageImgPath.'/'.$value['name'];
                                                                $image_name = pathinfo($value['name']);
                                                                $ext = $image_name['extension'];
                                                                if($ext=='jpg'||$ext=='jpeg'||$ext=='png') {
                                                                    $image = TrainerContentImageImgPath.'/'.$value['name'];  
                                                                }
                                                            }
                                                        } 
                                                        $image_count = count($course_detail['course_images']);
                                                        // dd($image_url);
                                                    ?>
                                                    <div class="flex-item">
                                                        <div class="phot-upld">
                                                            @if($image_count>'1')
                                                                <a href="{{ url('/admin/course/image/delete/'.$value['id']) }}" title="Delete" class="btn btn-remov del_btn">
                                                                <i class="fa fa-times" style="float:right"></i>
                                                                </a>
                                                            @endif
                                                            <a href="{{$image_url}}" target="_blank"> 
                                                                <img src="{{$image}}"  width="120px" height="120px"  class="img-responsive modal-upload-image-preview">
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                @endif
                            @endif
                            <div class="form-group row">
                                <label for="lname12" class="col-sm-3 text-right control-label col-form-label">Upload Images:</label>
                                <div class="col-sm-7">
                                    <div class="write_review_modal_image_container rvw_upld">
                                        
                                        <span id="img-error" style="color: red;"></span>
                                        <div class="photos-sec photos-sec_image">
                                            <div class="flex image_upload_class image_class">
                                            </div>
                                        </div>
                                        <div class="add-phots">
                                            <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload new Image</a>
                                            <input type="file" name="images[]" id="id_proof" class="review_modal_image id_proof_document rem0" multiple>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px" id="bt_submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.unit_class').multipleSelect();
     });   
</script>

<script>
    $(document).ready(function(){
    var form = $('#add_course_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { 
            if(element.attr("name") == "images[]") {
                element.parent().before(error); 
            }else if(element.attr("name")=="sub_subcategory_id"){
                element.parent().before(error); 
            }else{

                element.before(error); 
            }
        },
        ignore: [],
        rules:{

            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
            sub_subcategory_id:{
                required:true,
            },
            course_title:{
               required:true,
               minlength:2,
               maxlength:100,
            },
            upload_optn:{
                required:true,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:5000,
            },
            language_id:{
                required:true,
            },
            no_of_exams:{
                // required:true,
                // min:1,
                digits:true,
                maxlength:7,
            },
            paid_amount:{
                min:1,
                digits:true,
                maxlength:7,
            },
            content_type:{
                required:true,
            },
            "images[]":{
                required:true,
            },
            start_date:{
                required:true,
            },
            end_date:{
                required:true,
            },
        },
        submitHandler:function(form){
            var content_type = $('#content_type').val();
            var paid_amount  = $('#payment').val();
            var start_date   = $('#start_date').val();
            var end_date     = $('#end_date').val();

            var err = 0;
            /*$('.subject_id').each(function(key){
                var field_data = $(this).val();
                // alert
                var current    = $(this);

                if(field_data==''){
                    err = 1;
                    current.prev('span').text('*This field is required.');
                }else{
                    err = 0;
                    current.prev('span').text('');
                }
            });
*/
           /* $('.unit_class').each(function(key){

                var field_data = $(this).val();
                var field_data2 = $(this).text();
                var current    = $(this);
                alert(field_data2);
                if(field_data=='' && field_data2!='All selected'){
                    err = 1;
                    // alert('1');
                    current.prev('span').html('*This field is required.');
                }else{
                    err = 0;
                    // alert('0');
                    current.prev('span').html('');
                }
                // alert(err);
            });*/
                // alert(err);

            $('#end_date_error').text('');
            if(end_date<=start_date){
                $('#end_date_error').text('End date should not be equal or less than start date');
                return false;
            }
           
            $('#vide_req').text('');
      
            var video_file = $("#video_file")[0].files.length;
            var video_file = $('.upload_video_sec > .upload_video > div').length;
            // alert(video_file);
            if(video_file==0){
                $('#vide_req').text('*This field is required.');
                return false;
            }
            
            $('#payment_error').text('');
            if(content_type=='paid'){
                // alert(paid_amount);
                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }
            // alert(content_type);
           
            if(err==0){
                
                form.submit();
            }
        },
    });
});
</script>
<script>
    $(document).ready(function(){


    var form = $('#edit_course_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { 
            if(element.attr("name") == "images[]") {
                element.parent().before(error); 
            }else{

                element.before(error); 
            }
        },
        ignore: [],
        rules:{

            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
            sub_subcategory_id:{
                required:true,
            },
            course_title:{
               required:true,
               minlength:2,
               maxlength:100,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:5000,
            },
            language_id:{
                required:true,
            },
            no_of_exams:{
                // required:true,
                // min:1,
                digits:true,
                maxlength:7,
            },
            paid_amount:{
                // min:1,
                digits:true,
                maxlength:7,
            },
            start_date:{
                required:true,
            },
            end_date:{
                required:true,
            },
        },
        submitHandler:function(form){
            var content_type = $('#content_type').val();
            var paid_amount  = $('#payment').val();
            var start_date   = $('#start_date').val();
            var end_date     = $('#end_date').val();

            // var err = 0;
           // $('.subject_id').each(function(key){
           //      var field_data = $(this).val();
           //      // alert
           //      var current    = $(this);

           //      if(field_data==''){
           //          err = 1;
           //          current.prev('span').text('*This field is required.');
           //      }else{
           //          err = 0;
           //          current.prev('span').text('');
           //      }
           //  });

           /*$('.unit_id').each(function(key){

                var field_data = $(this).val();
                var current    = $(this);
                var error      = $(this).prev('.error2');
                err = 0;
                if(field_data==''){
                    err = 1;
                    current.closest('#subject_div').next('span').text('*This field is required.');
                }else{
                  
                    current.closest('span').html('');
                }
            });*/
            $('#end_date_error').text('');
            if(end_date<=start_date){
                $('#end_date_error').text('End date should not be equal or less than start date');
                return false;
            }
            $('#payment_error').text('');
            if(content_type=='paid'){
                // alert(paid_amount);
                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }

                form.submit();
            // if(err==0){
            // }
        },
    });
});
</script>

<script type="text/javascript">
    $('#content_type').click(function(){
        var value = $(this).val();
        // alert(value);
        $('#paid_amount_container').hide();
        if(value=='paid'){
            $('.gst_class').show();
            $('#paid_amount_container').show();
        }
        if(value=='free'){
            $('.gst_class').hide();
            $('#paid_amount_container').hide();
        }
    });
    $(document).ready(function(){
        var value = $('#content_type').val();
        if(value=='paid'){
            $('.gst_class').show();
            $('#paid_amount_container').show();        
            
        }else{
            $('.gst_class').hide();
            $('#paid_amount_container').hide();
        }
    })
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_image', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='jpg' || extension=='jpeg' || extension=='png')){
                dataURL = reader.result;
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only jpg,jpeg,png file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld img');
            output.src = dataURL;
            var len = $('.photos-sec_image > .flex > div').length;
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.photos-sec> .image_class').append('<div class="flex-item uploaded-section"><div class="phot-upld"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="images['+inc_val+']" class="review_modal_image rem'+inc_val+' id_proof">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }                    
            
           
            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.photos-sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_video', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='mp4' || extension=='mov' || extension=='avi')){
                dataURL = "{{DefaultVideoImgPath}}";
            }else if((extension=='pdf')){
                dataURL = "{{DefaultPdfImgPath}}";
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only mp4, mov, avi, pdf file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld_video img');
            output.src = dataURL;
            var len = $('.upload_video_sec > .upload_video > div').length;
            // alert(len);
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.upload_video_sec> .upload_video').append('<div class="flex-item uploaded-section"><div class="phot-upld  phot-upld_video"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="file['+inc_val+']" class="review_modal_video review_modal_video_css rem'+inc_val+' video_id">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }
            /*}else{
                $('#img-error').text('Maximum file size can be 2 MB');
            }*/                       
            
            //this_addr.after('<input type="file" name="upld-gal-hidden['+inc_val+']" accept="image/*" class="review_modal_image" id="upld-gal-hidden['+inc_val+']">')

            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.upload_video_sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>

<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        if(category_id==''){
            category_id = 0;
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                // $('#unit_id').html(resp.units);
                $('tbody').children('tr:not(:first)').remove();
                $('#subject_id').html('<option value="" readonly>Choose Subject</option>');
                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        if(sc==''){
            sc = 0;
        }
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('tbody').children('tr:not(:first)').remove();
                $('#subject_id').html('<option value="" readonly>Choose Subject</option>');
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.dtpckr').datepicker({ format: 'dd-mm-yyyy' });
    });
</script>

<script type="text/javascript">
    $('#plus-icn').click(function(){
        // $('.subject_id').find('option').show();
        var current             = $(this);
        var tr_count            = $('tr').length;
        var sub_sub_category_id = $('#sub_subcategory').val();
        // alert(count);
        
        // var subject_id =current.closest('#subject_div').find('#subject_id  option:selected').val();
        
        // alert(subject_id);
        $('#subject_div_error').text('');
        if(sub_sub_category_id==''){

            $('#subject_div_error').text('Please select sub-category 2 to add new');
        }
        if(sub_sub_category_id!=''){

            $('.loader').show();
            $.ajax({
                type:"get",
                url: "{{ url('admin/subject/content') }}"+'/'+tr_count+'/'+sub_sub_category_id,
                success:function(resp){
                    // alert(resp);
                    
                    // $('tr:last').after(resp);
                    $(resp).insertAfter('tr:last');
                    $('tr:last').attr('subject_attr',tr_count+1);
                    $('.subject_id  option:selected').each(function() {
                       var value = $(this).val();

                       if(value!=''){

                        // $('.subject_id').not($(this)).find('option[value="' + value + '"]').hide()
                        $('tr:last').find('option[value="' + value + '"]').hide();
                       }
                   });
                    $('.loader').hide();
                    // $(resp).insertAfter('.subject_div_class');
                },
            });
        }

    });
    $(document).ready(function(){

        $(document).on('click', '.remove-subject', function() {
              
            var value = $(this).closest('#subject_div').find('#subject_id  option:selected').val();
            $('.subject_id').not(this).find('option[value="' + value + '"]').show();
            // alert(value);
            $(this).closest('tr').remove();
        });
    });
    
</script>
<script type="text/javascript">
    $(document).on('change', '.subject_id', function() {
        var current    = $(this);
        var subject_id = $(this).val();
        var value = current.closest('#subject_div').attr('subject_attr');

        // alert(value);
        // var random_id = Math.floor((Math.random() * 155099*subject_id) + 1);
        value = value-1;
        current.closest('select').attr('name', 'content['+value+'][subject]');
        current.closest('tr').find('#unit_id').attr('name', 'content['+value+'][unit_id][]');
        $('.loader').show();
        if(subject_id!=''){
            $('.subject_id').find('option').show();
            $.ajax({
                type:"get",
                url: "{{ url('get/units') }}"+"/"+subject_id,
                
                success:function(resp){
                    current.closest('tr').find('#unit_id').html(resp);
                    $('.subject_id  option:selected').each(function() {
                        var value = $(this).val();

                        if(value!=''){
                           

                            $('.subject_id').not(this).find('option[value="' + value + '"]').hide()
                           
                            // alert(value)
                        }
                          
                    });
                
                    current.closest('tr').find('#unit_id').multipleSelect('refresh');

                   
                },
            });
        }else{
            current.closest('tr').find('#unit_id').html('<option value="" readonly>Choose Unit</option>');
        }
        $('.loader').hide();
    });

</script>

<!-- <script type="text/javascript">
    $(document).on('click','.unit_class',function(){
        var unit_id = $(this).val();
        var current = $(this);
        if(unit_id!=''){
        alert(unit_id);

            current.closest('#subject_div').closest('#subject_div').find("#unit_id option[value =" + unit_id + "]").hide();
        }
    });
</script> -->
<script type="text/javascript">
    $(document).on('click','#bt_submit',function(){

        $('#unit_id').each(function() {
        $(this).rules('add', {
            required: true,
            messages: {
                required:  "*This field is required.",
            },
        });
        });
         
    });   

</script>
<script type="text/javascript">
    $('#sub_subcategory').on('change',function(){


        var value = $(this).val();
        
        $('tbody').children( 'tr:not(:first)' ).remove();
        $('#unit_id').multipleSelect('refresh');
        if(value==''){

            value = '0';
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/subjects') }}"+"/"+value,
            
            success:function(resp){

                $('#subject_id').html(resp);
                $('.loader').hide();
            },
        });
        
    });
</script>
@endsection

@endsection    