@extends('backEnd.layouts.master')
@section('title','New Signup Requests')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Teacher Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">New Signup Requests</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <tr role="row" class="heading">
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th width="20%">Email</th>
                                            <th>Contact Number</th>
                                            <th>Admin Approval</th>
                                            <th> Action </th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($trainers))
                                        @foreach($trainers as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['first_name']) }}</td>
                                                <td>{{ $value['last_name'] }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ $value['contact'] }}</td>

                                                <td>
                                                    @if($value['admin_approval']=='P')
                                                        Pending
                                                    @else
                                                        Rejected
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/trainer/signup-request/view/'.$value['id']) }}" title="View Details"><i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection
