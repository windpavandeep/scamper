@extends('backEnd.layouts.master')
@section('title','Signup Request Detail')
@section('content')
<style type="text/css">
	.ck{
		margin-top: 6px !important;
	}
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Signup Request Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/trainer/signup-requests') }}">Teacher Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Signup Request Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{url('/admin/trainer/signup-request/view/'.$trainer_id)}}" id="new_request">
                        <div class="card-body">
                            <h4 class="card-title">Signup Request Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name:</label>
                                <div class="col-sm-7">

                                    <label class="col-sm-3 ck">{{ucfirst(@$trainer_details->user_details->first_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname4" class="col-sm-3 text-right control-label col-form-label">Last Name:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-5 ck">{{ucfirst(@$trainer_details->user_details->last_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->email }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Mobile Number:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->contact }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Date of Birth:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{ $trainer_details->user_details->dob }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Gender:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->gender}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Country:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->country->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">State:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->state->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">City:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->user_details->city->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Pincode:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->pincode }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Employement Status:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->employment_status->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Organization Name:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->organization_name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Experience(In Years):</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->experience }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Domain:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->domain->name }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Subject:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$trainer_details->subject->name }}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Admin Approval:</label>
                                <div class="col-sm-7">
                                    <select class="form-control admin_approval" name="admin_approval">
                                        <option value="">Select Approval</option>
                                        <option value="A" <?php if(@$trainer_details->admin_approval == 'A'){echo 'selected'; } ?> >Approve</option>
                                        <option value="R" <?php if(@$trainer_details->admin_approval == 'R'){echo 'selected'; } ?> >Reject</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="rejection_reason">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Rejection Reason:</label>
                                <div class="col-sm-7">
                                    <textarea name="rejection_reason" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>  
                        <div class="border-top">
                            <div class="card-body">
                                @csrf
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
@endsection
<script>
    var form = $('#new_request');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules:{
            "admin_approval":{
                required:true,
            },
            "rejection_reason":{
                required:true,
                minlength:2,
                maxlength:500,
            },
           
        },
    });
</script>
<script type="text/javascript"> 
    $(document).ready(function(){

        if("{{@$trainer_details->admin_approval=='R'}}"){

            $('#rejection_reason').show();
        }else{
            $('#rejection_reason').hide();
        }
        $('.admin_approval').change(function(){
            var approval = $(this).val();
            // alert(approval);
            if(approval!=''&& approval!='A'){
                $('#rejection_reason').show();
            }else{
                $('#rejection_reason').hide(); 
            }
        });
    });
</script>
    
@endsection 