@extends('backEnd.layouts.master')
@section('title','Subscription Plan Request Detail')
@section('content')
<style type="text/css">
	.ck{
		margin-top: 6px !important;
	}
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Subscription Plan Request Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/trainer/subscription-plan/requests') }}">Teacher Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Subscription Plan Request Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="new_request">
                        <div class="card-body">
                            <h4 class="card-title">Subscription Plan Request Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Teacher Name:</label>
                                <div class="col-sm-7">

                                    <label class="col-sm-3 ck">{{ucfirst(@$subscription_details->first_name)}} {{ucfirst(@$subscription_details->last_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Teacher Contact Number:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$subscription_details->contact }}</label>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Plan Title:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{ucfirst(@$subscription_details->title)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Price(INR):</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$subscription_details->price }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{!! ucfirst(@$subscription_details->description) !!}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Validity(In Weeks):</label>
                                <div class="col-sm-7">
                                    <label class="col-sm-12 ck">{{@$subscription_details->validity }}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Admin Approval:</label>
                                <div class="col-sm-7">
                                    <select class="form-control admin_approval" name="admin_approval">
                                        <option value="">Select Approval</option>
                                        <option value="A" <?php if(@$subscription_details->admin_approval == 'A'){echo 'selected'; } ?> >Approve</option>
                                        <option value="R" <?php if(@$subscription_details->admin_approval == 'R'){echo 'selected'; } ?> >Reject</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="rejection_reason">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Rejection Reason:</label>
                                <div class="col-sm-7">
                                    <textarea name="rejection_reason" class="form-control" rows="5">{{@$subscription_details->rejection_reason}}</textarea>
                                </div>
                            </div>
                        </div>  
                        <div class="border-top">
                            <div class="card-body">
                                @csrf
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
@endsection
<script>
    var form = $('#new_request');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules:{
            "admin_approval":{
                required:true,
            },
            "rejection_reason":{
                required:true,
                minlength:2,
                maxlength:500,
            },
           
        },
    });
</script>
<script type="text/javascript"> 
    $(document).ready(function(){

        if("{{@$subscription_details->admin_approval=='R'}}"){

            $('#rejection_reason').show();
        }else{
            $('#rejection_reason').hide();
        }
        $('.admin_approval').change(function(){
            var approval = $(this).val();
            // alert(approval);
            if(approval!=''&& approval!='A'){
                $('#rejection_reason').show();
            }else{
                $('#rejection_reason').hide(); 
            }
        });
    });
</script>
    
@endsection    