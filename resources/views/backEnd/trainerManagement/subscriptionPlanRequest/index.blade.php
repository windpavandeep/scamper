@extends('backEnd.layouts.master')
@section('title','Subscription Plan Requests')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Teacher Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Subscription Plan Requests</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <tr role="row" class="heading">
                                            <th>Teacher Name</th>
                                            <th>Plan Title</th>
                                            <th width="20%">Teacher Contact Number</th>
                                            <th width="16%">Admin Approval</th>
                                            <th> Action </th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($subscription_plan))
                                        @foreach($subscription_plan as $key=>$value)
                                            <tr>
                                                <td><a href="{{url('/admin/trainer/edit/'.$value['trainer_id'])}}"><u>{{ ucfirst($value['first_name']) }} {{ ucfirst($value['last_name']) }}</u></a></td>
                                                <td>{{ ucfirst($value['title']) }}</td>
                                                <td>{{ $value['contact'] }}</td>

                                                <td>
                                                    @if($value['admin_approval']=='P')
                                                        Pending
                                                    @elseif(($value['admin_approval']=='A'))
                                                        Approved
                                                    @else
                                                        Rejected
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/trainer/subscription-plan/request/view/'.$value['id']) }}" title="View Details"><i class="fa fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection