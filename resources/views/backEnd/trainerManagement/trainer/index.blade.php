@extends('backEnd.layouts.master')
@section('title','Teachers')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Teacher Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Teachers</h5>
                        <div class="add_btn">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                Export <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right export-import" role="menu">
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="xls">Export to Excel</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="csv">Export to CSV</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="report_optn" type="pdf">Export to PDF</a>
                                    </li>
                                    <!-- <li>
                                        <a href="{{url('admin/trainer/export/pdf')}}" class="report_optn" type="pdf">Export to PDF</a>
                                    </li> -->
                                </ul>
                            </div>
                            <a href="{{ url('admin/trainer/add') }}" class="btn btn-primary">Add Teacher</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Organization Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($trainer_details))
                                        @foreach($trainer_details as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['first_name']) }}</td>
                                                <td>{{ ucfirst($value['last_name']) }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ $value['contact'] }}</td>
                                                <td>{{ ucfirst($value['organization_name']) }}</td>
                                                
                                                <td>
                                                    <a href="{{ url('/admin/trainer/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/trainer/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ url('admin/trainer/credential/mail/'.$value['id']) }}" title="Send Credential"><i class="fa fa-envelope"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div id="form_modal2" class="modal fade" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Export</h4>
                                    </div>
                                    <form action="{{url('admin/trainer/export')}}"" class="form-horizontal" method="post" id="filter_modal">
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Date: </label>
                                                        <div class="col-sm-7">
                                                            <input type="date" class="form-control date_range" id="date_range" name="date" value="" placeholder="Select interval">
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Teacher: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="trainer_id">
                                                                <option value="">Select Teacher</option>
                                                                @if(!empty($trainer_details))
                                                                    @foreach($trainer_details as $key=>$value)
                                                                        <option value="{{$value['id']}}">{{ ucfirst($value['first_name']) }} {{ ucfirst($value['last_name']) }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="export_type" value="" id="export_type">
                                        @csrf
                                        <div class="modal-footer">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Close</button>
                                            <button class="btn btn-primary" id="export_btn" type="submit">Export to </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
<script>
    $('.report_optn').click(function(){
        var value  = $(this).attr('type');
        // var url    = $(this).attr('url');
        $('#export_type').val(value);
        // $('#form_data').submit();

        if(value == 'xls'){
            // alert(url);
            // $('#filter_modal').attr('action',url);
            $('#export_btn').text('Export to excel');
            $('#form_modal2').modal('show');
        }else if(value == 'pdf'){
            // $('#filter_modal').attr('action',url);
            $('#export_btn').text('Export to pdf');
            $('#form_modal2').modal('show');
        }else{
            // $('#form_data').attr('action',url);
            // $('#filter_modal').attr('action',csv_url);
            $('#export_btn').text('Export to csv');
            $('#form_modal2').modal('show');
        }
    });
</script>
@endsection