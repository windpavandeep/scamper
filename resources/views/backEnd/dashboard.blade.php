@extends('backEnd.layouts.master')
@section('title','Dashboard')
@section('content')
<style type="text/css">
 /*9 may 2019 */
.dash-card
{
  padding: 20px!important;
}
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Dashboard</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h4 class="card-title">Site Analysis</h4>
                                <!-- <h5 class="card-subtitle">Overview of Latest Month</h5> -->
                            </div>
                        </div>
                        <div class="row">
                            <!-- column -->
                            <!-- <div class="col-lg-9">
                                <div class="flot-chart">
                                    <div class="flot-chart-content" id="flot-line-chart"></div>
                                </div>
                            </div> -->
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-plus m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{$students}}</h5>
                                           <small class="font-light">Students</small>
                                        </div>
                                    </div>
                                     <div class="col-3">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-user m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{$teachers}}</h5>
                                           <small class="font-light"> Teachers</small>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-cart-plus m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{$courses}}</h5>
                                           <small class="font-light">Products</small>
                                        </div>
                                    </div>
                                     <div class="col-3">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-tag m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{$exams}}</h5>
                                           <small class="font-light">Exams</small>
                                        </div>
                                    </div>
                                    <div class="col-3 m-t-15">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-table m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{CURRENCY}} {{$earnings}}</h5>
                                           <small class="font-light">Earning</small>
                                        </div>
                                    </div>
                                    <div class="col-3 m-t-15">
                                        <div class="bg-dark p-10 text-white text-center">
                                           <i class="fa fa-globe m-b-5 font-16"></i>
                                           <h5 class="m-b-0 m-t-5">{{CURRENCY}} {{$pending_payments}}</h5>
                                           <small class="font-light">Pending payment</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- column -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>


@endsection