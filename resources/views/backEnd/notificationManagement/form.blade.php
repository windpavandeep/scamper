@extends('backEnd.layouts.master')
@section('title','Send Notifications')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Send Notifications</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Notification Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Send Notifications</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="form_data" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Send Notifications</h4>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Student:</label>
                                <div class="col-sm-7">
                                    <select class="select_email form-control" multiple style="width: 590px;" name="student_email[]" placeholder="Select Student" id="email">
                                        @if(!empty($students))
                                            @foreach($students as $key=>$student)
                                                <option value="{{$student['id']}}">{{ucfirst($student['first_name'])}} {{ucfirst($student['last_name'])}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Select Trainer:</label>
                                <div class="col-sm-7">
                                    <select class="select_email form-control" multiple style="width: 590px;" name="trainer_email[]" placeholder="Select Trainer" id="email">
                                        @if(!empty($trainers))
                                            @foreach($trainers as $key=>$trainer)
                                                <option value="{{$trainer['id']}}">{{ucfirst($trainer['first_name'])}} {{ucfirst($trainer['last_name'])}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="" placeholder="Title" name="heading" value="">
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea name="desc" class="form-control" placeholder="Description" id="description" rows="10"></textarea>
                                </div>
                            </div>
                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<link  rel="stylesheet" type="text/css" href="{{url('/public/backEnd/css/multiple-select.css')}}">
<script type="text/javascript" src="{{url('/public/backEnd/js/multiple-select.js')}}"></script>
<script>
    $('#form_data').validate({
        rules:{
            desc:{
                minlength:2,
                maxlength:500,
            }
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element);
        }
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select_email').multipleSelect();
        // $('select').multipleSelect('checkAll');
     });   
</script>
@endsection
@endsection    