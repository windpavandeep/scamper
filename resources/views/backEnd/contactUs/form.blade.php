@extends('backEnd.layouts.master')
@section('title','Contact Us Detail')
@section('content')
<style type="text/css">
    .ck{
        margin-top: 8px !important;
    }
    .reply_field{
        margin-left: 10px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Contact Us Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/contact-us')}}">Contact Us</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact Us Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form id="contact_us_form" method="post" action="">
                        <div class="card-body">
                            <h4 class="card-title">Contact Us Detail</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name: </label>
                                <label class="col-sm-3 ck">
                                <div class="col-sm-8">
                                    {{ucfirst(@$contact_us_details->first_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Last Name: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">{{ucfirst(@$contact_us_details->last_name)}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">{{ucfirst(@$contact_us_details->email)}}</label>
                                </div>
                              
                            </div>
                             <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No:</label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">{{ @$contact_us_details->contact_no }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label"> Query: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-12 ck">{{ ucfirst(@$contact_us_details->enquiry) }}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label"> Status: </label>
                                <div class="col-sm-7">
                                    <?php
                                        $status ='Pending';
                                        if(@$contact_us_details['reply_status']=='R'){

                                            $status ='Replied';
                                        }
                                    ?>
                                    <label class="col-sm-3 ck">{{$status}}</label>
                                </div>
                            </div>
                          
                            @if(!empty($contact_us_details['reply']))
                                <div class="form-group row reply">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Previous Reply: </label>
                                    <div class="col-md-8">
                                        <label class="col-sm-12 ck">{{ ucfirst(@$contact_us_details->reply) }}</label>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row reply ">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label"> Reply: </label>
                                <div class="col-sm-8">
                                    <textarea type="text" class="form-control reply_field" id="fname12" placeholder="Reply" rows="8" name="reply"></textarea>
                                </div>
                            </div>

                            
                        </div>
                        <div class="border-top reply_field">
                            <div class="card-body">
                                @csrf
                                <button class="btn btn-primary" style="float: right;margin-bottom: 20px">
                                    Submit
                                </button>
                             <!--    <a href="{{url('admin/franchise')}}" class="btn btn-primary" style="float: right; margin-bottom: 20px">Back</button></a> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
<script type="text/javascript">
    $('#contact_us_form').validate({
        rules:{
            reply:{
                required:true,
                minlength:2,
                maxlength:1000,
            },
        },
    });
</script>
@endsection    