@extends('backEnd.layouts.master')
@section('title','Contact Us')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Contact Us</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Contact Us</h5>
                        <!-- <div class="add_btn"> -->
                            <!-- <div class="btn-group pull-right">
                                <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                Export <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right export-import" role="menu">
                                    <li>
                                        <a href="{{url('admin/companies/export?excel')}}" class="report_optn" type="excel">Export to Excel</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export?csv')}}" class="report_optn" type="csv">Export to CSV</a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/companies/export/pdf')}}" class="report_optn" type="pdf">Export to PDF</a>
                                    </li>
                                </ul>
                            </div> -->
                            <!-- <a href="{{ url('admin/student/add') }}" class="btn btn-primary">Add Student</a> -->
                        <!-- </div> -->
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Status</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($contact_us))
                                        <?php foreach($contact_us as $key=>$value){ 
                                            if($value['reply_status'] == 'P'){
                                                $status = 'Pending'; 
                                            }else{
                                                $status = 'Replied'; 

                                            }
                                        ?>
                                            <tr>
                                                <td>{{ ucfirst($value['first_name']) }}</td>
                                                <td>{{ ucfirst($value['last_name']) }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ $value['contact_no'] }}</td>
                                                <td>{{ $status }}</td>
                                                
                                                <td>
                                                    <a href="{{ url('/admin/contact-us/detail/'.$value['id']) }}" title="Info"><i class="fa fa-edit"></i>
                                                    </a>
                                                    
                                                </td>
                                            </tr>
                                        <?php } ?>    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection