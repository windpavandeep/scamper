<?php 
    if(isset($student_id)){
        $task    = 'Edit';
        $action  =  url('/admin/course/edit/'.$course_id);
    }else{
        $task    = 'Add';
        $action  =  url('/admin/course/add');
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Course')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Course</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/courses') }}">Course Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Course</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="course_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Course</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                       <option value="">Choose Category</option>
                                       @foreach($categories as $category)
                                           <option value="{{ $category['id'] }}">{{ucfirst($category['name'])}}</option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                       <option data-display="Choose Category" value="" readonly>Choose Sub-Category</option>
                                       
                                   </select>
                            </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2:</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                         <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                            <option data-display="Choose Category" value="" readonly>Choose Sub-Category 2</option>
                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subject:</label>
                                <div class="col-sm-7">
                                   <select class="form-control"  name="subject_id" id="subject_id">
                                        <option value="" readonly>Choose Subject</option>
                                        @foreach($subjects as $subject)
                                            <option value="{{ $subject['id'] }}">{{ ucfirst($subject['name']) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Unit:</label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="unit_id" id="unit_id">
                                        <option data-display="Choose Unit" value="" readonly>Choose Unit</option>
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Language: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="language_id">
                                        <option data-display="Choose Category" value="" readonly>Choose language</option>
                                        @foreach($languages as $language)
                                            <option value="{{ $language['id'] }}">{{ucfirst($language['name'])}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Title:</label>
                                <div class="col-md-7">
                                    <input type="text" name="course_title" class="form-control" placeholder="Title" >
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Upload Option: </label>
                                <div class="col-sm-7">
                                    
                                    <select class="niceselc form-control" name="upload_optn" id="upload_optn">
                                        <option value="" readonly>Choose upload option</option>
                                        <option value="pdf">Pdf</option>
                                        <option value="video">Video</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="pdf_div" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Upload PDF: </label>
                                <div class="col-sm-7">
                                    <span class="error" id="pdf_file_req" style="color:red"></span>
                                 <!--    <label>Upload PDF</label> -->
                                    <input type="file" class="form-control" name="pdf" placeholder="Upload PDF" accept=".pdf" id="pdf_file">
                                </div>
                            </div>
                            <div class="form-group row" id="video_div" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Upload Video: </label>
                                <div class="col-sm-7">
                                    <span class="error" id="vide_req" style="color:red"></span>
                                    <!-- <label>Upload Video</label> -->
                                    <input type="file" class="form-control" name="video" placeholder="Upload Video" accept=".mp4,.mov,.avi" id="video_file">
                                </div>
                            </div>
                        
                           
                                <div class="form-group row" >
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Content Type: </label>
                                    <div class="col-sm-7">
                                       <select class="niceselc form-control" name="content_type" id="content_type">
                                            <option value="free" selected="">Free</option>
                                            <option value="paid">Paid</option>
                                        </select>
                                    </div>
                                </div>
                         
                                <div class="form-group row" id="paid_amount_container">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image12">Paid</label>
                                    <div class="col-md-7">
                                    <span class="error" id="payment_error" style="color:red"></span>
                                        <input type="text" name="paid_amount" class="form-control" placeholder="Enter Price" id="payment">
                                    </div>
                                </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Add Description:</label>
                                <div class="col-md-7">
                                   <textarea class="form-control ckeditor" name="description" id="description" placeholder="Add Description of the content why users should buy this?"></textarea>
                                     
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">No Of Exams:</label>
                                <div class="col-md-7">
                                    <input type="text" name="no_of_exams" class="form-control" placeholder="No Of Exams" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Start Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="Start Date" autocomplete="off" name="start_date" id="start_date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">End Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="End Date" autocomplete="off" name="end_date" id="end_date">
                                    <span id="end_date_error" class="error1"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname12" class="col-sm-3 text-right control-label col-form-label">Upload New Images:</label>
                                <div class="col-sm-7">
                                    <div class="write_review_modal_image_container rvw_upld">
                                        
                                        <span id="img-error" style="color: red;"></span>
                                        <div class="photos-sec">
                                            <div class="flex">
                                            </div>
                                        </div>
                                        <div class="add-phots">
                                            <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload Images</a>
                                            <input type="file" name="images[]" id="id_proof" class="review_modal_image id_proof_document rem0" multiple>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        $('#video_div').hide();
        $('#pdf_div').hide();
    $('#upload_optn').on('change',function(){
        var selectd = $(this).val();
        if(selectd == 'pdf'){
            $('#pdf_div').show();
            $('#video_div').hide();
        } else if(selectd == 'video'){
            $('#video_div').show();
            $('#pdf_div').hide();
        }else{
            $('#video_div').hide();
            $('#pdf_div').hide();
        }
    });
    });
</script>
<script>
    var form = $('#course_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { 
            if(element.attr("name") == "images[]") {
                element.parent().before(error); 
            }else{

                element.before(error); 
            }
        },
        ignore: [],
        rules:{

            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
            subject_id:{
               required:true,
            },
            course_title:{
               required:true,
               minlength:2,
               maxlength:255,
            },
            upload_optn:{
                required:true,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:1200,
            },
            language_id:{
                required:true,
            },
            no_of_exams:{
                required:true,
                min:1,
                digits:true,
                maxlength:7,
            },
            paid_amount:{
                min:1,
                digits:true,
                maxlength:7,
            },
            content_type:{
                required:true,
            },
            "images[]":{
                required:true,
            },
            start_date:{
                required:true,
            },
            end_date:{
                required:true,
            },
        },
        submitHandler:function(form){
            var content_type = $('#content_type').val();
            var upload_optn  = $('#upload_optn').val();
            var paid_amount  = $('#payment').val();
            var start_date   = $('#start_date').val();
            var end_date     = $('#end_date').val();

            $('#end_date_error').text('');
            if(end_date<=start_date){
                $('#end_date_error').text('End date should not be equal or less than start date');
                return false;
            }
            $('#pdf_file_req').text('');
            $('#vide_req').text('');
            if(upload_optn=='pdf'){
                var pdf_file = $("#pdf_file")[0].files.length;
                if(pdf_file==0){
                    $('#pdf_file_req').text('*This field is required.');
                    return false;
                }

            }
            if(upload_optn=='video'){
                var video_file = $("#video_file")[0].files.length;
                if(video_file==0){
                    $('#vide_req').text('*This field is required.');
                    return false;
                }
            }
            $('#payment_error').text('');
            if(content_type=='paid'){
                // alert(paid_amount);
                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }
            // alert(content_type);
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $('#content_type').click(function(){
        var value = $(this).val();
        // alert(value);
        if(value=='paid'){
            $('#paid_amount_container').show();
        }
        if(value=='free'){
            $('#paid_amount_container').hide();
        }
    });
    $(document).ready(function(){
        $('#paid_amount_container').hide();        
    })
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_image', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='jpg' || extension=='jpeg' || extension=='png')){
                dataURL = reader.result;
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only jpg,jpeg,png file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld img');
            output.src = dataURL;
            var len = $('.photos-sec > .flex > div').length;
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.photos-sec> .flex').append('<div class="flex-item uploaded-section"><div class="phot-upld"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="images['+inc_val+']" class="review_modal_image rem'+inc_val+' id_proof">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }
            /*}else{
                $('#img-error').text('Maximum file size can be 2 MB');
            }*/                       
            
            //this_addr.after('<input type="file" name="upld-gal-hidden['+inc_val+']" accept="image/*" class="review_modal_image" id="upld-gal-hidden['+inc_val+']">')

            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.photos-sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                $('#unit_id').html(resp.units);

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.dtpckr').datepicker();
    });
</script>
@endsection

@endsection    