<?php 
    if(isset($course_id)){
        $task    = 'Edit';
        $form_id = 'edit_course_form';
        $action  =  url('/admin/course/edit/'.$course_id);
        $disabled= 'disabled';
    }else{
        $task    = 'Add';
        $form_id = 'add_course_form';
        $action  =  url('/admin/course/add');
        $disabled= '';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Product')
@section('content')
<style type="text/css">
    .plus-icn {
        font-size: 30px;
        line-height: 1.1;
        color: #27a9e3;
    }
    .error2{
        color: red;
    }
</style>
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Product</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/courses') }}">Product Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Product</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="{{$form_id}}" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Product</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                       <option value="">Choose Category</option>
                                       @foreach($categories as $category)
                                           <option value="{{ $category['id'] }}" <?php if(@$category['id']==@$course_detail['category_id']){echo'selected';}?>>{{ucfirst($category['name'])}}</option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                        <option data-display="Choose Category" value="">    Choose Sub-Category</option>
                                        @if(!empty($sub_category))
                                            @foreach($sub_category as $value)
                                                <option value="{{$value['id']}}" <?php if(@$value['id']==@$course_detail['sub_category_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2:</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                         <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                            <option data-display="Choose Category" value="" readonly>Choose Sub-Category 2</option>
                                            @if(!empty($sub_sub_category))
                                                @foreach($sub_sub_category as $value)
                                                    <option value="{{$value['id']}}" <?php if(@$value['id']==@$course_detail['sub_subcategory_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subject:</label>
                                <div class="col-md-7">
                                    <div class="table-responsive" >
                                        <table id="" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Subject</th>
                                                    <th>Unit</th>
                                                    <th></th>                              
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($task=='Edit')
                                                    @foreach(@$course_detail['content_subject_units'] as $key=>$value)
                                                        <tr id="subject_div" class="subject_div_class">
                                                            <td>
                                                                
                                                                <div class="form-group in-group">
                                                                    <span class="error1"></span>
                                                                    <select class="form-control subject_id" id="subject_id" name="content[{{$key}}][subject]">
                                                                        <option value="" readonly>  
                                                                            Choose Subject
                                                                        </option>
                                                                        @foreach(@$subjects as $subject)
                                                                            <option value="{{ $subject['id'] }}" <?php if(@$subject['id']==@$value['subject_id']){echo'selected';}?>>{{ ucfirst($subject['name']) }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td id="unit_div">
                                                                <div class="form-group in-group">
                                                                    <span class="error2"></span>
                                                                    <select class="form-control unit_id unit_class" id="unit_id" name="content[{{$key}}][unit_id]">
                                                                        <option value="" readonly>Choose Unit</option>

                                                                        @foreach($units as $key1=>$unit)
                                                                            @if(($unit['subject_id']==$value['subject_id']) && ($unit['id']==$value['unit_id']))
                                                                                <option value="{{ $unit['id'] }}" <?php if($unit['id']==$value['unit_id']){echo'selected';}?>>{{ ucfirst($unit['name']) }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                @if(@$key=='0')
                                                                    <a href="javascript:;" id="plus-icn"><i class="mdi mdi-plus-box plus-icn" title="Add New"></i></i></td>
                                                                    @else
                                                                    <a href="javascript:;" id="plus-icn" class="remove-subject"><i class="mdi mdi-plus-box plus-icn" title="Delete"></i></i></td>
                                                                @endif
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr id="subject_div" class="subject_div_class">
                                                        <td>
                                                            <div class="form-group in-group">
                                                                <span class="error1"></span>
                                                                <select class="form-control subject_id" id="subject_id" name="content[subject_id]">
                                                                    <option value="" readonly>Choose Subject</option>
                                                                    @foreach($subjects as $subject)
                                                                        <option value="{{ $subject['id'] }}">{{ ucfirst($subject['name']) }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td id="unit_div">
                                                            <div class="form-group in-group">
                                                                <span class="error2"></span>
                                                                <select class="form-control unit_id unit_class" id="unit_id" name="content[subject_id][unit_id]">
                                                                    <option value="" readonly>Choose Unit</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td><a href="javascript:;" id="plus-icn"><i class="mdi mdi-plus-box plus-icn" title="Add New"></i></i></td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Language: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="language_id">
                                        <option data-display="Choose Category" value="" readonly>Choose language</option>
                                        @foreach($languages as $language)
                                            <option value="{{ $language['id'] }}"  <?php if(@$course_detail['language_id']==$language['id']){echo'selected';}?>>{{ucfirst($language['name'])}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Title:</label>
                                <div class="col-md-7">
                                    <input type="text" name="course_title" class="form-control" placeholder="Title" value="{{ isset($course_detail->title)?$course_detail->title:''}}">
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Upload Option: </label>
                                <div class="col-sm-7">
                                    
                                    <select class="niceselc form-control" name="upload_optn" id="upload_optn" {{$disabled}}>
                                        <option value="" readonly>Choose upload option</option>
                                        <option value="pdf" <?php if(@$course_detail['upload_type']=='pdf'){echo'selected';}?>>Pdf</option>
                                        <option value="video" <?php if(@$course_detail['upload_type']=='video'){echo'selected';}?>>Video</option>
                                    </select>
                                </div>
                            </div>
                        
                                @if(!empty($course_detail['course_files']))
                                    @if(!($course_detail['course_files']->isEmpty())) 
                                        <div class="form-group row"> 
                                            <label for="lname34" class="col-sm-3 text-right control-label col-form-label">Uploaded Documents</label>
                                            <div class="col-md-7">
                                               
                                                @foreach($course_detail['course_files'] as $key => $value )
                                                    <?php  
                                                        
                                                        $file     = DefaultImgPath; 
                                                        $file_url = 'javascript:;';
                                                        if(!empty($value['file'])) {
                                                            if(file_exists(TrainerContentBasePath.'/'.$value['file'])) {
                                                                $file_url = TrainerContentImgPath.'/'.$value['file'];
                                                                $file_name = pathinfo($value['file']);
                                                                $ext = $file_name['extension'];
                                                                if($ext=='mov'||$ext=='mp4'||$ext=='avi') {
                                                                    $file = DefaultVideoImgPath;;  
                                                                }else if($ext=='pdf'){
                                                                    $file = DefaultPdfImgPath;
                                                                }
                                                            }
                                                        }   
                                                        $count = count($course_detail['course_files']);
                                                        // dd($image_url);
                                                    ?>
                                                    <div class="flex-item">
                                                        <div class="phot-upld">
                                                            @if($count>1)
                                                            <a href="{{ url('/admin/service-provider/document/delete/'.$value['id']) }}" title="Delete" class="btn btn-remov del_btn">
                                                            <i class="fa fa-times" style="float:right"></i>
                                                            </a>
                                                            @endif
                                                            <a href="{{$file_url}}" target="_blank"> 
                                                                <img src="{{$file}}"  width="120px" height="120px"  class="img-responsive modal-upload-image-preview">
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        
                                    @endif
                                @endif
                       
                                <div class="form-group row" id="pdf_div" >
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">PDF: </label>
                                    <div class="col-sm-7">
                                        <span class="error" id="pdf_file_req" style="color:red"></span>
                                        <!-- <label>Upload PDF</label> -->
                                       <!--  <input type="file" class="form-control" name="pdf[]" placeholder="Upload PDF" accept=".pdf" id="pdf_file"  multiple=""> -->
                                        <div class="write_review_modal_image_container rvw_upld">
                                            
                                            <span id="img-error" style="color: red;"></span>
                                            <div class="photos-sec upload_pdf_sec">
                                                <div class="flex image_upload_class upload_pdf">
                                                </div>
                                            </div>
                                            <div class="add-phots">
                                                <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload new PDF</a>
                                                <input type="file" name="pdf[]" id="pdf_id" class=" review_modal_pdf review_modal_video_css id_proof_document rem0" multiple>
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                            
                       
                                <div class="form-group row" id="video_div" >
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Video: </label>
                                    <div class="col-sm-7">
                                        <span class="error" id="vide_req" style="color:red"></span>
                                        <!-- <label>Upload Video</label> -->
                                        <div class="write_review_modal_image_container rvw_upld">
                                            
                                            <span id="img-error" style="color: red;"></span>
                                            <div class="photos-sec upload_video_sec">
                                                <div class="flex image_upload_class upload_video">
                                                </div>
                                            </div>
                                            <div class="add-phots">
                                                <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload new Video</a>
                                                <input type="file" name="video[]" id="video_id" class=" review_modal_video review_modal_video_css id_proof_document rem0" multiple>
                                            </div>
                                        </div>        
                                        <!-- <input type="file" class="form-control" name="video[]" placeholder="Upload Video" accept=".mp4,.mov,.avi" id="video_file" multiple=""> -->
                                    </div>
                                </div>
                        
                            
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Content Type: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="content_type" id="content_type">
                                        <option value="free" selected="" <?php if(@$course_detail['content_availability']=='free'){echo'selected';}?>>Free</option>
                                        <option value="paid" <?php if(@$course_detail['content_availability']=='paid'){echo'selected';}?>>Paid</option>
                                        <option value="subscriber" <?php if(@$course_detail['content_availability']=='subscriber'){echo'selected';}?>>Subscriber</option>
                                    </select>
                                </div>
                            </div>
                     
                            <div class="form-group row" id="paid_amount_container">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image12">Paid</label>
                                <div class="col-md-7">
                                <span class="error" id="payment_error" style="color:red"></span>
                                    <input type="text" name="paid_amount" class="form-control" placeholder="Paid" id="payment" value="{{ isset($course_detail->paid_amount)?$course_detail->paid_amount:''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Add Description:</label>
                                <div class="col-md-7">
                                   <textarea class="form-control ckeditor" name="description" id="description" placeholder="Add Description of the content why users should buy this?">{{ isset($course_detail->description)?$course_detail->description:''}}</textarea>
                                     
                                </div>
                            </div>
                            <div class="form-group row" >
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Give Free Questions: </label>
                                <div class="col-sm-7">
                                   <select class="niceselc form-control" name="free_question" id="content_type">
                                        <option value="Y" selected="" <?php if(@$course_detail['free_question']=='Y'){echo'selected';}?>>Yes</option>
                                        <option value="N" <?php if(@$course_detail['free_question']=='N'){echo'selected';}?>>No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">No Of Exams:</label>
                                <div class="col-md-7">
                                    <input type="text" name="no_of_exams" class="form-control" placeholder="No Of Exams" value="{{ isset($course_detail->no_of_exams)?$course_detail->no_of_exams:''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Start Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="Start Date" autocomplete="off" name="start_date" id="start_date" value="{{isset($course_detail->start_date)?date('d-m-Y',strtotime($course_detail->start_date)) :''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">End Date:</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control dtpckr" placeholder="End Date" autocomplete="off" name="end_date" id="end_date" value="{{isset($course_detail->end_date)?date('d-m-Y',strtotime($course_detail->end_date)) :''}}">
                                    <span id="end_date_error" class="error1"></span>
                                </div>
                            </div>
                            @if(!empty($course_detail['course_images']))
                                @if(!($course_detail['course_images']->isEmpty())) 
                                    
                                        <div class="form-group row"> 
                                            <label for="lname34" class="col-sm-3 text-right control-label col-form-label">Uploaded Images</label>
                                            <div class="col-md-7">
                                               
                                                @foreach($course_detail['course_images'] as $key => $value )
                                                    <?php  
                                                        
                                                        $image = DefaultImgPath; 
                                                        $image_url = 'javascript:;';
                                                        if(!empty($value['name'])) {
                                                            if(file_exists(TrainerContentImageBasePath.'/'.$value['name'])) {
                                                                $image_url = TrainerContentImageImgPath.'/'.$value['name'];
                                                                $image_name = pathinfo($value['name']);
                                                                $ext = $image_name['extension'];
                                                                if($ext=='jpg'||$ext=='jpeg'||$ext=='png') {
                                                                    $image = TrainerContentImageImgPath.'/'.$value['name'];  
                                                                }
                                                            }
                                                        } 
                                                        $image_count = count($course_detail['course_images']);
                                                        // dd($image_url);
                                                    ?>
                                                    <div class="flex-item">
                                                        <div class="phot-upld">
                                                            @if($image_count>'1')
                                                                <a href="{{ url('/admin/course/image/delete/'.$value['id']) }}" title="Delete" class="btn btn-remov del_btn">
                                                                <i class="fa fa-times" style="float:right"></i>
                                                                </a>
                                                            @endif
                                                            <a href="{{$image_url}}" target="_blank"> 
                                                                <img src="{{$image}}"  width="120px" height="120px"  class="img-responsive modal-upload-image-preview">
                                                            </a>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                @endif
                            @endif
                            <div class="form-group row">
                                <label for="lname12" class="col-sm-3 text-right control-label col-form-label">Upload Images:</label>
                                <div class="col-sm-7">
                                    <div class="write_review_modal_image_container rvw_upld">
                                        
                                        <span id="img-error" style="color: red;"></span>
                                        <div class="photos-sec photos-sec_image">
                                            <div class="flex image_upload_class image_class">
                                            </div>
                                        </div>
                                        <div class="add-phots">
                                            <a href="javascript:void(0);" class="btn-up-phots" id="upld-gal">Upload new Image</a>
                                            <input type="file" name="images[]" id="id_proof" class="review_modal_image id_proof_document rem0" multiple>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
@if($task=='Edit')
    <script type="text/javascript">
        $(document).ready(function(){
            var upload_type = "{{@$course_detail['upload_type']}}";
            if(upload_type=='pdf'){
                $('#video_div').hide();
            }else{
                $('#pdf_div').hide();
            }
        });
    </script>
@endif
@if($task=='Add')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#video_div').hide();
            $('#pdf_div').hide();
            $('#upload_optn').on('change',function(){
                var selectd = $(this).val();
                if(selectd == 'pdf'){
                    $('#pdf_div').show();
                    $('#video_div').hide();
                } else if(selectd == 'video'){
                    $('#video_div').show();
                    $('#pdf_div').hide();
                }else{
                    $('#video_div').hide();
                    $('#pdf_div').hide();
                }
            });
        });
    </script>
@endif
<script>
    var form = $('#add_course_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { 
            if(element.attr("name") == "images[]") {
                element.parent().before(error); 
            }else{

                element.before(error); 
            }
        },
        ignore: [],
        rules:{

            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
          
            course_title:{
               required:true,
               minlength:2,
               maxlength:255,
            },
            upload_optn:{
                required:true,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:1200,
            },
            language_id:{
                required:true,
            },
            no_of_exams:{
                // required:true,
                // min:1,
                digits:true,
                maxlength:7,
            },
            paid_amount:{
                min:1,
                digits:true,
                maxlength:7,
            },
            content_type:{
                required:true,
            },
            "images[]":{
                required:true,
            },
            start_date:{
                required:true,
            },
            end_date:{
                required:true,
            },
        },
        submitHandler:function(form){
            var content_type = $('#content_type').val();
            var upload_optn  = $('#upload_optn').val();
            var paid_amount  = $('#payment').val();
            var start_date   = $('#start_date').val();
            var end_date     = $('#end_date').val();

            var err = 0;
            $('.subject_id').each(function(key){
                var field_data = $(this).val();
                // alert
                var current    = $(this);
                // alert(key);
                var error      = $(this).prev('.error1');
                // alert(error);
                if(field_data==''){
                    // alert('dfdf');
                    err = 1;
                    current.closest('#subject_div').find('span').text('*This field is required.');
                }else{
                    err = 0;
                    current.closest('#subject_div').find('span').text('');
                }
            });

            $('.unit_id').each(function(key){

                var field_data = $(this).val();
                var current    = $(this);
                var error      = $(this).prev('.error2');
                if(field_data==''){
                    err = 1;
                    current.closest('#subject_div').next('span').text('*This field is required.');
                }else{
                    err = 0;
                    current.closest('#subject_div').next('span').text('');
                }
            });

            $('#end_date_error').text('');
            if(end_date<=start_date){
                $('#end_date_error').text('End date should not be equal or less than start date');
                return false;
            }
            $('#pdf_file_req').text('');
            $('#vide_req').text('');
            if(upload_optn=='pdf'){
                var pdf_file = $('.upload_pdf_sec > .upload_pdf > div').length;
                // alert(pdf_file);
                if(pdf_file==0){
                    $('#pdf_file_req').text('*This field is required.');
                    return false;
                }

            }
            if(upload_optn=='video'){
                var video_file = $("#video_file")[0].files.length;
                var video_file = $('.upload_video_sec > .upload_video > div').length;
                // alert(video_file);
                if(video_file==0){
                    $('#vide_req').text('*This field is required.');
                    return false;
                }
            }
            $('#payment_error').text('');
            if(content_type=='paid'){
                // alert(paid_amount);
                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }
            // alert(content_type);
           
            if(err==0){
                
                form.submit();
            }
        },
    });
</script>
<script>
    var form = $('#edit_course_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { 
            if(element.attr("name") == "images[]") {
                element.parent().before(error); 
            }else{

                element.before(error); 
            }
        },
        ignore: [],
        rules:{

            category_id:{
               required:true,
            },
            sub_category_id:{
               required:true,
            },
          
            course_title:{
               required:true,
               minlength:2,
               maxlength:255,
            },
            upload_optn:{
                required:true,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:1200,
            },
            language_id:{
                required:true,
            },
            no_of_exams:{
                // required:true,
                // min:1,
                digits:true,
                maxlength:7,
            },
            paid_amount:{
                min:1,
                digits:true,
                maxlength:7,
            },
            content_type:{
                required:true,
            },
            start_date:{
                required:true,
            },
            end_date:{
                required:true,
            },
        },
        submitHandler:function(form){
            var content_type = $('#content_type').val();
            var upload_optn  = $('#upload_optn').val();
            var paid_amount  = $('#payment').val();
            var start_date   = $('#start_date').val();
            var end_date     = $('#end_date').val();

            var err = 0;
            $('.subject_id').each(function(key){
                var field_data = $(this).val();
                // alert
                var current    = $(this);
                // alert(key);
                var error      = $(this).prev('.error1');
                // alert(error);
                if(field_data==''){
                    // alert('dfdf');
                    err = 1;
                    current.closest('#subject_div').find('span').text('*This field is required.');
                }else{
                    err = 0;
                    current.closest('#subject_div').find('span').text('');
                }
            });

            $('.unit_id').each(function(key){

                var field_data = $(this).val();
                var current    = $(this);
                var error      = $(this).prev('.error2');
                if(field_data==''){
                    err = 1;
                    current.closest('#subject_div').next('span').text('*This field is required.');
                }else{
                    err = 0;
                    current.closest('#subject_div').next('span').text('');
                }
            });

            $('#end_date_error').text('');
            if(end_date<=start_date){
                $('#end_date_error').text('End date should not be equal or less than start date');
                return false;
            }
            $('#pdf_file_req').text('');
            /*$('#vide_req').text('');
            if(upload_optn=='pdf'){
                var pdf_file = $('.upload_pdf_sec > .upload_pdf > div').length;
                // alert(pdf_file);
                if(pdf_file==0){
                    $('#pdf_file_req').text('*This field is required.');
                    return false;
                }

            }*/
            /*if(upload_optn=='video'){
                var video_file = $("#video_file")[0].files.length;
                var video_file = $('.upload_video_sec > .upload_video > div').length;
                // alert(video_file);
                if(video_file==0){
                    $('#vide_req').text('*This field is required.');
                    return false;
                }
            }*/
            $('#payment_error').text('');
            if(content_type=='paid'){
                // alert(paid_amount);
                if(paid_amount==''){

                    $('#payment_error').text('*This field is required.');
                    return false;
                }
            }
            // alert(content_type);
           
            if(err==0){
                
                form.submit();
            }
        },
    });
</script>

<script type="text/javascript">
    $('#content_type').click(function(){
        var value = $(this).val();
        // alert(value);
        $('#paid_amount_container').hide();
        if(value=='paid'){
            $('#paid_amount_container').show();
        }
        if(value=='free'){
            $('#paid_amount_container').hide();
        }
    });
    $(document).ready(function(){
        $('#paid_amount_container').hide();        
    })
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_image', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='jpg' || extension=='jpeg' || extension=='png')){
                dataURL = reader.result;
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only jpg,jpeg,png file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld img');
            output.src = dataURL;
            var len = $('.photos-sec_image > .flex > div').length;
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.photos-sec> .image_class').append('<div class="flex-item uploaded-section"><div class="phot-upld"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="images['+inc_val+']" class="review_modal_image rem'+inc_val+' id_proof">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }                    
            
           
            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.photos-sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_video', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='mp4' || extension=='mov' || extension=='avi')){
                dataURL = "{{DefaultVideoImgPath}}";
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only mp4,mov,avi file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld_video img');
            output.src = dataURL;
            var len = $('.upload_video_sec > .upload_video > div').length;
            // alert(len);
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.upload_video_sec> .upload_video').append('<div class="flex-item uploaded-section"><div class="phot-upld  phot-upld_video"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="video['+inc_val+']" class="review_modal_video review_modal_video_css rem'+inc_val+' video_id">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }
            /*}else{
                $('#img-error').text('Maximum file size can be 2 MB');
            }*/                       
            
            //this_addr.after('<input type="file" name="upld-gal-hidden['+inc_val+']" accept="image/*" class="review_modal_image" id="upld-gal-hidden['+inc_val+']">')

            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.upload_video_sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>
<script type="text/javascript">
    var inc_val = 0;
    $(document).on('change','.review_modal_pdf', function () {
        // alert('aa');
        var input = this;
        var this_addr = $(this);
        var extension = this_addr.val().split('.').pop().toLowerCase();

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = '';
            if((extension=='pdf')){
                dataURL = "{{DefaultPdfImgPath}}";
            }else{
                swal({
                  title: "This file Type is not valid",
                  text: "Only pdf file type is supported",
                  icon: "warning",
                  buttons: "Ok", 
                  dangerMode: true,
                }).then((okay) => {
                    if (okay) {
                        return false;
                    }else{
                        return false;
                    }
                });

                return false;
            }      
            var output = $('.phot-upld_pdf img');
            output.src = dataURL;
            var len = $('.upload_pdf_sec > .upload_pdf > div').length;
            // alert(len);
            // var fileSize = input.files[0].size / 5024 / 1024; // in MB
            // if(fileSize < 2) {
                // alert(len);
            if (len <= 4) {
                $('#img-error').text('');
                $('.upload_pdf_sec> .upload_pdf').append('<div class="flex-item uploaded-section"><div class="phot-upld  phot-upld_pdf"><img src="'+ dataURL +'" class="img-responsive modal-upload-image-preview" /><a class="btn btn-remov"  data-toggle="tooltip" title="Delete Image" inpt-id="'+inc_val+'"><i class="fa fa-times"></i></a></div></div>');
                this_addr.off('change');
                inc_val++;
                if (len<=4) {
                    this_addr.after('<input type="file" name="pdf['+inc_val+']" class="review_modal_pdf review_modal_video_css rem'+inc_val+' pdf_id">');
                }
            }else{
                $('#img-error').text("Maximum 5 files can be uploaded");
            }
            /*}else{
                $('#img-error').text('Maximum file size can be 2 MB');
            }*/                       
            
            //this_addr.after('<input type="file" name="upld-gal-hidden['+inc_val+']" accept="image/*" class="review_modal_image" id="upld-gal-hidden['+inc_val+']">')

            $('.btn-remov').on('click',function(){
                $(this).closest('.uploaded-section').remove();
                var rem_id = $(this).attr('inpt-id');
                $('.rem'+rem_id).remove();
                if ($('.upload_pdf_sec > .flex > div').length <5) {
                    $('#img-error').text('');
                }else{
                    $('#img-error').text("Maximum 5 files can be uploaded");
                }
            });
        };        
        reader.readAsDataURL(input.files[0]);
    });
</script>
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                // $('#unit_id').html(resp.units);

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.dtpckr').datepicker({ format: 'dd-mm-yyyy' });
    });
</script>

<script type="text/javascript">
    $('#plus-icn').click(function(){

        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('admin/subject/content') }}",
            success:function(resp){
                // alert(resp);
                $('.table-responsive tr:last').after(resp);
                // $(resp).insertAfter('.subject_div_class');
            },
        });
    });
    $(document).ready(function(){

        $(document).on('click', '.remove-subject', function() {
            $(this).closest('tr').remove();
        });
    });
    
</script>
<script type="text/javascript">
$(document).on('focusin', '.subject_id', function(){
    var previous_subject_id    = $(this).val();
    $(document).on('change', '.subject_id', function() {
        var current    = $(this);
        var subject_id = $(this).val();

    // $('#subject_id').on('change',function(){
        // alert(pre_subject_id);
        var random_id = Math.floor((Math.random() * 155099*subject_id) + 1);;
        current.closest('select').attr('name', 'content['+random_id+'][subject]');
        current.closest('tr').find('#unit_id').attr('name', 'content['+random_id+'][unit_id]');
        $('.loader').show();
        if(subject_id!=''){

            $.ajax({
                type:"get",
                url: "{{ url('get/units') }}"+"/"+subject_id,
                
                success:function(resp){
                    current.closest('tr').find('#unit_id').html(resp);
                    $('.loader').hide();
                },
            });
        }else{
            current.closest('tr').find('#unit_id').html('<option value="" readonly>Choose Unit</option>');
        }
    });
});
</script>
<!-- <script type="text/javascript">
    $(document).on('click','.unit_class',function(){
        var unit_id = $(this).val();
        var current = $(this);
        if(unit_id!=''){
        alert(unit_id);

            current.closest('#subject_div').closest('#subject_div').find("#unit_id option[value =" + unit_id + "]").hide();
        }
    });
</script> -->
@endsection

@endsection    