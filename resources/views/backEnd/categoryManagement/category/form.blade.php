<?php
    if(isset($category_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Trainer')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Category</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/categories') }}">Category Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Category</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="category_form">
                        <div class="card-body">
                            <h4 class="card-title" id="category">{{$task}} Category({{@$category->name}})</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ isset($category['name'])? $category['name']: '' }}" id="category_input">
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="category_id" id="category_id" value="{{@$category_id}}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    var form = $('#category_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:100,
                regex:/^[a-zA-z - ' .]+$/,
                remote:{
                    url:"{{url('admin/validate/category/name')}}",
                    data:{
                        category_id:function(){
                            return $('#category_id').val();
                        },
                    },   
                }, 
            },
        },
        messages:{
            name:{
                remote:'This category already exists.', 
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $('#category_input').on('input',function(){
        var value = $(this).val();
        if(value!=''){
            $('#category').text('Edit Category('+value+")");
        }else{
            $('#category').text('Edit Category');
        }
    });
</script>
@endsection
