<?php
    if(isset($sub_sub_category_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Sub Sub-Category')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Sub Sub-Category</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/categories') }}">Category Management</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/subcategories/'.$category_id) }}">Sub Categories</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/sub-subcategories/'.$sub_category_id) }}">Sub Sub-Categories</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Sub Sub-Category</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="category_form">
                        <div class="card-body">
                            <h4 class="card-title" id="sub_sub_category_input_id">{{ $task }} 
                            
                            @if(!empty($sub_sub_category['name']))
                                Sub Sub-Category({{ @$sub_sub_category['name'] }})
                            @else
                                Sub Sub-Category 
                            @endif
                        </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="sub_sub_category__input" placeholder="Name" name="name" value="{{ isset($sub_sub_category['name'])? $sub_sub_category['name']: '' }}" >
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="sub_sub_category_id" id="sub_sub_category_id" value="{{@$sub_sub_category_id}}">
                                <input type="hidden" name="sub_category_id" id="sub_category_id" value="{{@$sub_category_id}}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    var form = $('#category_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:100,
                regex:/^[a-zA-z - ' .]+$/,
                remote:{
                    url:"{{url('admin/validate/sub-subcategory/name')}}",
                    data:{
                        sub_sub_category_id:function(){
                            return $('#sub_sub_category_id').val();
                        },
                        sub_category_id:function(){
                            return $('#sub_category_id').val();
                        },
                    },   
                }, 
            },
        },
        messages:{
            name:{
                remote:'This sub subcategory already exists.', 
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $('#sub_sub_category__input').on('input',function(){
        var value = $(this).val();
        // alert(value);
        if(value!=''){
            $('#sub_sub_category_input_id').text('{{$task}} Sub Sub-Category('+value+')');
        }else{
            $('#sub_sub_category_input_id').text('{{$task}} Sub Sub-Category');
        }
    });
</script>
@endsection
