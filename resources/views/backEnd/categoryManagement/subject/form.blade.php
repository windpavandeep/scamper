<?php
    if(isset($subject_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Subject')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Subject</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/subjects') }}">Category Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Subject</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="subject_form">
                        <div class="card-body">
                            <h4 class="card-title">{{$task}} Subject
                            </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="category_id" id="category_id">
                                       <option value="">Choose Category</option>
                                       @foreach($categories as $category)
                                           <option value="{{ $category['id'] }}" <?php if(@$category['id']==@$subject['category_id']){echo'selected';}?>>{{ucfirst($category['name'])}}</option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                <div class="col-sm-7">
                                    <select class="niceselc form-control" name="sub_category_id" id="sub_category">
                                        <option data-display="Choose Category" value="">    Choose Sub-Category</option>
                                        @if(!empty($sub_category))
                                            @foreach($sub_category as $value)
                                                <option value="{{$value['id']}}" <?php if(@$value['id']==@$subject['sub_category_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Sub-Category 2:</label>
                                <div class="col-sm-7">
                                    
                                    <select class="niceselc form-control" name="sub_subcategory_id" id="sub_subcategory">
                                        <option data-display="Choose Sub-Category 2" value="" readonly>Choose Sub-Category 2</option>
                                        @if(!empty($sub_sub_category))
                                            @foreach($sub_sub_category as $value)
                                                <option value="{{$value['id']}}" <?php if(@$value['id']==@$subject['sub_sub_category_id']){echo'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                               
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <span class="error1" id="subject_error"></span>
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ isset($subject['name'])? $subject['name']: '' }}" id="subject_input">
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="subject_id" id="subject_id" value="{{@$subject_id}}">
                            
                                <input type="hidden" name="error" value="0" id="total_error">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    $(document).ready(function(){

        var form = $('#subject_form');
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules:{
                category_id:{
                    required:true,
                },
                sub_category_id:{
                    required:true,
                },
                sub_subcategory_id:{
                    required:true,
                },
                name:{
                    required:true,
                    minlength:2,
                    maxlength:100,
                    regex:/^[a-zA-z - ' .]+$/,
                },
            },
            
            submitHandler:function(form){
                var error = $('#total_error').val();
                
                if(error<1){

                    form.submit();
                }
                
            },
        });
    })
</script>
<!-- <script type="text/javascript">
    $(document).ready(function(){
        $('#subject_input').on('input',function(){
            var value = $(this).val();
            if(value!=''){
                $('#subject').text('{{$task}} Subject('+value+")");
            }else{
                $('#subject').text('{{$task}} Subject');
            }
        });
    });
</script> -->
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        if(category_id==''){
            category_id = 0;
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
                
                // $('#unit_id').html(resp.units);

                $('#sub_subcategory').html('<option data-display="Choose Category" value="">Choose Sub-Category 2</option>');
                $('.loader').hide();
            }
        })
    });
</script>

<script type="text/javascript">

    $('#sub_category').on('change', function() {
        $('.loader').show();
        var sc = $(this).val();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-subcategories') }}"+"/"+sc,
            success:function(resp){
                $('#sub_subcategory').html(resp);
                $('.loader').hide();
            }
        })
    });

</script>
<script type="text/javascript">
    $('#sub_subcategory').on('change',function(){
        var subject_name = $('#subject_input').val();
        var subject_id   = $('#subject_id').val();
        var value        = $(this).val();
        var error        = $('#total_error').val();
        $('#subject_error').text('');
        if(value!=''){
            $('#sub_sub_category_id').val(value);
        }
        if(subject_name!=''){

            $.ajax({
                type:"POST",
                url: "{{ url('/admin/validate/subject/name') }}",
                data:{
                    "_token":"{{csrf_token()}}",
                    name:subject_name,
                    subject_id:subject_id,
                    sub_sub_category_id:value,
                },
                success:function(resp){
                    // alert(resp);
                    if(resp=='false'){

                        $('#total_error').val('1');
                        $('#subject_error').text('This subject already exists.');
                    }else{
                        $('#subject_error').text('');
                        $('#total_error').val('0');
                    }
                },
            });
        }
    });
</script>
<script type="text/javascript">
    $('#subject_input').on('input',function(){
        var subject_name = $(this).val();
        var subject_id   = $('#subject_id').val();
        var value        = $('#sub_subcategory').val();
        var error        = $('#total_error').val();
        $('#subject_error').text('');
        if(value!=''){
            $('#sub_sub_category_id').val(value);
        }
        if(subject_name!=''){

            $.ajax({
                type:"POST",
                url: "{{ url('/admin/validate/subject/name') }}",
                data:{
                    "_token":"{{csrf_token()}}",
                    name:subject_name,
                    subject_id:subject_id,
                    sub_sub_category_id:value,
                },
                success:function(resp){
                    // alert(resp);
                    if(resp=='false'){

                        $('#total_error').val('1');
                        $('#subject_error').text('This subject already exists.');
                    }else{
                        $('#subject_error').text('');
                        $('#total_error').val('0');
                    }
                },
            });
        }
    });
</script>
@endsection
