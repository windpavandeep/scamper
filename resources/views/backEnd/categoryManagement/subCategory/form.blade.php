<?php
    if(isset($sub_category_id)){
        $task = 'Edit';
        $url  = url('admin/subcategory/edit/'.$sub_category_id);
    }else{
        $task = 'Add';
        $url  = url('admin/subcategory/add/'.$category_id);
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Sub Category')
@section('content')
<style type="text/css">
    #abc{
        color: #ca5e58;
        margin-left: 261px;
        font-weight: 600;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Sub Category</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/categories') }}">Category Management</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/subcategories/'.$category_id) }}">Sub Categories</a></li>
                            
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Sub Category</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$url}}" id="category_form">
                        <div class="card-body">
                            <h4 class="card-title" id="sub_category">{{ $task }} 
                                @if(!empty($sub_category['name']))
                                    Sub Category({{ @$sub_category['name'] }})
                                @else
                                    Sub Category
                                @endif
                            </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="sub_category_input" placeholder="Name" name="name" value="{{ isset($sub_category['name'])? $sub_category['name']: '' }}">
                                </div>
                            </div>
                            <span id="abc"></span>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Show On Footer: </label>
                                <div class="col-sm-2">
                                    <input type="radio" name="add_to_footer" value="Yes" <?php if(@$sub_category->place_footer=='Yes'){echo'checked';}?>>
                                    <label for="fname" class="text-right control-label col-form-label">Yes </label>
                                </div>
                                 <div class="col-sm-2">
                                    <input type="radio" name="add_to_footer" value="No" <?php if(@$sub_category->place_footer=='No'){echo'checked';}?>>
                                    <label for="fname" class="text-right control-label col-form-label">No </label>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="sub_category_id" id="sub_category_id" value="{{@$sub_category_id}}">
                                <input type="hidden" name="category_id" id="category_id" value="{{@$category_id}}">
                                <button typ ="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px" name="myButton" id="submit_button">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    var form = $('#category_form');
    form.validate({
        
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:100,
                regex:/^[a-zA-z - ' .]+$/,
                remote:{
                    url:"{{url('admin/validate/subcategory/name')}}",
                    data:{
                        sub_category_id:function(){
                            return $('#sub_category_id').val();
                        },
                        category_id:function(){
                            return $('#category_id').val();
                        },
                    },   
                }, 
            },
            add_to_footer:{
                required:true,
            },
        },
        messages:{

            name:{
                remote:'This subcategory already exists.', 
            },
        },
        errorPlacement:function errorPlacement(error, element){ 
            if (element.attr('name') == 'add_to_footer') {
                $('#abc').text('*This field is required.');
            }else{

                element.before(error); 
            }
            /*var i = 0;

                if (element.attr('name') == 'name') {
                    i= 1;
                    // error.appendTo('#editor1_error');
                    $('#submit_button').removeAttr('disabled');
                   // alert('enter');
                    error.insertAfter(element);
                }
                if(i==1){
                    $('#submit_button').removeAttr('disabled');
                }else{
                    $('#submit_button').attr('disabled','disabled');
                }*/
                // $(InsetAfter(error);
            
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $('#sub_category_input').on('input',function(){
        var value = $(this).val();
        // alert(value);
        if(value!=''){
            $('#sub_category').text('{{$task}} Sub Category('+value+')');
        }else{
            $('#sub_category').text('{{$task}} Sub Category');
        }
    });
</script>
@endsection
