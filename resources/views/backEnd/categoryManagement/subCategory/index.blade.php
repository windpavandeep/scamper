@extends('backEnd.layouts.master')
@section('title','Sub Categories')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Category Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/categories') }}">Category Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sub Categories</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Sub Categories</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/subcategory/add/'.$category_id) }}" class="btn btn-primary">Add Sub Category</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($details))
                                        @foreach($details as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['name']) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/subcategory/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/subcategory/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{url('admin/sub-subcategories/'.$value['id'])}}" title="Sub Sub Categories"><i class="fa fa-crosshairs"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection