<?php
    if(isset($subject_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Subject')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Subject</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/subjects') }}">Category Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Subject</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="subject_form">
                        <div class="card-body">
                            <h4 class="card-title" id="subject">{{$task}} 
                                @if(!empty(@$subject->name))
                                    Subject({{@$subject->name}})
                                @else
                                    Subject
                                @endif
                            </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="{{ isset($subject['name'])? $subject['name']: '' }}" id="subject_input">
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="subject_id" id="subject_id" value="{{@$subject_id}}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    var form = $('#subject_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            name:{
                required:true,
                minlength:2,
                maxlength:100,
                regex:/^[a-zA-z - ' .]+$/,
                remote:{
                    url:"{{url('admin/validate/subject/name')}}",
                    data:{
                        subject_id:function(){
                            return $('#subject_id').val();
                        },
                    },   
                }, 
            },
        },
        messages:{
            name:{
                remote:'This subject already exists.', 
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#subject_input').on('input',function(){
            var value = $(this).val();
            if(value!=''){
                $('#subject').text('{{$task}} Subject('+value+")");
            }else{
                $('#subject').text('{{$task}} Subject');
            }
        });
    });
</script>
@endsection
