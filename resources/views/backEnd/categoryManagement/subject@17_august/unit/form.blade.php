<?php
    if(isset($unit_id)){
        $task = 'Edit';
    }else{
        $task = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Unit')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Unit</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/subject/unit/'.$subject_id) }}">Category Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Unit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="unit_form">
                        <div class="card-body">
                            <h4 class="card-title" id="subject">{{$task}}
                                @if(!empty(@$unit->name))
                                    Unit({{@$unit->name}})
                                @else
                                    Unit
                                @endif
                            </h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Name" name="unit_name" value="{{ isset($unit['name'])? $unit['name']: '' }}" id="subject_input">
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="unit_id" id="unit_id" value="{{@$unit_id}}">
                                <input type="hidden" name="subject_id" id="subject_id" value="{{@$subject_id}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @include('backEnd.common.footer')
@endsection    
@section('scripts')
<script>
    var form = $('#unit_form');
    form.validate({
        
        rules:{
            unit_name:{
                required:true,
                minlength:2,
                maxlength:100,
                regex:/^[ A-Za-z0-9 -]*$/,
                remote:{
                    url:"{{url('admin/validate/unit/name')}}",
                    data:{
                        subject_id:function(){
                            return $('#subject_id').val();
                        },
                        unit_id:function(){
                            return $('#unit_id').val();
                        },
                    },   
                }, 
            },
        },
        messages:{
            unit_name:{
                remote:'This unit already exists.', 
            },
        },
        errorPlacement:function errorPlacement(error, element){ 
            element.before(error); 
                // $(InsetAfter(error);
            
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $('#subject_input').on('input',function(){
        var value = $(this).val();
        // alert(value);
        if(value!=''){
            $('#subject').text('{{$task}} Unit('+value+')');
        }else{
            $('#subject').text('{{$task}} Unit');
        }
    });
</script>
@endsection
