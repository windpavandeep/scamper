@extends('backEnd.layouts.master')
@section('title','Home Page Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Home Page Content</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/home/page/first/section') }}">Home Page Content</a></li>
                            <li class="breadcrumb-item active" aria-current="page">First Section</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="first_section_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Home Page Content</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Title: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="First Title" name="first_title" value="{{ isset($content_details['first_title'])? $content_details['first_title']: '' }}" maxlength="50">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Second Title: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control"  placeholder="Second Title" name="second_title" value="{{ isset($content_details['second_title'])? $content_details['second_title']: '' }}" maxlength="40">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Third Title: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control"  placeholder="Third Title" name="third_title" value="{{ isset($content_details['third_title'])? $content_details['third_title']: '' }}" maxlength="80">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control"  placeholder="Contact No" name="contact_no" value="{{ isset($content_details['contact_no'])? $content_details['contact_no']: '' }}">
                                </div>
                            </div>
                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="student_id" value="{{ @$student_id }}" id="student_id">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')


<script type="text/javascript">
    $('#first_section_form').validate({
        rules:{
            first_title:{
                required:true,
                
            },
            second_title:{
                required:true,
                
            },
            third_title:{
                required:true,
                
            },
            contact_no:{
                required:true,
                // digits:true,
                minlength:7,
                maxlength:15,
                regex:/^[ 0-9 +]+$/
                
            },
        },
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
    });
</script>

@endsection

@endsection    