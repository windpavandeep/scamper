@extends('backEnd.layouts.master')
@section('title','Home Page Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Home Page Content</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/home/page/second/section') }}">Home Page Content</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Second Section</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="second_section_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <?php 
                                $readonly = '';
                                if(@$content_id==5){
                                    $readonly = "readonly";
                                }
                            ?>
                            <h4 class="card-title">Second Section</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{ isset($second_section_content['title'])? $second_section_content['title']: '' }}" maxlength="40" {{$readonly}}>
                                </div>
                            </div>
                            @if(@$content_id!=5)
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description: </label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" placeholder="Description" name="description" maxlength="200">{{ isset($second_section_content['description'])? $second_section_content['description']: '' }}</textarea>
                                </div>
                            </div>
                            @endif
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                <div class="col-md-7">
                                    <?php  
                                        $image = DefaultImgPath;
                                        if (!empty($second_section_content->image)) {
                                            // dd($second_section_content->image);
                                            if (file_exists(HomeContentImageBasePath.'/'.$second_section_content->image)) {
                                               $image = HomeContentImageImgPath.'/'.$second_section_content->image;
                                            }
                                                
                                        }
                                    ?>
                                   
                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image</label>
                                <div class="col-md-7">
                                   <input type="file" name="image" id="img_upload">
                                    @if(@$content_id==5)
                                        <div class="alert-warning" style="padding:5px; margin-right:32%; margin-top:1%;">
                                            <span> <i class="fa fa-warning m-r-5"> </i> Recommended size of the image is 695 x 350 (W x H) pixels </span>   
                                        </div>  
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#second_section_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/
            },
            description:{
                required:true,
                
            },
        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }

        });

    });
</script>
@endsection

@endsection    