@extends('backEnd.layouts.master')
@section('title','Home Page Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Home Page Content</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/homepage/third/section') }}">Home Page Content</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Third Section</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="third_section_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Third Section</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{ isset($third_section_content['title'])? $third_section_content['title']: '' }}" maxlength="40">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description: </label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" placeholder="Description" name="description" maxlength="400">{{ isset($third_section_content['description'])? $third_section_content['description']: '' }}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                <div class="col-md-7">
                                    <?php  
                                        
                                        if (!empty($third_section_content->image)) {
                                            // dd($second_section_content->image);
                                            if (file_exists(HomeContentImageBasePath.'/'.$third_section_content->image)) {
                                               $image = HomeContentImageImgPath.'/'.$third_section_content->image;
                                            }else{
                                                $image = DefaultImgPath;
                                                
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }

                                        $video    = 'DefaultVideoImgPath';
                                        $file_url = 'javascript:;';
                                        if(!empty(@$third_section_content->video)){
                                            if(file_exists(HomeContentImageBasePath.'/'.$third_section_content->video)){
                                                $file_url = HomeContentImageImgPath.'/'.$third_section_content->video;
                                                $video = DefaultVideoImgPath;
                                                
                                            }
                                        }
                                        
                                    ?>
                                   
                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image</label>
                                <div class="col-md-7">
                                    <input type="file" name="image" id="img_upload">
                                </div>
                                
                            </div>
                            @if($content_id==2)
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Video</label>
                                    <div class="col-md-7">
                                        <a href="{{$file_url}}" target="blank" title="Preview Video">
                                        <img src="{{$video}}" width="100%" height="100%" id="old_video" alt="No image" class="modal-upload-image-preview"></a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="video">Upload Video</label>
                                    <div class="col-md-7">
                                        <input type="file" name="video" id="video_upload" accept =".mov,.mp4,.avi">
                                    </div>
                                    
                                </div>
                            @endif
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#third_section_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z & -]+$/
            },
            description:{
                required:true,
                
            },
            image:{
                accept: "jpg|jpeg|png|svg"
            },  
        },
        messages:{
            image:{
                accept:'Please select an image of .jpeg, .jpg, .svg, .png file format.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png'|| ext == 'svg')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            }

        });

    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_video').attr('src',"{{DefaultVideoImgPath}}");
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#video_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'mp4' || ext == 'mov' || ext == 'avi')
                {   

                    input = document.getElementById('video_upload');

                    readURL(this);
                }
            }

        });

    });
</script>
@endsection

@endsection    