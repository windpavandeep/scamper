@extends('backEnd.layouts.master')
@section('title','Pending Payments')
@section('content')
<style type="text/css">
    .payment_button{
        text-align:right;
        margin-top: 20px;
        margin-right: 20px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Payment Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Paid Payments</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Transaction ID</th>
                                        <!-- <th>Course Title</th> -->
                                        <th>Paid On</th>
                                        <th>Teacher Name</th>
                                        <th>Content Type</th>
                                        <th>Total Price</th>
                                        <th>Admin Commission</th>
                                        <th>Paid Amount</th>
                                        <!-- <th width="13%">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($payments))
                                        @foreach($payments as $key=>$value)
                                            <tr>
                                                <td>{{ $value['razor_pay_id'] }} </td>
                                                
                                                <!-- <td>{{ ucfirst($value['title']) }}</td> -->
                                                
                                                
                                                <td>
                                                    {{ date('d/m/Y',strtotime($value['paid_on'])) }} 
                                                </td>
                                                <td>
                                                    @if(!empty($value['trainer_contents']['trainer_detail']['first_name']))
                                                        {{ ucfirst($value['trainer_contents']['trainer_detail']['first_name']) }}  {{ ucfirst($value['trainer_contents']['trainer_detail']['last_name']) }}
                                                    @else
                                                        {{ ucfirst($value['subscription_plan']['trainer_detail']['first_name']) }}  {{ ucfirst($value['subscription_plan']['trainer_detail']['last_name']) }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($value['subscription_plan_tile']=='')
                                                        Subscription Plan
                                                    @else
                                                        Product
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $value['price'] }}
                                                </td>
                                                <td>
                                                    {{ 
                                                        $value['admin_commission'] 
                                                    }}
                                                </td>
                                                <td>
                                                    {{ 
                                                        $value['trainer_price'] 
                                                    }}
                                                </td>
                                                <!-- <td>
                                                    <a href="{{ url('/admin/pending/payment/detail/'.$value['id']) }}" title="Info"><i class="fa fa-eye"></i>
                                                    </a>
                                                    
                                                </td> -->
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection