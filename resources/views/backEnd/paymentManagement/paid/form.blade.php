@extends('backEnd.layouts.master')
@section('title','Pending Payment Detail')
@section('content')
<style type="text/css">
    .ck{
        margin-top: 8px !important;
    }
    .reply_field{
        margin-left: 10px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Pending Payment Detail</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('admin/pending/payments')}}">Pending Payment</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Pending Payment Detail</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                        <div class="card-body">
                            <h4 class="card-title">Pending Payment Detail</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Customer Name: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{ucfirst(@$payments['first_name'])}} {{ucfirst(@$payments['last_name'])}}
                                    </label>
                                </div>
                            </div>
                           
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email: </label>
                                <div class="col-sm-8">
                                    
                                    <label class="col-sm-8 ck">
                                        {{@$payments['email']}}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{@$payments['contact']}}
                                    </label>
                                </div>
                            </div>
                            @if(!empty($payments['trainer_contents']['trainer_detail']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Trainer Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-8 ck">
                                            {{ucfirst(@$payments['trainer_contents']['trainer_detail']['first_name'])}}                                             {{ucfirst(@$payments['trainer_contents']['trainer_detail']['last_name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($payments['trainer_contents']['category']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Category Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-8 ck">
                                            {{ucfirst(@$payments['trainer_contents']['category']['name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($payments['trainer_contents']['sub_category']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-8 ck">
                                            {{ucfirst(@$payments['trainer_contents']['sub_category']['name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($payments['trainer_contents']['sub_sub_category']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Sub-Category-2 Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-8 ck">
                                            {{ucfirst(@$payments['trainer_contents']['sub_sub_category']['name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($payments['trainer_contents']['subject']))
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Subject Name: </label>
                                    <div class="col-sm-8">
                                        <label class="col-sm-8 ck">
                                            {{ucfirst(@$payments['trainer_contents']['subject']['name'])}}
                                        </label>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Course Name: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-8 ck">
                                        {{ucfirst(@$payments['trainer_contents']['title'])}}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Content Type: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-8 ck">
                                        {{ucfirst(@$payments['trainer_contents']['upload_type'])}}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Purchased On: </label>
                                
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{ date('d/m/Y',strtotime($payments['purchased_on'])) }} 
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Valid Till: </label>
                                
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{ date('d/m/Y',strtotime($payments['valid_till'])) }} 
                                    </label>
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Transaction ID: </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-7 ck">
                                        {{ ucfirst(@$payments['razor_pay_id']) }}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Price({{CURRENCY}}): </label>
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                    
                                        {{number_format(@$payments['price'],2)}}
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Admin Commission({{CURRENCY}}): </label>
                                
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{number_format(@$payments['admin_commission'],2)}}
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Trainer Price({{CURRENCY}}): </label>
                                
                                <div class="col-sm-8">
                                    <label class="col-sm-3 ck">
                                        {{number_format(@$payments['trainer_price'],2)}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="border-top">
                            <div class="card-body">
                                
                                @csrf
                                <button class="btn btn-primary" style="float: right;margin-bottom: 20px">
                                    Submit
                                </button>
                            </div>
                        </div> -->
                    
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>


@endsection    