@extends('backEnd.layouts.master')
@section('title','Pending Payments')
@section('content')
<style type="text/css">
    .payment_button{
        text-align:right;
        margin-top: 20px;
        margin-right: 20px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Payment Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Pending Payments</h5>
                        <div class="table-responsive">
                            <form method="post" action="{{url('/admin/pending/payment/pay')}}">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Select To Pay</th>
                                            <th>Transaction ID</th>
                                            <!-- <th>Course Title</th> -->
                                            <th>Total Price</th>
                                            <th>Purchased On</th>
                                            <th>Content Type</th>
                                            <th>Commission</th>
                                            <th>Trainer Amount</th>
                                            <th width="13%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(!empty($payments))
                                            @foreach($payments as $key=>$value)
                                                <tr>
                                                    <td> <input type="checkbox" name="transaction_id[]" value="{{ $value['id'] }}">  </td>
                                                    <td>{{ $value['razor_pay_id'] }} </td>
                                                    
                                                    <!-- <td>{{ ucfirst($value['title']) }}</td> -->
                                                    
                                                    <td>
                                                        {{ $value['final_total'] }}
                                                    </td>
                                                    
                                                    <td>
                                                        {{ date('d/m/Y',strtotime($value['purchased_on'])) }} 
                                                    </td>
                                                    <td>
                                                    @if($value['title']=='')
                                                        Subscription Plan
                                                    @else
                                                        Product
                                                    @endif
                                                     </td>
                                                    <td>
                                                        {{ 
                                                           $value['admin_commission']
                                                        }}
                                                    </td>
                                                    <td>
                                                        {{ 
                                                            $value['trainer_price'] 
                                                        }}
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('/admin/pending/payment/detail/'.$value['id']) }}" title="Info"><i class="fa fa-eye"></i>
                                                        </a>
                                                        
                                                    </td>
                                                </tr>
                                            @endforeach    
                                        @endif
                                    </tbody>
                                </table>
                                @if(count($payments)>0)
                                    <div  class="payment_button">
                                        <button typ ="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px" name="myButton" id="submit_button">Pay</button>
                                        @csrf
                                    </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection