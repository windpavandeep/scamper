@extends('backEnd.layouts.master')
@section('title','Edit Section1 Content')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Section1</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/about-us') }}">About Us</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Section1 Content</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="about_us_form">
                        <div class="card-body">
                            <h4 class="card-title">Edit Section1 Content</h4>
                           
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="10" placeholder="Description" name="desc" id="desc">{{ isset($section1_detail['description'])?$section1_detail['description']:'' }}</textarea>
                                </div>
                            </div>
                        
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
    var form = $('#about_us_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        ignore: [],
        rules:{
            desc:{
                required: function(){
                    CKEDITOR.instances.desc.updateElement();
                },
                minlength:2,
                maxlength:1500,
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
@endsection
@endsection    