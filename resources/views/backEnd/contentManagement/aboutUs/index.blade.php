@extends('backEnd.layouts.master')
@section('title','About Us')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">About Us</h5>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Section1 Content
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/about-us/section1/edit') }}" title="Edit"><i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Section2 Content
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/about-us/section2') }}" title="view"><i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Our Partners
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/about-us/our-partners') }}" title="view"><i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection