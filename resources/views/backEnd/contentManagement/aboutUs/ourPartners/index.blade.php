@extends('backEnd.layouts.master')
@section('title','Our Partners')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/about-us') }}">About Us</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Our Partners</h5>
                            <div class="add_btn">
                                <a href="{{ url('admin/about-us/our-partner/add') }}" class="btn btn-primary">Add Our Partner</a>
                            </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <!-- <th>Image</th> -->
                                        <th width="85%">Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($our_partners))
                                        @foreach($our_partners as $key=>$value)
                                            <tr>
                                            
                                                <td>
                                                    <?php
                                                        if(!empty($value['image'])){
                                                            // echo'<pre>'; print_r($stories->image);die;
                                                            if(file_exists(AboutUsImageBasePath.'/'.$value['image'])){
                                                                $image = AboutUsImageImgPath.'/'.$value['image'];
                                                            }else{
                                                                $image = DefaultImgPath;
                                                            }
                                                        }else{
                                                            $image = DefaultImgPath;
                                                        }   
                                                        
                                                    ?>
                                                    <img src="{{$image}}"  style="width:40%; height:30px" id="old_image" alt="No image" >
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/about-us/our-partner/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/about-us/our-partner/delete/'.$value['id']) }}" title="Delete"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection