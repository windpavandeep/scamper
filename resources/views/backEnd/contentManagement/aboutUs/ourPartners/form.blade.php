<?php 
    if(isset($partner_id)){

        $task    = 'Edit';
        $id      = 'edit_form';
    }else{
        $task    = 'Add';
        $id      = 'add_form';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Our Partner')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Our Partner</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/about-us') }}">About Us</a></li>
                            <li class="breadcrumb-item"><a href="{{ url('admin/about-us/our-partners') }}">Our Partners</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Our Partner</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="{{$id}}"  enctype=multipart/form-data>
                        <div class="card-body">
                            <h4 class="card-title">Our Partner Content Info</h4>
                            <?php
                                if(!empty($our_partner->image)){
                                    // echo'<pre>'; print_r($stories->image);die;
                                    if(file_exists(AboutUsImageBasePath.'/'.$our_partner->image)){
                                        $image = AboutUsImageImgPath.'/'.$our_partner->image;
                                    }else{
                                        $image = DefaultImgPath;
                                    }
                                }else{
                                    $image = DefaultImgPath;
                                }   
                            ?>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Image :</label>
                                <div class="col-md-3 p-l-15 ">
                                    <img src="{{$image}}"  style="width: 133px; height: 131px;" id="old_image" alt="No image" >
                                </div>
                            </div>                                  
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">New Image :</label>
                                <div class="col-md-6">
                                    <input type="file" onChange="readURL(this);" id="img_upload" name="image" />    
                                </div>
                            </div>    

                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>

    $('#add_form').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            image:{
                required:true,
                accept: "jpg|jpeg|png|gif",
            },
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
    $('#edit_form').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            image:{
                accept: "jpg|jpeg|png|gif",
            },
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#old_image')
                        .attr('src', e.target.result)
                        .width(138)
                        .height(131);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
@endsection
@endsection    