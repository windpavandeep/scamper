@extends('backEnd.layouts.master')
@section('title','Header Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/header/content') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Header Content</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="header_content_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Header Content</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact No: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Contact No" name="contact_no" value="{{ isset($content['contact_no'])? $content['contact_no']: '' }}" maxlength="40">
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact Email: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Contact Email" name="email" value="{{ isset($content['email'])? $content['email']: '' }}" maxlength="40">
                                </div>
                            </div> -->
                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#header_content_form');
    form.validate({
        // errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            contact_no:{
                required:true,
            },
            email:{
                required:true,
                email: true,
            },
        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>


@endsection

@endsection    