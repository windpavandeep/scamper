@extends('backEnd.layouts.master')
@section('title','Our Faculty')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Our Faculty</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/our-faculty/add') }}" class="btn btn-primary">Add Our Faculty</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($detail))
                                        @foreach($detail as $key=>$value)
                                            <tr>
                                                <td>
                                                    {{ ucfirst($value['name']) }}</a>   
                                                </td>
                                                
                                                <td>
                                                    {{ ucfirst($value['designation']) }}</a>   
                                                </td>
                                                
                                                <td>
                                                    <a href="{{ url('/admin/our-faculty/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/our-faculty/delete/'.$value['id']) }}"  title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection