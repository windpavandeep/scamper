<?php 
    if(isset($pop_id)){
        $task    = 'Edit';
    }else{
        $task    = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Adv. Pop Up')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{$task}} Adv. Pop Up </h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/advertisement/pop-ups') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$task}} Adv. Pop Up</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="second_section_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">{{$task}} Adv. Pop Up</h4>
                            <?php
                                $title_hidden = '';
                                $description_hidden = '';
                                $image_hidden ='';
                                if(empty($pop_up->title) && $task=='Edit'){
                                    $title_hidden = 'hidden';
                                }

                                if(empty($pop_up->description) && $task=='Edit'){
                                    $description_hidden = 'hidden';
                                }
                                if(empty($pop_up->image) && $task=='Edit'){
                                    $image_hidden = 'hidden';
                                }
                            ?>
                            <div class="form-group row" {{$title_hidden}}>
                                <span class="error1" id="title_error"></span>
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title: </label>

                                <div class="col-sm-7">

                                    <input type="text" class="form-control" placeholder="Title" name="title" value="{{ isset($pop_up['title'])? $pop_up['title']: '' }}" maxlength="40" id="title">
                                </div>
                            </div>

                                <div class="form-group row" {{$description_hidden}}>
                                    <span class="error1" id="desc_error"></span>
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description: </label>
                                    <div class="col-sm-7">
                                        <textarea type="text" class="form-control" placeholder="Description" name="description" maxlength="200" id="desc">{{ isset($pop_up['description'])? $pop_up['description']: '' }}</textarea>
                                    </div>
                                </div>
                            <div class="form-group row">
                                <label for="fname4" class="col-sm-3 text-right control-label col-form-label">Url</label>
                                <div class="col-sm-7">
                                    <span class="error1" id="url_error"></span>
                                    <input type="text" class="form-control" id="url" placeholder="Url" name="url" value="{{ isset($pop_up['url'])?$pop_up['url']:'' }}">
                                </div>
                            </div>
                             <div class="form-group row" {{$image_hidden}}>
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                
                                <div class="col-md-7">
                                    <?php  
                                        if (!empty($pop_up->image)) {
                                            // dd($second_section_content->image);
                                            
                                                $image = AdvertisementPopUpImgPath.'/'.$pop_up->image;
                                            
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    ?>
                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row" {{$image_hidden}}>
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image</label>
                                <div class="col-md-7">
                                   <input type="file" name="image" id="img_upload">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image11">Status</label>
                                <div class="col-md-7">
                                    <select class="form-control" name="status">
                                        <option value="">Select Status</option>
                                        <option value="A" <?php if(@$pop_up->status=='A'){echo'selected';}?>>Active</option>
                                        <option value="I" <?php if(@$pop_up->status=='I'){echo'selected';}?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        <center><span class="error1" id="content_error"></span></center>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                @csrf
                                <input type="hidden" name="pop_id" value="{{@$pop_id}}" id="pop_id">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
@if($task=='Add')
<script>
    var form = $('#second_section_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            title:{
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z0-9 % -]+$/,
            },
            description:{
                maxlength:255,
            },
            url:{
                required:true,
                url: true,
                minlength:5,
                maxlength:50,
            },
            status:{
                required:true,
                remote:{
                    url:"{{url('admin/validate/pop-up/status')}}"   
                },
            },
        },
        messages:{
            status:{
                remote:'Only One advertisement pop-up can be active at a time'    
            },
        },
        
        
        submitHandler:function(form){

            var title = $('#title').val();
            var desc  =  $('#desc').val();
            input     = document.getElementById('img_upload').files.length;
            alert(input);
            $('#image_error').text('');
            $('#desc_error').text('');
            $('#title_error').text('');
            $('#content_error').text('');
            if(title =='' && desc==''&&input==''){
                $('#content_error').text('Please enter either title, description or image.');
                return false;
            }
            if(title==''&&input==''){
                $('#title_error').text('*This field is required.');
                return false;
            }
            if(desc==''&&input==''){
                $('#desc_error').text('*This field is required.');
                return false;
            }
            if(title!=''&& desc!='' &&input!=''){
                $('#content_error').text('Only title, description or image can be added at a time.');
                return false;
            }
            
            if(title ==''&& desc!=''&&input!=''){
                $('#content_error').text('Only title, description or image can be added at a time.');
                return false;
            }
            if(title !='' && desc==''&&input!=''){
                $('#content_error').text('Only title, description or image can be added at a time.');
                return false;
            }
            if(title !='' && desc==''&&input==''){
                $('#desc_error').text('*This field is required.');
                return false;
            }
            
            form.submit();
        },
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            }

        });

    });
</script>
@if($task=='Edit'&& !empty(@$pop_up->title))
    <script type="text/javascript">
    
        var form = $('#second_section_form');
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules:{
                title:{
                    required:true,
                    minlength:2,
                    maxlength:255,
                    regex:/^[a-zA-z0-9 % -]+$/,
                },
                description:{
                    required:true,
                    maxlength:255,
                },
                url:{
                    required:true,
                    url: true,
                    minlength:5,
                    maxlength:50,
                },
                status:{
                    required:true,
                    remote:{
                        url:"{{url('admin/validate/pop-up/status')}}",
                        data:{
                            pop_id:function(){
                                return $('#pop_id').val();
                            },
                        },   
                    },
                },
            },
            messages:{
                status:{
                    remote:'Only One advertisement pop-up can be active at a time'    
                },
            },
            
            
            submitHandler:function(form){  
                form.submit();
            },
        });
    </script>
@endif
@if($task=='Edit'&& !empty(@$pop_up->image))
    <script type="text/javascript">

        var form = $('#second_section_form');
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules:{
                url:{
                    required:true,
                    url: true,
                    minlength:5,
                    maxlength:50,
                },
                status:{
                    required:true,
                    remote:{
                        url:"{{url('admin/validate/pop-up/status')}}",
                        data:{
                            pop_id:function(){
                                return $('#pop_id').val();
                            },
                        },   
                    },
                },
            },
            messages:{
                status:{
                    remote:'Only One advertisement pop-up can be active at a time'    
                },
            },
            
            
            submitHandler:function(form){  
                form.submit();
            },
        });
    </script>
@endif
@endsection

@endsection    