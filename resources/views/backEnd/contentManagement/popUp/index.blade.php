@extends('backEnd.layouts.master')
@section('title','Adv. Pop Ups')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Adv. Pop Ups</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/advertisement/pop-up/add') }}" class="btn btn-primary">Add Adv. Pop Up</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($pop_ups))
                                        @foreach($pop_ups as $key=>$value)
                                            <tr>
                                                <td>{{ $value['title'] }}</td>
                                                <?php  
                                                    $image='';
                                                    if (!empty($value['image'])) {
                                                        // dd($second_section_content->image);
                                                        
                                                            $image = AdvertisementPopUpImgPath.'/'.$value['image'];
                                                        
                                                    }
                                                ?>
                                                <td> @if(!empty($value['image']))<img src="{{$image}}" width="40%" height="40%" id="old_image" class="modal-upload-image-preview">@endif</td>
                                                <td>
                                                    @if($value['status']=='A')
                                                        Active
                                                    @else
                                                        Inactive
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/advertisement/pop-up/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/advertisement/pop-up/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection