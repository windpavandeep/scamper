@extends('backEnd.layouts.master')
@section('title','Edit Terms & Conditions')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Terms &amp; Conditions</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item " aria-current="page"> <a href="{{url('/admin/term-condition/edit')}}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Terms &amp; Conditions</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="term_condition_form">
                        <div class="card-body">
                            <h4 class="card-title">Edit Terms &amp; Conditions</h4>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="10" placeholder="Description" name="desc" id="desc">{{ isset($terms_conditions['description'])?$terms_conditions['description']:'' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
    var form = $('#term_condition_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        ignore: [],
        rules:{
            desc:{
                required: function(){
                    CKEDITOR.instances.desc.updateElement();
                },
                minlength:2,
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
@endsection
@endsection    