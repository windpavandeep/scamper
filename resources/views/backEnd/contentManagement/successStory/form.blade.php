<?php 
    if(isset($sucess_story_id)){
        $task    = 'Edit';
        $form_id = 'edit_success_story_form';
    }else{
        $task    = 'Add';
        $form_id = 'add_success_story_form';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Success Story')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<link rel="stylesheet" href="{{ url('public/backEnd/css/jquery.rateyo.css') }}">
<script src="{{ url('public/backEnd/js/jquery.rateyo.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Success Story</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/success-stories') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Success Story</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="{{$form_id}}" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Success Story Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Name" name="name" value="{{ isset($success_story['name'])? $success_story['name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Designation: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname2" placeholder="Designation" name="designation" value="{{ isset($success_story['designation'])? $success_story['designation']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Ratings:</label>
                                <div class="col-sm-7">
                                <span class="error1" id="rating_error"></span>
                                    <?php  
                                        $rating = '0';
                                        if(isset($success_story['ratings'])){
                                            $rating = $success_story['ratings'];
                                        }
                                    ?>
                                    <div id="rateYo"></div>
                                    <input name="ratings" value="{{$rating}}" type="hidden" id="rating_count">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="8" placeholder="Description" name="desc">{{ isset($success_story['description'])?$success_story['description']:'' }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                               <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image:</label>
                                <div class="col-md-7">
                                    <?php 
                                        if(!empty($success_story['image'])){
                                            if(file_exists(SuccessStoryImageBasePath.'/'.$success_story['image'])){
                                                $image = SuccessStoryImageImgPath.'/'.$success_story['image'];
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }    
                                    ?>
                                    <img src="{{ $image }}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image:</label>
                                <div class="col-md-7">
                                   <input type="file" name="image" id="img_upload">
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $(function(){
            $("#rateYo").rateYo({
                fullStar: true,
                rating: "{{@$rating}}",
            });
        });
        $('#rateYo').on("rateyo.change",function(){
            var rating = $('#rateYo').rateYo("rating");
            // console.log(rating);
            $('#rating_count').val(rating);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }
        });
    });
</script>
<script>
    var form = $('#add_success_story_form');
        form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
            ignore: [],
            rules:{
                name:{
                    required:true,
                    minlength:2,
                    maxlength:100,
                    regex:/^[a-zA-z ]+$/
                },
                designation:{
                    required:true,
                    minlength:2,
                    maxlength:100,
                    regex:/^[a-zA-z ]+$/
                },
                image:{
                    required:true,
                    accept: "jpg|jpeg|png"
                },
                desc:{
                    required: function(){
                        CKEDITOR.instances.desc.updateElement();
                    },
                    minlength:2,
                    maxlength:500,
                },
            },
            messages:{
                image:{
                    accept:'Please select an image of .jpeg, .jpg, .png file format.',
                },
            },
            submitHandler:function(form){
                /*var rating = $('#rating_count').val();
                // alert(rating);
                if(rating=='0'){
                    $('#rating_error').html('Please select ratings from 1 to 5');
                    return false;
                }else{
                    $('#rating_error').html('');
                }*/
                form.submit();
            },
        });
    var form = $('#edit_success_story_form');
        form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
            ignore: [],
            rules:{
                name:{
                    required:true,
                    minlength:2,
                    maxlength:100,
                    regex:/^[a-zA-z ]+$/
                },
                designation:{
                    required:true,
                    minlength:2,
                    maxlength:100,
                    regex:/^[a-zA-z ]+$/
                },
                image:{
                    accept: "jpg|jpeg|png"
                },
                desc:{
                    required: function(){
                        CKEDITOR.instances.desc.updateElement();
                    },
                    minlength:2,
                    maxlength:500,
                },
            },
            messages:{
                image:{
                    accept:'Please select an image of .jpeg, .jpg, .png file format.',
                },
            },
            submitHandler:function(form){
                form.submit();
            },
        });
</script>
@endsection
@endsection    