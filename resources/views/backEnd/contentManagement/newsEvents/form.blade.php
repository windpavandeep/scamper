<?php 
    if(isset($news_event_id)){

        $task    = 'Edit';
        $id      = 'edit_news_event_form';
    }else{
        $task    = 'Add';
        $id      = 'add_news_event_form';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' News Event')
@section('content')
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} News Event</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/news-events') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} News Event</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="{{$id}}" enctype=multipart/form-data>
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} News Event</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Title" name="title" value="{{ isset($news_event['title'])? $news_event['title']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="10" placeholder="Description" name="desc" id="desc">{{ isset($news_event['description'])?$news_event['description']:'' }}</textarea>
                                </div>
                            </div>
                            <?php
                                if(!empty($news_event->image)){
                                    // echo'<pre>'; print_r($stories->image);die;
                                 // echo'<pre>'; print_r($become_trainer->image);die;
                                    if(file_exists(NewsEventImageBasePath.'/'.$news_event->image)){
                                        $image = NewsEventImageImgPath.'/'.$news_event->image;
                                    }else{
                                        $image = DefaultImgPath;
                                    }
                                }else{
                                    $image = DefaultImgPath;
                                }   
                                 // $media_image = systemImgPath.'/'.'index.png';
                            ?>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">Image :</label>
                                <div class="col-md-3 p-l-15 ">
                                    <img src="{{$image}}"  style="width: 133px; height: 131px;" id="old_image" alt="No image" >
                                </div>
                            </div>                                  
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label">New Image :</label>
                                <div class="col-md-6">
                                    <input type="file" onChange="readURL(this);" id="img_upload" name="image" />    
                                </div>
                            </div>    
                        
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>

    $('#add_news_event_form').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        ignore: [],
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:60,
            },
            image:{
                required:true,
                accept: "jpg|jpeg|png|gif",
            },
        
            desc:{
                required: function(){
                    CKEDITOR.instances.desc.updateElement();
                },
                minlength:2,
                maxlength:2500,
            },
        },
        messages:{

            image:{

                accept:'Only jpg,jpeg,gif and png images are allowed.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });


    $('#edit_news_event_form').validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        ignore: [],
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:60,
            },
        
            desc:{
                required: function(){
                    CKEDITOR.instances.desc.updateElement();
                },
                minlength:2,
                maxlength:2500,
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#old_image')
                        .attr('src', e.target.result)
                        .width(138)
                        .height(131);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
@endsection
@endsection    