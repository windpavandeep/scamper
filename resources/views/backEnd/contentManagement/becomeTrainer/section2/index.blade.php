@extends('backEnd.layouts.master')
@section('title','Section2')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/become-trainers') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Section2</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Section2</h5>
                            <div class="add_btn">
                                <a href="{{ url('admin/become-trainer/section2/add') }}" class="btn btn-primary">Add Section2</a>
                            </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <!-- <th>Image</th> -->
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($become_trainers))
                                        @foreach($become_trainers as $key=>$value)
                                            <tr>
                                            
                                                <td>{{ $value['title'] }}</td>
                                               
                                                <td>
                                                    <a href="{{ url('/admin/become-trainer/section2/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/become-trainer/section2/delete/'.$value['id']) }}" title="Delete"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection