@extends('backEnd.layouts.master')
@section('title','Become Trainers')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Become Trainer</h5>
                           <!--  <div class="add_btn">
                                <a href="{{ url('admin/become-trainer/add') }}" class="btn btn-primary">Add Become Trainer</a>
                            </div> -->
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <!-- <th>Image</th> -->
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Section1</td>
                                        <td>
                                            <a href="{{ url('/admin/become-trainer/section1/edit') }}" title="Edit"><i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Section2</td>
                                        <td>
                                            <a href="{{ url('/admin/become-trainer/section2') }}" ><i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection