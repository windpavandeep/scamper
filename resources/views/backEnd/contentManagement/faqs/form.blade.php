<?php 
    if(isset($faq_id)){

        $task    = 'Edit';
    }else{
        $task    = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' FAQ')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} FAQ</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/faqs') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} FAQ</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="faq_form">
                        <div class="card-body">
                            <h4 class="card-title">FAQ Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Question:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Enter Question" name="title" value="{{ isset($faq_details['title'])? $faq_details['title']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Answer:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control" rows="10" placeholder="Enter Answer" name="desc" id="desc">{{ isset($faq_details['description'])?$faq_details['description']:'' }}</textarea>
                                </div>
                            </div>
                        
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
    var form = $('#faq_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            title:{
                required:true,
                minlength:2,
                maxlength:80,
            },
            desc:{
                required:true,
                minlength:2,
                maxlength:1200,
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
@endsection
@endsection    