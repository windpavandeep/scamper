@extends('backEnd.layouts.master')
@section('title','Edit Welcome Sms')
@section('content')
<!-- <script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script> -->
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Welcome Sms</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                           <li class="breadcrumb-item " aria-current="page"> <a href="{{url('/admin/welcome-sms/edit')}}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Welcome Sms</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="welcome_sms_form">
                        <div class="card-body">
                            <h4 class="card-title">Edit Welcome Sms</h4>
                            <div class="form-group row">
                                <label for="fname1" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="5" placeholder="Description" name="desc" id="desc">{{ isset($sms_content['description'])?$sms_content['description']:'' }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
    var form = $('#welcome_sms_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            desc:{
                required:true,
                minlength:2,
                maxlength:1000,
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
@endsection
@endsection    