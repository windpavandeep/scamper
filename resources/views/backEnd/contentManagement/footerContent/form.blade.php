@extends('backEnd.layouts.master')
@section('title','Footer Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Content Management</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/footer/content') }}">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Footer Content</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="footer_content_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Footer Content</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Address: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Address" name="address" value="{{ isset($content['address'])? $content['address']: '' }}" maxlength="70">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact Number: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Contact Number" name="contact_no" value="{{ isset($content['contact_no'])? $content['contact_no']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" placeholder="Email" name="email" value="{{ isset($content['email'])? $content['email']: '' }}" maxlength="40">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description: </label>
                                <div class="col-sm-7">
                                    <textarea type="text" class="form-control" placeholder="Description" name="description" maxlength="200">{{ isset($content['description'])? $content['description']: '' }}</textarea>
                                </div>
                            </div>
                            
                            
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')

<script>
    var form = $('#footer_content_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            address:{
                required:true,
            },
            description:{
                required:true,
            },
            email:{
                required:true,
                email:true,
            },
            contact_no:{
                required:true,
                minlength:10,
                regex:/^[0-9 + -]+$/,
                maxlength:15
            },
        },
        
        submitHandler:function(form){
            form.submit();
        },
    });
</script>


@endsection

@endsection    