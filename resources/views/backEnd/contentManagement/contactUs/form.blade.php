@extends('backEnd.layouts.master')
@section('title','Edit Contact Us Content')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Edit Contact Us Content</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Contact Us Content</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="contact_us_content">
                        <div class="card-body">
                            <h4 class="card-title">Edit Contact Us Content</h4>
                            
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact Number:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname1" placeholder="Contact Number" name="contact_no" value="{{ isset($contact_us_content['contact_no'])? $contact_us_content['contact_no']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname2" class="col-sm-3 text-right control-label col-form-label">Address:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="address" placeholder="Address" name="address" value="{{ isset($contact_us_content['address'])? $contact_us_content['address']: '' }}">
                                    <div class="m-t-20" id="map"></div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Email" name="email" value="{{ isset($contact_us_content['email'])? $contact_us_content['email']: '' }}">
                                </div>
                            </div>

                        
                            <div class="border-top">
                                <div class="card-body">
                                    <input type="hidden" id="latitude" name="latitude" value="{{@$latitude}}">
                                    <input type="hidden" id="longitude" name="longitude" value="{{@$longitude}}">
                                    <input type="hidden" id="country" name="country" id="country" value="{{ isset($contact_us_content['country'])? $contact_us_content['country']: '' }}">
                                    <input type="hidden" id="administrative_area_level_2" name="city" value="{{ isset($contact_us_content['city'])? $contact_us_content['city']: '' }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.map')
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script type="text/javascript">
   
    var form = $('#contact_us_content');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            email:{
                  email:true,
                  required:true,
                  maxlength:100,
            },
            contact_no:{
                required:true,
                digits:true,
                minlength:10,
                maxlength:10
            },
            address:{
                required:true,
            },
            latitude:{
                required:true,
            },
            longitude:{
                required:true,
            },
        },
        messages:{
            latitude:{
                required:'Please select address from suggestions',
            },
        },
        submitHandler:function(form){
            form.submit();
        }
    });
</script>

@endsection
@endsection    