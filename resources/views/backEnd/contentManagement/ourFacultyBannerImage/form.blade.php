@extends('backEnd.layouts.master')
@section('title','Our Faculty Banner Image Edit')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Our Faculty Banner Image Edit</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="">Content Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Our Faculty Banner Image Edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Our Faculty Banner Image Edit</h4>
                           
                            
                            <div class="form-group row">
                               <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image:</label>
                                <div class="col-md-7">
                                    <?php 
                                        if(!empty($our_faculty_banner_image['image'])){
                                            if(file_exists(OurFacultyImageBasePath.'/'.$our_faculty_banner_image['image'])){
                                                $image = OurFacultyImageImgPath.'/'.$our_faculty_banner_image['image'];
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }    
                                    ?>
                                    <img src="{{ $image }}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image:</label>
                                <div class="col-md-7">
                                   <input type="file" name="image" id="img_upload" accept=".jpg,.png,jpeg">
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')

<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }
        });
    });
</script>

@endsection
@endsection    