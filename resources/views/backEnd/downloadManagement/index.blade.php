@extends('backEnd.layouts.master')
@section('title','Downloads')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Download Management</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Downloads</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/download/add') }}" class="btn btn-primary">Add Download</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($downloads))
                                        @foreach($downloads as $key=>$value)
                                            <tr>
                                                <td>
                                                    {{ ucfirst($value['name']) }}</a>   
                                                </td>
                                                
                                                
                                                <td>
                                                    <a href="{{ url('/admin/download/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/download/delete/'.$value['id']) }}"  title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection