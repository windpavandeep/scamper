<?php 
    if(isset($download_content_id)){
        $task    = 'Edit';
        $form_id = 'edit_download_form';
    }else{
        $task    = 'Add';
        $form_id = 'add_download_form';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Download')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Download</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/downloads') }}">Download Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Download</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="{{$form_id}}" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">Download Info</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Name" name="name" value="{{ isset($download_content['name'])? $download_content['name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Pdf</label>
                                <div class="col-md-7">
                                    <?php  
                                        $file_url = 'javascript:;';
                                        if (!empty($download_content->file)) {
                                            // dd(CourseFileBasePath.'/'.$course_detail->pdf);
                                            if (file_exists(DownloadBasePath.'/'.$download_content->file)) {
                                                $file_url = DownloadImgPath.'/'.$download_content->file;
                                                $pdf = DefaultPdfImgPath;
                                            }else{
                                                $pdf = DefaultImgPath;
                                                
                                            }
                                        }else{
                                            $pdf = DefaultImgPath;
                                        }

                                    ?>
                                   <a href="{{$file_url}}" target="blank">
                                    <img src="{{$pdf}}" width="100%" height="100%" id="old_pdf" alt="No image" class="modal-upload-image-preview"></a>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Pdf</label>
                                <div class="col-md-7">
                                    <input type="file" name="pdf" id="pdf_upload" accept=".pdf">
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')

<script>
    var form = $('#add_download_form');
        form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules:{
                name:{
                    required:true,
                    minlength:2,
                    maxlength:100
                },
                pdf:{
                    required:true,
                    accept:"pdf",
                },
            },
            messages:{
                pdf:{
                    accept:'Only pdf file format is allowed'
                },
            },
            submitHandler:function(form){
          
                form.submit();
            },
        });
   
</script>
<script>
    var form = $('#edit_download_form');
        form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules:{
                name:{
                    required:true,
                    minlength:2,
                    maxlength:100
                },
                pdf:{
                    accept:"pdf",
                },
            },
            messages:{
                pdf:{
                    accept:'Only pdf file format is allowed'
                },
            },
            submitHandler:function(form){
          
                form.submit();
            },
        });
   
</script>
   
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_pdf').attr('src',"{{DefaultPdfImgPath}}");
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#pdf_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext=="pdf")
                {
                    input = document.getElementById('pdf_upload');

                    readURL(this);
                }
            }
        });

    });
</script>
@endsection
@endsection    