@extends('backEnd.layouts.master')
@section('title','Students')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Student Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Students</h5>
                        <div class="add_btn">
                            <button type="button" class="btn btn-fit-height" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            Export <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right export-import" role="menu">
                                <li>
                                    <a href="javascript:;" class="report_optn" type="xls">Export to Excel</a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="report_optn" type="csv">Export to CSV</a>
                                </li>
                                <li>
                                    <a href="javascript:;" class="report_optn" type="pdf">Export to PDF</a>
                                </li>
                               <!--  <li>
                                    <a href="{{url('admin/student/export/pdf')}}" class="report_optn" type="pdf">Export to PDF</a>
                                </li> -->
                            </ul>
                            <a href="{{ url('admin/student/add') }}" class="btn btn-primary">Add Student</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Registration Date</th>
                                        <th>Contact Number</th>
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($student_details))
                                        @foreach($student_details as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['first_name']) }}</td>
                                                <td>{{ ucfirst($value['last_name']) }}</td>
                                                <td>{{ $value['email'] }}</td>
                                                <td>{{ date('d-m-Y',strtotime($value['created_at'])) }}</td>
                                                <td>{{ $value['contact'] }}</td>
                                                
                                                <td>
                                                    <a href="{{ url('/admin/student/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/student/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{ url('admin/student/credential/mail/'.$value['id']) }}" title="Send Credential"><i class="fa fa-envelope"></i></a>
                                                    <a href="{{ url('admin/student/exam/'.$value['id']) }}" title="Exams"><i class="fa fa-eye"></i></a>
                                                    <a href="{{ url('admin/student/subscriptions/'.$value['id']) }}" title="Subscription"><i class="mdi mdi-cash-usd"></i></a>

                                                       
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <div id="form_modal2" class="modal fade" role="dialog" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Export</h4>
                                    </div>
                                    <form action="{{url('admin/student/export')}}" class="form-horizontal" method="post" id="filter_modal">
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Date: </label>
                                                        <div class="col-sm-7">
                                                            <input type="date" class="form-control date_range" id="date_range" name="date" value="" placeholder="Select interval">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Domain: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="domain_id">
                                                                <option value="">Select Domain.</option>
                                                                @if(!empty($domains))
                                                                    @foreach($domains as $key=>$value)
                                                                        <option value="{{$value['id']}}">{{ $value['name'] }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Contact No.: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="contact">
                                                                <option value="">Select Contact No.</option>
                                                                @if(!empty($student_details))
                                                                    @foreach($student_details as $key=>$value)
                                                                        <option value="{{$value['contact']}}">{{ $value['contact'] }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Email Id: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="email_id">
                                                                <option value="">Select Email Id.</option>
                                                                @if(!empty($student_details))
                                                                    @foreach($student_details as $key=>$value)
                                                                        <option value="{{$value['id']}}">{{ $value['email'] }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Student: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="student_id">
                                                                <option value="">Select Student</option>
                                                                @if(!empty($student_details))
                                                                    @foreach($student_details as $key=>$value)
                                                                        <option value="{{$value['id']}}">{{ ucfirst($value['first_name']) }} {{ ucfirst($value['last_name']) }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Pincode: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="pincode">
                                                                <option value="">Select Pincode</option>
                                                                @if(!empty($student_details))
                                                                    @foreach($student_details as $key=>$value)
                                                                        @if(empty($value['pincode']))
                                                                            @continue    
                                                                        @endif
                                                                        <option value="{{$value['pincode']}}">{{ $value['pincode'] }}</option>
                                                                    @endforeach
                                                                @endif
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select Country: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="country_id" id="country_id">
                                                                <option value="">Select Country</option>
                                                                @foreach($countries as $country)
                                                                   <option value="{{$country['id']}}"  <?php if(@$student_details->country_id==$country['id']){echo 'selected';}?>>{{ ucfirst($country['name']) }}</option>
                                                                @endforeach
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">Select State: </label>
                                                        <div class="col-sm-7">
                                                            <select class="form-control" name="state_id" id="state_id">
                                                               <option value=''>Select State</option>
                                                               
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="form-group row">
                                                        <label for="fname" class="col-sm-5 text-right control-label col-form-label">City: </label>
                                                        <div class="col-sm-7">
                                                           <select class="form-control" name="city_id" id="city_id">
                                                               <option value=''>Select City</option>
                                                            
                                                           </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="export_type" value="" id="export_type">
                                        @csrf
                                        <div class="modal-footer">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button class="btn btn-secondary" data-dismiss="modal" aria-hidden="true">Close</button>
                                            <button class="btn btn-primary" id="export_btn" type="submit">Export to </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
<script type="text/javascript">
    $('.report_optn').on('click',function(){
        var value  = $(this).attr('type');
        $('#export_type').val(value);
    });
</script>
<script type="text/javascript">
    $('#country_id').on('change',function(){
        // alert('enter');
        var clickedIndex = $(this).val();
        if(clickedIndex==''){
            clickedIndex = '0';
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/states') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#state_id').html(resp);
                $('#city_id').html('<option value="">Select City </option>');
                // $("#state_id").selectpicker("refresh");
                // $("#city_id").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });
    $('#state_id').on('change',function(){

        var clickedIndex = $(this).val();
        var clickedIndex = $(this).val();
        if(clickedIndex==''){
            clickedIndex = '0';
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/cities') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#city_id').html(resp);
                $('.loader').hide();
            }
        })
    });
</script>
<script>
    $('.report_optn').click(function(){
        var value  = $(this).attr('type');
        // var url    = $(this).attr('url');
        $('#export_type').val(value);
        // $('#form_data').submit();

        if(value == 'xls'){
            // alert(url);
            // $('#filter_modal').attr('action',url);
            $('#export_btn').text('Export to excel');
            $('#form_modal2').modal('show');
        }else if(value == 'pdf'){
            // $('#filter_modal').attr('action',url);
            $('#export_btn').text('Export to pdf');
            $('#form_modal2').modal('show');
        }else{
            // $('#form_data').attr('action',url);
            // $('#filter_modal').attr('action',csv_url);
            $('#export_btn').text('Export to csv');
            $('#form_modal2').modal('show');
        }
    });
</script>
@endsection