<?php 
    if(isset($student_id)){
        $task    = 'Edit';
        $action  =  url('/admin/student/edit/'.$student_id);
    }else{
        $task    = 'Add';
        $action  =  url('/admin/student/add');
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Student')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Student</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/students') }}">Student Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Student</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="{{$action}}" id="Student_form" enctype="multipart/form-data">
                        <div class="card-body">
                            <h4 class="card-title">{{ $task }} Student</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="First Name" name="first_name" value="{{ isset($student_details['first_name'])? $student_details['first_name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Last Name: </label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname1" placeholder="Last Name" name="last_name" value="{{ isset($student_details['last_name'])? $student_details['last_name']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname5" class="col-sm-3 text-right control-label col-form-label">Date of Birth</label>
                                <div class="col-sm-7">
                                    <div class="input-group">
                                        <input type="text" class="form-control mydatepicker" placeholder="MM/DD/YYYY" name="dob" value="{{ isset($student_details['dob'])? date('d-m-Y',strtotime($student_details['dob'])): '' }}" id="mydate">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname12" placeholder="Email" name="email" value="{{ isset($student_details['email'])? $student_details['email']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Contact:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname21" placeholder="Contact Number" name="contact" value="{{ isset($student_details['contact'])? $student_details['contact']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Gender: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="gender">
                                       <option value=''>Select Gender</option>
                                       <option value="male" <?php if(@$student_details->gender=='male'){echo "selected";}?>>Male</option>
                                       <option value="female"  <?php if(@$student_details->gender=='female'){echo "selected";}?>>Female</option>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Country: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="country_id" id="country_id">
                                        <option value="">Select Country</option>
                                        @foreach($countries as $country)
                                           <option value="{{$country['id']}}"  <?php if(@$student_details->country_id==$country['id']){echo 'selected';}?>>{{ ucfirst($country['name']) }}</option>
                                        @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">State: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="state_id" id="state_id">
                                       <option value=''>Select State</option>
                                        @if(!empty($states))
                                            @foreach($states as $key=>$value)
                                                <option value="{{ $value['id'] }}"  <?php if(@$student_details->state_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">City: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="city_id" id="city_id">
                                       <option value=''>Select City</option>
                                        @if(!empty($states))
                                            @foreach($cities as $key=>$value)
                                                <option value="{{ $value['id'] }}"  <?php if(@$student_details->city_id==$value['id']){echo 'selected';}?>>{{ ucfirst($value['name']) }}</option>
                                            @endforeach
                                        @endif
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">District:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname01" placeholder="District" name="district" value="{{ isset($student_details['district'])? $student_details['district']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Address:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname21" placeholder="Address" name="address" value="{{ isset($student_details['address'])? $student_details['address']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname34" class="col-sm-3 text-right control-label col-form-label">Category: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="category_id" id="category_id">
                                        <option value=''>Select Category</option>
                                        @foreach($domains as $domain)
                                           <option value="{{$domain['id']}}"  <?php if(@$student_details['user_courses']['category_id']==$domain['id']){echo 'selected';}?>>{{ ucfirst($domain['name']) }}</option>
                                        @endforeach
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname34" class="col-sm-3 text-right control-label col-form-label">Sub Category: </label>
                                <div class="col-sm-7">
                                   <select class="form-control" name="sub_category_id" id="sub_category">
                                        <option value=''>Select Sub Category</option>
                                        @if(!empty($sub_categories))
                                            @foreach($sub_categories as $sub_category)
                                               <option value="{{$sub_category['id']}}"  <?php if(@$student_details['user_courses']['sub_category_id']==$sub_category['id']){echo 'selected';}?>>{{ ucfirst($sub_category['name']) }}</option>
                                            @endforeach
                                        @endif
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                <div class="col-md-7">
                                    <?php  
                                        if (!empty($student_details->image)) {
                                            // dd($student_details->image);
                                            if (file_exists(StudentProfileBasePath.'/'.$student_details->image)) {
                                                $image = StudentProfileImgPath.'/'.$student_details->image;
                                            }else{
                                                $image = DefaultImgPath;
                                            }
                                        }else{
                                            $image = DefaultImgPath;
                                        }
                                    ?>
                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" id="image">Upload Image</label>
                                <div class="col-md-7">
                                   <input type="file" name="image" id="img_upload">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname1" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                <div class="col-sm-7">
                                    <select name="status" class="form-control">
                                        <option value="">Select Status</option>
                                        <option value="A" <?php if(@$student_details->status=='A'){echo'selected';}?>  >Active</option>
                                        <option value="I" <?php if(@$student_details->status=='I'){echo'selected';}?>>Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="student_id" value="{{ @$student_id }}" id="student_id">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
    
</div>
@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        var today = new Date();
        $('.mydatepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    });
</script>
<script>
    var form = $('#Student_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            first_name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/
            },
            last_name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z ]+$/
            },
            category_id:{
                required:true,
            },
            sub_category_id:{
                required:true,
            },
            address:{
                required:true,
                minlength:2,
                maxlength:500
            },
            district:{
                required:true,
                minlength:2,
                maxlength:150
            },
            email:{
                required:true,
                email:true,
                remote:{
                    url:"{{url('admin/validate/student/email')}}",
                    data:{
                        student_id:function(){
                            return $('#student_id').val();
                        },
                    },   
                }, 
            },
            contact:{
                required:true,
                digits:true,
                minlength:7,
                maxlength:15,
                remote:{
                    url:"{{url('admin/validate/student/contact')}}",
                    data:{
                        student_id:function(){
                            return $('#student_id').val();
                        },
                    },   
                },
            },
            dob:{
                required:true
            },
            password:{
                required:true,
                regex:/^[a-zA-z 0-9 -)*&^$#@!(,.]+$/
            },
            confirm_password:{
                required:true,
                equalTo:"#password"
            },
            gender:{
                required:true
            },
            country_id:{
                required:true
            },
            state_id:{
                required:true
            },
            city_id:{
                required:true
            },
            Student_domain_id:{
                required:true
            },
            terms:{
                required:true
            },
            employment_status_id :{
                required:true
            },
            total_experiance:{
                required:true,
                maxlength:4,
                regex:/^[0-9 .]+$/,
            },
            image:{
                accept: "jpg|jpeg|png"
            }, 
            subject_id:{
                required:true     
            },
            pincode:{
                required:true,
                minlength:4,
                maxlength:10,
                regex:/^[0-9]+$/,     
            },
            organization_name:{
                required:true,
                minlength:2,
                maxlength:255,
                regex:/^[a-zA-z - ']+$/
            },
            status:{
                required:true,
            },
        },
        messages:{
            email:{
                remote:"This email-id already registered."
            },
            contact:{
                remote:"This contact number already registered."
            },
            image:{
                accept:'Please select an image of .jpeg, .jpg, .png file format.',
            },
        },
        submitHandler:function(form){
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $('#country_id').on('change',function(){
        // alert('enter');
        var clickedIndex = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/states') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#state_id').html(resp);
                $('#city_id').html('<option value="">Select City </option>');
                // $("#state_id").selectpicker("refresh");
                // $("#city_id").selectpicker("refresh");
                $('.loader').hide();
            }
        })
    });
    $('#state_id').on('change',function(){
        var clickedIndex = $(this).val();
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/cities') }}"+"/"+clickedIndex,
            success:function(resp){
                $('#city_id').html(resp);
                $('.loader').hide();
            }
        })
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        function readURL(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    $('#old_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#img_upload').change(function(){

            var img_name = $(this).val();

            if(img_name != '' && img_name != null)
            {
                var img_arr = img_name.split('.');

                var ext = img_arr.pop();
                ext = ext.toLowerCase();
                // alert(ext); return false;

                if(ext == 'jpeg' || ext == 'jpg' || ext == 'png')
                {
                    input = document.getElementById('img_upload');

                    readURL(this);
                }
            } else{

                $(this).val('');
                alert('Please select an image of .jpeg, .jpg, .png file format.');
            }

        });

    });
</script>
<script type="text/javascript">
    $('#category_id').on("change",function(){
        var category_id = $(this).val();
        if(category_id==''){
            category_id = 0;
        }
        $('.loader').show();
        $.ajax({
            type:"get",
            url: "{{ url('get/sub-categories') }}"+"/"+category_id,
            success:function(resp){
                $('#sub_category').html(resp.sub_category);
        
                $('.loader').hide();
            }
        })
    });
</script>
@endsection

@endsection    