@extends('backEnd.layouts.master')
@section('title','Questions')
@section('content')
<style type="text/css">
    .card-link {
        padding: 0;
        font-size: 22px;
    }
    .strong {
        font-weight: bold;
        font-size: 18px;
    }
    .comn_spp {
        margin: 0 20px;
        font-size: 17px;
    }
    p.markss {
        margin: 0;
    }
    .edt_sp {
        color: brown;
        cursor: pointer;
        text-decoration: underline;
    }
    .ck{
        margin-top: 7px;

    }
    .ck2{
        font-weight: bold;
    }

    #search_form_id .form-control{
        display: inline;
        width: 33%;
    }
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML">
</script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Question Bank</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/question-banks') }}">Exam Management</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Questions</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <!--  -->
                    <div class="card-body">
                        <form method="get" action="" id="search_form_id" style="display: flex">
                            <div class="col-sm-6" style="margin-bottom:4px">
                                <label for="lname1" class="ck2">Search:</label>
                                <select class="form-control" name="subject_id" id="subject_id" style="margin-left: 8px;">
                                    <option value="">Select Subject</option>
                                    @if(!empty($subjects))
                                        @foreach($subjects as $subject)
                                            <option value="{{$subject['id']}}" <?php if(@$subject_id==$subject['id']){echo'selected';}?>>{{ ucfirst($subject['name']) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <select class="form-control" name="unit_id" id="unit_id" style="margin-left: 25px">
                                    <option value="">Select Unit</option>
                                    @if(!empty($units))
                                        @foreach($units as $unit)
                                            <option value="{{$unit['id']}}"  <?php if(@$unit_id==$unit['id']){echo'selected';}?>>{{ ucfirst($unit['name']) }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </form>
                        <h5 class="card-title">Questions</h5>
                        <!-- <div class="add_btn">
                            <a href="{{ url('admin/exam/question-bank/question/import') }}" class="btn btn-primary">Import Questions</a>
                            <a href="{{ url('admin/exam/question-bank/question/add') }}" class="btn btn-primary">Add Question</a>
                        </div> -->
                        <div class="acrddn add_qutions">
                            <div id="accordion">
                                @if(empty($question_bank))
                                <div class="card">
                                    <div class="card-header">
                                        <p style="text-align: center;">No Record Found</p>
                                    </div>
                                </div>
                                @endif
                                @if(!empty($question_bank))
                                    @foreach($question_bank as $key=>$question)
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link" data-toggle="collapse" href="#collapse{{$key+1}}">
                                              Question {{$key+1}}
                                            </a>
                                            <p class="markss float-right">
                                                <span class="comn_spp mrks_num">Marks: {{@$question['marks']}}</span>
                                                <a href="{{url('/admin/question-bank/question/edit/'.$question['id'].'/')}}"><span class="comn_spp edt_sp"><i class="fa fa-edit"></i>Edit</span></a>
                                                <a href="{{url('/admin/question-bank/question/delete/'.$question['id'])}}"><span class="comn_spp edt_sp"><i class="fa fa-trash"></i>
                                                Delete</span></a>
                                            </p>
                                        </div>
                                        <div id="collapse{{$key+1}}" class="collapse show" data-parent="#accordion">
                                            <div class="card-body">
                                                <!-- Fields -->
                                                <div class="field_add">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <div class="form-group row">
                                                                   
                                                                    <div class="col-sm-6">
                                                                         <label for="lname1" class="control-label col-form-label ck2">Category Name:</label>

                                                                        <label class="ck">{{ $question['category_name'] }}</label>
                                                                    </div>     
                                                         
                                                                    <div class="col-sm-6">
                                                                        <label for="lname1" class="control-label col-form-label ck2"> Sub Category Name:</label>
                                                                        <label class=" ck">{{ $question['sub_category_name'] }}</label>
                                                                    </div>
                                                                </div>

                                                                @if(!empty($question['sub_sub_category']['name']))
                                                                <div class="form-group row">
                                                                    <div class="col-sm-6">
                                                                        <label for="lname1" class="control-label col-form-label ck2"> Sub Category-2 Name:</label>
                                                                        <label class="ck">{{ ucfirst($question['sub_sub_category']['name']) }} </label>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <label for="lname1" class="control-label col-form-label ck2"> Subject Name :</label>
                                                                        
                                                                        <label class="ck">{{ ucfirst($question['subject_name']) }}</label>
                                                                        
                                                                    </div>
                                                                </div>  
                                                                @endif 
                                                                <div class="form-group row">
                                                                    @if(empty($question['sub_sub_category']['name']))
                                                                        <div class="col-sm-6">
                                                                            <label for="lname1" class="control-label col-form-label ck2"> Subject Name :</label>
                                                                            
                                                                            <label class="ck">{{ ucfirst($question['subject_name']) }}</label>
                                                                            
                                                                        </div>
                                                                    @endif 
                                                                    <div class="col-sm-6">
                                                                        <label for="lname1" class="control-label col-form-label ck2"> Unit Name :</label>
                                                                       
                                                                        <label class="ck">{{ ucfirst($question['unit_name']) }}</label>
                                                                    
                                                                    </div> 
                                                                </div>
                                                               
                                                
                                                                <label class="strong">Question</label>
                                                                <p class="questionttl">
                                                                    @if(!empty($question['question']))
                                                                        {!! ucfirst(@$question['question']) !!}
                                                                    
                                                                        <div class="col-md-7">
                                                                            <?php  
                                                                                $image_url = 'javascript:;';
                                                                                if (!empty($question['ques_image'])) {
                                                                                    // dd($student_details->image);

                                                                                    if (file_exists(QuestionBankImageBasePath.'/'.$question['ques_image'])) {
                                                                                        $image = QuestionBankImageImgPath.'/'.$question['ques_image'];
                                                                                        $image_url = QuestionBankImageImgPath.'/'.$question['ques_image'];
                                                                                    }else{
                                                                                        $image = '';
                                                                                    }
                                                                                }else{
                                                                                    $image = '';
                                                                                }
                                                                            ?>
                                                                            @if(!empty($image))
                                                                                <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                                    <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="margin-bottom: 20px;">
                                                                                </a>
                                                                            @endif
                                                                        </div>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        @foreach($question['question_bank_options'] as $key=>$value)
                                                                
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label class="strong">Option @if($key==0)
                                                                        A
                                                                    @elseif($key==1)
                                                                        B
                                                                    @elseif($key==2)
                                                                        C
                                                                    @else
                                                                        D
                                                                    @endif</label>
                                                                    <p class="answ_tl">
                                                                        @if(!empty($value['options']))
                                                                            {!! ucfirst(@$value['options']) !!}
                                                                      
                                                                            <div class="col-md-7">
                                                                                <?php  
                                                                                    $image_url = 'javascript:;';
                                                                                    if (!empty($value['option_image'])) {
                                                                                        // dd($student_details->image);
                                                                                        if (file_exists(QuestionBankImageBasePath.'/'.$value['option_image'])) {
                                                                                            $image = QuestionBankImageImgPath.'/'.$value['option_image'];
                                                                                            $image_url = QuestionBankImageImgPath.'/'.$value['option_image'];
                                                                                        }else{
                                                                                            $image = '';
                                                                                        }
                                                                                    }else{
                                                                                        $image ='';
                                                                                    }
                                                                                ?>
                                                                                @if(!empty($image))
                                                                                    <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                                        <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="margin-bottom: 10px;">
                                                                                    </a>
                                                                                @endif
                                                                            </div>    
                                                                        @endif
                                                                    </p>
                                                                </div>
                                                            </div>  
                                                                  
                                                        @endforeach
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                <label class="strong">Correct Answer</label>
                                                                <p class="answ_tl">
                                                                    <strong>Option @if(@$question['correct_answer']=='1')
                                                                        A
                                                                    @elseif(@$question['correct_answer']=='2')
                                                                        B
                                                                    @elseif(@$question['correct_answer']=='3')
                                                                        C
                                                                    @elseif(@$question['correct_answer']=='4')
                                                                        D
                                                                    @endif
                                                                    </strong>- 
                                                                    @if(!empty(@$question['answer_explanation']))
                                                                        {!! ucfirst(@$question['answer_explanation']) !!}
                                                                    
                                                                        <?php
                                                                            $image_url = 'javascript:;';
                                                                            if (!empty($question['ans_image'])) {
                                                                                // dd($student_details->image);
                                                                                if (file_exists(QuestionBankImageBasePath.'/'.$question['ans_image'])) {
                                                                                    $image = QuestionBankImageImgPath.'/'.$question['ans_image'];
                                                                                    $image_url = QuestionBankImageImgPath.'/'.$question['ans_image'];
                                                                                }else{
                                                                                    $image = '';
                                                                                }
                                                                            }else{
                                                                                $image = '';
                                                                            }
                                                                        ?>
                                                                        @if(!empty($image))
                                                                            <a href="{{$image_url}}" target="_blank" class="thumbnail proof-thumb">
                                                                                <img src="{{$image}}" width="100%" height="100%" id="old_image" alt="No image" class="modal-upload-image-preview" style="float: right; margin-right: 80%">
                                                                            </a>
                                                                        @endif
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fields -->
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                @endif
                                <div class="pagntn">
                                    <ul class="pagination justify-content-end">
                                    
                                        {{ $paginate->appends(request()->input())->links() }}
                                     
                                    </ul> 
                                </div>
                            </div>
                        </div>

                    </div>
                   <!--  -->
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection    
@section('scripts')
    <script type="text/javascript">
        $(document).on('change', '#subject_id', function() {
            $("#unit_id").html('<option data-display="Select Unit" value="">Select Unit</unit_id>');
            $('#search_form_id').submit();
            /*$('.loader').show();
            
                $.ajax({
                    type:"get",
                    url: "{{ url('/get/unit') }}"+"/"+subject_id,
                   
                    success:function(resp){
                        $('#unit_id').html(resp);
                        $('.loader').hide();
                    },
                });*/
         });   
 

    </script>
    <script type="text/javascript">
        $(document).on('change', '#unit_id', function() {
            $('#search_form_id').submit();
            
         });   
    

    </script>
@endsection