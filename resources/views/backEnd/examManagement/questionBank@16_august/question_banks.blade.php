@extends('backEnd.layouts.master')
@section('title','Question Bank')
@section('content')

<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Exam Management</h4>
                <!-- <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Library</li>
                        </ol>
                    </nav>
                </div> -->
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Question Bank</h5>

                        <div class="add_btn">
                            <div class="btn-group pull-right">
                              
                                <a href="{{url('admin/question-bank/question/import')}}" class="btn btn-primary" type="excel">Import Questions</a>
                               
                            </div>
                            <a href="{{ url('admin/question-bank/question/add') }}" class="btn btn-primary">Add Questions</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Sub Category Name</th>
                                       
                                        <th width="13%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($question_banks))
                                        @foreach($question_banks as $key=>$value)
                                            <tr>
                                                <td>{{ ucfirst($value['category_name']) }}</td>
                                                <td>{{ ucfirst($value['sub_category_name']) }}</td>
                                                <td>
                                                    <a href="{{ url('/admin/question-banks/'.$value['id']) }}" title="Questions">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                 
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection