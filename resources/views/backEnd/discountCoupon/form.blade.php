<?php 
    if(isset($discount_coupon_id)){

        $task    = 'Edit';
    }else{
        $task    = 'Add';
    }
?>
@extends('backEnd.layouts.master')
@section('title',$task.' Discount Coupon')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">{{ $task }} Discount Coupon</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/discount-coupons') }}">Discount Coupon</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $task }} Discount Coupon</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="faq_form">
                        <div class="card-body">
                            <h4 class="card-title">{{$task}} Discount Coupon</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Coupon Code:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname" placeholder="Coupon Code" name="coupon_code" value="{{ isset($details['coupon_code'])? $details['coupon_code']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Discount Type:</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="discount_type" id="discount_type">
                                        <option value="">Discount Type</option>
                                        <option value="V" <?php if(@$details['discount_type']=='V'){echo'selected';}?>>Value</option>
                                        <option value="P" <?php if(@$details['discount_type']=='P'){echo'selected';}?>>Percent</option>
                                    </select>
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Discount Amount:</label>
                                <div class="col-sm-7">
                                    <span class="error1" id="discount_amount_error"></span>
                                    <input type="text" class="form-control" id="discount_amount" placeholder="Discount Amount" name="discount_amount" value="{{ isset($details['discount_amount'])? $details['discount_amount']: '' }}" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Uses Limit:</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="fname22" placeholder="Use per user limit" name="uses_limit" value="{{ isset($details['uses_per_user_limit'])? $details['uses_per_user_limit']: '' }}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Start Date:</label>
                                <div class="col-sm-7">
                                    <input type="date" class="form-control" id="start_date" placeholder="Start Date" name="start_date" value="{{isset($details->start_date)?$details->start_date: ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">End Date:</label>
                                <div class="col-sm-7">
                                    <span class="error1" id="end_date_err"></span>
                                    <input type="date" class="form-control" id="end_date" placeholder="End Date" name="end_date" value="{{isset($details->end_date)?$details->end_date: ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Status:</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="status">
                                        <option value="">Select Status</option>
                                        <option value="A" <?php if(@$details['status']=='A'){echo'selected';}?>>Active</option>
                                        <option value="I" <?php if(@$details['status']=='I'){echo'selected';}?>>Inactive</option>
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="discount_coupon_id" id="discount_coupon_id" value="{{ @$discount_coupon_id }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script>
    var form = $('#faq_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        rules:{
            "coupon_code":{
                required:true,
                minlength:2,
                maxlength:10,
                regex:/^[a-zA-z0-9]+$/,
                remote:{
                    type:"GET",
                    url:"{{url('admin/coupon_code/validate')}}",
                    data:{
                        discount_coupon_id:function(){
                           return $('#discount_coupon_id').val();
                        },
                    },
                }, 
            },
            "discount_amount":{
                required:true,
                min:1,
                number:true
            },
            "uses_limit":{
                required:true,
                number:true,
                min:1,
                maxlength:3
            },
            "discount_type":{
                required:true
            },
            "start_date":{
                required:true
            },
            "end_date":{
                required:true,
            },
            "status":{
                required:true
            },
        },
        messages:{
            "coupon_code":{
                remote:'This coupon code already exists.'
            },
        },
        submitHandler:function(form){

            var discount_type  = $('#discount_type').val();
            var discount_amount= $('#discount_amount').val();
            
            if(discount_type=='P'){
                if(parseInt(discount_amount)>=100){
                    $('#discount_amount_error').text('Discount amount must be less then 100%');
                    return false;
                }
            }
            form.submit();
        },
    });
</script>
<script type="text/javascript">
    $(document).on('input',function(){
        var start_date  = $('#start_date').val();
        var end_date    = $('#end_date').val();
        // alert(end_date);
        if((end_date=='')||(Date.parse(end_date)>Date.parse(start_date))||(start_date=='')){
            // alert('dsf');
            $(":submit").removeAttr("disabled");
            $('#end_date_err').text('');
        }
        /*else if(Date.parse(end_date)>=Date.parse(start_date)){
                var error = 0;
                ('#end_date_err').text('');
                // alert('sdfs');
        }*/else{
            var error = 1;
            $('#end_date_err').text('*End date must be greater then start date');
             $(":submit").attr("disabled", true);
        }
    });
</script>
@endsection
@endsection    