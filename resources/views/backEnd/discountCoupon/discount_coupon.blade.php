
@extends('backEnd.layouts.master')
@section('title','Send Discount Coupon')
@section('content')

<script src="{{url( backEndJsPath.'/multiple-select.min.js')}}"></script>
<script src="{{url( backEndJsPath.'/multiple-select.js')}}"></script>
<script type="text/javascript" src="{{ asset('/public/js/ckeditor/ckeditor.js') }}"></script>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Send Discount Coupon</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('admin/discount-coupons') }}">Discount Coupon</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Send Discount Coupon</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" method="post" action="" id="faq_form">
                        <div class="card-body">
                            <h4 class="card-title">Send Discount Coupon</h4>
                           
                                <!-- <span id="email_error" class="err"></span> -->
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Email:</label>
                                <div class="col-sm-7">
                                    <select class="form-control email" name="email[]" id="email" multiple="" style="width: 588px">
                                        @foreach($details as $detail)
                                            <option value="{{ $detail['email']}}" >{{ $detail['email'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description:</label>
                                <div class="col-sm-7">
                                    <textarea class="form-control ckeditor" rows="5" id="description" name="description">
                                        @if($discount_coupon->discount_type=='V')
                                           
                                            Congratulations you get a discount coupon of worth {{CURRENCY}}{{$discount_coupon->discount_amount}}. To avail this use coupon code {{$discount_coupon->coupon_code}}.

                                        @else
                                          
                                            Congratulations you get a discount of {{$discount_coupon->discount_amount}}%. To avail this use coupon code {{$discount_coupon->coupon_code}}.
                                              
                                        @endif      
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="discount_coupon_id" id="discount_coupon_id" value="{{ @$discount_coupon_id }}">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 20px">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@section('scripts')
<script type="text/javascript">
    var form = $('#faq_form');
    form.validate({
        errorPlacement: function errorPlacement(error, element) { element.before(error); },
        ignore:[],
        rules:{
            "email[]":{
                required:true,
            },
            description:{
                required: function(){
                   CKEDITOR.instances.description.updateElement();
                },
                minlength:2,
                maxlength:5000,
            },
        },
        
        submitHandler:function(form){

            var discount_type  = $('#discount_type').val();
            var discount_amount= $('#discount_amount').val();
            
            if(discount_type=='P'){
                if(parseInt(discount_amount)>=100){
                    $('#discount_amount_error').text('Discount amount must be less then 100%');
                    return false;
                }
            }
            form.submit();
        },
    });
</script>
<script type="text/javascript">

    $(document).ready(function() {
        $('.email').multipleSelect();
     }); 
     CKEDITOR.replace( 'description');  
</script>
@endsection
@endsection    