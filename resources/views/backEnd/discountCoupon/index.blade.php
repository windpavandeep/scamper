@extends('backEnd.layouts.master')
@section('title','Discount Coupons')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Discount Coupon</h4>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Discount Coupons</h5>
                        <div class="add_btn">
                            <a href="{{ url('admin/discount-coupon/add') }}" class="btn btn-primary">Add Discount Coupon</a>
                        </div>
                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Coupon Code</th>
                                        <th>Discount</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th width="10%">Status</th>
                                        <th width="15%"> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty(@$detail))
                                        @foreach($detail as $key=>$value)
                                            <tr>
                                                <td>{{ $value['coupon_code'] }}</td>
                                                <td>
                                                    @if($value['discount_type']=='P')
                                                        {{$value['discount_amount']}}%
                                                    @else
                                                        {{CURRENCY}}{{$value['discount_amount']}}
                                                    @endif
                                                </td>
                                                <td>{{ date('d-m-Y', strtotime($value['start_date'])) }}</td>
                                                <td>{{ date('d-m-Y', strtotime($value['end_date'])) }}</td>

                                                <td>
                                                    @if($value['status']=='A')
                                                        Active
                                                    @else
                                                        Inactive
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ url('/admin/discount-coupon/edit/'.$value['id']) }}" title="Edit"><i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="{{ url('/admin/discount-coupon/delete/'.$value['id']) }}" title="Delete" class="del_btn"><i class="fa fa-trash"></i>
                                                    </a>
                                                    <a href="{{url('admin/discount-coupon/mail/'.$value['id'])}}" title="Send Mail">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach    
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backEnd.common.footer')
</div>
@endsection