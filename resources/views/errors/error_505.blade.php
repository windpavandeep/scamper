<!DOCTYPE html>
<html>
<head>
    <title>Scamper Skills</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" type="text/css" href="{{ url(systemImgPath.'/fav.png') }}">
    <!-- <link rel="icon" href="img/fav.png" type="image/x-icon"> -->

    <link rel="stylesheet" type="text/css" href="{{ url('public/frontEnd/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/frontEnd/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/frontEnd/css/err_style.css') }}">
</head>
<body>
    <section class="error-main">
        <div class="error-table">
            <div class="error-table-cell">
                <div class="error-mini">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="error-cntnt">
                            <h1>505</h1>
                            <h3>Something went wrong. Please try later after sometime.</h3>
                            <!-- <p></p> -->
                            <div class="error-btns">
                                <a href="{{ url('/') }}" class="btn home-btn"> Home Page</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>
</html>