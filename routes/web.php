<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::any('/sent/student/exam/notifications','frontEnd\CronJobController@exam_notification');
Route::any('/exam/notification','frontEnd\CronJobController@exam_notification');
Route::match(['get','post'],'/pending/test','backEnd\paymentManagement\PaymentController@test');
Route::get('/','frontEnd\HomeController@index');
Route::get('/about-us','frontEnd\cms\CmsController@about_us');
Route::get('/news-events','frontEnd\cms\CmsController@news_events');
Route::get('/sitemaps','frontEnd\HomeController@site_map');
Route::get('/disclaimer','frontEnd\cms\CmsController@disclaimer');
Route::get('/faqs','frontEnd\cms\CmsController@faqs');
Route::get('/downloads','frontEnd\HomeController@downloads');
Route::get('/privacy-policy','frontEnd\cms\CmsController@privacy_policy');
Route::get('/term-conditions','frontEnd\cms\CmsController@terms_conditions');
Route::match(['get','post'],'trainer/complete-signup','frontEnd\AuthController@trainer_signup');
Route::any('set-password/{user_id}/{security_code}','frontEnd\AuthController@view');
Route::match(['get','post'],'/forgot-password','frontEnd\AuthController@forgot_password');
Route::any('/set-password','frontEnd\AuthController@set_Password');
Route::match(['get','post'],'counselling/request','frontEnd\HomeController@free_counselling_resquest');
// Route::match(['get','post'],'trainer/register','frontEnd\AuthController@verify_contact');

Route::match(['get','post'],'/test/send/otp','frontEnd\AuthController@test_otp');
// login/signup start
Route::match(['get','post'],'/register/{user_type}','frontEnd\AuthController@verify_contact');
Route::match(['get','post'],'send/otp','frontEnd\AuthController@send_otp');
Route::match(['get','post'],'verify/otp','frontEnd\AuthController@verify_otp');
Route::match(['get','post'],'destroy/otp','frontEnd\AuthController@destroy_otp');
Route::match(['get','post'],'login','frontEnd\AuthController@login');
Route::match(['get','post'],'/user/course/pay','frontEnd\payment\PurchaseCourseController@purchase_course');
Route::match(['get','post'],'user/complete-signup','frontEnd\AuthController@user_signup');
Route::get('/get/states/{country_id}','frontEnd\AuthController@get_states');
Route::get('/get/cities/{state_id}','frontEnd\AuthController@get_cities');
Route::get('validate/contact/{contact}','frontEnd\AuthController@validate_contact');
Route::get('validate/email','frontEnd\AuthController@validate_email');
Route::get('/logout','frontEnd\AuthController@logout');
Route::get('get/sub-categories/{category_id}','frontEnd\trainer\contentManagement\ContentController@get_subcategory');
Route::get('get/sub-subcategories/{sub_category_id}','frontEnd\trainer\contentManagement\ContentController@get_subsubcategory');

Route::get('/courses','frontEnd\HomeController@courses');
Route::get('/course/detail/{course_id}','frontEnd\HomeController@course_detail');
Route::match(['get','post'],'/become-retailer','frontEnd\HomeController@become_retailer');
Route::match(['get','post'],'/contact-us','frontEnd\HomeController@contact_us');

 
Route::group(['prefix'=>'student', 'middleware'=>'CheckStudentAuth'],function(){

	Route::match(['get','post'],'prep-exam-result','frontEnd\user\PrepExamController@prepExamResult');

	Route::match(['get','post'],'dashboard','frontEnd\user\DashboardController@dashboard');
	Route::match(['get','post'],'profile','frontEnd\user\ProfileController@index');
	Route::match(['get','post'],'profile/edit','frontEnd\user\ProfileController@edit');
	Route::match(['get','post'],'change-password','frontEnd\user\ProfileController@change_password');
	Route::match(['get','post'],'subscription-history','frontEnd\user\SubscriptionHistoryController@index');
	Route::match(['get','post'],'purchased-contents','frontEnd\user\ContentController@index');
	Route::match(['get','post'],'validate/discount-coupon','frontEnd\user\OrderController@verify_coupon_code');

	Route::match(['get','post'],'/pincode/{postal_code}','frontEnd\payment\PurchaseCourseController@set_pincodee_session');

	Route::match(['get','post'],'/order/detail/{course_id}','frontEnd\user\OrderController@index');
	Route::match(['get','post'],'/course/pay','frontEnd\user\OrderController@purchase_course');


	Route::match(['get','post'],'prep-exam','frontEnd\user\PrepExamController@index');
	Route::match(['get','post'],'prep-exam-instructions','frontEnd\user\PrepExamController@prepExamInstructions');
	Route::match(['get','post'],'exam-ques/{student_id}/{unit_id}/{totaltime}/{sub_subcategory_id}','frontEnd\user\PrepExamController@examQues');
	Route::match(['get','post'],'prep-test-submit','frontEnd\user\PrepExamController@prepTestSubmit');
	Route::match(['get','post'],'save-next-prep-test','frontEnd\user\PrepExamController@saveNextPreptest');
	Route::match(['get','post'],'mark-for-review-prep-test','frontEnd\user\PrepExamController@markForreviewPreptest');
	Route::match(['get','post'],'back-to-question','frontEnd\user\PrepExamController@backToQuestion');
	Route::match(['get','post'],'prep-test-review','frontEnd\user\PrepExamController@prepTestReview');
	Route::match(['get','post'],'go_to_next_prep_review_question','frontEnd\user\PrepExamController@goToNextPrepReviewQuestion');
	Route::match(['get','post'],'go_to_prep_review_question','frontEnd\user\PrepExamController@goToPrepReviewQuestion');
	Route::match(['get','post'],'go_to_rigth_side_review_question','frontEnd\user\PrepExamController@goToRigthSideReviewQuestion');

	Route::match(['get','post'],'live-lectures','frontEnd\user\DashboardController@liveLectures');
	Route::match(['get','post'],'all-videos/{active_id}','frontEnd\user\DashboardController@allVideos');
	Route::match(['get','post'],'related-notes/{active_id}','frontEnd\user\DashboardController@relatedNotes');
	Route::match(['get','post'],'solved-exams','frontEnd\user\DashboardController@solvedExams');

	Route::match(['get','post'],'exam','frontEnd\user\ExamController@index');

	Route::match(['get','post'],'buy_courses','frontEnd\user\BuyCourseController@index');

	Route::match(['get','post'],'chat_with_teacher','frontEnd\user\ChatWithTeacherController@index');

	Route::match(['get','post'],'notes','frontEnd\user\NotesController@index');

	Route::match(['get','post'],'downloads','frontEnd\user\DownloadsController@index');

	Route::match(['get','post'],'free_counselling','frontEnd\user\FreeCounsellingController@index');

	Route::match(['get','post'],'get_subject_by_Condtion','frontEnd\user\PrepExamController@getSubjectByCondtion');

	Route::match(['get','post'],'go_to_next_question','frontEnd\user\PrepExamController@goToNextQuestion');

	Route::match(['get','post'],'go_to_prevoius_question','frontEnd\user\PrepExamController@goToPrevoiusQuestion');

	Route::match(['get','post'],'exam_progress_report','frontEnd\user\ExamProgressController@index');

	Route::match(['get','post'],'tutorials_exams','frontEnd\user\TutorialsExamController@index');

	Route::match(['get','post'],'trainers','frontEnd\user\TrainersController@index');

	Route::match(['get','post'],'live_lectures','frontEnd\user\LiveLecturesController@index');

	Route::match(['get','post'],'term_condition','frontEnd\user\TermConditionController@index');

	Route::match(['get','post'],'notification','frontEnd\user\NotificationController@index');

	Route::match(['get','post'],'get_subject_by_course','frontEnd\user\DashboardController@getSubjectByCourse');

	Route::match(['get','post'],'get/unit_video/{subject_id}','frontEnd\user\DashboardController@unitVideo');

	Route::match(['get','post'],'get/subject_unit_video/{unit_id}','frontEnd\user\DashboardController@subjectunitVideo');

	Route::match(['get','post'],'get/view_unit_video/{subject_id}','frontEnd\user\DashboardController@viewunitVideo');

	Route::match(['get','post'],'get/view_subject_unit_video/{subject_id}','frontEnd\user\DashboardController@viewSubjectUnitVideo');

	Route::match(['get','post'],'get/view_video_by_title/{subject_id}','frontEnd\user\DashboardController@viewVideoByTitle');

	Route::match(['get','post'],'get/view_all_video','frontEnd\user\DashboardController@viewAllVideo');

	Route::match(['get','post'],'get/view_all_video_active_course/{active_id}','frontEnd\user\DashboardController@viewAllVideoActiveCourse');


	/**************************  Notes route  ************************/
	Route::match(['get','post'],'get/unit_notes/{subject_id}','frontEnd\user\DashboardController@unitNotes');

	Route::match(['get','post'],'get/subject_unit_notes/{unit_id}','frontEnd\user\DashboardController@subjectunitNotes');

	Route::match(['get','post'],'get/view_unit_notes/{subject_id}','frontEnd\user\DashboardController@viewunitNotes');

	Route::match(['get','post'],'get/view_subject_unit_notes/{subject_id}','frontEnd\user\DashboardController@viewSubjectUnitNotes');

	Route::match(['get','post'],'get/view_notes_by_title/{title}/{active_id}','frontEnd\user\DashboardController@viewNotesByTitle');

	Route::match(['get','post'],'get/view_all_notes_active_course/{active_id}','frontEnd\user\DashboardController@viewAllNotesActiveCourse');
	Route::match(['get','post'],'get/graph_analysis/{subject_id}/{active_course_id}','frontEnd\user\PrepExamController@graphAnalysis');
	Route::match(['get','post'],'video_lectures','frontEnd\user\DashboardController@videoLectures');
	Route::match(['get','post'],'get_video_lectures_by_course','frontEnd\user\DashboardController@getVideoLecturesByCourse');
	
	Route::match(['get','post'],'study_materials','frontEnd\user\DashboardController@studyMaterials');
	
	Route::match(['get','post'],'get_study_materials_by_course','frontEnd\user\DashboardController@getStudyMaterialsByCourse');
	Route::match(['get','post'],'expert_opinions','frontEnd\user\DashboardController@expertOpinions');
	Route::match(['get','post'],'grievance_cell','frontEnd\user\DashboardController@grievanceCell');
	Route::match(['get','post'],'my_subscriptions','frontEnd\user\DashboardController@mySubscriptions');
	Route::match(['get','post'],'submit_feedback','frontEnd\user\DashboardController@submitFeedback');
	Route::match(['get','post'],'privacy_policy','frontEnd\user\DashboardController@privacyPolicy');
	Route::match(['get','post'],'trainer_content_views/{trainer_content_file_id}','frontEnd\user\DashboardController@trainerContentViews');

});
// login/signup end
Route::match(['get','post'],'become-trainer','frontEnd\HomeController@become_trainer');
Route::group(['prefix'=>'trainer', 'middleware'=>'CheckTrainerAuth'],function(){
	Route::match(['get','post'],'dashboard','frontEnd\trainer\DashboardController@index');
	Route::match(['get','post'],'profile','frontEnd\trainer\ProfileController@index');
	Route::match(['get','post'],'profile/edit','frontEnd\trainer\ProfileController@edit');
	Route::match(['get','post'],'change-password','frontEnd\trainer\ProfileController@change_password');
	Route::match(['get','post'],'contents','frontEnd\trainer\contentManagement\ContentController@index');
	Route::match(['get','post'],'content/add','frontEnd\trainer\contentManagement\ContentController@add');
	Route::match(['get','post'],'content/delete/{content_id}','frontEnd\trainer\contentManagement\ContentController@delete');
	Route::match(['get','post'],'get/content/data','frontEnd\trainer\contentManagement\ContentController@render_contents');

	Route::match(['get','post'],'chats','frontEnd\trainer\chatManagement\ChatController@index');
	Route::match(['get','post'],'subscriptions','frontEnd\trainer\subscriptions\SubscriptionController@index');
	Route::match(['get','post'],'subscription/add','frontEnd\trainer\subscriptions\SubscriptionController@add');
	Route::match(['get','post'],'subscription/edit/{subscription_plan_id}','frontEnd\trainer\subscriptions\SubscriptionController@edit');
	Route::match(['get','post'],'subscription/delete/{subscription_plan_id}','frontEnd\trainer\subscriptions\SubscriptionController@delete');

	Route::match(['get','post'],'earnings','frontEnd\trainer\earnings\EarningController@index');
});
//Admin
Route::match(['get','post'],'get/unit/{subject_id}','backEnd\examManagement\QuestionBankController@get_units');
Route::match(['get','post'],'admin/login','backEnd\AuthController@login');
Route::match(['get','post'],'admin','backEnd\AuthController@login');
Route::match(['get','post'],'admin/forgot-password','backEnd\AuthController@forgot_password');
Route::match(['get','post'],'admin/set-password/{admin_id}/{security_code}','backEnd\AuthController@set_password');
Route::match(['get','post'],'get/units/{subject_id}','backEnd\courseManagement\CourseController@get_units');
Route::match(['get','post'],'get/subjects/{sub_sub_category_id}','backEnd\courseManagement\CourseController@get_subjects');
Route::group(['prefix'=>'admin', 'middleware'=>'CheckAdminAuth'],function(){

	Route::get('/logout','backEnd\AuthController@logout');
	Route::get('dashboard','backEnd\DashboadController@index');
	Route::match(['get','post'],'profile','backEnd\ProfileController@index');
	Route::match(['get','post'],'change-password','backEnd\ProfileController@change_password');


	//Trainer Management
	Route::match(['get','post'],'domains','backEnd\trainerManagement\DomainController@index');
	Route::match(['get','post'],'domain/add','backEnd\trainerManagement\DomainController@add');
	Route::match(['get','post'],'domain/edit/{domain_id}','backEnd\trainerManagement\DomainController@edit');
	Route::match(['get','post'],'domain/delete/{domain_id}','backEnd\trainerManagement\DomainController@delete');	
	Route::match(['get','post'],'validate/domain/name/{domain_id}','backEnd\trainerManagement\DomainController@validate_name');
	// New Signup Requests
	Route::match(['get','post'],'trainer/signup-requests','backEnd\trainerManagement\NewRequestController@index');
	Route::match(['get','post'],'trainer/signup-request/view/{trainer_id}','backEnd\trainerManagement\NewRequestController@view_details');
	//End

	//Trainer
	Route::match(['get','post'],'trainers','backEnd\trainerManagement\TrainerController@index');
	Route::match(['get','post'],'trainer/add','backEnd\trainerManagement\TrainerController@add');
	Route::match(['get','post'],'trainer/edit/{trainer_id}','backEnd\trainerManagement\TrainerController@edit');
	Route::match(['get','post'],'trainer/delete/{trainer_id}','backEnd\trainerManagement\TrainerController@delete');
	Route::match(['get','post'],'trainer/credential/mail/{trainer_id}','backEnd\trainerManagement\TrainerController@send_credentials_mail');
	Route::match(['get','post'],'/validate/trainer/email','backEnd\trainerManagement\TrainerController@validate_email');
	Route::match(['get','post'],'/validate/trainer/contact','backEnd\trainerManagement\TrainerController@validate_contact');
	Route::match(['get','post'],'trainer/export','backEnd\trainerManagement\TrainerController@export');
	//End

	//End Trainer Management
	// Subscription Plan Request
	Route::match(['get','post'],'trainer/subscription-plan/requests','backEnd\trainerManagement\planRequestController@index');
	Route::match(['get','post'],'trainer/subscription-plan/request/view/{subscription_plan_id}','backEnd\trainerManagement\planRequestController@view_details');

	//End
	//Category Management 
	Route::match(['get','post'],'categories','backEnd\categoryManagement\CategoryController@index');
	Route::match(['get','post'],'category/add','backEnd\categoryManagement\CategoryController@add');
	Route::match(['get','post'],'category/edit/{category_id}','backEnd\categoryManagement\CategoryController@edit');	
	Route::match(['get','post'],'category/delete/{category_id}','backEnd\categoryManagement\CategoryController@delete');
	Route::match(['get','post'],'validate/domain/name','backEnd\trainerManagement\DomainController@validate_name');
	//End 

	//Sub Category
	Route::match(['get','post'],'subcategories/{category_id}','backEnd\categoryManagement\SubCategoryController@index');
	Route::match(['get','post'],'subcategory/add/{category_id}','backEnd\categoryManagement\SubCategoryController@add');
	Route::match(['get','post'],'subcategory/edit/{sub_category_id}','backEnd\categoryManagement\SubCategoryController@edit');	
	Route::match(['get','post'],'subcategory/delete/{sub_category_id}','backEnd\categoryManagement\SubCategoryController@delete');
	Route::match(['get','post'],'validate/subcategory/name','backEnd\categoryManagement\SubCategoryController@validate_name');

	//End
	//Sub SubCategory
	Route::match(['get','post'],'sub-subcategories/{sub_category_id}','backEnd\categoryManagement\SubSubCategoryController@index');
	Route::match(['get','post'],'sub-subcategory/add/{sub_category_id}','backEnd\categoryManagement\SubSubCategoryController@add');
	Route::match(['get','post'],'sub-subcategory/edit/{sub_sub_category_id}','backEnd\categoryManagement\SubSubCategoryController@edit');	
	Route::match(['get','post'],'sub-subcategory/delete/{sub_sub_category_id}','backEnd\categoryManagement\SubSubCategoryController@delete');
	Route::match(['get','post'],'validate/sub-subcategory/name','backEnd\categoryManagement\SubSubCategoryController@validate_name');

	//End


	//Franchise
	Route::match(['get','post'],'/retailers','backEnd\franchise\FranchiseController@index');
	Route::match(['get','post'],'/retailer/detail/{franchise_id}','backEnd\franchise\FranchiseController@detail');
	//End

	//Contact Us
	Route::match(['get','post'],'/contact-us','backEnd\contactUs\ContactUsController@index');
	Route::match(['get','post'],'/contact-us/detail/{contact_us_id}','backEnd\contactUs\ContactUsController@detail');
	
	//End

	Route::match(['get','post'],'students','backEnd\studentManagement\StudentController@index');
	Route::match(['get','post'],'student/add','backEnd\studentManagement\StudentController@add');
	Route::match(['get','post'],'student/edit/{student_id}','backEnd\studentManagement\StudentController@edit');
	Route::match(['get','post'],'student/delete/{student_id}','backEnd\studentManagement\StudentController@delete');
	Route::match(['get','post'],'student/credential/mail/{student_id}','backEnd\studentManagement\StudentController@send_credentials_mail');
	Route::match(['get','post'],'/validate/student/email','backEnd\studentManagement\StudentController@validate_email');
	Route::match(['get','post'],'/validate/student/contact','backEnd\studentManagement\StudentController@validate_contact');
	Route::match(['get','post'],'student/export','backEnd\studentManagement\StudentController@export');

	Route::match(['get','post'],'student/subscriptions/{student_id}','backEnd\studentManagement\StudentController@subscriptions');

	// Course Management
	Route::match(['get','post'],'courses','backEnd\courseManagement\CourseController@index');
	Route::match(['get','post'],'course/add','backEnd\courseManagement\CourseController@add');
	Route::match(['get','post'],'course/edit/{course_id}','backEnd\courseManagement\CourseController@edit');
	Route::match(['get','post'],'course/delete/{course_id}','backEnd\courseManagement\CourseController@delete');
	Route::match(['get','post'],'subject/content/{content_count}/{sub_sub_category_id}','backEnd\courseManagement\CourseController@append_subject_content');
	Route::match(['get','post'],'/course/image/delete/{course_image_id}','backEnd\courseManagement\CourseController@course_image_delete');
	Route::match(['get','post'],'course/file/delete/{course_file_id}','backEnd\courseManagement\CourseController@course_file_delete');
	//End
	// Payment Management

	Route::match(['get','post'],'/pending/payments','backEnd\paymentManagement\PaymentController@index');
	Route::match(['get','post'],'/pending/payment/detail/{transaction_id}','backEnd\paymentManagement\PaymentController@payment_details');
	Route::match(['get','post'],'pending/payment/pay','backEnd\paymentManagement\PaymentController@pay_pending_payment');
	Route::match(['get','post'],'/paid/payments','backEnd\paymentManagement\PaymentController@paid_payment');

	Route::match(['get','post'],'/earnings','backEnd\paymentManagement\PaymentController@admin_earnings');
	Route::match(['get','post'],'earning/export','backEnd\paymentManagement\PaymentController@export_earnings');

	//End

	//Home Page Content
	//First Section
	Route::match(['get','post'],'/home/page/first/section','backEnd\homePageContent\HomePageController@index');

	//Second Section
	Route::match(['get','post'],'/home/page/second/section','backEnd\homePageContent\HomePageController@second_section');
	Route::match(['get','post'],'/home/page/second/section/edit/{content_id}','backEnd\homePageContent\HomePageController@second_section_edit');

	//Third Section
	Route::match(['get','post'],'/home/page/third/section','backEnd\homePageContent\HomePageController@third_section');
	Route::match(['get','post'],'/home/page/third/section/edit/{content_id}','backEnd\homePageContent\HomePageController@third_section_edit');

	//Content Management
	Route::match(['get','post'],'/header/content','backEnd\contentManagement\ContentController@header_content');
	Route::match(['get','post'],'/footer/content','backEnd\contentManagement\ContentController@footer_content');

	Route::match(['get','post'],'/advertisement/pop-ups','backEnd\contentManagement\PopContentController@index');
	Route::match(['get','post'],'advertisement/pop-up/add','backEnd\contentManagement\PopContentController@add');
	Route::match(['get','post'],'advertisement/pop-up/edit/{pop_id}','backEnd\contentManagement\PopContentController@edit');
	Route::match(['get','post'],'validate/pop-up/status','backEnd\contentManagement\PopContentController@validate_pop_status');
	Route::match(['get','post'],'advertisement/pop-up/delete/{pop_id}','backEnd\contentManagement\PopContentController@delete');
	//Faq
	Route::match(['get','post'],'faqs','backEnd\contentManagement\FaqController@index');
	Route::match(['get','post'],'faq/add','backEnd\contentManagement\FaqController@add');
	Route::match(['get','post'],'faq/edit/{faq_id}','backEnd\contentManagement\FaqController@edit');
	Route::match(['get','post'],'faq/delete/{faq_id}','backEnd\contentManagement\FaqController@delete');
	//End

	//About Us	
	Route::match(['get','post'],'about-us','backEnd\contentManagement\AboutUsController@index');
	Route::match(['get','post'],'about-us/section1/edit','backEnd\contentManagement\AboutUsController@edit_section1');	
	Route::match(['get','post'],'about-us/section2','backEnd\contentManagement\AboutUsController@section2_index');
	//Privacy Terms
	Route::match(['get','post'],'privacy-policy/edit','backEnd\contentManagement\ContentController@edit_privacy_terms');
	//
	//Terms & conditions
	Route::match(['get','post'],'term-condition/edit','backEnd\contentManagement\ContentController@edit_terms_conditions');
	//

	//Disclaimer Edit
	Route::match(['get','post'],'disclaimer/edit','backEnd\contentManagement\ContentController@edit_disclaimer');
	//
	Route::match(['get','post'],'about-us/section2/add','backEnd\contentManagement\AboutUsController@add_section2');
	Route::match(['get','post'],'about-us/section2/edit/{section2_id}','backEnd\contentManagement\AboutUsController@edit_section2');
	Route::match(['get','post'],'about-us/section2/delete/{section2_id}','backEnd\contentManagement\AboutUsController@delete_section2');

	Route::match(['get','post'],'about-us/our-partners','backEnd\contentManagement\PartnerController@index');
	Route::match(['get','post'],'about-us/our-partner/add','backEnd\contentManagement\PartnerController@add');
	Route::match(['get','post'],'about-us/our-partner/edit/{partner_id}','backEnd\contentManagement\PartnerController@edit');
	Route::match(['get','post'],'about-us/our-partner/delete/{partner_id}','backEnd\contentManagement\PartnerController@delete');
	
	Route::match(['get','post'],'success-stories','backEnd\contentManagement\StoryController@index');
	Route::match(['get','post'],'success-story/add','backEnd\contentManagement\StoryController@add');
	Route::match(['get','post'],'success-story/edit/{sucess_story_id}','backEnd\contentManagement\StoryController@edit');
	Route::match(['get','post'],'success-story/delete/{sucess_story_id}','backEnd\contentManagement\StoryController@delete');


	Route::match(['get','post'],'our-faculties','backEnd\contentManagement\OurFacultyController@index');
	Route::match(['get','post'],'our-faculty/add','backEnd\contentManagement\OurFacultyController@add');
	Route::match(['get','post'],'our-faculty/edit/{our_faculty_id}','backEnd\contentManagement\OurFacultyController@edit');
	Route::match(['get','post'],'our-faculty/delete/{our_faculty_id}','backEnd\contentManagement\OurFacultyController@delete');

	Route::match(['get','post'],'our-faculty-banner-image/edit','backEnd\contentManagement\OurFacultyController@banner_image_edit');
	
	//End

	//Exam Management
	Route::match(['get','post'],'/exams','backEnd\examManagement\ExamController@index');
	Route::match(['get','post'],'/exam/add','backEnd\examManagement\ExamController@add');
	Route::match(['get','post'],'/exam/edit/{exam_id}','backEnd\examManagement\ExamController@edit');
	Route::match(['get','post'],'/exam/delete/{exam_id}','backEnd\examManagement\ExamController@delete');
	Route::match(['get','post'],'/exam/question/{exam_id}','backEnd\examManagement\ExamController@add_question');
	Route::match(['get','post'],'/exam/question/edit/{question_id}/{exam_id}','backEnd\examManagement\ExamController@edit_question');
	Route::match(['get','post'],'exam/question/paper/{exam_id}','backEnd\examManagement\ExamController@question_paper');
	Route::match(['get','post'],'/exam/question/delete/{question_id}','backEnd\examManagement\ExamController@delete_question');
	Route::match(['get','post'],'/validate/add/questions','backEnd\examManagement\ExamController@validate_add_questions');
	Route::match(['get','post'],'/validate/edit/question/{question_id}','backEnd\examManagement\ExamController@validate_edit_question');
	Route::match(['get','post'],'/code/generate','backEnd\examManagement\ExamController@code_generate');
	Route::match(['get','post'],'/get/exam/name/{category_id}','backEnd\examManagement\ExamController@get_exam_name');
	Route::match(['get','post'],'/import/file/{exam_id}','backEnd\examManagement\ExamController@import_file');

	//End
	//Question Bank
    Route::match(['get','post'],'question-banks','backEnd\examManagement\QuestionBankController@question_bank');
	Route::match(['get','post'],'question-banks/{sub_category_id}','backEnd\examManagement\QuestionBankController@index');
	Route::match(['get','post'],'question-bank/question/add','backEnd\examManagement\QuestionBankController@add_question_bank');
	Route::match(['get','post'],'question-bank/question/edit/{question_id}','backEnd\examManagement\QuestionBankController@edit_question');

	Route::match(['get','post'],'/validate/edit/question-bank/{question_id}','backEnd\examManagement\QuestionBankController@validate_edit_question');
	Route::match(['get','post'],'/question-bank/question/delete/{question_id}','backEnd\examManagement\QuestionBankController@delete_question');
	Route::match(['get','post'],'/question-bank/question/import','backEnd\examManagement\QuestionBankController@import_file');


	//End

	//Language Management
	Route::match(['get','post'],'/language','backEnd\languageManagement\LanguageController@index');
	Route::match(['get','post'],'/language/add','backEnd\languageManagement\LanguageController@add');
	Route::match(['get','post'],'/language/delete/{language_id}','backEnd\languageManagement\LanguageController@delete');
	Route::match(['get','post'],'/language/edit/{language_id}','backEnd\languageManagement\LanguageController@edit');
	Route::match(['get','post'],'/check/language/name','backEnd\languageManagement\LanguageController@check_name');
	Route::match(['get','post'],'/check/edit/language/name/{language_id}','backEnd\languageManagement\LanguageController@check_edit_name');
	//

	//Become Parnter
	Route::match(['get','post'],'/become-trainers','backEnd\contentManagement\BecomeTrainerController@index');
	Route::match(['get','post'],'/become-trainer/section1/edit','backEnd\contentManagement\BecomeTrainerController@section1_edit');
		Route::match(['get','post'],'/become-trainer/section2','backEnd\contentManagement\BecomeTrainerController@section2_index');
	Route::match(['get','post'],'/become-trainer/section2/add','backEnd\contentManagement\BecomeTrainerController@add');
	Route::match(['get','post'],'/become-trainer/section2/edit/{become_trainer_id}','backEnd\contentManagement\BecomeTrainerController@edit');
	Route::match(['get','post'],'/become-trainer/section2/delete/{become_trainer_id}','backEnd\contentManagement\BecomeTrainerController@delete');
	//

	Route::match(['get','post'],'/retailer-image/edit','backEnd\contentManagement\ContentController@edit_retailer_image');

	// Counselling requests
	Route::match(['get','post'],'/counselling/requests','backEnd\counsellings\CounsellingController@index');
	Route::match(['get','post'],'/counselling/request/detail/{counselling_request_id}','backEnd\counsellings\CounsellingController@request_details');
	//end
	//News Events
	Route::match(['get','post'],'/news-events','backEnd\contentManagement\NewsEventController@index');
	Route::match(['get','post'],'/news-event/add','backEnd\contentManagement\NewsEventController@add');
	Route::match(['get','post'],'/news-event/edit/{news_id}','backEnd\contentManagement\NewsEventController@edit');
	Route::match(['get','post'],'/news-event/delete/{news_id}','backEnd\contentManagement\NewsEventController@delete');
	//end

	//Subject
	Route::match(['get','post'],'subjects','backEnd\categoryManagement\SubjectController@index');
	Route::match(['get','post'],'subject/add','backEnd\categoryManagement\SubjectController@add');
	Route::match(['get','post'],'subject/edit/{subject_id}','backEnd\categoryManagement\SubjectController@edit');
	Route::match(['get','post'],'subject/delete/{subject_id}','backEnd\categoryManagement\SubjectController@delete');
	Route::match(['get','post'],'validate/subject/name','backEnd\categoryManagement\SubjectController@validate_name');

	//

	//Units
	Route::match(['get','post'],'subject/unit/{subject_id}','backEnd\categoryManagement\UnitController@index');
	Route::match(['get','post'],'subject/unit/add/{subject_id}','backEnd\categoryManagement\UnitController@add');
	Route::match(['get','post'],'subject/unit/edit/{unit_id}','backEnd\categoryManagement\UnitController@edit');
	Route::match(['get','post'],'subject/unit/delete/{unit_id}','backEnd\categoryManagement\UnitController@delete');

	Route::match(['get','post'],'validate/unit/name','backEnd\categoryManagement\UnitController@validate_name');

	//
	// Edit Contact Us Contents
	Route::match(['get','post'],'/contact-us/content/edit','backEnd\contentManagement\ContentController@edit_contact_us_content');
	//End

	Route::match(['get','post'],'sets','backEnd\examManagement\SetController@index');
	Route::match(['get','post'],'set/add','backEnd\examManagement\SetController@add');
	Route::match(['get','post'],'set/edit/{set_id}','backEnd\examManagement\SetController@edit');	
	Route::match(['get','post'],'set/delete/{set_id}','backEnd\examManagement\SetController@delete');
	Route::match(['get','post'],'validate/set/name','backEnd\examManagement\SetController@validate_name');

	//Discount Coupon
	Route::match(['get','post'],'discount-coupons','backEnd\discountCoupon\DiscountCouponController@index');
	Route::match(['get','post'],'discount-coupon/add','backEnd\discountCoupon\DiscountCouponController@add');
	Route::match(['get','post'],'discount-coupon/edit/{set_id}','backEnd\discountCoupon\DiscountCouponController@edit');	
	Route::match(['get','post'],'discount-coupon/delete/{set_id}','backEnd\discountCoupon\DiscountCouponController@delete');
	Route::match(['get','post'],'/coupon_code/validate','backEnd\discountCoupon\DiscountCouponController@validate_coupon_code');
	Route::match(['get','post'],'discount-coupon/mail/{discount_coupon_id}','backEnd\discountCoupon\DiscountCouponController@discount_coupon_mail');
	//End
	//welcome sms edit
	Route::match(['get','post'],'welcome-sms/edit','backEnd\contentManagement\WelcomeSmsController@edit');
	//

	//Referrals
	Route::match(['get','post'],'referrals','backEnd\referCode\ReferCodeController@index');
	Route::match(['get','post'],'referral/add','backEnd\referCode\ReferCodeController@add');
	Route::match(['get','post'],'referral/edit/{referral_id}','backEnd\referCode\ReferCodeController@edit');
	Route::match(['get','post'],'referral/delete/{referral_id}','backEnd\referCode\ReferCodeController@delete');
	Route::match(['get','post'],'validate/coordinator-id','backEnd\referCode\ReferCodeController@validate_refer_id');

//App Slider Contents
	Route::match(['get','post'],'homepage-banners','backEnd\appContents\HomepageBannerController@index');
	Route::match(['get','post'],'homepage-banner/add','backEnd\appContents\HomepageBannerController@add');
	Route::match(['get','post'],'homepage-banner/edit/{banner_id}','backEnd\appContents\HomepageBannerController@edit');
	Route::match(['get','post'],'homepage-banner/delete/{banner_id}','backEnd\appContents\HomepageBannerController@delete');

	Route::match(['get','post'],'get/products','backEnd\appContents\HomepageBannerController@course_products');
	//End
	//latest roots
	Route::match(['get','post'],'/import/referrals','backEnd\referCode\ReferCodeController@import_referrals');
	Route::match(['get','post'],'validate/pincode','backEnd\referCode\ReferCodeController@validate_pincode');


	Route::match(['get','post'],'walkthroughs','backEnd\appContents\WalkthroughController@index');
	Route::match(['get','post'],'walkthrough/edit/{walkthrough_id}','backEnd\appContents\WalkthroughController@edit');
	//
	//
	//student exam result
	Route::match(['get','post'],'student/exam/{student_id}','backEnd\studentManagement\StudentExamController@index');
	Route::match(['get','post'],'student/analysis/report','backEnd\studentManagement\StudentExamController@export_student_report');
	Route::match(['get','post'],'student/exam/detail/{user_exam_id}','backEnd\studentManagement\StudentExamController@exam_details');
	Route::match(['get','post'],'/all/test/exams','backEnd\examManagement\allTestExams\ExamController@index');
	Route::match(['get','post'],'/all/exam/student/{exam_id}','backEnd\examManagement\allTestExams\ExamController@details');
	Route::match(['get','post'],'/exam/student/result/{exam_id}','backEnd\examManagement\allTestExams\ExamController@student_exam_detail');
	Route::match(['get','post'],'test/exam/report','backEnd\examManagement\allTestExams\ExamController@export_students_exam_report');

	//end	
	//Download Management
	Route::match(['get','post'],'downloads','backEnd\downloadManagement\DownloadController@index');
	Route::match(['get','post'],'download/add','backEnd\downloadManagement\DownloadController@add');
	Route::match(['get','post'],'download/edit/{download_content_id}','backEnd\downloadManagement\DownloadController@edit');
	Route::match(['get','post'],'download/delete/{download_content_id}','backEnd\downloadManagement\DownloadController@delete');
	//End

	//Sub Admin management
    Route::match(['get','post'], 'sub-admins','backEnd\superAdmin\SubAdminController@index');
    Route::match(['get','post'], 'sub-admin/add','backEnd\superAdmin\SubAdminController@add');
    Route::match(['get','post'], 'sub-admin/edit/{sub_admin_id}','backEnd\superAdmin\SubAdminController@edit');
    Route::get('sub-admin/delete/{sub_admin_id}','backEnd\superAdmin\SubAdminController@delete');
    Route::get('sub-admin/report/excel', 'backEnd\superAdmin\SubAdminController@excel_report');
    Route::get('sub-admin/report/csv','backEnd\superAdmin\SubAdminController@csv_report');
    Route::get('sub-admin/report/pdf','backEnd\superAdmin\SubAdminController@pdf_report');
    Route::match(['get','post'], 'sub-admin/send-credentials/{sub_admin_id}','backEnd\superAdmin\SubAdminController@send_credentials');
    Route::get('/validate/sub-admin/email','backEnd\superAdmin\SubAdminController@validate_email');
    Route::get('/validate/sub-admin/contact','backEnd\superAdmin\SubAdminController@validate_contact');
    //End
    //access rights
    Route::get('access-rights/{sub_admin_id}', 'backEnd\superAdmin\AccessRightController@index');
    Route::match(['get','post'], 'access-right/update', 'backEnd\superAdmin\AccessRightController@update');


    // Test 
    Route::match(['get','post'],'tests','backEnd\test\TestController@index');
    Route::match(['get','post'],'test/add','backEnd\test\TestController@add');
    Route::match(['get','post'],'test/edit/{acess_right_id}','backEnd\test\TestController@edit');
    Route::match(['get','post'],'test/delete/{acess_right_id}','backEnd\test\TestController@delete');
    //

    //Notification Management
    Route::match(['get','post'],'send/notifications','backEnd\notificationManagement\NotificationController@index');
    //End

    //report

    Route::match(['get','post'],'student/reports','backEnd\reportManagement\studentManagement\StudentController@index');
    
    Route::match(['get','post'],'teacher/reports','backEnd\reportManagement\trainerManagement\TrainerController@index');
    Route::match(['get','post'],'all/test/exam/reports','backEnd\reportManagement\allTestExams\ExamController@index');
    Route::match(['get','post'],'sales/reports','backEnd\reportManagement\paymentManagement\PaymentController@admin_earnings');
});


define('SITELINK','https://scamperskills.com/');

define('PROJECT_NAME','Scamper Skills');
define('frontEndCssPath','public/frontEnd/css');
define('frontEndJsPath','public/frontEnd/js');
define('frontEndStudentCssPath','public/frontEnd/css/student');
define('frontEndStudentJsPath','public/frontEnd/js/student');
define('backEndCssPath','public/backEnd/css');
define('backEndJsPath','public/backEnd/js');
define('COMMON_ERROR','Some error occured. Please try again later after sometime');
define('CURRENCY',"₹");

define('systemImgPath','public/images/system');
// controller
define('AdminProfileBasePath','public/images/profile/admin');
define('TrainerProfileBasePath','public/images/profile/trainer');
define('StudentProfileBasePath','public/images/profile/student');
define('TrainerContentBasePath','public/images/trainerContents');
define('TrainerContentImageBasePath','public/images/trainerContents/trainerContentImage');
define('HomeContentImageBasePath','public/images/homeContent');
define('AboutUsImageBasePath','public/images/aboutUs');
define('SuccessStoryImageBasePath','public/images/successStories');
define('OurFacultyImageBasePath','public/images/ourFaculty');
define('ExamImageBasePath','public/images/exam');
define('AdvertisementPopUpBasePath','public/images/advertisementPopUp');
define('BecomeTrainerBasePath','public/images/becomeTrainer');
define('RetailerImageBasePath','public/images/retailerImage');
define('NewsEventImageBasePath','public/images/newsEvents');
define('QuestionBankImageBasePath','public/images/questionBank');
define('WalkthroughBasePath','public/images/walkins');
define('HomepageBannerBasePath','public/images/bannerImages');

define('DownloadBasePath','public/images/downloads');
// views
define('systemImagePath',asset('public/images/system'));
define('TrainerProfileImgPath',asset('public/images/profile/trainer'));
define('AdminProfileImgath',asset('public/images/profile/admin'));
define('StudentProfileImgPath',asset('public/images/profile/student'));
define('DefaultUserPath',asset('public/images/system/d2.jpg'));
define('DefaultImgPath',asset('public/images/system/default-image.png'));
define('DefaultVideoImgPath',asset('public/images/system/vidlec.png'));
define('DefaultPdfImgPath',asset('public/images/system/note.png'));
define('TrainerContentImgPath',asset('public/images/trainerContents'));
define('TrainerContentImageImgPath',asset('public/images/trainerContents/trainerContentImage'));
define('HomeContentImageImgPath',asset('public/images/homeContent'));
define('AboutUsImageImgPath',asset('public/images/aboutUs'));
define('SuccessStoryImageImgPath',asset('public/images/successStories'));
define('OurFacultyImageImgPath',asset('public/images/ourFaculty'));
define('ExamImageImgPath',asset('public/images/exam'));
define('AdvertisementPopUpImgPath',asset('public/images/advertisementPopUp'));
define('BecomeTrainerImgPath',asset('public/images/becomeTrainer'));
define('RetailerImageImgPath',asset('public/images/retailerImage'));
define('NewsEventImageImgPath',asset('public/images/newsEvents'));
define('QuestionBankImageImgPath',asset('public/images/questionBank'));
define('DefaultCsvImgPath',asset('public/images/system/sample.csv'));
define('WalkthroughImgPath',asset('public/images/walkins'));
define('HomepageBannerImgPath',asset('public/images/bannerImages'));
define('DownloadImgPath',asset('public/images/downloads'));
define("UNAUTHORIZE_ERR", "Sorry, You are not authorized to access this page.");